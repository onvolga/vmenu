<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Di­n раn");
	define("TOPIC_INFO_TITLE", "Thфng tin chь р«");
	define("TOPIC_MESSAGE_TITLE", "Tin");

	define("MY_FORUM_TOPICS_MSG", "chь р« di­n раn cьa tфi");
	define("ALL_FORUM_TOPICS_MSG", "T¤t cд chь р« di­n раn");
	define("MY_FORUM_TOPICS_DESC", "BХn gЈp v¤n р« hoЈc bХn cу gЈp v¤n р« vа рг cу kinh nghi®m giдi quyЄt рЯюc? BХn muЇn thЎc mЎc hay muЇn chia sл nhЯng kinh nghi®m quэ bбu cьa mмnh cho mчi ngЯ¶i? TХi sao bХn реng kэ thаnh thаnh viкn cьa di­n раn vа gia nh§p cфng р°ng.");
	define("NEW_TOPIC_MSG", "chь р« mѕi");
	define("NO_TOPICS_MSG", "Khфng tмm th¤y chь р«");
	define("FOUND_TOPICS_MSG", "Chъng tфi tмm th¤y <b>{found_record}</b> chь р« рбp Сng yкu cҐu '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Khфng tмm th¤y di­n раn");

	define("FORUM_NAME_COLUMN", "Di­n раn");
	define("FORUM_TOPICS_COLUMN", "Cбc chь р«");
	define("FORUM_REPLIES_COLUMN", "Cбc h°i рбp");
	define("FORUM_LAST_POST_COLUMN", "LҐn c§p nh§t cuЇi");
	define("FORUM_MODERATORS_MSG", "Cбc quдn trё viкn");

	define("TOPIC_NAME_COLUMN", "chь р«");
	define("TOPIC_AUTHOR_COLUMN", "Tбc giд");
	define("TOPIC_VIEWS_COLUMN", "SЇ ngЯ¶i xem");
	define("TOPIC_REPLIES_COLUMN", "SЇ lҐn h°i рбp");
	define("TOPIC_UPDATED_COLUMN", "LҐn c§p nh§t cuЇi");
	define("TOPIC_ADDED_MSG", "Cбm Ѕn bХn <br>chь р« cьa bХn рг рЯюc thкm");

	define("TOPIC_ADDED_BY_FIELD", "Thкm b·i");
	define("TOPIC_ADDED_DATE_FIELD", "Thкm");
	define("TOPIC_UPDATED_FIELD", "LҐn c§p nh§t cuЇi");
	define("TOPIC_NICKNAME_FIELD", "Tкn nick");
	define("TOPIC_EMAIL_FIELD", "Рёa chп email cьa bХn");
	define("TOPIC_NAME_FIELD", "chь р«");
	define("TOPIC_MESSAGE_FIELD", "Nµi dung");
	define("TOPIC_NOTIFY_FIELD", "Gщi t¤t cд phдn h°i рЄn email cьa tфi");

	define("ADD_TOPIC_BUTTON", "Thкm chь р«");
	define("TOPIC_MESSAGE_BUTTON", "Thкm nµi dung");

	define("TOPIC_MISS_ID_ERROR", "Sai tham sЇ<b>ID chь р«</b>");
	define("TOPIC_WRONG_ID_ERROR", "<b>ID chь р«</b> cу tham sЇ bё lІi.");
	define("FORUM_SEARCH_MESSAGE", "Chъng tфi tмm th¤y {search_count} nµi dung рбp Сng рi«u ki®n '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Xem trЯѕc");
	define("TOPIC_SAVE_BUTTON", "LЯu lХi");

	define("LAST_POST_ON_SHORT_MSG", "Trкn:");
	define("LAST_POST_IN_SHORT_MSG", "Trong:");
	define("LAST_POST_BY_SHORT_MSG", "B·i:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "LҐn chпnh sШa cuЇi:");

?>