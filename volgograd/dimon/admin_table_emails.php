<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_table_emails.php                                   ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	$table_name = $table_prefix . "newsletters_users";
	$table_alias = "e";
	$table_pk = "email_id";
	$table_title = EMAILS_MSG;
	$min_column_allowed = 1;

	$db_columns = array(
		"email_id" => array(EMAIL_ID_MSG, INTEGER, 1, false),
		"email" => array(EMAIL_FIELD, TEXT, 2, false),
		"date_added" => array(DATE_ADDED_MSG, DATETIME, 2, false),
	);

	$db_aliases["id"] = "email_id";

?>
