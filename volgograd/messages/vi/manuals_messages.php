<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Ta i liк u hЯЅ ng dв n");
	define("NO_MANUALS_MSG", "Khфng ti m thв y ta i liк u");
	define("NO_MANUAL_ARTICLES_MSG", "Khфng co  chu  рк ");
	define("MANUALS_PREV_ARTICLE", "Kк  sau");
	define("MANUALS_NEXT_ARTICLE", "Kк  tiк p");
	define("MANUAL_CONTENT_MSG", "Sе p xк p");
	define("MANUALS_SEARCH_IN_MSG", "Ti m kiк m trong");
	define("MANUALS_SEARCH_FOR_MSG", "Ti m kiк m");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Tв t ca  ta i liк u");
	define("MANUALS_SEARCH_RESULTS_INFO", "Ti m thв y {results_number} chu  рк  cho рiк u kiк n {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Kк t qua  ti m kiк m");
	define("MANUALS_NOT_FOUND_ANYTHING", "Khфng ti m thв y '{seach_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Khфng nф i dung");
	define("MANUALS_SEARCH_TITLE", "Ti m khфng tЯ  рф ng");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Kк t qua  ti m kiк m");

?>