<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  export_functions.php                                     ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	function get_field_value($field_source)
	{
		global $db, $dbe, $db_columns, $related_columns, $related_table_alias, $apply_translation, $date_formats, $date_edit_format, $datetime_edit_format;

		if (preg_match_all("/\{(\w+)\}/i", $field_source, $matches)) {
			$field_value = $field_source;
			for($p = 0; $p < sizeof($matches[1]); $p++) {
				$f_source = $matches[1][$p];
				// get field type
				$column_type = TEXT; $column_name = ""; $column_format = "";
				if (isset($db_columns[$f_source])) {
					$column_type = $db_columns[$f_source][1];
					$column_name = $f_source;
				} else if (isset($related_table_alias) && $related_table_alias && preg_match("/^".$related_table_alias."_/", $f_source)) {
					$related_column_name = preg_replace("/^".$related_table_alias."_/", "", $f_source);
					if (isset($related_columns[$related_column_name])) {
						$column_type = $related_columns[$related_column_name][1];
						$column_name = $f_source;
					}
				} else {
					$date_formats_regexp = implode("|", $date_formats);
					if (preg_match("/".$date_formats_regexp."$/", $f_source, $format_match)) {
						$f_source_wf = preg_replace("/_".$format_match[0]."$/", "", $f_source);
						if (isset($db_columns[$f_source_wf]) && ($db_columns[$f_source_wf][1] == DATE || $db_columns[$f_source_wf][1] == DATETIME)) {
							$column_name = $f_source_wf;
							$column_type = $db_columns[$column_name][1];
							$column_format = $format_match[0];
						}
					}
				}

				if ($column_name) {
					if ($column_type == DATE) {
						$f_source_value = $dbe->f($column_name, DATETIME);
						if (is_array($f_source_value)) {
							if ($column_format) {
								$f_source_value = va_date(array($column_format), $f_source_value);
							} else {
								$f_source_value = va_date($date_edit_format, $f_source_value);
							}
						}
					} else if ($column_type == DATETIME) {
						$f_source_value = $dbe->f($column_name, DATETIME);
						if (is_array($f_source_value)) {
							if ($column_format) {
								$f_source_value = va_date(array($column_format), $f_source_value);
							} else {
								$f_source_value = va_date($datetime_edit_format, $f_source_value);
							}
						}
					} else {
						$f_source_value = $dbe->f($column_name);
						if ($apply_translation) {
							$f_source_value = get_translation($f_source_value);
						}
					}
					$field_value = str_replace("{".$f_source."}", $f_source_value, $field_value);
				}
			}
		} else {
			$field_value = $field_source;
		}

		return $field_value;
	}

	function set_db_column($column_title, $field_source, $column_checked, $column_link = "")
	{
		global $t, $db, $total_columns, $table_alias, $checked_columns;

		$total_columns++;
		//$column_checked = in_array($table_alias.".".$column_name, $checked_columns) ? " checked " : "";
		$t->set_var("col", $total_columns);
		$t->set_var("field_source", htmlspecialchars($field_source));
		$t->set_var("column_link", $column_link);
		$t->set_var("column_checked", $column_checked);
		$t->set_var("column_title", htmlspecialchars($column_title));
		$t->parse("rows", true);
		if($total_columns % 2 == 0) {
			$t->parse("columns", true);
			$t->set_var("rows", "");
		}
	}

?>