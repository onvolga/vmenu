<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "התקנת חנות ViArt");

	define("INSTALL_STEP_1_TITLE", "התקנה: צעד מס' 1");
	define("INSTALL_STEP_1_DESC", "תודה שבחרת בחנות ViArt. כדי להמשיך בהתקנה, נא למלא את הפרטים הנדרשים בהמשך. שים לב שבסיס הנתונים שבחרת חייב להיות קיים. אם בחרת בסיס נתונים המשתמש ב-ODBC, לדוגמא MS Access, לפני שתמשיך עליך ליצור עבורו DSN.");
	define("INSTALL_STEP_2_TITLE", "התקנה צעד מס' 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "התקנה צעד מס' 3");
	define("INSTALL_STEP_3_DESC", "נא לבחור מבנה אתר. תוכל לשנות את המבנה מאוחר יותר.");
	define("INSTALL_FINAL_TITLE", "התקנה: סיום");
	define("SELECT_DATE_TITLE", "בחר בתבנית תאריך");

	define("DB_SETTINGS_MSG", "הגדרות בסיס נתונים");
	define("DB_PROGRESS_MSG", "התקדמות איוש מבנה בסיס הנתונים");
	define("SELECT_PHP_LIB_MSG", "בחר בספרית PHP");
	define("SELECT_DB_TYPE_MSG", "בחר סוג בסיס נתונים");
	define("ADMIN_SETTINGS_MSG", "הגדרות מנהליות");
	define("DATE_SETTINGS_MSG", "תבנית תאריך");
	define("NO_DATE_FORMATS_MSG", "אין תבניות תאריך");
	define("INSTALL_FINISHED_MSG", "בנקודה זו הושלמה התקנה בסיסית. נא בדוק את ההגדרות בחלק המנהלי ובצע שנויים נדרשים");
	define("ACCESS_ADMIN_MSG", "הקש כאן כדי להגיע לחלק המינהלי");
	define("ADMIN_URL_MSG", "URL של המנהלה");
	define("MANUAL_URL_MSG", "URL ידני");
	define("THANKS_MSG", "תודה שבחרת <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "סוג בסיס נתונים");
	define("DB_TYPE_DESC", "נא לבחור ב- <b>type of database</b> . אם אתה משתמש ב- SQL Server או Microsoft Access, נא לבחור ODBC.");
	define("DB_PHP_LIB_FIELD", "סיפרית PHP");
	define("DB_HOST_FIELD", "שם מארח");
	define("DB_HOST_DESC", "נא הכנס את <b>name</b> או <b>IP address of the server</b> שעליו בסיס הנתונים של ה- ViArt ירוץ. אם אתה מריץ את בסיס הנתונים שלך על PC מקומי תוכל להשאיר את  \"<b>localhost</b>\" ואת ה-port ריק. אם אתה משתמש בבסיס הנתונים של החברה המארחת, נא לראות את הגדרות השרת בתעוד שלה");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "שם בסיס הנתונים / DSN");
	define("DB_NAME_DESC", "אם אתה משתמש בבסיס נתונים מסוג s MySQL או PostgreSQL נא הכנס את <b>name of the database</b> במקום בו הינך רוצה ש- ViArt יצור את הטבלאות שלו. בסיס נתונים זה חייב להיות קיים. אם אתה משתמש ב- ViArt למטרת בדיקה ב-PC המקומי שלך אזי לרוב המערכות יש בסיס נתונים \"<b>test</b>\" שאתה יכול להשתמש. אם לא, נא צור בסיס נתונים כגון \"viart\" והשתמש בו. אם אתה משתמש ב- Microsoft Access or SQL Server אזי שם בסיס הנתונים צריך להיות ה-  <b>name of the DSN</b> ששמת בחלק ה-Data Sources בלוח הבקרה שלך.");
	define("DB_USER_FIELD", "שם משתמש");
	define("DB_PASS_FIELD", "סיסמא");
	define("DB_USER_PASS_DESC", "<b>Username</b> ו- <b>Password</b> - נא להכניס את שם המשתמש והסיסמא לגישה לבסיס הנתונים. אם אתה משתמש בהתקנה מקומית אזי שם המשתמש יהיה בוודאי \"<b>root</b>\" והסיסמא ריקה. זה מתאים למטרות בדיקה אבל לא בטוח לשימוש בשרת הסופי.");
	define("DB_PERSISTENT_FIELD", "חיבור מתמיד");
	define("DB_PERSISTENT_DESC", "כדי להשתמש בחיבור המתמיד של MySQL או Postgre סמן תיבה זו.אם אינך יודע את המשמעות השאר את התיבה בלתי מסומנת.");
	define("DB_CREATE_DB_FIELD", "צור DB");
	define("DB_CREATE_DB_DESC", "כדי ליצור בסיס נתונים, אם זה אפשרי, סמן תיבה זו. מתאים לשימוש רק עם MySQL ו- Postgre");
	define("DB_POPULATE_FIELD", "אייש DB");
	define("DB_POPULATE_DESC", "כדי ליצור את מבנה הטבלא של בסיס הנתונים ולמלא אותה במידע, סמן תיבה זו.");
	define("DB_TEST_DATA_FIELD", "מידע נסיון");
	define("DB_TEST_DATA_DESC", "כדי להוסיף מידע נסיון לבסיס הנתונים שלך, סמן תיבה זו");
	define("ADMIN_EMAIL_FIELD", "אי-מייל של המנהל");
	define("ADMIN_LOGIN_FIELD", "כניסת המנהל");
	define("ADMIN_PASS_FIELD", "סיסמת המנהל");
	define("ADMIN_CONF_FIELD", "אשר סיסמא");
	define("DATETIME_SHOWN_FIELD", "תבנית תאריך ושעה (נראה באתר)");
	define("DATE_SHOWN_FIELD", "תבנית תאריך ושעה (נראה באתר)");
	define("DATETIME_EDIT_FIELD", "תבנית תאריך ושעה (לעריכה)");
	define("DATE_EDIT_FIELD", "תבנית תאריך (לעריכה)");
	define("DATE_FORMAT_COLUMN", "תבנית תאריך");
	define("CURRENT_DATE_COLUMN", "תאריך נוכחי");

	define("DB_LIBRARY_ERROR", "פונקציות PHP עבור {db_library} לא הוגדרו. נא לבדוק את ערכי בסיס הנתונים בקובץ ההגדרות - php.ini.");
	define("DB_CONNECT_ERROR", "לא מצליח להתחבר לבסיס הנתונים. נא לבדוק את הגדרות בסיס הנתונים.");
	define("INSTALL_FINISHED_ERROR", "תהליך ההתקנה הסתיים");
	define("WRITE_FILE_ERROR", "אין לך אשורי כתיבה לקובץ <b>'includes/var_definition.php'</b>. נא לשנות את אשורי הקובץ לפני שאתה ממשיך.");
	define("WRITE_DIR_ERROR", "אין לך אישור כתיבה לתיקיה <b>'includes/'</b>. נא לשנות את אשורי התיקיה לפני שאתה ממשיך.");
	define("DUMP_FILE_ERROR", "קובץ '{file_name}' לא נמצא.");
	define("DB_TABLE_ERROR", "טבלאת '{table_name}' לא נמצאה. נא לאייש את בסיס הנתונים במידע הנדרש.");
	define("TEST_DATA_ERROR", "בדוק את <b>{POPULATE_DB_FIELD}</b> לפני איוש הטבלא במידע נסיון");
	define("DB_HOST_ERROR", "אין אפשרות למצוא את שם המארח שסיפקת ");
	define("DB_PORT_ERROR", "לא ניתן להתחבר לשרת בסיס הנתונים ע\"י ה-Port שסיפקת");
	define("DB_USER_PASS_ERROR", "שם המשתמש או הסיסמא שסיפקת אינם נכונים");
	define("DB_NAME_ERROR", "ערכי הכניסה הם נכונים, אך בסיס הנתונים '{db_name}' לא נמצא.");

	// upgrade messages
	define("UPGRADE_TITLE", "שדרוג חנות ViArt");
	define("UPGRADE_NOTE", "הערה: כדאי לבצע גבוי לבסיס הנתונים לפני ההמשך");
	define("UPGRADE_AVAILABLE_MSG", "שדרוג בסיס הנתונים זמין");
	define("UPGRADE_BUTTON", "שדרג את בסיס הנתונים ל- {version_number} עכשיו");
	define("CURRENT_VERSION_MSG", "גירסא מותקנת נוכחית");
	define("LATEST_VERSION_MSG", "גירסא זמינה להתקנה");
	define("UPGRADE_RESULTS_MSG", "תוצאות שדרוג");
	define("SQL_SUCCESS_MSG", "שאלת SQL הצליחה");
	define("SQL_FAILED_MSG", "שאלת SQL נכשלה");
	define("SQL_TOTAL_MSG", "סך כל שאלות SQL שבוצעו");
	define("VERSION_UPGRADED_MSG", "בסיס הנתונים שלך שודרג ל-");
	define("ALREADY_LATEST_MSG", "ברשותך הגירסא האחרונה");
	define("DOWNLOAD_NEW_MSG", "נמצאה גירסא חדשה");
	define("DOWNLOAD_NOW_MSG", "הורד גירסא {version_number} עכשיו");
	define("DOWNLOAD_FOUND_MSG", "נמצא שגירסה חדשה מס' {version_number} זמינה להורדה. נא הקש על הקישור שלמטה להתחלת ההורדה. לאחר השלמת ההורדה והחלפת הקבצים אל תשכח להריץ את שיגרת השדרוג מחדש.");
	define("NO_XML_CONNECTION", "אזהרה! אין קשר אל 'http://www.viart.com/' !");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>