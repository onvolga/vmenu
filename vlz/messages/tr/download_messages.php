<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download iletileri
	define("DOWNLOAD_WRONG_PARAM", "Yanlэю download parametresi.");
	define("DOWNLOAD_MISS_PARAM", "Eksik download parametresi");
	define("DOWNLOAD_INACTIVE", "Download aktif deрil.");
	define("DOWNLOAD_EXPIRED", "Download sьresi dolmuю.");
	define("DOWNLOAD_LIMITED", "Azami download etme sayэsэnэ doldurmuюsunuz.");
	define("DOWNLOAD_PATH_ERROR", "Yazdэрэnэz ьrьn adresi bulunamadэ.");
	define("DOWNLOAD_RELEASE_ERROR", "Model bulunamadэ.");
	define("DOWNLOAD_USER_ERROR", "Bu dosyayэ sadece kayэtlэ ьyeler indirebilir.");
	define("ACTIVATION_OPTIONS_MSG", "Etkinleюtirme Seзenekleri");
	define("ACTIVATION_MAX_NUMBER_MSG", "Azami Etkinleюtirme Sayэsэ");
	define("DOWNLOAD_OPTIONS_MSG", "Эndirilebilir / Yazэlэm Seзenekleri");
	define("DOWNLOADABLE_MSG", "Эndirilebilir (Yazэlэm)");
	define("DOWNLOADABLE_DESC", "Эndirilebilir ьrьn iзin aynэ zamanda 'Эndirme Sьresi', 'Эndirilebilir Dosyanэn Yolu' ve 'Aktivasyon Seзenekleri'ni belirtebilirsiniz");
	define("DOWNLOAD_PERIOD_MSG", "Эndirme Sьresi");
	define("DOWNLOAD_PATH_MSG", "Эndirilebilir Dosyaya ulaюэm");
	define("DOWNLOAD_PATH_DESC", "Noktalэ virgьl ile ayэrarak birden зok yol girebilirsiniz.");
	define("UPLOAD_SELECT_MSG", "Yьklenecek dosyayэ seзip {button_name} butonuna tэklayэnэz.");
	define("UPLOADED_FILE_MSG", "<b>{filename}</b> dosyasэ yьklendi.");
	define("UPLOAD_SELECT_ERROR", "Lьtfen цnce bir dosya seзiniz.");
	define("UPLOAD_IMAGE_ERROR", "Sadece resim dosyalarэ yьklenebilir.");
	define("UPLOAD_FORMAT_ERROR", "Bu tьr dosya yьkleme izni yoktur.");
	define("UPLOAD_SIZE_ERROR", "{filesize} ndan geniю dosyalarэ yьkleme izni yoktur.");
	define("UPLOAD_DIMENSION_ERROR", "{dimension} den daha geniю resimleri yьkleme izni yoktur.");
	define("UPLOAD_CREATE_ERROR", "Sistem dosyayэ oluюturamadэ");
	define("UPLOAD_ACCESS_ERROR", "Dosya yьkleme izniniz yok");
	define("DELETE_FILE_CONFIRM_MSG", "Bu dosyayэ silmek istediрinizden emin misiniz?");
	define("NO_FILES_MSG", "Dosya bulunamadэ");
	define("SERIAL_GENERATE_MSG", "Seri Numarasэ Ьret");
	define("SERIAL_DONT_GENERATE_MSG", "ьretme");
	define("SERIAL_RANDOM_GENERATE_MSG", "Yazэlэm ьrьnь iзin rasgele seri numarasэ ьret");
	define("SERIAL_FROM_PREDEFINED_MSG", "Tanэmlanmэю listeden seri numarasэ al");
	define("SERIAL_PREDEFINED_MSG", "Tanэmlanmэю seri numaralarэ");
	define("SERIAL_NUMBER_COLUMN", "Seri Numarasэ");
	define("SERIAL_USED_COLUMN", "Kullanэlmэю");
	define("SERIAL_DELETE_COLUMN", "Sil");
	define("SERIAL_MORE_MSG", "Daha зok seri numarasэ eklensin mi?");
	define("SERIAL_PERIOD_MSG", "seri numarasэ aralэрэ");
	define("DOWNLOAD_SHOW_TERMS_MSG", "юart ve Koюullarэ gцster");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Yazэlэmэ indirebilmesi iзin kullanэcэnэn юart ve koюullarэ okumuю ve onaylэyor olmasэ gerekiyor");
	define("DOWNLOAD_TERMS_MSG", "Юartlar ve Koюullar");
	define("DOWNLOAD_TERMS_USER_DESC", "Юartlar ve Koюullarэ okudum ve onaylэyorum");
	define("DOWNLOAD_TERMS_USER_ERROR", "Ьrьnь indirebilmek iзin юartlarэmэzэ ve koюullarэmэzэ okumuю ve onaylэyor olmanэz gerekiyor");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>