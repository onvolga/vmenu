<?php	

	$category_id        = get_param("category_id");
	$search_category_id = get_param("search_category_id");
	if (strlen($search_category_id)) {
		$category_id = $search_category_id;
	} elseif (!strlen($category_id)) {
		$category_id = 0;
	}

	$html_template = get_setting_value($block, "html_template", "block_blz_category_title.html"); 
	$t->set_file("block_body", $html_template);

	$sql  = " SELECT category_name, a_title ";
	$sql .= " FROM " . $table_prefix . "categories WHERE category_id = " . $db->tosql($category_id, INTEGER);
	$db->query($sql);
	if ($db->next_record())
	{
		$category_name = get_translation($db->f("category_name"));
		$a_title = get_translation($db->f("a_title"));
		if(strlen($a_title)){
			$category_name = $a_title;
		}
	}

	if (!strlen($category_name)){
			
  		$category_name = "Каталог интернет-магазина";
			
	}

	$t->set_var("category_name", $category_name);

	$block_parsed = true;

?>