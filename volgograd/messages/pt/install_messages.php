<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "Instalaзгo ViArt SHOP");

	define("INSTALL_STEP_1_TITLE", "Instalaзгo: 1є Passo");
	define("INSTALL_STEP_1_DESC", "Obrigado por escolher a ViArt SHOP. Para continuar a instalaзгo, por favor, preencha os dados pedidos abaixo. Por favor, certifique-se de que a base de dados que seleccionar jб existe. Se instalar numa base de dados que utilize ODBC (ex.: MS Access), primeiro, deverб criar um DSN para prosseguir.");
	define("INSTALL_STEP_2_TITLE", "Instalaзгo: 2є Passo");
	define("INSTALL_STEP_2_DESC", " ");
	define("INSTALL_STEP_3_TITLE", "Instalaзгo: 3є Passo");
	define("INSTALL_STEP_3_DESC", "Por favor, seleccione um layout do site. Terб a possibilidade de alterar o layout posteriormente.");
	define("INSTALL_FINAL_TITLE", "Fim da Instalaзгo");
	define("SELECT_DATE_TITLE", "Seleccionar o Formato da Data");

	define("DB_SETTINGS_MSG", "Definiзхes da Base de Dados");
	define("DB_PROGRESS_MSG", "A carregar o progresso da estrutura da base de dados");
	define("SELECT_PHP_LIB_MSG", "Seleccionar a Biblioteca PHP");
	define("SELECT_DB_TYPE_MSG", "Seleccionar o Tipo de Base de Dados");
	define("ADMIN_SETTINGS_MSG", "Definiзхes Administrativas");
	define("DATE_SETTINGS_MSG", "Formatos da Data");
	define("NO_DATE_FORMATS_MSG", "Nгo hб formatos da data disponнveis");
	define("INSTALL_FINISHED_MSG", "Atй este ponto, a instalaзгo bбsica estб concluнda. Por favor, verifique as definiзхes na secзгo de administraзгo e efectue as necessбrias modificaзхes.");
	define("ACCESS_ADMIN_MSG", "Para ter acesso а secзгo da administraзгo, clique aqui");
	define("ADMIN_URL_MSG", "URL da Administraзгo");
	define("MANUAL_URL_MSG", "URL do Manual");
	define("THANKS_MSG", "Obrigado por ter escolhido <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "Tipo de Base de Dados");
	define("DB_TYPE_DESC", "Por favor, seleccione <b>type of database</b> que estб a utilizar. Se estiver a utilizar o SQL Server ou o Microsoft Access, por favor, seleccione ODBC.");
	define("DB_PHP_LIB_FIELD", "Biblioteca PHP");
	define("DB_HOST_FIELD", "Nome do Host");
	define("DB_HOST_DESC", "Por favor, digite o <b>name</b> ou <b>IP address of the server</b> no qual a sua base de dados ViArt irб correr. Se estiver a correr a sua base de dados no seu PC local, entгo poderб deixar apenas como \"<b>localhost</b>\" e a porta em branco. Se estiver a utilizar uma base de dados proveniente do servidor da empresa, por favor, veja a documentaзгo do servidor da sua empresa para as definiзхes do servidor.");
	define("DB_PORT_FIELD", "Nъmero da Porta");
	define("DB_NAME_FIELD", "Nome da Base de Dados / DSN");
	define("DB_NAME_DESC", "Se estiver a utilizar uma base de dados como o MySQL ou o PostgreSQL, por favor, digite <b>name of the database</b> onde gostasse que o ViArt criasse as suas tabelas. Esta base de dados jб deverб existir. Se estб a instalar o ViArt apenas com o propуsito de o testar no seu PC local, entгo a maioria dos sistemas possui \"<b>test</b>\" base de dados que pode utilizar. Senгo, por favor, crie uma base de dados similar а \"viart\" e use-a. Se estiver a utilizar o Microsoft Access ou o SQL Server, o nome da base de dados deverб ser <b>name of the DSN</b> que tinha ajustado na secзгo Data Sources (ODBC) do seu Painel de Controlo.");
	define("DB_USER_FIELD", "Nome do Utilizador");
	define("DB_PASS_FIELD", "Senha");
	define("DB_USER_PASS_DESC", "<b>Username</b> e <b>Password</b> - por favor, digite o Nome de Utilizador e a Senha que pretende utilizar para ter acesso а base de dados. Se estiver a utilizar um teste de instalaзгo local, o Nome de Utilizador й, provavelmente, \"<b>root</b>\" e a Senha fica, provavelmente, em branco. Mas repare que, apesar de ser bom testar, nгo й seguro em servidores de produзгo.");
	define("DB_PERSISTENT_FIELD", "Conexгo persistente");
	define("DB_PERSISTENT_DESC", "to use MySQL or Postgre persistent connections, tick this box. If you do not know what it means, then leaving it unticked is probably best.");
	define("DB_CREATE_DB_FIELD", "Criar Base de Dados");
	define("DB_CREATE_DB_DESC", "para criar base de dados clique nesta caixa. Funciona apenas para o MySQL e o Postgre.");
	define("DB_POPULATE_FIELD", "Carregar a Base de Dados");
	define("DB_POPULATE_DESC", "para criar a estrutura tabelar da base de dados e carregб-la com dados, assinale na caixa");
	define("DB_TEST_DATA_FIELD", "Testar Dados");
	define("DB_TEST_DATA_DESC", "para adicionar alguns dados de teste а sua base de dados, assinale na caixa");
	define("ADMIN_EMAIL_FIELD", "E-mail do Administrador");
	define("ADMIN_LOGIN_FIELD", "Login do Administrador");
	define("ADMIN_PASS_FIELD", "Senha do Administrator");
	define("ADMIN_CONF_FIELD", "Confirmar a Senha");
	define("DATETIME_SHOWN_FIELD", "Formato da Data e da Hora (mostrado no site)");
	define("DATE_SHOWN_FIELD", "Formato da Data (mostrado no site)");
	define("DATETIME_EDIT_FIELD", "Formato da Data e da Hora (para editar)");
	define("DATE_EDIT_FIELD", "Formato da Data (para editar)");
	define("DATE_FORMAT_COLUMN", "Formato da Data");
	define("CURRENT_DATE_COLUMN", "Data Actual");

	define("DB_LIBRARY_ERROR", "As funзхes de PHP para {db_library} nгo estгo definidas. Por favor, verifique as definiзхes da sua base de dados no seu ficheiro de configuraзгo - php.ini.");
	define("DB_CONNECT_ERROR", "Nгo conecta а base de dados. Por favor, verifique os parвmetros da sua base de dados.");
	define("INSTALL_FINISHED_ERROR", "Processo de instalaзгo finalizado.");
	define("WRITE_FILE_ERROR", "Nгo tem permissгo para gravar no arquivo <b>'includes/var_definition.php'</b>. Por favor, altere as permissхes de arquivo antes de continuar.");
	define("WRITE_DIR_ERROR", "Nгo tem permissгo para gravar na pasta <b>'includes/'</b>. Por favor, altere as permissхes de pasta antes de continuar.");
	define("DUMP_FILE_ERROR", "O ficheiro Dump '{file_name}' nгo foi encontrado.");
	define("DB_TABLE_ERROR", "A tabela '{table_name}' nгo foi encontrada. Por favor, construa a base de dados com os dados necessбrios.");
	define("TEST_DATA_ERROR", "Verificar <b>{POPULATE_DB_FIELD}</b> antes de preencher as tabelas com dados de teste");
	define("DB_HOST_ERROR", "O nome do host que especificou nгo foi encontrado.");
	define("DB_PORT_ERROR", "Nгo conecta ao servidor da base de dados, utilizando a porta especificada.");
	define("DB_USER_PASS_ERROR", "O Nome do Utilizador e/ou a Senha que especificou nгo estгo os correctos.");
	define("DB_NAME_ERROR", "As definiзхes de login estavam correctas, mas a base de dados '{db_name}' nгo foi encontrada.");

	// upgrade messages
	define("UPGRADE_TITLE", "Actualizaзгo ViArt SHOP");
	define("UPGRADE_NOTE", "Nota: por favor, faзa um backup da base de dados antes de continuar");
	define("UPGRADE_AVAILABLE_MSG", "Upgrade da base de dados disponнvel");
	define("UPGRADE_BUTTON", "Actualizar a base de dados para {version_number}, agora");
	define("CURRENT_VERSION_MSG", "Versгo actualmente instalada");
	define("LATEST_VERSION_MSG", "Versгo disponнvel para instalaзгo");
	define("UPGRADE_RESULTS_MSG", "Resultados da actualizaзгo");
	define("SQL_SUCCESS_MSG", "Consulta SQL bem sucedida");
	define("SQL_FAILED_MSG", "Consulta SQL mal sucedida");
	define("SQL_TOTAL_MSG", "Total de consultas SQL executadas");
	define("VERSION_UPGRADED_MSG", "A sua base de dados foi actualizada para");
	define("ALREADY_LATEST_MSG", "Jб possui a ъltima versгo");
	define("DOWNLOAD_NEW_MSG", "Uma nova versгo foi encontrada");
	define("DOWNLOAD_NOW_MSG", "Download da versгo {version_number}, agora");
	define("DOWNLOAD_FOUND_MSG", "Detectбmos que a nova versгo {version_number} estб disponнvel para download. Por favor, clique no link abaixo para iniciar o download. Depois de concluir o download e substitutir os ficheiros, nгo se esqueзa de correr o Upgrade novamente.");
	define("NO_XML_CONNECTION", "Aviso! A conexгo para 'http://www.viart.com/' nгo pode ser estabelecida");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>