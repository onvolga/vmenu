<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forumo юinutлs
	define("FORUM_TITLE", "Forumas");
	define("TOPIC_INFO_TITLE", "Temos informacija");
	define("TOPIC_MESSAGE_TITLE", "Юinutл");

	define("MY_FORUM_TOPICS_MSG", "Mano forumo temos");
	define("ALL_FORUM_TOPICS_MSG", "Visos forumo temos");
	define("MY_FORUM_TOPICS_DESC", "Ar jыs galvojote kad su jыsш bлda susidыrл kas nors kitas? Ar norite dalintis savo patirtimi su naujais vartotojais? Kodлl netapti forumo nariu ir jungtis prie bendruomenлs?");
	define("NEW_TOPIC_MSG", "Nauja tema");
	define("NO_TOPICS_MSG", "Nerasta jokiш temш");
	define("FOUND_TOPICS_MSG", "Mes radome <b>{found_records}</b> temas atitinkanиias sаlygas '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Nerasta jokiш forumш");

	define("FORUM_NAME_COLUMN", "Forumas");
	define("FORUM_TOPICS_COLUMN", "Temos");
	define("FORUM_REPLIES_COLUMN", "Atsakymai");
	define("FORUM_LAST_POST_COLUMN", "Paskutinis atnaujinimas");
	define("FORUM_MODERATORS_MSG", "Vedлjas");

	define("TOPIC_NAME_COLUMN", "Tema");
	define("TOPIC_AUTHOR_COLUMN", "Autorius");
	define("TOPIC_VIEWS_COLUMN", "Юiыrлjimai");
	define("TOPIC_REPLIES_COLUMN", "Atsakymai");
	define("TOPIC_UPDATED_COLUMN", "Paskutinis atnaujinimas");
	define("TOPIC_ADDED_MSG", "Dлkojame jums <br>Jыsш tema buvo pridлta");

	define("TOPIC_ADDED_BY_FIELD", "Kieno pridлta");
	define("TOPIC_ADDED_DATE_FIELD", "Pridлta");
	define("TOPIC_UPDATED_FIELD", "Paskutinis atnaujinimas");
	define("TOPIC_NICKNAME_FIELD", "Pravardл");
	define("TOPIC_EMAIL_FIELD", "Jыsш epaрto adresas");
	define("TOPIC_NAME_FIELD", "Tema");
	define("TOPIC_MESSAGE_FIELD", "Юinutл");
	define("TOPIC_NOTIFY_FIELD", "Siшskit visus atsakymus б mano epaрtа");

	define("ADD_TOPIC_BUTTON", "Pridлti temа");
	define("TOPIC_MESSAGE_BUTTON", "Pridлti юinutж");

	define("TOPIC_MISS_ID_ERROR", "Trыkstamas <b>Gijos ID</b> parametras.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Gijos ID</b> parametras turi blogа reikрmж.");
	define("FORUM_SEARCH_MESSAGE", "Mes radome {search_count} юinutes, atitinkanиias sаlygа(as) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Prieрюiыra");
	define("TOPIC_SAVE_BUTTON", "Saugoti");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>