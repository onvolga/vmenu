<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.8                                                ***
  ***      File:  ultimatepay_process.php                                  ***
  ***      Built: Tue Sep 13 18:30:31 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

/*
 * UltimatePay (http://www.ultimatepay.com/) transaction handler by www.viart.com
 */

 	$is_admin_path = true;
	$root_folder_path = "../";

	include_once ($root_folder_path ."includes/common.php");
	include_once ($root_folder_path ."includes/order_items.php");
	include_once ($root_folder_path ."includes/parameters.php");
	include_once ($root_folder_path ."messages/".$language_code."/cart_messages.php");

	$vc = get_session("session_vc");
	$order_id = get_session("session_order_id");

	$order_errors = check_order($order_id, $vc);
	if($order_errors) {
		echo $order_errors;
		exit;
	}

	$payment_parameters = array();
	$pass_parameters = array();
	$post_parameters = '';
	$pass_data = array();
	$variables = array();
	get_payment_parameters($order_id, $payment_parameters, $pass_parameters, $post_parameters, $pass_data, $variables);

	$pass_data = array();
	foreach ($payment_parameters as $parameter_name => $parameter_value) {
		if (isset($pass_parameters[$parameter_name]) && $pass_parameters[$parameter_name] == 1) {
			$pass_data[$parameter_name] = $parameter_value;
			if(isset($pass_data[strtolower($parameter_name)])){
				unset($pass_data[strtolower($parameter_name)]);
				$pass_data[$parameter_name] = $parameter_value;
			}
		}
	}

	$hash_string  = '';
	$hash_string .= (isset($payment_parameters['userid']))? $payment_parameters['userid']: "";
	$hash_string .= (isset($payment_parameters['adminpwd']))? $payment_parameters['adminpwd']: "";
	$hash_string .= (isset($payment_parameters['secret']))? $payment_parameters['secret']: "";
	$hash_string .= (isset($payment_parameters['pkgid']))? $payment_parameters['pkgid']: "";
	$hash_string .= (isset($payment_parameters['currency']))? $payment_parameters['currency']: "";
	$hash_string .= (isset($payment_parameters['amount']))? $payment_parameters['amount']: "";
	$hash_string .= (isset($payment_parameters['paymentid']))? $payment_parameters['paymentid']: "";
	$hash_string .= (isset($payment_parameters['merchtrans']))? $payment_parameters['merchtrans']: "";
	$hash_string .= (isset($payment_parameters['riskmode']))? $payment_parameters['riskmode']: "";
	$hash_string .= (isset($payment_parameters['developerid']))? $payment_parameters['developerid']: "";
	$hash_string .= (isset($payment_parameters['appid']))? $payment_parameters['appid']: "";

	$pass_data['hash'] = md5($hash_string);

	$t = new VA_Template('.'.$settings["templates_dir"]);
	$t->set_file("main","payment.html");
	$payment_name = 'PayByCash';
	$goto_payment_message = str_replace("{payment_system}", $payment_name, GOTO_PAYMENT_MSG);
	$goto_payment_message = str_replace("{button_name}", CONTINUE_BUTTON, $goto_payment_message);
	$t->set_var("GOTO_PAYMENT_MSG", $goto_payment_message);
	$t->set_var("payment_url",$payment_parameters['action_url']);
	$t->set_var("submit_method", "post");
	foreach ($pass_data as $parameter_name => $parameter_value) {
		$t->set_var("parameter_name", $parameter_name);
		$t->set_var("parameter_value", $parameter_value);
		$t->parse("parameters", true);
	}
	$t->sparse("submit_payment", false);
	$t->pparse("main");

	exit;
?>