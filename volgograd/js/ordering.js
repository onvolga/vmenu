// shopping javacript
function checkOrder(orderForm)
{
	var prMessage = "{REQUIRED_PROPERTY_MSG}";
	var prIDs = orderForm.properties.value;
	if (prIDs != "") {
		var properties = prIDs.split(",");
		for ( var i = 0; i < properties.length; i++) {
			var prID = properties[i];
			var cp = prID.split("_");
			var cartID = "";
			if (cp.length == 4) {
				cartID = cp[0] + "_" + cp[1] + "_" + cp[2];
			} else {
				cartID = cp[0];
			}
			if (orderForm.elements["property_required_" + prID] && orderForm.elements["property_required_" + prID].value == 1) {
				var productName = orderForm.elements["item_name_" + cartID].value;
				var prValue = "";
				var prControl = orderForm.elements["property_control_" + prID].value;
				if (prControl == "LISTBOX") {
					prValue = orderForm.elements["property_" + prID].options[orderForm.elements["property_" + prID].selectedIndex].value;
				} else if (prControl == "RADIOBUTTON") {
					var radioControl = orderForm.elements["property_" + prID];
					if (radioControl.length) {
						for ( var ri = 0; ri < radioControl.length; ri++) {
							if (radioControl[ri].checked) {
								prValue = radioControl[ri].value;
								break;
							}
						}
					} else {
						if (radioControl.checked) {
							prValue = radioControl.value;
						}
					}
				} else if (prControl == "CHECKBOXLIST") {
					if (orderForm.elements["property_total_" + prID]) {
						var totalOptions = parseInt(orderForm.elements["property_total_" + prID].value);
						for ( var ci = 1; ci <= totalOptions; ci++) {
							if (orderForm.elements["property_" + prID + "_" + ci].checked) {
								prValue = 1;
								break;
							}
						}
					}
				} else {
					prValue = orderForm.elements["property_" + prID].value;
				}
				if (prValue == "") {
					var propertyName = orderForm.elements["property_name_" + prID].value;
					prMessage = prMessage.replace("\{property_name\}", propertyName);
					prMessage = prMessage.replace("\{product_name\}", productName);
					alert(prMessage);
					if (prControl != "RADIOBUTTON" && prControl != "CHECKBOXLIST") {
						orderForm.elements["property_" + prID].focus();
					}
					return false;
				}
			}
		}
	}


	// check if all shipments selected
	var shippingError = "{REQUIRED_DELIVERY_MSG}";
	var shippingIndex = 1; 
	// read shipping groupds 
	while (orderForm.elements["shipping_type_id_"+shippingIndex]) {
		var shippingTypeId  = ""; 
		var shippingObj = orderForm.elements["shipping_type_id_"+shippingIndex];
		var shippingControl = (shippingObj.length) ? shippingObj[0].type : shippingObj.type;
		if (shippingControl == "select-one") {
			shippingTypeId = shippingObj.options[shippingObj.selectedIndex].value;
		} else if (shippingControl == "radio") { 
			for(var i = 0; i < shippingObj.length; i++) {
				var radioShippingId = shippingObj[i].value;
				if (shippingObj[i].checked) {
					shippingTypeId = radioShippingId;
				}
			}
		} else {
			shippingTypeId = shippingObj.value;
		}

		if (shippingTypeId == "") {
			alert(shippingError);
			if (shippingControl == "select-one") {
				shippingObj.focus();
			} else if (shippingControl == "radio") { 
				shippingObj[0].focus();
			}
			return false;
		}

		// get next index
		shippingIndex++;
	}

	orderForm.operation.value = 'save';
	return true;
}


function changeProperty()
{
	calculateOrder();
}

function changeShipping()
{
	calculateOrder();
}

function changeShippingList()
{
	calculateOrder();
}

function calculateItems()
{
	calculateOrder();
}

function calculateOrder()
{
	var orderForm = document.order_info;
	// initiliaze variables with shop settings
	var pricesType = parseFloat(orderForm.tax_prices_type.value);
	if (isNaN(pricesType)) { pricesType = 0; }
	var pointsRate = parseFloat(orderForm.points_rate.value);
	if (isNaN(pointsRate)) { pointsRate = 1; }
	var priceObj = "";

	// initialize array for total values
	var totalValues = new Array();

	// get tax rates 
	var taxRates = prepareData("tax_rates", "tax_id=");

	// calculate order goods
	var goodsTotal = 0; var goodsPoints = 0;

	// get all order items 
	var orderItems = prepareData("order_items", "cart_item_id=");
	if (orderItems instanceof Array) {
		for (cartId in orderItems) {
			if(!(orderItems[cartId] instanceof Function)){
				var orderItem = orderItems[cartId];
				var subcomponentsShowType = orderItem["subcomponents_show_type"];
				var parentCartId = orderItem["parent_cart_id"];
				var quantity = orderItem["quantity"];
				// check pay points variable
				var payPoints = 0;
				if (subcomponentsShowType == 1 && parentCartId != "") {
					if (orderForm.elements["pay_points_" + parentCartId] && orderForm.elements["pay_points_" + parentCartId].checked) {
						payPoints = 1;
					}
				} else if (orderForm.elements["pay_points_" + cartId] && orderForm.elements["pay_points_" + cartId].checked) {
					payPoints = 1;
				}
				if (payPoints != 1) {
					var price = orderItem["price"];
					var itemQuantity = orderItem["quantity"];
					var itemTypeId = orderItem["item_type_id"];
					var itemTaxId = orderItem["tax_id"];
					var taxFreeOption = orderItem["tax_free"];
	
					var priceTotal = Math.round(price * quantity * 100) / 100;
					var itemTaxes = getTaxAmount(taxRates, itemTypeId, priceTotal, itemQuantity, itemTaxId, taxFreeOption, 2) 
					var priceTax = getTaxAmount(taxRates, itemTypeId, priceTotal, itemQuantity, itemTaxId, taxFreeOption, 1) 
					taxRates = addTaxValues(taxRates, itemTaxes, "goods");
	
					goodsTotal += priceTotal;
				} else {
					var pointsPrice = orderItem["points_price"];
					goodsPoints += (pointsPrice * quantity);
				}
			}
		}
		// check total values
		totalValues = calculateTotals(totalValues, goodsTotal, taxRates, "goods")
		var goodsTotalControl = document.getElementById("goods_total_excl_tax");
		if (goodsTotalControl) {
			goodsTotalControl.innerHTML = currencyFormat(totalValues["goods_excl_tax"]);
		}
		var goodsTaxControl = document.getElementById("goods_tax_total");
		if (goodsTaxControl) {
			goodsTaxControl .innerHTML = currencyFormat(totalValues["goods_tax"]);
		}
		var goodsTotalInclTaxControl = document.getElementById("goods_total_incl_tax");
		if (goodsTotalInclTaxControl) {
			goodsTotalInclTaxControl.innerHTML = currencyFormat(totalValues["goods_incl_tax"]);
		}
	}
	// end of order goods calculations

	// calculate order properties
	var totalPropertiesPrice = 0; var totalPropertiesPoints = 0; var orderProperties = "";
	if (orderForm.order_properties) { orderProperties = orderForm.order_properties.value; }
	if (orderProperties != "") {

		var properties = orderProperties.split(",");
		for ( var i = 0; i < properties.length; i++) {
			var prID = properties[i];
			var prValue = "";
			var propertyPrice = 0;
			var prPayPoints = 0;
			if (orderForm.elements["property_pay_points_" + prID] && orderForm.elements["property_pay_points_" + prID].checked) {
				prPayPoints = 1;
			}
			var prControl = orderForm.elements["op_control_" + prID].value;
			var taxFreeOption = parseInt(orderForm.elements["op_tax_free_" + prID].value);
			if (prControl == "LISTBOX") {
				prValue = orderForm.elements["op_" + prID].options[orderForm.elements["op_" + prID].selectedIndex].value;
			} else if (prControl == "RADIOBUTTON") {
				var radioControl = orderForm.elements["op_" + prID];
				if (radioControl.length) {
					for ( var ri = 0; ri < radioControl.length; ri++) {
						if (radioControl[ri].checked) {
							prValue = radioControl[ri].value;
							break;
						}
					}
				} else {
					if (radioControl.checked) {
						prValue = radioControl.value;
					}
				}
			} else if (prControl == "CHECKBOXLIST") {
				if (orderForm.elements["op_total_" + prID]) {
					var totalOptions = parseInt(orderForm.elements["op_total_" + prID].value);
					for ( var ci = 1; ci <= totalOptions; ci++) {
						if (orderForm.elements["op_" + prID + "_" + ci].checked) {
							var checkedValue = orderForm.elements["op_" + prID + "_" + ci].value;
							if (orderForm.elements["op_option_price_" + checkedValue]) {
								var checkedPrice = parseFloat(orderForm.elements["op_option_price_" + checkedValue].value);
								if (!isNaN(checkedPrice) && checkedPrice!= 0) {
									propertyPrice += parseFloat(checkedPrice);
								}
							}
						}
					}
				}
			}
			if (prValue != "") {
				if (orderForm.elements["op_option_price_" + prValue]) {
					var optionPrice = orderForm.elements["op_option_price_" + prValue].value;
					if (optionPrice != "") {
						propertyPrice = parseFloat(optionPrice);
						if (prPayPoints == 1) {
							if (propertyPrice > 0) {
								totalPropertiesPoints += (propertyPrice * pointsRate);
							}
							propertyPrice = 0;
						}
					}
				}
			}
			
			if (isNaN(propertyPrice)) { propertyPrice = 0; }
			var propertiesTaxes = getTaxAmount(taxRates, "properties", propertyPrice, 1, 0, taxFreeOption, 2) 
			var propertyTax = getTaxAmount(taxRates, "properties", propertyPrice, 1, 0, taxFreeOption, 1) 
			taxRates = addTaxValues(taxRates, propertiesTaxes, "properties");
			totalPropertiesPrice += propertyPrice;
			var propertyPrices = calculatePrices(propertyPrice, propertyTax);

			var priceControl = document.getElementById("op_price_excl_tax_" + prID);
			if (priceControl) {
				if (propertyPrice == 0) {
					priceControl.innerHTML = "";
				} else {
					priceControl.innerHTML = currencyFormat(propertyPrices["excl_tax"]);
				}
			}
			var taxControl = document.getElementById("op_tax_" + prID);
			if (taxControl) {
				if (propertyPrice == 0) {
					taxControl.innerHTML = "";
				} else {
					taxControl.innerHTML = currencyFormat(propertyTax);
				}
			}
			var priceInclTaxControl = document.getElementById("op_price_incl_tax_" + prID);
			if (priceInclTaxControl) {
				if (propertyPrice == 0) {
					priceInclTaxControl.innerHTML = "";
				} else {
					priceInclTaxControl.innerHTML = currencyFormat(propertyPrices["incl_tax"]);
				}
			}
		}
		// check total values
		totalValues = calculateTotals(totalValues, totalPropertiesPrice, taxRates, "properties")
	}
	// end of properties calculations

	// calculate shipping
	var shippingTotalCost = 0;
	var shippingTotalPoints = 0;
	var shippingIndex = 1; var shippingObj = "";
	// read shipping groupds 
	while (orderForm.elements["shipping_type_id_"+shippingIndex]) {
		var shippingTypeId  = ""; 
		var shippingCost = 0; var shippingPoints = 0; var shippingPayPoints = 0;
		var shippingObj = orderForm.elements["shipping_type_id_"+shippingIndex];
		var shippingControl = (shippingObj.length) ? shippingObj[0].type : shippingObj.type;
		if (shippingControl == "select-one") {
			shippingTypeId = shippingObj.options[shippingObj.selectedIndex].value;
		} else if (shippingControl == "radio") { 
			for(var i = 0; i < shippingObj.length; i++) {
				var radioShippingId = shippingObj[i].value;
				if (shippingObj[i].checked) {
					shippingTypeId = radioShippingId;
				}
			}
		} else {
			shippingTypeId = shippingObj.value;
		}
		// check if user select to pay with points
		if (orderForm.elements["shipping_pay_points_"+shippingIndex] && orderForm.elements["shipping_pay_points_"+shippingIndex].checked) {
			shippingPayPoints = 1;
		}

		// get shipping methods for current group
		var shippingMethods = prepareData("shipping_methods_"+shippingIndex, "shipping_id=");
		// check shipping cost
		var shippingInclTax = ""; var shippingTax = ""; var shippingExclTax = "";
		if (shippingTypeId == "") {
			shippingObj = document.getElementById("shipping_cost_excl_tax_" + shippingIndex);
			if (shippingObj) { shippingObj.innerHTML = ""; }
			shippingObj = document.getElementById("shipping_tax_" + shippingIndex);
			if (shippingObj) { shippingObj.innerHTML = ""; }
			shippingObj= document.getElementById("shipping_cost_incl_tax_" + shippingIndex);
			if (shippingObj) { shippingObj.innerHTML = ""; }
		} else {
			shippingCost = parseFloat(shippingMethods[shippingTypeId]["cost"]);
			if (shippingPayPoints == 1) {
				shippingPoints = shippingCost * pointsRate;
				shippingCost = 0;
			}
			shippingTotalCost += shippingCost;
			shippingTotalPoints += shippingPoints;
			var shippingTaxFree = parseInt(shippingMethods[shippingTypeId]["tax_free"]); 
			var shippingTaxes = getTaxAmount(taxRates, "shipping", shippingCost, 1, 0, shippingTaxFree, 2) 
			var shippingTax = getTaxAmount(taxRates, "shipping", shippingCost, 1, 0, shippingTaxFree, 1) 
			if (shippingPayPoints == 1) {
				shippingTaxes = "";
				shippingTax = "";
			}
			taxRates = addTaxValues(taxRates, shippingTaxes, "shipping");
			var shippingPrices = calculatePrices(shippingCost, shippingTax);
			shippingExclTax = shippingPrices["excl_tax"];
			shippingInclTax = shippingPrices["incl_tax"];

			// update shipping cost in cart
			shippingObj = document.getElementById("shipping_cost_excl_tax_" + shippingIndex);
			if (shippingObj) { shippingObj.innerHTML = currencyFormat(shippingExclTax); }
			shippingObj = document.getElementById("shipping_tax_" + shippingIndex);
			if (shippingObj) { shippingObj.innerHTML = currencyFormat(shippingTax); }
			shippingObj= document.getElementById("shipping_cost_incl_tax_" + shippingIndex);
			if (shippingObj) { shippingObj.innerHTML = currencyFormat(shippingInclTax); }
		}

		// get next index
		shippingIndex++;
	}
	// end shipping checks

	// calculate total values
	totalValues = calculateTotals(totalValues, shippingTotalCost, taxRates, "shipping")
	// end shipping calculations

	// calculate discounts
	var maxDiscount = goodsTotal; var totalDiscount = 0; var totalDiscountTax = 0;
	var coupons = prepareData("order_coupons", "coupon_id=");
	if (coupons instanceof Array) {
		for (var couponId in coupons) {
			if(!(coupons[couponId] instanceof Function)){
				var coupon = coupons[couponId];
				var couponType = coupon["type"];
				var couponAmount = coupon["amount"];
				var couponTaxFree = coupon["tax_free"];
				var discountAmount = 0;
				var discountTax = 0;
				if (couponType == 1) {
					discountAmount = Math.round(goodsTotal * couponAmount) / 100;
				} else {
					discountAmount = couponAmount;
				}
				if (discountAmount > maxDiscount) {
					discountAmount = maxDiscount;
				}
				maxDiscount -= discountAmount;
				var discountTaxes = getDiscountTaxes(taxRates, totalValues, discountAmount, couponTaxFree, 2)
				var discountTax = getDiscountTaxes(taxRates, totalValues, discountAmount, couponTaxFree, 1)
				taxRates = addTaxValues(taxRates, discountTaxes, "discount");
				var discountPrices = calculatePrices(discountAmount, discountTax);

				totalDiscount += discountAmount;
				totalDiscountTax += discountTax;

				priceObj = document.getElementById("coupon_amount_excl_tax_" + couponId);
				if (priceObj) {
					priceObj.innerHTML = "- " + currencyFormat(discountPrices["excl_tax"]);
				}
				priceObj = document.getElementById("coupon_tax_" + couponId);
				if (priceObj) {
					priceObj.innerHTML = "- " + currencyFormat(discountTax);
				}
				priceObj = document.getElementById("coupon_amount_incl_tax_" + couponId);
				if (priceObj) {
					priceObj.innerHTML = "- " + currencyFormat(discountPrices["incl_tax"]);
				}
			}
		}
		// show discount and goods cost after discount total values
		totalValues = calculateTotals(totalValues, discountAmount, taxRates, "discount")

		priceObj = document.getElementById("total_discount_excl_tax");
		if (priceObj) {
			priceObj.innerHTML = "- " + currencyFormat(totalValues["discount_excl_tax"]);
		}
		priceObj = document.getElementById("total_discount_tax");
		if (priceObj) {
			priceObj.innerHTML = "- " + currencyFormat(totalValues["discount_tax"]);
		}
		priceObj = document.getElementById("total_discount_incl_tax");
		if (priceObj) {
			priceObj.innerHTML = "- " + currencyFormat(totalValues["discount_incl_tax"]);
		}

		priceObj = document.getElementById("discounted_amount_excl_tax");
		if (priceObj) {
			priceObj.innerHTML = currencyFormat(totalValues["goods_excl_tax"] - totalValues["discount_excl_tax"]);
		}
		priceObj = document.getElementById("discounted_tax_amount");
		if (priceObj) {
			priceObj.innerHTML = currencyFormat(totalValues["goods_tax"] - totalValues["discount_tax"]);
		}
		priceObj = document.getElementById("discounted_amount_incl_tax");
		if (priceObj) {
			priceObj.innerHTML = currencyFormat(totalValues["goods_incl_tax"] - totalValues["discount_incl_tax"]);
		}
		
	}

	// calculate and show taxes
	var taxesTotal = 0;
	for (var taxId in taxRates) {
		var taxObj = document.getElementById("tax_" + taxId);
		var taxTotal = 0;
		if (taxRates[taxId]["tax_total"]) {
			taxTotal = Math.round(taxRates[taxId]["tax_total"] * 100) / 100;
		}
		taxesTotal += taxTotal;
		if (taxObj) {
			taxObj.innerHTML = currencyFormat(taxTotal);
		}
	}

	//var goodsTotal = parseFloat(orderForm.goods_value.value); todo delete

	// calculate order total
	var orderTotal = goodsTotal - totalDiscount + totalPropertiesPrice + shippingTotalCost;
	if (pricesType != 1) {
		orderTotal += taxesTotal;
	}

	// calculate gift vouchers
	var vouchers = ("order_vouchers", "voucher_id=");
	if (vouchers instanceof Array) {
		for (var voucherId in vouchers) {
			if(!(vouchers[voucherId] instanceof Function)){
				var voucher = vouchers[voucherId];
				var voucherTitle = voucher["title"];
				var voucherMaxAmount = voucher["max_amount"];
				var voucherAmount = voucherMaxAmount;
				if (voucherAmount > orderTotal) {
					voucherAmount = orderTotal;
				}
				orderTotal -= voucherAmount;
				priceObj = document.getElementById("voucher_amount_" + voucherId);
				if (priceObj) {
					if (voucherAmount > 0) {
						priceObj.innerHTML = "- " + currencyFormat(voucherAmount);
					} else {
						priceObj.innerHTML = "";
					}
				}
			}
		}
	}
	// calculate processing fee
	var processingFees = ""; var processingFee = 0;
	var feeObj = document.getElementById("processing_fee");
	if (orderForm.processing_fees && (feeObj || orderForm.processing_fee)) {
		processingFees = orderForm.processing_fees.value;
		var feeType = 0;
		var feeValue = 0;
		if (processingFees != "") {
			var feesValues = processingFees.split(",");
			if (feesValues.length == 3) {
				feeType = parseInt(feesValues[1]);
				feeValue = parseFloat(feesValues[2]);
			} else if (feesValues.length > 3) {
				var paymentId = "";
				if (orderForm.payment_id)	{
					if (orderForm.payment_id.options) {
						paymentId = orderForm.payment_id.options[orderForm.payment_id.selectedIndex].value;
					} else if (orderForm.payment_id.length > 0) {
						for (var i = 0; i < orderForm.payment_id.length; i++) {
								if (orderForm.payment_id[i].checked) {
									paymentId = orderForm.payment_id[i].value;
									break;
								}
						}
					}
				}
				for (var f = 0; f < feesValues.length; f = f + 3) {
					feePayment = feesValues[f];
					if (paymentId == feePayment) {
						feeType = parseInt(feesValues[f + 1]);
						feeValue = parseFloat(feesValues[f + 2]);
						break;
					}
				}
			}
		}

		if (feeType == 1) {
			processingFee = Math.round(orderTotal * feeValue) / 100;
		} else {
			processingFee = feeValue;
		}
		if (feeObj) {
			feeObj.innerHTML = currencyFormat(processingFee);
		} else if (orderForm.processing_fee) {
			orderForm.processing_fee.value = currencyFormat(processingFee);
		}
	}
	orderTotal += processingFee;

	var orderTotalControl = document.getElementById("order_total_desc");
	if (orderTotalControl) {
		orderTotalControl.innerHTML = currencyFormat(orderTotal);
	}

	// disable payment gatewates which are out of orderTotal limitations

	var paymentsIdsList = orderForm.payments_ids_list.value;
	if (paymentsIdsList != "") {
		var minValuesList = orderForm.min_values_list.value;
		var maxValuesList = orderForm.max_values_list.value;
	  
		var paymentIdsArray = paymentsIdsList.split(";");
		var minValuesArray = minValuesList.split(";");
		var maxValuesArray = maxValuesList.split(";");
	  
		for (var i = 0; i < paymentIdsArray.length - 1; i++) { // -1 is for last delimiter

			var currentPaymentId = "payment_id_" + paymentIdsArray[i];
			var currentPaymentMin = parseFloat(minValuesArray[i]);
			var currentPaymentMax = parseFloat(maxValuesArray[i]);
	  
			if (orderTotal > currentPaymentMax || orderTotal < currentPaymentMin) {
				document.getElementById(currentPaymentId).disabled = true; 	
				if (document.getElementById(currentPaymentId).selected == true) {
					document.getElementById(currentPaymentId).selected = false;
				}
			} else {              
				document.getElementById(currentPaymentId).disabled = false;
			}
		}
	}

	// calculate points if available
	var pointsBalance = parseFloat(orderForm.points_balance_value.value);
	var pointsDecimals = 0;
	if (orderForm.points_decimals && orderForm.points_decimals.value != "") {
		pointsDecimals = parseFloat(orderForm.points_decimals.value);
		if (isNaN(pointsDecimals)) { pointsDecimals = 0; }
	}

	var orderTotalPoints = goodsPoints + totalPropertiesPoints + shippingTotalPoints;
	var totalPointsControl = document.getElementById("total_points_amount");
	if (totalPointsControl) {
		totalPointsControl.innerHTML = formatNumber(orderTotalPoints, pointsDecimals);
	}
	var remainingPointsControl = document.getElementById("remaining_points");
	if (remainingPointsControl) {
		remainingPointsControl.innerHTML = formatNumber(pointsBalance - orderTotalPoints, pointsDecimals);
	}

}

function getDiscountTaxes(taxRates, totalValues, discountAmount, taxFreeOption, returnType)
{
	var goodsTotal = totalValues["goods_total"];
	var taxAmount = 0;
	var taxesValues = new Array();
	if (taxFreeOption != 1) {
		if (taxRates instanceof Array) {
			for (taxId in taxRates) {
				if(!(taxRates[taxId] instanceof Function) && !(taxRates[taxId]["goods"] instanceof Function)){
					var goodsTax = taxRates[taxId]["goods"];
					var discountTax = Math.round((discountAmount * goodsTax * 100) / goodsTotal) / 100;
					taxesValues[taxId] = new Array();
					taxesValues[taxId]["tax_amount"] = discountTax;
					taxesValues[taxId]["price_amount"] = discountAmount;
					taxAmount += discountTax;
				}
			}
		}
	}

	if (returnType == 2) {
		return taxesValues;
	} else {
		return taxAmount;
	}
}

function getTaxAmount(taxRates, itemType, amount, quantity, itemTaxId, taxFreeOption, returnType) 
{
	var taxRound = 1;
	if (document.order_info.tax_round) {
		taxRound = parseInt(document.order_info.tax_round.value);
		if (isNaN(taxRound)) { taxRound = 1; }
	}

	var taxesValues = new Array();
	var pricesType = parseFloat(document.order_info.tax_prices_type.value);
	if (isNaN(pricesType)) { pricesType = 0; }

	// calculate summary tax
	var taxAmount = 0; var taxPercent = 0; var fixedTax = 0;
	if (taxFreeOption != 1) {
		// calculate summary tax
		if (taxRates instanceof Array) {
			for (taxId in taxRates) {
				if(!(taxRates[taxId] instanceof Function)){
					var taxRate = taxRates[taxId];
					var taxType = taxRate["tax_type"];

					// check if the tax coould be applied for current item
					if (taxType == 1 || (taxType == 2 && itemTaxId == taxId)) {

						var currentTaxPercent = 0; var currentFixedTax = 0; var currentItemTax = 0;
						// check tax percent
						if (taxRate["types"] && taxRate["types"][itemType] && taxRate["types"][itemType]["tax_percent"]) {
							currentTaxPercent = parseFloat(taxRate["types"][itemType]["tax_percent"]);
						} else {
							currentTaxPercent = parseFloat(taxRate["tax_percent"]);
						}
						// check fixed tax amount 
						if (taxRate["types"] && taxRate["types"][itemType] && taxRate["types"][itemType]["fixed_amount"]) {
							currentFixedTax = parseFloat(taxRate["types"][itemType]["fixed_amount"]) * quantity;
						} else if (taxRate["fixed_amount"]) {
							currentFixedTax = parseFloat(taxRate["fixed_amount"]) * quantity;
						} else {
							currentFixedTax = 0;
						}
						// calculate tax amount for each tax
						if (pricesType == 1) { // prices includes tax
							currentItemTax = (Math.round(amount * 100) - Math.round(amount * 10000 / ( 100 + currentTaxPercent))) / 100 - currentFixedTax; 
						} else {
							currentItemTax = Math.round(amount * currentTaxPercent) / 100 + currentFixedTax;
						}
						if (taxRound == 1) {
							currentItemTax = Math.round(currentItemTax * 100) / 100;
						}

						taxesValues[taxId] = new Array();
						//taxesValues[taxId]["tax_name"] = "";
						//taxesValues[taxId]["show_type"] = "";
						taxesValues[taxId]["tax_percent"] = currentTaxPercent;
						taxesValues[taxId]["fixed_value"] = currentFixedTax;
						taxesValues[taxId]["tax_amount"] = currentItemTax;
						taxesValues[taxId]["price_amount"] = amount;

						taxPercent += currentTaxPercent;
						fixedTax += currentFixedTax;
						taxAmount += currentItemTax;

					} // end tax check
				}
			}
		}
	} else {
		taxPercent = 0;
	}

	if (returnType == 2) {
		return taxesValues;
	} else {
		return taxAmount;
	}
}

function addTaxValues(taxRates, taxValues, amountType)
{
	var taxRound = 1;
	if (document.order_info.tax_round) {
		taxRound = parseInt(document.order_info.tax_round.value);
		if (isNaN(taxRound)) { taxRound = 1; }
	}

	if (taxValues instanceof Array) {
		for (taxId in taxValues) {
			if(!(taxValues[taxId] instanceof Function)){
				var taxInfo = taxValues[taxId];
				var taxAmount = parseFloat(taxInfo["tax_amount"]);
				if (taxRound == 1) {
					taxAmount = Math.round(taxAmount * 100) / 100;
				}
				if (!taxRates[taxId][amountType]) {
					taxRates[taxId][amountType] = 0;
				}
				if (!taxRates[taxId]["tax_total"]) {
					taxRates[taxId]["tax_total"] = 0;
				}
				taxRates[taxId][amountType] += taxAmount;
				if (amountType == "discount") {
					taxRates[taxId]["tax_total"] -= taxAmount;
				} else {
					taxRates[taxId]["tax_total"] += taxAmount;
				}
			}
		}
	}
	return taxRates;
}


function calculateTotals(totalValues, totalAmount, taxRates, amountType)
{
	var pricesType = parseFloat(document.order_info.tax_prices_type.value);
	if (isNaN(pricesType)) { pricesType = 0; }

	totalValues[amountType+"_total"] = totalAmount;
	totalValues[amountType+"_excl_tax"] = 0;
	totalValues[amountType+"_tax"] = 0;
	totalValues[amountType+"_incl_tax"] = 0;
	for (taxId in taxRates) {
		if(!(taxRates[taxId][amountType] instanceof Function)){
			if (taxRates[taxId][amountType]) {
				totalValues[amountType+"_tax"] += taxRates[taxId][amountType];
			}
		}
	}
	if (pricesType == 1) {
		totalValues[amountType+"_excl_tax"] += (totalAmount - totalValues[amountType+"_tax"]);
		totalValues[amountType+"_incl_tax"] += totalAmount;
	} else {
		totalValues[amountType+"_excl_tax"] += totalAmount;
		totalValues[amountType+"_incl_tax"] += 1 * totalAmount + totalValues[amountType+"_tax"];
	}

	return totalValues;
}

function calculatePrices(amount, tax)
{
	var prices = new Array();
	var pricesType = parseFloat(document.order_info.tax_prices_type.value);
	if (isNaN(pricesType)) { pricesType = 0; }

	prices["base"] = amount;
	prices["tax"] = tax;
	if (pricesType == 1) {                           
		prices["excl_tax"] = (amount - tax);
		prices["incl_tax"] = amount;
	} else {
		prices["excl_tax"] = amount;
		prices["incl_tax"] = 1 * amount + tax;
	}

	return prices;
}


function totalTaxValue(taxValues)
{
	var taxRound = 1;
	if (document.order_info.tax_round) {
		taxRound = parseInt(document.order_info.tax_round.value);
		if (isNaN(taxRound)) { taxRound = 1; }
	}

	var totalTax = 0;
	if (taxValues instanceof Array) {
		for (taxId in taxValues) {
			if(!(taxValues[taxId] instanceof Function)){
				var taxInfo = taxValues[taxId];
				var taxAmount = parseFloat(taxInfo["tax_amount"]);
				if (taxRound == 1) {
					taxAmount = Math.round(taxAmount * 100) / 100;
				}
				totalTax += taxAmount;
			}
		}
	}
	return totalTax;
}


function getTaxAmountOld(amount, taxPercent, taxFree, pricesType) 
{
	var taxAmount = 0;
	if (taxFree != 1) {
		if (pricesType == 1) {
			taxAmount = (Math.round(amount * 100) - Math.round(amount * 10000 / ( 100 + taxPercent))) / 100; 
		} else {
			taxAmount = Math.round(amount * taxPercent) / 100;
		}
	}
	return taxAmount;
}

function currencyFormat(numberValue)
{
	var orderForm = document.order_info;
	var currencyLeft = orderForm.currency_left.value;
	var currencyRight = orderForm.currency_right.value;
	var currencyRate = orderForm.currency_rate.value;
	var currencyDecimals = orderForm.currency_decimals.value;
	var currencyPoint = orderForm.currency_point.value;
	var currencySeparator = orderForm.currency_separator.value;
	return currencyLeft + formatNumber(numberValue * currencyRate, currencyDecimals, currencyPoint, currencySeparator) + currencyRight;
}

function formatNumber(numberValue, decimals, decimalPoint, thousandsSeparator)
{
	if (decimals == undefined) {
		decimals = 0;
	}
	if (thousandsSeparator == undefined) {
		thousandsSeparator = ",";
	}

	var numberParts = "";
	var roundValue = 1;
	for (var d = 0; d < decimals; d++) {
		roundValue *= 10;
	}
	numberValue = Math.round(numberValue * roundValue) / roundValue;
	var numberSign = "";
	if (numberValue < 0) {
		numberSign = "-";
		numberValue = Math.abs(numberValue);
	} 

	var numberText = new String(numberValue);
	var numberParts = numberText.split(".");
	var beforeDecimal = numberParts[0];
	var afterDecimal = "";
	numberText = "";
	if (numberParts.length == 2) {
		afterDecimal = numberParts[1];
	}
	while (beforeDecimal.length > 0) {
		if (beforeDecimal.length > 3) {
			numberText = thousandsSeparator + beforeDecimal.substring(beforeDecimal.length - 3, beforeDecimal.length) + numberText;
			beforeDecimal = beforeDecimal.substring(0, beforeDecimal.length - 3);
		} else {
			numberText = beforeDecimal + numberText;
			beforeDecimal = "";
		}
	}
	if (decimals > 0) {
		while (afterDecimal.length < decimals) {
			afterDecimal += "0";
		}
		if (decimalPoint == undefined) {
			decimalPoint = ".";
		}
		numberText += decimalPoint + afterDecimal;
	}
	numberText = numberSign + numberText;

	return numberText;
}

function changeCountry(orderForm, controlType)
{
	var refreshPage = true;
	if (controlType == 'personal') {
		if (orderForm.delivery_country_id || orderForm.delivery_country_id) { refreshPage = false; }
	}
	if (refreshPage) {
		orderForm.operation.value = "refresh";
		orderForm.submit();
	}
}

function changeState(orderForm, controlType)
{
	var refreshPage = true;
	if (controlType == 'personal') {
		if (orderForm.delivery_state_id || orderForm.delivery_country_id || orderForm.delivery_state_id || orderForm.delivery_country_id) {
			refreshPage = false;
		} else if (orderForm.country_id) {
			if (orderForm.country_id.selectedIndex == 0) { refreshPage = false; }
		} else if (orderForm.country_id) {
			if (orderForm.country_id.selectedIndex == 0) { refreshPage = false; }
		}
	} else if (orderForm.delivery_country_id) {
		if (orderForm.delivery_country_id.selectedIndex == 0) { refreshPage = false; }
	} else if (orderForm.delivery_country_id) {
		if (orderForm.delivery_country_id.selectedIndex == 0) { refreshPage = false; }
	}
	if (refreshPage) {
		orderForm.operation.value = "refresh";
		orderForm.submit();
	}
}

function changeZip(orderForm, controlType)
{
	var refreshPage = true;
	if (controlType == 'personal') {
		if (orderForm.delivery_zip || orderForm.delivery_country_id || orderForm.delivery_country_id) {
			refreshPage = false;
		} else if (orderForm.country_id) {
			if (orderForm.country_id.selectedIndex == 0) { refreshPage = false; }
		} else if (orderForm.country_id) {
			if (orderForm.country_id.selectedIndex == 0) { refreshPage = false; }
		}
		
	} else if (orderForm.delivery_country_id) {
		if (orderForm.delivery_country_id.selectedIndex == 0) { refreshPage = false; }
	} else if (orderForm.delivery_country_id) {
		if (orderForm.delivery_country_id.selectedIndex == 0) { refreshPage = false; }
	}
	if (refreshPage) {
		orderForm.operation.value = "refresh";
		orderForm.submit();
	}
}

function checkSame()
{
	var refreshPage = false;
	var orderForm = document.order_info;
	var sameChecked = document.order_info.same_as_personal.checked;
	if (sameChecked) {
		var fieldName = "";
		var fields = new Array("name", "first_name", "last_name", "company_id", "company_name", "email",
			"address1", "address2", "city", "province", "address1",
			"phone", "daytime_phone", "evening_phone", "cell_phone", "fax",
			"phone_code", "daytime_phone_code", "evening_phone_code", "cell_phone_code", "fax_code");
		for (var i = 0; i < fields.length; i++) {
			fieldName = fields[i];
			if (orderForm.elements[fieldName] && orderForm.elements["delivery_" + fieldName]) {
				orderForm.elements["delivery_" + fieldName].value = orderForm.elements[fieldName].value;
			}
		}
		if (orderForm.country_id && orderForm.delivery_country_id) {
			if (orderForm.country_id.selectedIndex != orderForm.delivery_country_id.selectedIndex) {
				orderForm.delivery_country_id.selectedIndex = orderForm.country_id.selectedIndex;
				refreshPage = true;
			}
		}
		if (orderForm.country_id && orderForm.delivery_country_id) {
			if (orderForm.country_id.selectedIndex != orderForm.delivery_country_id.selectedIndex) {
				orderForm.delivery_country_id.selectedIndex = orderForm.country_id.selectedIndex;
				refreshPage = true;
			}
		}
		if (orderForm.state_id && orderForm.delivery_state_id) {
			if (orderForm.state_id.selectedIndex != orderForm.delivery_state_id.selectedIndex) {
				orderForm.delivery_state_id.selectedIndex = orderForm.state_id.selectedIndex;
				refreshPage = true;
			}
		}
		if (orderForm.state_id && orderForm.delivery_state_id) {
			if (orderForm.state_id.selectedIndex != orderForm.delivery_state_id.selectedIndex) {
				orderForm.delivery_state_id.selectedIndex = orderForm.state_id.selectedIndex;
				refreshPage = true;
			}
		}
		if (orderForm.zip && orderForm.delivery_zip) {
			if (orderForm.zip.value != orderForm.delivery_zip.value) {
				orderForm.delivery_zip.value = orderForm.zip.value;
				refreshPage = true;
			}
		}
	}
	if (refreshPage) {
		orderForm.operation.value = "refresh";
		orderForm.submit();
	}
}

function uncheckSame()
{
	if (document.order_info.same_as_personal) {
		document.order_info.same_as_personal.checked = false;
	}
}

function checkMaxLength(obj, maxLength)
{
  return (obj.value.length < maxLength);
}

function checkBoxesMaxLength(e, itemForm, cpID, maxLength)
{
	var key;
	if (window.event) {
		key = window.event.keyCode; //IE
	} else {
		key = e.which; //Firefox
	}

	if (key == 8 || key == 9 || key == 16 || key == 17 || key == 35 || key == 36 || key == 37 || key == 39 || key == 46 || key == 116) {
		return true;
	}

	var totalOptions = parseInt(itemForm.elements["property_total_" + cpID].value);
	var totalLength = 0;
	for ( var ci = 1; ci <= totalOptions; ci++) {
		if (itemForm.elements["property_" + cpID + "_" + ci].value != "") {
			var valueText = itemForm.elements["property_" + cpID + "_" + ci].value;
			totalLength += valueText.length;
		}
	}
  return (totalLength < maxLength);
}

function prepareData(dataName, dataDelimiter)
{
	var data = new Array();
	var dataValue = document.order_info.elements[dataName].value;
	if (dataValue != "") {
		var records = dataValue.split(dataDelimiter);
		for (var t = 0; t < records.length; t++) {
			var record = records[t];
			var ampPos = record.indexOf("&");
			if (ampPos != -1) {
				var dataId = record.substring(0, ampPos);
				var recordValue = record.substring(ampPos+1, record.length);
				data[dataId] = new Array();
				// get record parameters
				var paramsPairs = recordValue.split("&");
				for (var p = 0; p < paramsPairs.length; p++) {
					var paramPair = paramsPairs[p];
					var equalPos = paramPair.indexOf("=");
					if (equalPos != -1) {
						var paramName = paramPair.substring(0, equalPos);
						var paramValue = paramPair.substring(equalPos + 1, paramPair.length);
						if (dataName == "tax_rates" && paramName.substring(0, 10) == "item_type_") { // special condition for taxes
							var itemTaxStr = paramName.substring(10, paramName.length);
							var undPos = itemTaxStr.indexOf("_");
							var itemTaxType = itemTaxStr.substring(0, undPos);
							var itemTaxCode = itemTaxStr.substring(undPos + 1, itemTaxStr.length);
							if(!data[dataId]["types"]) {
								data[dataId]["types"] = new Array();
							}
							if(!data[dataId]["types"][itemTaxCode]) {
								data[dataId]["types"][itemTaxCode] = new Array();
							}
							if (itemTaxType == "percent") {
								data[dataId]["types"][itemTaxCode]["tax_percent"] = decodeParamValue(paramValue);
							} else if (itemTaxType == "fixed") {
								data[dataId]["types"][itemTaxCode]["fixed_amount"] = decodeParamValue(paramValue);
							}
						} else {
							data[dataId][paramName] = decodeParamValue(paramValue);
						}
					}
				} // end of record parameters cycle
			}
		}
	}
	return data;
}

function decodeParamValue(paramValue)
{
	paramValue = paramValue.replace(/%0D/g, "\r");
	paramValue = paramValue.replace(/%0A/g, "\n");
	paramValue = paramValue.replace(/%27/g, "'");
	paramValue = paramValue.replace(/%22/g, "\"");
	paramValue = paramValue.replace(/%26/g, "&");
	paramValue = paramValue.replace(/%2B/g, "+");
	paramValue = paramValue.replace(/%25/g, "%");
	paramValue = paramValue.replace(/%3D/g, "=");
	paramValue = paramValue.replace(/%7C/g, "|");
	paramValue = paramValue.replace(/%23/g, "#");
	return paramValue;
}


function addressWindow(windowUrl)
{
	var addressWindow = window.open (windowUrl, 'addressWindow', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=600,height=500');
	addressWindow.focus();
}

function ccUsersWindow(windowUrl)
{
	var usersWindow = window.open (windowUrl, 'usersWindow', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=800,height=600');
	usersWindow.focus();
}

function ccSetUser(ui)
{
	var orderForm = document.order_info;
	var personalNumber = orderForm.personal_number.value;
	var deliveryNumber = orderForm.delivery_number.value;

	orderForm.cc_user_id.value = ui["user_id"];
	var loginObj = document.getElementById("cc_user_login");
	loginObj.innerHTML = ui["login"];

	var ccRemoveObj = document.getElementById("cc_remove_user");
	ccRemoveObj.style.display = "inline";

	
	if (personalNumber) {
		setAddress(1, ui["name"], ui["first_name"], ui["last_name"], ui["company_id"], ui["company_name"], 
			ui["email"], ui["address1"], ui["address2"], ui["city"], ui["province"], 
			ui["state_id"], ui["country_id"], ui["zip"], 
			ui["phone"], ui["daytime_phone"], ui["evening_phone"], ui["cell_phone"], ui["fax"]);
	}
	if (deliveryNumber) {
		setAddress(2, ui["delivery_name"], ui["delivery_first_name"], ui["delivery_last_name"], ui["delivery_company_id"], ui["delivery_company_name"], 
			ui["delivery_email"], ui["delivery_address1"], ui["delivery_address2"], ui["delivery_city"], ui["delivery_province"], 
			ui["delivery_state_id"], ui["delivery_country_id"], ui["delivery_zip"], 
			ui["delivery_phone"], ui["delivery_daytime_phone"], ui["delivery_evening_phone"], ui["delivery_cell_phone"], ui["delivery_fax"]);
	}
}

function ccRemoveUser()
{
	var orderForm = document.order_info;
	var personalNumber = orderForm.personal_number.value;
	var deliveryNumber = orderForm.delivery_number.value;

	orderForm.cc_user_id.value = "";
	var loginObj = document.getElementById("cc_user_login");
	loginObj.innerHTML = "";

	var ccRemoveObj = document.getElementById("cc_remove_user");
	ccRemoveObj.style.display = "none";
}

function setAddress(addressType, name, firstName, lastName, companyId, companyName, email, address1, address2, city, province, stateId, countryId, postalCode, phone, daytimePhone, eveningPhone, cellPhone, fax)
{
	var orderForm = document.order_info;
	var prefix = "";	
	if (addressType == 2) {
		prefix = "delivery_";
	}
	if (orderForm.elements[prefix+"name"]) { orderForm.elements[prefix+"name"].value = name; }
	if (orderForm.elements[prefix+"first_name"]) { orderForm.elements[prefix+"first_name"].value = firstName; }
	if (orderForm.elements[prefix+"last_name"]) { orderForm.elements[prefix+"last_name"].value = lastName; }
	if (orderForm.elements[prefix+"company_id"]) { 
		var control = orderForm.elements[prefix+"company_id"];
		control.selectedIndex = 0;
		for (var i = 0; i < control.options.length; i++) {
			if (control.options[i].value == companyId) {
				control.options[i].selected = true;
			}
		}
	}
	if (orderForm.elements[prefix+"company_name"]) { orderForm.elements[prefix+"company_name"].value = companyName; }
	if (orderForm.elements[prefix+"email"]) { orderForm.elements[prefix+"email"].value = email; }
	if (orderForm.elements[prefix+"address1"]) { orderForm.elements[prefix+"address1"].value = address1; }
	if (orderForm.elements[prefix+"address2"]) { orderForm.elements[prefix+"address2"].value = address2; }
	if (orderForm.elements[prefix+"city"]) { orderForm.elements[prefix+"city"].value = city; }
	if (orderForm.elements[prefix+"province"]) { orderForm.elements[prefix+"province"].value = province; }
	if (orderForm.elements[prefix+"country_id"]) { 
		var control = orderForm.elements[prefix+"country_id"];
		control.selectedIndex = 0;
		for (var i = 0; i < control.options.length; i++) {
			if (control.options[i].value == countryId) {
				control.options[i].selected = true;
			}
		}
	}
	// update states list first
	var pbId = orderForm.pb_id.value;
	if (addressType == 1) {
		updateStates(pbId, "personal");
	} else {
		updateStates(pbId, "delivery");
	}
	if (orderForm.elements[prefix+"state_id"]) { 
		var control = orderForm.elements[prefix+"state_id"];
		control.selectedIndex = 0;
		for (var i = 0; i < control.options.length; i++) {
			if (control.options[i].value == stateId) {
				control.options[i].selected = true;
			}
		}
	}
	if (orderForm.elements[prefix+"zip"]) { orderForm.elements[prefix+"zip"].value = postalCode; }
	if (orderForm.elements[prefix+"postal_code"]) { orderForm.elements[prefix+"postal_code"].value = postalCode; }
	if (orderForm.elements[prefix+"phone"]) { orderForm.elements[prefix+"phone"].value = phone; }
	if (orderForm.elements[prefix+"daytime_phone"]) { orderForm.elements[prefix+"daytime_phone"].value = daytimePhone; }
	if (orderForm.elements[prefix+"evening_phone"]) { orderForm.elements[prefix+"evening_phone"].value = eveningPhone; }
	if (orderForm.elements[prefix+"cell_phone"]) { orderForm.elements[prefix+"cell_phone"].value = cellPhone; }
	if (orderForm.elements[prefix+"fax"]) { orderForm.elements[prefix+"fax"].value = fax; }
	// update form after address change
	orderFormUpdate(addressType);
}

function orderFormUpdate(addressType)
{
	var orderForm = document.order_info;
	var personalNumber = orderForm.personal_number.value;
	var deliveryNumber = orderForm.delivery_number.value;
	var isCity = false; 
	var isState = false;
	var isZip = false;
	var isCountry = false;

	if (deliveryNumber) {
		if (addressType == "delivery" || addressType == "2") {
			if (!orderForm.delivery_city || orderForm.delivery_city.value != "") { isCity = true; }
			if (!orderForm.delivery_state_id || orderForm.delivery_state_id.length <= 1 || orderForm.delivery_state_id.value != "") { isState = true; }
			if (!orderForm.delivery_zip || orderForm.delivery_zip.value != "") { isZip = true; }
			if (!orderForm.delivery_country_id || orderForm.delivery_country_id.value != "") { isCountry = true; }
		}
	} else {
		if (!orderForm.city || orderForm.city.value != "") { isCity = true; }
		if (!orderForm.state_id || orderForm.state_id.length <= 1 || orderForm.state_id.value != "") { isState = true; }
		if (!orderForm.zip || orderForm.zip.value != "") { isZip = true; }
		if (!orderForm.country_id || orderForm.country_id.value != "") { isCountry = true; }
	}

	// update order form only if delivery address was selected
	if (/*isCity &&*/ isState /*&& isZip*/ && isCountry) {	
		var pbId = orderForm.pb_id.value;
		orderForm.operation.value = "refresh";
		orderForm.submit();
	}
}

function updateStates(pbId, controlType)
{
	var peronsalCountryObj = document.getElementById("country_id_" + pbId);
	var deliveryCountryObj = document.getElementById("delivery_country_id_" + pbId);
	var personalStateObj = document.getElementById("state_id_" + pbId);
	var deliveryStateObj = document.getElementById("delivery_state_id_" + pbId);
	var personalStateComObj = document.getElementById("state_id_comments_" + pbId);
	var deliveryStateComObj = document.getElementById("delivery_state_id_comments_" + pbId);
	var personalStateReqObj = document.getElementById("state_id_required_" + pbId);
	var deliveryStateReqObj = document.getElementById("delivery_state_id_required_" + pbId);

	var countryId = ""; var stateObj = ""; var stateCommentsObj = ""; var stateRequiredObj = "";
	if (controlType == "personal") {
		if (peronsalCountryObj) {
			countryId = peronsalCountryObj.options[peronsalCountryObj.selectedIndex].value;
		}
		if (personalStateObj) { stateObj = personalStateObj; }
		if (personalStateComObj) { stateCommentsObj = personalStateComObj; }
		if (personalStateReqObj) { stateRequiredObj = personalStateReqObj; }
	} else {
		if (deliveryCountryObj) {
			countryId = deliveryCountryObj.options[deliveryCountryObj.selectedIndex].value;
		}
		if (deliveryStateObj) { stateObj = deliveryStateObj; }
		if (deliveryStateComObj) { stateCommentsObj = deliveryStateComObj; }
		if (deliveryStateReqObj) { stateRequiredObj = deliveryStateReqObj; }
	}
	if (stateObj) {
		// remove old list
		var totalOptions = stateObj.options.length;
		for (var i = totalOptions - 1; i >= 1; i--) {
			stateObj.options[i] = null;
		}
		// check and add new states list
		if (countryId == "") {
			stateObj.style.display = "none";
			if (stateRequiredObj) { stateRequiredObj.style.display = "none"; }
			if (stateCommentsObj) {
				stateCommentsObj.innerHTML = selectCountryFirst;
				stateCommentsObj.style.display = "inline";
			}
		} else if (states[countryId]) {
			for(stateId in states[countryId]){
				//var key  = val;
				var stateName = states[countryId][stateId];				
				stateObj.options[stateObj.length] = new Option(stateName, stateId);
			}
			stateObj.style.display = "inline";
			if (stateRequiredObj) { stateRequiredObj.style.display = "inline"; }
			if (stateCommentsObj) {
				stateCommentsObj.innerHTML = "";
				stateCommentsObj.style.display = "none";
			}
		} else {
			stateObj.style.display = "none";
			if (stateRequiredObj) { stateRequiredObj.style.display = "none"; }
			if (stateCommentsObj) {
				stateCommentsObj.innerHTML = noStatesForCountry;
				stateCommentsObj.style.display = "inline";
			}
		}
	}
}
