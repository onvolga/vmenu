<?php

$html_template = get_setting_value($block, "html_template", "vi_header_logo.html");
$t->set_file("block_body", $html_template);
$logo_image = get_translation(get_setting_value($settings, "logo_image", "images/tr.gif"));
$logo_image_alt = get_translation(get_setting_value($settings, "logo_image_alt", HOME_PAGE_TITLE));
$logo_width = get_setting_value($settings, "logo_image_width", "");
$logo_height = get_setting_value($settings, "logo_image_height", "");
$path = get_setting_value($settings, "site_url", "");
$logo_image_alt = parseSiteTags($logo_image_alt);
$t->set_var("logo_image", $logo_image);
$t->set_var("logo_image_alt", $logo_image_alt);
$t->set_var("logo_width", $logo_width );
$t->set_var("logo_height", $logo_height);
$t->set_var("path", $path);

$block_parsed = true;
$t->parse("block_body", false);

?>