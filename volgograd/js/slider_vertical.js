var startDelay = 0; //Specify initial delay before slider starts to scroll on page (1000=1 seconds)
var sliderSpeed = 1; //Specify slider scroll speed (larger is faster 1-10)
var pauseit = 1; //Pause slider onMousever (0=no, 1=yes)
var speed = 20;
var copySpeed = sliderSpeed;
var pauseSpeed = (pauseit==0)? copySpeed: 0;

function verticalSlider(){
	sliderSecond = document.getElementById("sliderContent2");
	if (parseInt(slider.style.top) > (sliderHeight*(-1)+8)) 	//if slider hasn't reached the end of its height
		slider.style.top = parseInt(slider.style.top) - copySpeed+"px" 	//move slider up
	else
		slider.style.top = parseInt(sliderSecond.style.top) + sliderHeight+"px"	 //else, reset to original position
	
	if (parseInt(sliderSecond.style.top) > (sliderHeight*(-1)+8))
		sliderSecond.style.top = parseInt(sliderSecond.style.top) - copySpeed+"px"
	else
		sliderSecond.style.top = parseInt(slider.style.top) + sliderHeight+"px"
}

function pauseSlider()	{
	if(sliderContainer.attachEvent)	{
	   sliderContainer.attachEvent('onmouseover', function startSlider (){copySpeed = pauseSpeed;});
	   sliderContainer.attachEvent('onmouseout', function stopSlider (){copySpeed = sliderSpeed;});
	} 
	else	{
	   sliderContainer.onmouseover = function startSlider (){copySpeed = pauseSpeed;}; 
	   sliderContainer.onmouseout = function startSlider (){copySpeed = sliderSpeed;};
	}
}

function initSlider()	{
	slider = document.getElementById("sliderContent");	//Set ID for main DIV
	slider.style.top = 0;
	slider.className = "sliderClassVertical";	//Specify CLASS for main DIV tag
	sliderHeight = slider.offsetHeight;			//Specify main DIV height
	sliderContainer = document.getElementById("sliderBox");
	
	sliderSecond=document.createElement("div")	//Create new element DIV
	sliderSecond.setAttribute("id", "sliderContent2")	//Set ID for new DIV
	sliderSecond.className = "sliderClassVertical";	//Specify the same CLASS for new DIV tag
	sliderSecond.innerHTML = slider.innerHTML;			//Copy content of main DIV
	sliderContainer.insertBefore(sliderSecond, slider.nextSibling)	//Insert new UL after main DIV
	sliderSecond.style.top = sliderHeight + "px";
	
	sliderBoxHeight = sliderContainer.offsetHeight;	
	pauseSlider();		
	setTimeout('sliderTime=setInterval("verticalSlider()",speed)', startDelay);
}

window.onload = initSlider;