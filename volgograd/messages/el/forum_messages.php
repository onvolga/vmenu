<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "РлзспцпсЯет иЭмбфпт");
	define("TOPIC_MESSAGE_TITLE", "МЮнхмб");

	define("MY_FORUM_TOPICS_MSG", "Фб дйкЬ мпх иЭмбфб");
	define("ALL_FORUM_TOPICS_MSG", "јлб фб иЭмбфб");
	define("MY_FORUM_TOPICS_DESC", "ёчефе бнбсщфзиеЯ еЬн фп рсьвлзмб рпх Эчефе еЯнбй Энб рпх кЬрпйпт Ьллпт Эчей ресЬуей; Иб ерйихмпэубфе нб мпйсбуфеЯфе фзн емрейсЯб убт ме фпхт нЭпхт чсЮуфет; ГйбфЯ ден гЯнеуфе чсЮуфзт цьспхм кбй ден гЯнеуфе Энб мЭспт фзт кпйньфзфбт;");
	define("NEW_TOPIC_MSG", "НЭп иЭмб");
	define("NO_TOPICS_MSG", "Ден всЭизкбн иЭмбфб");
	define("FOUND_TOPICS_MSG", "всЭизкбн <b>{found_records}</b> иЭмбфб ме бхфпэт фпхт ьспхт '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "иЭмб");
	define("FORUM_REPLIES_COLUMN", "БрбнфЮуейт");
	define("FORUM_LAST_POST_COLUMN", "ФелехфбЯб бнбнЭщуз");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "иЭмб");
	define("TOPIC_AUTHOR_COLUMN", "УхггсбцЭбт");
	define("TOPIC_VIEWS_COLUMN", "Фп еЯдбн");
	define("TOPIC_REPLIES_COLUMN", "БрбнфЮуейт");
	define("TOPIC_UPDATED_COLUMN", "ФелехфбЯб бнбнЭщуз");
	define("TOPIC_ADDED_MSG", "ехчбсйуфпэме фп Ьсисп убт ёчей рспуфеиеЯ");

	define("TOPIC_ADDED_BY_FIELD", "РспуфЭизке брь");
	define("TOPIC_ADDED_DATE_FIELD", "РспуфЭизке брь");
	define("TOPIC_UPDATED_FIELD", "ФелехфбЯб бнбнЭщуз");
	define("TOPIC_NICKNAME_FIELD", "Шехдюнхмп");
	define("TOPIC_EMAIL_FIELD", "Фп e-mail убт");
	define("TOPIC_NAME_FIELD", "иЭмб");
	define("TOPIC_MESSAGE_FIELD", "МЮнхмб");
	define("TOPIC_NOTIFY_FIELD", "УфеЯле ьлет фйт брбнфЮуейт уфп злекфспнйкь фбчхдспмеЯп мпх");

	define("ADD_TOPIC_BUTTON", "Рсьуиеуе иЭмб ");
	define("TOPIC_MESSAGE_BUTTON", "Рсьуиеуе МЮнхмб");

	define("TOPIC_MISS_ID_ERROR", "ЛеЯрей фп <b>ID</b>");
	define("TOPIC_WRONG_ID_ERROR", "Фп ID ёчей <b>ЛЬипт</b> рбсЬмефсп");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>