<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "ГћjГіnustumiГ°stГ¶Г°");
	define("SUPPORT_REQUEST_INF_TITLE", "Г“ska eftir upplГЅsingum");
	define("SUPPORT_REPLY_FORM_TITLE", "Svara");

	define("MY_SUPPORT_ISSUES_MSG", "ГћjГіnustubeiГ°nir mГ­nar");
	define("MY_SUPPORT_ISSUES_DESC", "Ef ГѕГє lendir Г­ einhverjum vandrГ¦Г°um meГ° vГ¶runa sem ГѕГє keyptir, er ГѕjГіnustuteymiГ° okkar sannarlega til ГѕjГіnustu reiГ°ubГєiГ°. ГћГє getur sent inn ГѕjГіnustubeiГ°ni meГ° ГѕvГ­ aГ° smella ГЎ slГіГ°ina hГ©r aГ° ofan.");
	define("NEW_SUPPORT_REQUEST_MSG", "NГЅ beiГ°ni");
	define("SUPPORT_REQUEST_ADDED_MSG", "Takk fyrir <br>. ГћjГіnustuteymiГ° okkar mun sinna beiГ°ni Гѕinni eins skjГіtt og auГ°iГ° er");
	define("SUPPORT_SELECT_DEP_MSG", "Velja deild");
	define("SUPPORT_SELECT_PROD_MSG", "Velja vГ¶ru");
	define("SUPPORT_SELECT_STATUS_MSG", "Velja stГ¶Г°u");
	define("SUPPORT_NOT_VIEWED_MSG", "Ekki skoГ°aГ°");
	define("SUPPORT_VIEWED_BY_USER_MSG", "SkoГ°aГ° af notanda");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "SkoГ°aГ° af stjГіrnanda");
	define("SUPPORT_STATUS_NEW_MSG", "NГЅ");
	define("NO_SUPPORT_REQUEST_MSG", "Engin mГЎl fundust");

	define("SUPPORT_SUMMARY_COLUMN", "ГљtdrГЎttur");
	define("SUPPORT_TYPE_COLUMN", "GerГ°");
	define("SUPPORT_UPDATED_COLUMN", "SГ­Г°ast uppfГ¦rt");

	define("SUPPORT_USER_NAME_FIELD", "Nafn Гѕitt");
	define("SUPPORT_USER_EMAIL_FIELD", "Netfang Гѕitt");
	define("SUPPORT_IDENTIFIER_FIELD", "AuГ°kennir (reikningur #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Umhverfi (stГЅrikerfi, gagnagrunnur, netГѕjГіnn, o.s.frv.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Deild");
	define("SUPPORT_PRODUCT_FIELD", "Vara");
	define("SUPPORT_TYPE_FIELD", "GerГ°");
	define("SUPPORT_CURRENT_STATUS_FIELD", "NГєverandi staГ°a");
	define("SUPPORT_SUMMARY_FIELD", "Eins lГ­nu ГєtdrГЎttur");
	define("SUPPORT_DESCRIPTION_FIELD", "LГЅsing");
	define("SUPPORT_MESSAGE_FIELD", "SkilaboГ°");
	define("SUPPORT_ADDED_FIELD", "BГ¦tt viГ°");
	define("SUPPORT_ADDED_BY_FIELD", "BГ¦tt viГ° af");
	define("SUPPORT_UPDATED_FIELD", "SГ­Г°ast uppfГ¦rt");

	define("SUPPORT_REQUEST_BUTTON", "Senda inn beiГ°ni");
	define("SUPPORT_REPLY_BUTTON", "Svara");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Breytuna <b>Nr. ГѕjГіnustubeiГ°nar</b> vantar");
	define("SUPPORT_MISS_CODE_ERROR", "Breytuna <b>StaГ°festing</b> vantar");
	define("SUPPORT_WRONG_ID_ERROR", "Breytan <b>Nr. ГѕjГіnustubeiГ°nar</b> hefur rangt gildi");
	define("SUPPORT_WRONG_CODE_ERROR", "Breytan <b>StaГ°festing</b> hefur rangt gildi");

	define("MAIL_DATA_MSG", "PГіstgГ¶gn");
	define("HEADERS_MSG", "Fyrirsagnir");
	define("ORIGINAL_TEXT_MSG", "Upprunalegur texti");
	define("ORIGINAL_HTML_MSG", "Upprunalegt HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° loka miГ°um.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° svara miГ°um.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° ГєtbГєa nГЅja miГ°a.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° fjarlГ¦gja miГ°a.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° uppfГ¦ra miГ°a.<br>");
	define("NO_TICKETS_FOUND_MSG", "Engir miГ°ar fundust");
	define("HIDDEN_TICKETS_MSG", "Faldir miГ°ar");
	define("ALL_TICKETS_MSG", "Allir miГ°ar");
	define("ACTIVE_TICKETS_MSG", "Virkir miГ°ar");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "ГћГє ert ekki skrГЎГ°ur Г­ Гѕessari deild");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "ГћГє er ekki skrГЎГ°ur Г­ neina deild");
	define("REPLY_TO_NAME_MSG", "Svara {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "FrГіГ°leiksflokkur");
	define("KNOWLEDGE_TITLE_MSG", "FrГіГ°leikstitill");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "StaГ°a frГіГ°leiksgreinar");
	define("SELECT_RESPONSIBLE_MSG", "Velja ГЎbyrgan");

?>