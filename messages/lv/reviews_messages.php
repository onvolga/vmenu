<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Izvзles");
	define("BAD_MSG", "Slikti");
	define("POOR_MSG", "Vвji");
	define("AVERAGE_MSG", "Vidзji");
	define("GOOD_MSG", "Labi");
	define("EXCELLENT_MSG", "Izcili");
	define("REVIEWS_MSG", "Atsauksmes");
	define("NO_REVIEWS_MSG", "Nav atsauksmes");
	define("WRITE_REVIEW_MSG", "Rakstоt atsauci");
	define("RATE_PRODUCT_MSG", "Novзrtзt рo produktu");
	define("RATE_ARTICLE_MSG", "Novзrtзt рo rakstu");
	define("NOT_RATED_PRODUCT_MSG", "Produktam nav vзrtзjuma");
	define("NOT_RATED_ARTICLE_MSG", "Rakstam nav vзrtзjuma");
	define("AVERAGE_RATING_MSG", "Vidзjais klienta vзrtзjums");
	define("BASED_ON_REVIEWS_MSG", "veidots no {total_votes} vзrtзjumiem");
	define("POSITIVE_REVIEW_MSG", "Pozоtоvвs klientu atsauksmes");
	define("NEGATIVE_REVIEW_MSG", "Negatоvвs klientu atsauksmes");
	define("SEE_ALL_REVIEWS_MSG", "Skatоties visas klientu atsauksmes...");
	define("ALL_REVIEWS_MSG", "Visas atsauksmes");
	define("ONLY_POSITIVE_MSG", "Tikai pozitоvвs");
	define("ONLY_NEGATIVE_MSG", "Tikai negatоvвs");
	define("POSITIVE_REVIEWS_MSG", "Pozitоvвs atsauksmes");
	define("NEGATIVE_REVIEWS_MSG", "Negatоvвs atsauksmes");
	define("SUBMIT_REVIEW_MSG", "Paldies<br>Jыsu komentвri ir saтemti. Pзc to apstiprinврanas tie tiks publicзti mыsu mвjas lapв.<br>Mзs paturam tiesоbas nepublicзt informвciju, kas neatbilst mыsu lietoрanas nosacоjumiem.");
	define("ALREADY_REVIEW_MSG", "Jыs jau esat ievietojis atsauci.");
	define("RECOMMEND_PRODUCT_MSG", "Vai Jыs bыtu gatavs ieteikt<br> рo produktu iegвdвties arо citiem?");
	define("RECOMMEND_ARTICLE_MSG", "Vai Jыs bыtu gatavs ieteikt<br> рo rakstu izlasоt arо citiem?");
	define("RATE_IT_MSG", "Novзrtз to");
	define("NAME_ALIAS_MSG", "Vвrds vai iesauka");
	define("SHOW_MSG", "Rвdоt");
	define("FOUND_MSG", "Atrasts");
	define("RATING_MSG", "Vidзjais vзrtзjums");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>