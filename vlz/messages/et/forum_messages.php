<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Foorum");
	define("TOPIC_INFO_TITLE", "Teema info");
	define("TOPIC_MESSAGE_TITLE", "Teade");

	define("MY_FORUM_TOPICS_MSG", "Minu foorumi teemad");
	define("ALL_FORUM_TOPICS_MSG", "KС…ik foorumi teemad");
	define("MY_FORUM_TOPICS_DESC", "Kas oled kunagi mС…elnud, et ka kellelgi teisel on olnud sama probleem, mis sind hetkel vaevab? Kas tahaksid jagada oma kogemusi uute kasutajatega? Miks mitte saada foorumi kasutajaks ja СЊhineda kommuuniga?");
	define("NEW_TOPIC_MSG", "Uus teema");
	define("NO_TOPICS_MSG", "Teemasid ei leitud");
	define("FOUND_TOPICS_MSG", "Leidsime <b>{found_records}</b> teemat, mis vastasid tingimus(t)ele '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Ei leitud СЊhtegi foorumit");

	define("FORUM_NAME_COLUMN", "Foorum");
	define("FORUM_TOPICS_COLUMN", "Teemad");
	define("FORUM_REPLIES_COLUMN", "Vastused");
	define("FORUM_LAST_POST_COLUMN", "Viimati uuendatud");
	define("FORUM_MODERATORS_MSG", "Moderaatorid");

	define("TOPIC_NAME_COLUMN", "Teema ");
	define("TOPIC_AUTHOR_COLUMN", "Autor");
	define("TOPIC_VIEWS_COLUMN", "Vaadatud");
	define("TOPIC_REPLIES_COLUMN", "Vastused");
	define("TOPIC_UPDATED_COLUMN", "Viimati uuendatud");
	define("TOPIC_ADDED_MSG", "AitРґh<br>Sinu teema on lisatud");

	define("TOPIC_ADDED_BY_FIELD", "Lisas");
	define("TOPIC_ADDED_DATE_FIELD", "Lisatud");
	define("TOPIC_UPDATED_FIELD", "Viimati uuendatud");
	define("TOPIC_NICKNAME_FIELD", "HСЊСЊdnimi");
	define("TOPIC_EMAIL_FIELD", "Teie e-postiaadress");
	define("TOPIC_NAME_FIELD", "Teema ");
	define("TOPIC_MESSAGE_FIELD", "Teade");
	define("TOPIC_NOTIFY_FIELD", "Saada kС…ik vastused minu e-posti");

	define("ADD_TOPIC_BUTTON", "Lisa teema");
	define("TOPIC_MESSAGE_BUTTON", "Lisa teade");

	define("TOPIC_MISS_ID_ERROR", "Puuduv <b>Thread ID</b> parameeter.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> parameetril on vale vРґРґrtus.");
	define("FORUM_SEARCH_MESSAGE", "Leidsime {search_count} teadet, mis vastavad tingimus(t)ele '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Eelvaade");
	define("TOPIC_SAVE_BUTTON", "Salvesta");

	define("LAST_POST_ON_SHORT_MSG", "On (1367): ");
	define("LAST_POST_IN_SHORT_MSG", "Teemas:");
	define("LAST_POST_BY_SHORT_MSG", "By (1369):");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Viimati muudetud:");

?>