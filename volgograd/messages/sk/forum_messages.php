<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Fуrum");
	define("TOPIC_INFO_TITLE", "Informбcia o tйme");
	define("TOPIC_MESSAGE_TITLE", "Sprбva");

	define("MY_FORUM_TOPICS_MSG", "Moje tйmy");
	define("ALL_FORUM_TOPICS_MSG", "Vљetky tйmy");
	define("MY_FORUM_TOPICS_DESC", "Stalo sa Vбm niekedy, ћe problйm, ktorэ mбte, uћ niekto pred Vami rieљil? Chceli by ste zdieѕaќ Vaљe skъsenosti s novэmi uћнvateѕmi? Staтte sa иlenom fуra a pridajte sa ku komunite.");
	define("NEW_TOPIC_MSG", "Novб tйma");
	define("NO_TOPICS_MSG", "Ћiadne tйmy neboli nбjdenй");
	define("FOUND_TOPICS_MSG", "Naљiel som <b>{found_records}</b> tйmy obsahujъce text '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "Fуrum");
	define("FORUM_TOPICS_COLUMN", "Tйma");
	define("FORUM_REPLIES_COLUMN", "Odpovedн");
	define("FORUM_LAST_POST_COLUMN", "Naposledy aktualizovanй");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "Tйma");
	define("TOPIC_AUTHOR_COLUMN", "Autor");
	define("TOPIC_VIEWS_COLUMN", "Zobrazenн");
	define("TOPIC_REPLIES_COLUMN", "Odpovedн");
	define("TOPIC_UPDATED_COLUMN", "Naposledy aktualizovanй");
	define("TOPIC_ADDED_MSG", "Пakujeme<br>Vaљa tйma bola pridanб");

	define("TOPIC_ADDED_BY_FIELD", "Pridal");
	define("TOPIC_ADDED_DATE_FIELD", "Pridanй");
	define("TOPIC_UPDATED_FIELD", "Naposledy aktualizovanй");
	define("TOPIC_NICKNAME_FIELD", "Prezэvka");
	define("TOPIC_EMAIL_FIELD", "Vaљa emailovб adresa");
	define("TOPIC_NAME_FIELD", "Tйma");
	define("TOPIC_MESSAGE_FIELD", "Sprбva");
	define("TOPIC_NOTIFY_FIELD", "Odoslaќ vљetky odpovede na mфj email");

	define("ADD_TOPIC_BUTTON", "Pridaќ tйmu");
	define("TOPIC_MESSAGE_BUTTON", "Pridaќ sprбvu");

	define("TOPIC_MISS_ID_ERROR", "Chэba <b>Thread ID</b> parameter.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> mб chybnъ hodnotu.");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>