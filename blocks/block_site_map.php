<?php

	$default_title = "{SITE_MAP_TITLE}";

	$site_map_tree = array();
	
	$sitemap_settings           = get_settings("site_map");
	$site_map_custom_pages      = get_setting_value($sitemap_settings, "site_map_custom_pages");
	$site_map_categories        = get_setting_value($sitemap_settings, "site_map_categories");
	$site_map_items             = get_setting_value($sitemap_settings, "site_map_items");
	$site_map_forum_categories  = get_setting_value($sitemap_settings, "site_map_forum_categories");
	$site_map_forums            = get_setting_value($sitemap_settings, "site_map_forums");		
	$site_map_ad_categories     = get_setting_value($sitemap_settings, "site_map_ad_categories");
	$site_map_ads               = get_setting_value($sitemap_settings, "site_map_ads");
	$site_map_manual_categories = get_setting_value($sitemap_settings, "site_map_manual_categories");
	$site_map_manual_articles   = get_setting_value($sitemap_settings, "site_map_manual_articles");
	$site_map_manuals           = get_setting_value($sitemap_settings, "site_map_manuals");
	$site_map_manufacturers     = get_setting_value($sitemap_settings, "site_map_manufacturers");
	$site_map_manufacturers = 1;
	
	$friendly_urls      = get_setting_value($settings, "friendly_urls");
	$friendly_extension = get_setting_value($settings, "friendly_extension");
	
	include_once("./messages/" . $language_code . "/manuals_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/articles_functions.php");
	include_once("./includes/forums_functions.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/manuals_functions.php");

	$type = get_param("type");
	$level = get_param("level");
	$parent_id = get_param("parent_id");
		
	$t->set_file("block_body", "block_site_map.html");
	$t->set_var("pb_id", $pb_id);
	
	if(!strlen($type)){
		if($site_map_custom_pages){
			$t->set_var("type", "custom_pages");

			$user_id      = get_session("session_user_id");
			$user_type_id = get_session("session_user_type_id");

			$sql  = " SELECT p.page_id, p.page_code, p.page_title, p.page_url, p.friendly_url FROM ";
			if (isset($site_id)) {
				$sql .= "(";
			}
			if (strlen($user_id)) {
				$sql .= "(";
			}
			$sql .= $table_prefix . "pages p ";
			if (isset($site_id)) {
				$sql .= " LEFT JOIN " . $table_prefix . "pages_sites s ON (s.page_id=p.page_id AND p.sites_all=0)) ";
			}
			if (strlen($user_id)) {
				$sql .= " LEFT JOIN " . $table_prefix . "pages_user_types ut ON (ut.page_id=p.page_id AND p.user_types_all=0)) ";
			}
			$sql .= " WHERE p.is_showing=1 AND p.is_site_map=1 ";
			if (isset($site_id)) {
				$sql .= " AND (p.sites_all=1 OR s.site_id=" . $db->tosql($site_id, INTEGER, true, false) . ") ";
			} else {
				$sql .= " AND p.sites_all=1";
			}
			if (strlen($user_id)) {
				$sql .= " AND (p.user_types_all=1 OR ut.user_type_id=". $db->tosql($user_type_id, INTEGER) . ") ";
			} else {
				$sql .= " AND p.user_types_all=1 ";
			}
			$sql .= " ORDER BY p.page_order, p.page_title ";			
			$db->query($sql);
			$i=0;
			$is_custom_pages = false;
			while ($db->next_record()) {
				$is_custom_pages = true;
				$i++;
				$item_id   = $db->f('page_id');
				$item_name = $db->f('page_title');
				if ($db->f("friendly_url") && $friendly_urls) {
					$item_url = $db->f("friendly_url") . $friendly_extension;
				} elseif ($db->f('page_url')) {
					$item_url = $db->f("page_url");
				} else {
					$item_url = "page.php?page=" . $db->f("page_code");
				}	
				$site_map_tree["custom_page_" . $item_id] = array(
					SITEMAP_TITLE_INDEX => $item_name,
					SITEMAP_URL_INDEX   => $item_url
				);
				$t->set_var("categori_id",  "");
				$t->set_var("sub_class", "ExpandLeaf");
				$t->set_var("item_url",  $settings["site_url"] . $item_url);
				$t->set_var("item_name", get_translation($item_name));
				if($i%3){
					$t->parse("item_element");
				}else{
					$i=0;
					$t->parse("item_element");
					$t->parse("row_element");
					$t->set_var("item_element", "");
				}
			}
			if($is_custom_pages){
				while($i%3){
					$i++;
					if($i%3){
						$t->set_var("item_url",  "");
						$t->set_var("item_name", "");
						$t->parse("item_element");
					}else{
						$t->set_var("item_url",  "");
						$t->set_var("item_name", "");
						$t->parse("item_element");
						$t->parse("row_element");
						$t->set_var("item_element", "");
					}
				}
				$t->parse("custom_element");
			}else{
				$t->set_var("custom_element", "");
			}

		}
		if($site_map_categories){
			$is_categories = false;
			$allowed_categories_ids = VA_Categories::find_all_ids("c.parent_category_id=" . $db->tosql(0, INTEGER), VIEW_CATEGORIES_ITEMS_PERM);
			if(sizeof($allowed_categories_ids)){
				$is_categories = true;
			}
			if($site_map_items){
				$sql_params = array();
				$sql_params["brackets"] = "(";		
				$sql_params["join"]     = " INNER JOIN " . $table_prefix . "items_categories ic ON i.item_id=ic.item_id ";		
				$sql_params["join"] .= ")";
				$sql_params["where"] = "ic.category_id = " . $db->tosql(0, INTEGER);
				$allowed_items_ids = VA_Products::find_all_ids($sql_params, VIEW_ITEMS_PERM);
				if(sizeof($allowed_items_ids)){
					$is_categories = true;
				}
			}
			if($is_categories){
				$t->set_var("tree_type", "products");
				$t->parse("tree_js");
				$t->set_var("type", "products");
				$t->set_var("sub_class", "ExpandClosed");
				$t->set_var("item_name", PRODUCTS_TITLE);
				$t->set_var("item_url", get_custom_friendly_url("products.php"));
				$t->set_var("categori_id", 0);
				$t->parse("item", false);
			}
		}

		$articles_total_records = array();
		$articles_top_categories =  VA_Articles_Categories::find_all("c.category_id", 
			array("c.category_name", "c.friendly_url"),
			array(
				"where" => " c.parent_category_id=0 ",
				"order" => " ORDER BY c.category_order, c.category_name"
			)
		);

		if ($articles_top_categories) {
			foreach ($articles_top_categories AS $article_top_category_id => $articles_top_category) {		
				$show_categories = get_setting_value($sitemap_settings, "site_map_articles_categories_" . $article_top_category_id);
				$show_items      = get_setting_value($sitemap_settings, "site_map_articles_" . $article_top_category_id);						
	
				if ($articles_top_category["c.friendly_url"] && $friendly_urls) {
					$category_url =  $articles_top_category["c.friendly_url"] . $friendly_extension;
				} else {
					$category_url = "articles.php?category_id=" . $article_top_category_id;
				}
				if($show_categories){
					$is_categories = false;
					$allowed_categories_ids = VA_Articles_Categories::find_all_ids(" c.category_path LIKE '%" . $db->tosql($article_top_category_id, INTEGER) . ",%' ", VIEW_CATEGORIES_ITEMS_PERM);
					if(sizeof($allowed_categories_ids)){
						$is_categories = true;
					}
					$sql_params = array();
					$sql_params["where"] = "ac.category_id = " . $db->tosql($article_top_category_id, INTEGER);
					$allowed_items_ids = VA_Articles::find_all_ids($sql_params, VIEW_ITEMS_PERM);
					if(sizeof($allowed_items_ids)){
						if($show_items){
							$is_categories = true;
						}
					}

					if($is_categories){
						$t->set_var("tree_type", "articles_" . $article_top_category_id);
						$t->parse("tree_js");
						$t->set_var("type", "articles_" . $article_top_category_id);
						$t->set_var("sub_class", "ExpandClosed");
						$t->set_var("item_name", get_translation($articles_top_category["c.category_name"]));
						$t->set_var("item_url", $settings["site_url"] . $category_url);
						$t->set_var("categori_id", $article_top_category_id);
						$t->parse("item");
					}
				}
			}
		}

		if($site_map_forum_categories){
			$is_categories = false;
			$found_categories = VA_Forum_Categories::find_all("c.category_id", 
				array("c.category_name", "c.friendly_url"),
				array("order" => " ORDER BY c.category_order, c.category_name")
			);
			if(sizeof($found_categories)){
				$is_categories = true;
			}
			if($is_categories){
				$t->set_var("tree_type", "forums");
				$t->parse("tree_js");
				$t->set_var("type", "forums");
				$t->set_var("sub_class", "ExpandClosed");
				$t->set_var("item_name", FORUM_TITLE);
				$t->set_var("item_url", get_custom_friendly_url("forums.php"));
				$t->set_var("categori_id", 0);
				$t->parse("item");
			}
		}

		if($site_map_ad_categories){
			$is_categories = false;
			$found_categories = VA_Ads_Categories::find_all("c.category_id", 
				array("c.category_name", "c.parent_category_id", "c.friendly_url"),
				array("order" => " ORDER BY c.category_order, c.category_name",
					"where" => "c.parent_category_id = ".$db->tosql(0, INTEGER)
				)
			);
			if(sizeof($found_categories)){
				$is_categories = true;
			}
			if($site_map_ads){
				$found_ads = VA_Ads::find_all("", 
					array("i.item_id", "i.item_title", "i.friendly_url"),
					array("order" => " ORDER BY i.item_order, i.item_title",
						"where" => " ac.category_id = " . $db->tosql(0, INTEGER),
					)
				);
				if(sizeof($found_ads)){
					$is_categories = true;
				}
			}

			if($is_categories){
				$t->set_var("tree_type", "ads");
				$t->parse("tree_js");
				$t->set_var("type", "ads");
				$t->set_var("sub_class", "ExpandClosed");
				$t->set_var("item_name", ADS_TITLE);
				$t->set_var("item_url", get_custom_friendly_url("ads.php"));
				$t->set_var("categori_id", 0);
				$t->parse("item");
			}
		}

		if($site_map_manual_categories){
			$is_categories = false;
			$found_categories = VA_Manuals_Categories::find_all("c.category_id", 
				array("c.category_name", "c.friendly_url"),
				array("order" => " ORDER BY c.category_order, c.category_name")
			);
			if(sizeof($found_categories)){
				$is_categories = true;
			}
			if($is_categories){
				$t->set_var("tree_type", "manuals");
				$t->parse("tree_js");
				$t->set_var("type", "manuals");
				$t->set_var("sub_class", "ExpandClosed");
				$t->set_var("item_name", MANUALS_TITLE);
				$t->set_var("item_url", get_custom_friendly_url("manuals.php"));
				$t->set_var("categori_id", 0);
				$t->parse("item");
			}
		}
		if($site_map_manufacturers){
			$is_categories = false;
			$sql  = " SELECT manufacturer_id, manufacturer_name, friendly_url ";
			$sql .=	" FROM " . $table_prefix . "manufacturers  ";
			$db->query($sql);
			if($db->next_record()){
				$is_categories = true;
			}
			if($is_categories){
				$t->set_var("tree_type", "manufacturers");
				$t->parse("tree_js");
				$t->set_var("type", "manufacturers");
				$t->set_var("sub_class", "ExpandClosed");
				$t->set_var("item_name", MANUFACTURERS_TITLE);
				$t->set_var("item_url", get_custom_friendly_url("site_map.php"));
				$t->set_var("categori_id", 0);
				$t->parse("item");
			}
		}

		$t->set_var("navigator_block", "");
		$t->parse("top_element");
// ------------------------------- END main -------------------------------
	}else{
		$type_array = explode("_", $type);
		$type = $type_array[0];
		$type_id = (isset($type_array[1]))? $type_array[1]: "";
		$parent_array = explode("_", $parent_id);
		$parent_id = $parent_array[0];
		$sub_parent_id = (isset($parent_array[1]))? $parent_array[1]: "";
		if($type == "products" && $site_map_categories){
			$allowed_categories_ids = VA_Categories::find_all_ids("c.parent_category_id=" . $db->tosql($parent_id, INTEGER), VIEW_CATEGORIES_ITEMS_PERM);
			foreach ($allowed_categories_ids AS  $category_id) {
				$the_expand = "ExpandLeaf";
				$allowed_sub_categories_ids = VA_Categories::find_all_ids("c.parent_category_id=" . $db->tosql($category_id, INTEGER), VIEW_CATEGORIES_ITEMS_PERM);
				if(sizeof($allowed_sub_categories_ids)){
					$the_expand = "ExpandClosed";
				}
				if($site_map_items){
					$sql_params = array();
					$sql_params["brackets"] = "(";		
					$sql_params["join"]     = " INNER JOIN " . $table_prefix . "items_categories ic ON i.item_id=ic.item_id ";		
					$sql_params["join"] .= ")";
					$sql_params["where"] = "ic.category_id = " . $db->tosql($category_id, INTEGER);
					$allowed_items_ids = VA_Products::find_all_ids($sql_params, VIEW_ITEMS_PERM);
					if(sizeof($allowed_items_ids)){
						$the_expand = "ExpandClosed";
					}
				}
				$sql  = " SELECT c.category_id, c.category_name, c.friendly_url, cc.parent_category_id ";
				$sql .= " FROM " . $table_prefix . "categories c";
				$sql .= " LEFT JOIN " . $table_prefix . "categories cc ON c.category_id = cc.parent_category_id ";
				$sql .= " WHERE c.category_id IN (" . $db->tosql($category_id, INTEGER) . ")";	
				$sql .= " GROUP BY c.category_id ";
				$sql .= " ORDER BY c.category_order, c.category_name ";
				$db->query($sql);
				if($db->next_record()){
					$category_name = $db->f("category_name");
					$friendly_url = $db->f("friendly_url");
					$parent_category_id = $db->f("parent_category_id");
					if ($friendly_url && $friendly_urls) {
						$item_url = $friendly_url . $friendly_extension;
					} else {
						$item_url = "products.php?category_id=" . $category_id;
					}

					$t->set_var("categori_id",  $category_id);
					$t->set_var("sub_class", $the_expand);
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($category_name));
					$t->parse("sub_element");
				}
			}
			if($site_map_items){
				$sql_params = array();
				$sql_params["brackets"] = "(";		
				$sql_params["join"]     = " INNER JOIN " . $table_prefix . "items_categories ic ON i.item_id=ic.item_id ";		
				$sql_params["join"] .= ")";
				$sql_params["where"] = "ic.category_id = " . $db->tosql($parent_id, INTEGER);
				$allowed_items_ids = VA_Products::find_all_ids($sql_params, VIEW_ITEMS_PERM);
				foreach ($allowed_items_ids AS  $item_id) {
					$sql  = " SELECT i.item_id, i.item_name, i.friendly_url ";
					$sql .= " FROM " . $table_prefix . "items i ";
					$sql .= " WHERE i.item_id = " . $db->tosql($item_id, INTEGER);
					$sql .= " ORDER BY i.item_order, i.item_name ";
					$db->query($sql);
					if($db->next_record()){
						$item_id = $db->f("item_id");
						$item_name = $db->f("item_name");
						$friendly_url = $db->f("friendly_url");
						if ($friendly_url && $friendly_urls) {
							$item_url = $friendly_url . $friendly_extension;
						} else {
							$item_url = "product_details.php?category_id=".$parent_id."&item_id=".$item_id;
						}
		
						$t->set_var("categori_id",  $item_id);
						$t->set_var("sub_class", "ExpandLeaf");
						$t->set_var("item_url",  $settings["site_url"] . $item_url);
						$t->set_var("item_name", get_translation($item_name));
						$t->parse("sub_element");
					}
				}
			}
		}
		if($type == "articles" && get_setting_value($sitemap_settings, "site_map_articles_categories_" . $type_id)){
			$allowed_categories_ids = VA_Articles_Categories::find_all_ids(" c.category_path LIKE '%" . $db->tosql($parent_id, INTEGER) . ",%' ", VIEW_CATEGORIES_ITEMS_PERM);
			foreach ($allowed_categories_ids AS  $category_id) {		
				$the_expand = "ExpandLeaf";
				$allowed_sub_categories_ids = VA_Articles_Categories::find_all_ids(" c.category_path LIKE '%" . $db->tosql($category_id, INTEGER) . ",%' ", VIEW_CATEGORIES_ITEMS_PERM);
				if(sizeof($allowed_sub_categories_ids)){
					$the_expand = "ExpandClosed";
				}
				if(get_setting_value($sitemap_settings, "site_map_articles_" . $type_id)){
					$sql_params = array();
					$sql_params["where"] = "ac.category_id = " . $db->tosql($category_id, INTEGER);
					$allowed_items_ids = VA_Articles::find_all_ids($sql_params, VIEW_ITEMS_PERM);
					if(sizeof($allowed_items_ids)){
						$the_expand = "ExpandClosed";
					}
				}
				$sql  = " SELECT c.category_id, c.category_name, c.friendly_url ";
				$sql .= " FROM " . $table_prefix . "articles_categories c";
				$sql .= " WHERE c.category_id = " . $db->tosql($category_id, INTEGER);	
				$sql .= " GROUP BY c.category_id ";
				$sql .= " ORDER BY c.category_order, c.category_name ";
				$db->query($sql);
				if($db->next_record()){
					//$category_id = $db->f("category_id");
					$category_name = $db->f("category_name");
					$friendly_url = $db->f("friendly_url");
					$parent_category_id = $db->f("parent_category_id");
					if ($friendly_url && $friendly_urls) {
						$item_url = $friendly_url . $friendly_extension;
					} else {
						$item_url = "articles.php?category_id=" . $category_id;
					}

					$t->set_var("categori_id",  $category_id);
					$t->set_var("sub_class", $the_expand);
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($category_name));
					$t->parse("sub_element");
				}
			}
			$show_items      = get_setting_value($sitemap_settings, "site_map_articles_" . $type_id);						
			if($show_items){
				$sql_params = array();
				$sql_params["where"] = "ac.category_id = " . $db->tosql($parent_id, INTEGER);
				$allowed_items_ids = VA_Articles::find_all_ids($sql_params, VIEW_ITEMS_PERM);
				foreach ($allowed_items_ids AS  $article_id) {
					$sql  = " SELECT a.article_id, a.article_title, a.friendly_url ";
					$sql .= " FROM " . $table_prefix . "articles a ";
					$sql .= " WHERE a.article_id = " . $db->tosql($article_id, INTEGER);
					$sql .= " ORDER BY a.article_order, a.article_title ";
					$db->query($sql);
					if($db->next_record()){
						$article_id = $db->f("article_id");
						$article_title = $db->f("article_title");
						$friendly_url = $db->f("friendly_url");
						if ($friendly_url && $friendly_urls) {
							$item_url = $friendly_url . $friendly_extension;
						} else {
							$item_url = "article.php?category_id=".$parent_id."&article_id=".$article_id;
						}
		
						$t->set_var("categori_id",  "");
						$t->set_var("sub_class", "ExpandLeaf");
						$t->set_var("item_url",  $settings["site_url"] . $item_url);
						$t->set_var("item_name", get_translation($article_title));
						$t->parse("sub_element");
					}
				}
			}
		}
		if($type == "forums" && $site_map_forum_categories){
			if(!$parent_id){
				$found_categories = VA_Forum_Categories::find_all("c.category_id", 
					array("c.category_name", "c.friendly_url"),
					array("order" => " ORDER BY c.category_order, c.category_name")
				);
				foreach ($found_categories AS  $category_id => $category) {
					$the_expand = "ExpandLeaf";
					if($site_map_forums){
						$found_forums = VA_Forums::find_all("", 
							array("fl.forum_id", "fl.forum_name", "fl.friendly_url", "fl.category_id"),
							array(
								"where" => " fl.category_id = " . $db->tosql($category_id, INTEGER),
								"order" => " ORDER BY fl.forum_order, fl.forum_name"
							)
						);
						if(sizeof($found_forums)){
							$the_expand = "ExpandClosed";
						}
					}
					if ($category['c.friendly_url'] && $friendly_urls) {
						$item_url = $category['c.friendly_url'] . $friendly_extension;
					} else {
						$item_url = "forums.php?category_id=" . $category_id;
					}

					$t->set_var("categori_id",  $category_id);
					$t->set_var("sub_class", $the_expand);
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($category['c.category_name']));
					$t->parse("sub_element");
				}
			}elseif($site_map_forums){
				$found_forums = VA_Forums::find_all("", 
					array("fl.forum_id", "fl.forum_name", "fl.friendly_url", "fl.category_id"),
					array(
						"where" => " fl.category_id = " . $db->tosql($parent_id, INTEGER),
						"order" => " ORDER BY fl.forum_order, fl.forum_name"
					)
				);
				foreach ($found_forums AS  $forum_id => $forum) {
					if ($forum['fl.friendly_url'] && $friendly_urls) {
						$item_url = $forum['fl.friendly_url'] . $friendly_extension;
					} else {
						$item_url = "forum.php?forum_id=" . $forum['fl.forum_id'];
					}

					$t->set_var("categori_id",  $forum['fl.forum_id']);
					$t->set_var("sub_class", "ExpandLeaf");
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($forum['fl.forum_name']));
					$t->parse("sub_element");
				}
			}
		}

		if($type == "ads" && $site_map_ad_categories){
			$found_categories = VA_Ads_Categories::find_all("c.category_id", 
				array("c.category_name", "c.parent_category_id", "c.friendly_url"),
				array("order" => " ORDER BY c.category_order, c.category_name",
					"where" => "c.parent_category_id = ".$db->tosql($parent_id, INTEGER)
				)
			);
			$category_url_prefix = "ads.php?category_id=";
			foreach ($found_categories AS  $category_id => $category) {
				$the_expand = "ExpandLeaf";
				$found_sub_categories = VA_Ads_Categories::find_all("c.category_id", 
					array("c.category_name", "c.parent_category_id", "c.friendly_url"),
					array("order" => " ORDER BY c.category_order, c.category_name",
						"where" => "c.parent_category_id = ".$db->tosql($category_id, INTEGER)
					)
				);
				if(sizeof($found_sub_categories)){
					$the_expand = "ExpandClosed";
				}
				if($site_map_ads){
					$found_ads = VA_Ads::find_all("", 
						array("i.item_id", "i.item_title", "i.friendly_url"),
						array("order" => " ORDER BY i.item_order, i.item_title",
							"where" => " ac.category_id = " . $db->tosql($category_id, INTEGER),
						)
					);
					if(sizeof($found_ads)){
						$the_expand = "ExpandClosed";
					}
				}
				if ($category['c.friendly_url'] && $friendly_urls) {
					$item_url = $category['c.friendly_url'] . $friendly_extension;
				} else {
					$item_url = "ads.php?category_id=" . $category_id;
				}
				$t->set_var("categori_id",  $category_id);
				$t->set_var("sub_class", $the_expand);
				$t->set_var("item_url",  $settings["site_url"] . $item_url);
				$t->set_var("item_name", get_translation($category['c.category_name']));
				$t->parse("sub_element");
			}
			if($site_map_ads){
				$found_ads = VA_Ads::find_all("", 
					array("i.item_id", "i.item_title", "i.friendly_url"),
					array("order" => " ORDER BY i.item_order, i.item_title",
						"where" => " ac.category_id = " . $db->tosql($parent_id, INTEGER),
					)
				);
				foreach ($found_ads AS  $ad_id => $ad) {
					if ($ad['i.friendly_url'] && $friendly_urls) {
						$item_url = $ad['i.friendly_url'] . $friendly_extension;
					} else {
						$item_url = "forum.php?forum_id=" . $ad['i.item_id'];
					}

					$t->set_var("categori_id",  $ad['i.item_id']);
					$t->set_var("sub_class", "ExpandLeaf");
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($ad['i.item_title']));
					$t->parse("sub_element");
				}

			}

		}
		if($type == "manuals" && $site_map_manual_categories){
			if(!$parent_id){
				$found_categories = VA_Manuals_Categories::find_all("c.category_id", 
					array("c.category_name", "c.friendly_url"),
					array("order" => " ORDER BY c.category_order, c.category_name")
				);
				foreach ($found_categories AS  $category_id => $category) {
					$the_expand = "ExpandLeaf";
					if($site_map_manual_articles){
						$found_items = VA_Manuals::find_all("", 
							array("ml.manual_id", "ml.manual_title", "ml.friendly_url", "c.category_id"),
							array(
								"where" => " c.category_id = " . $db->tosql($category_id, INTEGER),
								"order" => " ORDER BY ml.manual_order, ml.manual_title"
							)
						);
						if(sizeof($found_items)){
							$the_expand = "ExpandClosed";
						}
					}
					if ($category['c.friendly_url'] && $friendly_urls) {
						$item_url = $category['c.friendly_url'] . $friendly_extension;
					} else {
						$item_url = "manuals.php?category_id=" . $category_id;
					}

					$t->set_var("categori_id",  $category_id);
					$t->set_var("sub_class", $the_expand);
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($category['c.category_name']));
					$t->parse("sub_element");
				}
			}elseif($site_map_manual_articles && !$sub_parent_id){
				$found_items = VA_Manuals::find_all("", 
					array("ml.manual_id", "ml.manual_title", "ml.friendly_url", "c.category_id"),
					array(
						"where" => " c.category_id = " . $db->tosql($parent_id, INTEGER),
						"order" => " ORDER BY ml.manual_order, ml.manual_title"
					)
				);
				foreach ($found_items AS  $item_id => $item) {
					$the_expand = "ExpandLeaf";
					if($site_map_manual_articles){
						$sql_manual  = " SELECT article_id, article_title, parent_article_id, friendly_url, manual_id";
						$sql_manual .= " FROM " . $table_prefix . "manuals_articles ";
						$sql_manual .= " WHERE allowed_view=1";
						$sql_manual .= " AND manual_id =" . $db->tosql($item['ml.manual_id'], INTEGER);
						$sql_manual .= " ORDER BY article_order, article_title ";
						$db->query($sql_manual);
						if ($db->next_record()) {
							$the_expand = "ExpandClosed";
						}
					}
					if ($item['ml.friendly_url'] && $friendly_urls) {
						$item_url = $item['ml.friendly_url'] . $friendly_extension;
					} else {
						$item_url = "manuals_articles.php?manual_id=" . $item['ml.manual_id'];
					}

					$t->set_var("categori_id",  $parent_id."_".$item['ml.manual_id']);
					$t->set_var("sub_class", $the_expand);
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($item['ml.manual_title']));
					$t->parse("sub_element");
				}
			}
			if($site_map_manual_articles && $site_map_manuals && $sub_parent_id){
				$sql_manual  = " SELECT article_id, article_title, parent_article_id, friendly_url, manual_id";
				$sql_manual .= " FROM " . $table_prefix . "manuals_articles ";
				$sql_manual .= " WHERE allowed_view=1";
				$sql_manual .= " AND manual_id =" . $db->tosql($sub_parent_id, INTEGER);
				$sql_manual .= " ORDER BY article_order, article_title ";
				$db->query($sql_manual);
				while ($db->next_record()) {
					$article_id         = $db->f("article_id");
					$parent_article_id  = $db->f("parent_article_id");
					$friendly_url       = $db->f("friendly_url");
					$article_title      = $db->f("article_title");
					if ($friendly_url && $friendly_urls) {
						$item_url = $friendly_url . $friendly_extension;
					} else {
						$item_url = "manuals_article_details.php?article_id=" . $article_id;
					}

					$t->set_var("categori_id",  $article_id);
					$t->set_var("sub_class", "ExpandLeaf");
					$t->set_var("item_url",  $settings["site_url"] . $item_url);
					$t->set_var("item_name", get_translation($article_title));
					$t->parse("sub_element");
				}
			}
		}
		if($type == "manufacturers" && $site_map_manufacturers){
			$sql  = " SELECT manufacturer_id, manufacturer_name, friendly_url ";
			$sql .=	" FROM " . $table_prefix . "manufacturers  ";
			$sql .= " ORDER BY manufacturer_order ";
			$sql .= " , manufacturer_name ";
			$db->query($sql);
			$i = 0;
			while ($db->next_record()) {
				$i++;
				$manufacturer_id         = $db->f("manufacturer_id");
				$manufacturer_name  = $db->f("manufacturer_name");
				$friendly_url       = $db->f("friendly_url");
				if ($friendly_url && $friendly_urls) {
					$item_url = $friendly_url . $friendly_extension;
				} else {
					$item_url = "products.php?manf=" . $manufacturer_id;
				}

//				$t->set_var("categori_id",  $manufacturer_id);
//				$t->set_var("sub_class", "ExpandLeaf");
				$t->set_var("item_url",  $settings["site_url"] . $item_url);
				$t->set_var("item_name", get_translation($manufacturer_name));
				if($i%3){
					$t->parse("item_element");
				}else{
					$i=0;
					$t->parse("item_element");
					$t->parse("row_element");
					$t->set_var("item_element", "");
				}
			}
			while($i%3){
				$i++;
				if($i%3){
					$t->set_var("item_url",  "");
					$t->set_var("item_name", "");
					$t->parse("item_element");
				}else{
					$t->set_var("item_url",  "");
					$t->set_var("item_name", "");
					$t->parse("item_element");
					$t->parse("row_element");
					$t->set_var("item_element", "");
				}
			}
			$t->parse("manufacturers_element");
		}

	}

	$block_parsed = true;
?>