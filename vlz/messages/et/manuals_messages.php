<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Manualid");
	define("NO_MANUALS_MSG", "Manuale ei leitud");
	define("NO_MANUAL_ARTICLES_MSG", "Artiklied ei ole");
	define("MANUALS_PREV_ARTICLE", "Eelmine");
	define("MANUALS_NEXT_ARTICLE", "JРґrgmine");
	define("MANUAL_CONTENT_MSG", "Indeks");
	define("MANUALS_SEARCH_IN_MSG", "Otsi (kus?)");
	define("MANUALS_SEARCH_FOR_MSG", "Otsi");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "KС…ik manualid");
	define("MANUALS_SEARCH_RESULTS_INFO", "Leitud {results_number} artiklit otsingule {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Otsingutulemused");
	define("MANUALS_NOT_FOUND_ANYTHING", "Ei leidnud midagi otsingule '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Sisu puudub");
	define("MANUALS_SEARCH_TITLE", "Manualide otsing");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Manualide otsingutulemused");

?>