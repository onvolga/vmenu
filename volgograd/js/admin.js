var userAgent = navigator.userAgent.toLowerCase();
var isIE = ((userAgent.indexOf("msie") != -1) && (userAgent.indexOf("opera") == -1) && (userAgent.indexOf("webtv") == -1));
var tid = new Array();

function findPosX(obj, addWidth)
{
	var curleft = 0;
	if (addWidth) {
		curleft += obj.offsetWidth;
	}
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	} else if (obj.x){
		curleft += obj.x;
	}

	return curleft;
}

function findPosY(obj, addHeight)
{
	var curtop = 0;
	if (addHeight) {
		curtop += obj.offsetHeight;
	}
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	} else if (obj.y) {
		curtop += obj.y;
	}

	return curtop;
}

function getPageSize()
{
  var w = 0, h = 0;
  if (window.innerWidth) { //Non-IE
    w = window.innerWidth;
    h = window.innerHeight;
		if (window.scrollMaxY && window.scrollMaxY > 0) {
			w -= getScrollBarWidth(); // decrease page size for scrollbar width for FF
		}
		if (window.scrollMaxX && window.scrollMaxX > 0) { 
			h -= getScrollBarWidth(); // decrease page size for scrollbar height for FF
		}
  } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
    //IE 6+ in 'standards compliant mode'
    w = document.documentElement.clientWidth;
    h = document.documentElement.clientHeight;
  } else if ( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    w = document.body.clientWidth;
    h = document.body.clientHeight;
  }

	var pageSize= new Array(w, h);    
	return pageSize;
}

function getPageSizeWithScroll()
{
	var xWithScroll = 0; var yWithScroll = 0; 
	if (window.innerHeight && window.scrollMaxY) { // Firefox         
		yWithScroll = window.innerHeight + window.scrollMaxY;
		xWithScroll = window.innerWidth + window.scrollMaxX;     
		if (window.scrollMaxY > 0) {
			xWithScroll -= getScrollBarWidth(); // decrease page size for scrollbar width for FF 
		}
		if (window.scrollMaxX > 0) {
			yWithScroll -= getScrollBarWidth(); // decrease page size for scrollbar height for FF 
		}
	} else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac         
		yWithScroll = document.body.scrollHeight;         
		xWithScroll = document.body.scrollWidth;     
	} else { // works in Explorer 6 Strict, Mozilla (not FF) and Safari         
		yWithScroll = document.body.offsetHeight;         
		xWithScroll = document.body.offsetWidth;       
	}     
	var arrayPageSizeWithScroll = new Array(xWithScroll,yWithScroll);    
	return arrayPageSizeWithScroll; 
} 

function getScroll()
{
	var w = window.pageXOffset ||
		document.body.scrollLeft ||
		document.documentElement.scrollLeft;
	var h = window.pageYOffset ||
		document.body.scrollTop ||
		document.documentElement.scrollTop;
	var arrayScroll = new Array(w, h);    
	return arrayScroll;
}

function getScrollBarWidth () 
{
	var inner = document.createElement('p');   
	inner.style.width = "100%";   
	inner.style.height = "200px";   
 
	var outer = document.createElement('div');   
	outer.style.position = "absolute";   
	outer.style.top = "0px";   
	outer.style.left = "0px";   
	outer.style.visibility = "hidden";   
	outer.style.width = "200px";   
	outer.style.height = "150px";   
	outer.style.overflow = "hidden";   
	outer.appendChild (inner);   
  
	document.body.appendChild (outer);   
	var w1 = inner.offsetWidth;   
	outer.style.overflow = 'scroll';   
	var w2 = inner.offsetWidth;   
	if (w1 == w2) w2 = outer.clientWidth;   
  
	document.body.removeChild (outer);   
  
	return (w1 - w2);   
};  

function reloadSite(formObj)
{
	formObj.operation.value = "";
	formObj.submit();
}

function showHint(parentObj, hintId){
	var hintObj = document.getElementById(hintId);
	var popupObj = document.createElement("div");
	var xPos = findPosX(parentObj, true) + 2;
	var yPos = findPosY(parentObj);
	popupObj.id        = hintId + "Popup";;
	popupObj.className = "hintPopup";
	popupObj.style.left = xPos + "px";
	popupObj.style.top = yPos + "px";
	popupObj.style.display  = 'block';
	popupObj.innerHTML = hintObj.innerHTML;
	
	document.body.insertBefore(popupObj, document.body.firstChild);
}

function hideHint(hintId)
{
	var popupObj = document.getElementById(hintId+"Popup");
	if (popupObj) {
		document.body.removeChild(popupObj);
	}
}

function show(parentName, subName)
{
	var parentMenu = document.getElementById(parentName);
	var subMenu = document.getElementById(subName);

	// hide all other sub menus
	var menu = new Array("shop_sub", "orders_sub", "cms_sub", "articles_sub", "helpdesk_sub", "helpdesk2_sub", "forum_sub", "manual_sub", "ads_sub", "registrations_sub", "users_sub", "system_sub");
	for (var i = 0; i < menu.length; i++) {
		if (subName != menu[i]) {
			var menuObj = document.getElementById(menu[i]);
			if (menuObj && menuObj.style.display == "block") {
				menuObj.style.display='none';
				showSelectBoxes(menu[i]);
				if (tid[menu[i]]) {
					clearTimeout(tid[menu[i]]);
					tid[menu[i]] = "";
				}
			}
		}
	}


	if (subMenu) {
		if (parentName == 'book') {
			subMenu.style.top = findPosY(parentMenu, true) + "px";
		} else {
			subMenu.style.top = findPosY(parentMenu, true) + "px";
		}
		if (parentName == 'book') {
			subMenu.style.left = findPosX(parentMenu, false) + "px";
		} else {
			subMenu.style.left = findPosX(parentMenu, false) + "px";
		}
		subMenu.style.display='block';
		hideSelectBoxes(subName);
		if (tid[subName]) {
			clearTimeout(tid[subName]);
			tid[subName] = "";
		}
	}
}


function hide(subName)
{
	tid[subName] = setTimeout("hideMenu('" + subName + "')", 500);
}

function hideMenu(subName)
{
	var subMenu = document.getElementById(subName);
	if (subMenu) {
		subMenu.style.display = 'none';
		showSelectBoxes(subName);
	}
}

function showSelectBoxes(subName)
{
	if (isIE) {
		var subMenu = document.getElementById(subName);
		var selects = subMenu.overlapObjects;
		if (selects && selects.length > 0) {
			for (var i = 0; i < selects.length; i++) {
				selects[i].style.visibility = "visible";
			}
		}
		subMenu.overlapObjects = null;
	}
}

function hideSelectBoxes(subName)
{
	if (isIE) {
		var subMenu = document.getElementById(subName);

		var x = findPosX (subMenu, false);
		var y = findPosY (subMenu, false);
		var w = subMenu.offsetWidth;
		var h = subMenu.offsetHeight;

		if (!subMenu.overlapObjects) {
			subMenu.overlapObjects = new Array();
		}

		selects = document.getElementsByTagName("select");
		for (i = 0; i != selects.length; i++)
		{
			var selectObj = selects[i];
			if (selectObj.style.visibility == "hidden") {
				continue;
			}

			var ox = findPosX(selectObj, false);
			var oy = findPosY(selectObj, false);
			var ow = selectObj.offsetWidth;
			var oh = selectObj.offsetHeight;

			if (ox > (x + w) || (ox + ow) < x) {
				continue;
			}
			if (oy > (y + h) || (oy + oh) < y) {
				continue;
			}
			subMenu.overlapObjects[subMenu.overlapObjects.length] = selectObj;

			selects[i].style.visibility = "hidden";
		}
	}
}

function optionsWindow(pagename)
{
	var optionsWindow = window.open (pagename, 'optionsWindow', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=600,height=500');
	optionsWindow.focus();
}


function openHelpWindow(pagename)
{
	var popupWin = window.open (pagename, 'popup', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=500');
	popupWin.focus();
}

function addParam2URL(param, value, url)
{
	if (url.indexOf("?") == -1) {
		url = url + "?" +param + "=" + value;
	} else {
		url = url + "&" + param + "=" + value;
	}

	return url;
}

function mouseX(evt) {
	if (evt.pageX) {
		return evt.pageX;
	} else if (evt.clientX) {
		return evt.clientX + (document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft);
	} else {
		return null;
	}
}

function mouseY(evt) {
	if (evt.pageY) {
		return evt.pageY;
	} else if (evt.clientY) {
		return evt.clientY + (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
	} else {
		return null;
	}
}

function showNotes(event, bookmarkID, bookmarkNotes)	{
	var bookNote = document.getElementById("notes_" + bookmarkID);
	var len = bookmarkNotes.length;
	if (bookNote && len!=0) {
	var leftPos = mouseX(event) - 380;
   	var topPos  = mouseY(event) - 100;
		bookNote.style.left = leftPos;
		bookNote.style.top = topPos;
		bookNote.style.display = "block";
	}
}

function hideNotes(event, bookmarkID)
{
	var bookNote = document.getElementById("notes_" + bookmarkID);
	if (bookNote) {
		bookNote.style.display = 'none';
	}
}

function changeTab(newTabName, formName)
{
	var formObj = "";
	if (formName) {
		formObj = document.forms[formName];
	} else {
		formObj = document.record;
	}
	var currentTabName = formObj.tab.value;
	if (currentTabName != newTabName) {
		currentTab = document.getElementById("tab_" + currentTabName);
		newTab = document.getElementById("tab_" + newTabName);

		currentData = document.getElementById("data_" + currentTabName);
		newData = document.getElementById("data_" + newTabName);

		currentTab.className = "adminTab";
		newTab.className = "adminTabActive";

		if (currentData) {
			currentData.style.display = "none";
		}
		if (newData) {
			newData.style.display = "block";
		}

		formObj.tab.value = newTabName;

		// check if we need change the rows
		var rowObj = newTab.parentNode;
		if (rowObj && rowObj.id && rowObj.id.substring(0, 7) == "tab_row") {
			var tabs = "";
			var activeRowId = rowObj.id;
			var rowId = 1;
			while ((rowObj = document.getElementById("tab_row_" + rowId))) {
				if (rowObj.id == activeRowId) {
					tabs += "<div id='"+rowObj.id+"' class='tabRow'>" + rowObj.innerHTML + "</div>";
				} else {
					tabs = "<div id='"+rowObj.id+"' class='tabRow'>" + rowObj.innerHTML + "</div>" + tabs;
				}
				rowId++;
			}
			var tabsObj = document.getElementById("tabs");
			if (tabsObj && tabs != "") {
				tabsObj.innerHTML = tabs;
			}
		}
	}
}

function overhid(cat) {
	var nameCat = document.getElementById(cat);
	if (nameCat.className != "leftNavActive") {
		nameCat.className = "leftNavActive";
	}
	else {
		nameCat.className = "leftNavNonActive";
	}
}

/*using namespace BLZ*/
	var BLZ = BLZ || {};
	
	BLZ.utils = {
		addListener: null,
		removeListener: null
	};

	BLZ.utils.loader = function() {
		if (typeof window.addEventListener === 'function') {
			BLZ.utils.addListener = function (elem, ev, fn) {
				elem.addEventListener(ev, fn, false);
			};
			BLZ.utils.removeListener = function (elem, ev, fn) {
				elem.removeEventListener(ev, fn, false);
			};			
		}
		else if (typeof document.attachEvent === 'object') {
			BLZ.utils.addListener = function (elem, ev, fn) {
				elem.attachEvent('on' + ev, fn);
			};
			BLZ.utils.removeListener = function (elem, ev, fn) {
				elem.detachEvent('on' + ev, fn);
			};
		}
		else {
			BLZ.utils.addListener = function (elem, ev, fn) {
				elem['on' + ev] = fn;
			};
			BLZ.utils.removeListener = function (elem, ev, fn) {
				elem['on' + ev] = null;
			};
		}
	}();