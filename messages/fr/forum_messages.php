<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "L'Information De Matiиre");
	define("TOPIC_MESSAGE_TITLE", "Message");

	define("MY_FORUM_TOPICS_MSG", "Mes Matiиres De Forum");
	define("ALL_FORUM_TOPICS_MSG", "Toutes les Matiиres De Forum");
	define("MY_FORUM_TOPICS_DESC", "Vous кtes-vous jamais demandй si le problиme que vous avez est un que quelqu'un d'autre a йprouvй ? aimez-vous partager votre expertise avec de nouveaux utilisateurs ? Pourquoi pas vous devenez un utilisateur de forum et devenez une partie de la communautй?");
	define("NEW_TOPIC_MSG", "Nouvelle Matiиre");
	define("NO_TOPICS_MSG", "Matiиre n'a pas trouvй");
	define("FOUND_TOPICS_MSG", "We've found <b>{found_records}</b> topics matching the term(s) '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "Matiиre");
	define("FORUM_REPLIES_COLUMN", "Rйponses");
	define("FORUM_LAST_POST_COLUMN", "Dernier mis а jour");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "Matiиre");
	define("TOPIC_AUTHOR_COLUMN", "Auteur");
	define("TOPIC_VIEWS_COLUMN", "Vues");
	define("TOPIC_REPLIES_COLUMN", "Rйponses");
	define("TOPIC_UPDATED_COLUMN", "Dernier mis а jour");
	define("TOPIC_ADDED_MSG", "Merci <br>que votre matiиre a йtй ajoutйe");

	define("TOPIC_ADDED_BY_FIELD", "Supplйmentaire prиs");
	define("TOPIC_ADDED_DATE_FIELD", "Supplйmentaire");
	define("TOPIC_UPDATED_FIELD", "Dernier Mis а jour");
	define("TOPIC_NICKNAME_FIELD", "Surnom");
	define("TOPIC_EMAIL_FIELD", "Votre Email address");
	define("TOPIC_NAME_FIELD", "Topic");
	define("TOPIC_MESSAGE_FIELD", "Message");
	define("TOPIC_NOTIFY_FIELD", "Envoyez toutes les rйponses а mon email");

	define("ADD_TOPIC_BUTTON", "Ajoutez La Matiиre");
	define("TOPIC_MESSAGE_BUTTON", "Ajoutez Le Message");

	define("TOPIC_MISS_ID_ERROR", "Paramиtre absent d'identification de fil.");
	define("TOPIC_WRONG_ID_ERROR", "Le paramиtre d'identification de fil a la valeur fausse.");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>