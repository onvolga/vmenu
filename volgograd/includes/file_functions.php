<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  file_functions.php                                       ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


function unique_filename($file_path_original)
{
	$file_path = $file_path_original;
	$file_index = 0;
	while (file_exists($file_path)) {
		$file_index++;
		$delimiter_pos = strrpos($file_path_original, ".");
		// check slash position
		$slash_pos = strrpos($file_path_original, "/");
		$back_slash_pos = strrpos($file_path_original, "\\");
		if($delimiter_pos && $delimiter_pos > intval($slash_pos) && $delimiter_pos > intval($back_slash_pos)) {
			$file_path = substr($file_path_original, 0, $delimiter_pos) . "_" . $file_index . substr($file_path_original, $delimiter_pos);
		} else {
			$file_path = $index . "_" . $file_path_original;
		}
	}
	return $file_path;
}


function mkdir_recursively($file_path)
{
	// check if we need create a new dirs recursively
	if (preg_match("/^(.+[\/\\\\])[^\/\\\\]*$/", $file_path, $matches)) {
		$dir_path = $matches[1];
		if (!file_exists($dir_path)) {
	    $dir_path = preg_replace("/(\/){2,}|(\\\){1,}/", "/", $dir_path); 
	    $dir_path = rtrim($dir_path, "/");
			$dir_parts = explode("/", $dir_path);
			$dir_path = ""; // clear var to create dir one by one
			foreach ($dir_parts as $dir_part) {
				$dir_path .= $dir_part."/";
				if (!is_dir($dir_path)) {
					mkdir($dir_path, 0777);
				}
			}
		}
	}

}



?>