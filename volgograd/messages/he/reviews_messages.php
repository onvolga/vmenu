<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "אופציונלי");
	define("BAD_MSG", "גרוע");
	define("POOR_MSG", "חלש");
	define("AVERAGE_MSG", "ממוצע");
	define("GOOD_MSG", "טוב");
	define("EXCELLENT_MSG", "מצוין");
	define("REVIEWS_MSG", "סקירות");
	define("NO_REVIEWS_MSG", "לא נמצאו סקירות");
	define("WRITE_REVIEW_MSG", "כתוב סקירה");
	define("RATE_PRODUCT_MSG", "דרג מוצר זה");
	define("RATE_ARTICLE_MSG", "דרג פריט זה");
	define("NOT_RATED_PRODUCT_MSG", "המוצר טרם דורג");
	define("NOT_RATED_ARTICLE_MSG", "הפריט טרם דורג");
	define("AVERAGE_RATING_MSG", "דרוג ממוצע של הלקוחות");
	define("BASED_ON_REVIEWS_MSG", "מבוסס על {total_votes} סקירות");
	define("POSITIVE_REVIEW_MSG", "סקירת לקוח חיובית");
	define("NEGATIVE_REVIEW_MSG", "סקירת לקוח שלילית");
	define("SEE_ALL_REVIEWS_MSG", "צפה בכל סקירות הלקוחות");
	define("ALL_REVIEWS_MSG", "כל הסקירות");
	define("ONLY_POSITIVE_MSG", "חיוביות בלבד");
	define("ONLY_NEGATIVE_MSG", "שליליות בלבד");
	define("POSITIVE_REVIEWS_MSG", "סקירות חיוביות");
	define("NEGATIVE_REVIEWS_MSG", "סקירות שליליות");
	define("SUBMIT_REVIEW_MSG", "תודה<br>הערותיך ישקלו. אם יאושרו הם יוצגו באתר.<br>אנו שומרים את הזכות לא להציג הגשות שאינן תואמות את הכללים.");
	define("ALREADY_REVIEW_MSG", "כבר הגשת סקירה");
	define("RECOMMEND_PRODUCT_MSG", "האם תמליץ <br> על מוצר זה לאחרים?");
	define("RECOMMEND_ARTICLE_MSG", "האם תמליץ <br> על פריט זה לאחרים?");
	define("RATE_IT_MSG", "דרג");
	define("NAME_ALIAS_MSG", "שם או כנויי");
	define("SHOW_MSG", "הראה");
	define("FOUND_MSG", "נמצא");
	define("RATING_MSG", "דרוג ממוצע");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "רק משתמשים רשומים יכולים להוסיף את סקירתם");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "מצטערים, אינך יכול להוסיף סקירה");

	define("ALLOWED_VIEW_REVIEWS_MSG", "מורשה לראות סקירות");
	define("ALLOWED_POST_REVIEWS_MSG", "מורשה להוסיף סקירות");
	define("REVIEWS_RESTRICTIONS_MSG", "מגבלות הסקירות");
	define("REVIEWS_PER_USER_MSG", "מיספר הסקירות למשתמש");
	define("REVIEWS_PER_USER_DESC", "השאר שדה זה ריק אם ברצונך לא להגביל אתמיספר הסקירות למשתמש");
	define("REVIEWS_PERIOD_MSG", "תקופת הסקירות");
	define("REVIEWS_PERIOD_DESC", "התקופה בה מגבלות הסקירות יהיהו בתוקף");

	define("AUTO_APPROVE_REVIEWS_MSG", "אשור אוטומטי של סקירות");
	define("BY_RATING_MSG", "לפי הדרוג");
	define("SELECT_REVIEWS_FIRST_MSG", "נא לבחור סקירה");
	define("SELECT_REVIEWS_STATUS_MSG", "נא לבחור סטטוס");
	define("REVIEWS_STATUS_CONFIRM_MSG", "אתה עומד לשנות את הסטטוס של הסקירות שנבחרו ל- '{status_name}'. להמשיך?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "אתה בטוח שברצונך להסיר את הסקירות שנבחרו ({total_reviews})?");

?>