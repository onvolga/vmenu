<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "ViArt SHOP Inљtalбcia");

	define("INSTALL_STEP_1_TITLE", "Inљtalбcia: Krok 1");
	define("INSTALL_STEP_1_DESC", "Пakujeme, ћe ste si vybrali ViArt SHOP. Aby ste mohli dokonиiќ inљtalбciu, vyplтte nasledovnэ formulбr. Nezabudnite, ћe databбza musн uћ existovaќ. Ak inљtalujete na databбzu, ktorб pouћнva ODBC ako naprнklad MS Access, mali by ste najprv vytvoriќ DSN, aћ potom pokraиovaќ");
	define("INSTALL_STEP_2_TITLE", "Inљtalбcia: Krok 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Inљtalбcia: Krok 3");
	define("INSTALL_STEP_3_DESC", "Prosнm vyberte si vzhѕad strбnky. Neskфr ho mфћete zmeniќ.");
	define("INSTALL_FINAL_TITLE", "Inљtalбcia: Poslednэ krok");
	define("SELECT_DATE_TITLE", "Vyberte formбt dбtumu");

	define("DB_SETTINGS_MSG", "Nastavenia databбzy");
	define("DB_PROGRESS_MSG", "Stav napетania љtruktъry databбzy");
	define("SELECT_PHP_LIB_MSG", "Vyberte PHP kniћnicu");
	define("SELECT_DB_TYPE_MSG", "Vyberte typ databбzy");
	define("ADMIN_SETTINGS_MSG", "Administraиnй nastavenia");
	define("DATE_SETTINGS_MSG", "Formбty dбtumu");
	define("NO_DATE_FORMATS_MSG", "Ћiadne formбty dбtumu nie sъ k dispozнcii");
	define("INSTALL_FINISHED_MSG", "V tomto okamihu je Vaљa inљtalбcia ukonиenб. Prosнm skontrolujte nastavenia v administraиnэch nastaveniach a urobte potrebnй nastavenia.");
	define("ACCESS_ADMIN_MSG", "Pre prнstup k administraиnэm nastaveniam kliknite sem");
	define("ADMIN_URL_MSG", "Administraиnб URL");
	define("MANUAL_URL_MSG", "Manual URL");
	define("THANKS_MSG", "Пakujeme, ћe ste si vybrali <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "Typ databбzy");
	define("DB_TYPE_DESC", "Please select the <b>type of database</b> that you are using. If you using SQL Server or Microsoft Access, please select ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP kniћnica");
	define("DB_HOST_FIELD", "Hostname");
	define("DB_HOST_DESC", "Please enter the <b>name</b> or <b>IP address of the server</b> on which your ViArt database will run. If you are running your database on your local PC then you can probably just leave this as \"<b>localhost</b>\" and the port blank. If you using a database provided by your hosting company, please see your hosting company's documentation for the server settings.");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "Meno databбzy / DSN");
	define("DB_NAME_DESC", "If you are using a database such as MySQL or PostgreSQL then please enter the <b>name of the database</b> where you would like ViArt to create its tables. This database must exist already. If you are just installing ViArt for testing purposes on your local PC then most systems have a \"<b>test</b>\" database you can use. If not, please create a database such as \"viart\" and use that. If you are using Microsoft Access or SQL Server then the Database Name should be the <b>name of the DSN</b> that you have set up in the Data Sources (ODBC) section of your Control Panel.");
	define("DB_USER_FIELD", "Uћнvateѕskй meno");
	define("DB_PASS_FIELD", "Heslo");
	define("DB_USER_PASS_DESC", "<b>Username</b> and <b>Password</b> - please enter the username and password you want to use to access the database. If you are using a local test installation the username is probably \"<b>root</b>\" and the password is probably blank. This is fine for testing, but please note that this is not secure on production servers.");
	define("DB_PERSISTENT_FIELD", "Permanetnй pripojenie");
	define("DB_PERSISTENT_DESC", "to use MySQL or Postgre persistent connections, tick this box. If you do not know what it means, then leaving it unticked is probably best.");
	define("DB_CREATE_DB_FIELD", "Create DB");
	define("DB_CREATE_DB_DESC", "to create database if possible, tick this box. Works only for MySQL and Postgre");
	define("DB_POPULATE_FIELD", "Naplniќ databбzu");
	define("DB_POPULATE_DESC", "Pre vytvorenie tabuѕky љtruktъry databбzy a jej naplnenie dбtami zakliknite toto polниko");
	define("DB_TEST_DATA_FIELD", "Test Data");
	define("DB_TEST_DATA_DESC", "to add some test data to your database tick the checkbox");
	define("ADMIN_EMAIL_FIELD", "Administrбtorov email");
	define("ADMIN_LOGIN_FIELD", "Administrбtorove uћнvateѕskй meno");
	define("ADMIN_PASS_FIELD", "Administrбtorove heslo");
	define("ADMIN_CONF_FIELD", "Potvrdiќ heslo");
	define("DATETIME_SHOWN_FIELD", "Formбt иasu (zobrazenэ na strбnke)");
	define("DATE_SHOWN_FIELD", "Formбt dбtumu (zobrazenэ na strбnke)");
	define("DATETIME_EDIT_FIELD", "Formбt иasu (pre ъpravy)");
	define("DATE_EDIT_FIELD", "Formбt dбtumu (pre ъpravy)");
	define("DATE_FORMAT_COLUMN", "Formбt dбtumu");
	define("CURRENT_DATE_COLUMN", "Dneљnэ dбtum");

	define("DB_LIBRARY_ERROR", "PHP funkcie pre {db_library} nie sъ definovanй. Prosнm skontrolujte nastavenia databбzy v konfiguraиnom sъbore - php.ini.");
	define("DB_CONNECT_ERROR", "Nemфћem sa pripojiќ k databбze. Skontrolujte parametre databбzy.");
	define("INSTALL_FINISHED_ERROR", "Inљtalaиnэ proces bol uћ ukonиenэ.");
	define("WRITE_FILE_ERROR", "Nemбm prбva na zбpis do sъboru <b>'includes/var_definition.php'</b>. Pred pokraиovanнm skontrolujte prнstupovй prбva.");
	define("WRITE_DIR_ERROR", "Nemбm prбva na zбpis do adresбra <b>'includes/'</b>. Pred pokraиovanнm skontrolujte prнstupovй prбva.");
	define("DUMP_FILE_ERROR", "Dump sъbor '{file_name}' nebol nбjdenэ.");
	define("DB_TABLE_ERROR", "Tabuѕka '{table_name}' nebola nбjdenб. Prosнm naplтte databбzu prнsluљnэmi dбtami.");
	define("TEST_DATA_ERROR", "Check <b>{POPULATE_DB_FIELD}</b> before populating tables with test data");
	define("DB_HOST_ERROR", "The hostname that you specified could not be found.");
	define("DB_PORT_ERROR", "Can't connect to database server using specified port.");
	define("DB_USER_PASS_ERROR", "The username or password you specified is not correct.");
	define("DB_NAME_ERROR", "Login settings were correct, but the database '{db_name}' could not be found.");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt SHOP Aktualizбcia");
	define("UPGRADE_NOTE", "Poznбmka: Zvбћte prosнm vytvorenie zбlohy databбzy pred pokraиovanнm.");
	define("UPGRADE_AVAILABLE_MSG", "Aktualizбcia k dispozнcii");
	define("UPGRADE_BUTTON", "Aktualizovaќ na verziu {version_number} teraz");
	define("CURRENT_VERSION_MSG", "Vaљa aktuбlne nainљtalovanб verzia");
	define("LATEST_VERSION_MSG", "Verzia dostupnб na inљtalбciu");
	define("UPGRADE_RESULTS_MSG", "Vэsledky aktualizбcie");
	define("SQL_SUCCESS_MSG", "SQL dotaz ъspeљnэ");
	define("SQL_FAILED_MSG", "SQL dotaz neъspeљnэ");
	define("SQL_TOTAL_MSG", "Spolu vykonanэch SQL dotazov");
	define("VERSION_UPGRADED_MSG", "Vaљa verzia bola aktualizovanб na");
	define("ALREADY_LATEST_MSG", "Mбte nainљtalovanъ najaktuбlnejљiu verziu");
	define("DOWNLOAD_NEW_MSG", "The new version was detected");
	define("DOWNLOAD_NOW_MSG", "Download version {version_number} now");
	define("DOWNLOAD_FOUND_MSG", "We have detected that the new {version_number} version is available to download. Please click the link below to start downloading. After completing the download and replacing the files don't forget to run Upgrade routine again.");
	define("NO_XML_CONNECTION", "Warning! No connection to 'http://www.viart.com/' available!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>