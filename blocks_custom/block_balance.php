<?php

$html_template = get_setting_value($block, "html_template", "block_balance.html");
$t->set_file("block_body", $html_template);

// Ловим информацию о пользователе
$user_info = get_session("session_user_info");
$user_id = $user_info['user_id'];

// Если просмотр не разрешен (по типу пользователя), выключаем
$user_types = get_setting_value($vars, "user_type", array());
if(!in_array($user_info['user_type_id'], $user_types)) {
	return;
}

// Тарифный план соответсвует группе пользователя
$db->query("SELECT type_name FROM " . $table_prefix . "user_types WHERE `type_id` = " . $user_info['user_type_id']);
if($db->next_record()) {
	$t->set_var('tarif_name', $db->f('type_name'));
}

// Высчитываем текущий баланс пользователя
$db->query("SELECT credit_amount, credit_action FROM " . $table_prefix . "users_credits WHERE user_id = " . $user_id);
$credit_balance = 0;
while($db->next_record()) {
	$credit_balance += $db->f('credit_amount') * $db->f('credit_action');
}
$t->set_var('balance_count', currency_format(number_format($credit_balance, 2, '.', '')));

global $db, $table_prefix;

// Установка комиссионных переменных
function set_var_month_summ($sql, $name_option) {
	
	global $db, $t;
	$db->query($sql);
	$$name_option = 0;
	while($db->next_record()) { 
		$$name_option += $db->f('merchant_commission') * $db->f('quantity');
	}
	$t->set_var($name_option, number_format($$name_option, 2, '.', ''));
	return $$name_option;
	
}

// Получаем предметы, принадлежащие продавцу, по которым еще не было проведено списание
$general_sql = "SELECT * FROM " . $table_prefix . "orders_items AS oi ";
$general_sql .= " JOIN " . $table_prefix . "orders AS o ON o.order_id = oi.order_id ";
$general_sql .= " LEFT OUTER JOIN " . $table_prefix . "writes_off AS woff ON woff.order_item_id = oi.order_item_id ";
$general_sql .= "WHERE oi.item_user_id = " . $user_id . " AND woff.order_item_id is NULL ";

// Комиссионные за предыдущий месяц
if(date('n') == 1) { // В январе смотрим на декабрь предыдущего года
	$additional_sql_prev = " AND YEAR(o.order_placed_date) = YEAR(NOW()) - 1 AND MONTH(o.order_placed_date) = 12";
} else { // В остальное время смотрим на предыдущий месяц текущего года
	$additional_sql_prev = " AND YEAR(o.order_placed_date) = YEAR(NOW()) AND MONTH(o.order_placed_date) = MONTH(NOW()) - 1";
}
$summ_commisions_last_month = set_var_month_summ($general_sql . $additional_sql_prev, 'summ_commisions_last_month');

// Комиссионные за текущий месяц
$additional_sql_current = " AND YEAR(o.order_placed_date) = YEAR(NOW()) AND MONTH(o.order_placed_date) = MONTH(NOW())";
$summ_commisions_current_month = set_var_month_summ($general_sql . $additional_sql_current, 'summ_commisions_current_month');

$t->set_var('blocked_balance', currency_format(number_format($summ_commisions_last_month + $summ_commisions_current_month, 2, '.', '')));

$block_parsed = true;
$t->parse("block_body", false);

?>