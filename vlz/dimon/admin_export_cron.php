<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_export_cron.php                                    ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	@set_time_limit (900);
	chdir (dirname(__FILE__));
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/file_functions.php");

	check_admin_security("import_export");

	$cron_templates = array();
	$sql  = " SELECT * FROM ".$table_prefix."export_templates  ";
	$sql .= " WHERE is_cronjob=1 ";
	$db->query($sql);
	while ($db->next_record()) {
		$template_id = $db->f("template_id");
		$cron_templates[$template_id] = $db->Record;
	}

	foreach ($cron_templates as $template_id => $template_data) {
		$_POST = array(); // clear request array before start export
		$cron_table = $template_data["table_name"];
		$file_path_mask = $template_data["file_path_mask"];
		$file_path_copy = $template_data["file_path_mask_copy"];
		$order_status_update = $template_data["order_status_update"];
		// build file path from the mask
		if (preg_match_all("/\{(\w+)\}/is", $file_path_mask, $matches)) {
			for ($p = 0; $p < sizeof($matches[1]); $p++) {
				$tag = $matches[1][$p];
				if (in_array($tag, $date_formats)) {
					$file_path_mask = str_replace("{".$tag."}", va_date($tag), $file_path_mask);
				}
			}
		}
		if (preg_match_all("/\{(\w+)\}/is", $file_path_copy, $matches)) {
			for ($p = 0; $p < sizeof($matches[1]); $p++) {
				$tag = $matches[1][$p];
				if (in_array($tag, $date_formats)) {
					$file_path_copy = str_replace("{".$tag."}", va_date($tag), $file_path_copy);
				}
			}
		}
		if (!$file_path_mask) {
			return;
		}

		// check for available unique name for file
		$file_path = unique_filename($file_path_mask);
		mkdir_recursively($file_path);
		$fp = fopen($file_path, "w");

		if (strlen($file_path_copy)) {
			$file_path_copy = unique_filename($file_path_copy);
			mkdir_recursively($file_path_copy);
			$fp_copy = fopen($file_path_copy, "w");
		}

		if (!$fp) {
			// can't open file
			continue;
		}

		$_POST["table"] = $cron_table;
		$_POST["csv_delimiter"] = "comma";
		$_POST["related_delimiter"] = "row";
		$_POST["operation"] = "export";
		$_POST["order_status_update"] = $order_status_update;

		$col  = 0;
		$sql  = " SELECT field_title, field_source FROM ".$table_prefix."export_fields ";
		$sql .= " WHERE template_id=" . $db->tosql($template_id, INTEGER);
		$sql .= " ORDER BY field_order ";
		$db->query($sql);
		while ($db->next_record()) {
			$col++;
		  $field_title = $db->f("field_title");
		  $field_source = $db->f("field_source");
			// save parameters in post
			$_POST["column_title_" . $col] = $field_title;
			$_POST["field_source_" . $col] = $field_source;
			$_POST["db_column_" . $col] = "1";
			
		}
		$_POST["total_columns"] = $col;

		// set filters for template
		$sql  = " SELECT filter_parameter, filter_value FROM ".$table_prefix."export_filters ";
		$sql .= " WHERE template_id=" . $db->tosql($template_id, INTEGER);
		$db->query($sql);
		while ($db->next_record()) {
		  $filter_parameter = $db->f("filter_parameter");
		  $filter_value = $db->f("filter_value");
			$_POST[$filter_parameter] = $filter_value;
		}

		// import data
		include("./admin_export.php");

		if ($fp) {
			fclose($fp);
			// if there are no records we delete empty file
			if (isset($total_records) && $total_records == 0) {
				unlink($file_path);
			}
		}
		if (isset($fp_copy) && strlen($file_path_copy)) {
			fclose($fp_copy);
			unset($fp_copy);
			// if there are no records we delete empty file
			if (isset($total_records) && $total_records == 0) {
				unlink($file_path_copy);
			}
		}
	}
	
?>
