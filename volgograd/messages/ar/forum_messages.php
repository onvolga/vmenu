<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "ЗбгдКПм");
	define("TOPIC_INFO_TITLE", "гЪбжгЗК ЗбгжЦжЪ");
	define("TOPIC_MESSAGE_TITLE", "ЗбСУЗбе");

	define("MY_FORUM_TOPICS_MSG", "гжЗЦнЪн Эн ЗбгдКПм");
	define("ALL_FORUM_TOPICS_MSG", "МгнЪ ЗбгжЗЦнЪ Эн ЗбгдКПм");
	define("MY_FORUM_TOPICS_DESC", "еб КНУ ИГд гФЯбКЯ нЪЗдн гдеЗ ФОХ ВОСї еб КСнП гФЗСЯЙ ОИСЗКЯ гЪ ЗбГЪЦЗБ ЗбМППї бгЗРЗ бЗ КЯжд ЪЦжЗр Эн гМКгЪдЗї");
	define("NEW_TOPIC_MSG", "гжЦжЪ МПнП");
	define("NO_TOPICS_MSG", "бЗ КжМП ЗнЙ гжЦжЪЗК");
	define("FOUND_TOPICS_MSG", "<b>{search_string}</b>' ЗбгдКПм КШЗИЮ ЯбгЙ ЗбИНЛ <b>{found_records}</b> бЮП Кг ЗнМЗП");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "ЗбгдКПм");
	define("FORUM_TOPICS_COLUMN", "ЗбгжЦжЪ");
	define("FORUM_REPLIES_COLUMN", "ЗбСПжП");
	define("FORUM_LAST_POST_COLUMN", "ВОС КНПнЛ");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "ЗбгжЦжЪ");
	define("TOPIC_AUTHOR_COLUMN", "ЗбЯЗКИ");
	define("TOPIC_VIEWS_COLUMN", "ЗбгФЗеПЗК");
	define("TOPIC_REPLIES_COLUMN", "ЗбСПжП");
	define("TOPIC_UPDATED_COLUMN", "ВОС КНПнЛ");
	define("TOPIC_ADDED_MSG", "бЮП Кг ЗЦЗЭЙ гжЦжЪЯ ИдМЗН<br>ФЯСЗр бЯ");

	define("TOPIC_ADDED_BY_FIELD", "ЯКИ ИжЗУШЙ");
	define("TOPIC_ADDED_DATE_FIELD", "ЗЦнЭ");
	define("TOPIC_UPDATED_FIELD", "ВОС КНПнЛ");
	define("TOPIC_NICKNAME_FIELD", "ЗбЕУг");
	define("TOPIC_EMAIL_FIELD", "ИСнПЯ ЗбЕбЯКСждн");
	define("TOPIC_NAME_FIELD", "ЗбгжЦжЪ");
	define("TOPIC_MESSAGE_FIELD", "ЗбСУЗбе");
	define("TOPIC_NOTIFY_FIELD", "ГСУб МгнЪ ЗбСПжП Збм ИСнПн ЗбЕбЯКСждн");

	define("ADD_TOPIC_BUTTON", "ЗЦЭ гжЦжЪ");
	define("TOPIC_MESSAGE_BUTTON", "ЗЦЭ СУЗбе");

	define("TOPIC_MISS_ID_ERROR", ".СЮг ЮУг гЭЮжП");
	define("TOPIC_WRONG_ID_ERROR", ".СЮг ЮУг нНгб Юнге ОЗШЖе");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>