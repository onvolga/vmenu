<?php

	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./includes/products_functions.php");

	$default_title = "{search_name} &nbsp; {SEARCH_TITLE}";

	$html_template = get_setting_value($block, "html_template", "block_search.html"); 
  $t->set_file("block_body", $html_template);

	$t->set_var("search_href", get_custom_friendly_url("products_search.php"));
	$t->set_var("search_name", PRODUCTS_TITLE);
	// clear tags before parse
	$t->set_var("search_categories", "");
	$t->set_var("search_category_id", "");
	$t->set_var("no_search_categories", "");
	$t->set_var("advanced_search", "");
	
	$query_string = transfer_params("", false);

	$category_id = 0; $search_string = "";
	// check category_id and search parameter only for product pages
	if ($cms_page_code == "products_list" || $cms_page_code == "product_details" 
		|| $cms_page_code == "product_options" || $cms_page_code == "product_reviews" 
		|| $cms_page_code == "products_search_results") {
		$category_id = get_param("category_id");
		$search_category_id = get_param("search_category_id");
		if ($search_category_id) { $category_id = $search_category_id; }
		$search_string = trim(get_param("search_string"));
	}

	
	$t->set_var("advanced_search_href", htmlspecialchars(get_custom_friendly_url("search.php").$query_string));
	$t->global_parse("advanced_search", false, false, true);
	
	$t->set_var("search_string", htmlspecialchars($search_string));

	if ($t->block_exists("search_category_id")) {	
		$search_categories = array();
		$search_categories[] = array(0, SEARCH_IN_ALL_MSG);  
		if($category_id != 0) {
			$search_categories[] = array($category_id, SEARCH_IN_CURRENT_MSG);
		}

		if (!strlen($category_id)) { $category_id = 0; }
		$categories_ids = VA_Categories::find_all_ids("c.parent_category_id = " . $db->tosql($category_id, INTEGER), VIEW_CATEGORIES_ITEMS_PERM);
		if ($categories_ids) {
			$sql  = " SELECT category_id, category_name ";
			$sql .= " FROM " . $table_prefix . "categories ";
			$sql .= " WHERE category_id IN (" . $db->tosql($categories_ids, INTEGERS_LIST) . ") ";
			$sql .= " ORDER BY category_order ";
			$search_categories = get_db_values($sql, $search_categories);
		}
		// set up search form parameters
		$t->set_var("no_search_categories", "");
		if (sizeof($search_categories) > 1) {
			//set_options($search_categories, $category_id, "search_category_id");
			set_options($search_categories, 0, "search_category_id");
			$t->global_parse("search_categories", false, false, true);
		} else {
			$t->set_var("search_categories", "");
		}

		$t->set_var("current_category_id", htmlspecialchars($category_id));	
	} else {
		$t->set_var("current_category_id", "");
	}

	
	$block_parsed = true;

?>