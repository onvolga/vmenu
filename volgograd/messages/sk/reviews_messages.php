<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Voliteѕnй");
	define("BAD_MSG", "Zlэ");
	define("POOR_MSG", "Slabэ");
	define("AVERAGE_MSG", "Priemernэ");
	define("GOOD_MSG", "Dobrэ");
	define("EXCELLENT_MSG", "Vэbornэ");
	define("REVIEWS_MSG", "Hodnotenia");
	define("NO_REVIEWS_MSG", "Ћiadne hodnotenia nenбjdenй");
	define("WRITE_REVIEW_MSG", "Napнsaќ recenziu");
	define("RATE_PRODUCT_MSG", "Hodnotiќ tento produkt");
	define("RATE_ARTICLE_MSG", "Hodnotiќ tento иlбnok");
	define("NOT_RATED_PRODUCT_MSG", "Produkt eљte nie je hodnotenэ");
	define("NOT_RATED_ARTICLE_MSG", "Иlбnok eљte nie je hodnotenэ.");
	define("AVERAGE_RATING_MSG", "Priemernй hodnotenie zбkaznнkmi");
	define("BASED_ON_REVIEWS_MSG", "zaloћenй na {total_votes} recenziбch");
	define("POSITIVE_REVIEW_MSG", "Pozitнvne zбkaznнcke hodnotenia");
	define("NEGATIVE_REVIEW_MSG", "Negatнvne zбkaznнcke hodnotenia");
	define("SEE_ALL_REVIEWS_MSG", "Zobraziќ vљetky zбkaznнcke hodnotenia");
	define("ALL_REVIEWS_MSG", "Vљetky hodnotenia");
	define("ONLY_POSITIVE_MSG", "Iba pozitнvne");
	define("ONLY_NEGATIVE_MSG", "Iba negatнvne");
	define("POSITIVE_REVIEWS_MSG", "Pozitнvne hodnotenia");
	define("NEGATIVE_REVIEWS_MSG", "Negatнvne hodnotenia");
	define("SUBMIT_REVIEW_MSG", "Пakujeme<br>Vaљe pripomienky budъ zrevidovanй. Ak prejdъ schvбlenнm, zobrazia sa na naљej strбnke.<br>Vyhradzujeme si prбvo nezverejniќ prнspevky nespетajъce naљe pravidlб.");
	define("ALREADY_REVIEW_MSG", "Uћ ste vloћili recenziu.");
	define("RECOMMEND_PRODUCT_MSG", "Odporuиili by ste <br> tento produkt inэm?");
	define("RECOMMEND_ARTICLE_MSG", "Odporuиili by ste <br> tento иlбnok inэm?");
	define("RATE_IT_MSG", "Hodnotiќ");
	define("NAME_ALIAS_MSG", "Meno alebo alias");
	define("SHOW_MSG", "Ukбzaќ");
	define("FOUND_MSG", "Nбjdenэ");
	define("RATING_MSG", "Priemernй hodnotenie");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>