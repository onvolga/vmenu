<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "פורום");
	define("TOPIC_INFO_TITLE", "מידע נושא");
	define("TOPIC_MESSAGE_TITLE", "הודעה");

	define("MY_FORUM_TOPICS_MSG", "נושאי הפורום שלי");
	define("ALL_FORUM_TOPICS_MSG", "כל נושאי הפורום");
	define("MY_FORUM_TOPICS_DESC", "האם שאלת את עצמך אם הבעיות שנתקלת בהם, גם אחרים נתקלו בהם?, האם ברצונך לחלוק את המומחיות של עם אחרים?, מדוע שלא תצטרף לפורום ולקהילה.");
	define("NEW_TOPIC_MSG", "נושא חדש");
	define("NO_TOPICS_MSG", "לא נמצאו נושאים");
	define("FOUND_TOPICS_MSG", "נמצאו <b>{found_records}</b> רשומות התואמות ל- '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "לא נמצאו רשומות");

	define("FORUM_NAME_COLUMN", "פורום");
	define("FORUM_TOPICS_COLUMN", "נושאים");
	define("FORUM_REPLIES_COLUMN", "תשובות");
	define("FORUM_LAST_POST_COLUMN", "עדכון אחרון");
	define("FORUM_MODERATORS_MSG", "מתווכים");

	define("TOPIC_NAME_COLUMN", "נושא");
	define("TOPIC_AUTHOR_COLUMN", "מחבר");
	define("TOPIC_VIEWS_COLUMN", "דעות");
	define("TOPIC_REPLIES_COLUMN", "תשובות");
	define("TOPIC_UPDATED_COLUMN", "Last updated");
	define("TOPIC_ADDED_MSG", "תודה<br>הנושא שלך הוסף");

	define("TOPIC_ADDED_BY_FIELD", "הוסף ע\"י");
	define("TOPIC_ADDED_DATE_FIELD", "הוסף ע\"י");
	define("TOPIC_UPDATED_FIELD", "עדכון אחרון");
	define("TOPIC_NICKNAME_FIELD", "שם חיבה");
	define("TOPIC_EMAIL_FIELD", "כתובת המייל שלך");
	define("TOPIC_NAME_FIELD", "נושא");
	define("TOPIC_MESSAGE_FIELD", "הודעה");
	define("TOPIC_NOTIFY_FIELD", "שלח את כל התגובות באי-מייל");

	define("ADD_TOPIC_BUTTON", "הוסף נושא");
	define("TOPIC_MESSAGE_BUTTON", "הוסיף הודעה");

	define("TOPIC_MISS_ID_ERROR", "חסרים <b>Thread ID</b> ערכים");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> ערכים עם ערך שגוי");
	define("FORUM_SEARCH_MESSAGE", "נמצאו {search_count} הודעות התואמות ל- '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "צפיה מראש");
	define("TOPIC_SAVE_BUTTON", "שמור");

	define("LAST_POST_ON_SHORT_MSG", "על:");
	define("LAST_POST_IN_SHORT_MSG", "ב:");
	define("LAST_POST_BY_SHORT_MSG", "על ידי:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "שוני אחרון:");

?>