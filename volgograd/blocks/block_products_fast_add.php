<?php
	
	include_once("./includes/items_properties.php");

	$default_title = "{FAST_PRODUCT_ADDING_MSG}";
	
	$shopping_cart = get_session("shopping_cart");
	$user_info = get_session("session_user_info");
	$friendly_urls = get_setting_value($settings, "friendly_urls", 0);
	$friendly_extension = get_setting_value($settings, "friendly_extension", "");
	$user_id = get_session("session_user_id");
	
	$current_ts = va_timestamp();
		
	srand((double) microtime() * 1000000);
	$rnd = rand();

	$remove_parameters = array();
	if ($friendly_urls && isset($page_friendly_url) && $page_friendly_url) {
		$current_page = $page_friendly_url . $friendly_extension;
		$query_string = transfer_params($page_friendly_params, true);
	} else {
		$query_string = transfer_params("", true);
	}
	$t->set_var("current_href", $current_page);

	$html_template = get_setting_value($block, "html_template", "block_products_fast_add.html"); 
	$t->set_file("block_body", $html_template);

	$fast_code = ""; $fast_quantity = "";
	$param_pb_id = get_param("pb_id"); // check if this block was used
	$operation = get_param("operation");
	$errors = ""; $message = "";
	if ($param_pb_id == $pb_id && $operation) {
		// check parameters from request
		$fast_id = "";
		$fast_code = trim(get_param("fast_code"));
		$fast_quantity = trim(get_param("fast_quantity"));
		$fast_url = "";

		if (!strlen($fast_code)) {
			$errors .= str_replace("{field_name}", CODE_MSG, REQUIRED_MESSAGE) . "<br>";
		}		
		if (!strlen($errors)) {
			$sql  = " SELECT i.item_id, i.item_type_id, i.item_name, i.friendly_url ";
			$sql .= " FROM (" . $table_prefix . "items i ";
			$sql .= " LEFT JOIN " . $table_prefix . "item_types it ON i.item_type_id=it.item_type_id) ";
			$sql .= " WHERE i.item_code=" . $db->tosql($fast_code, TEXT);
			$sql .= " OR i.manufacturer_code=" . $db->tosql($fast_code, TEXT);
			$db->query($sql);
			if ($db->next_record()) {
				$fast_id = $db->f("item_id");
				$friendly_url = $db->f("friendly_url");
				if ($friendly_urls && strlen($friendly_url)) {
					$fast_url = $friendly_url.$friendly_extension;
				} else {
					$fast_url = "product_details.php?item_id=".urlencode($fast_id);
				}

			} else {
				$errors .=  NO_PRODUCTS_MSG . "<br>";
			}
		}

		if (!strlen($errors)) {
			// there are no errors so we can try add product
			$index = ""; $price = ""; $options_errors = ""; $type = "";
			$quantity = $fast_quantity;
			if (!$quantity) { $quantity = 1; }
			$item_added = add_to_cart($fast_id, $index, $price, $quantity, $type, "ADD", $new_cart_id, $errors, $message);
			if ($item_added) {
				check_coupons();
				// clear parameters after success
				$fast_code = ""; $fast_quantity = "";
			} else if ($errors && $errors == $options_errors) {
				// redirect to details page to select necessary options
				header("Location: ". $fast_url);
				exit;
			}
		}
	}

	if (strlen($errors)) {
		$t->set_var("errors", $errors);
		$t->parse("errors_block", false);
	} else {
		$t->set_var("errors_block", "");
	}
	if (strlen($message)) {
		$t->set_var("message", $message);
		$t->parse("message_block", false);
	} else {
		$t->set_var("message_block", "");
	}
	$t->set_var("fast_code", $fast_code);			
	$t->set_var("fast_quantity",  $fast_quantity);

	$block_parsed = true;
?>