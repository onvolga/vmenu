<?php

	@set_time_limit(600);
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "messages/" . $language_code . "/install_messages.php");
	include_once($root_folder_path . "messages/" . $language_code . "/cart_messages.php");
	include_once("./admin_common.php");

	check_admin_security("system_upgrade");

	$eol = get_eol();
	$operation = get_param("operation");


	$sqls = array();

		// check and add new subscribe/unsubscribe pages
		$sql = " SELECT MAX(page_id) FROM ".$table_prefix."cms_pages "; 
		$max_page_id = get_db_value($sql);

		$sql = " SELECT module_id FROM ".$table_prefix."cms_modules WHERE module_code='global' "; 
		$global_module_id = get_db_value($sql);

		$sql = " SELECT page_id FROM ".$table_prefix."cms_pages WHERE page_code='subscribe' "; 
		$subscribe_page_id = get_db_value($sql);
		$sql = " SELECT page_id FROM ".$table_prefix."cms_pages WHERE page_code='unsubscribe' "; 
		$unsubscribe_page_id = get_db_value($sql);

		if (!$subscribe_page_id) {
			$max_page_id++;
			$subscribe_page_id = $max_page_id;
			$sql = "INSERT INTO ".$table_prefix."cms_pages (page_id,module_id,page_order,page_code,page_name) VALUES (";
			$sql.= $db->tosql($subscribe_page_id, INTEGER).",";
			$sql.= $db->tosql($global_module_id, INTEGER).",";
			$sql.= $db->tosql(13, INTEGER).",";
			$sql.= $db->tosql("subscribe", TEXT).",";
			$sql.= $db->tosql("SUBSCRIBE_TITLE", TEXT).")";
			$sqls[] = $sql;
		}
		if (!$unsubscribe_page_id) {
			$max_page_id++;
			$unsubscribe_page_id = $max_page_id;
			$sql = "INSERT INTO ".$table_prefix."cms_pages (page_id,module_id,page_order,page_code,page_name) VALUES (";
			$sql.= $db->tosql($unsubscribe_page_id, INTEGER).",";
			$sql.= $db->tosql($global_module_id, INTEGER).",";
			$sql.= $db->tosql(14, INTEGER).",";
			$sql.= $db->tosql("unsubscribe", TEXT).",";
			$sql.= $db->tosql("UNSUBSCRIBE_TITLE", TEXT).")";
			$sqls[] = $sql;
		}

		// check subscribe, unsubscribe, header and footer blocks
		$sql = " SELECT block_id FROM ".$table_prefix."cms_blocks WHERE block_code='subscribe' "; 
		$subscribe_block_id = get_db_value($sql);
		$sql = " SELECT block_id FROM ".$table_prefix."cms_blocks WHERE block_code='unsubscribe' "; 
		$unsubscribe_block_id = get_db_value($sql);
		$sql = " SELECT block_id FROM ".$table_prefix."cms_blocks WHERE block_code='header' "; 
		$header_block_id = get_db_value($sql);
		$sql = " SELECT block_id FROM ".$table_prefix."cms_blocks WHERE block_code='footer' "; 
		$footer_block_id = get_db_value($sql);

		// check page settings
		$sql = " SELECT MAX(ps_id) FROM ".$table_prefix."cms_pages_settings "; 
		$max_ps_id = get_db_value($sql);

		$sql = " SELECT page_id FROM ".$table_prefix."cms_pages_settings WHERE page_id=".$db->tosql($subscribe_page_id, INTEGER); 
		$subscribe_ps_id = get_db_value($sql);
		$sql = " SELECT page_id FROM ".$table_prefix."cms_pages_settings WHERE page_id=".$db->tosql($unsubscribe_page_id, INTEGER); 
		$unsubscribe_ps_id = get_db_value($sql);

		// subscribe page settings
		if (!$subscribe_ps_id) {
			$max_ps_id++;
			$subscribe_ps_id = $max_ps_id;

			$sql = "INSERT INTO ".$table_prefix."cms_pages_settings ";
			$sql.= " (ps_id,page_id,key_code,key_type,key_rule,layout_id,site_id) VALUES (";
			$sql.= $db->tosql($subscribe_ps_id, INTEGER).",";
			$sql.= $db->tosql($subscribe_page_id, INTEGER).",";
			$sql.= $db->tosql("", TEXT).",";
			$sql.= $db->tosql("", TEXT).",";
			$sql.= $db->tosql("", TEXT).",";
			$sql.= $db->tosql(1, INTEGER).",";
			$sql.= $db->tosql(1, INTEGER).")";
			$sqls[] = $sql;

			// added blocks for page
			if ($header_block_id) {
				$sql = "INSERT INTO ".$table_prefix."cms_pages_blocks ";
				$sql.= " (ps_id,frame_id,block_id,block_key,block_order) VALUES (";
				$sql.= $db->tosql($subscribe_ps_id, INTEGER).",";
				$sql.= $db->tosql(1, INTEGER).",";
				$sql.= $db->tosql($header_block_id, INTEGER).",";
				$sql.= $db->tosql("", TEXT).",";
				$sql.= $db->tosql(1, INTEGER).")";
				$sqls[] = $sql;
			}
	  
			if ($subscribe_block_id) {
				$sql = "INSERT INTO ".$table_prefix."cms_pages_blocks ";
				$sql.= " (ps_id,frame_id,block_id,block_key,block_order) VALUES (";
				$sql.= $db->tosql($subscribe_ps_id, INTEGER).",";
				$sql.= $db->tosql(3, INTEGER).",";
				$sql.= $db->tosql($subscribe_block_id, INTEGER).",";
				$sql.= $db->tosql("", TEXT).",";
				$sql.= $db->tosql(1, INTEGER).")";
				$sqls[] = $sql;
			}
	  
			if ($footer_block_id) {
				$sql = "INSERT INTO ".$table_prefix."cms_pages_blocks ";
				$sql.= " (ps_id,frame_id,block_id,block_key,block_order) VALUES (";
				$sql.= $db->tosql($subscribe_ps_id, INTEGER).",";
				$sql.= $db->tosql(5, INTEGER).",";
				$sql.= $db->tosql($footer_block_id, INTEGER).",";
				$sql.= $db->tosql("", TEXT).",";
				$sql.= $db->tosql(1, INTEGER).")";
				$sqls[] = $sql;
			}

		}

		// unsubscribe page settings
		if (!$unsubscribe_ps_id) {
			$max_ps_id++;
			$unsubscribe_ps_id = $max_ps_id;

			$sql = "INSERT INTO ".$table_prefix."cms_pages_settings ";
			$sql.= " (ps_id,page_id,key_code,key_type,key_rule,layout_id,site_id) VALUES (";
			$sql.= $db->tosql($unsubscribe_ps_id, INTEGER).",";
			$sql.= $db->tosql($unsubscribe_page_id, INTEGER).",";
			$sql.= $db->tosql("", TEXT).",";
			$sql.= $db->tosql("", TEXT).",";
			$sql.= $db->tosql("", TEXT).",";
			$sql.= $db->tosql(1, INTEGER).",";
			$sql.= $db->tosql(1, INTEGER).")";
			$sqls[] = $sql;

			// added blocks for page
			if ($header_block_id) {
				$sql = "INSERT INTO ".$table_prefix."cms_pages_blocks ";
				$sql.= " (ps_id,frame_id,block_id,block_key,block_order) VALUES (";
				$sql.= $db->tosql($unsubscribe_ps_id, INTEGER).",";
				$sql.= $db->tosql(1, INTEGER).",";
				$sql.= $db->tosql($header_block_id, INTEGER).",";
				$sql.= $db->tosql("", TEXT).",";
				$sql.= $db->tosql(1, INTEGER).")";
				$sqls[] = $sql;
			}
	  
			if ($unsubscribe_block_id) {
				$sql = "INSERT INTO ".$table_prefix."cms_pages_blocks ";
				$sql.= " (ps_id,frame_id,block_id,block_key,block_order) VALUES (";
				$sql.= $db->tosql($unsubscribe_ps_id, INTEGER).",";
				$sql.= $db->tosql(3, INTEGER).",";
				$sql.= $db->tosql($unsubscribe_block_id, INTEGER).",";
				$sql.= $db->tosql("", TEXT).",";
				$sql.= $db->tosql(1, INTEGER).")";
				$sqls[] = $sql;
			}
	  
			if ($footer_block_id) {
				$sql = "INSERT INTO ".$table_prefix."cms_pages_blocks ";
				$sql.= " (ps_id,frame_id,block_id,block_key,block_order) VALUES (";
				$sql.= $db->tosql($unsubscribe_ps_id, INTEGER).",";
				$sql.= $db->tosql(5, INTEGER).",";
				$sql.= $db->tosql($footer_block_id, INTEGER).",";
				$sql.= $db->tosql("", TEXT).",";
				$sql.= $db->tosql(1, INTEGER).")";
				$sqls[] = $sql;
			}
		}

	// run SQL queries 
	echo "UPDATING...";
	foreach ($sqls as $id => $sql) {
		echo "<hr>".$sql;
		$db->query($sql);
	}

?>