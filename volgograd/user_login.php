<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_login.php                                           ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./messages/" . $language_code . "/forum_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/navigator.php");

	$cms_page_code = "user_login";
	$script_name   = "user_login.php";
	$current_page  = get_custom_friendly_url("user_login.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = LOGIN_TITLE;

	include_once("./includes/page_layout.php");

?>
