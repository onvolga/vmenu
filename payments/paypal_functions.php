<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  paypal_functions.php                                     ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


/*
 * PayPal functions by ViArt Ltd - http://www.viarts.ru/
 */

	function paypal_direct_payment($params)
	{
		global $token, $variables;

		$soap  = '<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope
   xmlns:xsi="http://www.w3.org/1999/XMLSchema-instance"
   xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/"
   xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
   xmlns:xsd="http://www.w3.org/1999/XMLSchema"
   SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">';//<?
		$soap .= '
    <SOAP-ENV:Header>
      <RequesterCredentials
        xmlns="urn:ebay:api:PayPalAPI"
        SOAP-ENV:mustUnderstand="1">
         <Credentials xmlns="urn:ebay:apis:eBLBaseComponents">
';
		if(isset($params["username"]) && strlen($params["username"])) {
			$soap .= "<Username>" . xml_escape_string($params["username"]) . "</Username>\r\n";
		} else {
			$soap .= "<Username/>\r\n";
		}
		if(isset($params["password"]) && strlen($params["password"])) {
			$soap .= "<Password>" . xml_escape_string($params["password"]) . "</Password>\r\n";
		} else {
			$soap .= "<Password/>\r\n";
		}
		if(isset($params["Subject"]) && strlen($params["Subject"])) {
			$soap .= "<Subject>" . xml_escape_string($params["subject"]) . "</Subject>";
		} else {
			$soap .= "<Subject/>";
		}
		$soap .= '
         </Credentials>
      </RequesterCredentials>
		</SOAP-ENV:Header>
		<SOAP-ENV:Body>
		<DoDirectPaymentReq xmlns="urn:ebay:api:PayPalAPI">
			<DoDirectPaymentRequest xmlns="urn:ebay:api:PayPalAPI">
				<Version xmlns="urn:ebay:apis:eBLBaseComponents">1.0</Version>
					<DoDirectPaymentRequestDetails xmlns="urn:ebay:apis:eBLBaseComponents">';
		if(isset($params["paymentaction"]) && strlen($params["paymentaction"])) {
			$soap .= "<PaymentAction>" . xml_escape_string($params["paymentaction"]) . "</PaymentAction>";
		} else {
			$soap .= "<PaymentAction>Sale</PaymentAction>";
		}

		$currencyID = (isset($params["currencyid"]) && strlen($params["currencyid"])) ? $params["currencyid"] : "USD";

		// check total values
		$order_total = $variables["order_total"];
		$goods_excl_tax = $variables["goods_excl_tax"];
		$goods_tax = $variables["goods_tax"];
		$properties_total_excl_tax = $variables["properties_total_excl_tax"];
		$processing_fee_excl_tax = $variables["processing_fee_excl_tax"];
		$total_discount_excl_tax = $variables["total_discount_excl_tax"];
		$shipping_cost_excl_tax = $variables["shipping_cost_excl_tax"];
		$shipments_total_excl_tax = isset($variables["shipments_total_excl_tax"]) ? $variables["shipments_total_excl_tax"] : 0;
		$tax_cost = $variables["tax_cost"];
		$credit_amount = $variables["credit_amount"];
		$paypal_itemtotal = $goods_excl_tax - $total_discount_excl_tax + $properties_total_excl_tax;

		$soap_pi = ""; $items_amount = 0; $items_tax = 0;
		foreach ($variables['items'] as $items_index => $items_array) {
			$item_amount = round($items_array["price_excl_tax"], 2);
			$item_tax = round($items_array["item_tax"], 2);
			$quantity = $items_array["quantity"];
			$items_amount += ($item_amount * $quantity);
			$items_tax += ($item_tax * $quantity);

			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string($items_array["item_name"]) . "</Name>";
			$soap_pi .= "<Number>" . xml_escape_string($items_array["item_code"]) . "</Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($item_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>" . xml_escape_string($items_array["quantity"]) . "</Quantity>";
			//$soap_pi .= '<SalesTax currencyID="' . $currencyID . '">' . xml_escape_string($item_tax) . "</SalesTax>\r\n";
			$soap_pi .= "</PaymentDetailsItem>";
		}
		// products price correction
		$correction_amount = round(($goods_excl_tax - $items_amount), 2);
		if ($correction_amount != 0) {
			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string("Correction") . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($correction_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			$soap_pi .= "</PaymentDetailsItem>";
		}

		foreach ($variables["properties"] as $number => $property) {
			$item_amount = round($property["property_price_excl_tax"], 2);
			$item_tax = round($property["property_tax"], 2);

			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string($property['property_name']) . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($item_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			//$soap_pi .= '<SalesTax currencyID="' . $currencyID . '">' . xml_escape_string($item_tax) . "</SalesTax>\r\n";
			$soap_pi .= "</PaymentDetailsItem>";
		}
		if ($variables["shipping_type_desc"]) {
			$item_amount = round($variables["shipping_cost_excl_tax"], 2);
			$item_tax = round($variables["shipping_tax"], 2);

			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string($variables["shipping_type_desc"]) . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($item_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			//$soap_pi .= '<SalesTax currencyID="' . $currencyID . '">' . xml_escape_string($item_tax) . "</SalesTax>\r\n";
			$soap_pi .= "</PaymentDetailsItem>";
		}
		// New Shipping Structure
		if (isset($variables["shipments"]) && is_array($variables["shipments"]) && sizeof($variables["shipments"]) > 0) {
			foreach ($variables["shipments"] as $shipment_id => $shipment) {
				$item_amount = round($shipment["shipping_cost_excl_tax"], 2);
				$item_tax = round($shipment["shipping_tax"], 2);
		  
				$soap_pi .= "<PaymentDetailsItem>";
				$soap_pi .= "<Name>" . xml_escape_string($shipment["shipping_desc"]) . "</Name>";
				$soap_pi .= "<Number>" . xml_escape_string($shipment["shipping_code"]) . "</Number>";
				$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($item_amount) . "</Amount>\r\n";
				$soap_pi .= "<Quantity>1</Quantity>";
				$soap_pi .= "</PaymentDetailsItem>";
			}
		}

		if ($variables["total_discount_excl_tax"] != 0) {
			$item_amount = round($variables["total_discount_excl_tax"], 2);
			$item_tax = round($variables["total_discount_tax"], 2);

			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string(TOTAL_DISCOUNT_MSG) . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string(-$item_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			//$soap_pi .= '<SalesTax currencyID="' . $currencyID . '">' . xml_escape_string(-$item_tax) . "</SalesTax>\r\n";
			$soap_pi .= "</PaymentDetailsItem>";
		}
		if ($processing_fee_excl_tax != 0) {
			$item_amount = round($processing_fee_excl_tax, 2);
			$item_tax = round($variables["processing_fee_tax"], 2);

			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string(TOTAL_DISCOUNT_MSG) . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($item_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			$soap_pi .= "</PaymentDetailsItem>";
		}

		if ($tax_cost) {
			$item_name = $variables["tax_name"];
			if (!$item_name) { $item_name = "Tax"; }

			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string($item_name) . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string($tax_cost) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			$soap_pi .= "</PaymentDetailsItem>";
		}

		if ($credit_amount > 0) {
			$soap_pi .= "<PaymentDetailsItem>";
			$soap_pi .= "<Name>" . xml_escape_string(CREDIT_AMOUNT_MSG) . "</Name>";
			$soap_pi .= "<Number></Number>";
			$soap_pi .= '<Amount currencyID="' . $currencyID . '">' . xml_escape_string(-$credit_amount) . "</Amount>\r\n";
			$soap_pi .= "<Quantity>1</Quantity>";
			$soap_pi .= "</PaymentDetailsItem>";
		}
		$item_total = $goods_excl_tax + $properties_total_excl_tax + $shipping_cost_excl_tax + $shipments_total_excl_tax + $processing_fee_excl_tax;
		$item_total -= $total_discount_excl_tax;
		$item_total += $tax_cost;
		$item_total -= $credit_amount;

		$soap .= "<PaymentDetails>";
		$soap .= '<OrderTotal currencyID="' . xml_escape_string($currencyID) . '">' . xml_escape_string(number_format($order_total, 2, ".", "")) . '</OrderTotal>';
		$soap .= '<ItemTotal currencyID="' . xml_escape_string($currencyID) . '">' . xml_escape_string(number_format($item_total, 2, ".", "")) . '</ItemTotal>';

		if(isset($params["orderdescription"]) && strlen($params["orderdescription"])) {
			$soap .= "<OrderDescription>" . xml_escape_string($params["orderdescription"]) . "</OrderDescription>\r\n";
		}
		if(isset($params["custom"]) && strlen($params["custom"])) {
			$soap .= "<Custom>" . xml_escape_string($params["custom"]) . "</Custom>\r\n";
		}
		if(isset($params["invoiceid"]) && strlen($params["invoiceid"])) {
			$soap .= "<InvoiceID>" . xml_escape_string($params["invoiceid"]) . "</InvoiceID>\r\n";
		}
		if(isset($params["buttonsource"]) && strlen($params["buttonsource"])) {
			$soap .= "<ButtonSource>" . xml_escape_string($params["buttonsource"]) . "</ButtonSource>\r\n";
		}

		$shipname = (isset($params["shipname"])) ? $params["shipname"] : "";
		$shipstreet1 = (isset($params["shipstreet1"])) ? $params["shipstreet1"] : "";
		$shipstreet2 = (isset($params["shipstreet2"])) ? $params["shipstreet2"] : "";
		$shipcityname = (isset($params["shipcityname"])) ? $params["shipcityname"] : "";
		$shipstateorprovince = (isset($params["shipstateorprovince"])) ? $params["shipstateorprovince"] : "";
		$shipcountry = (isset($params["shipcountry"])) ? $params["shipcountry"] : "";
		$shippostalcode = (isset($params["shippostalcode"])) ? $params["shippostalcode"] : "";
		if (strlen($shipname) || strlen($shipstreet1) || strlen($shipstreet2) || strlen($shipcityname) || strlen($shipstateorprovince) || strlen($shipcountry) || strlen($shippostalcode)) {
			$soap .= "<ShipToAddress>\r\n";
			if (!strlen($shipname)) {
				$ship_delivery_name = $variables["delivery_name"];
				$ship_delivery_first_name = $variables["delivery_first_name"];
				$ship_delivery_last_name = $variables["delivery_last_name"];
				if (strlen($ship_delivery_name)) {
					$shipname = $ship_delivery_name;
				} else if (strlen($ship_delivery_first_name) || strlen($ship_delivery_last_name)) {
					$shipname = trim($ship_delivery_first_name . " " . $ship_delivery_last_name);
				}
			}
			if (strlen($shipname)) { $soap .= "<Name>" . xml_escape_string($shipname) . "</Name>\r\n"; }
			if (strlen($shipstreet1)) { $soap .= "<Street1>" . xml_escape_string($shipstreet1) . "</Street1>\r\n"; }
			if (strlen($shipstreet2)) { $soap .= "<Street2>" . xml_escape_string($shipstreet2) . "</Street2>\r\n"; }
			if (strlen($shipcityname)) { $soap .= "<CityName>" . xml_escape_string($shipcityname) . "</CityName>\r\n"; }
			if (!strlen($shipstateorprovince) && (strtoupper($shipcountry) == "GB" || strtoupper($shipcountry) == "CA")) {
				$shipstateorprovince = "N/A";
			}
			if (strlen($shipstateorprovince)) { $soap .= "<StateOrProvince>" . xml_escape_string($shipstateorprovince) . "</StateOrProvince>\r\n"; }
			if (strlen($shipcountry)) { $soap .= "<Country>" . xml_escape_string($shipcountry) . "</Country>\r\n"; }
			if (strlen($shippostalcode)) { $soap .= "<PostalCode>" . xml_escape_string($shippostalcode) . "</PostalCode>\r\n"; }
			$soap .= "</ShipToAddress>\r\n";
		}

		$soap .= $soap_pi;

		$soap .= "</PaymentDetails>";
		$soap .= "<CreditCard>";
		if(isset($params["creditcardtype"]) && strlen($params["creditcardtype"])) {
			$cc_type = $params["creditcardtype"];
			$cc_types = array ("visa" => "Visa", "mc" => "MasterCard", "mastercard" => "MasterCard", "discover" => "Discover",
			"amex" => "Amex", "american express" => "Amex", "americanexpress" => "Amex", "switch" => "Switch", "solo" => "Solo");
			if (isset($cc_types[strtolower($cc_type)])) {
				$cc_type = $cc_types[strtolower($cc_type)];
			}
			$soap .= "<CreditCardType>" . xml_escape_string($cc_type) . "</CreditCardType>\r\n";
		}

		if(isset($params["creditcardnumber"]) && strlen($params["creditcardnumber"])) {
			$soap .= "<CreditCardNumber>" . xml_escape_string($params["creditcardnumber"]) . "</CreditCardNumber>\r\n";
		}
		if(isset($params["expmonth"]) && strlen($params["expmonth"])) {
			$soap .= "<ExpMonth>" . xml_escape_string($params["expmonth"]) . "</ExpMonth>\r\n";
		}
		if(isset($params["expyear"]) && strlen($params["expyear"])) {
			$soap .= "<ExpYear>" . xml_escape_string($params["expyear"]) . "</ExpYear>\r\n";
		}
		$soap .= "<CardOwner>";
		if(isset($params["payer"]) && strlen($params["payer"])) {
			$soap .= "<Payer>" . xml_escape_string($params["payer"]) . "</Payer>\r\n";
		}

		$soap .= "<PayerName>";
		if(isset($params["ccsalutation"]) && strlen($params["ccsalutation"])) {
			$soap .= "<Salutation>" . xml_escape_string($params["ccsalutation"]) . "</Salutation>\r\n";
		}
		if(isset($params["ccfirstname"]) && strlen($params["ccfirstname"])) {
			$soap .= "<FirstName>" . xml_escape_string($params["ccfirstname"]) . "</FirstName>\r\n";
		}
		if(isset($params["ccmiddlename"]) && strlen($params["ccmiddlename"])) {
			$soap .= "<MiddleName>" . xml_escape_string($params["ccmiddlename"]) . "</MiddleName>\r\n";
		}
		if(isset($params["cclastname"]) && strlen($params["cclastname"])) {
			$soap .= "<LastName>" . xml_escape_string($params["cclastname"]) . "</LastName>\r\n";
		}
		if(isset($params["ccsuffix"]) && strlen($params["ccsuffix"])) {
			$soap .= "<Suffix>" . xml_escape_string($params["ccsuffix"]) . "</Suffix>\r\n";
		}
		$soap .= "</PayerName>";

		$name = (isset($params["name"])) ? $params["name"] : "";
		$street1 = (isset($params["street1"])) ? $params["street1"] : "";
		$street2 = (isset($params["street2"])) ? $params["street2"] : "";
		$cityname = (isset($params["cityname"])) ? $params["cityname"] : "";
		$stateorprovince = (isset($params["stateorprovince"])) ? $params["stateorprovince"] : "";
		$country = (isset($params["country"])) ? $params["country"] : "";
		$postalcode = (isset($params["postalcode"])) ? $params["postalcode"] : "";
		if (strlen($name) || strlen($street1) || strlen($street2) || strlen($cityname) || strlen($stateorprovince) || strlen($country) || strlen($postalcode)) {
			$soap .= "<Address>\r\n";
			if (strlen($name)) { $soap .= "<Name>" . xml_escape_string($name) . "</Name>\r\n"; }
			if (strlen($street1)) { $soap .= "<Street1>" . xml_escape_string($street1) . "</Street1>\r\n"; }
			if (strlen($street2)) { $soap .= "<Street2>" . xml_escape_string($street2) . "</Street2>\r\n"; }
			if (strlen($cityname)) { $soap .= "<CityName>" . xml_escape_string($cityname) . "</CityName>\r\n"; }
			if (strlen($stateorprovince)) { $soap .= "<StateOrProvince>" . xml_escape_string($stateorprovince) . "</StateOrProvince>\r\n"; }
			if (strlen($country)) { $soap .= "<Country>" . xml_escape_string($country) . "</Country>\r\n"; }
			if (strlen($postalcode)) { $soap .= "<PostalCode>" . xml_escape_string($postalcode) . "</PostalCode>\r\n"; }
			$soap .= "</Address>\r\n";
		}

		$soap .= "</CardOwner>";
		if(isset($params["cvv2"]) && strlen($params["cvv2"])) {
			$soap .= "<CVV2>" . xml_escape_string($params["cvv2"]) . "</CVV2>\r\n";
		}
		if(isset($params["startmonth"]) && strlen($params["startmonth"])) {
			$soap .= "<StartMonth>" . xml_escape_string($params["startmonth"]) . "</StartMonth>\r\n";
		}
		if(isset($params["startyear"]) && strlen($params["startyear"])) {
			$soap .= "<StartYear>" . xml_escape_string($params["startyear"]) . "</StartYear>\r\n";
		}
		if(isset($params["issuenumber"]) && strlen($params["issuenumber"])) {
			$soap .= "<IssueNumber>" . xml_escape_string($params["issuenumber"]) . "</IssueNumber>\r\n";
		}

		$soap .= "</CreditCard>";

		if(isset($params["ipaddress"]) && strlen($params["ipaddress"])) {
			$soap .= "<IPAddress>" . xml_escape_string($params["ipaddress"]) . "</IPAddress>\r\n";
		} else {
			$soap .= "<IPAddress>" . get_ip() . "</IPAddress>\r\n";
		}
		$soap .= "<MerchantSessionId>" . session_id() . "</MerchantSessionId>\r\n";

		$soap .= "
				</DoDirectPaymentRequestDetails>
			</DoDirectPaymentRequest>
		</DoDirectPaymentReq>
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>";

		return $soap;
	}

?>