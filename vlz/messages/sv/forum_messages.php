<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "Trеdinformation");
	define("TOPIC_MESSAGE_TITLE", "Meddelande");

	define("MY_FORUM_TOPICS_MSG", "Mina forumtrеdar");
	define("ALL_FORUM_TOPICS_MSG", "Alla forumtrеdar");
	define("MY_FORUM_TOPICS_DESC", "Har du nеgonsin undrat om nеgon annan har upplevt samma problem som du har? Skulle du vilja berдtta om dina erfarenheter fцr andra anvдndare? Varfцr inte bli forumanvдndare och bli delaktig i webbplatsen?");
	define("NEW_TOPIC_MSG", "Ny trеd");
	define("NO_TOPICS_MSG", "Inga trеdar funna");
	define("FOUND_TOPICS_MSG", "Vi har hittat <b>{found_records}</b> forumtrеdar som matchar sцkterm(erna) '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Inga forum hittades");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "Дmne");
	define("FORUM_REPLIES_COLUMN", "Svar");
	define("FORUM_LAST_POST_COLUMN", "Senast uppdaterad");
	define("FORUM_MODERATORS_MSG", "Moderatorer");

	define("TOPIC_NAME_COLUMN", "Дmne");
	define("TOPIC_AUTHOR_COLUMN", "Trеdstartare");
	define("TOPIC_VIEWS_COLUMN", "Visad");
	define("TOPIC_REPLIES_COLUMN", "Svar");
	define("TOPIC_UPDATED_COLUMN", "Senast uppdaterad");
	define("TOPIC_ADDED_MSG", "Tack!<br>Din trеd har lagts till");

	define("TOPIC_ADDED_BY_FIELD", "Startad av");
	define("TOPIC_ADDED_DATE_FIELD", "Startad");
	define("TOPIC_UPDATED_FIELD", "Senast uppdaterad");
	define("TOPIC_NICKNAME_FIELD", "Anvдndarnamn");
	define("TOPIC_EMAIL_FIELD", "Din epostadress");
	define("TOPIC_NAME_FIELD", "Дmne");
	define("TOPIC_MESSAGE_FIELD", "Meddelande");
	define("TOPIC_NOTIFY_FIELD", "Skicka alla svar till min epost");

	define("ADD_TOPIC_BUTTON", "Ny trеd");
	define("TOPIC_MESSAGE_BUTTON", "Nytt meddelande");

	define("TOPIC_MISS_ID_ERROR", "Saknar <b>trеd-ID</b> parameter.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Trеd-ID</b> parameter har fel vдrde.");
	define("FORUM_SEARCH_MESSAGE", "Vi har hittat {search_count} meddelanden som matchar termen '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Fцrhandsvisa");
	define("TOPIC_SAVE_BUTTON", "Spara");

	define("LAST_POST_ON_SHORT_MSG", "Pе:");
	define("LAST_POST_IN_SHORT_MSG", "I:");
	define("LAST_POST_BY_SHORT_MSG", "Av:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Senast дndrad:");

?>