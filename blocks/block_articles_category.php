<?php

	$default_title = "{category_name}";

	$desc_image    = get_setting_value($vars, "articles_category_image", 3);
	$desc_type     = get_setting_value($vars, "articles_category_desc_type", 2);

	$category_name = get_translation(get_setting_value($category_info, "category_name"));
	$category_image = ""; $category_image_alt = "";
	if ($desc_image == 3) {
		$category_image = get_setting_value($category_info, "image_large");
		$category_image_alt = get_translation(get_setting_value($category_info, "image_large_alt"));
	} elseif ($desc_image == 2) {
		$category_image = get_setting_value($category_info, "image_small");
		$category_image_alt = get_translation(get_setting_value($category_info, "image_small_alt"));
	}
	$category_description = "";
	if ($desc_type == 2) {
		$category_description = get_translation(get_setting_value($category_info, "full_description"));
	} elseif ($desc_type == 1) {
		$category_description = get_translation(get_setting_value($category_info, "short_description"));
	}

	if(strlen($category_description) || $category_image)
	{
		$html_template = get_setting_value($block, "html_template", "block_category_description.html"); 
	  $t->set_file("block_body", $html_template);

		if (strlen($category_image)) {
			if (preg_match("/^http\:\/\//", $category_image)) {
				$image_size = "";
			} else {
				$image_size = @GetImageSize($category_image);
				if (isset($restrict_categories_images) && $restrict_categories_images) { $category_image = "image_show.php?art_cat_id=".$category_id."&type=large"; }
			}
			if (!strlen($category_image_alt)) { $category_image_alt = $category_name; }
			$t->set_var("alt", htmlspecialchars($category_image_alt));
			$t->set_var("src", htmlspecialchars($category_image));
			if(is_array($image_size)) {
				$t->set_var("width", "width=\"" . $image_size[0] . "\"");
				$t->set_var("height", "height=\"" . $image_size[1] . "\"");
			} else {
				$t->set_var("width", "");
				$t->set_var("height", "");
			}
			$t->sparse("image_large_block", false);
		} else {
			$t->set_var("image_large_block", "");
		}

		$t->set_var("category_name", $category_name);
		$t->set_var("full_description", $category_description);

		$block_parsed = true;
	}

?>