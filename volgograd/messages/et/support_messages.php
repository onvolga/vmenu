<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Tugikeskus");
	define("SUPPORT_REQUEST_INF_TITLE", "PРґringu info");
	define("SUPPORT_REPLY_FORM_TITLE", "Vasta");

	define("MY_SUPPORT_ISSUES_MSG", "Minu toe pРґringud");
	define("MY_SUPPORT_ISSUES_DESC", "Kui Teil esineb mingeid probleeme ostetud toodetega, on meie klienditugi valmis aitama. Toe pРґringu saatmiseks kliki СЊlaltoodud lingile.");
	define("NEW_SUPPORT_REQUEST_MSG", "Uus pРґring");
	define("SUPPORT_REQUEST_ADDED_MSG", "AitРґh<br>Meie klienditugid СЊritab Teid aidata nii kiiresti kui vС…imalik.");
	define("SUPPORT_SELECT_DEP_MSG", "Vali osakond");
	define("SUPPORT_SELECT_PROD_MSG", "Vali toode");
	define("SUPPORT_SELECT_STATUS_MSG", "Vali staatus");
	define("SUPPORT_NOT_VIEWED_MSG", "Ei ole vaadatud");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Kasutaja poolt vaadatud");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Administraatori poolt vaadatud");
	define("SUPPORT_STATUS_NEW_MSG", "Uus");
	define("NO_SUPPORT_REQUEST_MSG", "Probleeme ei leitud");

	define("SUPPORT_SUMMARY_COLUMN", "KokkuvС…te");
	define("SUPPORT_TYPE_COLUMN", "TСЊСЊp");
	define("SUPPORT_UPDATED_COLUMN", "Viimati uuendatud");

	define("SUPPORT_USER_NAME_FIELD", "Sinu nimi");
	define("SUPPORT_USER_EMAIL_FIELD", "Sinu e-postiaadress");
	define("SUPPORT_IDENTIFIER_FIELD", "Identifikaator (arve #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Keskkond (OS, Database, Web Server jne)");
	define("SUPPORT_DEPARTMENT_FIELD", "Osakond");
	define("SUPPORT_PRODUCT_FIELD", "Toode");
	define("SUPPORT_TYPE_FIELD", "TСЊСЊp");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Hetkestaatus");
	define("SUPPORT_SUMMARY_FIELD", "Р¬herealine kokkuvС…te");
	define("SUPPORT_DESCRIPTION_FIELD", "Kirjeldus");
	define("SUPPORT_MESSAGE_FIELD", "Teade");
	define("SUPPORT_ADDED_FIELD", "Lisatud");
	define("SUPPORT_ADDED_BY_FIELD", "Lisanud");
	define("SUPPORT_UPDATED_FIELD", "Viimati uuendatud");

	define("SUPPORT_REQUEST_BUTTON", "Esita pРґring");
	define("SUPPORT_REPLY_BUTTON", "Vasta");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Puuduv <b>toe ID</b> parameeter");
	define("SUPPORT_MISS_CODE_ERROR", "Puuduv <b>kinnitamise</b> parameeter");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Toe ID</b> parameetril on vale vРґРґrtus");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Kinnitamise</b> parameetril on vale vРґРґrtus");

	define("MAIL_DATA_MSG", "E-posti andmed");
	define("HEADERS_MSG", "PРґised");
	define("ORIGINAL_TEXT_MSG", "Originaaltekst");
	define("ORIGINAL_HTML_MSG", "Originaal   HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Vabandame, kuid sul ei ole lubatud sulgeda ticket'eid.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Vabandame, kuid sul ei ole lubatud vastata ticket'itele.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Vabandame, kuid sul ei ole lubatud luua uusi ticket'eid.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Vabandame, kuid sul ei ole lubatud eemaldada ticket'eid.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Vabandame, kuid sul ei ole lubatud uuendata ticket'eid.<br>");
	define("NO_TICKETS_FOUND_MSG", "Ticket'eid ei leitud.");
	define("HIDDEN_TICKETS_MSG", "Peidetud ticket'id");
	define("ALL_TICKETS_MSG", "KС…ik ticket'id");
	define("ACTIVE_TICKETS_MSG", "Aktiivsed ticket'id");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "Sind ei ole mРґРґratud sellesse osakonda.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "Sind ei ole mРґРґratud СЊhessegi osakonda.");
	define("REPLY_TO_NAME_MSG", "Vasta isikule {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge kategooria");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge pealkiri");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge artikli staatus");
	define("SELECT_RESPONSIBLE_MSG", "Vali vastutav");

?>