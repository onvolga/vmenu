<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  admin_order_shipping.php                                 ***
  ***      Built: Tue Nov  6 17:18:04 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/shipping_functions.php");
	include_once($root_folder_path . "includes/order_items.php");
	include_once($root_folder_path . "includes/order_recalculate.php");
	include_once($root_folder_path . "messages/" . $language_code . "/download_messages.php");
	include_once("./admin_common.php");

	check_admin_security("sales_orders");
	$order_id = get_param("order_id");
	$order_shipping_id = get_param("order_shipping_id");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main", "admin_order_shipping.html");
	$t->set_var("order_id", htmlspecialchars($order_id));
	$t->set_var("order_shipping_id", htmlspecialchars($order_shipping_id));


	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->set_var("admin_order_href", "admin_order.php");
	$t->set_var("admin_order_shipping_href", "admin_order_shipping.php");


	$operation = get_param("operation");
	if (strlen($operation)) {
		// update shipping tracking information
		$shipping_tracking_id = get_param("shipping_tracking_id");
		if (strlen($shipping_tracking_id)) {
			if ($order_shipping_id) {
				$sql  = " UPDATE " . $table_prefix . "orders_shipments SET tracking_id=" . $db->tosql($shipping_tracking_id, TEXT);
				$sql .= " WHERE order_shipping_id=" . $db->tosql($order_shipping_id, INTEGER);
			} else {
				$sql  = " UPDATE " . $table_prefix . "orders SET shipping_tracking_id=" . $db->tosql($shipping_tracking_id, TEXT);
				$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
			}
			$db->query($sql);

			// add shipping tracking event
			$oe = new VA_Record($table_prefix . "orders_events");
			$oe->add_textbox("order_id", INTEGER);
			$oe->add_textbox("status_id", INTEGER);
			$oe->add_textbox("admin_id", INTEGER);
			$oe->add_textbox("order_items", TEXT);
			$oe->add_textbox("event_date", DATETIME);
			$oe->add_textbox("event_type", TEXT);
			$oe->add_textbox("event_name", TEXT);
			$oe->add_textbox("event_description", TEXT);
			$oe->set_value("order_id", $order_id);
			$oe->set_value("admin_id", get_session("session_admin_id"));
			$oe->set_value("event_date", va_time());
			$oe->set_value("event_type", "update_shipping_tracking");
			//$oe->set_value("event_type", "remove_shipping_tracking");
			$oe->set_value("event_name", $shipping_tracking_id);
			$oe->insert_record();
		}
		// check custom shipping
		$shipping_type_id = get_param("shipping_type_id");
		$shipping_cost = get_param("shipping_cost");
		$custom_shipping_type = get_param("custom_shipping_type");
		$custom_shipping_cost = get_param("custom_shipping_cost");
		if (strlen($custom_shipping_type) && strlen($custom_shipping_cost)) {
			update_order_shipping($order_id, $order_shipping_id, "", $custom_shipping_type, $custom_shipping_cost);
		} else if (strlen($shipping_type_id) && strlen($shipping_cost)) {
			update_order_shipping($order_id, $order_shipping_id, $shipping_type_id, "", $shipping_cost);
		}

		$t->set_var("onload_js", "reloadParentWin();");
	}

	// get general order data 
	$sql  = " SELECT site_id, user_type_id, country_id, state_id, zip, ";
	$sql .= " delivery_country_id, delivery_state_id, delivery_zip, ";
	$sql .= " weight_total, shipping_type_id, shipping_type_desc, shipping_cost, shipping_tracking_id  ";
	$sql .= " FROM " . $table_prefix . "orders ";
	$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$delivery_site_id = $db->f("site_id");
		$user_type_id = $db->f("user_type_id");
		$delivery_country_id = $db->f("delivery_country_id");
		$delivery_state_id = $db->f("delivery_state_id");
		$delivery_postal_code = $db->f("delivery_zip");
		if (!strlen($delivery_country_id)) {
			$delivery_country_id = $db->f("country_id");
			$delivery_state_id = $db->f("state_id");
			$delivery_postal_code = $db->f("zip");
		}
		if ($order_shipping_id) {
			$sql  = " SELECT * FROM " . $table_prefix . "orders_shipments ";
			$sql .= " WHERE order_shipping_id=" . $db->tosql($order_shipping_id, INTEGER);
			$db->query($sql);
			if ($db->next_record()) {
				$cur_weight_total = $db->f("goods_weight") + $db->f("tare_weight");
				$cur_shipping_type_id = $db->f("shipping_id");
				$cur_shipping_type_desc = $db->f("shipping_desc");
				$cur_shipping_cost = $db->f("shipping_cost");
				$cur_shipping_tracking_id = $db->f("tracking_id");
			}
		} else {
			$cur_weight_total = $db->f("weight_total");
			$cur_shipping_type_id = $db->f("shipping_type_id");
			$cur_shipping_type_desc = $db->f("shipping_type_desc");
			$cur_shipping_cost = $db->f("shipping_cost");
			$cur_shipping_tracking_id = $db->f("shipping_tracking_id");
		}
	} else {
		echo ERRORS_MSG;
		exit;
	}





	$delivery_items = array();	
	$total_quantity = 0; $total_weight = 0; $total_packages = 0; $total_shipping_cost = 0;
	$sql  = " SELECT oi.*, ";
	$sql .= " i.shipping_modules_default, i.shipping_modules_ids ";
	$sql .= " FROM (" . $table_prefix . "orders_items oi ";
	$sql .= " LEFT JOIN " . $table_prefix . "items i ON i.item_id=oi.item_id) ";
	if (strlen($order_shipping_id)) {
		$sql .= " WHERE oi.order_id=" . $db->tosql($order_id, INTEGER);
		//$sql .= " WHERE oi.order_shipping_id=" . $db->tosql($order_shipping_id, INTEGER);
	} else {
		$sql .= " WHERE oi.order_id=" . $db->tosql($order_id, INTEGER);
	}
	$db->query($sql);
	while ($db->next_record()) {

		$item_id = $db->f("item_id");
		$item_name = get_translation($db->f("item_name"));
		$price = $db->f("price");
		$quantity = $db->f("quantity");
		$packages_number = $db->f("packages_number");
		if ($packages_number <= 0) { $packages_number = 0.1; }
		$weight = $db->f("weight");
		$width = $db->f("width");
		$height = $db->f("height");
		$length = $db->f("length");
		$is_shipping_free = $db->f("is_shipping_free");
		$shipping_cost = $db->f("shipping_cost");
		$shipping_modules_default = 0;
		$shipping_modules_ids = "";
		// TODO: probably apply new multi-shipping rules 
		//$shipping_modules_default = $db->f("shipping_modules_default");
		//$shipping_modules_ids = $db->f("shipping_modules_ids");

		// populate delivery items to calculate cost
		$delivery_items[] = array(
			"item_id" => $item_id,
			"price" => $price,
			"quantity" => $quantity,
			"packages_number" => $packages_number,
			"weight" => $weight,
			"width" => $width,
			"height" => $height,
			"length" => $length,
			"is_shipping_free" => $is_shipping_free,
			"shipping_cost" => $shipping_cost,
			"shipping_modules_default" => $shipping_modules_default,
			"shipping_modules_ids" => $shipping_modules_ids,
		);

		// calculate weight, cost and packages for total quantity to show this info per product
		$packages_number *= $quantity;
		$weight *= $quantity;
		$shipping_cost *= $quantity;
		
		$total_quantity += $quantity;
		$total_weight += $weight; 
		$total_packages += $packages_number; 
		$total_shipping_cost += $shipping_cost;

		$t->set_var("item_name", htmlspecialchars($item_name));
		$t->set_var("price", ($price));
		$t->set_var("quantity", $quantity);
		$t->set_var("packages_number", round($packages_number, 4));
		$t->set_var("weight", round($weight, 4));
		$t->set_var("width", round($width, 4));
		$t->set_var("height", round($height, 4));
		$t->set_var("length", round($length, 4));
		if ($is_shipping_free) {
			$t->set_var("shipping_cost", FREE_SHIPPING_MSG);
		} else {
			$t->set_var("shipping_cost", currency_format($shipping_cost));
		}
	      
		$t->parse("order_packages", true);
	}

	$t->set_var("total_quantity", intval($total_quantity));
	$t->set_var("total_weight", round($total_weight, 4));
	$t->set_var("total_packages", round($total_packages, 4));
	$t->set_var("total_shipping_cost", currency_format($total_shipping_cost));

	// parse current shipping method and tracking number
	if (strlen($cur_shipping_type_desc) || $cur_shipping_cost > 0) {
		if (!strlen($cur_shipping_type_desc)) {	
			$cur_shipping_type_desc = PROD_SHIPPING_MSG;
		}
		if ($cur_shipping_cost > 0) {	
			$cur_shipping_type_desc .= " (". currency_format($cur_shipping_cost) .")";
		}
	} else {
		$cur_shipping_type_desc = NO_SHIPPING_MSG;
	}
	if (!strlen($cur_shipping_tracking_id)) {
		$cur_shipping_tracking_id = NOT_AVAILABLE_MSG;
	}
	$t->set_var("cur_shipping_type_desc", $cur_shipping_type_desc);
	$t->set_var("cur_shipping_tracking_id", $cur_shipping_tracking_id);
	$t->set_var("currency_left", $currency["left"]);
	$t->set_var("currency_right", $currency["right"]);

	// check predefined shipping methods
	$shipping_groups = get_shipping_types($delivery_country_id, $delivery_state_id, $delivery_postal_code, $delivery_site_id, $user_type_id, $delivery_items);
	if (is_array($shipping_groups) && sizeof($shipping_groups) > 0) {
		foreach ($shipping_groups as $group_id => $shipping_group) {
			$shipping_types = $shipping_group["types"]; // get shipping types
			// parse empty shipping
			$t->set_var("shipping_type_id", "");
			$t->set_var("shipping_type_desc", "");
			$t->parse("shipping_types", true);
			for ($i = 0; $i < sizeof($shipping_types); $i++) {
				$shipping_type_id = $shipping_types[$i][0];
				$shipping_type_desc = $shipping_types[$i][2];
				$shipping_cost = $shipping_types[$i][3];
				if ($shipping_cost > 0) {
					$shipping_type_desc .= " (" . currency_format($shipping_cost) . ")";
				} else {
				}
				$t->set_var("shipping_type_id", $shipping_type_id);
				$t->set_var("shipping_type_desc", $shipping_type_desc);
				$t->set_var("shipping_cost", $shipping_cost);
				$t->parse("shipping_cost_values", true);
				$t->parse("shipping_types", true);
			}
			$t->parse("predefined_shipping_types", false);
		}
	}

	$errors = "";

	if (strlen($errors)) {
		$t->set_var("after_upload", "");
		$t->set_var("errors_list", $errors);
		$t->parse("errors", false);
	} else {
		$t->set_var("errors", "");
	}

	$t->pparse("main");

?>