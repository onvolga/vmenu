<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_product_registration.php                            ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/editgrid.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./includes/friendly_functions.php");
	include_once("./includes/registration_functions.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	
	check_user_security("access_product_registration");

	$cms_page_code = "user_product_registration";
	$script_name   = "user_product_registration.php";
	$current_page  = get_custom_friendly_url("user_product_registration.php");
	$auto_meta_title = REGISTER_PRODUCT_MSG;

	include_once("./includes/page_layout.php");

?>
