<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// съобщения свързани с поддръжката
	define("SUPPORT_TITLE", "Център за поддръжка");
	define("SUPPORT_REQUEST_INF_TITLE", "Изисквана информация");
	define("SUPPORT_REPLY_FORM_TITLE", "Отговор");

	define("MY_SUPPORT_ISSUES_MSG", "Моите молби за помощ.");
	define("MY_SUPPORT_ISSUES_DESC", "Ако изпитвате някакви проблеми с продуктите, които сте закупили, нашият център за поддръжка е готов да ви помогне. Чувствайте се свободни да пуснете молба за поддръжка, като кликнете на линка по-долу.");
	define("NEW_SUPPORT_REQUEST_MSG", "Ново запитване");
	define("SUPPORT_REQUEST_ADDED_MSG", "Благодарим ви<br>Нашият център за поддръжка ще се опита да ви помогне веднага щом има възможност.");
	define("SUPPORT_SELECT_DEP_MSG", "Изберете раздел");
	define("SUPPORT_SELECT_PROD_MSG", "Изберете продукт");
	define("SUPPORT_SELECT_STATUS_MSG", "Изберете статут");
	define("SUPPORT_NOT_VIEWED_MSG", "Не прегледани");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Прегледани от потребител");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Прегледани от администратор");
	define("SUPPORT_STATUS_NEW_MSG", "Ново");
	define("NO_SUPPORT_REQUEST_MSG", "Не са намерени резултати");

	define("SUPPORT_SUMMARY_COLUMN", "Кратко описание");
	define("SUPPORT_TYPE_COLUMN", "Тип");
	define("SUPPORT_UPDATED_COLUMN", "Последно обновен");

	define("SUPPORT_USER_NAME_FIELD", "Вашето име");
	define("SUPPORT_USER_EMAIL_FIELD", "Вашият e-mail адрес");
	define("SUPPORT_IDENTIFIER_FIELD", "Идентификатор (Фактура #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Обстановка (Операционна система, База данни, Уеб сървър и др.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Раздел");
	define("SUPPORT_PRODUCT_FIELD", "Продукт");
	define("SUPPORT_TYPE_FIELD", "Тип");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Текущ статут");
	define("SUPPORT_SUMMARY_FIELD", "Заглавие");
	define("SUPPORT_DESCRIPTION_FIELD", "Описание");
	define("SUPPORT_MESSAGE_FIELD", "Съобщение");
	define("SUPPORT_ADDED_FIELD", "Добавено");
	define("SUPPORT_ADDED_BY_FIELD", "Добавено от");
	define("SUPPORT_UPDATED_FIELD", "Последно обновено");

	define("SUPPORT_REQUEST_BUTTON", "Изпрати запитването");
	define("SUPPORT_REPLY_BUTTON", "Отговор");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Пропуснат <b>Support ID</b> параметър");
	define("SUPPORT_MISS_CODE_ERROR", "Пропуснат <b>Verification</b> параметър");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Support ID</b> параметъра има грешна стойност");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verification</b> параметъра има грешна стойност");

	define("MAIL_DATA_MSG", "Пощенски данни");
	define("HEADERS_MSG", "Заглавия");
	define("ORIGINAL_TEXT_MSG", "Оригинален текст");
	define("ORIGINAL_HTML_MSG", "Оригинален HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Съжаляваме, но не ви е позволено да затваряте билети.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Съжаляваме, но не ви е позволено да отговаряте на билети.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Съжаляваме, но не ви е позволено да създавате нови билети.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Съжаляваме, но не ви е позволено да премахвате билети.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Съжаляваме, но не ви е позволено да обновявате билети.<br>");
	define("NO_TICKETS_FOUND_MSG", "Не бяха намерени билети.");
	define("HIDDEN_TICKETS_MSG", "Скрити билети");
	define("ALL_TICKETS_MSG", "Всички билети");
	define("ACTIVE_TICKETS_MSG", "Активни билети");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "Не сте назначени към този отдел.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "Не сте назначен към никой отдел.");
	define("REPLY_TO_NAME_MSG", "Отговори на  {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Категория Знания");
	define("KNOWLEDGE_TITLE_MSG", "Заглавие на Знанията");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Статус на Статията за Знания");
	define("SELECT_RESPONSIBLE_MSG", "Избери отговорно");

?>