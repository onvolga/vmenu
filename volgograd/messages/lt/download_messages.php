<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// siuntimш юinutлs
	define("DOWNLOAD_WRONG_PARAM", "Blogas(i) siuntimo parametras(ai)");
	define("DOWNLOAD_MISS_PARAM", "Trыkstamas(i) siuntimo parametras(ai)");
	define("DOWNLOAD_INACTIVE", "Siuntimas neaktyvus.");
	define("DOWNLOAD_EXPIRED", "Jыsш siuntimo laikas baigлsi.");
	define("DOWNLOAD_LIMITED", "Jыs virрijote didюiausiа siuntimш skaiиiш.");
	define("DOWNLOAD_PATH_ERROR", "Kelias iki prekлs nerastas.");
	define("DOWNLOAD_RELEASE_ERROR", "Leidimas nerastas.");
	define("DOWNLOAD_USER_ERROR", "Tik registruoti vartotojai gali siшstis рiа bylа.");
	define("ACTIVATION_OPTIONS_MSG", "Aktyvacijos parinktys");
	define("ACTIVATION_MAX_NUMBER_MSG", "Didюiausias aktyvacijш kiekis");
	define("DOWNLOAD_OPTIONS_MSG", "Siunиiamш / Programiniш Parinktys");
	define("DOWNLOADABLE_MSG", "Siunиiamas (Programinis)");
	define("DOWNLOADABLE_DESC", "siunиiamam gaminiui jыs galite nurodyti \"Siuntimosi periodа\", \"Keliа iki siunиiamos bylos\" ir \"Aktyvavimo Parinktis\"");
	define("DOWNLOAD_PERIOD_MSG", "Siuntimosi periodas");
	define("DOWNLOAD_PATH_MSG", "Kelias iki siuntimosi bylos");
	define("DOWNLOAD_PATH_DESC", "Jыs galite pridлti daug keliш atskirtш kabliataрkiais");
	define("UPLOAD_SELECT_MSG", "Rinkis bylа nusiшsti ir spausk {button_name} mygtukа.");
	define("UPLOADED_FILE_MSG", "Byla <b>{filename}</b> buvo uюkrautas.");
	define("UPLOAD_SELECT_ERROR", "Praрome pirmiau rinktis bylа.");
	define("UPLOAD_IMAGE_ERROR", "Tik vaizdш bylos leidюiamos.");
	define("UPLOAD_FORMAT_ERROR", "Рis bylos tipas neleidюiamas.");
	define("UPLOAD_SIZE_ERROR", "Bylos didesnлs kaip {filesize} neleidюiamos.");
	define("UPLOAD_DIMENSION_ERROR", "Vaizdai didesni kaip {dimension} neleidюiami.");
	define("UPLOAD_CREATE_ERROR", "Sistema negali sukurti bylos.");
	define("UPLOAD_ACCESS_ERROR", "Jыs neturite teisiш uюkrauti bylш.");
	define("DELETE_FILE_CONFIRM_MSG", "Ar jыs tikrai norite trinti рiа bylа?");
	define("NO_FILES_MSG", "Bylш nerasta");
	define("SERIAL_GENERATE_MSG", "Generuok serijinб numerб");
	define("SERIAL_DONT_GENERATE_MSG", "Negeneruok");
	define("SERIAL_RANDOM_GENERATE_MSG", "Generuok atsitiktinб serijinб nr programiniam gaminiui");
	define("SERIAL_FROM_PREDEFINED_MSG", "gauk serijinб nr iр prieрnustatyto sаraрo");
	define("SERIAL_PREDEFINED_MSG", "Prieрnustatyti serijiniai numeriai");
	define("SERIAL_NUMBER_COLUMN", "Serijinis numeris");
	define("SERIAL_USED_COLUMN", "Naudotas");
	define("SERIAL_DELETE_COLUMN", "Trink");
	define("SERIAL_MORE_MSG", "Pridлti daugiau serijiniш numeriш?");
	define("SERIAL_PERIOD_MSG", "Serial Number Period");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Show Terms & Conditions");
	define("DOWNLOAD_SHOW_TERMS_DESC", "To download the product user has to read and agree to our terms and conditions");
	define("DOWNLOAD_TERMS_MSG", "Terms & Conditions");
	define("DOWNLOAD_TERMS_USER_DESC", "I have read and agree to your terms and conditions");
	define("DOWNLOAD_TERMS_USER_ERROR", "To download the product you have to read and agree to our terms and conditions");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>