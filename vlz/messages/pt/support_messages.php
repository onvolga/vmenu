<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Centro de Apoio");
	define("SUPPORT_REQUEST_INF_TITLE", "Pedido de Informaзгo");
	define("SUPPORT_REPLY_FORM_TITLE", "Responder");

	define("MY_SUPPORT_ISSUES_MSG", "As Minhas Dъvidas");
	define("MY_SUPPORT_ISSUES_DESC", "Se tiver algum problema com algum produto comprado, a nossa equipa de apoio estarб pronta a ajudб-lo. Contacte-nos sempre que necessitar, clicando no link acima para submeter o seu pedido de apoio.");
	define("NEW_SUPPORT_REQUEST_MSG", "Novo Pedido");
	define("SUPPORT_REQUEST_ADDED_MSG", "Obrigado<br>A nossa Equipa de Apoio tentarб ajudб-lo, relativamente ao seu pedido, o mais brevemente possнvel.");
	define("SUPPORT_SELECT_DEP_MSG", "Seleccionar o Departamento");
	define("SUPPORT_SELECT_PROD_MSG", "Seleccionar o Produto");
	define("SUPPORT_SELECT_STATUS_MSG", "Seleccionar o Status");
	define("SUPPORT_NOT_VIEWED_MSG", "Nгo visualizado");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Visualizado pelo utilizador");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Visualizado pelo administrador");
	define("SUPPORT_STATUS_NEW_MSG", "Novo");
	define("NO_SUPPORT_REQUEST_MSG", "Nгo foram encontrados assuntos");

	define("SUPPORT_SUMMARY_COLUMN", "Sumбrio");
	define("SUPPORT_TYPE_COLUMN", "Tipo");
	define("SUPPORT_UPDATED_COLUMN", "Ъltima actualizaзгo");

	define("SUPPORT_USER_NAME_FIELD", "O Seu Nome");
	define("SUPPORT_USER_EMAIL_FIELD", "O Seu Endereзo de E-mail");
	define("SUPPORT_IDENTIFIER_FIELD", "Identificador (Factura #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Ambiente (SO, Base de Dados, Web Server, etc.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Departamento");
	define("SUPPORT_PRODUCT_FIELD", "Produto");
	define("SUPPORT_TYPE_FIELD", "Tipo");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Status actual");
	define("SUPPORT_SUMMARY_FIELD", "Sumбrio de uma linha");
	define("SUPPORT_DESCRIPTION_FIELD", "Descriзгo");
	define("SUPPORT_MESSAGE_FIELD", "Mensagem");
	define("SUPPORT_ADDED_FIELD", "Adicionado");
	define("SUPPORT_ADDED_BY_FIELD", "Adicionado por");
	define("SUPPORT_UPDATED_FIELD", "Ъltima Actualizaзгo");

	define("SUPPORT_REQUEST_BUTTON", "Submeter pedido");
	define("SUPPORT_REPLY_BUTTON", "Responder");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Parвmetro <b>Support ID</b> em falta");
	define("SUPPORT_MISS_CODE_ERROR", "Parвmetro <b>Verification</b> em falta");
	define("SUPPORT_WRONG_ID_ERROR", "O parвmetro <b>Support ID</b> tem valor(es) errado(s)");
	define("SUPPORT_WRONG_CODE_ERROR", "O parвmetro <b>Verification</b> tem valor(es) errado(s)");

	define("MAIL_DATA_MSG", "Dados do Correio");
	define("HEADERS_MSG", "Cabeзalhos");
	define("ORIGINAL_TEXT_MSG", "Texto Original");
	define("ORIGINAL_HTML_MSG", "HTML Original");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>