<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//telepРЅtР№si СЊzenetek
	define("INSTALL_TITLE", " ViArt WebР±ruhР±z TelepРЅtР№s");

	define("INSTALL_STEP_1_TITLE", "TelepРЅtР№s: 1. LР№pР№s");
	define("INSTALL_STEP_1_DESC", "KС†szС†njСЊk, hogy a ViArt webР±ruhР±zat vР±lasztottad. A telepРЅtР№s folytatР±sР±hoz ki kell tС†ltened az alР±bbi mezС…ket. Figyelem: az adatbР±zis kivР±lasztР±sР±hoz mР±r a telepРЅtР№s elС…tt lР№tre kell hozni egy adatbР±zist. Ha olyan adatbР±zist telepРЅtesz ami  ODBC t  hasznР±l, mint a Microsoft Access , tovР±bb haladР±s elС…tt  lР№tre kell hozni DNS-t.");
	define("INSTALL_STEP_2_TITLE", "TelepРЅtР№s: 2. LР№pР№s");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "TelepРЅtР№s: 3. LР№pР№s");
	define("INSTALL_STEP_3_DESC", "VР±lassz a weboldal elrendezР№sek kС†zСЊl. KР№sС…bb termР№szetesen ezt megvР±ltoztathatod.");
	define("INSTALL_FINAL_TITLE", "TelepРЅtР№s: BefejezР№s");
	define("SELECT_DATE_TITLE", "VР±lassz dР±tumot formР±tumot");

	define("DB_SETTINGS_MSG", "AdatbР±zis beР±llРЅtР±sok");
	define("DB_PROGRESS_MSG", "AdatbР±zis struktСѓra lР№trehozР±sa");
	define("SELECT_PHP_LIB_MSG", "VР±lassz PHP KС†nyvtР±rat");
	define("SELECT_DB_TYPE_MSG", "VР±lassz adatbР±zis tРЅpust");
	define("ADMIN_SETTINGS_MSG", "AdminisztratРЅv beР±llРЅtР±sok");
	define("DATE_SETTINGS_MSG", "DР±tum formР±tumok");
	define("NO_DATE_FORMATS_MSG", "Nincs elР№rhetС… dР±tum formР±tum");
	define("INSTALL_FINISHED_MSG", "A telepРЅtР№s elkР№szСЊlt. KР№rlek, ellenС…rizd a beР±llРЅtР±sokat, Р№s vР№gezd el a szСЊksР№ges vР±ltoztatР±sokat az adminisztrР±ciСѓs menСЊben.");
	define("ACCESS_ADMIN_MSG", "Az adminisztrР±ciСѓs felСЊlet elР№rР№sР№hez kattintР±s ide");
	define("ADMIN_URL_MSG", "AdminisztrР±ciСѓ URL");
	define("MANUAL_URL_MSG", "HasznР±lati utasРЅtР±s URL");
	define("THANKS_MSG", "KС†szС†njСЊk, hogy a <b>ViArt WebР±ruhР±zat</b> vР±lasztotta.   ");

	define("DB_TYPE_FIELD", "AdatbР±zis TРЅpus");
	define("DB_TYPE_DESC", "KР№rlek vР±latd ki az <b>adatbР±zis tРЅpust</b>. Ha  SQL Servert vagy Microsoft Access-t hasznР±lsz, vР±laszd az ODBC lehetС…sР№get.");
	define("DB_PHP_LIB_FIELD", "PHP KС†nyvtР±r");
	define("DB_HOST_FIELD", "HostnР№v");
	define("DB_HOST_DESC", "KР№rlek add meg a <b>szerver nevР№t</b>vagy a <b>szerver IP cРЅmР№t</b>ahol a Viart-hoz hasznР±lni kРЅvР±nt adatbР±zis elР№rhetС…. Ha az adatbР±zis lokР±lis PC-Р№n fut akkor valСѓszРЅnС‹leg jСѓ a \\\"<b>localhost</b>\\\" Р№s hagyd a portot СЊresen. Ha egy szolgР±ltatСѓd Р±ltal biztosРЅtott adatbР±zist hasznР±lsz , akkor tanulmР±nyozd Р±t a szolgР±ltatСѓ szerver beР±llРЅtР±sainak a dokumentР±ciСѓjР±t (Р±ltalР±ban itt is jСѓ a localhost).");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "AdatbР±zis NР№v / DSN");
	define("DB_NAME_DESC", "Ha MySQL vagy PostgreSQL adatbР±zist hasznР±lsz, add meg az <b>adatbР±zis neve</b> mezС…ben a nevР№t, hogy a ViArt telepРЅtС… lР№trehozza a szСЊksР№ges tР±blР±kat. Az adatbР±zisnak mР±r lР№teznie kell. Ha tesztelР№s cР№ljР±bСѓl telepРЅted a Viart-ot lokР±lis pc-re, akkor a legtС†bb gР№pnek van egy <b>test</b>\\\" adatbР±zisa , amit hasznР±lhatsz. Ha nem , akkor kР№szРЅts egy adatbР±zist \\\"viart\\\" nР№ven, Р№s azt hasznР±ld. Ha  Microsoft Access-t vagy SQL Servert hasznР±lsz akkor az adatbР±zis nevР№nek a <b>DSN neve</b> tartomР±nyban kell lenni, amit beР±llРЅtottР±l az adatforrР±soknР±l (ODBC)  a kontrol panelen.");
	define("DB_USER_FIELD", "FelhasznР±lСѓ nР№v");
	define("DB_PASS_FIELD", "JelszСѓ");
	define("DB_USER_PASS_DESC", "<b>felhasznР±lСѓ nР№v</b> Р№s <b>jelszСѓ</b> - kР№rlek add meg a felhasznР±lСѓ nevet Р№s a jelszСѓt amit az adatbР±zis elР№rР№sР№hezz akarsz hasznР±lni. Ha egy lokР±lis gР№pen teszt installР±lР±st hasznР±lsz, a lehetsР№ges felhasznР±lСѓnР№v \\\"<b>root</b>\\\" Р№s nincs jelszСѓ. Ez РЅgy kivР±lСѓ tesztelР№sre , de nem biztonsР±gos egy nyilvР±nos szerveren.");
	define("DB_PERSISTENT_FIELD", "Р‘llandСѓ Kapcsolat");
	define("DB_PERSISTENT_DESC", "Р‘llandСѓ MySQL / Postgre kapcsolat esetР№n klikkeld be ezt a dobozt, ha nem tudod , hogy mit jelent , inkР±bb hagyd СЊresen.");
	define("DB_CREATE_DB_FIELD", "AdatbР±zis LР№trehozР±sa");
	define("DB_CREATE_DB_DESC", "az adatbР±zis lР№trehozР±sР±hoz kattintsd be a jelС†lС… nР№gyzetet. Csak MySQL Р№s Postgre esetР№ben mС‹kС†dik");
	define("DB_POPULATE_FIELD", "AdatbР±zi feltС†ltР№se");
	define("DB_POPULATE_DESC", "az adatbР±zis struktСЉra lР№trehozР±sР±hoz Р№s adatfeltС†ltР№shez kattintsd be a jelС†lС… nР№gyzetet");
	define("DB_TEST_DATA_FIELD", "DemСѓ adatok");
	define("DB_TEST_DATA_DESC", "Ha szeretnР№d, hogy a demСѓ tartalmak bekerСЊljenek az adatbР±zisba, kkattintsd be a jelС†lС…nР№gyzetet.");
	define("ADMIN_EMAIL_FIELD", "Р¬gyintР№zС… Email");
	define("ADMIN_LOGIN_FIELD", "Р¬gyintР№zС… azonosРЅtСѓ");
	define("ADMIN_PASS_FIELD", "Р¬gyintР№zС… jelszСѓ");
	define("ADMIN_CONF_FIELD", "IsmР№teld meg a jelszСѓt");
	define("DATETIME_SHOWN_FIELD", "IdС… formР±tum (webhelyen lР±tszik)");
	define("DATE_SHOWN_FIELD", "DР±tum formР±tum (webhelyen lР±tszik)");
	define("DATETIME_EDIT_FIELD", "IdС… formР±tum (szerkesztР№skor)");
	define("DATE_EDIT_FIELD", "DР±tum formР±tum (szerkesztР№skor)");
	define("DATE_FORMAT_COLUMN", "DР±tum formР±tum");
	define("CURRENT_DATE_COLUMN", "Mai dР±tum");

	define("DB_LIBRARY_ERROR", "PHP funkciСѓk nincsenek definiР±lva a {db_library} szР±mР±ra. KР№rem ellenС…rizze az adatbР±zis beР±llРЅtР±sait a konfigurР±ciСѓban. FР±jl:  php.ini.");
	define("DB_CONNECT_ERROR", "Nem lehet csatlakozni az adatbР±zishoz. KР№rem ellenС…rizze az adatbР±zis paramР№tereit.");
	define("INSTALL_FINISHED_ERROR", " A telepРЅtР№s folyamat mР±r befejezett.");
	define("WRITE_FILE_ERROR", "Nincs РЅrР±si engedР№lye a <b>'includes/var_definition.php'</b> fР±jlhoz. FolytatР±s elС…tt meg kell vР±ltozatni.");
	define("WRITE_DIR_ERROR", "Nem rendelkezik РЅrР±si engedР№llyel a <b>'includes/'</b> mappР±hoz. KР№rem megvР±ltoztatni a mappa engedР№lyeket.");
	define("DUMP_FILE_ERROR", "Dump fР±jl '{file_name}' nem talР±lhatСѓ.");
	define("DB_TABLE_ERROR", "TР±bla 'table_name' nem talР±lhatСѓ. KР№rem feltС†lteni az adatbР±zist a szСЊksР№ges adattal.");
	define("TEST_DATA_ERROR", "EllenС…rizd a <b>{POPULATE_DB_FIELD}</b> mielС…tt kС†zР№teszel tР±blР±kat teszt adatokkal.");
	define("DB_HOST_ERROR", "A hostnР№v amit meghatР±roztР±l, nem talР±lhatСѓ.");
	define("DB_PORT_ERROR", "MySQL szerver meghatР±rozott portjР±hoz nem lehet csatlakozni.");
	define("DB_USER_PASS_ERROR", "A meghatР±rozott felhasznР±lСѓnР№v jelszСѓ helytelen.");
	define("DB_NAME_ERROR", "Login beР±llРЅtР±sok rendben vannak, de az adatbР±zis '{db_name}'  nem talР±lhatСѓ.");

	//frissРЅtР№s СЊzenetek
	define("UPGRADE_TITLE", " ViArt SHOP frissРЅtР№s");
	define("UPGRADE_NOTE", "MegjegyezР№s: KР№rem kР№szРЅtsen mentР№st az adatbР±zisrСѓl, mielС…tt frissРЅtene!");
	define("UPGRADE_AVAILABLE_MSG", "AdatbР±zis frissРЅtett vР±ltozat elР№rhetС…");
	define("UPGRADE_BUTTON", "FrissРЅtР№s a  {version_number} verziСѓra ");
	define("CURRENT_VERSION_MSG", "Jelenleg telepРЅtett vР±ltozatot");
	define("LATEST_VERSION_MSG", "ElР№rhetС… telepРЅthetС… vР±ltozat ");
	define("UPGRADE_RESULTS_MSG", "FrissРЅtР№s eredmР№nye");
	define("SQL_SUCCESS_MSG", "SQL lekР№rdezР№s sikeres");
	define("SQL_FAILED_MSG", "SQL lekР№rdezР№s nem sikerСЊlt");
	define("SQL_TOTAL_MSG", "Teljes SQL lekР№rdezР№s megtС†rtР№nt");
	define("VERSION_UPGRADED_MSG", "Az adatbР±zisod frissСЊlt a");
	define("ALREADY_LATEST_MSG", "MР±r a legСЉjabb vР±ltozattal rendelkezel.");
	define("DOWNLOAD_NEW_MSG", "РЄj vР±ltozatot talР±ltunk.");
	define("DOWNLOAD_NOW_MSG", "{version_number} verziСѓ letС†ltР№se most.");
	define("DOWNLOAD_FOUND_MSG", "Р™rzР№keltСЊk, hogy az СЉj {version_number} verziСѓ letС†lthetС…. Az alР±bbi linkre kattintva lehet elkezdeni a letС†ltР№st. A letС†ltР№s utР±n Р№s a fР±jlok cserР№lР№se ne felejtsd  frissРЅtР№st СЉjra futatni.");
	define("NO_XML_CONNECTION", "FigyelmeztetР№s! Nincs kapcsolat 'HTTP:www.viart.com/' ! ");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>