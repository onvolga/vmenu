<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "Onderwerp informatie");
	define("TOPIC_MESSAGE_TITLE", "Bericht");

	define("MY_FORUM_TOPICS_MSG", "Mijn berichten");
	define("ALL_FORUM_TOPICS_MSG", "Alle onderwerpen");
	define("MY_FORUM_TOPICS_DESC", "Praat mee in ons forum en deel uw ervaringen en expertise met andere ebook readers!");
	define("NEW_TOPIC_MSG", "Nieuw onderwerp/bericht");
	define("NO_TOPICS_MSG", "Geen onderwerpen gevonden");
	define("FOUND_TOPICS_MSG", "We hebben <b>{found_records}</b> onderwerpen gevonden die voldoen aan de term(en) '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Geen fora gevonden");

	define("FORUM_NAME_COLUMN", "Onderwerp");
	define("FORUM_TOPICS_COLUMN", "Berichten");
	define("FORUM_REPLIES_COLUMN", "Reacties");
	define("FORUM_LAST_POST_COLUMN", "Laatste reactie");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "Onderwerp");
	define("TOPIC_AUTHOR_COLUMN", "Gepost door");
	define("TOPIC_VIEWS_COLUMN", "Bekeken");
	define("TOPIC_REPLIES_COLUMN", "Reacties");
	define("TOPIC_UPDATED_COLUMN", "Laatst bijgewerkt");
	define("TOPIC_ADDED_MSG", "Bedankt<br>Uw onderwerp is geplaatst");

	define("TOPIC_ADDED_BY_FIELD", "Geplaatst door");
	define("TOPIC_ADDED_DATE_FIELD", "Geplaatst");
	define("TOPIC_UPDATED_FIELD", "Laatst bijgewerkt");
	define("TOPIC_NICKNAME_FIELD", "Naam");
	define("TOPIC_EMAIL_FIELD", "Email (wordt niet getoond)");
	define("TOPIC_NAME_FIELD", "Onderwerp");
	define("TOPIC_MESSAGE_FIELD", "Bericht");
	define("TOPIC_NOTIFY_FIELD", "Verstuur reacties naar mijn emailadres");

	define("ADD_TOPIC_BUTTON", "Plaats!");
	define("TOPIC_MESSAGE_BUTTON", "Voeg bericht toe");

	define("TOPIC_MISS_ID_ERROR", "Ontbrekende <b>Thread ID</b> parameter");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> parameter heeft verkeerde waarde");
	define("FORUM_SEARCH_MESSAGE", "We hebben {search_count} berichten gevonden met de zoekterm(en) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Opslaan");

	define("LAST_POST_ON_SHORT_MSG", "Op:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "Door:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Laatst gewijzigd:");

?>