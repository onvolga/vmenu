<?php
/*------------------------------------------------------------------------------*/
/*���� �������: 27-11-2012                                                      */
/*����������� ��� ������� �������� (2 ������)                                   */
/*��������� �����:  															*/
/*                 \includes\shipping_functions.php								*/
/*                 \admin\admin_order_info.php									*/
/*                 \admin\admin_shipping_type.php								*/
/*                 \admin\admin_upload.php										*/
/*                 \blocks\block_order_info.php									*/
/*                 \templates\user\block_order_info.html						*/
/*                 \templates\admin\admin_shipping_type.html					*/
/*                 \templates\admin\admin_order_info.html						*/
/*------------------------------------------------------------------------------*/
function get_shipping_types($delivery_country_id, $delivery_state_id, $delivery_postal_code, $delivery_site_id, $user_type_id, $delivery_items, $call_center = 0)
{
	global $db, $table_prefix, $country_code, $postal_code, $order_total, $state_code, $r, $errors;
	global $goods_total_full, $total_quantity, $weight_total;
	global $shipping_packages, $shipping_items_total, $shipping_weight, $shipping_quantity;
	global $user_items;

$useUserShippingFilter = false;
if($user_items){
/**
 * get user filtered shippings
 */

	$useUserShippingFilter = true;
	$userShippingsArray = array();
	if($user_items){
		$userShippingsSelected = get_db_value("SELECT ss_record FROM va_users WHERE user_id=".$user_items);

	    $userShippingsArray = array_filter(explode(",", $userShippingsSelected), 'strlen');
	}
    if(count($userShippingsArray) == 0){
        $useUserShippingFilter = false;
    }
}
	// check modules available in delivery items
	$is_default = false;
	$modules_ids = array();
	foreach ($delivery_items as $key => $item) {
		$shipping_modules_default = $item["shipping_modules_default"];
		$shipping_modules_ids = $item["shipping_modules_ids"];
		if (!strlen($shipping_modules_ids) && !$shipping_modules_default) {
			// if no modules selected use default methods
			$shipping_modules_default = true;
			$delivery_items[$key]["shipping_modules_default"] = 1;
		}
		if ($shipping_modules_default) {
			$is_default = true;
		} else if (strlen($shipping_modules_ids)) {
			$item_modules = explode(",", $shipping_modules_ids);
			$delivery_items[$key]["shipping_modules_ids"] = $item_modules; // assign values as array
			for ($m = 0; $m < sizeof($item_modules); $m++) {
				if (!in_array($item_modules[$m], $modules_ids)) {
					$modules_ids[] = $item_modules[$m];
				}
			}
		}
	}

	// check active shipping modules for delivery items
	$shipping_modules = array();
	if ($is_default || sizeof($modules_ids) > 0) {
		$sql  = " SELECT * ";
		$sql .= " FROM " . $table_prefix . "shipping_modules ";
		$sql .= " WHERE is_active=1 AND (";
		if ($is_default) {
			$sql .= " is_default=1 ";
		}
		if ($is_default && sizeof($modules_ids) > 0) {
			$sql .= " OR ";
		}
		if (sizeof($modules_ids) > 0) {
			$sql .= " shipping_module_id IN (" . $db->tosql($modules_ids, INTEGERS_LIST) . ") ";
		}
		$sql .= ") ";
		if ($call_center) {
			$sql .= " AND is_call_center=1 ";
		}
		$db->query($sql);
		while ($db->next_record()) {
			$shipping_module_id   = $db->f("shipping_module_id");
			$user_added_id   = $db->f("user_added_id");
if(!in_array($shipping_module_id, $userShippingsArray) && $useUserShippingFilter == true){
			continue;
}
if($user_added_id && $user_added_id!= $user_items){
	continue;
}
			$shipping_module_name = $db->f("shipping_module_name");
			$user_module_name     = $db->f("user_module_name");
			$is_external          = $db->f("is_external");
			$is_default           = $db->f("is_default");
			$php_external_lib     = $db->f("php_external_lib");
			$external_url         = $db->f("external_url");
			$cost_add_percent     = $db->f("cost_add_percent");
			$shipping_modules[$shipping_module_id] = array(
				"is_default" => $is_default, 
				"module_id" => $shipping_module_id, 
				"module_name" => $shipping_module_name, 
				"user_module_name" => $user_module_name, 
				"is_external" => $is_external, 
				"php_external_lib" => $php_external_lib, 
				"external_url" => $external_url, 
				"cost_add_percent" => $cost_add_percent,
			);
		}
	}

	// check shipping methods available for selected destination 
	$default_modules = array();
	foreach ($shipping_modules as $module_id => $module) {
		$sql  = " SELECT st.shipping_type_id ";
		$sql .= " FROM ((((";
		$sql .= $table_prefix . "shipping_types st ";
		$sql .= " LEFT JOIN " . $table_prefix . "shipping_types_countries stc ON st.shipping_type_id=stc.shipping_type_id) ";
		$sql .= " LEFT JOIN " . $table_prefix . "shipping_types_states stt ON st.shipping_type_id=stt.shipping_type_id) ";
		if ($delivery_site_id) {
			$sql .= " LEFT JOIN " . $table_prefix . "shipping_types_sites s ON st.shipping_type_id=s.shipping_type_id) ";
		} else {
			$sql .= ")";
		}
		if (strlen($user_type_id)) {
			$sql .= " LEFT JOIN " . $table_prefix . "shipping_types_users ut ON st.shipping_type_id=ut.shipping_type_id) ";
		} else {
			$sql .= ")";
		}
		$sql .= " WHERE st.is_active=1 ";
		$sql .= " AND st.shipping_module_id=" . $db->tosql($module_id, INTEGER);
		$sql .= " AND (st.countries_all=1 OR stc.country_id=" . $db->tosql($delivery_country_id, INTEGER, true, false) . ") ";
		$sql .= " AND (st.states_all=1 OR stt.state_id=" . $db->tosql($delivery_state_id, INTEGER, true, false) . ") ";
		if ($delivery_site_id) {
			$sql .= " AND (st.sites_all=1 OR s.site_id=" . $db->tosql($delivery_site_id, INTEGER, true, false) . ")";
		} else {
			$sql .= " AND st.sites_all=1 ";
		}
		if (strlen($user_type_id)) {
			$sql .= " AND (st.user_types_all = 1 OR ut.user_type_id=" . $db->tosql($user_type_id, INTEGER, true, false) . ")";
		} else {
			$sql .= " AND st.user_types_all = 1 ";
		}
		$shipping_types = array();
		$db->query($sql);
		if ($db->next_record()) {
			do {
				$shipping_type_id = $db->f("shipping_type_id");
				$shipping_types[$shipping_type_id] = $shipping_type_id;
			} while ($db->next_record());
			$shipping_modules[$module_id]["types_ids"] = array_keys($shipping_types); 
			if ($module["is_default"]) {	
				// add active default module to list
				$default_modules[] = $module_id;
			}
		} else {
			unset($shipping_modules[$module_id]);
		}
	}
	sort($default_modules); // sort to re-order array

	// remove inactive modules and modules without any methods and prepare shipping groups
	$shipping_groups = array();
	foreach ($delivery_items as $key => $item) {
		$shipping_modules_default = $item["shipping_modules_default"];
		$original_modules_ids = $item["shipping_modules_ids"];
		$shipping_modules_ids = array(); // updated information about possible modules
		if ($shipping_modules_default) {
			$shipping_modules_ids = $default_modules;
		} else if (is_array($original_modules_ids) && sizeof($original_modules_ids) > 0) {
			for ($m = 0; $m < sizeof($original_modules_ids); $m++) {
				$module_id = $original_modules_ids[$m];
				if (isset($shipping_modules[$module_id])) {
					$shipping_modules_ids[] = $module_id;
				}
			}
		}

		$delivery_items[$key]["shipping_modules_ids"] = $shipping_modules_ids; // assign udpated modules

		// check if there is already shipping group exists for such shipping modules or we need create a new one
		$shipping_group_id = "";
		foreach ($shipping_groups as $id => $group) {
			$group_modules = $group["modules"];
			if ($group_modules == $shipping_modules_ids) {
				$shipping_group_id = $id;
				break;
			}
		}
		if (!strlen($shipping_group_id)) {
			// add a new group
			$user_names = array();
			for ($sm = 0; $sm < sizeof($shipping_modules_ids); $sm++) {
				$module_id = $shipping_modules_ids[$sm];
				$user_name = $shipping_modules[$module_id]["user_module_name"];
				if (strlen($user_name) && !in_array($user_name, $user_names)) {
					$user_names[] = $user_name;
				}
			}

			$shipping_groups[] = array(
				"modules" => $shipping_modules_ids,
				"user_names" => $user_names,
				"items" => array(),
				"items_ids" => array(),
			);
			end($shipping_groups);
			$shipping_group_id = key($shipping_groups);
		}

		$delivery_items[$key]["shipping_group_id"] = $shipping_group_id; // assign shipping group
		$shipping_groups[$shipping_group_id]["items"][] = $key;
		$shipping_groups[$shipping_group_id]["items_ids"][] = $item["item_id"];
	}


	// get country and state codes
	$sql  = " SELECT country_code FROM " . $table_prefix . "countries ";
	$sql .= " WHERE country_id=" . $db->tosql($delivery_country_id, INTEGER);
	$country_code = get_db_value($sql);

	$sql  = " SELECT state_code FROM " . $table_prefix . "states ";
	$sql .= " WHERE state_id=" . $db->tosql($delivery_state_id, INTEGER);
	$state_code = get_db_value($sql);

	$postal_code = $delivery_postal_code;

	foreach ($shipping_groups as $group_id => $group) {
		$shipping_items = $group["items"];
		$modules_ids = $group["modules"];

		$shipping_types = array(); // return this array with available delivery methods
		$shipping_packages = array(); 
		$goods_total_full = 0; $shipping_items_total = 0; $total_quantity = 0; $weight_total = 0; $shipping_weight = 0; $shipping_quantity = 0;
		$goods_weight = 0; // weight of all items for current shipping group includes free delivery items
		// fit products to packages and get totals
		//foreach ($delivery_items as $id => $item) {
		for ($si = 0; $si < sizeof($shipping_items); $si++) {
			$item_index = $shipping_items[$si];
			$item = $delivery_items[$item_index];

			if (isset($item["full_price"])) {
				$price = $item["full_price"];
			} else {
				$price = $item["price"];
			}
			$quantity = $item["quantity"];
			$packages_number = $item["packages_number"];
			if ($packages_number <= 0) { $packages_number = 0.1; }
			if (isset($item["full_weight"])) {
				$weight = $item["full_weight"];
			} else {
				$weight = $item["weight"];
			}
			$goods_weight += $weight;
			$width = $item["width"];
			$height = $item["height"];
			$length = $item["length"];
			$is_shipping_free = $item["is_shipping_free"];
			$shipping_cost = $item["shipping_cost"];
  
			$item_total = $price * $quantity;
			$weight_total += ($weight * $quantity);
			$total_quantity += $quantity;
			$goods_total_full += $item_total;
			if (!$is_shipping_free) {
				$shipping_quantity += $quantity;
				$shipping_items_total += ($shipping_cost * $quantity); 
				$shipping_weight += ($weight * $quantity);
				// check each product one by one 
				for ($q = 1; $q <= $quantity; $q++) {
					$packages_left = $packages_number;
					while ($packages_left > 0) {
						// get no more than one package per iteration
						if ($packages_left > 1) {
							$package_number = 1;
						} else {
							$package_number = $packages_left;
						}
						$fit_in_package = false; // check if product could be fit in existed packages
						if ($package_number < 1) {
							foreach ($shipping_packages as $id => $package) {
								if ($package["width"] == $width && $package["height"] == $height
								&& $package["length"] == $length && ($package["packages"] + $package_number) <= 1) {
									$fit_in_package = true;
									$shipping_packages[$id]["price"] += round($price * ($package_number / $packages_number), 2);
									$shipping_packages[$id]["quantity"] += 1;
									$shipping_packages[$id]["packages"] += $package_number;
									$shipping_packages[$id]["weight"] += round($weight * ($package_number / $packages_number), 2);
								}
							}
						}
						if (!$fit_in_package) {
							// add to new package
							$shipping_packages[] = array(
								"price" => round($price * ($package_number / $packages_number), 2),
								"quantity" => 1,
								"packages" => $package_number,
								"weight" => round($weight * ($package_number / $packages_number), 2),
								"width" => $item["width"],
								"height" => $item["height"],
								"length" => $item["length"],
							);
						}
						$packages_left = $packages_left - $package_number;
					}
				}
			}
		}

		// update goods weight for this group
		$shipping_groups[$group_id]["goods_weight"] = $goods_weight;
  
		// check if not all items are free to ship
		if ($shipping_quantity > 0) {
			for ($sm = 0; $sm < sizeof($modules_ids); $sm++) {
				$module_id = $modules_ids[$sm];
				$module = $shipping_modules[$module_id];
				$shipping_module_id = $module["module_id"];
				$shipping_module_name = $module["module_name"];
				$is_external  = $module["is_external"];
				$php_external_lib  = $module["php_external_lib"];
				$external_url  = $module["external_url"];
				$cost_add_percent  = $module["cost_add_percent"];
				$types_ids = $module["types_ids"];

				$module_shipping = array();
				$sql  = " SELECT st.shipping_type_id, st.shipping_type_code, st.shipping_type_desc, st.shipping_time, ";
				$sql .= " st.cost_per_order, st.cost_per_product, st.cost_per_weight, st.tare_weight, st.is_taxable ";
/*-------------------------------------------����������� ��� ������� ��������(1/2)-------------------------------------------*/				
				$sql .= " ,st.image_small, st.image_small_alt, st.image_large, st.image_large_alt ";
/*------------------------------------------/����������� ��� ������� ��������(1/2)-------------------------------------------*/	

/*dostavka custom*/
$sql .= " , st.blz_shipping_type,st.p_evening,st.p_weekend,st.p_at_time,st.p_to_floor,st.self_delivery,st.p_info,st.p_phones,st.p_address,st.p_monday,st.p_tuesday,st.p_wednesday,st.p_thursday,st.p_friday,st.p_saturday, ";
$sql .= " st.p_sunday,st.p_monday_text,st.p_tuesday_text,st.p_wednesday_text,st.p_thursday_text,st.p_friday_text,st.p_saturday_text,st.p_sunday_text ";

				$sql .= " FROM " . $table_prefix . "shipping_types st ";
				$sql .= " WHERE st.is_active=1 ";
				$sql .= " AND (st.min_weight IS NULL OR st.min_weight<=" . $db->tosql($shipping_weight, NUMBER) . ") ";
				$sql .= " AND (st.max_weight IS NULL OR st.max_weight>=" . $db->tosql($shipping_weight, NUMBER) . ") ";
				$sql .= " AND (st.min_goods_cost IS NULL OR st.min_goods_cost<=" . $db->tosql($goods_total_full, NUMBER) . ") ";
				$sql .= " AND (st.max_goods_cost IS NULL OR st.max_goods_cost>=" . $db->tosql($goods_total_full, NUMBER) . ") ";
				$sql .= " AND (st.min_quantity IS NULL OR st.min_quantity<=" . $db->tosql($shipping_quantity, NUMBER) . ") ";
				$sql .= " AND (st.max_quantity IS NULL OR st.max_quantity>=" . $db->tosql($shipping_quantity, NUMBER) . ") ";
				$sql .= " AND st.shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER);
				$sql .= " AND st.shipping_type_id IN (" . $db->tosql($types_ids, INTEGERS_LIST) . ") ";
				$sql .= " ORDER BY st.shipping_order, st.shipping_type_id ";
				$db->query($sql);
				while ($db->next_record()) {
					$row_shipping_type_id = $db->f("shipping_type_id");
					$row_shipping_type_code = $db->f("shipping_type_code");
					$row_shipping_type_desc = get_translation($db->f("shipping_type_desc"));
					$row_shipping_time = $db->f("shipping_time");
					$cost_per_order = $db->f("cost_per_order");
					$cost_per_product = $db->f("cost_per_product");
					$cost_per_weight = $db->f("cost_per_weight");
					$row_tare_weight = $db->f("tare_weight");
					$row_shipping_taxable = $db->f("is_taxable");
					$row_shipping_cost = ($shipping_items_total + $cost_per_order + ($cost_per_product * $shipping_quantity) + ($cost_per_weight * ($shipping_weight + $row_tare_weight)));					
					$shipping_type = array($row_shipping_type_id, $row_shipping_type_code, $row_shipping_type_desc, $row_shipping_cost, $row_tare_weight, $row_shipping_taxable, $row_shipping_time);
				
					$image_small = $db->f("image_small");
					$image_small_alt = $db->f("image_small_alt");
					$image_large = $db->f("image_large");
					$image_large_alt = $db->f("image_large_alt");
					array_push($shipping_type, $image_small, $image_small_alt, $image_large, $image_large_alt);

					/**
					 * dostavka custom shipping data
					 */
					$shipping_type["additional_data"] = array(
						"blz_shipping_type" => $db->f("blz_shipping_type"),
						"days" => array(
							"monday" => array("available" => $db->f("p_monday"), "info" => $db->f("p_monday_text")),
							"tuesday" => array("available" => $db->f("p_tuesday"), "info" => $db->f("p_tuesday_text")),
							"wednesday" => array("available" => $db->f("p_wednesday"), "info" => $db->f("p_wednesday_text")),
							"thursday" => array("available" => $db->f("p_thursday"), "info" => $db->f("p_thursday_text")),
							"friday" => array("available" => $db->f("p_friday"), "info" => $db->f("p_friday_text")),
							"saturday" => array("available" => $db->f("p_saturday"), "info" => $db->f("p_saturday_text")),
							"sunday" => array("available" => $db->f("p_sunday"), "info" => $db->f("p_sunday_text"))
						),
						"info" => $db->f("p_info"),
						"phones" => $db->f("p_phones"),
						/*self delivery data*/
						"address" => $db->f("p_address"),
						"self_delivery" => $db->f("self_delivery"),
						/*delivery data*/
						"evening" => $db->f("p_evening"),
						"weekend" => $db->f("p_weekend"),
						"at_time" => $db->f("p_at_time"),
						"to_floor" => $db->f("p_to_floor"),
					);			
				
					$module_shipping[] = $shipping_type;
					if (!$is_external) {
						$shipping_types[] = $shipping_type;
					}
				}
    
				if ($is_external && strlen($php_external_lib) && sizeof($module_shipping) > 0) {
					$module_params = array();
					$sql  = " SELECT * FROM " . $table_prefix . "shipping_modules_parameters ";
					$sql .= " WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER);
					$sql .= " AND not_passed<>1 ";
					$db->query($sql);
					while ($db->next_record()) {
						$param_name = $db->f("parameter_name");
						$param_source = $db->f("parameter_source");
						$module_params[$param_name] = $param_source;
					}
					if (!file_exists($php_external_lib)) {
						// check sub path if script run from admin folder
						if (preg_match("/^\.\//", $php_external_lib)) {
							$php_external_lib = ".".$php_external_lib;
						} else {
							$php_external_lib = "../".$php_external_lib;
						}
					}
					include($php_external_lib);
				}
				if ($cost_add_percent && $shipping_types) {
					for($i=0, $ic = count($shipping_types); $i<$ic; $i++) {
						$shipping_types[$i][3] = $shipping_types[$i][3] * (1 + $cost_add_percent/100);
					}
				}
			}
		}

		// check if there are no any methods
		// add default shipping type in case if there are no methods available
		if (sizeof($shipping_types) == 0) {
			if ($shipping_items_total > 0) {
				$shipping_types[] = array(0, "", PROD_SHIPPING_MSG, $shipping_items_total, 0, 1, 0);
			} else if ($shipping_quantity == 0 && $weight_total > 0) {
				// all products has a free delivery
				$shipping_types[] = array(0, "", PROD_SHIPPING_MSG, 0, 0, 0, 0);
			}
		}
		// save shipping types to group
		$shipping_groups[$group_id]["types"] = $shipping_types;
	}


	return $shipping_groups;
}


?>