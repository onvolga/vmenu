<?php

    include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once("./admin_common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/friendly_functions.php");

	check_admin_security("update_products");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main", "admin_blz_popular.html");

    $operation = get_param('operation');
	
    if($operation == 'cancel'){
        header('Location: ' . './admin_items_list.php');
        exit;
    }
	if($operation == 'save'){
        $popular_clothes_name = get_param('popular_clothes_name');
        $popular_clothes = get_param('popular_clothes');
        $popular_boots_name = get_param('popular_boots_name');
        $popular_boots = get_param('popular_boots');
        $popular_boys_name = get_param('popular_boys_name');
        $popular_boys = get_param('popular_boys');
        $popular_girls_name = get_param('popular_girls_name');
        $popular_girls = get_param('popular_girls');
        $popular_others_name = get_param('popular_others_name');
        $popular_others = get_param('popular_others');
        
        //t1
        $stmt = '   UPDATE va_blz_sliders 
                        SET items_type = "popular_clothes", ids_list = "' . $popular_clothes . '", user_tab_name = "' . $popular_clothes_name . '"
                        WHERE items_type = "popular_clothes"
                ';
        $db->query($stmt);
        
        //t2
        $stmt = '   UPDATE va_blz_sliders 
                        SET items_type = "popular_boots", ids_list = "' . $popular_boots . '", user_tab_name = "' . $popular_boots_name . '"
                        WHERE items_type = "popular_boots"
                ';
        $db->query($stmt);
        
        //t3
        $stmt = '   UPDATE va_blz_sliders 
                        SET items_type = "popular_boys", ids_list = "' . $popular_boys . '", user_tab_name = "' . $popular_boys_name . '"
                        WHERE items_type = "popular_boys"
                ';
        $db->query($stmt);
        
        //t4
        $stmt = '   UPDATE va_blz_sliders 
                        SET items_type = "popular_girls", ids_list = "' . $popular_girls . '", user_tab_name = "' .  $popular_girls_name . '"
                        WHERE items_type = "popular_girls"
                ';
        $db->query($stmt);
        
        //t5
        $stmt = '   UPDATE va_blz_sliders 
                        SET items_type = "popular_others", ids_list = "' . $popular_others . '", user_tab_name = "' .  $popular_others_name . '"
                        WHERE items_type = "popular_others"
                ';
        $db->query($stmt);

        header('Location: ' . './admin_blz_popular.php');
        exit;
    }
	
    $popular_clothes = get_db_values('SELECT ids_list, user_tab_name FROM va_blz_sliders WHERE  items_type = "popular_clothes"', array());
    $popular_boots = get_db_values('SELECT ids_list, user_tab_name FROM va_blz_sliders WHERE  items_type = "popular_boots"', array());
    $popular_boys = get_db_values('SELECT ids_list, user_tab_name FROM va_blz_sliders WHERE  items_type = "popular_boys"', array());
    $popular_girls = get_db_values('SELECT ids_list, user_tab_name FROM va_blz_sliders WHERE  items_type = "popular_girls"', array());
    $popular_others = get_db_values('SELECT ids_list, user_tab_name FROM va_blz_sliders WHERE  items_type = "popular_others"', array());
    
    $t->set_var('popular_clothes_name', $popular_clothes[0][1]);
    $t->set_var('popular_clothes', $popular_clothes[0][0]);
    $t->set_var('popular_boots_name', $popular_boots[0][1]);
    $t->set_var('popular_boots', $popular_boots[0][0]);
    $t->set_var('popular_boys_name', $popular_boys[0][1]);
    $t->set_var('popular_boys', $popular_boys[0][0]);
    $t->set_var('popular_girls_name', $popular_girls[0][1]);
    $t->set_var('popular_girls', $popular_girls[0][0]);
    $t->set_var('popular_others_name', $popular_others[0][1]);
    $t->set_var('popular_others', $popular_others[0][0]);


	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->pparse("main");