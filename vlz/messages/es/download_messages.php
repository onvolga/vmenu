<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// mensajes de la descarga
	define("DOWNLOAD_WRONG_PARAM", "El(Los) parametro(s) de la descarga es(son) erroneo(s).");
	define("DOWNLOAD_MISS_PARAM", "Falta el(los) parametro(s) de la descarga.");
	define("DOWNLOAD_INACTIVE", "La descarga no esta activa.");
	define("DOWNLOAD_EXPIRED", "Su periodo de descarga ha expirado.");
	define("DOWNLOAD_LIMITED", "Ha sobrepasado la cantidad mбxima de descargas.");
	define("DOWNLOAD_PATH_ERROR", "No se puede encontrar la ruta al producto.");
	define("DOWNLOAD_RELEASE_ERROR", "El nuevo producto no se ha encontrado.");
	define("DOWNLOAD_USER_ERROR", "Solo Usuarios Registrados pueden descargar este producto.");
	define("ACTIVATION_OPTIONS_MSG", "Opciones de Activaciуn");
	define("ACTIVATION_MAX_NUMBER_MSG", "Max nъmero de activaciones");
	define("DOWNLOAD_OPTIONS_MSG", "Descargable/Opciones de Software");
	define("DOWNLOADABLE_MSG", "Descargable (Software)");
	define("DOWNLOADABLE_DESC", "Para descargar el producto, puede tambiйn especificar 'Perнodo de Descarga', 'Ruta a el Archivo Descargable' y 'Opciones de Activaciones'");
	define("DOWNLOAD_PERIOD_MSG", "Perнodo de Descarga");
	define("DOWNLOAD_PATH_MSG", "Ruta a el Archivo Descargable");
	define("DOWNLOAD_PATH_DESC", "Puede aсadir multiples rutas separadas por coma");
	define("UPLOAD_SELECT_MSG", "Seleccione un archivo a subir y pulse el botуn {button_name}.");
	define("UPLOADED_FILE_MSG", "El archivo <b>{filename}</b> fue subido.");
	define("UPLOAD_SELECT_ERROR", "Por favor seleccione primero un archivo.");
	define("UPLOAD_IMAGE_ERROR", "Sуlo se permiten archivos de imбgenes.");
	define("UPLOAD_FORMAT_ERROR", "Este tipo de archivo no estб permitido.");
	define("UPLOAD_SIZE_ERROR", "Archivos mayores de {filesize} no estбn permitidos.");
	define("UPLOAD_DIMENSION_ERROR", "Imбgenes mayores de {dimension} no estбn permitidas.");
	define("UPLOAD_CREATE_ERROR", "El sistema no puede crear el archivo.");
	define("UPLOAD_ACCESS_ERROR", "No tiene permisos para subir archivos.");
	define("DELETE_FILE_CONFIRM_MSG", "Estб seguro de que desea borrar este archivo?");
	define("NO_FILES_MSG", "No se encontraron archivos");
	define("SERIAL_GENERATE_MSG", "Generar Nъmero de Serie");
	define("SERIAL_DONT_GENERATE_MSG", "No generar");
	define("SERIAL_RANDOM_GENERATE_MSG", "generar serial de producto al azar de Software");
	define("SERIAL_FROM_PREDEFINED_MSG", "Obtener el nъmero de serie de lista predefinida");
	define("SERIAL_PREDEFINED_MSG", "Nъmeros de Serie Predefinidos");
	define("SERIAL_NUMBER_COLUMN", "Nъmero de Serie");
	define("SERIAL_USED_COLUMN", "Usado");
	define("SERIAL_DELETE_COLUMN", "Eliminar");
	define("SERIAL_MORE_MSG", "Aсadir mбs nъmeros de Serie?");
	define("SERIAL_PERIOD_MSG", "Perнodo de Nъmero de Serie");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Mostrar Tйrminos y Condiciones");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Para descargar el producto, el usuario ha de leer y aceptar nuestros tйrminos y condiciones");
	define("DOWNLOAD_TERMS_MSG", "Tйrminos y Condiciones");
	define("DOWNLOAD_TERMS_USER_DESC", "He leнdo y acepto los tйrminos y condiciones");
	define("DOWNLOAD_TERMS_USER_ERROR", "Para descargar el producto, usted tiene que leer y aceptar nuestros tйrminos y condiciones");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>