<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.10RE                                               ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viartsoft.ru                                         ***
  ***                                                                      ***
  ****************************************************************************
*/

	// сообщения форума
	define("FORUM_TITLE", "Форум");
	define("TOPIC_INFO_TITLE", "Информация о теме");
	define("TOPIC_MESSAGE_TITLE", "Сообщение");

	define("MY_FORUM_TOPICS_MSG", "Мои темы на форуме");
	define("ALL_FORUM_TOPICS_MSG", "Все темы форума");
	define("MY_FORUM_TOPICS_DESC", "Возможно, Ваш вопрос уже обсуждался и имеет решение. Возможно, Вы хотите поделиться своими мнением и знаниями с другими  пользователями. Приглашаем на наш Форум!");
	define("NEW_TOPIC_MSG", "Новая тема");
	define("NO_TOPICS_MSG", "Тема не найдена");
	define("FOUND_TOPICS_MSG", "Найдено <b> {found_records} </b> тем, соответствующих запросу ' <b> {search_string} </b> '");
	define("NO_FORUMS_MSG", "Форумы не найдены");

	define("FORUM_NAME_COLUMN", "Форумы в разделе");
	define("FORUM_TOPICS_COLUMN", "Тема");
	define("FORUM_REPLIES_COLUMN", "Ответов");
	define("FORUM_LAST_POST_COLUMN", "Последнее обновление");
	define("FORUM_MODERATORS_MSG", "Модераторы");

	define("TOPIC_NAME_COLUMN", "Тема");
	define("TOPIC_AUTHOR_COLUMN", "Автор");
	define("TOPIC_VIEWS_COLUMN", "Просмотры");
	define("TOPIC_REPLIES_COLUMN", "Ответы");
	define("TOPIC_UPDATED_COLUMN", "Обновлено");
	define("TOPIC_ADDED_MSG", "Спасибо<br>Ваша тема добавлена");

	define("TOPIC_ADDED_BY_FIELD", "Добавил");
	define("TOPIC_ADDED_DATE_FIELD", "Добавлено");
	define("TOPIC_UPDATED_FIELD", "Последнее обновление");
	define("TOPIC_NICKNAME_FIELD", "Ваше имя на форуме (NICKNAME)");
	define("TOPIC_EMAIL_FIELD", "Ваш электронный адрес");
	define("TOPIC_NAME_FIELD", "Тема");
	define("TOPIC_MESSAGE_FIELD", "Сообщение");
	define("TOPIC_NOTIFY_FIELD", "Присылать все ответы на мой электронный адрес");

	define("ADD_TOPIC_BUTTON", "Добавить тему");
	define("TOPIC_MESSAGE_BUTTON", "Отправить");

	define("TOPIC_MISS_ID_ERROR", "Пропущен параметр <b>ID сообщения</b>.");
	define("TOPIC_WRONG_ID_ERROR", "Параметр <b>ID сообщения</b имеет неправильное значение");
	define("FORUM_SEARCH_MESSAGE", "Найдено {search_count} сообщений, соответствующих условию '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Предпросмотр");
	define("TOPIC_SAVE_BUTTON", "Сохранить");

	define("LAST_POST_ON_SHORT_MSG", "На:");
	define("LAST_POST_IN_SHORT_MSG", "В:");
	define("LAST_POST_BY_SHORT_MSG", "От:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Последние изменения:");
    define("USERS_ATTACHED_FILES_MSG", "Прикреплённые файлы");

?>
