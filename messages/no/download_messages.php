<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// nedlastingsbeskjeder
	define("DOWNLOAD_WRONG_PARAM", "Feil nedlastingsverdi(er).");
	define("DOWNLOAD_MISS_PARAM", "Nedlastingsverdi(er) mangler.");
	define("DOWNLOAD_INACTIVE", "Nedlasting ikke aktivert.");
	define("DOWNLOAD_EXPIRED", "Din nedlastingsperiode har utgеtt.");
	define("DOWNLOAD_LIMITED", "Du har overskridet det tillatte antallet nedlastinger.");
	define("DOWNLOAD_PATH_ERROR", "Spor til vare kan ikke bi funnet.");
	define("DOWNLOAD_RELEASE_ERROR", "Utgivelse ble ikke funnet.");
	define("DOWNLOAD_USER_ERROR", "Kun registrerte brukere kan laste ned denne filen.");
	define("ACTIVATION_OPTIONS_MSG", "Aktiveringsvalg");
	define("ACTIVATION_MAX_NUMBER_MSG", "Maksimum antall aktiveringer");
	define("DOWNLOAD_OPTIONS_MSG", "Nedlastbar / Programvare valg");
	define("DOWNLOADABLE_MSG", "Nedlastbare (Programvare)");
	define("DOWNLOADABLE_DESC", "For nedlastbar produkt kan du ogsе spesifisere 'Nedlastingsperiode', 'Spor til nedlastbar fil' og 'Aktiveringsvalg' ");
	define("DOWNLOAD_PERIOD_MSG", "Nedlastingsperiode");
	define("DOWNLOAD_PATH_MSG", "Spor til nedlastbar fil");
	define("DOWNLOAD_PATH_DESC", "du kan legge til flere spor skilt med semikolon");
	define("UPLOAD_SELECT_MSG", "Velg fil for opplasting og trykk {button_name} tasten.");
	define("UPLOADED_FILE_MSG", "Filen <b>{filename}</b> har blitt lastet opp.");
	define("UPLOAD_SELECT_ERROR", "Vennligst velg en fil fшrst.");
	define("UPLOAD_IMAGE_ERROR", "Kun bildefiler er tillatt.");
	define("UPLOAD_FORMAT_ERROR", "Denne type filer er ikke tillatt.");
	define("UPLOAD_SIZE_ERROR", "Filer stшrre enn {filesize} er ikke tillatt.");
	define("UPLOAD_DIMENSION_ERROR", "Bilder stшrre enn {dimension} er ikke tillatt.");
	define("UPLOAD_CREATE_ERROR", "Systemet kan ikke opprette denne filen.");
	define("UPLOAD_ACCESS_ERROR", "Du har ikke tillatelse til е laste opp filer.");
	define("DELETE_FILE_CONFIRM_MSG", "Er du sikker pе at du vil slette denne filen?");
	define("NO_FILES_MSG", "Ingen filer ble funnet");
	define("SERIAL_GENERATE_MSG", "Generer serienummer");
	define("SERIAL_DONT_GENERATE_MSG", "Ikke generer");
	define("SERIAL_RANDOM_GENERATE_MSG", "Generer tilfeldig serienummer for programvare produkter");
	define("SERIAL_FROM_PREDEFINED_MSG", "Fе serienummer fra forhеndsbestemt liste");
	define("SERIAL_PREDEFINED_MSG", "Forhеndsbestemt serienmummer");
	define("SERIAL_NUMBER_COLUMN", "Serienummer");
	define("SERIAL_USED_COLUMN", "Brukt");
	define("SERIAL_DELETE_COLUMN", "Slett");
	define("SERIAL_MORE_MSG", "Tilfшye flere serienumre?");
	define("SERIAL_PERIOD_MSG", "Serienummer periode");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Vis bestemmelser & betingelser");
	define("DOWNLOAD_SHOW_TERMS_DESC", "For е nedlaste dette produktet mе brukeren lese og godta vеre bestemmelser og betingelser");
	define("DOWNLOAD_TERMS_MSG", "Bestemmelser & betingelser");
	define("DOWNLOAD_TERMS_USER_DESC", "Jeg har lest og godtatt bestemmelsene og betingelsene deres");
	define("DOWNLOAD_TERMS_USER_ERROR", "For е nedlaste dette produktet mе du lese og godta vеre bestemmelser og betingelser");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>