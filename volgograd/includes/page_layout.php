<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  page_layout.php                                          ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// initialize template class
	$t = new VA_Template($settings["templates_dir"]);
	
	mb_internal_encoding("UTF-8");

	/*
	*special site params
	*/

	$sql = "SELECT site_name, site_name_i, site_name_r, site_name_d, site_name_v, site_name_p FROM va_sites WHERE site_id = " . $site_id;
	$db->query($sql);
	if ($db->next_record()) {
		$s_name = $db->f("site_name");
		$s_name_i = $db->f("site_name_i");
		$s_name_r = $db->f("site_name_r");
		$s_name_d = $db->f("site_name_d");
		$s_name_v = $db->f("site_name_v");
		$s_name_p = $db->f("site_name_p");

		$t->set_var("site_name_main", $s_name);
		$t->set_var("site_name_i", $s_name_i);
		$t->set_var("site_name_r", $s_name_r);
		$t->set_var("site_name_d", $s_name_d);
		$t->set_var("site_name_v", $s_name_v);
		$t->set_var("site_name_p", $s_name_p);

		$siteTagsArray = array($s_name, $s_name_i, $s_name_r, $s_name_d, $s_name_v, $s_name_p);
		$siteReplacement = array("site_name_main", "site_name_i", "site_name_r", "site_name_d", "site_name_v", "site_name_p");

	}

	// get and set global values
	$site_url = get_setting_value($settings, "site_url", "");
	$secure_url = get_setting_value($settings, "secure_url", "");
	if ($is_ssl) {
		$absolute_url = $secure_url;
	} else {
		$absolute_url = $site_url;
	}
	$parsed_url = parse_url($site_url);
	$site_path = isset($parsed_url["path"]) ? $parsed_url["path"] : "/";
	
	$css_file = "";
	if (strlen(get_setting_value($settings, "style_name", ""))) {
		$css_file  = $absolute_url;
		$css_file .= "styles/" . get_setting_value($settings, "style_name");
		if (strlen(get_setting_value($settings, "scheme_name", ""))) {
			$css_file .= "_" . get_setting_value($settings, "scheme_name");
		}
		$css_file .= ".css";
	}

	$t->set_var("CHARSET", CHARSET);
	$t->set_var("meta_language", $language_code);
	$t->set_var("site_url", $site_url);
	$t->set_var("secure_url", $secure_url);
	$t->set_var("absolute_url", $absolute_url);
	$t->set_var("css_file", $css_file);
	if (isset($current_page)) {
		$t->set_var("current_href", $current_page);
	}

	// add google analytics code to hidden blocks
	$google_analytics = get_setting_value($settings, "google_analytics", 0);
	$google_tracking_code = get_setting_value($settings, "google_tracking_code", "");
	if ($google_analytics && $google_tracking_code) {
		$t->set_file("head_tag", "ga.html");
		$t->set_var("google_tracking_code", $google_tracking_code);
		
		$cookie_control = get_session("cookie_control");
		
		if($cookie_control == 1){
		
			$t->set_var("disable_google_cookies", "window['ga-disable-" . $google_tracking_code . "'] = true;");

		}
	}

	if (isset($debug_mode) && $debug_mode) {
		$t->set_var("debug_buffer", $debug_buffer);
	}

	// check page settings id	
	$sql  = " SELECT cps.* ";
	$sql .= " FROM (" . $table_prefix . "cms_pages_settings cps ";
	$sql .= " INNER JOIN " . $table_prefix . "cms_pages cp ON cp.page_id=cps.page_id) ";
	if (isset($cms_ps_id) && strlen($cms_ps_id)) {
		$sql .= " WHERE cps.ps_id=" . $db->tosql($cms_ps_id, INTEGER);
	} else {
		$sql .= " WHERE cp.page_code=" . $db->tosql($cms_page_code, TEXT);
		$sql .= " AND (cps.key_code='' OR cps.key_code IS NULL) ";
		$sql .= " AND (cps.key_type='' OR cps.key_type IS NULL) ";
		if (isset($site_id) && $site_id != 1) {
			$sql .= " AND (cps.site_id=1 OR cps.site_id=" . $db->tosql($site_id, INTEGER) . ") ";
		} else {
			$sql .= " AND cps.site_id=1 ";
		}
		$sql .= " ORDER BY cps.site_id DESC ";
	}
	$db->query($sql);
	if ($db->next_record()) {
		$ps_id = $db->f("ps_id");
		$layout_id = $db->f("layout_id");
		if (!isset($meta_title) || !strlen($meta_title)) {
			$meta_title = get_translation($db->f("meta_title"));
		}
		if (!isset($meta_keywords) || !strlen($meta_keywords)) {
			$meta_keywords = get_translation($db->f("meta_keywords"));
		}
		if (!isset($meta_description) || !strlen($meta_description)) {
			$meta_description = $db->f("meta_description");
		}
	} else {
		echo "Page <b>".$cms_page_code."</b> wasn't found.";
		exit;
	}

	// get layout template 
	$sql  = " SELECT * FROM " . $table_prefix . "cms_layouts ";
	$sql .= " WHERE layout_id=" . $db->tosql($layout_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$layout_template = $db->f("layout_template");
	}	

	// set layout
	$t->set_file("main", $layout_template);
	// set head tags
	set_head_tag("base", array("href"=>$absolute_url), "href", 1);
	if ($css_file) {
		set_link_tag($css_file, "stylesheet", "text/css");
	}
	set_script_tag("js/menu.js");
	set_script_tag("js/ajax.js");
	set_script_tag("js/blocks.js");
	set_script_tag("js/compare.js");
	set_script_tag("js/shopping.js");

	// get frames settings
	$frames = array();
	$sql  = " SELECT * FROM " . $table_prefix . "cms_frames ";
	$sql .= " WHERE layout_id=" . $db->tosql($layout_id, INTEGER);
	$db->query($sql);
	while ($db->next_record()) {
		$frame_id = $db->f("frame_id");
		$tag_name = $db->f("tag_name");
		// initialize all frames for layouts with empty values in case there are no saved settings
		$frames[$frame_id] = array(
			"tag_name" => $tag_name, "blocks" => 0, 
			"frame_style" => "", "html_frame_start" => "", 
			"html_between_blocks" => "", "html_before_block" => "", 
			"html_after_block" => "", "html_frame_end" => "", 
		);
	}	
	$sql  = " SELECT * FROM " . $table_prefix . "cms_frames_settings ";
	$sql .= " WHERE ps_id=" . $db->tosql($ps_id, INTEGER);
	$db->query($sql);
	while ($db->next_record()) {
		$frame_id = $db->f("frame_id");
		if (isset($frames[$frame_id])) {
			$tag_name = $frames[$frame_id]["tag_name"];
			$frames[$frame_id] = $db->Record;
			$frames[$frame_id]["tag_name"] = $tag_name;
			$frames[$frame_id]["blocks"] = 0;
			$t->set_var($tag_name."_style", $frames[$frame_id]["frame_style"]);
			$t->set_var($tag_name, $frames[$frame_id]["html_frame_start"]);
		}
	}	

	// get page blocks
	$page_blocks = array();
	$sql  = " SELECT cpb.pb_id, cb.block_code, cb.php_script, cpb.frame_id, cpb.block_key, ";
	$sql .= " cpb.tag_name, cpb.html_template, cpb.block_style, cpb.css_class, ";
	$sql .= " cb.block_title, cpb.block_title AS page_block_title ";
	$sql .= " FROM (" . $table_prefix . "cms_pages_blocks cpb ";
	$sql .= " INNER JOIN " . $table_prefix . "cms_blocks cb ON cpb.block_id=cb.block_id) ";
	$sql .= " WHERE cpb.ps_id=" . $db->tosql($ps_id, INTEGER);
	$sql .= " ORDER BY cpb.block_order ";
	$db->query($sql);
	while ($db->next_record()) {
		$pb_id = $db->f("pb_id");
		$page_blocks[$pb_id] = $db->Record;
		$page_blocks[$pb_id]["vars"] = array();
		$page_blocks[$pb_id]["periods"] = false;
		$page_blocks[$pb_id]["period_active"] = false;
	}

	// get blocks variables
	$sql  = " SELECT cbs.pb_id, cbs.variable_name, cbs.variable_value ";
	$sql .= " FROM " . $table_prefix . "cms_blocks_settings cbs ";
	$sql .= " WHERE cbs.ps_id=" . $db->tosql($ps_id, INTEGER);
	$db->query($sql);
	while ($db->next_record()) {
		$pb_id = $db->f("pb_id");
		if (isset($page_blocks[$pb_id])) {
			$variable_name = $db->f("variable_name");
			$variable_value = $db->f("variable_value");
			if (isset($page_blocks[$pb_id]["vars"][$variable_name])) {
				if (is_array($page_blocks[$pb_id]["vars"][$variable_name])) {
					$page_blocks[$pb_id]["vars"][$variable_name][] = $variable_value;
				} else {
					$page_blocks[$pb_id]["vars"][$variable_name] = array($page_blocks[$pb_id]["vars"][$variable_name]);
					$page_blocks[$pb_id]["vars"][$variable_name][] = $variable_value;
				}
			} else {
				$page_blocks[$pb_id]["vars"][$variable_name] = $variable_value;
			}
		}
	}

	// some vars to check periods
	$current_date = va_time();
	$current_ts = va_timestamp();
	$check_time = $current_date[HOUR] * 60 + $current_date[MINUTE];
	$week_values = array(
		"1" => 1, "2" => 2, "3" => 4, "4" => 8, "5" => 16, "6" => 32, "0" => 64,
	);
	$day_value = $week_values[date("w", $current_ts)];

	// get and check blocks periods
	$sql  = " SELECT cbp.pb_id, cbp.start_date, cbp.end_date, cbp.start_time, cbp.end_time, cbp.week_days ";
	$sql .= " FROM " . $table_prefix . "cms_blocks_periods cbp ";
	$sql .= " WHERE cbp.ps_id=" . $db->tosql($ps_id, INTEGER);
	$db->query($sql);
	while ($db->next_record()) {
		$pb_id = $db->f("pb_id");
		if (isset($page_blocks[$pb_id])) {
			$page_blocks[$pb_id]["periods"] = true; // has time periods to show this block on page

			$start_date_ts = 0;
			$start_date = $db->f("start_date", DATETIME);
			if (is_array($start_date)) {
				$start_date_ts = va_timestamp($start_date);
			}

			$end_date_ts = 0;
			$end_date = $db->f("end_date", DATETIME);
			if (is_array($end_date)) {
				$end_date[HOUR] = 23; $end_date[MINUTE] = 59; $end_date[SECOND] = 59;
				$end_date_ts = va_timestamp($end_date);
			}
			$start_time = $db->f("start_time");
			$end_time = $db->f("end_time");
			$week_days = $db->f("week_days");

			if ($current_ts >= $start_date_ts && ($current_ts <= $end_date_ts || !$end_date_ts) &&
				$check_time >= $start_time && ($check_time <= $end_time || !$end_time) &&
				($day_value&$week_days)
			) {
				$page_blocks[$pb_id]["period_active"] = true;
			}
		}
	}

	// parse blocks
	foreach ($page_blocks as $pb_id => $block) {
		// check if there are time periods to show this block
		if($block["periods"] && !$block["period_active"]) {
			continue;
		}
		$frame_id = $block["frame_id"];
		$frame_tag_name = $frames[$frame_id]["tag_name"];
		$php_script = $block["php_script"];
		$cms_block_code = $block["block_code"];
		$block_tag_name = $block["tag_name"];
		$html_template = $block["html_template"];
		$block_style = $block["block_style"];
		$block_class = $block["css_class"];
		$block_title = $block["block_title"];
		$page_block_title = $block["page_block_title"];
		if (strlen($page_block_title)) { $block_title = $page_block_title; }
		$vars = array();
		$vars = $block["vars"];
		$vars["block_key"] = $block["block_key"];
		$vars["tag_name"] = $frames[$frame_id]["tag_name"];
		$default_title = ""; // always clear default title before parse
		$block_parsed = false;
		// set global block vars
		$t->set_var("pb_id", $pb_id);
		$t->set_var("block_style", $block_style);
		// 
		$t->set_var("block_class", $block_class);
		$t->set_var("block_title", get_translation($block_title));

		if (file_exists("./blocks_custom/".$php_script)) {
			include("./blocks_custom/".$php_script);
		} else {
			include("./blocks/".$php_script);
		}
		if ($block_parsed) {

			if (!strlen($block_tag_name)) {
				// parse frame data only for frame blocks 
				$frames[$frame_id]["blocks"]++;
				if ($frames[$frame_id]["blocks"] > 1) {
					$t->set_var("frame_code", $frames[$frame_id]["html_between_blocks"]);
					$t->copy_var("frame_code", $tag_name, true);
				}
				$t->set_var("frame_code", $frames[$frame_id]["html_before_block"]);
				$t->copy_var("frame_code", $tag_name, true);
			}

			// set block global vars
			$tag_name = strlen($block_tag_name) ? $block_tag_name : $frame_tag_name;
			if (!strlen($block_title)) { $block_title = $default_title; }
			$accumulate_parse = true;
			if (strlen($block_tag_name)) { $accumulate_parse = false; }

			// set final block title
			$t->set_block("block_title", get_translation($block_title));
			$t->parse("block_title", false);
			// check if we need to hide block title
			$parsed_title = $t->get_var("block_title");
			if (!strlen($parsed_title)) { $block_class = "hidden-title ". $block_class; }
			$t->set_var("block_class", $block_class);

			$t->parse_to("block_body", $tag_name, $accumulate_parse);

			if (!strlen($block_tag_name)) {
				// parse frame data only for frame blocks 
				$t->set_var("frame_code", $frames[$frame_id]["html_after_block"]);
				$t->copy_var("frame_code", $tag_name, true);
			}
		}
	}

	// close frames 
	foreach ($frames as $frame_id => $frame) {
		$tag_name = $frames[$frame_id]["tag_name"];
		$t->set_var("frame_code", $frames[$frame_id]["html_frame_end"]);
		$t->copy_var("frame_code", $tag_name, true);
	}	

	// check if auto data has to be applied
	if (!strlen($meta_title)) { 
		if (isset($auto_meta_title) && strlen($auto_meta_title)) {
			$meta_title = strip_tags($auto_meta_title); 
		} else {
			$meta_title = get_setting_value($settings, "site_name"); 
		}
	}
	if (!strlen($meta_description) && isset($auto_meta_description)) { 
		$meta_description = get_meta_desc($auto_meta_description); 
	}
/**
*prapere data
*/

$meta_title = parseSiteTags($meta_title);
$meta_keywords = parseSiteTags($meta_keywords);
$meta_description = parseSiteTags($meta_description);

	// set some meta data
	$t->set_var("meta_title", get_translation($meta_title));
	if ($meta_keywords) {
		set_head_tag("meta", array("name"=>"keywords","content"=>$meta_keywords), "name", 1);
	}
	if ($meta_description) {
		set_head_tag("meta", array("name"=>"description","content"=>$meta_description), "name", 1);
	}
	if (isset($canonical_url) && strlen($canonical_url)) {
		set_link_tag(htmlspecialchars($canonical_url), "canonical", "");
	}
	if ($google_analytics && $google_tracking_code) {
		$t->parse_to("head_tag", "head_tags");
	}

	// parse page content
	$t->pparse("main");



?>