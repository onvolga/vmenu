<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Onjuiste download parameter(s)");
	define("DOWNLOAD_MISS_PARAM", "Ontbrekende download parameter(s)");
	define("DOWNLOAD_INACTIVE", "Download niet actief");
	define("DOWNLOAD_EXPIRED", "Uw download periode is verstreken");
	define("DOWNLOAD_LIMITED", "U heeft het mamixmum aantal downloads bereikt");
	define("DOWNLOAD_PATH_ERROR", "Het pad naar dit product kan niet worden gevonden");
	define("DOWNLOAD_RELEASE_ERROR", "Uitgave niet gevonden");
	define("DOWNLOAD_USER_ERROR", "Alleen geregistreerde gebruikers kunnen dit bestand downloaden");
	define("ACTIVATION_OPTIONS_MSG", "Activatie opties");
	define("ACTIVATION_MAX_NUMBER_MSG", "Maximum aantal activaties");
	define("DOWNLOAD_OPTIONS_MSG", "Ebook bestand");
	define("DOWNLOADABLE_MSG", "Beschikbaar maken");
	define("DOWNLOADABLE_DESC", "Let op: u kunt hier alleen Mobipocket (.prc) bestanden uploaden!");
	define("DOWNLOAD_PERIOD_MSG", "Download periode");
	define("DOWNLOAD_PATH_MSG", "Upload ebook");
	define("DOWNLOAD_PATH_DESC", "Mobipocket .prc bestand");
	define("UPLOAD_SELECT_MSG", "Selecteer bestand en klik op {button_name}.");
	define("UPLOADED_FILE_MSG", "Bestand <b>{filename}</b> is ontvangen");
	define("UPLOAD_SELECT_ERROR", "Selecteer eerst een bestand");
	define("UPLOAD_IMAGE_ERROR", "Alleen afbeeldingen zijn toegestaan");
	define("UPLOAD_FORMAT_ERROR", "U kunt voor afbeeldingen alleen .gif en .jpg bestanden uploaden.<br><br>Ebooks dienen het Mobipocket (.prc) formaat te hebben.");
	define("UPLOAD_SIZE_ERROR", "Bestanden groter dan {filesize} zijn niet toegestaan");
	define("UPLOAD_DIMENSION_ERROR", "Afbeeldingen groter dan {dimension} zijn niet toegestaan");
	define("UPLOAD_CREATE_ERROR", "Systeem kan het bestand niet aanmaken");
	define("UPLOAD_ACCESS_ERROR", "U heeft geen rechten om bestanden te uploaden");
	define("DELETE_FILE_CONFIRM_MSG", "Weet u zeker dat u dit bestand wilt verwijderen?");
	define("NO_FILES_MSG", "Geen bestanden gevonden");
	define("SERIAL_GENERATE_MSG", "Genereer serienummer");
	define("SERIAL_DONT_GENERATE_MSG", "don't generate");
	define("SERIAL_RANDOM_GENERATE_MSG", "generate random serial for software product");
	define("SERIAL_FROM_PREDEFINED_MSG", "get serial number from predefined list");
	define("SERIAL_PREDEFINED_MSG", "Predefined Serial Numbers");
	define("SERIAL_NUMBER_COLUMN", "Serial Number");
	define("SERIAL_USED_COLUMN", "Used");
	define("SERIAL_DELETE_COLUMN", "Delete");
	define("SERIAL_MORE_MSG", "Add more serial numbers?");
	define("SERIAL_PERIOD_MSG", "Serial Number Period");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Show Terms & Conditions");
	define("DOWNLOAD_SHOW_TERMS_DESC", "To download the product user has to read and agree to our terms and conditions");
	define("DOWNLOAD_TERMS_MSG", "Terms & Conditions");
	define("DOWNLOAD_TERMS_USER_DESC", "I have read and agree to your terms and conditions");
	define("DOWNLOAD_TERMS_USER_ERROR", "To download the product you have to read and agree to our terms and conditions");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>