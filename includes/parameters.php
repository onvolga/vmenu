<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  parameters.php                                           ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	$parameters = array(
		"name", "first_name", "last_name", "company_id", "company_name", "email",
		"address1", "address2", "city", "province", "state_id", "zip", "country_id",
		"phone", "daytime_phone", "evening_phone", "cell_phone", "fax", "additional_comments", "req"
	);

	$cc_parameters = array(
		"cc_name", "cc_first_name", "cc_last_name", "cc_number", "cc_start_date", "cc_expiry_date", 
		"cc_type", "cc_issue_number", "cc_security_code", "pay_without_cc"
	);

	$additional_parameters = array(
		"nickname", "friendly_url", "paypal_account", "msn_account", "icq_number", "user_site_url", "short_description", "full_description", "is_hidden"
	);
	
	$merchant_add_parameters = array("user_site", "work_with", "minimal_order", "user_shipping_minimal", "free_shipping_condition", "warranty_and_returning", "remark");

    $merchant_legal_parameters = array("legal_name", "tax_id", "ogrn", "legal_address", "letters_address", "contact_person_phone");

	$call_center_user_parameters = array(
		"user_id", "name", "first_name", "last_name", "company_id", "company_name", "email", 
		"address1", "address2", "city", "province", "state_id", "zip", "country_id", 
		"phone", "daytime_phone", "evening_phone", "cell_phone", "fax",
		"delivery_name", "delivery_first_name", "delivery_last_name", "delivery_company_id", 
		"delivery_company_name", "delivery_email", "delivery_address1", "delivery_address2", 
		"delivery_city", "delivery_province", "delivery_state_id", "delivery_zip", 
		"delivery_country_id", "delivery_phone", "delivery_daytime_phone", 
		"delivery_evening_phone", "delivery_cell_phone", "delivery_fax", "additional_comments", "req", "delivery_additional_comments", "delivery_req"
	);

	$phone_parameters = array(
		"phone", "daytime_phone", "evening_phone", "cell_phone", "fax",
		"delivery_phone", "delivery_daytime_phone", "delivery_evening_phone", "delivery_cell_phone", "delivery_fax",
	);
	
?>