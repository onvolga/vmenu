<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  preview.php                                              ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

	
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/sorter.php");
	//session_start();
	
	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","preview.html");
	
	$style_name=get_param("style_name");
	
	$t->set_var("css_path", "../styles/".$style_name.".css");
	
	$preview_html = $_SESSION['preview_html'];
	$t->set_var("preview_block", $preview_html);

	$t->pparse("main");
?>
	
