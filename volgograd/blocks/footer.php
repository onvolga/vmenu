<?php

	$friendly_urls = get_setting_value($settings, "friendly_urls", 0);
	$friendly_extension = get_setting_value($settings, "friendly_extension", "");

	$user_id = get_session("session_user_id");		
	$user_info = get_session("session_user_info");
	$user_type_id = get_setting_value($user_info, "user_type_id", "");
	
	$t->set_template_path($settings["templates_dir"]);
	$t->set_file("block_body", "footer.html");
	$t->set_var("site_url", $settings["site_url"]);

	$t->set_var("index_href", get_custom_friendly_url("index.php"));
	$t->set_var("products_href", get_custom_friendly_url("products.php"));
	$t->set_var("basket_href", get_custom_friendly_url("basket.php"));
	$t->set_var("user_profile_href", get_custom_friendly_url("user_profile.php"));
	$t->set_var("admin_href", "admin.php");
	$footer_where = get_session("session_user_id") ? " access_level=1 " : " guest_access_level=1 ";
	$sql  = " SELECT fl.menu_title, fl.menu_url, fl.menu_target, fl.onclick_code ";
	$sql .= " FROM " . $table_prefix . "footer_links fl ";
	$sql .= " WHERE " . $footer_where ;
	$sql .= " ORDER BY fl.menu_order ";
	$db->query($sql);
	if ($db->next_record()) {
		$t->set_var("page_separator", "");
		do {
			$menu_title = get_translation($db->f("menu_title"));
			$menu_url = $db->f("menu_url");
			$menu_friendly_url = get_custom_friendly_url($menu_url);
			$menu_target = $db->f("menu_target");
			$onclick_code = $db->f("onclick_code");

			if ($menu_url == "index.php") {
				$menu_url = $site_url;
			} if (preg_match("/^\//", $menu_url)) {
				$menu_url = preg_replace("/^".preg_quote($site_path, "/")."/i", "", $menu_url);
				$menu_url = $site_url . get_custom_friendly_url($menu_url);
			} else if (!preg_match("/^http\:\/\//", $menu_url) && !preg_match("/^https\:\/\//", $menu_url) && !preg_match("/^javascript\:/", $menu_url)) {
				$menu_url = $site_url . $menu_friendly_url;
			}

			$t->set_var("menu_title", htmlspecialchars($menu_title));
			$t->set_var("menu_url", htmlspecialchars($menu_url));
			if ($menu_target) {
				$t->set_var("menu_target", "target=\"".htmlspecialchars($menu_target)."\"");
			} else {
				$t->set_var("menu_target", "");
			}
			$t->set_var("onclick_code", $onclick_code);

			$t->sparse("footer_links");
			$t->sparse("page_separator", false);
		} while($db->next_record());
	} else {
		$t->set_var("custom_pages", "");
	}
	if ($settings["html_below_footer"]) {
		$html_below_footer = get_translation($settings["html_below_footer"]);
		if (get_setting_value($settings, "php_in_footer_body", 0)) {
			eval_php_code($html_below_footer);
		}
		$t->set_block("footer_html", $html_below_footer);
		$t->parse("footer_html", false);

		if ($t->block_exists("footer_block")) {
			$t->parse("footer_block", false);
		}
	} else {
		$t->set_var("footer_block", "");
	}

	$block_parsed = true;

?>