<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "מדריכים");
	define("NO_MANUALS_MSG", "לא נמצאו מדריכים");
	define("NO_MANUAL_ARTICLES_MSG", "אין פריטים");
	define("MANUALS_PREV_ARTICLE", "קודם");
	define("MANUALS_NEXT_ARTICLE", "הבא");
	define("MANUAL_CONTENT_MSG", "אידקס");
	define("MANUALS_SEARCH_IN_MSG", "חפש ב");
	define("MANUALS_SEARCH_FOR_MSG", "חפש ב");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "כל המדריכים");
	define("MANUALS_SEARCH_RESULTS_INFO", "נמצאו {results_number} פריט(ים) ל- {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "חפש בתוצאות");
	define("MANUALS_NOT_FOUND_ANYTHING", "לא נמצא דבר ל- '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "ללא תוכן");
	define("MANUALS_SEARCH_TITLE", "חיפוש במדריך");
	define("MANUALS_SEARCH_RESULTS_TITLE", "תוצאות חיפוש במדריך");

?>