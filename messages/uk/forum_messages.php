<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// повідомлення форуму
	define("FORUM_TITLE", "Форум");
	define("TOPIC_INFO_TITLE", "Загальна iнформацiя по темi");
	define("TOPIC_MESSAGE_TITLE", "Повідомлення");

	define("MY_FORUM_TOPICS_MSG", "Мої теми на форумі");
	define("ALL_FORUM_TOPICS_MSG", "Всi теми форуму");
	define("MY_FORUM_TOPICS_DESC", "Можливо така проблема як у Вас вже обговорювалась та була вирішена. Чи не бажаєте ви подiлитись вашим досвiдом з iншими користувачами? Пропонуємо Вам стати користувачем форуму i стати частиною спiльноти користувачів.");
	define("NEW_TOPIC_MSG", "Нова тема");
	define("NO_TOPICS_MSG", "Жодної теми не знайдено");
	define("FOUND_TOPICS_MSG", "Знайдено <b>{found_records}</b> тем, що вiдповiдають запиту '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Не знайдено жодного форуму");

	define("FORUM_NAME_COLUMN", "Форум");
	define("FORUM_TOPICS_COLUMN", "Тема");
	define("FORUM_REPLIES_COLUMN", "Вiдповiдей");
	define("FORUM_LAST_POST_COLUMN", "Останнi змiни");
	define("FORUM_MODERATORS_MSG", "Модератори");

	define("TOPIC_NAME_COLUMN", "Тема");
	define("TOPIC_AUTHOR_COLUMN", "Автор");
	define("TOPIC_VIEWS_COLUMN", "Переглядiв");
	define("TOPIC_REPLIES_COLUMN", "Вiдповiдей");
	define("TOPIC_UPDATED_COLUMN", "Останнi змiни");
	define("TOPIC_ADDED_MSG", "Дякуємо<br>Ваша тема була додана");

	define("TOPIC_ADDED_BY_FIELD", "Ким додана тема");
	define("TOPIC_ADDED_DATE_FIELD", "Коли додана тема");
	define("TOPIC_UPDATED_FIELD", "Останнi змiни");
	define("TOPIC_NICKNAME_FIELD", "Псевдонiм");
	define("TOPIC_EMAIL_FIELD", "Ваша електронна адреса");
	define("TOPIC_NAME_FIELD", "Тема");
	define("TOPIC_MESSAGE_FIELD", "Повiдомлення");
	define("TOPIC_NOTIFY_FIELD", "Надсилати всi вiдповiдi на мою елекронну адресу");

	define("ADD_TOPIC_BUTTON", "Додати тему");
	define("TOPIC_MESSAGE_BUTTON", "Додати повiдомлення");

	define("TOPIC_MISS_ID_ERROR", "Пропущено параметр <b>ID повідомлення</b>.");
	define("TOPIC_WRONG_ID_ERROR", "Параметер <b>ID повідомлення</b> має хибне значення.");
	define("FORUM_SEARCH_MESSAGE", "Знайдено {search_count} повідомлень що відповідають запиту '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Перегляд");
	define("TOPIC_SAVE_BUTTON", "Зберегти");

	define("LAST_POST_ON_SHORT_MSG", "На:");
	define("LAST_POST_IN_SHORT_MSG", "У:");
	define("LAST_POST_BY_SHORT_MSG", "Від:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Останні зміни:");

?>