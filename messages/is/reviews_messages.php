<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "ValfrjГЎls");
	define("BAD_MSG", "MjГ¶g lГ©leg");
	define("POOR_MSG", "Frekar lГ©leg");
	define("AVERAGE_MSG", "Hvorki nГ©");
	define("GOOD_MSG", "Frekar gГіГ°");
	define("EXCELLENT_MSG", "MjГ¶g gГіГ°");
	define("REVIEWS_MSG", "ГЃlit");
	define("NO_REVIEWS_MSG", "Engin ГЎlit fundust");
	define("WRITE_REVIEW_MSG", "Skrifa ГЎlit");
	define("RATE_PRODUCT_MSG", "Gefa Гѕessari vГ¶ru einkunn");
	define("RATE_ARTICLE_MSG", "Gefa Гѕessari grein einkunn");
	define("NOT_RATED_PRODUCT_MSG", "VГ¶ru hefur ekki veriГ° gefin einkunn");
	define("NOT_RATED_ARTICLE_MSG", "Grein hefur ekki veriГ° gefin einkunn");
	define("AVERAGE_RATING_MSG", "MeГ°aleinkunn gefin af viГ°skiptavinum");
	define("BASED_ON_REVIEWS_MSG", "reiknaГ° Гєt frГЎ {total_votes} ГЎlitum");
	define("POSITIVE_REVIEW_MSG", "JГЎkvГ¦Г° ГЎlit viГ°skiptavina");
	define("NEGATIVE_REVIEW_MSG", "NeikvГ¦Г° ГЎlit viГ°skiptavina");
	define("SEE_ALL_REVIEWS_MSG", "SjГЎ Г¶ll ГЎlit viГ°skiptavina..");
	define("ALL_REVIEWS_MSG", "Г–ll ГЎlit");
	define("ONLY_POSITIVE_MSG", "AГ°eins jГЎkvГ¦Г°");
	define("ONLY_NEGATIVE_MSG", "AГ°eins neikvГ¦Г°");
	define("POSITIVE_REVIEWS_MSG", "JГЎkvГ¦Г° ГЎlit");
	define("NEGATIVE_REVIEWS_MSG", "NeikvГ¦Г° ГЎlit");
	define("SUBMIT_REVIEW_MSG", "Takk fyrir. <br> Athugasemdir ГѕГ­nar verГ°a skoГ°aГ°ar. VerГ°i ГѕГ¦r samГѕykktar munu ГѕГ¦r birtast ГЎ sГ­Г°unni okkar.<br> ViГ° ГЎskiljum okkur Гѕann rГ©tt aГ° hafna greinum stangist ГѕГ¦r ГЎ viГ° leiГ°arvГ­si okkar. ");
	define("ALREADY_REVIEW_MSG", "ГћГє hefur nГє Гѕegar sent inn ГЎlit");
	define("RECOMMEND_PRODUCT_MSG", "Myndir ГѕГє mГ¦la meГ°<br> Гѕessari vГ¶ru viГ° aГ°ra?");
	define("RECOMMEND_ARTICLE_MSG", "Myndir ГѕГє mГ¦la meГ°<br> Гѕessari grein viГ° aГ°ra? ");
	define("RATE_IT_MSG", "Gefa ГѕvГ­ einkunn");
	define("NAME_ALIAS_MSG", "Nafn eГ°a gГ¦lunafn");
	define("SHOW_MSG", "SГЅna");
	define("FOUND_MSG", "FundiГ°");
	define("RATING_MSG", "MeГ°aleinkunn");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>