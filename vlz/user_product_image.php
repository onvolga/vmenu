<?php

	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/editgrid.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");

	check_user_security("access_products");

	$cms_page_code = "user_product_image";
	$script_name   = "user_product_image.php";
	$current_page  = get_custom_friendly_url("user_product_image.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = "Изображение";

	include_once("./includes/page_layout.php");

?>