<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1                                                  ***
  ***      http://hlieviy.com                                              ***
  ***                                                                      ***
  ****************************************************************************
*/

	
	$type = "list";
	include_once("./includes/common.php");
	include_once("./includes/navigator.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./messages/" . $language_code . "/forum_messages.php");
	include_once("./messages/" . $language_code . "/manuals_messages.php");
	include_once("./messages/" . $language_code . "/reviews_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/previews_functions.php");
	include_once("./includes/items_properties.php");

	$cms_page_code = "merchants_ratings";
	$script_name = "merchants_ratings.php";
	$current_page = get_custom_friendly_url("merchants_ratings.php");
	$tax_rates = get_tax_rates();
	$page_friendly_url = ""; $page_friendly_params = array();

	include_once("./includes/page_layout.php");
	
?>
