<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Optional");
	define("BAD_MSG", "Schlecht");
	define("POOR_MSG", "DСЊrftig");
	define("AVERAGE_MSG", "Durchschnittlich");
	define("GOOD_MSG", "Gut");
	define("EXCELLENT_MSG", "Hervorragend");
	define("REVIEWS_MSG", "Erfahrungsberichte");
	define("NO_REVIEWS_MSG", "Keine Erfahrungsberichte gefunden");
	define("WRITE_REVIEW_MSG", "Schreiben Sie einen Erfahrungsbericht");
	define("RATE_PRODUCT_MSG", "Bewerten Sie dieses Produkt");
	define("RATE_ARTICLE_MSG", "Bewerten Sie diesen Produkt");
	define("NOT_RATED_PRODUCT_MSG", "Produkt ist noch nicht bewertet");
	define("NOT_RATED_ARTICLE_MSG", "Produkt ist noch nicht bewertet");
	define("AVERAGE_RATING_MSG", "Durchschnittliche Kundenbewertung");
	define("BASED_ON_REVIEWS_MSG", "basiert auf {total_votes} Erfahrungsberichten");
	define("POSITIVE_REVIEW_MSG", "Positiver Erfahrungsbericht");
	define("NEGATIVE_REVIEW_MSG", "Negativer Erfahrungsbericht");
	define("SEE_ALL_REVIEWS_MSG", "Alle Erfahrungsberichte ansehen");
	define("ALL_REVIEWS_MSG", "Alle Erfahrungsberichte");
	define("ONLY_POSITIVE_MSG", "Nur positive");
	define("ONLY_NEGATIVE_MSG", "Nur negative");
	define("POSITIVE_REVIEWS_MSG", "Positive Erfahrungsberichte");
	define("NEGATIVE_REVIEWS_MSG", "Negative Erfahrungsberichte");
	define("SUBMIT_REVIEW_MSG", "Vielen Dank<br>Ihre Kommentare werden nun СЊberprСЊft. Bei Freigabe werden sie auf unserer Seite erscheinen.<br>Wir behalten uns das Recht vor, jegliche Einsendungen zurСЊckzuweisen, die nicht unseren Richtlinien entsprechen.");
	define("ALREADY_REVIEW_MSG", "Sie haben bereits einen Erfahrungsbericht verfasst.");
	define("RECOMMEND_PRODUCT_MSG", "WСЊrden Sie dieses Produkt empfehlen?");
	define("RECOMMEND_ARTICLE_MSG", "WСЊrden Sie diesen Produkt empfehlen?");
	define("RATE_IT_MSG", "Bewerten");
	define("NAME_ALIAS_MSG", "Name oder Alias");
	define("SHOW_MSG", "Anzeigen");
	define("FOUND_MSG", "Gefunden");
	define("RATING_MSG", "Durchschnittliche Bewertung");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Nur registrierte Benutzer kС†nnen Ihre Review eingeben");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, es ist Ihnen nicht erlaub Reviewen einzugeben");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Erlaubt to Reviewen einzusehen");
	define("ALLOWED_POST_REVIEWS_MSG", "Erlaubt Reviewen einzugeben");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviewen BeschrРґnkungen");
	define("REVIEWS_PER_USER_MSG", "Anzahl der Reviewen pro Benutzer");
	define("REVIEWS_PER_USER_DESC", "Feld leerlassen wenn Sie die Anzahl der Reviewen pro Benutzer nicht limitieren wollen");
	define("REVIEWS_PERIOD_MSG", "Reviewen Zeitraum");
	define("REVIEWS_PERIOD_DESC", "Zeitraum in dem EinschrРґnkungen fСЊr Reviewen inkraft sind");

	define("AUTO_APPROVE_REVIEWS_MSG", "Automatisch genehmige Reviewen");
	define("BY_RATING_MSG", "Nach Rang");
	define("SELECT_REVIEWS_FIRST_MSG", "Bitte wРґhle erst Reviewen ");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "Sie sind im Begriff den Status von ausgewРґhlten Reviewen zu Рґndern '{status_name}'. Weitermachen?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Sind Sie sicher, daРЇ Sie die ausgewРґhltenn Reviewen lС†schen wollen ({total_reviews})?");

?>