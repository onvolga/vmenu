<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Manuaali");
	define("NO_MANUALS_MSG", "Ei manuaaleja");
	define("NO_MANUAL_ARTICLES_MSG", "Ei artikkeleitд");
	define("MANUALS_PREV_ARTICLE", "Edellinen");
	define("MANUALS_NEXT_ARTICLE", "Seuraava");
	define("MANUAL_CONTENT_MSG", "Hakemisto");
	define("MANUALS_SEARCH_IN_MSG", "Hae tддltд");
	define("MANUALS_SEARCH_FOR_MSG", "Hae tддltд");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Kaikki manuaalit");
	define("MANUALS_SEARCH_RESULTS_INFO", "Lцytyi{results_number} artikkelia haulla {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Hakutulokset");
	define("MANUALS_NOT_FOUND_ANYTHING", "Haulla ei lцytnyt mitддn'{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Ei sisдltцд");
	define("MANUALS_SEARCH_TITLE", "Manuaalien haku");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Manuaalien haun tulos");

?>