<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// kэlavuz iletileri
	define("MANUALS_TITLE", "Manueller");
	define("NO_MANUALS_MSG", "Bir manuel bulunamadэ");
	define("NO_MANUAL_ARTICLES_MSG", "Makale yok");
	define("MANUALS_PREV_ARTICLE", "Цnceki");
	define("MANUALS_NEXT_ARTICLE", "Sonraki");
	define("MANUAL_CONTENT_MSG", "Indeks");
	define("MANUALS_SEARCH_IN_MSG", "Эзinde ara");
	define("MANUALS_SEARCH_FOR_MSG", "Ara");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Tьm manueller");
	define("MANUALS_SEARCH_RESULTS_INFO", "{search_string} iзin {results_number} sonuз bulundu");
	define("MANUALS_SEARCH_RESULT_MSG", "Arama Sonuзlarэ");
	define("MANUALS_NOT_FOUND_ANYTHING", "{search_string}' iзin kayэt bulunamadэ");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Эзerik yok");
	define("MANUALS_SEARCH_TITLE", "Manuel aramasэ");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Manuel aramasэ sonuзlarэ");

?>