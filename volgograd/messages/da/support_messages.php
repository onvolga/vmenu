<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Support center");
	define("SUPPORT_REQUEST_INF_TITLE", "Anmod om information");
	define("SUPPORT_REPLY_FORM_TITLE", "Svar");

	define("MY_SUPPORT_ISSUES_MSG", "Mine support henvendelser");
	define("MY_SUPPORT_ISSUES_DESC", "Hvis du oplever nogle problemer med de produkter du har kГёbt, er vi klar til hjГ¦lpe. Venligt kontakt os pГҐ ved at klikke pГҐ ovenstГҐende link for at sende en hjГ¦lpe forespГёrgsel.");
	define("NEW_SUPPORT_REQUEST_MSG", "Ny henvendelse");
	define("SUPPORT_REQUEST_ADDED_MSG", "Mange tak<br>Vi behandler din forespГёrgsel og kontakter dig dig sГҐ hurtigt som muligt. ");
	define("SUPPORT_SELECT_DEP_MSG", "VГ¦lg Afdeling");
	define("SUPPORT_SELECT_PROD_MSG", "VГ¦lg Type");
	define("SUPPORT_SELECT_STATUS_MSG", "Se");
	define("SUPPORT_NOT_VIEWED_MSG", "Ikke set");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Set af bruger");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Set af administrator");
	define("SUPPORT_STATUS_NEW_MSG", "Ny");
	define("NO_SUPPORT_REQUEST_MSG", "Ingen sager fundet");

	define("SUPPORT_SUMMARY_COLUMN", "Status");
	define("SUPPORT_TYPE_COLUMN", "Type");
	define("SUPPORT_UPDATED_COLUMN", "Sidst opdateret");

	define("SUPPORT_USER_NAME_FIELD", "Dit navn");
	define("SUPPORT_USER_EMAIL_FIELD", "Din e-mail-adresse");
	define("SUPPORT_IDENTIFIER_FIELD", "Identifikator (Faktura #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "MiljГё (OS, database, web server, etc.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Afdeling");
	define("SUPPORT_PRODUCT_FIELD", "Produkt");
	define("SUPPORT_TYPE_FIELD", "Status");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Aktuel status");
	define("SUPPORT_SUMMARY_FIELD", "Г‰n-linjes resumГ©");
	define("SUPPORT_DESCRIPTION_FIELD", "Beskrivelse");
	define("SUPPORT_MESSAGE_FIELD", "Besked");
	define("SUPPORT_ADDED_FIELD", "TilfГёjet");
	define("SUPPORT_ADDED_BY_FIELD", "TilfГёjet af");
	define("SUPPORT_UPDATED_FIELD", "Senest opdateret");

	define("SUPPORT_REQUEST_BUTTON", "Send henvendelse");
	define("SUPPORT_REPLY_BUTTON", "Svar");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Mangler <b> Support ID</b> parameter");
	define("SUPPORT_MISS_CODE_ERROR", "Mangler <b> verificering</b> parameter");
	define("SUPPORT_WRONG_ID_ERROR", "<b> Support ID</b> paramter har forkert vГ¦rdi");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verificering<b/> parameter har forkert vГ¦rdi ");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Overskrifter");
	define("ORIGINAL_TEXT_MSG", "Originaltekst");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>