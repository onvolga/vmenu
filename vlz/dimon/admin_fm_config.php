<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_fm_config.php                                      ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

	
	$conf_root_dir = "..";
	$conf_array_dirs = array("templates", "styles", "images", "video", "previews");
	$conf_array_files_ext = array("gif", "jpg", "jpeg", "bmp", "png", "doc", "pdf", "xls", "html", "htm", "css", "swf", "flv", "avi", "asf", "wmv", "vma", "mpg", "mpeg");

	$tmp_dir_arch = "va_temp";
	
	$host = $admin_site_url;
	
	$array_text_files = array("html", "htm", "css");
	$array_image_file = array("gif", "jpg", "jpeg", "bmp", "png");
	$array_download_file = array("doc", "pdf", "xls", "swf", "flv", "avi", "asf", "wmv", "vma", "mpg", "mpeg");
	
	$array_archive_file = array("zip", "gz");
	
	
	

?>
