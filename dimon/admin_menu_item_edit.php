<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_menu_item_edit.php                                 ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

	include_once("./admin_config.php");
	include_once($root_folder_path."includes/common.php");
	include_once($root_folder_path . "includes/record.php");

	include_once("./admin_common.php");

	check_admin_security("layouts");

	$menu_id = get_param("menu_id");
	$return_page = get_param("return_page");
	if ($return_page == "") {
		$return_page = "admin_layouts.php";
	}

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","admin_menu_item_edit.html");
	
	$t->set_var("admin_upload_href", "admin_upload.php");
	$t->set_var("admin_select_href", "admin_select.php");
	
	// Get menu_id and check if it exists
	$sql  = " SELECT * ";
	$sql .= " FROM ".$table_prefix."menus ";
	$sql .= " WHERE menu_id = ".$db->tosql($menu_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$t->set_var("parent_menu_name", $db->f("menu_name"));
		$t->set_var("parent_menu_title", $db->f("menu_title"));
		$t->set_var("parent_menu_id", $menu_id);
	} else {
		header("Location: admin_custom_menus.php");
		exit;
	}

	$t->set_var("admin_href"      , "admin.php");
	$t->set_var("admin_layout_href", "admin_layout.php");
	$t->set_var("admin_menu_item_edit_href" , "admin_menu_item_edit.php");
	$t->set_var("admin_menu_href"  , "admin_menu_items.php?menu_id=".$menu_id);
	$t->set_var("admin_custom_menu_href", "admin_custom_menu.php");
	$t->set_var("admin_custom_menus_href", "admin_custom_menus.php");
	$t->set_var("admin_menu_items_href", "admin_menu_items.php");
	$t->set_var("CONFIRM_DELETE_JS", str_replace("{record_name}", ADMIN_MENU_ITEM_MSG, CONFIRM_DELETE_MSG));

	$r = new VA_Record($table_prefix . "menus_items");
	$r->return_page = "admin_menu_items.php?menu_id=" . $menu_id;
	
	$r->add_where("menu_item_id", INTEGER);
	$r->add_textbox("menu_id", INTEGER, MENU_ID_MSG);
	$r->add_textbox("menu_order", INTEGER, MENU_ORDER_MSG);
	$r->change_property("menu_order", REQUIRED, true);
	$r->add_textbox("menu_title", TEXT, MENU_TITLE_MSG);
	$r->add_textbox("menu_image", TEXT, MENU_IMAGE_MSG);
	$r->add_textbox("menu_image_active", TEXT, MENU_IMAGE_ACTIVE_MSG);
	$r->add_textbox("menu_url", TEXT, MENU_URL_MSG);
	$r->add_textbox("menu_prefix", TEXT, PREFIX_IMAGE_MSG);
	$r->add_textbox("menu_prefix_active", TEXT, ACTIVE_PREFIX_IMAGE_MSG);
	$r->change_property("menu_url", REQUIRED, true);
	$r->add_textbox("menu_target", TEXT, MENU_TARGET_MSG);	
	$r->add_checkbox("show_non_logged", INTEGER);
	$r->change_property("show_non_logged", DEFAULT_VALUE, 1);
	$r->add_checkbox("show_logged", INTEGER);
	$r->change_property("show_logged", DEFAULT_VALUE, 1);
	$r->add_textbox("menu_prefix_active", TEXT, ACTIVE_PREFIX_IMAGE_MSG);
	
	$menu = array();

	//-- parent items
	$menu_item_id = get_param("menu_item_id");
	$sql  = " SELECT * FROM " . $table_prefix . "menus_items ";
	$sql .= " WHERE menu_id=" . $db->tosql($menu_id, INTEGER);
	if (strlen($menu_item_id)) {
		$sql .= " AND menu_item_id<>" . $db->tosql($menu_item_id, INTEGER);
	}
	$sql .= " ORDER BY menu_path, menu_order ";
	$db->query($sql);
	
	while($db->next_record()) {
		$list_id = $db->f("menu_item_id");
		$parent_menu_id = $db->f("parent_menu_item_id");

		if ($parent_menu_id == "") {
			$parent_menu_id = 0;
		}

		$list_title = $db->f("menu_title");
		if (defined($list_title)) {
			$list_title = constant($list_title);
		}

		$menu_values = array(
			"menu_title" => $list_title, 
			"menu_url" => $db->f("menu_url"), 
			"menu_path" => $db->f("menu_path")
		);
		$menu[$list_id] = $menu_values;
		
		$menu[$parent_menu_id]["subs"][] = $list_id;
	}

	$items = array();
	build_menu(0);

	$r->add_select("parent_menu_item_id", TEXT, $items, PARENT_ITEM_MSG);
	$r->set_event(AFTER_INSERT, "build_menus_tree");
	$r->set_event(AFTER_UPDATE, "build_menus_tree");
	
	$r->process();

	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->set_var("menu_id", $menu_id);
	$t->pparse("main");

function spaces_level($level)
{
	$spaces = "";
	for ($i =1; $i <= $level; $i++) {
		$spaces .= "---";
	}
	return $spaces . " ";
}

/**
 * Get menu items data from database.
 */
function build_menus_tree()
{
	global $db, $table_prefix, $layout_id, $menu_id;

	// update menu links for new structure
	$items = array();
	$sql  = " SELECT menu_item_id, parent_menu_item_id FROM " . $table_prefix . "menus_items ";
	$sql .= " WHERE menu_id=" . $db->tosql($menu_id, INTEGER);
	$sql .= " ORDER BY menu_item_id ";
	$db->query($sql);
	while ($db->next_record()) {
		$menu_item_id = $db->f("menu_item_id");
		$parent_menu_item_id = $db->f("parent_menu_item_id");
		$items[$menu_item_id] = $parent_menu_item_id;
	}
	
	foreach ($items as $menu_item_id => $parent_menu_item_id) {
		if (!$parent_menu_item_id || $parent_menu_item_id == $menu_item_id) {
			$parent_menu_item_id = 0;
		}
		
		$menu_item_path = ""; 
		$current_parent_item_id = $parent_menu_item_id;

		while ($current_parent_item_id) {
			$menu_item_path = $current_parent_item_id.",".$menu_item_path;
			$parent_item_id = isset($items[$current_parent_item_id]) ? $items[$current_parent_item_id] : 0;
			if ($parent_item_id == $current_parent_item_id) {
				$current_parent_item_id = 0;
			} else {
				$current_parent_item_id = $parent_item_id;
			}
		}
		
		$sql  = " UPDATE " . $table_prefix . "menus_items SET ";
		$sql .= " parent_menu_item_id=" . $db->tosql($parent_menu_item_id, INTEGER) . ", ";
		$sql .= " menu_path=" . $db->tosql($menu_item_path, TEXT);
		$sql .= " WHERE menu_item_id=" . $db->tosql($menu_item_id, INTEGER);
		$db->query($sql);
	}	

}

/**
 * Build menu for item id hierarchy 
 */
function build_menu($parent_id) {

	global $t, $menu, $items, $menu_id, $selected_menu_item_id;
	if (!empty($menu)) {
		$subs = $menu[$parent_id]["subs"];
		for ($m = 0; $m < sizeof($subs); $m++) {
			$item_id = $subs[$m];
			if ($item_id != $selected_menu_item_id) {	
				$menu_path = $menu[$item_id]["menu_path"];
				$menu_title = $menu[$item_id]["menu_title"];
				if (!$menu_title) {
					$menu_title = $menu[$item_id]["menu_url"];
				}
				$menu_level = preg_replace("/\d/", "", $menu_path);
				$spaces = spaces_level(strlen($menu_level));
		
				$items[] = array($item_id, $spaces.$menu_title);
		
				if (isset($menu[$item_id]["subs"])) {
					build_menu($item_id);
				}
			}
		}
	}
}
?>
