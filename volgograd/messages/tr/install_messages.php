<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// kurulum iletileri
	define("INSTALL_TITLE", "ViArt SHOP Kurulumu");

	define("INSTALL_STEP_1_TITLE", "Kurulum: Adэm 1");
	define("INSTALL_STEP_1_DESC", "ViArt SHOP 'u seзtiрiniz iзin teюekkьr ederiz. Kurulum iюlemini tamamlamak iзin lьtfen aюaрэda istenen bilgileri doldurunuz. Unutmayэnэz ki seзtiрiniz veri tabanэ  цnceden kurulmuю olmalэdэr.  MS Access gibi bir ODBC kullanan veri tabanэ kuruyorsanэz , iюleme devam etmeden, цncelikle bir DSN oluюturmanэz gerekir.");
	define("INSTALL_STEP_2_TITLE", "Kurulum: Adэm 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Kurulum: Adэm 3");
	define("INSTALL_STEP_3_DESC", "Lьtfen bir site planэ seзiniz, sonradan planэ deрiюtirebilirsiniz.");
	define("INSTALL_FINAL_TITLE", "Kurulum: Son Adэm");
	define("SELECT_DATE_TITLE", "Tarih Formatэnэ Seз");

	define("DB_SETTINGS_MSG", "Veri tabanэ Ayarlarэ");
	define("DB_PROGRESS_MSG", "Database yapэlandэrma iюlemi.");
	define("SELECT_PHP_LIB_MSG", "PHP Kitaplэрэ Seз");
	define("SELECT_DB_TYPE_MSG", "Veritabanэ Tipi Seз");
	define("ADMIN_SETTINGS_MSG", "Yцnetim Ayarlarэ");
	define("DATE_SETTINGS_MSG", "Tarih Formatlarэ");
	define("NO_DATE_FORMATS_MSG", "Mevcut tarih formatэ yok");
	define("INSTALL_FINISHED_MSG", "Bu noktada ana kurulumunuz tamamlanmэю bulunuyor. Lьtfen yцnetim bцlьmь ayarlarэnэ kontrol ettiрinizden ve gerekli deрiюiklikleri yaptэрэnэzdan emin olun.");
	define("ACCESS_ADMIN_MSG", "Yцnetim bцlьmьne ulaюmak iзin buraya tэklayэnэz.");
	define("ADMIN_URL_MSG", "Yцnetim URL");
	define("MANUAL_URL_MSG", "Kэlavuz URL");
	define("THANKS_MSG", "<b>ViArt SHOP</b> u seзtiрiniz iзin teюekkьr ederiz.");

	define("DB_TYPE_FIELD", "Veri tabanэ tipi");
	define("DB_TYPE_DESC", "Kullandэрэnэz <b>veritabanэnэn</b>tipini seзiniz. SQL Server veya Microsoft Access kullanэyorsanэz ODBC'yi seзin.");
	define("DB_PHP_LIB_FIELD", "PHP Kitaplэрэ");
	define("DB_HOST_FIELD", "Hostname");
	define("DB_HOST_DESC", "ViArt veritabanэnэzэn зalэюacaрэ sunucunun <b>ad</b>nэ veya <b>IP adresi</b>ni giriniz. Veritabanэnэzэ yerel PC'nizde зalэюtэyorsanэz, bunu \"<b>localhost</b>\" юeklinde ve portu da boю olarak bэrakabilirsiniz. Hosting firmasэnэn veritabanэnэ kullanmanэz halinde, sunucu ayarlarэ iзin firmanэn belgelerine baюvurun.");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "Veritabanэ Adэ / DSN");
	define("DB_NAME_DESC", "MySQL veya PostgreSQL gibi bir veritabanэ kullanэyorsanэz, ViArt'эn tablolarэ oluюturmasэnэ istediрiniz <b>veritabanэnэn adэ</>nэ girin. Bu veritabanэ mutlaka mevcut olmalэdэr. Eрer ViArt'э yerel PC'nize deneme amacэyla kuruyorsanэz, зoрu sistemde hazэr bulunan bir \"<b>test</b>\" veritabanэnэn olmamasэ durumunda, \"viart\" adэyla bir tane oluюturun. Ama eрer Microsoft Access veya SQL Server kullanэyorsanэz, bu durumda Veritabanэnэn Adэ, Kontrol Paneli'nin Veri kaynaklarэ (ODBC) bцlьmьnde kurmuю olduрunuz <b>DSN'nin adэ</b> olmalэdэr.");
	define("DB_USER_FIELD", "Kullanэcэ Adэ");
	define("DB_PASS_FIELD", "Юifre");
	define("DB_USER_PASS_DESC", "Veritabanэ <b>Kullanэcэ adэ</b> ve <b>Юifresini</b> giriniz.");
	define("DB_PERSISTENT_FIELD", "Kalэcэ (persistent) baрlantэ");
	define("DB_PERSISTENT_DESC", "MySQL yahut Postgre gibi sьrekli baрlantэ kullanmak iзin kutuyu iюaretleyin. Bu konuda bilginiz yoksa, kutuyu boю bэrakэn.");
	define("DB_CREATE_DB_FIELD", "Veritabanэ Oluюtur");
	define("DB_CREATE_DB_DESC", "Veritabanэ oluюturmak mьmkьnse, bu kutuyu iюaretleyin. Sadece MySQL ve PostgreSQL iзin geзerlidir.");
	define("DB_POPULATE_FIELD", "Populate DB");
	define("DB_POPULATE_DESC", "veritabanэ tablo yapэsэnэ oluюturmak ve iзine veri yazmak iзin kutuyu iюaretleyin");
	define("DB_TEST_DATA_FIELD", "Test Data");
	define("DB_TEST_DATA_DESC", "veritabanэnэza test verisi eklemek iзin kutuyu iюaretleyin");
	define("ADMIN_EMAIL_FIELD", "Yцnetici Eposta");
	define("ADMIN_LOGIN_FIELD", "Yцnetici giriюi");
	define("ADMIN_PASS_FIELD", "Admin Юifre");
	define("ADMIN_CONF_FIELD", "Юifre Doрrula");
	define("DATETIME_SHOWN_FIELD", "Tarih Zaman Formatэ (sitede gцrьlecek)");
	define("DATE_SHOWN_FIELD", "Tarih Formatэ (sitede gцrьlecek)");
	define("DATETIME_EDIT_FIELD", "Tarih Zaman Formatэ (for editing)");
	define("DATE_EDIT_FIELD", "Tarih Formatэ (for editing)");
	define("DATE_FORMAT_COLUMN", "Tarih Formatэ");
	define("CURRENT_DATE_COLUMN", "Geзerli Tarih");

	define("DB_LIBRARY_ERROR", "PHP fonksiyonlarэ {db_library} iзin belirlenmemiю. Lьtfen configuration file - php.ini database ayarlarэnэ kontrol ediniz.");
	define("DB_CONNECT_ERROR", "Database 'e ulaюэlamэyor. Lьtfen database parametrelerini kontrol edip tekrar deneyiniz.");
	define("INSTALL_FINISHED_ERROR", "Kurulum tamamlandэ.");
	define("WRITE_FILE_ERROR", "<b>'includes/var_definition.php'</b> dosyasэ yazэlabilir deрil. Devam etmeden цnce CHMOD ayarlarэnэ dьzeltiniz.");
	define("WRITE_DIR_ERROR", "<b>'includes'</b> klasцrь yazэlabilir deрil. Devam etmeden цnce CHMOD ayarlarэnэ dьzeltiniz.");
	define("DUMP_FILE_ERROR", "{file_name}' bulunamadэ.");
	define("DB_TABLE_ERROR", "{table_name}' bulunamadэ. Lьtfen veritabanэna gerekli veriyi yerleюtiriniz.");
	define("TEST_DATA_ERROR", "Test verileri olan tablolarэ yayэnlamadan цnce <b>{POPULATE_DB_FIELD}</b>'yi kontrol edin");
	define("DB_HOST_ERROR", "Vermiю olduрunuz host adresi bulunamadэ");
	define("DB_PORT_ERROR", "Belirtilen porttaki veritabanэ sunucusuna baрlanэlamadэ");
	define("DB_USER_PASS_ERROR", "Belirtilen kullanэcэ adэ veya юifre hatalэ");
	define("DB_NAME_ERROR", "Baрlantэ ayarlarэ doрru fakat '{db_name}' veritabanэ bulunamadэ");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt SHOP Upgrade");
	define("UPGRADE_NOTE", "Uyarэ: Lьtfen iюleme baюlamadan цnce veritabanэnэn yedeрini aldэрэnэzdan emin olunuz.");
	define("UPGRADE_AVAILABLE_MSG", "Upgrade Available");
	define("UPGRADE_BUTTON", "{version_number} a yьkselt");
	define("CURRENT_VERSION_MSG", "Kurulu Versiyon");
	define("LATEST_VERSION_MSG", "Kurulabilir Version");
	define("UPGRADE_RESULTS_MSG", "Yьkseltme Sonuзlarэ");
	define("SQL_SUCCESS_MSG", "SQL sorgusu baюarэlэ");
	define("SQL_FAILED_MSG", "SQL sorgusu baюarэsэz");
	define("SQL_TOTAL_MSG", "Yьrьtьlen toplam SQL sorgusu");
	define("VERSION_UPGRADED_MSG", "Ьrьn versiyonu yьkseltilmiюtir.");
	define("ALREADY_LATEST_MSG", "Ьrьnьn son versiyonu zaten yьklь.");
	define("DOWNLOAD_NEW_MSG", "The new version was detected");
	define("DOWNLOAD_NOW_MSG", "{version_number} 'э indir");
	define("DOWNLOAD_FOUND_MSG", "Yьklenebilir yeni bir versiyon var {version_number}. Lьtfen indirmek iзin tэklayэnэz. Dosyalarэ indirip sunucunuza yьkledikten sonra yьkseltme butonuna (upgrade) tэklmayэ unutmayэn.");
	define("NO_XML_CONNECTION", "Dikkat 'http://www.viart.com/' baрlanэlamэyor!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>