<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Destek Merkezi");
	define("SUPPORT_REQUEST_INF_TITLE", "Destek Talep Bilgiler");
	define("SUPPORT_REPLY_FORM_TITLE", "Yanэtla");

	define("MY_SUPPORT_ISSUES_MSG", "Destek Taleplerim");
	define("MY_SUPPORT_ISSUES_DESC", "Sitemizden satэn aldэрэnэz ьrьnlerle ilgili olarak yaюadэрэnэz problemler iзin destek ekibimiz hizmetinizdedir.. Lьtfen yukarэdaki linke tэklayarak problemi bildiriniz.");
	define("NEW_SUPPORT_REQUEST_MSG", "Yeni Talep");
	define("SUPPORT_REQUEST_ADDED_MSG", "Teюekkьr ederiz<br>Talebiniz en kэsa zamanda gerзekleюtirilecektir.");
	define("SUPPORT_SELECT_DEP_MSG", "Bцlьm Seзiniz");
	define("SUPPORT_SELECT_PROD_MSG", "Ьrьn Seзiniz");
	define("SUPPORT_SELECT_STATUS_MSG", "Durum Seзiniz");
	define("SUPPORT_NOT_VIEWED_MSG", "Эzlenmedi");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Mьюteri Okudu.");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Admin Okudu.");
	define("SUPPORT_STATUS_NEW_MSG", "Yeni  ");
	define("NO_SUPPORT_REQUEST_MSG", "Bulunamadэ");

	define("SUPPORT_SUMMARY_COLUMN", "Цzet");
	define("SUPPORT_TYPE_COLUMN", "Tip");
	define("SUPPORT_UPDATED_COLUMN", "Son Gьncelleme");

	define("SUPPORT_USER_NAME_FIELD", "Adэnэz");
	define("SUPPORT_USER_EMAIL_FIELD", "Eposta Adresiniz");
	define("SUPPORT_IDENTIFIER_FIELD", "Kimlik (Fatura #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Ьrьn Modeli (Цrn. Motorola L6)");
	define("SUPPORT_DEPARTMENT_FIELD", "Bцlьm");
	define("SUPPORT_PRODUCT_FIELD", "Ьrьn");
	define("SUPPORT_TYPE_FIELD", "Tip");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Cari Durum");
	define("SUPPORT_SUMMARY_FIELD", "Tek satэrlэk Цzet");
	define("SUPPORT_DESCRIPTION_FIELD", "Tanэm");
	define("SUPPORT_MESSAGE_FIELD", "Mesaj");
	define("SUPPORT_ADDED_FIELD", "Eklendi");
	define("SUPPORT_ADDED_BY_FIELD", "Ekleyen");
	define("SUPPORT_UPDATED_FIELD", "Son Gьncelleюtirme");

	define("SUPPORT_REQUEST_BUTTON", "Talebi Gцnder");
	define("SUPPORT_REPLY_BUTTON", "Cevapla");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "<b>Destek ID</b> bilgisi eksik.");
	define("SUPPORT_MISS_CODE_ERROR", "<b>Onay</b> bilgisi eksik.");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Destek ID</b> bilgisi yanlэю.");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Onay</b> bilgisi yanlэю.");

	define("MAIL_DATA_MSG", "posta verisi/bilgisi");
	define("HEADERS_MSG", "sayfa baюlэрэ");
	define("ORIGINAL_TEXT_MSG", "orijinal metin");
	define("ORIGINAL_HTML_MSG", "orijinal HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>