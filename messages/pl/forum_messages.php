<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// informacje dot. forum
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "Informacje o temacie");
	define("TOPIC_MESSAGE_TITLE", "Wiadomo¶ж");

	define("MY_FORUM_TOPICS_MSG", "Moje tematy na forum");
	define("ALL_FORUM_TOPICS_MSG", "Wszystkie tematy na forum");
	define("MY_FORUM_TOPICS_DESC", "Czy zastanawiaіe¶ siк kiedy¶ nad tym, їe problem ktуry chcesz rozwi±zaж, zdaїyі siк juї komu¶ innemu? Moїe chcesz podzieliж siк rozwi±zaniem z nowymi uїytkownikami? Dlaczego by nie zostaж uїytkownikiem Forum i doі±czyж do spoіeczno¶жi?");
	define("NEW_TOPIC_MSG", "Nowy temat");
	define("NO_TOPICS_MSG", "Nie znaleziono tematуw");
	define("FOUND_TOPICS_MSG", "Znaleјli¶my <b>{found_records}</b> tematy pasuj±cych do szukanego wyraїenia: '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "Temat");
	define("FORUM_REPLIES_COLUMN", "Odpowiedzi");
	define("FORUM_LAST_POST_COLUMN", "Ostatni wpis");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "Temat");
	define("TOPIC_AUTHOR_COLUMN", "Autor");
	define("TOPIC_VIEWS_COLUMN", "Ogl±dany");
	define("TOPIC_REPLIES_COLUMN", "Odpowiedzi");
	define("TOPIC_UPDATED_COLUMN", "Ostatni wpis");
	define("TOPIC_ADDED_MSG", "Dziкkujemy<br>Twуj temat zostaі dodany");

	define("TOPIC_ADDED_BY_FIELD", "Dodany przez");
	define("TOPIC_ADDED_DATE_FIELD", "Dodany");
	define("TOPIC_UPDATED_FIELD", "Ostatni wpis");
	define("TOPIC_NICKNAME_FIELD", "Uїytkownik");
	define("TOPIC_EMAIL_FIELD", "Twуj adres email");
	define("TOPIC_NAME_FIELD", "Temat");
	define("TOPIC_MESSAGE_FIELD", "Tre¶ж");
	define("TOPIC_NOTIFY_FIELD", "Wy¶lij wszystkie odpowiedzi na mуj email");

	define("ADD_TOPIC_BUTTON", "Dodaj temat");
	define("TOPIC_MESSAGE_BUTTON", "Dodaj wpis");

	define("TOPIC_MISS_ID_ERROR", "Brakuje parametru <b>Identyfikuj±cego W±tek</b>.");
	define("TOPIC_WRONG_ID_ERROR", "Parametr <b>Identyfikatora w±tku</b> ma zі± warto¶ж.");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>