<?php

	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/sorter.php");
	include_once($root_folder_path . "includes/navigator.php");
	include_once("./admin_common.php");

	check_admin_security("products_reviews");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","admin_blz_reviews.html");

	include_once("./admin_header.php");
	include_once("./admin_footer.php");
    $shown = array('Нет', 'Да');
    $db->query("SELECT * FROM " . $table_prefix . "blz_reviews ");
	if ($db->next_record())
	{
        $num = 0;
		$t->set_var("no_records", "");
		do {
            $t->set_var('num', ++$num);
            $t->set_var('review_id', $db->f('review_id'));
            $t->set_var('author_name', $db->f('review_author'));
            $t->set_var('shown', $shown[$db->f('shown')]);
            $review = $db->f('review');
            if(strlen($review) >150){
                $review = substr($review, 0, 100) . '...';
            }
            $t->set_var('review', $review);
            $t->parse("records");
		} while ($db->next_record());
	}
	else
	{
		$t->set_var("records", "");
		$t->parse("no_records", false);
	}

	$t->pparse("main");