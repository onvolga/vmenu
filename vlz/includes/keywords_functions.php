<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  keywords_functions.php                                   ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	function keywords_fields()
	{
		global $settings;
		$fields = array(
			"item_name" => 1,
			"item_code" => 2,
			"manufacturer_code" => 3,
			"short_description" => 4,
			"full_description" => 5,
			"features" => 6,
			"special_offer" => 7,
			"notes" => 8,
			"meta_title" => 9,
			"meta_description" => 10,
			"meta_keywords" => 11,
		);
		$keywords_fields = array();
		foreach($fields as $field_name => $field_id) {
			$field_index = get_setting_value($settings, $field_name."_index", 0);
			if ($field_index) {
				$rank = get_setting_value($settings, $field_name."_rank", 0);
				$type = get_setting_value($settings, $field_name."_type", 1);
				$keywords_fields[$field_name] = array(
					"id" => $field_id, "rank" => $rank, "type" => $type,
				);
			}
		}
		return $keywords_fields;
	}

	function generate_keywords($data)
	{
		global $table_prefix, $db, $keywords_fields;

		// get item_id 
		$item_id = $data["item_id"];

		// prepare keywords
		if (!is_array($keywords_fields)) {
			$keywords_fields = keywords_fields();
		}
		$keywords = array();
		foreach($keywords_fields as $field_name => $field_data) {
			$field_keywords = array();
			$field_value = $data[$field_name];
			$field_id = $field_data["id"];
			$field_rank = $field_data["rank"];
			$field_type = $field_data["type"];

			// before strip tags add space before tags
			$field_value = str_replace("<", " <", $field_value);
			$field_value = strip_tags($field_value);
			// strip language tags
			$field_value = preg_replace("/\[\/[a-z]{2}\]/", " ", $field_value);
			// strip all non-word characters
			$field_value = preg_replace(KEYWORD_REPLACE_REGEXP, " ", $field_value);
			$field_value = str_replace("_", " ", $field_value);

			// get all words
			$words = explode(" ", $field_value);
			// remove empty values from array
			foreach ($words as $id => $word) {
				$word = mb_strtolower($word, "utf-8");
				$word = trim($word, "'");
				if (strlen($word)) {
					$words[$id] = $word;
				} else {
					unset($words[$id]);
				}
			}
			// calculate words rank if there are any available
			$words_num = sizeof($words);
			if ($words_num > 0) {
				if ($field_type == 2) {
					$max_rank = $field_rank;
					$field_rank = ceil($max_rank / $words_num);
				} else {
					$max_rank = $field_rank * $words_num;
				}
				// prepare word for adding to DB
				$pos = 0;
				foreach ($words as $id => $word) {
					$pos++;
					// calculate keyword rank
					$word_rank = $field_rank;
					if ($word_rank > $max_rank) { $word_rank = $max_rank; }
					$max_rank -= $word_rank;
					// add keyword to array
					$field_keywords[] = array(
						"word" => $word,
						"pos" => $pos,
						"rank" => $word_rank,
						"field_id" => $field_id,
					);
				}
			}

			// added all keywords to one array
			$keywords = array_merge ($keywords, $field_keywords); 
		}

		// check keywords if they already exists in DB or should be added
		check_keywords($keywords);
		// add keywords for products
		add_keywords($item_id, $keywords);
	}

	function check_keywords(&$keywords)
	{
		global $table_prefix, $db;

		//$keywords_ids = array();
		foreach($keywords as $key => $keyword_info) {
			$word = $keyword_info["word"];
			$sql  = " SELECT keyword_id FROM " . $table_prefix . "keywords ";
			$sql .= " WHERE keyword_name=" . $db->tosql($word, TEXT);
			$db->query($sql);
			if ($db->next_record()) {
				$keyword_id = $db->f("keyword_id");
			} else {
				if ($db->DBType == "postgre") {
					$keyword_id = get_db_value(" SELECT NEXTVAL('seq_" . $table_prefix . "keywords') ");
					$sql  = " INSERT INTO " . $table_prefix . "keywords (keyword_id, keyword_name) VALUES (";
					$sql .= $db->tosql($keyword_id, INTEGER) . ", ";
					$sql .= $db->tosql($word, TEXT) . ")";
					$db->query($sql);
				} else {
					$sql  = " INSERT INTO " . $table_prefix . "keywords (keyword_name) VALUES (";
					$sql .= $db->tosql($word, TEXT) . ")";
					$db->query($sql);
					if ($db->DBType == "mysql") {
						$sql = " SELECT LAST_INSERT_ID() ";
					} else {
						$sql = " SELECT MAX(keyword_id) FROM ".$table_prefix."keywords ";
					}
					$keyword_id = get_db_value($sql);
				}
			}
			$keywords[$key]["id"] = $keyword_id;
		}
	}

	function add_keywords($item_id, $keywords)
	{
		global $db, $table_prefix;

		$sql  = " DELETE FROM " . $table_prefix . "keywords_items ";
		$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);
		$db->query($sql);

		foreach($keywords as $key => $keyword_info) {
			$sql  = " INSERT INTO " . $table_prefix . "keywords_items ";
			$sql .= " (item_id, keyword_id, field_id, keyword_position, keyword_rank) VALUES (";
			$sql .= $db->tosql($item_id, INTEGER) . ", ";
			$sql .= $db->tosql($keyword_info["id"], INTEGER) . ", ";
			$sql .= $db->tosql($keyword_info["field_id"], INTEGER) . ", ";
			$sql .= $db->tosql($keyword_info["pos"], INTEGER) . ", ";
			$sql .= $db->tosql($keyword_info["rank"], INTEGER) . ") ";
			$db->query($sql);
		}

		$sql  = " UPDATE ".$table_prefix."items SET is_keywords=1 ";
		$sql .= " WHERE item_id=".$db->tosql($item_id, INTEGER);
		$db->query($sql);
	}

?>