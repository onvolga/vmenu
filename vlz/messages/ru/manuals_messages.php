<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.10RE                                               ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viartsoft.ru                                         ***
  ***                                                                      ***
  ****************************************************************************
*/

	// сообщения мануалов
	define("MANUALS_TITLE", "Справочные материалы");
	define("NO_MANUALS_MSG", "Не найдено ни одного справочника");
	define("NO_MANUAL_ARTICLES_MSG", "Не найдена ни одна статья");
	define("MANUALS_PREV_ARTICLE", "Предыдущая страница");
	define("MANUALS_NEXT_ARTICLE", "Следующая страница");
	define("MANUAL_CONTENT_MSG", "Вернуться к списку");
	define("MANUALS_SEARCH_IN_MSG", "Искать в");
	define("MANUALS_SEARCH_FOR_MSG", "Искать");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Все справочники");
	define("MANUALS_SEARCH_RESULTS_INFO", "Найдено {search_number} статьи(я) для {search_results}");
	define("MANUALS_SEARCH_RESULT_MSG", "Результаты поиска");
	define("MANUALS_NOT_FOUND_ANYTHING", "Ничего не найдено для '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Содержание отсутствует");
	define("MANUALS_SEARCH_TITLE", "Поиск по справочникам");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Результаты поиска по справочникам");

?>
