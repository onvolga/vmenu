<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "SpjallborГ°");
	define("TOPIC_INFO_TITLE", "UpplГЅsingar um ГѕrГЎГ°");
	define("TOPIC_MESSAGE_TITLE", "SkilaboГ°");

	define("MY_FORUM_TOPICS_MSG", "MГ­nir spjallГѕrГ¦Г°ir");
	define("ALL_FORUM_TOPICS_MSG", "Allir spjallГѕrГ¦Г°ir");
	define("MY_FORUM_TOPICS_DESC", "HefurГ°u velt fyrir ГѕГ©r hvort aГ° vandamГЎliГ° sem ГѕГє glГ­mir viГ° sГ© vandamГЎl sem einhvern annar hefur einnig ГЎtt viГ°? Viltu deila sГ©rfrГ¦Г°iГѕekkingu Гѕinni til nГЅrra notenda? HvГ­ ekki aГ° skrГЎ sig ГЎ spjallborГ°in og verГ°a hluti af samfГ©laginu?");
	define("NEW_TOPIC_MSG", "NГЅr ГѕrГЎГ°ur");
	define("NO_TOPICS_MSG", "Engir ГѕrГ¦Г°ir");
	define("FOUND_TOPICS_MSG", " <b>{found_records}</b> ГѕrГ¦Г°ir fundust samkvГ¦mt eftirfarandi leitarskilyrГ°um: '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Engin spjallborГ° fundust");

	define("FORUM_NAME_COLUMN", "SpjallborГ°");
	define("FORUM_TOPICS_COLUMN", "ГћrГ¦Г°ir ");
	define("FORUM_REPLIES_COLUMN", "SvГ¶r");
	define("FORUM_LAST_POST_COLUMN", "SГ­Г°ast uppfГ¦rt");
	define("FORUM_MODERATORS_MSG", "UmsjГіnarmenn");

	define("TOPIC_NAME_COLUMN", "ГћrГЎГ°ur");
	define("TOPIC_AUTHOR_COLUMN", "HГ¶fundur");
	define("TOPIC_VIEWS_COLUMN", "SkoГ°aГ°");
	define("TOPIC_REPLIES_COLUMN", "SvГ¶r");
	define("TOPIC_UPDATED_COLUMN", "SГ­Г°ast uppfГ¦rt");
	define("TOPIC_ADDED_MSG", "Takk fyrir, ГѕrГ¦Г°i ГѕГ­num var bГ¦tt viГ°");

	define("TOPIC_ADDED_BY_FIELD", "BГ¦tt viГ° af");
	define("TOPIC_ADDED_DATE_FIELD", "BГ¦tt viГ°");
	define("TOPIC_UPDATED_FIELD", "SГ­Г°ast uppfГ¦rt");
	define("TOPIC_NICKNAME_FIELD", "GГ¦lunafn");
	define("TOPIC_EMAIL_FIELD", "Netfang Гѕitt");
	define("TOPIC_NAME_FIELD", "ГћrГЎГ°ur");
	define("TOPIC_MESSAGE_FIELD", "SkilaboГ°");
	define("TOPIC_NOTIFY_FIELD", "Senda Г¶ll svГ¶r ГЎ netfang mitt");

	define("ADD_TOPIC_BUTTON", "BГ¦ta viГ° umrГ¦Г°uefni/ГѕrГ¦Г°i");
	define("TOPIC_MESSAGE_BUTTON", "BГ¦ta viГ° skilaboГ°um");

	define("TOPIC_MISS_ID_ERROR", "Breytu vantar fyrir <b>nГєmer ГѕrГЎГ°ar</b>.");
	define("TOPIC_WRONG_ID_ERROR", "Breytan fyrir <b>nГєmer ГѕrГЎГ°ar</b> hefur rangt gildi.");
	define("FORUM_SEARCH_MESSAGE", "{search_count} skilaboГ° fundust samkvГ¦mt eftirfarandi leitarskilyrГ°um: '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "ForskoГ°a");
	define("TOPIC_SAVE_BUTTON", "Vista");

	define("LAST_POST_ON_SHORT_MSG", "Гћann:");
	define("LAST_POST_IN_SHORT_MSG", "ГЌ ГѕrГ¦Г°inum:");
	define("LAST_POST_BY_SHORT_MSG", "Eftir:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "SГ­Г°ast breytt:");

?>