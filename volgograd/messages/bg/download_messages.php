<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//съобщения свързани със сваляне (download)
	define("DOWNLOAD_WRONG_PARAM", "Грешни параметри за сваляне.");
	define("DOWNLOAD_MISS_PARAM", "Изпуснати параметри за сваляне.");
	define("DOWNLOAD_INACTIVE", "Неактивно сваляне.");
	define("DOWNLOAD_EXPIRED", "Вашият период за сваляне изтече.");
	define("DOWNLOAD_LIMITED", "Достигнахте вашият максимален брой сваляния.");
	define("DOWNLOAD_PATH_ERROR", "Пътят към продукта не е намерен.");
	define("DOWNLOAD_RELEASE_ERROR", "Версията не бе намерена");
	define("DOWNLOAD_USER_ERROR", "Само регистрирани потребители могат да свалят този файл.");
	define("ACTIVATION_OPTIONS_MSG", "Опции за активация");
	define("ACTIVATION_MAX_NUMBER_MSG", "Максимален брой активации");
	define("DOWNLOAD_OPTIONS_MSG", "Разрешено за сваляне / Софтуерни опции");
	define("DOWNLOADABLE_MSG", "Разрешено за сваляне (Софтуер)");
	define("DOWNLOADABLE_DESC", "За изтегляемият продукт можете да посочите \"Период на теглене\", \"Път до файла за теглене\" и \"Опции за активацията\".");
	define("DOWNLOAD_PERIOD_MSG", "Период за сваляне");
	define("DOWNLOAD_PATH_MSG", "Път към файла за сваляне");
	define("DOWNLOAD_PATH_DESC", "Можете да добавите няколко пътя, разделени с точка и запетая");
	define("UPLOAD_SELECT_MSG", "Избери файл за качване и натисни {button_name} бутон.");
	define("UPLOADED_FILE_MSG", "Файлът <b>{filename}</b> беше качен.");
	define("UPLOAD_SELECT_ERROR", "Моля, първо изберете файл.");
	define("UPLOAD_IMAGE_ERROR", "Само файлове-картинки са разрешени.");
	define("UPLOAD_FORMAT_ERROR", "Този тип файл не е разрешен.");
	define("UPLOAD_SIZE_ERROR", "Файлове по-големи от {filesize} не се допускат.");
	define("UPLOAD_DIMENSION_ERROR", "Картинки по-големи от {dimension} не са позволени.");
	define("UPLOAD_CREATE_ERROR", "Системата не може да създаде файла.");
	define("UPLOAD_ACCESS_ERROR", "Вие нямате права да качвате файлове.");
	define("DELETE_FILE_CONFIRM_MSG", "Сигурни ли сте, че искате да изтриете този файл?");
	define("NO_FILES_MSG", "Не са намерени файлове.");
	define("SERIAL_GENERATE_MSG", "Генериране сериен номер");
	define("SERIAL_DONT_GENERATE_MSG", "Не генерирай");
	define("SERIAL_RANDOM_GENERATE_MSG", "Генерирай произволен сериен номер за софтуерен продукт");
	define("SERIAL_FROM_PREDEFINED_MSG", "Вземи сериен номер от предварителен списък");
	define("SERIAL_PREDEFINED_MSG", "Предварителни списъци със серийни номера");
	define("SERIAL_NUMBER_COLUMN", "Сериен номер");
	define("SERIAL_USED_COLUMN", "Използван");
	define("SERIAL_DELETE_COLUMN", "Изтриване");
	define("SERIAL_MORE_MSG", "Добави още серийни номера?");
	define("SERIAL_PERIOD_MSG", "Период на сериен номер");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Покажи общите условия");
	define("DOWNLOAD_SHOW_TERMS_DESC", "За да свали продукта потребителят трябва да се запознае с общите условия");
	define("DOWNLOAD_TERMS_MSG", "Общи условия");
	define("DOWNLOAD_TERMS_USER_DESC", "Запознах се с общите условия");
	define("DOWNLOAD_TERMS_USER_ERROR", "За да свалите продукта трябва да се запознаете с общите условия");

	define("DOWNLOAD_TITLE_MSG", "Заглавие на тегленето");
	define("DOWNLOADABLE_FILES_MSG", "Файлове за теглене");
	define("DOWNLOAD_INTERVAL_MSG", "Интервал на теглене");
	define("DOWNLOAD_LIMIT_MSG", "Лимит на тегленията");
	define("DOWNLOAD_LIMIT_DESC", "Брой пъти, които един файл може да се изтегли");
	define("MAXIMUM_DOWNLOADS_MSG", "Максимален брой тегления");
	define("PREVIEW_TYPE_MSG", "Тип на превюто");
	define("PREVIEW_TITLE_MSG", "Заглавие на превюто");
	define("PREVIEW_PATH_MSG", "Път до превюто на файла");
	define("PREVIEW_IMAGE_MSG", "Превю на изображението");
	define("MORE_FILES_MSG", "Още файлове");
	define("UPLOAD_MSG", "Качване");
	define("USE_WITH_OPTIONS_MSG", "Използвай само с опции");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Превю докато теглиш");
	define("PREVIEW_USE_PLAYER_MSG", "Използвай плеър за превю");
	define("PROD_PREVIEWS_MSG", "Превюта");

?>