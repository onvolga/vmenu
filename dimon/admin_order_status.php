<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  admin_order_status.php                                   ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "messages/" . $language_code . "/cart_messages.php");
	include_once("./admin_common.php");

	check_admin_security("sales_orders");
	check_admin_security("order_statuses");

	$tab = get_param("tab");
	if (!$tab) { $tab = "general"; }
	$operation = get_param("operation");
	if ($operation) { $tab = "general"; }

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main", "admin_order_status.html");

	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->set_var("admin_href", "admin.php");  
	$t->set_var("admin_lookup_tables_href",  "admin_lookup_tables.php");
	$t->set_var("admin_order_statuses_href", "admin_order_statuses.php");
	$t->set_var("admin_order_status_href",   "admin_order_status.php");
	$t->set_var("admin_email_help_href",     "admin_email_help.php");
	$t->set_var("admin_order_help_href",     "admin_order_help.php");
	$t->set_var("admin_download_info_href",  "admin_download_info.php");
	$t->set_var("admin_privileges_select_href", "admin_privileges_select.php");
	$t->set_var("CONFIRM_DELETE_JS", str_replace("{record_name}", STATUS_MSG, CONFIRM_DELETE_MSG));

	$paid_statuses = 
		array( 
			array(1, PAID_MSG), array(0, NOT_PAID_MSG)
		);

	$activations_values = 
		array( 
			array(1, ACTIVATE_MSG), array(0, DISABLE_MSG)
		);

	$commission_values = 
		array( 
			array(1, COMMISSION_REWARD_ADD_MSG), array(-1, COMMISSION_REWARD_SUBTRACT_MSG),
		);

	$stock_level_values = 
		array( 
			array(1, STOCK_LEVEL_RESERVE_MSG), array(-1, STOCK_LEVEL_RELEASE_MSG),
		);

	$points_action_values = 
		array( 
			array(-1, POINTS_SUBTRACT_MSG), array(1, POINTS_RETURN_MSG),
		);

	$credit_action_values = 
		array( 
			array(-1, SUBSTRACT_CREDIT_AMOUNT_MSG), array(1, RETURN_CREDIT_BALANCE_MSG),
		);

	$credit_note_values = 
		array( 
			array(-1, SUBSTRACT_CREDIT_AMOUNT_MSG), array(1, RETURN_CREDIT_BALANCE_MSG),
		);

	$mail_types = 
		array( 
			array(1, HTML_MSG), array(0, PLAIN_TEXT_MSG)
		);

	$status_types = array(
		array("", ""), 
		array("NEW", "NEW"), 
		array("PAYMENT_INFO", "PAYMENT_INFO"), 
		array("CONFIRMED", "CONFIRMED"), 
		array("PAID", "PAID"), 
		array("SHIPPED", "SHIPPED"), 
		array("PENDING", "PENDING"), 
		array("DECLINED", "DECLINED"), 
		array("VALIDATED", "VALIDATED"), 
		array("FAILED", "FAILED"), 
		array("DISPATCHED", "DISPATCHED"), 
		array("REFUNDED", "REFUNDED"), 
		array("CAPTURED", "CAPTURED"), 
		array("VOIDED", "VOIDED"), 
		array("AUTHORIZED", "AUTHORIZED"), 
		array("CANCELLED", "CANCELLED"), 
		array("CREDIT_NOTE", "CREDIT_NOTE"), 
		array("OTHER", "OTHER"), 
	);

	$html_editor = get_setting_value($settings, "html_editor_email", get_setting_value($settings, "html_editor", 1));
	$t->set_var("html_editor", $html_editor);
	$editors_list = 'mb,mrb,sb,ab';
	add_html_editors($editors_list, $html_editor);
	
	$r = new VA_Record($table_prefix . "order_statuses");
	$r->return_page = "admin_order_statuses.php";

	$r->add_where("status_id", INTEGER);
	$r->add_checkbox("is_active", INTEGER);
	$r->add_textbox("status_order", INTEGER, STATUS_ORDER_MSG);
	$r->parameters["status_order"][REQUIRED] = true;
	$r->add_textbox("status_name", TEXT, STATUS_NAME_MSG);
	$r->parameters["status_name"][REQUIRED] = true;
	$r->add_select("status_type", TEXT, $status_types, STATUS_TYPE_MSG);
	$r->change_property("status_type", REQUIRED, true);
	$r->add_checkbox("allow_user_cancel", INTEGER);
	$r->add_checkbox("is_user_cancel", INTEGER);
	$r->add_checkbox("generate_invoice", INTEGER);
	$r->add_checkbox("user_invoice_activation", INTEGER);
	$r->add_checkbox("vi_sf_download", INTEGER);
	$r->add_checkbox("vi_blank_pd4_download", INTEGER);
	$r->add_checkbox("is_dispatch", INTEGER);
	$r->add_checkbox("is_list", INTEGER);
	$r->change_property("is_list", DEFAULT_VALUE, 1);
	$r->add_checkbox("show_for_user", INTEGER);
	$r->add_checkbox("item_notify", INTEGER);
	$r->add_checkbox("credit_note_action", INTEGER);
	$r->add_radio("paid_status", INTEGER, $paid_statuses, PAID_STATUS_MSG);
	$r->add_radio("download_activation", INTEGER, $activations_values);
	$r->add_checkbox("download_notify", INTEGER);
	$r->add_radio("commission_action", INTEGER, $commission_values);
	$r->add_radio("stock_level_action", INTEGER, $stock_level_values);
	$r->add_radio("points_action", INTEGER, $points_action_values);
	$r->add_radio("credit_action", INTEGER, $credit_action_values);

	// customer notification fields
	$r->add_checkbox("mail_notify", INTEGER);
	$r->add_textbox("mail_from", TEXT);
	$r->add_textbox("mail_cc", TEXT);
	$r->add_textbox("mail_bcc", TEXT);
	$r->add_textbox("mail_reply_to", TEXT);
	$r->add_textbox("mail_return_path", TEXT);
	$r->add_textbox("mail_subject", TEXT);
	$r->add_radio("mail_type", INTEGER, $mail_types);
	$r->parameters["mail_type"][DEFAULT_VALUE] = 0;
	$r->add_textbox("mail_body", TEXT);

	$r->add_checkbox("sms_notify", INTEGER);
	$r->add_textbox("sms_recipient", TEXT, USER_SMS_RECIPIENT_MSG);
	$r->add_textbox("sms_originator",TEXT, USER_SMS_ORIGINATOR_MSG);
	$r->add_textbox("sms_message",   TEXT, USER_SMS_MESSAGE_MSG);

	// merchant notification fields
	$r->add_checkbox("merchant_notify", INTEGER);
	$r->add_textbox("merchant_to", TEXT);
	$r->add_textbox("merchant_from", TEXT);
	$r->add_textbox("merchant_cc", TEXT);
	$r->add_textbox("merchant_bcc", TEXT);
	$r->add_textbox("merchant_reply_to", TEXT);
	$r->add_textbox("merchant_return_path", TEXT);
	$r->add_textbox("merchant_subject", TEXT);
	$r->add_radio("merchant_mail_type", INTEGER, $mail_types);
	$r->parameters["merchant_mail_type"][DEFAULT_VALUE] = 0;
	$r->add_textbox("merchant_body", TEXT);

	$r->add_checkbox("merchant_sms_notify", INTEGER);
	$r->add_textbox("merchant_sms_recipient", TEXT, MERCHANT_SMS_RECIPIENT_MSG);
	$r->add_textbox("merchant_sms_originator",TEXT, MERCHANT_SMS_ORIGINATOR_MSG);
	$r->add_textbox("merchant_sms_message",   TEXT, MERCHANT_SMS_MESSAGE_MSG);


	// supplier notification fields
	$r->add_checkbox("supplier_notify", INTEGER);
	$r->add_textbox("supplier_to", TEXT);
	$r->add_textbox("supplier_from", TEXT);
	$r->add_textbox("supplier_cc", TEXT);
	$r->add_textbox("supplier_bcc", TEXT);
	$r->add_textbox("supplier_reply_to", TEXT);
	$r->add_textbox("supplier_return_path", TEXT);
	$r->add_textbox("supplier_subject", TEXT);
	$r->add_radio("supplier_mail_type", INTEGER, $mail_types);
	$r->parameters["supplier_mail_type"][DEFAULT_VALUE] = 0;
	$r->add_textbox("supplier_body", TEXT);

	$r->add_checkbox("supplier_sms_notify", INTEGER);
	$r->add_textbox("supplier_sms_recipient", TEXT, SMS_RECIPIENT_MSG);
	$r->add_textbox("supplier_sms_originator",TEXT, SMS_ORIGINATOR_MSG);
	$r->add_textbox("supplier_sms_message",   TEXT, SMS_MESSAGE_MSG);

	// admin notification fields
	$r->add_checkbox("admin_notify", INTEGER);
	$r->add_textbox("admin_to", TEXT);
	$r->add_textbox("admin_to_groups_ids", TEXT);
	$r->add_textbox("admin_from", TEXT);
	$r->add_textbox("admin_cc", TEXT);
	$r->add_textbox("admin_bcc", TEXT);
	$r->add_textbox("admin_reply_to", TEXT);
	$r->add_textbox("admin_return_path", TEXT);
	$r->add_textbox("admin_subject", TEXT);
	$r->add_radio("admin_mail_type", INTEGER, $mail_types);
	$r->parameters["admin_mail_type"][DEFAULT_VALUE] = 0;
	$r->add_textbox("admin_body", TEXT);

	$r->add_checkbox("admin_sms_notify", INTEGER);
	$r->add_textbox("admin_sms_recipient", TEXT, ADMIN_SMS_RECIPIENT_MSG);
	$r->add_textbox("admin_sms_originator",TEXT, ADMIN_SMS_ORIGINATOR_MSG);
	$r->add_textbox("admin_sms_message",   TEXT, ADMIN_SMS_MESSAGE_MSG);

	$r->add_textbox("final_message",   TEXT);
	$r->set_event(BEFORE_VALIDATE, "check_status_options");
	$r->set_event(BEFORE_DEFAULT, "set_status_order");
	$r->set_event(BEFORE_SHOW, "show_admin_groups");
	
	$r->set_event(AFTER_INSERT, "after_insert_status");
	$r->set_event(AFTER_UPDATE, "after_update_status");


	// access levels
	$r->add_checkbox("view_order_groups_all", INTEGER);
	$r->add_textbox("view_order_groups_ids", TEXT);
	$r->add_checkbox("set_status_groups_all", INTEGER);
	$r->add_textbox("set_status_groups_ids", TEXT);
	$r->add_checkbox("update_order_groups_all", INTEGER);
	$r->add_textbox("update_order_groups_ids", TEXT);
	
	$r->process();
	
	
	
	// current status
	$current_status_id = get_param("status_id");
	
	// available_statuses
	$available_order_ids = array();
	if(!empty($current_status_id)) {
		$db->query("SELECT available_order_ids FROM " . $table_prefix . "order_statuses WHERE status_id = " . $current_status_id);
		if($db->next_record()) {
			$available_order_ids = explode(",", $db->f('available_order_ids'));
		}
	}
	
	// get all statuses
	$db->query("SELECT * FROM " . $table_prefix . "order_statuses");
	while($db->next_record()) {
		$t->set_var("status_available_value", $db->f('status_id'));
		$t->set_var("status_available_description", $db->f('status_name'));
		if(in_array($db->f('status_id'), $available_order_ids)) {
			$t->set_var("status_available_checked", "checked");
		} else {
			$t->set_var("status_available_checked", "");
		}
		$t->parse("statuses_list", true);
	}

	$tabs = array(
		"general" => array("title" => ADMIN_GENERAL_MSG), 
		"user_notify" => array("title" => USER_NOTIFICATION_MSG), 
		"merchant_notify" => array("title" => MERCHANT_MSG), 
		"supplier_notify" => array("title" => SUPPLIER_MSG), 
		"admin_notify" => array("title" => ADMINISTRATOR_NOTIFICATION_MSG), 
		"final_checkout" => array("title" => FINAL_CHECKOUT_MSG),
		"access_levels" => array("title" => ACCESS_LEVELS_MSG),
	);

	parse_admin_tabs($tabs, $tab, 7);

	$t->pparse("main");

	function check_status_options()
	{
		global $r;
		if ($r->get_value("sms_notify")) {
			$r->change_property("sms_message", REQUIRED, true);
		} 
		if ($r->get_value("merchant_sms_notify")) {
			$r->change_property("merchant_sms_message", REQUIRED, true);
		} 
		if ($r->get_value("admin_sms_notify")) {
			$r->change_property("admin_sms_message", REQUIRED, true);
		} 
	}
	
	
	function set_available_statuses($for_status_id) {
		
		global $db, $table_prefix, $_POST;
		$saving_string = "";
		foreach($_POST['status_available'] as $saving_status_id) {
			if(!empty($saving_string)) {
				$saving_string .= ",";
			}
			$saving_string .= $saving_status_id;
		}
		$db->query("UPDATE " . $table_prefix . "order_statuses SET `available_order_ids` = '" . $saving_string . "' WHERE status_id = " . $for_status_id);
		
	}
	
	// update available_statuses
	function after_update_status () {
		
		global $_POST;
		set_available_statuses($_POST['status_id']);
		
	}
	
	// insert available_statuses
	function after_insert_status() {
		
		global $db, $table_prefix, $_POST;
		$db->query("SELECT MAX(status_id) FROM " . $table_prefix . "order_statuses");
		if ($db->next_record()) {
			$m_status_id = $db->f(0);
			set_available_statuses($m_status_id);
		}
		
	}

	function set_status_order()  
	{
		global $db, $table_prefix, $r;
		$sql = "SELECT MAX(status_order) FROM " . $table_prefix . "order_statuses ";
		$db->query($sql);
		if ($db->next_record()) {
			$status_order = $db->f(0) + 1;
			$r->change_property("status_order", DEFAULT_VALUE, $status_order);
		}	
	}

	function show_admin_groups()
	{
		global $db, $table_prefix, $r, $t;

		$data = array(
			"admin_to_groups_ids" => "admin_to_groups",
			"view_order_groups_ids" => "view_order_admin_groups",
			"set_status_groups_ids" => "set_status_admin_groups",
			"update_order_groups_ids" => "update_order_admin_groups",
		);
		
		foreach ($data as $field_name => $block_name) {
			$ids = $r->get_value($field_name);
			if ($ids) {
				$sql  = " SELECT ap.privilege_id, ap.privilege_name ";
				$sql .= " FROM " . $table_prefix . "admin_privileges ap ";
				$sql .= " WHERE ap.privilege_id IN (" . $db->tosql($ids, INTEGERS_LIST) . ") ";
				$sql .= " ORDER BY ap.privilege_name ";
				$db->query($sql);
				while($db->next_record())
				{
					$row_privilege_id = $db->f("privilege_id");
					$privilege_name = get_translation($db->f("privilege_name"));
			
					$t->set_var("privilege_id", $row_privilege_id);
					$t->set_var("privilege_name", $privilege_name);
					$t->set_var("privilege_name_js", str_replace("\"", "&quot;", $privilege_name));
			
					$t->parse($block_name, true);
					$t->parse($block_name."_js", true);
				}
			}
		}

	}

?>