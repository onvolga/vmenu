<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  barcode_image.php                                        ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/barcode_functions.php");

	$text = isset($_REQUEST['text']) ? $_REQUEST['text'] : '123456789012';  				// parameter: bar code text
	$imgtype = isset($_REQUEST['imgtype']) ? $_REQUEST['imgtype'] : 'png'; 					// parameter: image type (png, gif, jpg)
	$codetype = isset($_REQUEST['codetype']) ? $_REQUEST['codetype'] : 'code128'; 	// parameter: code type (code128, ean13, code39, int25, upca)

	draw_barcode($text, $imgtype, $codetype);

?>
