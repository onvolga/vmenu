
DROP TABLE IF EXISTS `va_blz_items_icons`;
CREATE TABLE `va_blz_items_icons`(
 `icon_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `icon_path` VARCHAR(1024),
 `icon_order` INT(11),
 `icon_name` VARCHAR(255)
  ,PRIMARY KEY (icon_id)
  ,KEY icon_order (icon_order)
);

DELETE FROM `va_blz_items_icons`;
INSERT INTO `va_blz_items_icons` (icon_id, icon_path, icon_order, icon_name) VALUES (1, 'images/items_icons/present.png', 1, 'Подарок');
INSERT INTO `va_blz_items_icons` (icon_id, icon_path, icon_order, icon_name) VALUES (2, 'images/items_icons/discount20.png', 2, 'Скидка 20%');
INSERT INTO `va_blz_items_icons` (icon_id, icon_path, icon_order, icon_name) VALUES (4, 'images/items_icons/newCol.png', 3, 'Новая коллекция');

DROP TABLE IF EXISTS `va_blz_items_icons_assigned`;
CREATE TABLE `va_blz_items_icons_assigned`(
 `item_id` INT(11),
 `icon_id` INT(11)
  ,KEY item_id (item_id)
  ,KEY icon_id (icon_id)
);

DELETE FROM `va_blz_items_icons_assigned`;
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (485, 2);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (174, 2);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (175, 1);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (175, 2);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (175, 4);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (176, 4);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (518, 1);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (478, 2);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (478, 4);
INSERT INTO `va_blz_items_icons_assigned` (item_id, icon_id) VALUES (174, 1);

DROP TABLE IF EXISTS `va_blz_reviews`;
CREATE TABLE `va_blz_reviews`(
 `review_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `review_author` VARCHAR(60),
 `review` TEXT,
 `shown` INT(1) UNSIGNED default '0'
  ,PRIMARY KEY (review_id)
  ,KEY shown (shown)
);

DELETE FROM `va_blz_reviews`;
INSERT INTO `va_blz_reviews` (review_id, review_author, review, shown) VALUES (1, 'Yuri ', 'comment', 1);
INSERT INTO `va_blz_reviews` (review_id, review_author, review, shown) VALUES (4, 'Валерик', 'Еще отзыв', 1);
INSERT INTO `va_blz_reviews` (review_id, review_author, review, shown) VALUES (5, 'Светлана Б.', 'Покупали пальтишечко, для коньков самое оно.', 1);
INSERT INTO `va_blz_reviews` (review_id, review_author, review, shown) VALUES (6, 'Ольга', 'Очень понравился магазин и отношение к клиенту. Всем рекомендую!', 1);
INSERT INTO `va_blz_reviews` (review_id, review_author, review, shown) VALUES (3, 'Анастасия Ю.', 'Заказывала Heelys, заказ пришел быстро и отличного качества. Персонал вежливый, помогли определить размер.', 1);

DROP TABLE IF EXISTS `va_blz_sliders`;
CREATE TABLE `va_blz_sliders`(
 `row_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `ids_list` TEXT,
 `items_type` VARCHAR(255),
 `user_tab_name` VARCHAR(80),
 `slider_order` INT(4)
  ,PRIMARY KEY (row_id)
);

DELETE FROM `va_blz_sliders`;
INSERT INTO `va_blz_sliders` (row_id, ids_list, items_type, user_tab_name, slider_order) VALUES (1, '79', 'popular_others', '', 5);
INSERT INTO `va_blz_sliders` (row_id, ids_list, items_type, user_tab_name, slider_order) VALUES (4, '1001,101,102,103,104,105,106,107,79', 'popular_clothes', 'Одежда', 1);
INSERT INTO `va_blz_sliders` (row_id, ids_list, items_type, user_tab_name, slider_order) VALUES (5, '582,578,579,454,52', 'popular_boots', 'Обувь', 2);
INSERT INTO `va_blz_sliders` (row_id, ids_list, items_type, user_tab_name, slider_order) VALUES (6, '474,489,725,517,518,559,600', 'popular_boys', 'Для мачиков', 3);
INSERT INTO `va_blz_sliders` (row_id, ids_list, items_type, user_tab_name, slider_order) VALUES (7, '569,101,103,104,598', 'popular_girls', 'Для девочек', 4);
