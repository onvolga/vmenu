function openWindowSelect(windowUrl, formName, fieldName, idName, selectionType)
{
	var queryString = "";
  if (formName != "") {
		queryString = "?form_name=" + formName;
	}
  if (fieldName != "") {
		queryString += ((queryString == "") ? "?" : "&");
		queryString += "field_name=" + fieldName;
	}
  if (idName != "") {
		queryString += ((queryString == "") ? "?" : "&");
		queryString += "id_name=" + idName;
	}
  if (selectionType != "") {
		queryString += ((queryString == "") ? "?" : "&");
		queryString += "selection_type=" + selectionType;
	}
	var windowSelect = window.open (windowUrl + queryString, 'windowSelect', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=600,height=500');
	windowSelect.focus();
}

function selectItem(itemId, itemName)
{
	if (window.opener) {
		var formName  = document.form_list.form_name.value;
		var fieldName  = document.form_list.field_name.value;
		var idName  = document.form_list.id_name.value;
		var selectionType = document.form_list.selection_type.value;
		window.opener.setItem(itemId, itemName, formName, fieldName, idName, selectionType);
		window.opener.focus();
	}
	window.close();
}

function closeWindowSelect()
{
	window.opener.focus();
	window.close();
}

function setItem(itemId, itemName, formName, fieldName, idName, selectionType)
{
	if (selectionType == "single") {
		// used only for user select on some forms
		var idControl = document.forms[formName].elements[fieldName];
		var itemNameObj = document.getElementById(idName);
		idControl.value = itemId;
		var itemInfo = "<a href=\""+userViewLink+"?user_id="+itemId+"\" class=\"title\" target=\"_blank\">"+itemName+"</a>";
		itemInfo += " (#"+itemId+") - <a href=\"#\" onClick=\"removeSingleItem('" + id + "', '" + formName + "', '" + fieldName + "', '" + idName + "'); return false;\">";
		itemInfo += removeButton + "</a> | ";
		itemNameObj.innerHTML = itemInfo;
	} else {
		var itemAdded = false;
		var itemsArray = items[fieldName];
		for(var id in itemsArray)
		{
			if (id == itemId) {
				itemAdded = true;
			}
		}
		
		if (!itemAdded) {
			// add new item to global array
			items[fieldName][itemId] = itemName;
			generateItemsList(formName, fieldName, idName);
		}
	}
}

function removeSingleItem(itemId, formName, fieldName, idName)
{
	var idControl = document.forms[formName].elements[fieldName];
	var itemNameObj = document.getElementById(idName);
	idControl.value = "";
	itemNameObj.innerHTML = "";
}


function removeItem(itemId, formName, fieldName, idName)
{
	delete items[fieldName][itemId];
	generateItemsList(formName, fieldName, idName);
}

function generateItemsList(formName, fieldName, idName)
{
	var idsControl = ""; var itemsIds = "";
	var selectedDiv = document.getElementById(idName);; 
	var itemsArray = items[fieldName]; 
	selectedDiv.innerHTML = "";
	for(var id in itemsArray)
	{
		var itemName = itemsArray[id];
		var itemInfo = "<li class=selectedCategory>" + itemName;
		itemInfo += " - <a href=\"#\" onClick=\"removeItem('" + id + "', '" + formName + "', '" + fieldName + "', '" + idName + "'); return false;\">";
		itemInfo += removeButton + "</a>";
		if (selectedDiv.insertAdjacentHTML) {
			selectedDiv.insertAdjacentHTML("beforeEnd", itemInfo);
		} else {
			selectedDiv.innerHTML += itemInfo;
		}
		if (itemsIds != "") { itemsIds += "," }
		itemsIds += id;
	}
	idsControl = document.forms[formName].elements[fieldName];
	idsControl.value = itemsIds;
}
