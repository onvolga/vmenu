<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Hilfe-Center");
	define("SUPPORT_REQUEST_INF_TITLE", "Anfrage-Informationen");
	define("SUPPORT_REPLY_FORM_TITLE", "Antwort");

	define("MY_SUPPORT_ISSUES_MSG", "Meine Anfragen");
	define("MY_SUPPORT_ISSUES_DESC", "Sollten Sie Probleme mit den gekauften Produkten haben, steht Ihnen unser Hilfe-Team gerne zur VerfСЊgung. Klicken Sie auf den Verweis oben um eine Anfrage abzusenden.");
	define("NEW_SUPPORT_REQUEST_MSG", "Neue Anfrage");
	define("SUPPORT_REQUEST_ADDED_MSG", "Vielen Dank<br>Unser Team wird sich nun bemСЊhen, Ihnen so schnell wie mС†glich zu helfen.");
	define("SUPPORT_SELECT_DEP_MSG", "Abteilung wРґhlen");
	define("SUPPORT_SELECT_PROD_MSG", "Produkt wРґhlen");
	define("SUPPORT_SELECT_STATUS_MSG", "Status wРґhlen");
	define("SUPPORT_NOT_VIEWED_MSG", "Nicht angesehen");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Vom Kunden angesehen");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Vom Administrator angesehen");
	define("SUPPORT_STATUS_NEW_MSG", "Neu");
	define("NO_SUPPORT_REQUEST_MSG", "Nichts gefunden");

	define("SUPPORT_SUMMARY_COLUMN", "Zusammenfassung");
	define("SUPPORT_TYPE_COLUMN", "Typ");
	define("SUPPORT_UPDATED_COLUMN", "Zuletzt aktualisiert");

	define("SUPPORT_USER_NAME_FIELD", "Ihr Name");
	define("SUPPORT_USER_EMAIL_FIELD", "Ihre E-Mail-Adresse");
	define("SUPPORT_IDENTIFIER_FIELD", "Kennung (Rechnung #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Umgebung (Betriebssystem, Datenbank, Webserver, etc.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Abteilung");
	define("SUPPORT_PRODUCT_FIELD", "Produkt");
	define("SUPPORT_TYPE_FIELD", "Typ");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Aktueller Status");
	define("SUPPORT_SUMMARY_FIELD", "Online-Zusammenfassung");
	define("SUPPORT_DESCRIPTION_FIELD", "Beschreibung");
	define("SUPPORT_MESSAGE_FIELD", "Nachricht");
	define("SUPPORT_ADDED_FIELD", "HinzugefСЊgt");
	define("SUPPORT_ADDED_BY_FIELD", "HinzugefСЊgt von");
	define("SUPPORT_UPDATED_FIELD", "Zuletzt aktualisiert");

	define("SUPPORT_REQUEST_BUTTON", "Anfrage absenden");
	define("SUPPORT_REPLY_BUTTON", "Antworten");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Fehlender Parameter <b>Support ID</b>");
	define("SUPPORT_MISS_CODE_ERROR", "Fehlender Parameter <b>Verifizierung</b>");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Support ID</b> Parameter hat falschen Wert");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verifizierung</b> Parameter hat falschen Wert");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, Sie haben keine Genehmigung das Ticket zu schlieРЇen");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, Sie haben keine Genehmigung das Ticket zu beantworten");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, Sie haben keine Genehmigung ein neues Ticket zu erstellen");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, Sie haben keine Genehmigung das Ticket zu lС†schen");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, Sie haben keine Genehmigung das Ticket zu erweitern");
	define("NO_TICKETS_FOUND_MSG", "Keine Tickets gefunden");
	define("HIDDEN_TICKETS_MSG", "Versteckte Tickets");
	define("ALL_TICKETS_MSG", "Alle Tickets");
	define("ACTIVE_TICKETS_MSG", "Aktive Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "Sie gehС†ren nicht zu diesem Department");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "Sie sind keinem Department zugeordnet");
	define("REPLY_TO_NAME_MSG", "Antwort an {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Wissens Kategorie");
	define("KNOWLEDGE_TITLE_MSG", "Wissens Titel");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Wissens Atikel Status");
	define("SELECT_RESPONSIBLE_MSG", "WРґhle Verantwortlich");

?>