<?php

	include_once("./includes/navigation_functions.php");

	$default_title = "{menu_title}";

	$menu_id = $vars["block_key"];
	
	$html_template = get_setting_value($block, "html_template", "block_navigation.html"); 
  $t->set_file("block_body", $html_template);
	$t->set_var("item_block", "");
  $t->set_var("block_class", $block["css_class"]);

	// If no record, but menu exists, show two levels by default
	$visible_depth_level = get_setting_value($vars, "visible_depth_level", 2);
	
	// Menu items
	create_custom_menu_tree($menu_id, $visible_depth_level);
	if (!$menu_tree) return;
	$t->set_var("item", "");
	array_walk($menu_tree[0]['subs'], 'show_custom_menu_tree');
	
	$menu_title = "";
	$sql = "SELECT * FROM " . $table_prefix . "menus WHERE menu_id = " . $db->tosql($menu_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$menu_title = get_translation($db->f("menu_title"));
	}
	$t->set_var("menu_title", $menu_title);

	$block_parsed = true;

?>