<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  index.php                                                ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

	
	$type = "list";
	include_once("./includes/common.php");
	include_once("./includes/navigator.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./messages/" . $language_code . "/forum_messages.php");
	include_once("./messages/" . $language_code . "/manuals_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/previews_functions.php");
	include_once("./includes/items_properties.php");

	$cms_page_code = "index";
	$script_name = "index.php";
	$current_page = get_custom_friendly_url("index.php");
	$tax_rates = get_tax_rates();
	$page_friendly_url = ""; $page_friendly_params = array();

	include_once("./includes/page_layout.php");
	
?>