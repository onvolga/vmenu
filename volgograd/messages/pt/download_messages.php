<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Parвmetro(s) de download errado(s).");
	define("DOWNLOAD_MISS_PARAM", "Falta(m) parвmetro(s) de download.");
	define("DOWNLOAD_INACTIVE", "Download inactivo.");
	define("DOWNLOAD_EXPIRED", "O seu perнodo de download expirou.");
	define("DOWNLOAD_LIMITED", "Acabou de exceder o nъmero mбximo de downloads.");
	define("DOWNLOAD_PATH_ERROR", "O caminho para o produto nгo pode ser encontrado.");
	define("DOWNLOAD_RELEASE_ERROR", "Versгo nгo encontrada.");
	define("DOWNLOAD_USER_ERROR", "Apenas os utilizadores registados podem fazer o download deste ficheiro.");
	define("ACTIVATION_OPTIONS_MSG", "Opзхes de Activaзгo");
	define("ACTIVATION_MAX_NUMBER_MSG", "Nъmero Mбximo de Activaзхes");
	define("DOWNLOAD_OPTIONS_MSG", "Opзхes de Software/Download");
	define("DOWNLOADABLE_MSG", "Software para Download");
	define("DOWNLOADABLE_DESC", "Ao fazer o download do produto poderб tambйm especificar \"Perнodo de tempo\"");
	define("DOWNLOAD_PERIOD_MSG", "Perнodo de Download");
	define("DOWNLOAD_PATH_MSG", "Caminho para o Ficheiro Descarregбvel");
	define("DOWNLOAD_PATH_DESC", "poderia adicionar mъltiplos caminhos separados por ponto e vнrgula");
	define("UPLOAD_SELECT_MSG", "Seleccione o ficheiro para upload e pressione o botгo {button_name}.");
	define("UPLOADED_FILE_MSG", "O upload do ficheiro <b>{filename}</b> foi concluнdo.");
	define("UPLOAD_SELECT_ERROR", "Por favor, seleccione primeiro um ficheiro.");
	define("UPLOAD_IMAGE_ERROR", "Apenas ficheiros de imagens sгo permitidos.");
	define("UPLOAD_FORMAT_ERROR", "Este tipo de ficheiro nгo й permitido.");
	define("UPLOAD_SIZE_ERROR", "Ficheiros maiores que {filesize} nгo sгo permitidos.");
	define("UPLOAD_DIMENSION_ERROR", "Imagens maiores do que {dimension} nгo sгo permitidas.");
	define("UPLOAD_CREATE_ERROR", "O sistema nгo pode criar o ficheiro.");
	define("UPLOAD_ACCESS_ERROR", "Nгo tem permissгo para fazer o upload de ficheiros.");
	define("DELETE_FILE_CONFIRM_MSG", "Tem a certeza que pretende eliminar este ficheiro?");
	define("NO_FILES_MSG", "Nгo foram encontrados ficheiros");
	define("SERIAL_GENERATE_MSG", "Criar Nъmero de Sйrie");
	define("SERIAL_DONT_GENERATE_MSG", "nгo criar");
	define("SERIAL_RANDOM_GENERATE_MSG", "gerar sйrie aleatуria para o produto de software");
	define("SERIAL_FROM_PREDEFINED_MSG", "encontrar nъmero de sйrie da lista predefinida");
	define("SERIAL_PREDEFINED_MSG", "Nъmeros de Sйrie Predefinidos");
	define("SERIAL_NUMBER_COLUMN", "Nъmero de Sйrie");
	define("SERIAL_USED_COLUMN", "Utilizado");
	define("SERIAL_DELETE_COLUMN", "Eliminar");
	define("SERIAL_MORE_MSG", "Adicionar mais nъmeros de sйrie?");
	define("SERIAL_PERIOD_MSG", "Perнodo do Nъmero de Sйrie");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Mostrar Termos & Condiзхes");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Para fazer o download do produto o utilizador tem que ler e concordar com os nossos termos e condiзхes");
	define("DOWNLOAD_TERMS_MSG", "Termos & Condiзхes");
	define("DOWNLOAD_TERMS_USER_DESC", "Eu li e concordo com os vossos termos e condiзхes");
	define("DOWNLOAD_TERMS_USER_ERROR", "Para fazer o download do produto tem que ler e concordar com os nossos termos e condiзхes");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>