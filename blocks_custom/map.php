<?php

$html_template = get_setting_value($block, "html_template", "map.html");
$t->set_file("block_body", $html_template);

$sql = "SELECT u.user_id as uid, u.company_name as main_company_name, st.p_address as adress_self_delivery ";
$sql .= "FROM " . $table_prefix . "users u ";
$sql .= "INNER JOIN " . $table_prefix . "user_types ut on u.user_type_id = ut.type_id AND ut.is_seller = 1 ";
$sql .= "INNER JOIN " . $table_prefix . "shipping_modules sm on u.user_id = sm.user_added_id AND sm.available_for_user = 1 ";
$sql .= "INNER JOIN " . $table_prefix . "shipping_types st on st.shipping_module_id = sm.shipping_module_id AND st.is_active = 1 AND st.self_delivery = 1";

if (isset($_GET['id'])) {	
	$sql .= " WHERE u.user_id = " . $_GET['id'];
	$t->set_var("set_center", true);
	
} else  {
	$db->query($sql);	
	$t->set_var("set_center", false);	
}

$db->query($sql);
$shop_addresses = "";
while($db->next_record()) {	$shop_addresses .= $db->f("adress_self_delivery"). "|"; }	
$t->set_var("shop_addresses", $shop_addresses);	

$block_parsed = true;
$t->parse("block_body", false);

?>