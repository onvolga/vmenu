<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  navigation_functions.php                                 ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


/**
 * Actual recursive function to show menu items
 *
 * @param integer / array  $item
 * @param integer $key
 * @param integer $parent_key
 */
function show_custom_menu_tree($item, $key, $parent_key = 0) {
	global $t;
	global $menu_tree, $marked_item_ids, $selected_item_id;
	
	if (!is_array($item)) {
		$key  = $item;
		$item = $menu_tree[$key];
	}
	
	$t->set_var("subitems_" . $key, "");
	if (isset($item["subs"])) {
		foreach ($item["subs"] AS $sub_item_key) {
			show_custom_menu_tree($menu_tree[$sub_item_key], $sub_item_key, $key);
		}
	}
	
	$level         = $item["level"] + 1;
	$title         = $item["title"];
	$url           = $item["url"];
	$target        = $item["target"];
	$image         = $item["image"];
	$active_image  = $item["active_image"];
	$prefix        = $item["prefix"];
	$active_prefix = $item["prefix_active"];
	
	$t->set_var("title", $title);
	$t->set_var("url",   $url);
				
	if ($target) {
		$t->set_var("target", 'target="' . $target . '"');
	} else {
		$t->set_var("target", "");
	}
	$t->set_var("level", $level);
				
	$is_item_marked = in_array($key, $marked_item_ids);
	
	$item_class = "";
	if ($selected_item_id == $key) {
		$item_class .= " customMarked customActive";
	}
	if ($is_item_marked) {
		$item_class .= " customMarked";
	}	
	$t->set_var("item_class", $item_class);

	if ($active_prefix && $is_item_marked) {
		$t->set_var("src", $active_prefix);
		$t->parse("image_prefix", false);
	} elseif ($prefix && !$is_item_marked) {
		$t->set_var("src", $prefix);
		$t->parse("image_prefix", false);
	} else {
		$t->set_var("image_prefix", "");
	}			
	
	if ($image || ($is_item_marked && $active_image)) {
		if (!$is_item_marked || ($is_item_marked && !$active_image)) {
			$t->set_var("src", $image);
		} else {
			$t->set_var("src", $active_image);
		}
		$t->parse("image", false);
	} else {
		$t->set_var("image", "");
	}
				
	$subitems = $t->get_var("subitems_" . $key);
	if ($subitems) {
		$t->set_var("subitems", "<ul>" . $subitems . "</ul>");
	} else {
		$t->set_var("subitems", "");
	}	
		
	if ($parent_key > 0) {
		$t->parse_to("item", "subitems_" . $parent_key);
	} else {
		$t->parse("item");
	}
}
/**
 * Create structure with information about menu tree
 *
 * @param integer $side_menu_id
 * @param integer $depth_level - max depth level of items in the tree
 */
function create_custom_menu_tree($side_menu_id, $depth_level = 2) {
	global $db, $table_prefix;	
	global $settings, $menu_tree, $marked_item_ids, $selected_item_id;
	
	$user_id  = get_session("session_user_id");
	$site_url = get_setting_value($settings, "site_url");
	
	$menu_tree        = array();
 	$selected_item_id = 0;
	$marked_item_ids  = array();
	
	// additional connection to get forced friendly urls
	$db2 = new VA_SQL();
	$db2->DBType      = $db->DBType;
	$db2->DBDatabase  = $db->DBDatabase;
	$db2->DBHost      = $db->DBHost;
	$db2->DBPort      = $db->DBPort;
	$db2->DBUser      = $db->DBUser;
	$db2->DBPassword  = $db->DBPassword;
	$db2->DBPersistent= $db->DBPersistent;	
	
	$sql  = " SELECT * FROM " . $table_prefix . "menus_items ";
	$sql .= " WHERE menu_id = " . $db->tosql($side_menu_id, INTEGER);
	if (strlen(get_session("session_user_id"))) {
		$sql .= " AND show_logged=1 ";
	} else {
		$sql .= " AND show_non_logged=1 ";
	}
	$sql .= " ORDER BY menu_order";
	$db->query($sql);
	while ($db->next_record()) {
		$id         = $db->f("menu_item_id");
		$parent_id  = $db->f("parent_menu_item_id");
		$menu_url   = $db->f("menu_url");
		
		if (check_selected_url($menu_url)) {
			$selected_item_id = $id;			
			$marked_item_ids  = explode(",", $db->f("menu_path"));
		}
		
		if ($menu_url == "index.php") {
			$menu_url = $site_url;
		} elseif (preg_match("/^\//", $menu_url)) {
			$menu_url = preg_replace("/^" . preg_quote($site_path, "/") . "/i", "", $menu_url);
			$menu_url = $site_url . get_forced_friendly_url($menu_url, $db2);
		} elseif (!preg_match("/^http\:\/\//", $menu_url) && !preg_match("/^https\:\/\//", $menu_url) && !preg_match("/^javascript\:/", $menu_url)) {
			$menu_url = $site_url . get_forced_friendly_url($menu_url, $db2);
		}
		
		$menu_tree[$id]["title"]           = get_translation($db->f("menu_title"));
		$menu_tree[$id]["url"]             = $menu_url;
		$menu_tree[$id]["path"]            = $db->f("menu_path");
		$menu_tree[$id]["level"]           = strlen(preg_replace("/\d/", "", $menu_tree[$id]["path"]));
		$menu_tree[$id]["image"]           = $db->f("menu_image");
		$menu_tree[$id]["active_image"]    = $db->f("menu_image_active");
		$menu_tree[$id]["prefix"]          = $db->f("menu_prefix");
		$menu_tree[$id]["prefix_active"]   = $db->f("menu_prefix_active");
		$menu_tree[$id]["target"]          = $db->f("menu_target");		
		$menu_tree[$id]["parent_id"]       = $parent_id;
		
		if (!isset($menu_tree[$parent_id]["all_subs"])) {
			$menu_tree[$parent_id]["all_subs"] = array();
			$menu_tree[$parent_id]["subs"] = array();
		}
		$menu_tree[$parent_id]['all_subs'][] = $id;
		
		//echo "id $id parent $parent_id selected $selected_item_id level " . $menu_tree[$id]["level"] . " depth $depth_level<br/>";
		if ($menu_tree[$id]["level"] < $depth_level || $selected_item_id == $id || $depth_level == 0) {
			//echo 'show ' . $menu_tree[$id]["title"] . '<br/>';
			$menu_tree[$parent_id]['subs'][] = $id;
		}					
	}
	
	if($marked_item_ids) {
		foreach ($marked_item_ids AS $id) {
			if (isset($menu_tree[$id])) {
				$menu_tree[$id]['subs'] = $menu_tree[$id]['all_subs'];
			}
		}
	}
	if ($selected_item_id && isset($menu_tree[$selected_item_id]['all_subs'])) {
		$menu_tree[$selected_item_id]['subs'] = $menu_tree[$selected_item_id]['all_subs'];
	}
}


?>