<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// съобщения свързани с ръководства
	define("MANUALS_TITLE", "Ръководства");
	define("NO_MANUALS_MSG", "Не са намерени ръководства");
	define("NO_MANUAL_ARTICLES_MSG", "Няма предмети");
	define("MANUALS_PREV_ARTICLE", "Предишен");
	define("MANUALS_NEXT_ARTICLE", "Следващ");
	define("MANUAL_CONTENT_MSG", "Индекс");
	define("MANUALS_SEARCH_IN_MSG", "Търсене в");
	define("MANUALS_SEARCH_FOR_MSG", "Търсене");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Всички ръководства");
	define("MANUALS_SEARCH_RESULTS_INFO", "Намерени {results_number} артикул(а) за {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Резултати от търсенето");
	define("MANUALS_NOT_FOUND_ANYTHING", "Не е намерено нищо за '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Няма съдържание");
	define("MANUALS_SEARCH_TITLE", "Търсене на ръководства");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Резултати от търсенето на ръководства");

?>