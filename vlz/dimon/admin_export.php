<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_export.php                                         ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	@set_time_limit (900);
	include_once ("./admin_config.php");
	include_once ($root_folder_path . "includes/common.php");
	include_once ($root_folder_path . "includes/record.php");
	include_once ($root_folder_path . "includes/url.php");
	include_once ($root_folder_path . "messages/".$language_code."/cart_messages.php");
	include_once ($root_folder_path . "messages/".$language_code."/download_messages.php");
	include_once("./admin_common.php");

	check_admin_security("import_export");


	// special connection to export data
	$dbe = new VA_SQL();
	$dbe->DBType       = $db->DBType;
	$dbe->DBDatabase   = $db->DBDatabase;
	$dbe->DBUser       = $db->DBUser;
	$dbe->DBPassword   = $db->DBPassword;
	$dbe->DBHost       = $db->DBHost;
	$dbe->DBPort       = $db->DBPort;
	$dbe->DBPersistent = $db->DBPersistent;
	
	$comma_decimal = false;
	if ($comma_decimal) {
		$prices = array('price', 'buying_price', 'trade_price', 'properties_price', 'trade_properties_price',
			'sales_price', 'trade_sales');
	}
	
	$apply_translation = false;
	$eol = get_eol();
	$delimiters_symbols = array(
		"comma" => ",", "tab" => "\t", "semicolon" => ";", "vertical_bar" => "|",
		"row" => "row", "space" => " ", "newline" => $eol);

	$delimiters = array(array("comma", COMMA_MSG), array("tab", TAB_MSG), array("semicolon", SEMICOLON_MSG), array("vertical_bar", VERTICAL_BAR_MSG));
	$related_delimiters = array(array("row", ROWS_MSG), array("comma", COMMA_MSG), array("tab", TAB_MSG), array("space", SPACE_MSG), array("semicolon", SEMICOLON_MSG), array("vertical_bar", VERTICAL_BAR_MSG), array("newline", NEWLINE_MSG));

	
	$errors = "";
	$template_errors = "";
	$template_success = "";
	$sql_where = "";
	$rnd = get_param("rnd");
	$table = get_param("table");
	$csv_delimiter = get_param("csv_delimiter");
	$related_delimiter = get_param("related_delimiter");
	$delimiter_symbol = isset($delimiters_symbols[$csv_delimiter]) ? $delimiters_symbols[$csv_delimiter] : ",";
	$related_delimiter_symbol = isset($delimiters_symbols[$related_delimiter]) ? $delimiters_symbols[$related_delimiter] : "row";
	$operation = get_param("operation");
	$category_id = get_param("category_id");
	$session_rnd = get_session("session_rnd");
	$id = get_param("id");
	$ids = get_param("ids");
	$s_on = get_param("s_on"); // order number / users online
	$s_ne = get_param("s_ne");
	$s_kw = get_param("s_kw");
	$s_sd = get_param("s_sd"); // start date
	$s_ed = get_param("s_ed"); // end date
	$s_ad = get_param("s_ad"); // users address
	$s_ut = get_param("s_ut"); // user type
	$s_ap = get_param("s_ap"); // approved
	$s_os = get_param("s_os");
	$s_ci = get_param("s_ci");
	$s_si = get_param("s_si");
	$s_ex = get_param("s_ex");
	$s_pd = get_param("s_pd");
	$s_cct = get_param("s_cct");
	$s_sti = get_param("s_sti");	
	$s_rn = get_param("s_rn"); // registration number
	$s_ap = get_param("s_ap"); // approved
	$s_pi = get_param("s_pi"); // product id
	
	$type = get_param("type"); // to separate filtered and all requests)

	$s = trim(get_param("s"));
	$sc = get_param("sc");
	$sit = get_param("sit");
	$sl = get_param("sl");
	$ss = get_param("ss");
	$ap = get_param("ap");
	$param_site_id = get_param("param_site_id");

	// order action parameter
	$order_status_update = get_param("order_status_update");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","admin_export.html");

	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->set_var("admin_select_href",     "admin_select.php");
	$t->set_var("admin_export_href",     "admin_export.php");
	$t->set_var("admin_items_list_href", "admin_items_list.php");
	$t->set_var("admin_users_list_href", "admin_newsletter_users.php");

	$admin_export_custom_url = new VA_URL("admin_export_custom.php", true, array("table"));
	$admin_export_custom_url->add_parameter("table", CONSTANT, $table);
	$t->set_var("admin_export_custom_url", $admin_export_custom_url->get_url());

	$is_export = true;
	if ($table == "items" || $table == "items_files") {
		check_admin_security("products_export");
		if ($table == "items") {
			include("./admin_table_items.php");
		} elseif ($table == "items_files") {
			include("./admin_table_items_files.php");
			foreach ($db_columns as $key => $item) {
				$db_columns[$key][0] = str_replace(array("<br>", "<br />"), "", $db_columns[$key][0]);
			}
		}
		$sql_join_before = "";
		$sql_join        = "";
		
		if (strlen($id)) {
			$sql_where = " WHERE i.item_id=" . $dbe->tosql($id, INTEGER);
		} else if (strlen($ids)) {
			$sql_where = " WHERE i.item_id IN (" . $dbe->tosql($ids, TEXT, false) . ")";
		} else {
			
			$category_id = get_param("category_id");
			$search = (strlen($s) || strlen($sit) || strlen($sl) || strlen($ss) || strlen($ap) || strlen($param_site_id)) ? true : false;
			if ($sc) { $category_id = $sc; }
			
			$sa = "";
			$tree = new VA_Tree("category_id", "category_name", "parent_category_id", $table_prefix . "categories", "tree");
			
			$where = array();	
			if ($search && $category_id != 0) {
				$where[] = " c.category_id = ic.category_id ";
				$where[] = " (ic.category_id = " . $dbe->tosql($category_id, INTEGER)
						 . " OR c.category_path LIKE '" . $dbe->tosql($tree->get_path($category_id), TEXT, false) . "%')";
				$sql_join_before .=	" (( ";
				$sql_join  .= " LEFT JOIN " . $table_prefix . "items_categories ic ON i.item_id=ic.item_id) ";
				$sql_join  .= " LEFT JOIN " . $table_prefix . "categories c ON c.category_id = ic.category_id)";
			} elseif (!$search && strlen($category_id)) {
				$where []= " ic.category_id = " . $dbe->tosql($category_id, INTEGER);
				$sql_join_before .=	" ( ";
				$sql_join  .= " LEFT JOIN " . $table_prefix . "items_categories ic ON i.item_id=ic.item_id) ";
			}
								
			if ($s) {
				$sa = explode(" ", $s);
				for($si = 0; $si < sizeof($sa); $si++) {
					$sa[$si] = str_replace("%","\%",$sa[$si]);
					$where[] = " (i.item_name LIKE '%" . $dbe->tosql($sa[$si], TEXT, false) . "%'"
							 .  " OR i.item_code LIKE '%" . $dbe->tosql($sa[$si], TEXT, false) . "%' "
							 .  " OR i.manufacturer_code LIKE '%" . $dbe->tosql($sa[$si], TEXT, false) . "%')";
				}
			}
			if ($sit == 2) {
				$where[] = " (tiny_image = '' OR tiny_image IS NULL)";
				$where[] = " (small_image = '' OR small_image IS NULL)";
				$where[] = " (big_image = '' OR big_image IS NULL)";
				$where[] = " (super_image = '' OR super_image IS NULL)";
			}

			if (strlen($sl)) {
				if ($sl == 1) {
					$where[] = " (i.stock_level>0 OR i.stock_level IS NULL) ";
				} else {
					$where[] = " i.stock_level<1 ";
				}
			}
			if (strlen($ss)) {
				if ($ss == 1) {
					$where[] = " i.is_showing=1 ";
				} else {
					$where[] = " i.is_showing=0 ";
				}
			}
			if (strlen($ap)) {
				if ($ap == 1) {
					$where[] = " i.is_approved=1 ";
				} else {
					$where[] = " i.is_approved=0 ";
				}
			}
			if (strlen($param_site_id)) {
				if ($param_site_id == "all") {
					$where[] = " i.sites_all=1 ";
				} else {
					$sql_join_before .= "(";
					$sql_join .= " LEFT JOIN " . $table_prefix . "items_sites s ON (s.item_id = i.item_id AND i.sites_all = 0 )) ";
					$where[] = " (s.site_id=" . $dbe->tosql($param_site_id, INTEGER) . " OR i.sites_all=1) ";
				}
			}
			
			if (count($where)) {
				$sql_where = " WHERE " . implode (" AND ", $where);				
			}
		}
	} else if ($table == "categories") {
		check_admin_security("categories_export");
		include("./admin_table_categories.php");
	} else if ($table == "registrations") {
		check_admin_security("admin_registration");
		include("./admin_table_registrations.php");
		
		$where = "";
		if (strlen($id)) {
			$where = " reg.registration_id=" . $dbe->tosql($id, INTEGER);
		} else if (strlen($ids)) {
			$where = " reg.registration_id IN (" . $dbe->tosql($ids, TEXT, false) . ")";
		} else {
			if ($s_rn) {
				if (preg_match("/^(\d+)(,\d+)*$/", $s_rn))	{
					$where  = " (reg.registration_id IN (" . $s_rn . ") ";
					$where .= " OR reg.invoice_number=" . $dbe->tosql($s_rn, TEXT);
					$where .= " OR reg.serial_number=" . $dbe->tosql($s_rn, TEXT) . ") ";
				} else {
					$where .= " (reg.invoice_number=" . $dbe->tosql($s_rn, TEXT);
					$where .= " OR reg.serial_number=" . $dbe->tosql($s_rn, TEXT) . ") ";
				}
			}
			
			if ($s_pi) {
				if (strlen($where)) { $where .= " AND "; }
				$where .= " reg.item_id=" . $dbe->tosql($s_pi, INTEGER);
			}
			
			if ($s_ne) {
				if (strlen($where)) { $where .= " AND "; }
				$s_ne_sql = $dbe->tosql($s_ne, TEXT, false);
				
				$where .= " (u.name LIKE '%" . $s_ne_sql . "%'";
				$name_parts = explode(" ", $s_ne, 2);
				if (sizeof($name_parts) == 1) {
					$where .= " OR u.first_name LIKE '%" . $s_ne_sql . "%'";
					$where .= " OR u.last_name LIKE '%" . $s_ne_sql . "%'";
				} else {
					$where .= " OR (u.first_name LIKE '%" . $dbe->tosql($name_parts[0], TEXT, false) . "%' ";
					$where .= " AND u.last_name LIKE '%" . $dbe->tosql($name_parts[1], TEXT, false) . "%') ";
				}
				$where .= ") ";	
			}
			
			if ($s_kw) {
				if (strlen($where)) { $where .= " AND "; }
				$s_kw_sql = $dbe->tosql($s_kw, TEXT, false);
				$where .= " (reg.item_name LIKE '%" . $s_kw_sql . "%'";
				$where .= " OR reg.item_code LIKE '%" . $s_kw_sql . "%'";
				$where .= " OR it.item_name  LIKE '%" . $s_kw_sql . "%'";
				$where .= " OR it.item_code  LIKE '%" . $s_kw_sql . "%')";
			}
			
			if ($s_sd) {
				if (strlen($where)) { $where .= " AND "; }
				$where .= " reg.date_added>=" . $dbe->tosql($s_sd, DATE);
			}
			if ($s_ed) {
				if (strlen($where)) { $where .= " AND "; }
				$day_after_end = mktime (0, 0, 0, $s_ed[MONTH], $s_ed[DAY] + 1, $s_ed[YEAR]);
				$where .= " reg.date_added<" . $dbe->tosql($day_after_end, DATE);
			}		
			
			if (strlen($s_ap)) {
				if (strlen($where)) { $where .= " AND "; }
				if ($s_ap) {
					$where .= " reg.is_approved=1";
				} else {
					$where .= " reg.is_approved=0";
				}
			}
		}
		if ($where) {
			$sql_where = " WHERE " . $where;				
		}
			
	} else if ($table == "users") {
		check_admin_security("export_users");
		include("./admin_table_users.php");
		if (strlen($id)) {
			$sql_where = " WHERE user_id>" . $dbe->tosql($id, INTEGER);
		} else if (strlen($ids)) {
			$sql_where = " WHERE user_id IN (" . $dbe->tosql($ids, TEXT, false) . ")";
		} else {
			if (strlen($s_ne)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$s_ne_sql = $dbe->tosql($s_ne, TEXT, false);
				$sql_where .= " (u.email LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR u.login LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR u.name LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR u.first_name LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR u.last_name LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR u.company_name LIKE '%" . $s_ne_sql . "%')";
			}
	  
			if (strlen($s_ad)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " (u.address1 LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR u.address2 LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR u.city LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR u.province LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR u.state_id LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR u.zip LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR u.country_id LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR s.state_name LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%'";
				$sql_where .= " OR c.country_name LIKE '%" . $dbe->tosql($s_ad, TEXT, false) . "%')";
				$sql_join_before = " ((";
				$sql_join  = " LEFT JOIN " . $table_prefix . "countries c ON u.country_id=c.country_id) ";
				$sql_join .= " LEFT JOIN " . $table_prefix . "states s ON u.state_id=s.state_id)";
			}
	  
			if (strlen($s_sd)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$s_sd_value = parse_date($s_sd, $date_edit_format, $date_errors);
				$sql_where .= " u.registration_date>=" . $dbe->tosql($s_sd_value, DATE);
			}
	  
			if (strlen($s_ed)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$end_date = parse_date($s_ed, $date_edit_format, $date_errors);
				$day_after_end = mktime (0, 0, 0, $end_date[MONTH], $end_date[DAY] + 1, $end_date[YEAR]);
				$sql_where .= " u.registration_date<" . $dbe->tosql($day_after_end, DATE);
			}
	  
			if (strlen($s_ut)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " u.user_type_id=" . $dbe->tosql($s_ut, INTEGER);
			}
	  
			if (strlen($s_ap)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= ($s_ap == 1) ? " u.is_approved=1 " : " u.is_approved=0 ";
			}
	  
			if (strlen($s_on)) {
				$current_date = va_time();
				$cyear = $current_date[YEAR]; $cmonth = $current_date[MONTH]; $cday = $current_date[DAY];
				$online_ts = mktime ($current_date[HOUR], $current_date[MINUTE] - $online_time, $current_date[SECOND], $cmonth, $cday, $cyear);
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				if ($s_on == 1) {
					$sql_where .= " u.last_visit_date>=" . $dbe->tosql($online_ts, DATETIME);
				} else {
					$sql_where .= " u.last_visit_date<" . $dbe->tosql($online_ts, DATETIME);
				}
			}
			if ($sql_where) { $sql_where = " WHERE " . $sql_where; }
		}
	} else if ($table == "newsletters_users") {
		check_admin_security("export_users");
		include("./admin_table_emails.php");
		if (strlen($id)) {
			$sql_where = " WHERE email_id>" . $dbe->tosql($id, INTEGER);
		} else if (strlen($ids)) {
			$sql_where = " WHERE email_id IN (" . $dbe->tosql($ids, TEXT, false) . ")";
		}
	} else if ($table == "orders") {
		check_admin_security("sales_orders");
		include_once($root_folder_path."includes/order_items.php");
		include_once($root_folder_path."includes/order_links.php");
		include("./admin_table_orders.php");

		$sql_join = " INNER JOIN " . $table_prefix . "orders_items oi ON o.order_id=oi.order_id ";

		if (strlen($id)) {
			$sql_where .= " WHERE o.order_id>" . $dbe->tosql($id, INTEGER);
		} else if (strlen($ids)) {
			$sql_where .= " WHERE o.order_id IN (" . $dbe->tosql($ids, TEXT, false) . ")";
		} else {

			$sql  = " SELECT setting_name,setting_value FROM " . $table_prefix . "global_settings ";
			$sql .= " WHERE setting_type='order_info' ";
			if ($multisites_version) {
				$sql .= " AND (site_id=1 OR site_id=" . $dbe->tosql($site_id,INTEGER) . ") ";
				$sql .= " ORDER BY site_id ASC ";
			}
			$dbe->query($sql);
			while($dbe->next_record()) {
				$order_info[$dbe->f("setting_name")] = $dbe->f("setting_value");
			}

			if (preg_match("/^(\d+)(,\d+)*$/", $s_on))	{
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " (o.order_id IN (" . $dbe->tosql($s_on, TEXT, false) . ") ";
				$sql_where .= " OR o.invoice_number=" . $dbe->tosql($s_on, TEXT);
				$sql_where .= " OR o.transaction_id=" . $dbe->tosql($s_on, TEXT) . ") ";
			} else if (strlen($s_on)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " (o.invoice_number=" . $dbe->tosql($s_on, TEXT);
				$sql_where .= " OR o.transaction_id=" . $dbe->tosql($s_on, TEXT) . ") ";
			}

			if(strlen($s_ne)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$s_ne_sql = $dbe->tosql($s_ne, TEXT, false);
				$sql_where .= " (o.email LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR o.name LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR o.first_name LIKE '%" . $s_ne_sql . "%'";
				$sql_where .= " OR o.last_name LIKE '%" . $s_ne_sql . "%')";
			}

			if(strlen($s_kw)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " (oi.item_name LIKE '%" . $dbe->tosql($s_kw, TEXT, false) . "%'";
				$sql_where .= " OR oi.item_properties LIKE '%" . $dbe->tosql($s_kw, TEXT, false) . "%'";
				$sql_where .= " OR o.shipping_type_desc LIKE '%" . $dbe->tosql($s_kw, TEXT, false) . "%')";
			}

			if(strlen($s_sd)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$s_sd_value = parse_date($s_sd, $date_edit_format, $date_errors);
				$sql_where .= " o.order_placed_date>=" . $dbe->tosql($s_sd_value, DATE);
			}

			if(strlen($s_ed)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$end_date = parse_date($s_ed, $date_edit_format, $date_errors);
				$day_after_end = mktime (0, 0, 0, $end_date[MONTH], $end_date[DAY] + 1, $end_date[YEAR]);
				$sql_where .= " o.order_placed_date<" . $dbe->tosql($day_after_end, DATE);
			}

			if(strlen($s_os)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " o.order_status=" . $dbe->tosql($s_os, INTEGER);
			}

			if(strlen($s_ci)) {
				if ($order_info["show_delivery_country_id"] == 1) {
					if (strlen($sql_where)) { $sql_where .= " AND "; }
					$sql_where .= " o.delivery_country_id=" . $dbe->tosql($s_ci, INTEGER);
				} else if ($order_info["show_country_id"] == 1) {
					if (strlen($sql_where)) { $sql_where .= " AND "; }
					$sql_where .= " o.country_id=" . $dbe->tosql($s_ci, INTEGER);
				}
			}
			if(strlen($s_si)) {
				if ($order_info["show_delivery_state_id"] == 1) {
					if (strlen($sql_where)) { $sql_where .= " AND "; }
					$sql_where .= " o.delivery_state_id=" . $dbe->tosql($s_si, INTEGER);
				} else if ($order_info["show_state_id"] == 1) {
					if (strlen($sql_where)) { $sql_where .= " AND "; }
					$sql_where .= " o.state_id=" . $dbe->tosql($s_si, INTEGER);
				}
			}
			if(strlen($s_sti)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " o.site_id=" . $dbe->tosql($s_sti, INTEGER);				
			}

			if (strlen($s_ex)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= ($s_ex == 1) ? " o.is_exported=1 " : " (o.is_exported<>1 OR o.is_exported IS NULL) ";
			}

			if (strlen($s_pd)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_join .= " INNER JOIN " . $table_prefix . "order_statuses os ON os.status_id=o.order_status ";
				$sql_where .= ($s_pd == 1) ? " os.paid_status=1 " : " (os.paid_status=0 OR os.paid_status IS NULL) ";
			}

			if(strlen($s_cct)) {
				if (strlen($sql_where)) { $sql_where .= " AND "; }
				$sql_where .= " o.cc_type=" . $dbe->tosql($s_cct, INTEGER);
			}
			
			if (!$sql_where && $type == "filtered") {
				$dbe->query("SELECT status_id FROM " . $table_prefix . "order_statuses WHERE (is_list=1 OR is_list IS NULL)");
				if ($dbe->next_record()) {
					$orders_statuses = array();
					do {
						$orders_statuses[] = $dbe->f("status_id");						
					} while ($dbe->next_record());					
					if (strlen($sql_where)) { $sql_where .= " AND "; }
					$sql_where .= " o.order_status IN ( " . $dbe->tosql($orders_statuses, INTEGERS_LIST) . " ) ";
				}			
			}
			if ($sql_where) { $sql_where = " WHERE " . $sql_where; }
		}

	} else if ($table == "tax_rates") {
		include("./admin_table_tax_rates.php");
	} else {
		$table_name = "";
		$table_title = "";
		$errors = CANT_FIND_TABLE_MSG;
	}	

	if (strlen(!$errors)) {
		$admin_export_custom_url->add_parameter("table", CONSTANT, $table);

		$sql  = " SELECT setting_name, setting_value FROM " . $table_prefix . "global_settings ";
		$sql .= " WHERE setting_type=" . $dbe->tosql($table, TEXT);
		if ($multisites_version) {
			$sql .= " AND (site_id=1 OR site_id=" . $dbe->tosql($site_id,INTEGER) . ") ";
			$sql .= " ORDER BY site_id ASC ";
		}
		$dbe->query($sql);
		while ($dbe->next_record()) {
			$custom_field = $dbe->f("setting_name");
			$custom_value = $dbe->f("setting_value");
			$admin_export_custom_url->add_parameter("field", CONSTANT, $custom_field);

			$edit_link = " &nbsp; <a href=\"" . $admin_export_custom_url->get_url() . "\"><font color=blue size=1>Edit</font></a>";
			$db_columns[$custom_field]  = array($custom_field, TEXT, CUSTOM_FIELD, false, $custom_value, $edit_link);
		}
		if(isset($related_columns)) {
			$admin_export_custom_url->add_parameter("table", CONSTANT, $related_table);

			$sql  = " SELECT setting_name, setting_value FROM " . $table_prefix . "global_settings ";
			$sql .= " WHERE setting_type=" . $dbe->tosql($related_table, TEXT);
			if ($multisites_version) {
				$sql .= " AND (site_id=1 OR site_id=" . $dbe->tosql($site_id,INTEGER) . ") ";
				$sql .= " ORDER BY site_id ASC ";
			}
			$dbe->query($sql);
			while ($dbe->next_record()) {
				$custom_field = $dbe->f("setting_name");
				$custom_value = $dbe->f("setting_value");
				$admin_export_custom_url->add_parameter("field", CONSTANT, $custom_field);
				$edit_link = " &nbsp; <a href=\"" . $admin_export_custom_url->get_url() . "\"><font color=blue size=1>Edit</font></a>";
				$related_columns[$custom_field]  = array($custom_field, TEXT, CUSTOM_FIELD, false, $custom_value, $edit_link);
			}
		}
	}

	$t->set_var("table", $table);
	$t->set_var("table_title", $table_title);

	if ($operation == "save_template") {
		$template_name = get_param("template_name");
	  if (!strlen($template_name)) {
			$template_errors = str_replace("{field_name}", EXPORT_TEMPLATE_MSG, REQUIRED_MESSAGE);
		}

		if(!strlen($errors) && !strlen($template_errors)) {
			// save new export template
			$r = new VA_Record($table_prefix . "export_templates");
			$r->add_where("template_id", INTEGER);
			$r->add_textbox("template_name", TEXT);
			$r->add_textbox("table_name", TEXT);
			$r->add_textbox("admin_id_added_by", INTEGER);
			$r->add_textbox("date_added", DATETIME);
			
			if ($db_type == "postgre") {
				$new_template_id = get_db_value(" SELECT NEXTVAL('seq_" . $table_prefix . "export_templates') ");
				$r->change_property("template_id", USE_IN_INSERT, true);
				$r->set_value("template_id", $new_template_id);
			}

			$r->set_value("template_name", $template_name);
			$r->set_value("table_name", $table);
			$r->set_value("admin_id_added_by", get_session("session_admin_id"));
			$r->set_value("date_added", va_time());
			$r->insert_record();

			if ($db_type == "mysql") {
				$new_template_id = get_db_value(" SELECT LAST_INSERT_ID() ");
				$r->set_value("template_id", $new_template_id);
			} elseif ($db_type == "access") {
				$new_template_id = get_db_value(" SELECT @@IDENTITY ");
				$r->set_value("template_id", $new_template_id);
			} elseif ($db_type == "db2") {
				$new_template_id = get_db_value(" SELECT PREVVAL FOR seq_" . $table_prefix . "export_templates FROM " . $table_prefix . "export_templates");
				$r->set_value("template_id", $new_template_id);
			}

			if (strlen($new_template_id)) {
				// start adding fields
				$fld = new VA_Record($table_prefix . "export_fields");
				$fld->add_where("field_id", INTEGER);
				$fld->add_textbox("template_id", INTEGER);
				$fld->set_value("template_id", $new_template_id);
				$fld->add_textbox("field_order", INTEGER);
				$fld->add_textbox("field_title", TEXT);
				$fld->add_textbox("field_source", TEXT);

				$field_order = 0;
				$total_columns = get_param("total_columns");
				for($col = 1; $col <= $total_columns; $col++) {
					$field_title = get_param("column_title_" . $col);
					$field_source = get_param("field_source_" . $col);
					$column_checked = get_param("db_column_" . $col);
					if($column_checked) { // if there is column title we can save this field even if it source empty
						$field_order++;
						$fld->set_value("field_order", $field_order);
						$fld->set_value("field_title", $field_title);
						$fld->set_value("field_source", $field_source);
						$fld->insert_record();
					}
				}
			}
			$template_success = EXPORT_TEMPLATE_SAVED_MSG;
		}
	} else if($operation == "export")	{
		if(!strlen($errors)) {

			// prepare categories for items table
			$categories = array();
			if ($table == "items") {
				$sql = "SELECT category_id,category_name FROM " . $table_prefix . "categories ";
				$dbe->query($sql);
				while ($dbe->next_record()) {
					$category_id = $dbe->f("category_id");
					$category_name = $dbe->f("category_name");
					if ($apply_translation) {
						$category_name = get_translation($category_name);
					}
					$categories[$category_id] = $category_name;

				}
			}

			// connection for additional operations
			$dbs = new VA_SQL();
			$dbs->DBType       = $dbe->DBType;
			$dbs->DBDatabase   = $dbe->DBDatabase;
			$dbs->DBUser       = $dbe->DBUser;
			$dbs->DBPassword   = $dbe->DBPassword;
			$dbs->DBHost       = $dbe->DBHost;
			$dbs->DBPort       = $dbe->DBPort;
			$dbs->DBPersistent = $dbe->DBPersistent;

			$columns = array();
			$total_columns = get_param("total_columns");
			$columns_selected = 0;
			$db_column        = 0;
			$columns_list     = "";
			$csv_columns_list = "";
			$exported_fields  = "";

			// generate db columns list
			foreach ($db_columns as $column_name => $column_info) {
				if($column_info[2] != RELATED_DB_FIELD && $column_info[2] != CUSTOM_FIELD) {
					if (!preg_match("/^order_property_/", $column_name)
						&& !preg_match("/^item_property_/", $column_name)
						&& !preg_match("/^item_feature_/", $column_name)
						&& !preg_match("/^registration_property_/", $column_name)) {
						$db_column++;
						if($db_column > 1) {
							$columns_list .= ", ";
						}				
						$columns_list .= $table_alias . "." . $column_name;
					}
				}
			}
			
			// generate selected columns
			$related_selected = 0;
			for($col = 1; $col <= $total_columns; $col++) {
				$column_title = get_param("column_title_" . $col);
				$field_source = get_param("field_source_" . $col);
				$column_checked  = get_param("db_column_" . $col);
				if($column_checked) { // get column only if it was checked
					$columns_selected++;
					if($columns_selected > 1) {
						$exported_fields .= "|";
						$csv_columns_list .= $delimiter_symbol;
					}
					$exported_fields .= $column_title;
					if(preg_match("/[,;\"\n\r\t\s]/", $column_title)) {
						$csv_columns_list .= "\"" . str_replace("\"", "\"\"", $column_title) . "\"";
					} else {
						$csv_columns_list .= $column_title;
					}
					$columns[] = $field_source;
					if (preg_match("/^oi_/", $field_source) || preg_match("/\{oi_/", $field_source)) {
						$related_selected++;
						$selected_related_columns[$field_source] = 1;
					}
				}
			}

/* DELETE BLOCK
			$total_related = get_param("total_related");
			for($col = 1; $col <= $total_related; $col++) {
				$column_name = get_param("related_column_" . $col);
				if ($column_name) {
					$related_selected++;
					$columns_selected++;
					if ($related_columns[$column_name][2] == CUSTOM_FIELD) {
						$column_alias = $column_name;
					} else {
						$column_alias = $related_table_alias."_".$column_name;
					}
					if (preg_match("/^order_item_property_/", $column_name)) {
						if ($columns_selected > 1) {
							$csv_columns_list .= $delimiter_symbol;
							$exported_fields .= ",";
						}
					} else {
						if ($columns_selected > 1) {
							//$columns_list .= ",";
							$csv_columns_list .= $delimiter_symbol;
							$exported_fields .= ",";
						}
						//$columns_list .= $related_table_alias.".".$column_name . " AS " . $column_alias;
					}
					if(preg_match("/[,;\"\n\r\t\s]/", $related_columns[$column_name][0])) {
						$csv_columns_list .= "\"" . str_replace("\"", "\"\"", $related_columns[$column_name][0]) . "\"";
					} else {
						$csv_columns_list .= $related_columns[$column_name][0];
					}
					$exported_fields .= $column_alias;
					$columns[] = $column_alias;
					$selected_related_columns[$column_alias] = 1;
				}
			}//*/


			//CUSTOM_FIELD

			if (isset($related_columns)) {
				// generate db columns list
				foreach ($related_columns as $column_name => $column_info) {
					if($column_info[2] != RELATED_DB_FIELD && $column_info[2] != CUSTOM_FIELD) {
						$column_alias = $related_table_alias."_".$column_name;
						if (!preg_match("/^order_item_property_/", $column_name)) {
							$columns_list .= ",";
							$columns_list .= $related_table_alias.".".$column_name . " AS " . $column_alias;
						}
					}
				}
			}


			$exported_fields .= "|csv_delimiter" . $csv_delimiter. "csv_delimiter";
			$exported_fields .= "|related_delimiter" . $related_delimiter . "related_delimiter";
			// update default columns list
			if ($table == "users") {
				$sql  = " UPDATE " . $table_prefix . "admins SET exported_user_fields=" . $dbe->tosql($exported_fields, TEXT);
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$dbe->query($sql);
			} else if ($table == "newsletters_users") {
				$sql  = " UPDATE " . $table_prefix . "admins SET exported_email_fields=" . $dbe->tosql($exported_fields, TEXT);
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$dbe->query($sql);
			} else if ($table == "orders") {
				$sql = " UPDATE " . $table_prefix . "admins SET exported_order_fields=" . $dbe->tosql($exported_fields, TEXT);
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$dbe->query($sql);
			} else if ($table == "items") {
				$sql = " UPDATE " . $table_prefix . "admins SET exported_item_fields=" . $dbe->tosql($exported_fields, TEXT);
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$dbe->query($sql);
			}

			if (isset($fp) && $fp) {
				fwrite($fp, $csv_columns_list.$eol); 
				if (isset($fp_copy) && isset($file_path_copy) && strlen($file_path_copy)) {
					fwrite($fp_copy, $csv_columns_list.$eol); 
				}
			} else {
				$csv_filename = $table_name . ".csv";
				header("Pragma: private");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: private", false);
				header("Content-Type: application/octet-stream");
				header("Content-Disposition: attachment; filename=" . $csv_filename);
				header("Content-Transfer-Encoding: binary");
		  
				echo $csv_columns_list . $eol;
			}

			$exported_user_id = 0; $exported_order_id = 0;
			if ($table == "users") {
				$sql  = " SELECT exported_user_id FROM " . $table_prefix . "admins ";
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$exported_user_id = get_db_value($sql);
			} else if ($table == "orders") {
				$sql  = " SELECT exported_order_id FROM " . $table_prefix . "admins ";
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$exported_order_id = get_db_value($sql);
			} else if ($table == "newsletters_users") {
				$sql  = " SELECT exported_email_id FROM " . $table_prefix . "admins ";
				$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
				$exported_email_id = get_db_value($sql);
			}
			$max_id = 0;
			// check records number
			$sql = "SELECT COUNT(*) FROM ";
			if (isset($sql_join_before) && $sql_join_before) { $sql .= $sql_join_before; }
			$sql .= $table_name . " " . $table_alias;
			if (isset($sql_join) && $sql_join) { $sql .= $sql_join; }
			$total_records = get_db_value($sql . $sql_where);
			$records_per_page = 1000;
			$total_pages = ceil($total_records / $records_per_page);

			// export data
			$sql = "SELECT " . $columns_list . " FROM ";
			if (isset($sql_join_before) && $sql_join_before) { $sql .= $sql_join_before; }
			$sql .= $table_name . " " . $table_alias;
			if (isset($sql_join) && $sql_join) { $sql .= $sql_join; }
			if (isset($table_pk) && $table_pk) {
				if (strlen($table_alias)) {
					$order_by = " ORDER BY " . $table_alias . "." . $table_pk;
				} else {
					$order_by = " ORDER BY " . $table_pk;
				}
			} else {
				$order_by = "";
			}
			$data_sql = $sql.$sql_where.$order_by;

			// START output data
			$row_data = array(); $record_number = 0; $related_number = 0; $prev_id = "";

			for ($page_number = 1; $page_number <= $total_pages; $page_number++) {

				$dbe->RecordsPerPage = $records_per_page;
				$dbe->PageNumber = $page_number;
				$dbe->query($data_sql);
				while ($dbe->next_record()) {
					if (!strlen($prev_id)) {
						$prev_id = $dbe->f($table_pk);
					}
					$record_number++;
					$row_id = $dbe->f($table_pk);
					if ($row_id > $max_id) { $max_id = $row_id; }
					if ($prev_id != $row_id || ($record_number > 1 && $related_delimiter == "row" && $related_selected > 0)) {
						// output csv
						$csv_row = "";
						for($i = 0; $i < $columns_selected; $i++) {
							$column_name = $columns[$i];
							$field_value = $row_data[$column_name];
							if ($column_name == "oi_item_properties") {
								$field_value = preg_replace("/^\<br\>/", "", $field_value);
								$field_value = preg_replace("/\<br\>/", "; ", $field_value);
							}
							
							if ($comma_decimal && in_array($column_name, $prices)) {
								$field_value = str_replace('.', ',', $field_value);
							}
							
							if (preg_match("/[,;\"\n\r\t\s]/", $field_value)) {
								$field_value = "\"" . str_replace("\"", "\"\"", $field_value) . "\"";
							}
							if($i > 0) {
								$csv_row .= $delimiter_symbol;
							}
							$csv_row .= $field_value;
						}
						if (isset($fp) && $fp) {
							fwrite($fp, $csv_row.$eol); 
							if (isset($fp_copy) && isset($file_path_copy) && strlen($file_path_copy)) {
								fwrite($fp_copy, $csv_row.$eol); 
							}
						} else {
							echo $csv_row.$eol;
						}
						// end output
						$related_number = 0;
		  
						// update exported status
						if ($table_name == $table_prefix . "orders") {
							$dbs->query("UPDATE " . $table_prefix . "orders SET is_exported=1 WHERE order_id=" . $prev_id);
							// update order status if such option selected
							if (strlen($order_status_update)) {
								update_order_status($prev_id, $order_status_update, true, "", $status_error);
							}
						}
					}
					$related_number++;
		  
					// collect data for next step
					for($i = 0; $i < $columns_selected; $i++) {
						$column_name = $columns[$i];
		  
						$field_value = "";
						if ($column_name == "item_category") {
							$item_id = $dbe->f("item_id");
							$sql  = " SELECT ic.category_id, c.category_path FROM " . $table_prefix . "items_categories ic ";
							$sql .= " LEFT JOIN " . $table_prefix . "categories c ON ic.category_id=c.category_id ";
							$sql .= " WHERE  ic.item_id=" . $dbe->tosql($item_id, INTEGER);
							$dbs->query($sql);
							while ($dbs->next_record()) {
								$category = "";
								$category_path = $dbs->f("category_path") . $dbs->f("category_id");
								// build full category path if available
								$categories_ids = explode(",", $category_path);
								for ($ci = 0; $ci < sizeof($categories_ids); $ci++) {
									$category_id = $categories_ids[$ci];
									if ($category_id > 0) {
										if (strlen($category)) { $category .= " > "; }
										$category .= $categories[$category_id];
									}
								}
								if (strlen($field_value)) { $field_value .= ";"; }
								// for top category use zero number
								if (!strlen($category)) { $category = 0; }
								$field_value .= $category;
							}
		  
						} else if (preg_match("/^item_property_/", $column_name)) {
							$property_name = substr($column_name, 14);
							$item_id = $dbe->f("item_id");
							$sql  = " SELECT property_id, control_type,property_description FROM " . $table_prefix . "items_properties ";
							$sql .= " WHERE item_id=" . $dbe->tosql($item_id, INTEGER);
							$sql .= " AND property_name=" . $dbe->tosql($property_name, TEXT);
							$dbs->query($sql);
							if ($dbs->next_record()) {
								$property_id = $dbs->f("property_id");
								$control_type = $dbs->f("control_type");
								if ($control_type == "LABEL" || $control_type == "TEXTBOX" || $control_type == "TEXTAREA") {
									if ($apply_translation) {
										$field_value = get_translation($dbs->f("property_description"));
									} else {
										$field_value = $dbs->f("property_description");
									}
								} else {
									$sql  = " SELECT property_value,additional_price FROM " . $table_prefix . "items_properties_values ";
									$sql .= " WHERE property_id=" . $dbe->tosql($property_id, INTEGER);
									$dbs->query($sql);
									while($dbs->next_record()) {
										$option_value = $dbs->f("property_value");
										$additional_price = $dbs->f("additional_price");
										if (strlen($field_value)) { $field_value .= ";"; }
										$field_value .= $option_value;
										if (strlen($additional_price)) {
											$field_value .= "=".$additional_price;
										}
									}
		  
								}
							}
						} else if (preg_match("/^item_feature_(\d+)_(.+)$/", $column_name, $matches)) {
							$group_id = $matches[1];
							$feature_name = $matches[2];
							$item_id = $dbe->f("item_id");
							$sql  = " SELECT fg.group_name, f.feature_name, f.feature_value ";
							$sql .= " FROM (" . $table_prefix . "features f ";
							$sql .= " INNER JOIN " . $table_prefix . "features_groups fg ON f.group_id=fg.group_id) ";
							$sql .= " WHERE f.item_id=" . $dbe->tosql($item_id, INTEGER);
							$sql .= " AND f.group_id=" . $dbe->tosql($group_id, INTEGER);
							$sql .= " AND f.feature_name=" . $dbe->tosql($feature_name, TEXT);
							$dbs->query($sql);
							if ($dbs->next_record()) {
								if ($apply_translation) {
									$field_value = get_translation($dbs->f("feature_value"));
								} else {
									$field_value = $dbs->f("feature_value");
								}
							}
						} else if (preg_match("/^order_property_/", $column_name)) {
							$property_id = substr($column_name, 15);
							$order_id = $dbe->f("order_id");
							$order_properties = array();
							$sql  = " SELECT op.property_id, op.property_type, op.property_name, op.property_value, ";
							$sql .= " op.property_price, op.property_points_amount, op.tax_free ";
							$sql .= " FROM " . $table_prefix . "orders_properties op ";
							$sql .= " WHERE op.order_id=" . $dbe->tosql($order_id, INTEGER);
							$sql .= " AND op.property_id=" . $dbe->tosql($property_id, INTEGER);
							$dbs->query($sql);
							while ($dbs->next_record()) {
								$property_value = $dbs->f("property_value");
								if (strlen($field_value)) { $field_value .= "; "; }
								if ($apply_translation) {
									$field_value .= get_translation($property_value);
								} else {
									$field_value .= $property_value;
								}
							}
		  
						} else if (preg_match("/^registration_property_/", $column_name)) {
							$property_id = substr($column_name, strlen("registration_property_"));
							$registration_id = $dbe->f("registration_id");
							$sql  = " SELECT property_value FROM " . $table_prefix . "registration_properties ";
							$sql .= " WHERE registration_id=" . $dbe->tosql($registration_id, INTEGER);
							$sql .= " AND property_id=" . $dbe->tosql($property_id, INTEGER);
							$dbs->query($sql);
							$field_value_parts = array();
							while ($dbs->next_record()) {
								if ($apply_translation) {
									$field_value_parts[] = get_translation($dbs->f("property_value"));
								} else {
									$field_value_parts[] = $dbs->f("property_value");
								}
							}
							$control_type = $db_columns[$column_name]["control_type"];
							if(($control_type == "CHECKBOXLIST" ||  $control_type == "RADIOBUTTON" || $control_type == "LISTBOX")) {
								$field_value = "";
								foreach ($field_value_parts AS $field_value_part) {
									$sql  = " SELECT property_value FROM " . $table_prefix . "registration_custom_values ";
									$sql .= " WHERE property_value_id=" . $dbe->tosql($field_value_part, INTEGER);
									$dbs->query($sql);
									if ($dbs->next_record()) {
										if ($field_value) $field_value .= " / ";
										$field_value .= $dbs->f("property_value");						
									}
								}
							} else {
								$field_value = implode(" / ", $field_value_parts);
							}
						} else if (preg_match("/^oi_order_item_property_/", $column_name)) {
							$property_id = substr($column_name, 23);
							$order_item_id = $dbe->f("oi_order_item_id");
							$sql  = " SELECT property_value FROM " . $table_prefix . "orders_items_properties ";
							$sql .= " WHERE order_item_id=" . $order_item_id;
							$sql .= " AND (property_id=" . $dbe->tosql($property_id, INTEGER, true, false);
							$sql .= " OR property_name=" . $dbe->tosql($property_id, TEXT) . ") ";
							$dbs->query($sql);
							if ($dbs->next_record()) {
								if ($apply_translation) {
									$field_value = get_translation($dbs->f("property_value"));
								} else {
									$field_value = $dbs->f("property_value");
								}
							}
						} else if ($column_name == "manufacturer_name") {
							$manufacturer_id = $dbe->f("manufacturer_id");
							if (strlen($manufacturer_id)) {
								$sql  = " SELECT manufacturer_name FROM " . $table_prefix . "manufacturers ";
								$sql .= " WHERE manufacturer_id=" . $dbe->tosql($manufacturer_id, INTEGER);
								$dbs->query($sql);
								if ($dbs->next_record()) {
									if ($apply_translation) {
										$field_value = get_translation($dbs->f("manufacturer_name"));
									} else {
										$field_value = $dbs->f("manufacturer_name");
									}
								}
							}
						} else {
							if ((isset($db_columns[$column_name]) && $db_columns[$column_name][2] == CUSTOM_FIELD)) {
								$field_source = $db_columns[$column_name][4];
								$field_value  = get_field_value($field_source);
							} else if ((isset($related_columns) && isset($related_columns[$column_name]) && $related_columns[$column_name][2] == CUSTOM_FIELD)) {
								$field_source = $related_columns[$column_name][4];
								$field_value  = get_field_value($field_source);
							} else {
								$column_type = TEXT; $related_column_name = "";
								if (isset($db_columns[$column_name])) {
									$column_type = $db_columns[$column_name][1];
								} else if (isset($related_table_alias) && $related_table_alias && preg_match("/^".$related_table_alias."_/", $column_name)) {
									$related_column_name = preg_replace("/^".$related_table_alias."_/", "", $column_name);
									if (isset($related_columns[$related_column_name])) {
										$column_type = $related_columns[$related_column_name][1];
									}
								}
		  
								if ($column_type == DATE) {
									$field_value = $dbe->f($column_name, DATETIME);
									if (is_array($field_value)) {
										$field_value = va_date($date_edit_format, $field_value);
									}
								} else if ($column_type == DATETIME) {
									$field_value = $dbe->f($column_name, DATETIME);
									if (is_array($field_value)) {
										$field_value = va_date($datetime_edit_format, $field_value);
									}
								} else {
									// check if it's a common field and we can get data directly from the record set
									if (isset($db_columns[$column_name]) || ($related_column_name && isset($related_columns[$related_column_name]))) {
										$field_value = $dbe->f($column_name);
									} else {
										// otherwise it's a custom field
										$field_value = get_field_value($column_name);
									}
									if ($apply_translation) {
										$field_value = get_translation($field_value);
									}
								}
							}
						}
						if (isset($selected_related_columns[$column_name]) && $related_number > 1) {
							$row_data[$column_name] .= $related_delimiter_symbol . $field_value;
						} else {
							$row_data[$column_name] = $field_value;
						}
					}
					$prev_id = $row_id;
				} // database rows cycle end
			} // pages cycle end

			if ($record_number > 0) {
				// last row output csv
				$csv_row = "";
				for($i = 0; $i < $columns_selected; $i++) {
					$column_name = $columns[$i];
					$field_value = $row_data[$column_name];
					if ($column_name == "oi_item_properties") {
						$field_value = preg_replace("/^\<br\>/", "", $field_value);
						$field_value = preg_replace("/\<br\>/", "; ", $field_value);
					}
					if ($comma_decimal && in_array($column_name, $prices)) {
						$field_value = str_replace('.', ',', $field_value);
					}
					if(preg_match("/[,;\"\n\r\t\s]/", $field_value)) {
						$field_value = "\"" . str_replace("\"", "\"\"", $field_value) . "\"";
					}
					if($i > 0) {
						$csv_row .= $delimiter_symbol;
					}
					$csv_row .= $field_value;
				}
				if (isset($fp) && $fp) {
					fwrite($fp, $csv_row.$eol); 
					if (isset($fp_copy) && isset($file_path_copy) && strlen($file_path_copy)) {
						fwrite($fp_copy, $csv_row.$eol); 
					}
				} else {
					echo $csv_row.$eol;
				}
				// end last row output
		  
				// update exported status
				if ($table_name == $table_prefix . "orders") {

					$dbs->query("UPDATE " . $table_prefix . "orders SET is_exported=1 WHERE order_id=" . $prev_id);
					// update order status if such option selected
					if (strlen($order_status_update)) {
						update_order_status($prev_id, $order_status_update, true, "", $status_error);
					}
				}
			}

			// END output data

			if ($table == "users") {
				if ($max_id > $exported_user_id) {
					$sql  = " UPDATE " . $table_prefix . "admins SET exported_user_id=" . $dbe->tosql($max_id, INTEGER);
					$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
					$dbe->query($sql);
				}
			} else if ($table == "newsletters_users") {
				if ($max_id > $exported_email_id) {
					$sql  = " UPDATE " . $table_prefix . "admins SET exported_email_id=" . $dbe->tosql($max_id, INTEGER);
					$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
					$dbe->query($sql);
				}
			} else if ($table == "orders") {
				if ($max_id > $exported_order_id) {
					$sql = " UPDATE " . $table_prefix . "admins SET exported_order_id=" . $dbe->tosql($max_id, INTEGER);
					$sql .= " WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
					$dbe->query($sql);
				}
			}

			return;
		}
	}


	if (strlen($errors)) {
		$t->set_var("errors_list", $errors);
		$t->parse("errors", false);
	} 

	if ($template_errors) {
		$t->set_var("errors_list", $template_errors);
		$t->parse("template_errors", false);
	}

	if ($template_success) {
		$t->set_var("success_message", $template_success);
		$t->parse("template_success", false);
	}

	$t->set_var("category_id", htmlspecialchars($category_id));
	$t->set_var("id", htmlspecialchars($id));
	$t->set_var("ids", htmlspecialchars($ids));
	$t->set_var("s_on", htmlspecialchars($s_on));
	$t->set_var("s_ne", htmlspecialchars($s_ne));
	$t->set_var("s_kw", htmlspecialchars($s_kw));
	$t->set_var("s_sd", htmlspecialchars($s_sd));
	$t->set_var("s_ed", htmlspecialchars($s_ed));
	$t->set_var("s_os", htmlspecialchars($s_os));
	$t->set_var("s_ad", htmlspecialchars($s_ad));
	$t->set_var("s_ut", htmlspecialchars($s_ut));
	$t->set_var("s_ap", htmlspecialchars($s_ap));
	$t->set_var("s_ci", htmlspecialchars($s_ci));
	$t->set_var("s_si", htmlspecialchars($s_si));
	$t->set_var("s_ex", htmlspecialchars($s_ex));
	$t->set_var("s_pd", htmlspecialchars($s_pd));
	$t->set_var("s_cct", htmlspecialchars($s_cct));
	
	$t->set_var("s_rn", htmlspecialchars($s_rn));
	$t->set_var("s_ap", htmlspecialchars($s_ap));
	$t->set_var("s_pi", htmlspecialchars($s_pi));
	
	$t->set_var("type", htmlspecialchars($type));
	
	$t->set_var("s", htmlspecialchars($s));
	$t->set_var("sc", htmlspecialchars($sc));
	$t->set_var("sl", htmlspecialchars($sl));
	$t->set_var("ss", htmlspecialchars($ss));
	$t->set_var("ap", htmlspecialchars($ap));

	$t->set_var("rnd", va_timestamp());

	if ($table_name == ($table_prefix . "items") || $table_name == ($table_prefix . "categories")) {
		//$t->parse("products_path", false);
		if ($table_name == ($table_prefix . "items")) {
			$admin_downloadable_export_url = new VA_URL("admin_export.php", false);
			$admin_downloadable_export_url->add_parameter("table", CONSTANT, "items_files");
			$admin_downloadable_export_url->add_parameter("id", REQUEST, "id");
			$admin_downloadable_export_url->add_parameter("ids", REQUEST, "ids");
			$admin_downloadable_export_url->add_parameter("category_id", REQUEST, "category_id");
			$admin_downloadable_export_url->add_parameter("s", REQUEST, "s");
			$admin_downloadable_export_url->add_parameter("sl", REQUEST, "sl");
			$admin_downloadable_export_url->add_parameter("ss", REQUEST, "ss");
			$admin_downloadable_export_url->add_parameter("ap", REQUEST, "ap");
			$t->set_var("admin_downloadable_export_url", $admin_downloadable_export_url->get_url());
			$t->parse("products_other_links", false);
		}
	} else if ($table == "orders") {
		$admin_orders_url = new VA_URL("admin_orders.php", false);
		$admin_orders_url->add_parameter("ids", REQUEST, "ids");
		$admin_orders_url->add_parameter("page", REQUEST, "page");
		$admin_orders_url->add_parameter("s_on", REQUEST, "s_on");
		$admin_orders_url->add_parameter("s_ne", REQUEST, "s_ne");
		$admin_orders_url->add_parameter("s_kw", REQUEST, "s_kw");
		$admin_orders_url->add_parameter("s_sd", REQUEST, "s_sd");
		$admin_orders_url->add_parameter("s_ed", REQUEST, "s_ed");
		$admin_orders_url->add_parameter("s_os", REQUEST, "s_os");
		$admin_orders_url->add_parameter("s_ci", REQUEST, "s_ci");
		$admin_orders_url->add_parameter("s_si", REQUEST, "s_si");
		$admin_orders_url->add_parameter("s_ex", REQUEST, "s_ex");
		$admin_orders_url->add_parameter("s_pd", REQUEST, "s_pd");
		$admin_orders_url->add_parameter("s_cct", REQUEST, "s_cct");
		$admin_orders_url->add_parameter("sort_ord", REQUEST, "sort_ord");
		$admin_orders_url->add_parameter("sort_dir", REQUEST, "sort_dir");

		$t->set_var("admin_orders_url", $admin_orders_url->get_url());

		//$t->parse("orders_path", false);
	}

	$default_columns = "";
	if ($table == "users") {
		$sql  = " SELECT exported_user_fields FROM " . $table_prefix . "admins WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
		$default_columns = get_db_value($sql);
	} else if ($table == "newsletters_users") {
		$sql  = " SELECT exported_user_fields FROM " . $table_prefix . "admins WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
		$default_columns = get_db_value($sql);
		//$t->parse("newsletters_path", false);
	} else if ($table == "orders") {
		$sql  = " SELECT exported_order_fields FROM " . $table_prefix . "admins WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
		$default_columns = get_db_value($sql);
	} else if ($table == "items") {
		$sql  = " SELECT exported_item_fields FROM " . $table_prefix . "admins WHERE admin_id=" . $dbe->tosql(get_session("session_admin_id"), INTEGER);
		$default_columns = get_db_value($sql);
	}
	$checked_columns = explode("|", $default_columns);

	// get default delimiters
	if(strpos($default_columns, "csv_delimiter")) {
		$start_delimiter = strpos($default_columns, "csv_delimiter");
		$end_delimiter = strpos($default_columns, "csv_delimiter", $start_delimiter + 13);
		$csv_delimiter = substr($default_columns, $start_delimiter + 13, $end_delimiter - $start_delimiter - 13);
	}
	if(strpos($default_columns, "related_delimiter")) {
		$start_delimiter = strpos($default_columns, "related_delimiter");
		$end_delimiter = strpos($default_columns, "related_delimiter", $start_delimiter + 17);
		$related_delimiter = substr($default_columns, $start_delimiter + 17, $end_delimiter - $start_delimiter - 17);
	}

	set_options($delimiters, $csv_delimiter, "delimiter");
	set_options($delimiters, $csv_delimiter, "delimiter_bottom");
	set_options($related_delimiters, $related_delimiter, "related_delimiter");
	set_options($related_delimiters, $related_delimiter, "related_delimiter_bottom");


	$t->set_var("table_name", $table_name);

	$template_id = get_param("template_id");
	$sql  = " SELECT template_id, template_name FROM " . $table_prefix . "export_templates ";
	$sql .= " WHERE table_name=" . $dbe->tosql($table, TEXT);
	$export_templates = get_db_values($sql, array(array("", BASIC_EXPORT_MSG)));
	set_options($export_templates, $template_id, "template_id");
	
	$total_columns = 0;
	$export_columns = array();

	if ($template_id) {
		$sql  = " SELECT field_title, field_source FROM " . $table_prefix . "export_fields " ;
		$sql .= " WHERE template_id=" . $dbe->tosql($template_id, INTEGER);
		$sql .= " ORDER BY field_order ";
		$dbe->query($sql);
		while ($dbe->next_record()) {
			$column_title = $dbe->f("field_title");
			$column_source = $dbe->f("field_source");
			$export_columns[] = array("source" => $column_source, "title" => $column_title, "checked" => "checked");
		}
	} else {
		foreach($db_columns as $column_name => $column_info) {
			if ($column_info[2] == RELATED_DB_FIELD) {
				if ($table == "items" && $column_name == "property_name") {
					$sql  = " SELECT property_name FROM " . $table_prefix . "items_properties ";
					$sql .= " WHERE item_type_id=0 ";
					$sql .= " GROUP BY property_name ";
					$dbe->query($sql);
					while ($dbe->next_record()) {
						$property_name = $dbe->f("property_name");
						$column_name   = "item_property_" . $property_name;
						if ($apply_translation) {
							$property_name = get_translation($property_name);
						}
						$column_title  = $property_name;
						$column_checked = in_array($column_title, $checked_columns) ? " checked " : "";
						$export_columns[] = array("source" => $column_name, "title" => $column_title, "checked" => $column_checked);
					}
				} else if ($table == "items" && $column_name == "feature_name") {
					$sql  = " SELECT fg.group_id, fg.group_name, f.feature_name FROM (" . $table_prefix . "features f ";
					$sql .= " INNER JOIN " . $table_prefix . "features_groups fg ON f.group_id=fg.group_id) ";
					$sql .= " GROUP BY fg.group_id, fg.group_name, f.feature_name ";
					$dbe->query($sql);
					while ($dbe->next_record()) {
						$group_id = $dbe->f("group_id");
						$group_name = $dbe->f("group_name");
						$feature_name = $dbe->f("feature_name");
						$column_name   = "item_feature_" . $group_id . "_" . $feature_name;
						if ($apply_translation) {
							$column_title  = get_translation($group_name) . " > " . get_translation($feature_name);
						} else {
							$column_title  = $group_name . " > " . $feature_name;
						}
						$column_checked = in_array($column_title, $checked_columns) ? " checked " : "";
						$export_columns[] = array("source" => $column_name, "title" => $column_title, "checked" => $column_checked);
					}
				} else if ($table == "items" && $column_name == "category_name") {
					$column_title  = $db_columns[$column_name][0];
					$column_checked = in_array($column_title, $checked_columns) ? " checked " : "";
					$export_columns[] = array("source" => "item_category", "title" => $column_title, "checked" => $column_checked);
				} else if ($table == "items" && $column_name == "manufacturer_name") {
					$column_title  = $db_columns[$column_name][0];
					$column_checked = in_array($column_title, $checked_columns) ? " checked " : "";
					$export_columns[] = array("source" => "manufacturer_name", "title" => $column_title, "checked" => $column_checked);
				}
			} else if ($column_info[2] != HIDE_DB_FIELD) {
				$column_title = $column_info[0];
				if ($column_info[2] == CUSTOM_FIELD) {
					$column_source = $column_info[4];
					$column_link = $column_info[5];
				} else {
					$column_link = "";
					$column_source = $column_name;
				}
				$column_checked = in_array($column_title, $checked_columns) ? " checked " : "";
				$export_columns[] = array("source" => $column_source, "title" => $column_title, "checked" => $column_checked, "link" => $column_link);
			}
		}
	}


	// if available some related data
	$total_related = 0;
	if(!$template_id && isset($related_columns)) {
		foreach ($related_columns as $column_name => $column_info) {
			if($column_info[2] != HIDE_DB_FIELD && $column_info[2] != RELATED_DB_FIELD) {
				if ($column_info[2] == CUSTOM_FIELD) {
					$column_title = $column_info[0];
					$column_source = $column_info[4];
					$column_link = $column_info[5];
				} else {
					$column_title = $column_info[0];
					$column_source = $related_table_alias."_".$column_name;
					$column_link = "";
				}
				$column_checked = in_array($column_title, $checked_columns) ? " checked " : "";

				$export_columns[] = array("source" => $column_source, "title" => $column_title, "checked" => $column_checked, "link" => $column_link);

				$total_related++;
			}
		}
	}

	if(isset($related_columns)) {
		$t->parse("related_delimiter_block", false);
		$t->parse("related_delimiter_bottom_block", false);
	}

	foreach($export_columns as $id => $export_column) {
		$field_source = $export_column["source"];
		$column_title = $export_column["title"];
		$column_checked = $export_column["checked"];
		$column_link = isset($export_column["link"]) ? $export_column["link"] : "";
		set_db_column($column_title, $field_source, $column_checked, $column_link);
	}

	if($total_columns % 2 != 0) {
		$t->parse("columns", true);
	}
	$t->set_var("total_columns", $total_columns);
	if (!strlen($template_id)) {
		$t->parse("custom_link", false);
		if(isset($related_columns)) {
			$admin_export_custom_url->remove_parameter("field");
			$admin_export_custom_url->add_parameter("table", CONSTANT, $related_table);
			$t->set_var("admin_export_custom_related_url", $admin_export_custom_url->get_url());
			$t->parse("custom_related", false);
		}
	}

	$t->pparse("main");

	function get_field_value($field_source)
	{
		global $db, $dbe, $db_columns, $related_columns, $related_table_alias, $apply_translation, $date_formats, $date_edit_format, $datetime_edit_format;

		if (preg_match_all("/\{(\w+)\}/i", $field_source, $matches)) {
			$field_value = $field_source;
			for($p = 0; $p < sizeof($matches[1]); $p++) {
				$f_source = $matches[1][$p];
				// get field type
				$column_type = TEXT; $column_name = ""; $column_format = "";
				if (isset($db_columns[$f_source])) {
					$column_type = $db_columns[$f_source][1];
					$column_name = $f_source;
				} else if (isset($related_table_alias) && $related_table_alias && preg_match("/^".$related_table_alias."_/", $f_source)) {
					$related_column_name = preg_replace("/^".$related_table_alias."_/", "", $f_source);
					if (isset($related_columns[$related_column_name])) {
						$column_type = $related_columns[$related_column_name][1];
						$column_name = $f_source;
					}
				} else {
					$date_formats_regexp = implode("|", $date_formats);
					if (preg_match("/".$date_formats_regexp."$/", $f_source, $format_match)) {
						$f_source_wf = preg_replace("/_".$format_match[0]."$/", "", $f_source);
						if (isset($db_columns[$f_source_wf]) && ($db_columns[$f_source_wf][1] == DATE || $db_columns[$f_source_wf][1] == DATETIME)) {
							$column_name = $f_source_wf;
							$column_type = $db_columns[$column_name][1];
							$column_format = $format_match[0];
						}
					}
				}

				if ($column_name) {
					if ($column_type == DATE) {
						$f_source_value = $dbe->f($column_name, DATETIME);
						if (is_array($f_source_value)) {
							if ($column_format) {
								$f_source_value = va_date(array($column_format), $f_source_value);
							} else {
								$f_source_value = va_date($date_edit_format, $f_source_value);
							}
						}
					} else if ($column_type == DATETIME) {
						$f_source_value = $dbe->f($column_name, DATETIME);
						if (is_array($f_source_value)) {
							if ($column_format) {
								$f_source_value = va_date(array($column_format), $f_source_value);
							} else {
								$f_source_value = va_date($datetime_edit_format, $f_source_value);
							}
						}
					} else {
						$f_source_value = $dbe->f($column_name);
						if ($apply_translation) {
							$f_source_value = get_translation($f_source_value);
						}
					}
					$field_value = str_replace("{".$f_source."}", $f_source_value, $field_value);
				}
			}
		} else {
			$field_value = $field_source;
		}

		return $field_value;
	}

	function set_db_column($column_title, $field_source, $column_checked, $column_link = "")
	{
		global $t, $db, $dbe, $total_columns, $table_alias, $checked_columns;

		$total_columns++;
		//$column_checked = in_array($table_alias.".".$column_name, $checked_columns) ? " checked " : "";
		$t->set_var("col", $total_columns);
		$t->set_var("field_source", htmlspecialchars($field_source));
		$t->set_var("column_link", $column_link);
		$t->set_var("column_checked", $column_checked);
		$t->set_var("column_title", htmlspecialchars($column_title));
		$t->parse("rows", true);
		if($total_columns % 2 == 0) {
			$t->parse("columns", true);
			$t->set_var("rows", "");
		}

	}

?>
