<?php

    $default_title = "";

    check_user_security("merchant_orders");

    $user_id = intval(get_session("session_user_id"));

    $stmt = "   select uts.setting_value 
            from va_user_types_settings uts
            inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
            where setting_name = 'user_order_paymant_selection'";

    $userPsAvailable = get_db_value($stmt);
    if(!$userPsAvailable){
        header("Location: " . get_custom_friendly_url("user_home.php"));
        exit;
    }

    $site_url = get_setting_value($settings, "site_url", "");   
    $secure_url = get_setting_value($settings, "secure_url", "");

    $html_template = get_setting_value($block, "html_template", "block_user_payments_system.html"); 
  $t->set_file("block_body", $html_template);

    $operation = get_param("operation");
    $t->set_var('user_payments_href', "user_payments_system.php");
    //save ps
    if($operation == "cancel"){
        header("Location: " . get_custom_friendly_url("user_home.php"));
        exit;
    }
    if($operation == "save"){
        $user_payments = get_param("user_payments");
        $valStr = '';
        if(is_array($user_payments)){
            $valStr = implode(",", $user_payments);
        }

        $db->query('UPDATE va_users SET ps_record = "'. $valStr . '"');
    }


    //show ps
    $userPaymentsSelected = get_db_value("SELECT ps_record FROM va_users WHERE user_id=".$user_id);

    $userPaymentsArray = array_filter(explode(",", $userPaymentsSelected), 'strlen');

    if(count($userPaymentsArray) == 0){
        $t->parse('ps_info_block', false);
    }
    $stmt = "   SELECT payment_id,payment_name 
                FROM va_payment_systems 
                WHERE available_for_user = 1 
                ORDER BY payment_id, payment_name ";

    $db->query($stmt);
    $availablePS = array();
    while($db->next_record()){
        $psId = $db->f("payment_id");
        $availablePS[$psId]["ps_name"] = $db->f("payment_name");
        if(in_array($psId, $userPaymentsArray)){
            $availablePS[$psId]["is_selected"] = 1;
        }
    }
    foreach ($availablePS as $k => $d){
        $t->set_var('ps_name', $d["ps_name"]);
        $t->set_var('ps_id', $k);
        if($d["is_selected"] == 1){
            $t->set_var('ps_selected', 'checked="checked"');
        }
        else{
            $t->set_var('ps_selected', '');
        }
        
        $t->parse('user_ps', true);
    }
  $block_parsed = true;

?>