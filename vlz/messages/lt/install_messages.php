<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// diegimo юinutлs
	define("INSTALL_TITLE", "ViArt parduotuvлs diegimas");

	define("INSTALL_STEP_1_TITLE", "Diegimas: Юingsnis 1");
	define("INSTALL_STEP_1_DESC", "Dлkojame uю ViArt SHOP pasirinkimа. Tam kad baigti рб diegimа, praрome uюpildyti visas reikiamas detales юemiau. Praрome atkreipti dлmesб kad duombazл kuriа renkatлs turi jau bыti sukurta. Jei jыs diegiate б duombazж kuri naudoja ODBC, pvz. MS Access jыs pirmiau turite sukurti DSN jai prieр tжsiant.");
	define("INSTALL_STEP_2_TITLE", "Diegimas: Юingsnis 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Diegimas: Юingsnis 3");
	define("INSTALL_STEP_3_DESC", "Praрome rinktis svetainлs iрdлstymа. Jыs galлsite keisti iрdлstymа vлliau.");
	define("INSTALL_FINAL_TITLE", "Diegimas: Galas");
	define("SELECT_DATE_TITLE", "Rinkitлs datos formatа");

	define("DB_SETTINGS_MSG", "Duombazлs nustatymai");
	define("DB_PROGRESS_MSG", "Duombazлs struktыros uюpildymas tжsiamas");
	define("SELECT_PHP_LIB_MSG", "Rinkitлs PHP bibliotekа");
	define("SELECT_DB_TYPE_MSG", "Rinkitлs duombazлs tipа");
	define("ADMIN_SETTINGS_MSG", "Valdymo nustatymai");
	define("DATE_SETTINGS_MSG", "Datos formatai");
	define("NO_DATE_FORMATS_MSG", "Nлra jokiш datos formatш");
	define("INSTALL_FINISHED_MSG", "Iki рio taрko pagrindinis diegimas baigtas. Praрome tikrai patikrinti nustatymus valdymo skyriuje ir atlikti reikiamus keitimus.");
	define("ACCESS_ADMIN_MSG", "Kad patekti б valdymo skyriш spauskite иia");
	define("ADMIN_URL_MSG", "Valdymo nuoroda");
	define("MANUAL_URL_MSG", "Rankinis URL nuoroda");
	define("THANKS_MSG", "Dлkojame jums uю pasirinktа <b>ViArt parduotuvж</b>.");

	define("DB_TYPE_FIELD", "Duombazлs tipas");
	define("DB_TYPE_DESC", "Praрome rinktis <b>type of database</b> kuriа naudojate. Jei jыs naudojate SQL Serverб arba Microsoft Access, praрome rinktis ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP biblioteka");
	define("DB_HOST_FIELD", "Рeimininko vardas");
	define("DB_HOST_DESC", "Praрome бvesti <b>name</b> ar <b>IP address of the server</b> ant kurio jыsш ViArt duomenш bazл suksis. Jei jыs sukate jыsш duombazж ant savo vietinio PC, tai jыs tikriausiai galite palikti tai kaip yra \"<b>localhost</b>\" ir prievadа tuриiа. Jei jыs naudojate duombazж suteiktа jыsш talpinimo firmos, praрome skaityti jыsш talpinimo firmos dokumentacijа serverio nustatymams.");
	define("DB_PORT_FIELD", "Prievadas");
	define("DB_NAME_FIELD", "Duombazлs vardas / DSN");
	define("DB_NAME_DESC", "Jei jыs naudojate duombazж tokiа kaip MySQL ar PostgreSQL tai praрome бvesti <b>name of the database</b> kur jыs norite kad ViArt sukurtш savo lenteles. Рi duombazл jau turi bыti sukurta. Jei jыs tik diegiate ViArt testavimo tikslams ant savo vietinio PC tai daugiausia sistemш turi \"<b>test</b>\" duombazж kuriа galite naudoti. Jei ne, praрome sukurti duombazж tokiа kaip \"viart\" ir jа naudoti. Jei jыs naudojate Microsoft Access ar SQL Serverб, tai Duombazлs Vardas turi bыti <b>name of the DSN</b> kurб jыs sukыrлte Data Sources (ODBC) skyriuke jыsш Valdymo Skydelyje.");
	define("DB_USER_FIELD", "Vartotojo vardas");
	define("DB_PASS_FIELD", "Slaptaюodis");
	define("DB_USER_PASS_DESC", "<b>Username</b> ir <b>Password</b> - praрome бvesti vartotojo vardа ir slaptaюodб kurб norite naudoti duombazлs priлjimui. Jei jыs naudojate vietinб testavimo diegimа vartotojo vardas tikriausiai yra \"<b>root</b>\" ir slaptaюodis tikriausiai yra tuриias. Tai gerai testavimui, bet uюsiюymлkite, kad tai nлra saugu ant veikianиiш gamybiniш serveriш.");
	define("DB_PERSISTENT_FIELD", "Iрliekantis pasijungimas");
	define("DB_PERSISTENT_DESC", "Tam kad naudoti MySQL iрliekanиius pasijungimus, uюdлktie varnelж рioje dлюutлje. Jei jыs neюinote kas tai yra , tai palikti jа neatюymлtа bus geriausia.");
	define("DB_CREATE_DB_FIELD", "Create DB");
	define("DB_CREATE_DB_DESC", "to create database if possible, tick this box. Works only for MySQL and Postgre");
	define("DB_POPULATE_FIELD", "Uюpildyk DB");
	define("DB_POPULATE_DESC", "Kad sukurti duombazлs lenteliш struktыrа ir uюpildyti jas duomenimis spustelkite юymiш dлюutж");
	define("DB_TEST_DATA_FIELD", "Bandymш duomenys");
	define("DB_TEST_DATA_DESC", "kad pridлti рiek tiek bandymш duomenш б jыsш duombazж paюymлkite varnele");
	define("ADMIN_EMAIL_FIELD", "Valdytojo e-paрtas");
	define("ADMIN_LOGIN_FIELD", "Valdytojo бsijungimas");
	define("ADMIN_PASS_FIELD", "Valdytojo slaptaюodis");
	define("ADMIN_CONF_FIELD", "Patvirtinkite slaptaюodб");
	define("DATETIME_SHOWN_FIELD", "Datos-laiko formatas (rodomas svetainлje)");
	define("DATE_SHOWN_FIELD", "Datos formatas (rodomas svetainлje)");
	define("DATETIME_EDIT_FIELD", "Datos-laiko formatas (redagavimui)");
	define("DATE_EDIT_FIELD", "Datos formatas (redagavimui)");
	define("DATE_FORMAT_COLUMN", "Datos formatas");
	define("CURRENT_DATE_COLUMN", "Dabartinл data");

	define("DB_LIBRARY_ERROR", "PHP funkcijos {db_library} nлra apibrлюtos. Praрome patikrinti duombazлs nustatymus jыsш konfigыracijos byloje - php.ini.");
	define("DB_CONNECT_ERROR", "Negaliu pasijungti prie duombazлs. Praрome tikrinti jыsш duombazлs parametrus.");
	define("INSTALL_FINISHED_ERROR", "Diegimo eiga jau baigлsi.");
	define("WRITE_FILE_ERROR", "Neturiu raрymo teisiш bylai <b>'includes/var_definition.php'</b>. Praрome pakeisti bylos teises prieр tжsiant.");
	define("WRITE_DIR_ERROR", "Neturiu raрymo teisiш aplankui  <b>'includes/'</b>. Praрome pakeisti aplanko teises prieр tжsiant.");
	define("DUMP_FILE_ERROR", "Iрkrovimo byla '{file_name}' nerasta.");
	define("DB_TABLE_ERROR", "Lentelл '{table_name}' nerasta. Praрome uюpildyti duombazж reikalingais duomenimis.");
	define("TEST_DATA_ERROR", "Patikrink <b>{POPULATE_DB_FIELD}</b> prieр uюpildant lenteles su bandymш duomenimis");
	define("DB_HOST_ERROR", "Mazgo vardas kurб nurodлte nerastas.");
	define("DB_PORT_ERROR", "Negaliu pasijungti prie MySQL serverio naudojant nurodytа prievadа.");
	define("DB_USER_PASS_ERROR", "Vartotojo vardas ir slaptaюodis kurб nurodлte neteisingi.");
	define("DB_NAME_ERROR", "Бsijungimo nustatymai buvo teisingi, bet duombazл '{db_name}' nerasta.");

	// atnaujinimo praneрimai
	define("UPGRADE_TITLE", "ViArt Parduotuvлs Naujinimas");
	define("UPGRADE_NOTE", "Pastaba: Praрome pagalvoti apie duombazлs kopijа prieр tжsiant.");
	define("UPGRADE_AVAILABLE_MSG", "Duombazлs naujinimas teikiamas");
	define("UPGRADE_BUTTON", "Naujinti duombazж iki {version_number} dabar");
	define("CURRENT_VERSION_MSG", "Dabar бdiegta versija");
	define("LATEST_VERSION_MSG", "Versija tiekiama diegimui");
	define("UPGRADE_RESULTS_MSG", "Naujinimo pasekmлs");
	define("SQL_SUCCESS_MSG", "SQL uюklausa pavyko");
	define("SQL_FAILED_MSG", "SQL uюklausa nepavyko");
	define("SQL_TOTAL_MSG", "Viso SQL uюklausш бvykdyta");
	define("VERSION_UPGRADED_MSG", "Jыsш duombazл buvo atnaujinta iki");
	define("ALREADY_LATEST_MSG", "Jыs jau turite naujausiа versijа");
	define("DOWNLOAD_NEW_MSG", "Nauja versija buvo aptikta");
	define("DOWNLOAD_NOW_MSG", "Nusisiшskite versijа {version_number} dabar");
	define("DOWNLOAD_FOUND_MSG", "Mes aptikome kad nauja {version_number} versija tiekiama nusisiuntimui. Praрome spausti nuorodа юemiau kad pradлti nusisiuntimа. Po siuntimosi baigimo ir bylш pakeitimo nepamirрkite leisti Naujinimo paprogramж vлl.");
	define("NO_XML_CONNECTION", "Бspлjimas! Nлra ryрio iki 'http://www.viart.com/' prieinama!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>