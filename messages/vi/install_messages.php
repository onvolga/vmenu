<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "Cаi рЈt ViArt SHOP");

	define("INSTALL_STEP_1_TITLE", "Cаi рЈt: BЯѕc 1");
	define("INSTALL_STEP_1_DESC", "Cбm Ѕn bХn рг chчn ViArt SHOP. Р¬ tiЄp tшc cаi рЈt, xin hгy рi«n chi  tiЄt yкu cҐu dЯѕi рвy. Xin lЯu э rўng database cьa bХn phдi t°n tХi trong mбy chь. NЄu bХn chчn cаi рЈt database bўng MS Access vѕi kЄt nЇi ODBC xin hгy tХo DSN рҐu tiкn trЯѕc khi tiЄp tшc quб trмnh nаy.");
	define("INSTALL_STEP_2_TITLE", "Cаi рЈt: BЯѕc 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Cаi рЈt: BЯѕc 3");
	define("INSTALL_STEP_3_DESC", "Xin hгy chчn ki¬u hi¬n thё trang web. BХn sЁ thay р±i nу sau nаy.");
	define("INSTALL_FINAL_TITLE", "Cаi рЈt: Hoаn t¤t");
	define("SELECT_DATE_TITLE", "Chчn рёnh dХng ngаy thбng");

	define("DB_SETTINGS_MSG", "Cбc thiЄt l§p database");
	define("DB_PROGRESS_MSG", "TХo c¤u trъc database");
	define("SELECT_PHP_LIB_MSG", "Chчn thЯ vi®n PHP");
	define("SELECT_DB_TYPE_MSG", "Chчn ki¬u database");
	define("ADMIN_SETTINGS_MSG", "Cбc thiЄt l§p v« quдn trё");
	define("DATE_SETTINGS_MSG", "Cбc рёnh dХng ngаy thбng");
	define("NO_DATE_FORMATS_MSG", "Khфng cу sЗn cбc рёnh dХng ngаy thбng");
	define("INSTALL_FINISHED_MSG", "TХi th¶i рi¬m nаy quб trмnh cаi рЈt cЅ bдn рг hoаn thаnh. Xin vui lтng ki¬m cбc thiЄt l§p · khu vсc quдn trё vа nhжng thay р±i bЎt buµc ");
	define("ACCESS_ADMIN_MSG", "Р¬ truy c§p vаo khu vсc quдn trё chчn · рвy");
	define("ADMIN_URL_MSG", "РЯ¶ng dзn khu vсc quдn trё");
	define("MANUAL_URL_MSG", "Tс vаo рЯ¶ng dзn");
	define("THANKS_MSG", "Cбm Ѕn bХn рг chчn <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "Ki¬u database");
	define("DB_TYPE_DESC", "Xin vui lтng chчn <b>ki¬u database</b> mа bХn sШ dшng. NЄu bХn sШ dшng SQL Server hoЈc MS Access, xin vui lтng chчn ODBC");
	define("DB_PHP_LIB_FIELD", "ThЯ vi®n PHP");
	define("DB_HOST_FIELD", "Hostname");
	define("DB_HOST_DESC", "Xin vui lтng рi«n vаo <b>tкn</b> hoЈc <b>рёa chп IP cьa mбy chь</b> mа database cьa ViArt sЁ chХy. NЄu bХn sШ dшng database trкn mбy chь nµi bµ xin vui lтng р¬ trЇng hoЈc gх <b>localhost</b>. NЄu bХn cаi рЈt trкn mбy chь hosting xin vui lтng xem lХi tаi li®u hoЈc liкn h® vѕi bµ ph§n hІ trю dёch vш hosting.");
	define("DB_PORT_FIELD", "C±ng");
	define("DB_NAME_FIELD", "Tкn database/DSN");
	define("DB_NAME_DESC", "NЄu bХn sШ dшng database MySQL hoЈc PostgreSQL xin vui lтng gх vаo <b>tкn database</b>. Database nаy phдi t°n tХi sЗn. NЄu bХn cаi рЈt ViArt SHOP vѕi mшc рнch test trкn mбy nµi bµ bХn cу th¬ рi«n vаo <b>test</b>. NЄu khфng bХn cу th¬ gх vаo <b>Viart</b> vа sШ dшng nу. NЄu bХn sШ dшng MS Access hoЈc SQL Server xin hгy рi«n vаo <b>tкn DSN</b> mа bХn thiЄt l§p trong kЄt nЇi ODBC · khu vсc Control Panel.");
	define("DB_USER_FIELD", "Tкn truy c§p");
	define("DB_PASS_FIELD", "M§t mг");
	define("DB_USER_PASS_DESC", "<b>Tкn truy c§p<b> vа <b>M§t mг</b> - Xin vui lтng рi«n vаo tкn truy c§p vа m§t mг mа bХn sШ dшng р¬ truy c§p рЯюc database. NЄu bХn dщng vѕi mшc рнch р¬ test xin vui lтng gх vаo \"<b>root</b>\" vа m§t mг р¬ trЇng. Xin lЯu э рЧng dщng nу cho mшc рнch рЯa lкn Internet vм nу khфng an toаn v« bдo m§t.");
	define("DB_PERSISTENT_FIELD", "Persistent Connection");
	define("DB_PERSISTENT_DESC", "р¬ sШ dшng ki¬u kЄt nЇi persistent vѕi MySQL vа PostgresSQL, chчn · ф nаy. NЄu bХn khфng chЎc chЎn, xin hгy bц qua.");
	define("DB_CREATE_DB_FIELD", "TХo DB");
	define("DB_CREATE_DB_DESC", "р¬ tХo database, nЄu рЯюc xin chчn ф nаy. ChСc nеng chп hoХt рµng vѕi ki¬u dж li®u MySQL vа Postgre");
	define("DB_POPULATE_FIELD", "РЯa vаo dж li®u mзu");
	define("DB_POPULATE_DESC", "Р¬ tХo c¤u trъc database xin chчn vаo ф nаy");
	define("DB_TEST_DATA_FIELD", "Ki¬m tra dж li®u");
	define("DB_TEST_DATA_DESC", "р¬ thкm vаo mµt vаi dж li®u р¬ ki¬m tra xin chчn ф nаy");
	define("ADMIN_EMAIL_FIELD", "Email ngЯ¶i quдn trё");
	define("ADMIN_LOGIN_FIELD", "Tкn truy c§p admin");
	define("ADMIN_PASS_FIELD", "M§t mг cьa admin");
	define("ADMIN_CONF_FIELD", "Xбc nh§n lХi m§t mг");
	define("DATETIME_SHOWN_FIELD", "Рёnh dХng gi¶ (trкn trang web)");
	define("DATE_SHOWN_FIELD", "Рёnh dХng ngаy thбng (trкn trang web)");
	define("DATETIME_EDIT_FIELD", "Рёnh dХng gi¶ (trкn trang quдn trё)");
	define("DATE_EDIT_FIELD", "Рёnh dХng ngаy thбng (trкn trang quдn trё)");
	define("DATE_FORMAT_COLUMN", "Рёnh dХng ngаy");
	define("CURRENT_DATE_COLUMN", "Ngаy hi®n tХi");

	define("DB_LIBRARY_ERROR", "Cбc hаm chСc nеng PHP cho {db_library} khфng рЯюc рёnh nghоa. Xin vui lтng ki¬m tra lХi thiЄt l§p database trong t®p c¤u hмnh php.ini.");
	define("DB_CONNECT_ERROR", "Khфng th¬ kЄt nЇi database. Xin vui lтng ki¬m tra tham sЇ database.");
	define("INSTALL_FINISHED_ERROR", "Quб trмnh cаi рЈt рг hoаn t¤t.");
	define("WRITE_FILE_ERROR", "Khфng cу quy«n ghi t®p dж li®u <b>'includes/var_definition.php'</b>. Xin vui lтng thay р±i quy«n truy c§p trЯѕc khi thсc hi®n bЯѕc nаy.");
	define("WRITE_DIR_ERROR", "Khфng cу quy«n ghi thЯ mшc <b>'includes/'</b>. Xin vui lтng thay р±i quy«n cho thЯ mшc trЯѕc khi tiЄp tшc.");
	define("DUMP_FILE_ERROR", "T®p '{file_name}' khфng tмm th¤y.");
	define("DB_TABLE_ERROR", "Table '{table_name}' khфng tмm th¤y. Xin vui lтng tХo database vѕi dж li®u cҐn thiЄt.");
	define("TEST_DATA_ERROR", "Ki¬m tra <b>{POPULATE_DB_FIELD}</b> trЯѕc khi tХo bдng table khi ki¬m tra dж li®u");
	define("DB_HOST_ERROR", "Hostname mа bХn mф tд khфng th¬ tмm th¤y.");
	define("DB_PORT_ERROR", "Khфng th¬ kЄt nЇi mбy chь database vѕi c±ng hi®n tХi.");
	define("DB_USER_PASS_ERROR", "Tкn truy c§p vа m§t mг bХn mф tд khфng ръng.");
	define("DB_NAME_ERROR", "Cбc thiЄt l§p реng nh§p lа ръng, nhЯng database '{db_name}' khфng th¬ tмm th¤y.");

	// upgrade messages
	define("UPGRADE_TITLE", "Nвng c¤p ViArt SHOP");
	define("UPGRADE_NOTE", "LЯu э: Xin hгy lЯu э rўng chЎc chЎn backup database trЯѕc khi tiЄp tшc.");
	define("UPGRADE_AVAILABLE_MSG", "Рг sЗn sаng c§p nh§t database");
	define("UPGRADE_BUTTON", "C§p nh§t database рЄn {version_number} ngay");
	define("CURRENT_VERSION_MSG", "Phiкn bдn hi®n tХi рг cаi рЈt");
	define("LATEST_VERSION_MSG", "Phiкn bдn mѕi р¬ cаi рЈt");
	define("UPGRADE_RESULTS_MSG", "KЄt quд nвng c¤p");
	define("SQL_SUCCESS_MSG", "Truy v¤n SQL thаnh cфng");
	define("SQL_FAILED_MSG", "Truy v¤n SQL bё lІi");
	define("SQL_TOTAL_MSG", "T¤t cд truy v¤n SQL thi hаnh");
	define("VERSION_UPGRADED_MSG", "Database cьa bХn рЯюc nвng c¤p рЄn");
	define("ALREADY_LATEST_MSG", "BХn рг c§p nh§t phiкn bдn mѕi nh¤t");
	define("DOWNLOAD_NEW_MSG", "Phiкn bдn mѕi рЯюc ki¬m tra");
	define("DOWNLOAD_NOW_MSG", "Tдi xuЇng phiкn bдn {version_number} ngay.");
	define("DOWNLOAD_FOUND_MSG", "Chъng tфi рг ki¬m tra phiкn bдn {version_number} рг sЗn sаng cho vi®c tдi xuЇng. Nh¤n chчn рЯ¶ng link dЯѕi рвy р¬ tдi xuЇng. Sau khi hoаn t¤t tдi xuЇng vа thay thЄ t®p dж li®u, nhѕ рЧng quкn chХy bдn c§p nh§t tr· lХi.");
	define("NO_XML_CONNECTION", "Cдnh bбo! Khфng cу kЄt nЇisЗn рЄn рёa chп 'http://www.viart.com/'!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>