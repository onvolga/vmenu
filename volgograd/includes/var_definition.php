<?php

	define("INSTALLED", true); // set to false if you want run install.php
	define("DEBUG",     true); // debug mode - set false on live site

	// database parameters
	$db_lib        = "mysql"; // mysql | postgre | odbc
	$db_type       = "mysql"; // mysql | postgre | access | db2
	$db_name       = "vmenu";
	$db_host       = "localhost";
	$db_port       = "";
	$db_user       = "root";
	$db_password   = "";
	$db_persistent = false;

	$table_prefix  = "va_";

	$default_language = "ru";

	// date parameters
	$datetime_show_format = array("DD", ".", "MM", ".", "YY", " ", "H", ":", "mm");
	$date_show_format     = array("DD", ".", "MM", ".", "YYYY");
	$datetime_edit_format = array("DD", ".", "MM", ".", "YYYY", " ", "HH", ":", "mm");
	$date_edit_format     = array("DD", ".", "MM", ".", "YYYY");

	// session settings
	$session_prefix = "navi1";

	// if you use multi-site functionality uncomment the following line and specify appropriate id
	$site_id = 2;

	// if you use VAT validation uncomment the following line
	//$vat_validation = true;
	// array of country codes for which VAT check is obligatory
	//$vat_obligatory_countries = array("GB");
	// array of country codes for which remote VAT check won't be run
	//$vat_remote_exception_countries = array("NL");

?>