<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// mensajes de soporte
	define("SUPPORT_TITLE", "Centro de Soporte");
	define("SUPPORT_REQUEST_INF_TITLE", "Pedir informaciуn");
	define("SUPPORT_REPLY_FORM_TITLE", "Responder");

	define("MY_SUPPORT_ISSUES_MSG", "Mis peticiones de ayuda");
	define("MY_SUPPORT_ISSUES_DESC", "Si tiene algъn problema con el producto que ha comprado, nuestros administradores estarбn dispuestos a resolverlo. Siйntase libre de contactarnos pinchando el enlace de arriba y escribiendo su peticiуn de ayuda. ");
	define("NEW_SUPPORT_REQUEST_MSG", "Nueva peticiуn");
	define("SUPPORT_REQUEST_ADDED_MSG", "Gracias<br>Nuestro aquipo de ayuda tratarб de atender su peticiуn lo mбs pronto posible.");
	define("SUPPORT_SELECT_DEP_MSG", "Seleccionar departamento");
	define("SUPPORT_SELECT_PROD_MSG", "Seleccionar producto");
	define("SUPPORT_SELECT_STATUS_MSG", "Seleccionar estado");
	define("SUPPORT_NOT_VIEWED_MSG", "No visto");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Visto por el cliente");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Visto por el administrador");
	define("SUPPORT_STATUS_NEW_MSG", "Nuevo");
	define("NO_SUPPORT_REQUEST_MSG", "No se encontraron peticiones");

	define("SUPPORT_SUMMARY_COLUMN", "Resumen");
	define("SUPPORT_TYPE_COLUMN", "Tipo");
	define("SUPPORT_UPDATED_COLUMN", "Ъltima Actualizaciуn ");

	define("SUPPORT_USER_NAME_FIELD", "Su nombre");
	define("SUPPORT_USER_EMAIL_FIELD", "Su correo electrуnico");
	define("SUPPORT_IDENTIFIER_FIELD", "Identificaciуn (Factura #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Entorno (Sistema Operativo, Base de datos, Servidor de Web, ets.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Departamento");
	define("SUPPORT_PRODUCT_FIELD", "Producto");
	define("SUPPORT_TYPE_FIELD", "Tipo");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Condiciуn Actual");
	define("SUPPORT_SUMMARY_FIELD", "Resumen en-linea");
	define("SUPPORT_DESCRIPTION_FIELD", "Descripciуn");
	define("SUPPORT_MESSAGE_FIELD", "Mensaje");
	define("SUPPORT_ADDED_FIELD", "Aсadido");
	define("SUPPORT_ADDED_BY_FIELD", "Aсadido por");
	define("SUPPORT_UPDATED_FIELD", "Ъltima actualizaciуn");

	define("SUPPORT_REQUEST_BUTTON", "Enviar Peticiуn");
	define("SUPPORT_REPLY_BUTTON", "Responder");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Falta el parametro de <b>Identificador de ayuda</b>");
	define("SUPPORT_MISS_CODE_ERROR", "Falta el paramento de <b> verificaciуn</b>");
	define("SUPPORT_WRONG_ID_ERROR", "El parametro de <b>Identificador de ayuda</b> tiene un valor errуneo");
	define("SUPPORT_WRONG_CODE_ERROR", "El parametro de <b>verificaciуn</b> tiene un valor errуneo");

	define("MAIL_DATA_MSG", "Datos de Email");
	define("HEADERS_MSG", "Cabeceras");
	define("ORIGINAL_TEXT_MSG", "Texto Original");
	define("ORIGINAL_HTML_MSG", "HTML Original");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>