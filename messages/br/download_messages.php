<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "ParРІmetro(s) de download errado(s)");
	define("DOWNLOAD_MISS_PARAM", "ParРІmetro(s) de download ausente(s)");
	define("DOWNLOAD_INACTIVE", "Download inativo");
	define("DOWNLOAD_EXPIRED", "Seu periodo de download expirou.");
	define("DOWNLOAD_LIMITED", "VocРє excedeu o nСЉmero mР±ximo de downloads");
	define("DOWNLOAD_PATH_ERROR", "O caminho para o produto nРіo pС„de ser encontrado");
	define("DOWNLOAD_RELEASE_ERROR", "VersРіo nРіo encontrada");
	define("DOWNLOAD_USER_ERROR", "Somente usuР°rios registrados podem baixar este arquivo");
	define("ACTIVATION_OPTIONS_MSG", "OpР·С…es de ativaР·Ріo");
	define("ACTIVATION_MAX_NUMBER_MSG", "NСЉmero mР±ximo de ativaР·С…es");
	define("DOWNLOAD_OPTIONS_MSG", "OpР·С…es de produto a baixar / software");
	define("DOWNLOADABLE_MSG", "Produto a baixar (software)");
	define("DOWNLOADABLE_DESC", "Para produtos a baixar, vocРє pode especificar tambР№m \"Periodo de download\", \"Caminho para o arquivo a baixar\" e \"OpР·С…es de ativaР·Ріo\"");
	define("DOWNLOAD_PERIOD_MSG", "Periodo de download");
	define("DOWNLOAD_PATH_MSG", "Caminho para o arquivo a baixar");
	define("DOWNLOAD_PATH_DESC", "vocРє pode adicionar vР±rios caminhos divididos por ponto-e-vРЅrgula");
	define("UPLOAD_SELECT_MSG", "Selecione a arquivo para upload e clique em {button_name}.");
	define("UPLOADED_FILE_MSG", "O arquivo <b>{filename}</b> foi transferido.");
	define("UPLOAD_SELECT_ERROR", "Por favor, selecione primeiro o arquivo.");
	define("UPLOAD_IMAGE_ERROR", "Somente arquivos de imagens sРіo permitidos.");
	define("UPLOAD_FORMAT_ERROR", "Esse tipo de arquivo nРіo Р№ autorizado.");
	define("UPLOAD_SIZE_ERROR", "Arquivos maiores do que {filesize} nРіo sРіo permitidos.");
	define("UPLOAD_DIMENSION_ERROR", "Arquivos maiores do que {dimension} nРіo sРіo permitidos.");
	define("UPLOAD_CREATE_ERROR", "O sistema nРіo pode criar o arquivo.");
	define("UPLOAD_ACCESS_ERROR", "VocРє nРіo possui permissС…es para fazer upload de arquivos");
	define("DELETE_FILE_CONFIRM_MSG", "VocРє tem certeza que quer deletar esse arquivo?");
	define("NO_FILES_MSG", "Nenhum arquivo foi encontrado");
	define("SERIAL_GENERATE_MSG", "Gerar nСЉmero de sР№rie");
	define("SERIAL_DONT_GENERATE_MSG", "NРіo gerar");
	define("SERIAL_RANDOM_GENERATE_MSG", "gerar nСЉmero de sР№rie aleatСѓrio para produto de software");
	define("SERIAL_FROM_PREDEFINED_MSG", "obter nСЉmero de sР№rie a partir de uma lista predefinida");
	define("SERIAL_PREDEFINED_MSG", "NСЉmeros de sР№rie predefinidos");
	define("SERIAL_NUMBER_COLUMN", "NСЉmero de sР№rie");
	define("SERIAL_USED_COLUMN", "Utilizado");
	define("SERIAL_DELETE_COLUMN", "Deletar");
	define("SERIAL_MORE_MSG", "Adicionar mais nСЉmeros de sР№rie?");
	define("SERIAL_PERIOD_MSG", "Periodo de nСЉmero de sР№rie");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Exibir Termos e CondiР·С…es");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Para baixar o produto, o usuР±rio deve ler e concordar com nosso termos e condiР·С…es");
	define("DOWNLOAD_TERMS_MSG", "Termos e CondiР·С…es");
	define("DOWNLOAD_TERMS_USER_DESC", "Li e concordo com os termos e condiР·С…es");
	define("DOWNLOAD_TERMS_USER_ERROR", "Para baixar o produto, vocРє deve ler e concordar com nosso termos e condiР·С…es");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>