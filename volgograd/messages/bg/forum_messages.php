<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//съобщения свързани с форума
	define("FORUM_TITLE", "Форум");
	define("TOPIC_INFO_TITLE", "Информация за темата");
	define("TOPIC_MESSAGE_TITLE", "Съобщение");

	define("MY_FORUM_TOPICS_MSG", "Моите теми");
	define("ALL_FORUM_TOPICS_MSG", "Всички теми");
	define("MY_FORUM_TOPICS_DESC", "Установили ли сте дали някой друг има проблем, с който вие сте се сблъскали? Бихте ли искали да споделите знанията и опитът си с други потребители? Защо не станете потребител на форума и да се присъедините към общността?");
	define("NEW_TOPIC_MSG", "Нова тема");
	define("NO_TOPICS_MSG", "Не са намерени теми");
	define("FOUND_TOPICS_MSG", "Намерихме <b>{found_records}</b> съвпадащи с '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Не са намерени форуми");

	define("FORUM_NAME_COLUMN", "Форум");
	define("FORUM_TOPICS_COLUMN", "Теми");
	define("FORUM_REPLIES_COLUMN", "Отговори");
	define("FORUM_LAST_POST_COLUMN", "Последно обновен");
	define("FORUM_MODERATORS_MSG", "Модератори");

	define("TOPIC_NAME_COLUMN", "Тема");
	define("TOPIC_AUTHOR_COLUMN", "Автор");
	define("TOPIC_VIEWS_COLUMN", "Прегледи");
	define("TOPIC_REPLIES_COLUMN", "Отговори");
	define("TOPIC_UPDATED_COLUMN", "Последно обновен");
	define("TOPIC_ADDED_MSG", "Благодаря<br>Вашата тема беше добавена");

	define("TOPIC_ADDED_BY_FIELD", "Добавено от");
	define("TOPIC_ADDED_DATE_FIELD", "Добавено");
	define("TOPIC_UPDATED_FIELD", "Последно обновено");
	define("TOPIC_NICKNAME_FIELD", "Прякор");
	define("TOPIC_EMAIL_FIELD", "Вашият e-mail адрес");
	define("TOPIC_NAME_FIELD", "Тема");
	define("TOPIC_MESSAGE_FIELD", "Съобщение");
	define("TOPIC_NOTIFY_FIELD", "Изпрати всички отговори на моя e-mail");

	define("ADD_TOPIC_BUTTON", "Добави тема");
	define("TOPIC_MESSAGE_BUTTON", "Добави съобщение");

	define("TOPIC_MISS_ID_ERROR", "Липсващ <b>Thread ID</b> параметър.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> параметърът има грешна стойност.");
	define("FORUM_SEARCH_MESSAGE", "Намерихме {search_count} съобщения съответстващи на критерия '{search_string}'.");
	define("TOPIC_PREVIEW_BUTTON", "Преглед");
	define("TOPIC_SAVE_BUTTON", "Запис");

	define("LAST_POST_ON_SHORT_MSG", "На:");
	define("LAST_POST_IN_SHORT_MSG", "В:");
	define("LAST_POST_BY_SHORT_MSG", "До:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Последно обновен:");

?>