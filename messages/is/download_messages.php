<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "RГ¶ng/rangar niГ°urhalsbreyta/ur.");
	define("DOWNLOAD_MISS_PARAM", "Vantar niГ°urhalsbreytu/r");
	define("DOWNLOAD_INACTIVE", "NiГ°urhal liggur niГ°ri");
	define("DOWNLOAD_EXPIRED", "NiГ°urhalstГ­mabil Гѕitt er runniГ° ГЎ enda.");
	define("DOWNLOAD_LIMITED", "ГћГє ert kominn framyfir hГЎmarksfjГ¶lda niГ°urhala.");
	define("DOWNLOAD_PATH_ERROR", "SlГіГ° aГ° vГ¶ru finnst ekki.");
	define("DOWNLOAD_RELEASE_ERROR", "ГљtgГЎfa fannst ekki.");
	define("DOWNLOAD_USER_ERROR", "AГ°eins skrГЎГ°ir notendur geta niГ°urhalaГ° Гѕessari skrГЎ.");
	define("ACTIVATION_OPTIONS_MSG", "Stillingar fyrir gangsetningu");
	define("ACTIVATION_MAX_NUMBER_MSG", "HГЎmarksfjГ¶ldi gangsetninga");
	define("DOWNLOAD_OPTIONS_MSG", "NiГ°urhalanlegt / HugbГєnaГ°arstillingar");
	define("DOWNLOADABLE_MSG", "NiГ°urhalanlegt (hugbГєnaГ°ur)");
	define("DOWNLOADABLE_DESC", "Fyrir niГ°urhalanlega vГ¶ru geturГ°u lГ­ka tilgreint ААћNiГ°urhalstГ­mabilААњ, ААћLeiГ° aГ° niГ°urhalanlegri skrГЎААњ og ААћgangsetningarstillingarААњ.");
	define("DOWNLOAD_PERIOD_MSG", "NiГ°urhalstГ­mabil");
	define("DOWNLOAD_PATH_MSG", "SlГіГ° aГ° niГ°urhalanlegri skrГЎ");
	define("DOWNLOAD_PATH_DESC", "ГѕГє getur bГ¦tt inn fleiri slГіГ°um og afmarkaГ° ГѕГ¦r meГ° semikommu");
	define("UPLOAD_SELECT_MSG", "Veldu skrГЎ til aГ° hlaГ°a upp/senda inn og smelltu ГЎ {button_name} hnappinn.");
	define("UPLOADED_FILE_MSG", "SkrГЎnni <b>{filename}</b> hefur veriГ° upphlaГ°iГ°.");
	define("UPLOAD_SELECT_ERROR", "Vinsamlegast veldu fyrst skrГЎ");
	define("UPLOAD_IMAGE_ERROR", "AГ°eins myndaskrГЎr eru leyfГ°ar");
	define("UPLOAD_FORMAT_ERROR", "Гћessi skrГЎartegund er ekki leyfГ°");
	define("UPLOAD_SIZE_ERROR", "SkrГЎr stГ¦rri en {filesize} eru ekki leyfГ°ar.");
	define("UPLOAD_DIMENSION_ERROR", "Myndir stГ¦rri en {dimension} eru ekki leyfГ°ar.");
	define("UPLOAD_CREATE_ERROR", "KerfiГ° getur ekki bГєiГ° til skrГЎnna.");
	define("UPLOAD_ACCESS_ERROR", "ГћГє hefur ekki rГ©ttindi til aГ° hlaГ°a upp skrГЎm");
	define("DELETE_FILE_CONFIRM_MSG", "Ertu viss um aГ° ГѕГє viljir eyГ°a Гѕessari skrГЎ?");
	define("NO_FILES_MSG", "Engar skrГЎr fundust");
	define("SERIAL_GENERATE_MSG", "ГљtbГєa raГ°nГєmer");
	define("SERIAL_DONT_GENERATE_MSG", "ekki ГєtbГєa");
	define("SERIAL_RANDOM_GENERATE_MSG", "ГєtbГєa slembiraГ°nГєmer fyrir hugbГєnaГ°");
	define("SERIAL_FROM_PREDEFINED_MSG", "sГ¦kja raГ°nГєmer Г­ fyrirfram tilgreindan lista");
	define("SERIAL_PREDEFINED_MSG", "Fyrirfram tilgreind raГ°nГєmer");
	define("SERIAL_NUMBER_COLUMN", "RaГ°nГєmer");
	define("SERIAL_USED_COLUMN", "NotaГ°");
	define("SERIAL_DELETE_COLUMN", "EyГ°a");
	define("SERIAL_MORE_MSG", "BГ¦ta viГ° fleiri raГ°nГєmerum?");
	define("SERIAL_PERIOD_MSG", "TГ­mabil raГ°nГєmer");
	define("DOWNLOAD_SHOW_TERMS_MSG", "SГЅna skilmГЎla");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Til aГ° niГ°urhala vГ¶runni Гѕarf notandi aГ° hafa lesiГ° og samГѕykkt skilmГЎla okkar");
	define("DOWNLOAD_TERMS_MSG", "SkilmГЎlar");
	define("DOWNLOAD_TERMS_USER_DESC", "Г‰g hef lesiГ° og samГѕykkt skilmГЎla ykkar");
	define("DOWNLOAD_TERMS_USER_ERROR", "Til aГ° niГ°urhala vГ¶runni Гѕarf ГѕГє aГ° hafa lesiГ° og samГѕykkt skilmГЎla okkar");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>