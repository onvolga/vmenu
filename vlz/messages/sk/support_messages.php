<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Centrum podpory");
	define("SUPPORT_REQUEST_INF_TITLE", "Ћiadosќ o informбciu");
	define("SUPPORT_REPLY_FORM_TITLE", "Odpovedaќ");

	define("MY_SUPPORT_ISSUES_MSG", "Moje ћiadosti o podporu");
	define("MY_SUPPORT_ISSUES_DESC", "Ak mбte akйkoѕvek problйmy s produktami, ktorй ste u nбs zakъpili, nбљ tнm podpory zбkaznнkom je pripravenэ Vбm pomфcќ. Pre zadanie poћiadavky staин kliknъќ na hornъ linku");
	define("NEW_SUPPORT_REQUEST_MSG", "Novб poћiadavka");
	define("SUPPORT_REQUEST_ADDED_MSG", "Пakujeme<br>Nбљ tнm podpory sa pokъsi pomфcќ s Vaљou poћiadavkou tak rэchlo, ako to len bude moћnй.");
	define("SUPPORT_SELECT_DEP_MSG", "Zvoliќ oddelenie");
	define("SUPPORT_SELECT_PROD_MSG", "Zvoliќ produkt");
	define("SUPPORT_SELECT_STATUS_MSG", "Zvoliќ stav");
	define("SUPPORT_NOT_VIEWED_MSG", "Neprezretй");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Prezretй zбkaznнkom");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Prezretй administrбtorom");
	define("SUPPORT_STATUS_NEW_MSG", "Novб");
	define("NO_SUPPORT_REQUEST_MSG", "Ћiadne dotazy nenбjdenй");

	define("SUPPORT_SUMMARY_COLUMN", "Sumбr");
	define("SUPPORT_TYPE_COLUMN", "Typ");
	define("SUPPORT_UPDATED_COLUMN", "Naposledy aktualizovanй");

	define("SUPPORT_USER_NAME_FIELD", "Vaљe meno");
	define("SUPPORT_USER_EMAIL_FIELD", "Vaљa emailovб adresa");
	define("SUPPORT_IDENTIFIER_FIELD", "Identifikбtor (инslo faktъry)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Prostredie (OS, Databбza, Web Server, atп.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Oddelenie");
	define("SUPPORT_PRODUCT_FIELD", "Produkt");
	define("SUPPORT_TYPE_FIELD", "Typ");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Momentбlny status");
	define("SUPPORT_SUMMARY_FIELD", "Jednoriadkovэ sumбr");
	define("SUPPORT_DESCRIPTION_FIELD", "Popis");
	define("SUPPORT_MESSAGE_FIELD", "Sprбva");
	define("SUPPORT_ADDED_FIELD", "Pridanэ");
	define("SUPPORT_ADDED_BY_FIELD", "Pridal");
	define("SUPPORT_UPDATED_FIELD", "Naposledy aktualizovanй");

	define("SUPPORT_REQUEST_BUTTON", "Odoslaќ poћiadavku");
	define("SUPPORT_REPLY_BUTTON", "Odpovedaќ");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Chэba <b>Podpora ID</b> parameter");
	define("SUPPORT_MISS_CODE_ERROR", "Chэba <b>kontrolnэ</b> parameter");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Podpora ID</b> parameter mб nesprбvnu hodnotu");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Kontrolnэ</b> parameter mб nesprбvnu hodnotu");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>