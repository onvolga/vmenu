<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Fel nerladdningsparameter(s).");
	define("DOWNLOAD_MISS_PARAM", "Saknade nerladdningsparameter(s).");
	define("DOWNLOAD_INACTIVE", "Nerladdning inaktiv.");
	define("DOWNLOAD_EXPIRED", "Din nerladdningtid har gеtt ut.");
	define("DOWNLOAD_LIMITED", "Du har gеtt цver antalet tillеtna nerladdningar.");
	define("DOWNLOAD_PATH_ERROR", "Sцkvдg till produkten kan inte hittas.");
	define("DOWNLOAD_RELEASE_ERROR", "Utgеvan hittades inte.");
	define("DOWNLOAD_USER_ERROR", "Endast registrerade anvдndare kan ladda ner denna fil.");
	define("ACTIVATION_OPTIONS_MSG", "Aktiveringsval");
	define("ACTIVATION_MAX_NUMBER_MSG", "Hцgst antal aktiveringar");
	define("DOWNLOAD_OPTIONS_MSG", "Nedladdningsbart / mjukvaruval");
	define("DOWNLOADABLE_MSG", "Nedladdningsbart (mjukvara)");
	define("DOWNLOADABLE_DESC", "Fцr nedladdningsbar produkt kan du ocksе specificera 'nedladdningsperiod', 'Sцkvдg till nedladdningsbar fil' och 'Aktiveringsval'");
	define("DOWNLOAD_PERIOD_MSG", "Nedladdningsperiod");
	define("DOWNLOAD_PATH_MSG", "Sцkvдg till nedladdningsbar fil");
	define("DOWNLOAD_PATH_DESC", "Du kan lдgga till flera sцkvдgar genom att еtskilja dem med semikolon.");
	define("UPLOAD_SELECT_MSG", "Vдlj fil att ladda upp och klicka pе  {button_name}-knappen.");
	define("UPLOADED_FILE_MSG", "Filen <b>{filename}</b> har laddats upp.");
	define("UPLOAD_SELECT_ERROR", "Var vдnlig vдlj en fil fцrst.");
	define("UPLOAD_IMAGE_ERROR", "Endast bilder дr tillеtna.");
	define("UPLOAD_FORMAT_ERROR", "Den hдr filtypen дr inte tillеten.");
	define("UPLOAD_SIZE_ERROR", "Filer stцrre дn {filesize} дr inte tillеtna.");
	define("UPLOAD_DIMENSION_ERROR", "Bilder stцrre дn {dimension} дr inte tillеtna.");
	define("UPLOAD_CREATE_ERROR", "Systemet kan inte skapa filen.");
	define("UPLOAD_ACCESS_ERROR", "Du har inte tillеtelse att ladda upp filer.");
	define("DELETE_FILE_CONFIRM_MSG", "Дr du sдker pе att du vill radera den hдr filen?");
	define("NO_FILES_MSG", "Inga filer hittades.");
	define("SERIAL_GENERATE_MSG", "Skapa serienummer.");
	define("SERIAL_DONT_GENERATE_MSG", "Skapa inte");
	define("SERIAL_RANDOM_GENERATE_MSG", "Skapa slumpat serienummer fцr mjukvaruprodukt");
	define("SERIAL_FROM_PREDEFINED_MSG", "Hдmta serienummer frеn fцrdefinierad lista.");
	define("SERIAL_PREDEFINED_MSG", "Fцrdefinierade serienummer");
	define("SERIAL_NUMBER_COLUMN", "Serienummer");
	define("SERIAL_USED_COLUMN", "Anvдnd");
	define("SERIAL_DELETE_COLUMN", "Raderad");
	define("SERIAL_MORE_MSG", "Lдgg till fler serienummer?");
	define("SERIAL_PERIOD_MSG", "Serienummerperiod");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Visa allmдnna villkor");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Fцr att kunna ladda ner produkten mеste kunden godkдnna vеra allmдnna villkor.");
	define("DOWNLOAD_TERMS_MSG", "Allmдnna villkor");
	define("DOWNLOAD_TERMS_USER_DESC", "Jag har lдst och godkдnner de allmдnna villkoren.");
	define("DOWNLOAD_TERMS_USER_ERROR", "Fцr att kunna ladda ner produkten mеste du godkдnna vеra allmдnna villkor.");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>