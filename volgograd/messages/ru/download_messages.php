<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.10RE                                               ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viartsoft.ru                                         ***
  ***                                                                      ***
  ****************************************************************************
*/

	// сообщения загрузки
	define("DOWNLOAD_WRONG_PARAM", "Неверно указаны параметры для скачивания");
	define("DOWNLOAD_MISS_PARAM", "Отсутствие параметра (ов) загрузки.");
	define("DOWNLOAD_INACTIVE", "Загрузка неактивна");
	define("DOWNLOAD_EXPIRED", "Ваш период загрузки истек.");
	define("DOWNLOAD_LIMITED", "Вы превысили максимальное число загрузок.");
	define("DOWNLOAD_PATH_ERROR", "Путь к файлу не найден.");
	define("DOWNLOAD_RELEASE_ERROR", "Выпуск не найден.");
	define("DOWNLOAD_USER_ERROR", "Только зарегистрированные пользователи могут загрузить этот файл.");
	define("ACTIVATION_OPTIONS_MSG", "Опции активации");
	define("ACTIVATION_MAX_NUMBER_MSG", "Максимальное кол-во активаций");
	define("DOWNLOAD_OPTIONS_MSG", "Опции цифровых товаров");
	define("DOWNLOADABLE_MSG", "Цифровые товары");
	define("DOWNLOADABLE_DESC", "для цифровых товаров можно выбрать 'Период загрузки', 'Путь к цифровому товару' и 'Опции активации'");
	define("DOWNLOAD_PERIOD_MSG", "Период<br> загрузки");
	define("DOWNLOAD_PATH_MSG", "Путь к<br> цифровому товару");
	define("DOWNLOAD_PATH_DESC", "Вы можете добавить несколько путей, разделив их точкой с запятой");
	define("UPLOAD_SELECT_MSG", "Выберите файл, чтобы загрузить и нажмите кнопку {button_name}.");
	define("UPLOADED_FILE_MSG", "Файл <b> {filename} </b> загружен.");
	define("UPLOAD_SELECT_ERROR", "Пожалуйста  сначала выберите файл.");
	define("UPLOAD_IMAGE_ERROR", "Разрешены только файлы изображений.");
	define("UPLOAD_FORMAT_ERROR", "Этот тип файлов недопустим");
	define("UPLOAD_SIZE_ERROR", "Файлы больше чем {filesize} не разрешены.");
	define("UPLOAD_DIMENSION_ERROR", "Изображения большие чем {dimension} не разрешены.");
	define("UPLOAD_CREATE_ERROR", "Система не может создать файл.");
	define("UPLOAD_ACCESS_ERROR", "У вас нет прав на загрузку файлов");
	define("DELETE_FILE_CONFIRM_MSG", "Действительно удалить этот файл?");
	define("NO_FILES_MSG", "Файлы не найдены");
	define("SERIAL_GENERATE_MSG", "Серийный номер");
	define("SERIAL_DONT_GENERATE_MSG", "не генерировать");
	define("SERIAL_RANDOM_GENERATE_MSG", "генерировать случайный серийный номер");
	define("SERIAL_FROM_PREDEFINED_MSG", "использовать серийный номер из подготовленного списка");
	define("SERIAL_PREDEFINED_MSG", "Подготовленные Серийные номера");
	define("SERIAL_NUMBER_COLUMN", "Серийный номер");
	define("SERIAL_USED_COLUMN", "Использованный");
	define("SERIAL_DELETE_COLUMN", "Удалить");
	define("SERIAL_MORE_MSG", "Добавить серийные номера?");
	define("SERIAL_PERIOD_MSG", "Период серийного номера");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Показать Условия и Соглашения");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Для скачивания товара пользователю необходимо<br> прочитать и подтвердить Условия и Соглашения");
	define("DOWNLOAD_TERMS_MSG", "Условия и Соглашения");
	define("DOWNLOAD_TERMS_USER_DESC", "Условия и Соглашения мною прочитаны и принимаются");
	define("DOWNLOAD_TERMS_USER_ERROR", "Для скачивания товара Вам необходимо прочитать и подтвердить Условия и Соглашения");

	define("DOWNLOAD_TITLE_MSG", "Название<br> загрузки");
	define("DOWNLOADABLE_FILES_MSG", "Загрузка файлов");
	define("DOWNLOAD_INTERVAL_MSG", "Интервал загрузки");
	define("DOWNLOAD_LIMIT_MSG", "Лимит<br> загрузки");
	define("DOWNLOAD_LIMIT_DESC", "сколько раз можно загрузить файл");
	define("MAXIMUM_DOWNLOADS_MSG", "Максимум загрузок");
	define("PREVIEW_TYPE_MSG", "Тип<br> предпросмотра");
	define("PREVIEW_TITLE_MSG", "Заголовок<br> предпросмотра");
	define("PREVIEW_PATH_MSG", "Файл<br> предпросмотра");
	define("PREVIEW_IMAGE_MSG", "Изображение<br> для предпросмотра");
	define("MORE_FILES_MSG", "больше файлов");
	define("UPLOAD_MSG", "Загрузить");
	define("USE_WITH_OPTIONS_MSG", "Использовать только с опциями");
	define("PREVIEW_AS_DOWNLOAD_MSG", "превью перед скачиванием");
	define("PREVIEW_USE_PLAYER_MSG", "Использовать плеер");
	define("PROD_PREVIEWS_MSG", "Документация");

?>
