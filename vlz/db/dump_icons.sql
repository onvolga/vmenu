
DROP TABLE IF EXISTS `va_blz_items_icons`;
CREATE TABLE `va_blz_items_icons`(
 `icon_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
 `icon_path` VARCHAR(1024),
 `icon_order` INT(11),
 `icon_name` VARCHAR(255)
  ,PRIMARY KEY (icon_id)
  ,KEY icon_order (icon_order)
);



DROP TABLE IF EXISTS `va_blz_items_icons_assigned`;
CREATE TABLE `va_blz_items_icons_assigned`(
 `item_id` INT(11),
 `icon_id` INT(11)
  ,KEY item_id (item_id)
  ,KEY icon_id (icon_id)
);