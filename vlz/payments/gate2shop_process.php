<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.8                                                ***
  ***      File:  gate2shop_process.php                                    ***
  ***      Built: Tue Sep 13 18:30:31 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

/*
 * Gate2Shop (www.g2s.com) handler by ViArt Ltd (http://www.viart.com/)
 */

	$is_admin_path = true;
	$root_folder_path = "../";

	include_once ($root_folder_path ."includes/common.php");
	include_once ($root_folder_path ."includes/order_items.php");
	include_once ($root_folder_path ."includes/parameters.php");
	include_once ($root_folder_path ."messages/".$language_code."/cart_messages.php");

	$vc = get_session("session_vc");
	$order_id = get_session("session_order_id");

	$order_errors = check_order($order_id, $vc);
	if($order_errors) {
		echo $order_errors;
		exit;
	}
	
	$payment_parameters = array();
	$pass_parameters = array();
	$post_parameters = '';
	$pass_data = array();
	$variables = array();
	get_payment_parameters($order_id, $payment_parameters, $pass_parameters, $post_parameters, $pass_data, $variables);
	
	$pass_data = array();
	foreach ($payment_parameters as $parameter_name => $parameter_value) {
		if (isset($pass_parameters[$parameter_name]) && $pass_parameters[$parameter_name] == 1) {
			if($parameter_name == 'time_stamp' || strtolower($parameter_name) == 'time_stamp'){
				$parameter_value = va_date(array("YYYY","-","MM","-","DD",".","HH",":","mm",":","ss"),$parameter_value);
			}
			$pass_data[$parameter_name] = $parameter_value;
			if(isset($pass_data[strtolower($parameter_name)])){
				unset($pass_data[strtolower($parameter_name)]);
				$pass_data[$parameter_name] = $parameter_value;
			}
		}
	}

	$total_tax_with_shipping = $payment_parameters['total_amount'];
	$checksum = $payment_parameters['secret'];
	$checksum.= $payment_parameters['merchant_id'];
	$checksum.= $payment_parameters['currency'];
	$checksum.= $payment_parameters['total_amount'];
	$count = 0;
	foreach ($variables['items'] as $items_index => $items_array) {
		$count++;
		$pass_data["item_name_".$count] = $items_array["item_name"];
		$checksum.= $items_array["item_name"];
		$pass_data["item_amount_".$count] = $items_array["price_incl_tax"];
		$checksum.= $items_array["price_incl_tax"];
		$pass_data["item_number_".$count] = $items_array["item_id"];
		$pass_data["quantity_".$count] = $items_array["quantity"];
		$checksum.= $items_array["quantity"];
	}
	if (isset($variables["properties"])){
		foreach ($variables["properties"] as $number => $property) {
			$count++;
			$pass_data["item_name_".$count] = $property['property_name'];
			$checksum.= $property['property_name'];
			$pass_data["item_amount_".$count] = $property['property_price_incl_tax'];
			$checksum.= $property['property_price_incl_tax'];
			$pass_data["item_number_".$count] = $count;
			$pass_data["quantity_".$count] = 1;
			$checksum.= 1;
		}
	}
	if (isset($variables["total_discount_incl_tax"]) && $variables["total_discount_incl_tax"] != 0) {
		$count++;
		$pass_data["item_name_".$count] = TOTAL_DISCOUNT_MSG;
		$checksum.= TOTAL_DISCOUNT_MSG;
		$pass_data["item_amount_".$count] = 0;
		$checksum.= 0;
		$pass_data["discount_".$count] = $variables["total_discount_incl_tax"];
		$pass_data["item_number_".$count] = $count;
		$pass_data["quantity_".$count] = 1;
		$checksum.= 1;
	}
	if (isset($variables["processing_fee"]) && $variables["processing_fee"] != 0) {
		$count++;
		$pass_data["item_name_".$count] = PROCESSING_FEE_MSG;
		$checksum.= PROCESSING_FEE_MSG;
		$pass_data["item_amount_".$count] = $variables['processing_fee'];
		$checksum.= $variables['processing_fee'];
		$pass_data["item_number_".$count] = $count;
		$pass_data["quantity_".$count] = 1;
		$checksum.= 1;
	}
	if (isset($variables["shipping_type_desc"]) && $variables["shipping_type_desc"]) {
		$count++;
		$pass_data["item_name_".$count] = $variables["shipping_type_desc"];
		$checksum.= $variables["shipping_type_desc"];
		$pass_data["item_amount_".$count] = $variables["shipping_cost_incl_tax"];
		$checksum.= $variables["shipping_cost_incl_tax"];
		$pass_data["item_number_".$count] = $count;
		$pass_data["quantity_".$count] = 1;
		$checksum.= 1;
	}

	$checksum.= $pass_data['time_stamp'];
	$checksum = md5($checksum);
	$pass_data["numberofitems"] = $count;
	$pass_data["checksum"] = $checksum;

	$post_parameters = '';
	foreach ($pass_data as $param_name => $param_value) {
		if ($post_parameters) { $post_parameters .= "&"; }
		$post_parameters .= urlencode($param_name) . "=" . urlencode($param_value);
	}

	$payment_url = $payment_parameters['payment_url'] . "?" . $post_parameters;

	header("Location: " . $payment_url);
	exit;
?>