<?php

    $default_title = "";

    check_user_security("merchant_orders");

    $user_id = intval(get_session("session_user_id"));

    $stmt = "   select uts.setting_value 
                from va_user_types_settings uts
                inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
                where setting_name = 'user_shipping_general_selection'";

    $selectionSsAvailable = get_db_value($stmt);

    $stmt = "   select uts.setting_value 
                from va_user_types_settings uts
                inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
                where setting_name = 'user_shipping_individual'";

    $userSsAvailable = get_db_value($stmt);
    if(!$selectionSsAvailable){
        header("Location: " . get_custom_friendly_url("user_home.php"));
        exit;
    }

    $site_url = get_setting_value($settings, "site_url", "");   
    $secure_url = get_setting_value($settings, "secure_url", "");

    $html_template = get_setting_value($block, "html_template", "block_user_shipping_systems.html"); 
  $t->set_file("block_body", $html_template);

    $operation = get_param("operation");

    $t->set_var('user_shipping_href', "user_shipping_systems.php");
    //save ps
    if($operation == "cancel"){
        header("Location: " . get_custom_friendly_url("user_home.php"));
        exit;
    }
    if($operation == "save"){
        $user_shippings = get_param("user_shippings");
        $per_order = get_param("per_order");
        $per_kg = get_param("per_kg");
        $valStr = '';
        if(is_array($user_shippings)){
            $valStr = implode(",", $user_shippings);
        }

        $db->query('UPDATE va_users SET ss_record = "'. $valStr . '"');
        $db->query('UPDATE va_users SET per_order = "'. $per_order . '"');
        $db->query('UPDATE va_users SET per_kg = "'. $per_kg . '"');
    }

    //show shipping
    $userShippingsSelected = get_db_value("SELECT ss_record FROM va_users WHERE user_id=".$user_id);
    $per_order = get_db_value("SELECT per_order FROM va_users WHERE user_id=".$user_id);
    $per_kg = get_db_value("SELECT per_kg FROM va_users WHERE user_id=".$user_id);
    $t->set_var('per_order', $per_order);
    $t->set_var('per_kg', $per_kg);
    $userShippingsSelectedArray = array_filter(explode(",", $userShippingsSelected), 'strlen');

    if(count($userShippingsSelectedArray) == 0){
        $t->parse('ss_info_block', false);
    }
    $stmt = "   SELECT shipping_module_id,shipping_module_name 
                FROM va_shipping_modules 
                WHERE (available_for_user = 1 AND user_added_id = ".$user_id.") OR (available_for_user = 1 AND user_added_id IS NULL) 
                ORDER BY shipping_module_id, shipping_module_name ";

    $db->query($stmt);
    $availableSS = array();

    while($db->next_record()){
        $ssId = $db->f("shipping_module_id");
        $availableSS[$ssId]["ss_name"] = $db->f("shipping_module_name");
        if(in_array($ssId, $userShippingsSelectedArray)){
            $availableSS[$ssId]["is_selected"] = 1;
        }
    }
    foreach ($availableSS as $k => $d){
        $t->set_var('ss_name', $d["ss_name"]);
        $t->set_var('ss_id', $k);
        if($d["is_selected"] == 1){
            $t->set_var('ss_selected', 'checked="checked"');
        }
        else{
            $t->set_var('ss_selected', '');
        }
        
        $t->parse('user_ss', true);
    }
    if($userSsAvailable){

        $s = new VA_Sorter($settings["templates_dir"], "sorter_img.html", "user_shipping_systems.php");
        $s->set_sorter(ID_MSG, "sorter_shipping_module_id", "1", "shipping_module_id");
        $s->set_sorter("Название модуля", "sorter_shipping_module_name", "2", "shipping_module_name");
        $s->set_sorter(ACTIVE_MSG, "sorter_is_active", "3", "is_active");
        $n = new VA_Navigator($settings["templates_dir"], "navigator.html", "user_shipping_systems.php");

        // set up variables for navigator
        $db->query("SELECT COUNT(*) FROM " . $table_prefix . "shipping_modules WHERE available_for_user = 1 AND user_added_id = ".$user_id);
        $db->next_record();
        $total_records = $db->f(0);
        $records_per_page = get_param("q") > 0 ? get_param("q") : 25;
        $pages_number = 5;
        $page_number = $n->set_navigator("navigator", "page", SIMPLE, $pages_number, $records_per_page, $total_records, false);

        $db->RecordsPerPage = $records_per_page;
        $db->PageNumber = $page_number;
        $sql  = " SELECT shipping_module_id, shipping_module_name, is_active, user_added_id ";
        $sql .= " FROM " . $table_prefix . "shipping_modules WHERE available_for_user = 1 AND user_added_id = ".$user_id. " " . $s->order_by;
        $db->query($sql);
        if ($db->next_record())
        {

            $t->parse("sorters", false);
            $t->set_var("no_records", "");
            do {
                $is_active = ($db->f("is_active") == 1) ? "<b>" . YES_MSG . "</b>" : NO_MSG;
                $t->set_var("is_active", $is_active);
                $user_added_id = $db->f("user_added_id");
                $t->set_var("shipping_module_id", $db->f("shipping_module_id"));
                $t->set_var("shipping_module_name", $db->f("shipping_module_name"));

                if($user_added_id && $user_added_id == $user_id){
                    $t->parse("edit_link", false);
                    $t->parse("edit_types_link", false);
                }
                else{
                    $t->set_var("edit_link", false);
                    $t->set_var("edit_types_link", false);
                }

                $t->parse("records", true);
            } while ($db->next_record());
        }
        else
        {
            $t->set_var("sorters", "");
            $t->set_var("records", "");
            $t->set_var("navigator", "");
            $t->parse("no_records", false);
        }

        $t->parse("available_systems_block", false);
        $t->parse("button_add", false);
    }
    else{
        $t->set_var("available_systems_block", false);
    }

    $block_parsed = true;

?>