<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Centro de Suporte");
	define("SUPPORT_REQUEST_INF_TITLE", "Solicitar informaР·С…es");
	define("SUPPORT_REPLY_FORM_TITLE", "Responder");

	define("MY_SUPPORT_ISSUES_MSG", "Minhas solicitaР·С…es de suporte");
	define("MY_SUPPORT_ISSUES_DESC", "Se vocРє tiver problemas com algum produto comprado conosco, nossa equipe de suporte esta a disposiР·Ріo para ajudР°-lo. Clique no link acima para solicitar suporte.");
	define("NEW_SUPPORT_REQUEST_MSG", "Nova solicitaР·Ріo");
	define("SUPPORT_REQUEST_ADDED_MSG", "Obrigado<br>Nossa equipe de suporte entrarР± em contato em atР№ 1 dia СЉtil.");
	define("SUPPORT_SELECT_DEP_MSG", "Escolher departamento");
	define("SUPPORT_SELECT_PROD_MSG", "Selecionar produto");
	define("SUPPORT_SELECT_STATUS_MSG", "Selecionar situaР·Ріo");
	define("SUPPORT_NOT_VIEWED_MSG", "NРіo Visualizado");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Visualizado pelo usuР±rio");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Visualizado pelo administrador");
	define("SUPPORT_STATUS_NEW_MSG", "Novo");
	define("NO_SUPPORT_REQUEST_MSG", "Nenhuma solicitaР·Ріo de suporte foi encontrada");

	define("SUPPORT_SUMMARY_COLUMN", "SumР°rio");
	define("SUPPORT_TYPE_COLUMN", "Tipo");
	define("SUPPORT_UPDATED_COLUMN", "Ultima atualizaР·Ріo");

	define("SUPPORT_USER_NAME_FIELD", "Seu nome");
	define("SUPPORT_USER_EMAIL_FIELD", "Seu email");
	define("SUPPORT_IDENTIFIER_FIELD", "Identificador (NС” da fatura)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Ambiente (OS, Banco de dados, Web Server, etc.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Departamento");
	define("SUPPORT_PRODUCT_FIELD", "Produto");
	define("SUPPORT_TYPE_FIELD", "Tipo");
	define("SUPPORT_CURRENT_STATUS_FIELD", "SituaР·Ріo atual");
	define("SUPPORT_SUMMARY_FIELD", "SumР°rio online");
	define("SUPPORT_DESCRIPTION_FIELD", "DescriР·Ріo");
	define("SUPPORT_MESSAGE_FIELD", "Mensagem");
	define("SUPPORT_ADDED_FIELD", "Adicionado");
	define("SUPPORT_ADDED_BY_FIELD", "Adicionado por");
	define("SUPPORT_UPDATED_FIELD", "Ultima atualizaР·Ріo");

	define("SUPPORT_REQUEST_BUTTON", "Submeter solicitaР·Ріo");
	define("SUPPORT_REPLY_BUTTON", "Responder");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "ParРІmeto  de <b>Identificador de suporte </b> faltando");
	define("SUPPORT_MISS_CODE_ERROR", "ParРІmeto  de <b>VerificaР·Ріo</b> faltando");
	define("SUPPORT_WRONG_ID_ERROR", "O parРІmeto <b>Identificador de suporte </b> possui um valor incorreto");
	define("SUPPORT_WRONG_CODE_ERROR", "O parРІmeto <b>VerificaР·Ріo </b> possui um valor incorreto");

	define("MAIL_DATA_MSG", "Dados de email");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Texto original");
	define("ORIGINAL_HTML_MSG", "HTML original");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>