<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "ViArt SHOP Installation");

	define("INSTALL_STEP_1_TITLE", "Installation: Trin 1");
	define("INSTALL_STEP_1_DESC", "Tak, fordi du valgte ViArt SHOP. For at fortsГ¦tte installationen, skal du udfylde de Гёnskede oplysninger nedenfor. BemГ¦rk, at den database, du vГ¦lger allerede bГёr eksistere. Hvis du installerer til en database, der bruger ODBC, f.eks MS Access skal du fГёrst oprette en DSN for det, fГёr du fortsГ¦tter.");
	define("INSTALL_STEP_2_TITLE", "Installation: Trin 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Installation: Trin 3");
	define("INSTALL_STEP_3_DESC", "VГ¦lg et site layout. Du vil vГ¦re i stand til at Г¦ndre layoutet bagefter.");
	define("INSTALL_FINAL_TITLE", "Installation: Afslutning");
	define("SELECT_DATE_TITLE", "VГ¦lg dato format");

	define("DB_SETTINGS_MSG", "Database indstillinger");
	define("DB_PROGRESS_MSG", "Vises i database struktur fremskridt");
	define("SELECT_PHP_LIB_MSG", "VГ¦lg PHP Bibliotek");
	define("SELECT_DB_TYPE_MSG", "VГ¦lg database type");
	define("ADMIN_SETTINGS_MSG", "Administrative indstillinger");
	define("DATE_SETTINGS_MSG", "Dato formater");
	define("NO_DATE_FORMATS_MSG", "Ingen datoformater til rГҐdighed");
	define("INSTALL_FINISHED_MSG", "PГҐ dette tidspunkt er den grundlГ¦ggende installation fГ¦rdig. SГёrg for at kontrollere indstillingerne i administrationsdelen, og foretag de nГёdvendige Г¦ndringer.");
	define("ACCESS_ADMIN_MSG", "klik her for at fГҐ adgang til administrationsdelen");
	define("ADMIN_URL_MSG", "Administration URL");
	define("MANUAL_URL_MSG", "Manual URL");
	define("THANKS_MSG", "Mange tak, fordi du valgte <b> ViArt SHOP </ b>.");

	define("DB_TYPE_FIELD", "Database type");
	define("DB_TYPE_DESC", "VГ¦lg venligst den <b> type database </ b>, du bruger. Hvis du bruger SQL Server eller Microsoft Access, kan du vГ¦lge ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP Bibliotek");
	define("DB_HOST_FIELD", "VГ¦rtsnavn");
	define("DB_HOST_DESC", "Angiv <b> navn </ b> eller <b> IP-adressen pГҐ den server </ b>, hvor din ViArt database vil kГёre. Hvis du kГёrer din database pГҐ dit lokale pc sГҐ kan du sikkert bare overlade dette som \"<b> localhost </ b>\" og porten tomt. Hvis du bruger en database fra din hosting selskab, kan du se din hosting virksomhedens dokumentation for serverindstillinger.");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "Database Navn / DSN");
	define("DB_NAME_DESC", "Hvis du bruger en database som MySQL eller PostgreSQL venligst indtast <b> databasens navn </ b>, hvor du Гёnsker ViArt at skabe sin tabeller. Denne database skal eksistere allerede.Hvis du kun installerer ViArt til testformГҐl pГҐ din lokale PC har de fleste systemer \"<b> test </ b>\" database, du kan bruge. Hvis ikke, kan du oprette en database som \"viart\" og bruge den. Hvis du bruger Microsoft Access eller SQL Server derefter databasenavnet bГёr <b> navnet pГҐ DSN </ b>, at du har oprettet i Datakilder (ODBC) del af dit Kontrolpanel.");
	define("DB_USER_FIELD", "Brugernavn");
	define("DB_PASS_FIELD", "Password");
	define("DB_USER_PASS_DESC", "<b> Brugernavn </ b> og <b> Password </ b> - angiv brugernavn og adgangskode, du vil bruge til at fГҐ adgang til databasen. Hvis du bruger en lokal test installation brugernavnet er sandsynligvis \"<b> root </ b>\" og password er sandsynligvis tomt. This is fine for testing, but please note that this is not secure on production servers. Det er godt nok til afprГёvning, men vГ¦r opmГ¦rksom pГҐ, at dette ikke er sikker pГҐ produktionen servere.");
	define("DB_PERSISTENT_FIELD", "Persistent Connection");
	define("DB_PERSISTENT_DESC", "to use MySQL or Postgre persistent connections, tick this box. If you do not know what it means, then leaving it unticked is probably best.");
	define("DB_CREATE_DB_FIELD", "Opret DB");
	define("DB_CREATE_DB_DESC", "til at oprette databasen, hvis det er muligt, sГ¦ttes kryds i denne boks. Virker kun for MySQL og Postgre");
	define("DB_POPULATE_FIELD", "Populate DB");
	define("DB_POPULATE_DESC", "for at oprette database tabel strukturen og udfylde den med data afkryds afkrydsningsfeltet");
	define("DB_TEST_DATA_FIELD", "Test data");
	define("DB_TEST_DATA_DESC", "For at tilfГёje data til din database, sГ¦t kryds i checkboksen");
	define("ADMIN_EMAIL_FIELD", "Administrator Email");
	define("ADMIN_LOGIN_FIELD", "Administrator Login");
	define("ADMIN_PASS_FIELD", "Administrator Password");
	define("ADMIN_CONF_FIELD", "BekrГ¦ft Password");
	define("DATETIME_SHOWN_FIELD", "Tidspunkt (vises pГҐ sitet)");
	define("DATE_SHOWN_FIELD", "Dato (vises pГҐ sitet)");
	define("DATETIME_EDIT_FIELD", "Tidspunkt (til redigering)");
	define("DATE_EDIT_FIELD", "Dato (til redigering)");
	define("DATE_FORMAT_COLUMN", "Dato");
	define("CURRENT_DATE_COLUMN", "Aktuel dato");

	define("DB_LIBRARY_ERROR", "PHP funktioner for {db_library} er ikke defineret. Kontroller din database indstillinger i din opsГ¦tningsfil - php.ini.");
	define("DB_CONNECT_ERROR", "Kan ikke opnГҐ forbindelse til database. Tjek databasens parametre");
	define("INSTALL_FINISHED_ERROR", "Installationen allerede fГ¦rdig.");
	define("WRITE_FILE_ERROR", "Du har ikke skrivestilladelse til filen <b> 'includes / var_definition.php' </ b>. Г†ndre venligst filrettigheder, fГёr du fortsГ¦tter.");
	define("WRITE_DIR_ERROR", "Du har ikke skrivestilladelse til mappen <b> 'includes /' </ b>. Г†ndre venligst mappetilladelser fГёr du fortsГ¦tter.");
	define("DUMP_FILE_ERROR", "Dump file '{file_name}' wasn't found.");
	define("DB_TABLE_ERROR", "Table '{table_name}' wasn't found. Please populate the database with the necessary data.");
	define("TEST_DATA_ERROR", "Check <b>{POPULATE_DB_FIELD}</b> before populating tables with test data");
	define("DB_HOST_ERROR", "Angivet vГ¦rtsnavn blev ikke fundet.");
	define("DB_PORT_ERROR", "Den angivne port kunne ikke opnГҐ forbindelse til database.");
	define("DB_USER_PASS_ERROR", "Det angivne brugernavn eller password er ikke rigtigt.");
	define("DB_NAME_ERROR", "Login indstillinger er korrekte, men databasen ");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt SHOP opgradering");
	define("UPGRADE_NOTE", "Note: Overvej at kГёre backup af database fГёr du gГҐr videre.");
	define("UPGRADE_AVAILABLE_MSG", "Opgradering af database tilgГ¦ngelig.");
	define("UPGRADE_BUTTON", "Opgrader database til {version_number}");
	define("CURRENT_VERSION_MSG", "Aktuelt installeret version");
	define("LATEST_VERSION_MSG", "Version tilgГ¦ngelig for installering");
	define("UPGRADE_RESULTS_MSG", "Opgrader resultater");
	define("SQL_SUCCESS_MSG", "SQL spГёrgsmГҐl lykkedes");
	define("SQL_FAILED_MSG", "SQL spГёrgsmГҐl mislykkedes");
	define("SQL_TOTAL_MSG", "Total SQL spГёrgsmГҐl gennemfГёrt");
	define("VERSION_UPGRADED_MSG", "Din database er blevet opgraderet til");
	define("ALREADY_LATEST_MSG", "Du har allerede den seneste version");
	define("DOWNLOAD_NEW_MSG", "Den nye version er fundet.");
	define("DOWNLOAD_NOW_MSG", "Download version {version_number}");
	define("DOWNLOAD_FOUND_MSG", "We have detected that the new {version_number} version is available to download. Please click the link below to start downloading. After completing the download and replacing the files don't forget to run Upgrade routine again.");
	define("NO_XML_CONNECTION", "Advarsel! Ingen forbindelse til 'http://www.viart.com/'");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>