<?php

	$default_title = "{custom_title}";

	$block_number = $vars["block_key"];
	
	$css_class = get_setting_value($vars, "cb_css_class", ""); // old parameter used in previous verison [DELETE]
	$user_type = get_setting_value($vars, "cb_user_type", "");
	$admin_type = get_setting_value($vars, "cb_admin_type", "");
	$params = get_setting_value($vars, "cb_params", "");

	$user_check = true;
	if (strlen($user_type)) {
		if (strtoupper($user_type) == "NON") {
			if (strlen(get_session("session_user_id"))) {
				$user_check = false;
			}
		} else if (strtoupper($user_type) == "ANY") {
			if (!strlen(get_session("session_user_id"))) {
				$user_check = false;
			}
		} else if (strtoupper($user_type) != "ALL") {
			if ($user_type != get_session("session_user_type_id")) {
				$user_check = false;
			}
		}
	}

	if (!$user_check && !strlen($admin_type)) {
		return;
	}

	$admin_check = true;
	if (strlen($admin_type)) {
		if (strtoupper($admin_type) == "ANY") {
			if (!strlen(get_session("session_admin_id"))) {
				$admin_check = false;
			}
		} else {
			if ($admin_type != get_session("session_admin_privilege_id")) {
				$admin_check = false;
			}
		}
	}

	if (!$admin_check && (!$user_check || !strlen($user_type))) {
		return;
	}


	if (strlen($params)) {
		$pairs = explode(";", $params);
		for ($i = 0; $i < sizeof($pairs); $i++) {
			$pair = explode("=", trim($pairs[$i]), 2);
			if (sizeof($pair) == 2) {
				list($param_name, $param_value) = $pair;
				if ($param_name == "category" || $param_name == "category_id") {
					$current_value = get_param("category_id");
					if (!strlen($current_value)) {
						$current_value = "0";
					}
				} else if ($param_name == "item" || $param_name == "product" || $param_name == "product_id") {
					$current_value = get_param("item_id");
				} else if ($param_name == "user" || $param_name == "user_id") {
					$current_value = get_session("session_user_id");
				} else {
					$current_value = get_param($param_name);
				}
				$param_values = explode(",", $param_value);
				if (!in_array($current_value, $param_values)) {
					return;
				}
			}
		}
	}


  $sql  = " SELECT block_title, block_path, block_desc FROM " . $table_prefix . "custom_blocks ";
  $sql .= " WHERE block_id=" . intval($block_number);
	$db->query($sql);
	if($db->next_record()) {
		$custom_title = get_translation($db->f("block_title"));
		$custom_title = get_currency_message($custom_title, $currency);
		$block_path = $db->f("block_path");
		$custom_body = "";
		if ($block_path) {
			$file_path = $block_path;
			if (!preg_match("/^http(s)?:\/\//", $file_path) && !file_exists($file_path)) {
				// check default dir for file
				if ($is_admin_path) {
					$file_path = "../templates/user/" . $block_path;
				} else {
					$file_path = "./templates/user/" . $block_path;
				}
				$is_file_exists = file_exists($file_path);
				if (!$is_file_exists) {
					$file_path = $block_path;
				}
			}
			$custom_body = join("", file($file_path));
		} else {
			$custom_body = get_translation($db->f("block_desc"));
		}
		$custom_body = get_translation($custom_body);
		$custom_body = get_currency_message($custom_body, $currency);
		if (get_setting_value($settings, "php_in_custom_blocks", 0)) {
			eval_php_code($custom_body);
		}
	} else {
		return;
	}
	
	if(!strlen($custom_body) && !strlen($custom_title)) {
		return;
	}
	if(strlen($custom_title)) {
		if (!$css_class) { $css_class = "block-custom"; }
		$html_template = get_setting_value($block, "html_template", "block_custom.html"); 
	  $t->set_file("block_body", $html_template);
	} else {
		if (!$css_class) { $css_class = "block-simple"; }
		$html_template = get_setting_value($block, "html_template", "block_simple.html"); 
	  $t->set_file("block_body", $html_template);
	}

	$t->set_var("css_class", $css_class);
	$t->set_block("custom_title", $custom_title);
	$t->parse("custom_title", false);
	$t->set_block("custom_body", $custom_body);
	$t->parse("custom_body", false);

	$block_parsed = true;

?>