<?php
	$language_code = get_language("messages.php");
	include_once("../messages/".$language_code."/cart_messages.php");

	if (!isset($va_type) || !$va_type) {
		$va_type = defined("VA_TYPE") ? strtolower(VA_TYPE) : "standard";
	}
	if (!isset($va_name) || !$va_name) {
		$va_name = defined("VA_PRODUCT") ? strtolower(VA_PRODUCT) : "shop";
	}
	$va_version_code = va_version_code();
	$permissions = get_permissions();

	// check permission
	$orders_permission = get_setting_value($permissions, "sales_orders", 0);
	$tickets_permission = get_setting_value($permissions, "support", 0);
	$site_users_permission = get_setting_value($permissions, "site_users", 0);

	// Admin Site URL settings
	$admin_folder     = get_admin_dir();
	$site_url         = get_setting_value($settings, "site_url", "");
	$secure_url       = get_setting_value($settings, "secure_url", "");
	$admin_site_url   = $site_url . $admin_folder;
	$admin_secure_url = $secure_url . $admin_folder;

	// SSL settings
	$ssl_admin_tickets  = get_setting_value($settings, "ssl_admin_tickets", 0);
	$ssl_admin_ticket   = get_setting_value($settings, "ssl_admin_ticket", 0);
	$ssl_admin_helpdesk = get_setting_value($settings, "ssl_admin_helpdesk", 0);
	if ($ssl_admin_tickets && strlen($secure_url)) {
		$tickets_site_url = $admin_secure_url;
	} else {
		$tickets_site_url = $admin_site_url;
	}
	if ($ssl_admin_ticket && strlen($secure_url)) {
		$ticket_site_url = $admin_secure_url;
	} else {
		$ticket_site_url = $admin_site_url;
	}
	if ($ssl_admin_helpdesk && strlen($secure_url)) {
		$helpdesk_site_url = $admin_secure_url;
	} else {
		$helpdesk_site_url = $admin_site_url;
	}

	// orders SSL settings
	$ssl_admin_orders_list = get_setting_value($settings, "ssl_admin_orders_list", 0);
	$ssl_admin_order_details = get_setting_value($settings, "ssl_admin_order_details", 0);
	$ssl_admin_orders_pages = get_setting_value($settings, "ssl_admin_orders_pages", 0);
	$secure_admin_order_create = get_setting_value($settings, "secure_admin_order_create", 0);
	if ($ssl_admin_orders_list && strlen($secure_url)) {
		$orders_list_site_url = $admin_secure_url;
	} else {
		$orders_list_site_url = $admin_site_url;
	}
	if ($ssl_admin_order_details && strlen($secure_url)) {
		$order_details_site_url = $admin_secure_url;
	} else {
		$order_details_site_url = $admin_site_url;
	}
	if ($ssl_admin_orders_pages && strlen($secure_url)) {
		$orders_pages_site_url = $admin_secure_url;
	} else {
		$orders_pages_site_url = $admin_site_url;
	}
	if ($secure_admin_order_create && strlen($secure_url)) {
		$admin_order_call_url = $admin_secure_url . "admin_order_call.php";
	} else {
		$admin_order_call_url = $admin_site_url . "admin_order_call.php";
	}

	$t->set_file("admin_header", "admin_header.html");

	$t->set_var("CHARSET", CHARSET);
	$t->set_var("site_url", $site_url);
	$t->set_var("index_href", $site_url . "index.php");	
	$t->set_var("admin_href", $admin_site_url . "admin.php");
	$t->set_var("admin_global_search_href", $admin_site_url . "admin_global_search.php");
	
	// New block
	$current_date_header = va_time();
	$cyear_header = $current_date_header[YEAR]; 
	$cmonth_header = $current_date_header[MONTH]; 
	$cday_header = $current_date_header[DAY];
	$today_header = mktime (0, 0, 0, $cmonth_header, $cday_header, $cyear_header);
	$tomorrow_header = mktime (0, 0, 0, $cmonth_header, $cday_header + 1, $cyear_header);
	
	$t->set_var("today_header",va_date($date_edit_format, $today_header));
	
	$new_date_header = date("Y-m-d");
	if ($orders_permission) {
		$sql = " SELECT count(o.order_id) ";
		$sql.= " FROM ".$table_prefix."orders o ";
		//$sql.= " INNER JOIN ".$table_prefix."order_statuses os ON os.paid_status = 1 AND os.status_id = o.order_status ";
		$sql.= " WHERE o.order_placed_date > ".$db->tosql($today_header,DATE);
		$sql.= " AND o.order_placed_date < ".$db->tosql($tomorrow_header,DATE);
		$t->set_var("new_orders",get_db_value($sql));
		$t->sparse("new_orders_block", false);
	}

	if ($site_users_permission) {
		$sql = " SELECT count(user_id) ";
		$sql.= " FROM ".$table_prefix."users ";
		$sql.= " WHERE registration_date > ".$db->tosql($today_header,DATE);
		$sql.= " AND registration_date < ".$db->tosql($tomorrow_header,DATE);
		$t->set_var("new_users",get_db_value($sql));
		$t->sparse("new_users_block", false);
	}
	
	if ($tickets_permission) {
		$sql = " SELECT count(support_id) ";
		$sql.= " FROM ".$table_prefix."support ";
		$sql.= " WHERE date_added > ".$db->tosql($today_header,DATE);
		$sql.= " AND date_added < ".$db->tosql($tomorrow_header,DATE);
		$t->set_var("new_tickets",get_db_value($sql));
		$t->sparse("new_tickets_block", false);
	}

	if($orders_permission || $tickets_permission || $site_users_permission) {
		$t->sparse("today_block", false);
	}


	
	$version_number = va_version();
	$version_name   = ucfirst($va_name);
	$version_type   = ucfirst($va_type);
	$t->set_var("version_number", $version_number);
	$t->set_var("version_name",   $version_name);
	if ($version_type != $version_name) {
		$t->set_var("version_type", $version_type);
	}

	// additional blocks	
	include_once ("./blocks/admin_leftside_menu.php");
	admin_leftside_topmenu_block('block_leftside_topmenu');
	admin_leftside_menu_block('block_leftside_menu');
	admin_leftside_breadcrumbs_block('block_leftside_breadcrumbs');
	
	include_once ("./blocks/admin_bookmarks.php");
	admin_bookmarks_block('block_bookmarks');

	$t->parse("admin_header", false);

?>