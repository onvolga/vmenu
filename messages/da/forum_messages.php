<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "Emne information");
	define("TOPIC_MESSAGE_TITLE", "Besked");

	define("MY_FORUM_TOPICS_MSG", "Mit Forum emner");
	define("ALL_FORUM_TOPICS_MSG", "Alle Forum emner");
	define("MY_FORUM_TOPICS_DESC", "Har du nogensinde tГ¦nk pГҐ at det problem du sidder med er der en anden, der ogsГҐ har haft? Kunne du tГ¦nke dig at dele dine erfaringer med nye brugere? Hvorfor ikke blive bruger af forummet og deltage i snakken og debatten?");
	define("NEW_TOPIC_MSG", "Nyt emne");
	define("NO_TOPICS_MSG", "Ingen emner fundet");
	define("FOUND_TOPICS_MSG", "Vi fandt <b>{found_records}</b> emner der matcher '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "ingen forum fundet");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "Emner");
	define("FORUM_REPLIES_COLUMN", "Svar");
	define("FORUM_LAST_POST_COLUMN", "Senest opdateret");
	define("FORUM_MODERATORS_MSG", "Moderatorer");

	define("TOPIC_NAME_COLUMN", "Emner");
	define("TOPIC_AUTHOR_COLUMN", "Forfatter");
	define("TOPIC_VIEWS_COLUMN", "Visninger");
	define("TOPIC_REPLIES_COLUMN", "Svar");
	define("TOPIC_UPDATED_COLUMN", "Seneste opdatering");
	define("TOPIC_ADDED_MSG", "Tak <br> Din emne blev tilfГёjet");

	define("TOPIC_ADDED_BY_FIELD", "TilfГёjet af");
	define("TOPIC_ADDED_DATE_FIELD", "TilfГёjetLagt");
	define("TOPIC_UPDATED_FIELD", "Sidst opdateret");
	define("TOPIC_NICKNAME_FIELD", "Brugenavn");
	define("TOPIC_EMAIL_FIELD", "Din email adresse");
	define("TOPIC_NAME_FIELD", "Emne");
	define("TOPIC_MESSAGE_FIELD", "Besked");
	define("TOPIC_NOTIFY_FIELD", "Send alle svar pГҐ min e-mail");

	define("ADD_TOPIC_BUTTON", "TilfГёj emne");
	define("TOPIC_MESSAGE_BUTTON", "TilfГёj besked");

	define("TOPIC_MISS_ID_ERROR", "Forsvundne <b> trГҐd ID </ b> parameter.");
	define("TOPIC_WRONG_ID_ERROR", "<b> TrГҐd ID </ b> parameter har forkert vГ¦rdi.");
	define("FORUM_SEARCH_MESSAGE", "Vi fandt {search_count} beskeder der svarer til '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Gem");

	define("LAST_POST_ON_SHORT_MSG", "PГҐ:");
	define("LAST_POST_IN_SHORT_MSG", "I:");
	define("LAST_POST_BY_SHORT_MSG", "Af:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Sidst Г¦ndret:");

?>