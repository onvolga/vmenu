<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// повідомлення підтримки
	define("SUPPORT_TITLE", "Центр пiдтримки");
	define("SUPPORT_REQUEST_INF_TITLE", "Iнформацiя по запиту");
	define("SUPPORT_REPLY_FORM_TITLE", "Вiдповiдь");

	define("MY_SUPPORT_ISSUES_MSG", "Мої запити");
	define("MY_SUPPORT_ISSUES_DESC", "Якщо у вас виникли проблеми пiсля купiвлi продукту нашi менеджери готовi вирiшити їх. Не вагайтесь нi хвилини i надсилайте вашi запити, йдучи по посиланню вище.");
	define("NEW_SUPPORT_REQUEST_MSG", "Новий запит");
	define("SUPPORT_REQUEST_ADDED_MSG", "Дякуємо,<br>Наш центр пiдтримки спробує невдовзi допомогти вам стосовно вашого запиту.");
	define("SUPPORT_SELECT_DEP_MSG", "Виберiть департамент");
	define("SUPPORT_SELECT_PROD_MSG", "Виберiть продукт");
	define("SUPPORT_SELECT_STATUS_MSG", "Виберiть Статус");
	define("SUPPORT_NOT_VIEWED_MSG", "Не переглянуто");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Переглянуто кристувачем");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Переглянуто адмiнiстратором");
	define("SUPPORT_STATUS_NEW_MSG", "Новий");
	define("NO_SUPPORT_REQUEST_MSG", "Не знайдено жодного запиту");

	define("SUPPORT_SUMMARY_COLUMN", "Заголовок");
	define("SUPPORT_TYPE_COLUMN", "Тип");
	define("SUPPORT_UPDATED_COLUMN", "Модифiковано");

	define("SUPPORT_USER_NAME_FIELD", "Ваше iм'я");
	define("SUPPORT_USER_EMAIL_FIELD", "Ваша поштова адреса");
	define("SUPPORT_IDENTIFIER_FIELD", "Iдентифiкатор (Номер замовлення)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Середовище (ОС, База данних, Веб сервер, тощо)");
	define("SUPPORT_DEPARTMENT_FIELD", "Департамент");
	define("SUPPORT_PRODUCT_FIELD", "Продукт");
	define("SUPPORT_TYPE_FIELD", "Тип запиту");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Поточний статус");
	define("SUPPORT_SUMMARY_FIELD", "Заголовок");
	define("SUPPORT_DESCRIPTION_FIELD", "Опис");
	define("SUPPORT_MESSAGE_FIELD", "Повiдомлення");
	define("SUPPORT_ADDED_FIELD", "Коли додано");
	define("SUPPORT_ADDED_BY_FIELD", "Ким додано");
	define("SUPPORT_UPDATED_FIELD", "Модифiковано");

	define("SUPPORT_REQUEST_BUTTON", "Надiслати запит");
	define("SUPPORT_REPLY_BUTTON", "Надiслати");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Пропущено параметер <b>Support ID</b>.");
	define("SUPPORT_MISS_CODE_ERROR", "Пропущено параметер <b>Перевiрочний код</b>.");
	define("SUPPORT_WRONG_ID_ERROR", "Параметер <b>Support ID</b> має хибне значення.");
	define("SUPPORT_WRONG_CODE_ERROR", "Параметер <b>Перевiрочний код</b> має хибне значення.");

	define("MAIL_DATA_MSG", "Дані листа");
	define("HEADERS_MSG", "Заголовки");
	define("ORIGINAL_TEXT_MSG", "Базовий текст");
	define("ORIGINAL_HTML_MSG", "Базовий HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>