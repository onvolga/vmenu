<?php

	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "messages/" . $language_code . "/install_messages.php");
	include_once($root_folder_path . "messages/" . $language_code . "/cart_messages.php");
	include_once("./admin_common.php");

	check_admin_security("system_upgrade");

	$sql_types = array(
		"mysql"   => "ALTER TABLE " . $table_prefix . "orders_items ADD COLUMN order_shipping_id INT(11) default '0' ",
		"postgre" => "ALTER TABLE " . $table_prefix . "orders_items ADD COLUMN order_shipping_id INT4 default '0' ",
		"access"  => "ALTER TABLE " . $table_prefix . "orders_items ADD COLUMN order_shipping_id INTEGER ",
	);
	$sql = $sql_types[$db_type];
	$db->query($sql);
	echo "Database updated";

?>