<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Falsche/r Download-Parameter.");
	define("DOWNLOAD_MISS_PARAM", "Fehlende/r Download-Parameter.");
	define("DOWNLOAD_INACTIVE", "Download deaktiviert.");
	define("DOWNLOAD_EXPIRED", "Ihre Download-Periode ist abgelaufen.");
	define("DOWNLOAD_LIMITED", "Sie haben Ihre maximale Anzahl Downloads СЊberschritten.");
	define("DOWNLOAD_PATH_ERROR", "Pfad zum Produkt wurde nicht gefunden.");
	define("DOWNLOAD_RELEASE_ERROR", "Ausgabe wurde nicht gefunden.");
	define("DOWNLOAD_USER_ERROR", "Nur registrierte Benutzer kС†nnen diese Datei downloaden.");
	define("ACTIVATION_OPTIONS_MSG", "Aktivierungsoptionen");
	define("ACTIVATION_MAX_NUMBER_MSG", "Max Anzahl Aktivierungen");
	define("DOWNLOAD_OPTIONS_MSG", "Downloadbar / Software Optionen");
	define("DOWNLOADABLE_MSG", "Downloadbar (Software)");
	define("DOWNLOADABLE_DESC", "fСЊr downloadbare Produkte kС†nnen Sie auch eine 'Download-Periode', 'Pfad zur downloadbaren Datei' sowie 'Aktivierungsoptionen' angeben");
	define("DOWNLOAD_PERIOD_MSG", "Download-Periode");
	define("DOWNLOAD_PATH_MSG", "Pfad zu downloadbarer Datei");
	define("DOWNLOAD_PATH_DESC", "Sie kС†nnen mehrere Pfade angeben, durch Semikolon getrennt");
	define("UPLOAD_SELECT_MSG", "WРґhlen Sie eine datei zum upload und klicken Sie {button_name}.");
	define("UPLOADED_FILE_MSG", "Datei <b>{filename}</b> wurde hochgeladen.");
	define("UPLOAD_SELECT_ERROR", "Bitte wРґhlen Sie zunРґchst eine Datei.");
	define("UPLOAD_IMAGE_ERROR", "Es sind nur Bild-Dateien zulРґssig.");
	define("UPLOAD_FORMAT_ERROR", "Dieser Dateityp ist nicht zulРґssig.");
	define("UPLOAD_SIZE_ERROR", "Dateien grС†РЇer als {dimension} sind nicht zulРґssig.");
	define("UPLOAD_DIMENSION_ERROR", "Bilder grС†РЇer als {dimension} sind nicht zulРґssig.");
	define("UPLOAD_CREATE_ERROR", "Das System kann die Datei nicht erzeugen");
	define("UPLOAD_ACCESS_ERROR", "Sie haben keine Berechtigung, Dateien hochzuladen.");
	define("DELETE_FILE_CONFIRM_MSG", "MС†chten Sie diese Datei wirklich lС†schen?");
	define("NO_FILES_MSG", "Keine Dateien gefunden");
	define("SERIAL_GENERATE_MSG", "Seriennummer erzeugen");
	define("SERIAL_DONT_GENERATE_MSG", "nicht erzeugen");
	define("SERIAL_RANDOM_GENERATE_MSG", "zufРґllige Sereinnummer fСЊr dieses Softwareprodukt erzeugen");
	define("SERIAL_FROM_PREDEFINED_MSG", "Seriennummer aus Liste auswРґhlen");
	define("SERIAL_PREDEFINED_MSG", "Angelegte Seriennummern");
	define("SERIAL_NUMBER_COLUMN", "Seriennummer");
	define("SERIAL_USED_COLUMN", "Benutzt");
	define("SERIAL_DELETE_COLUMN", "LС†schen");
	define("SERIAL_MORE_MSG", "Weitere Seriennummern hinzufСЊgen?");
	define("SERIAL_PERIOD_MSG", "Seriennummer Zeitraum");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Bestimmungen & Bedingungen anzeigen");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Um den Produkt runterzuladen bitte Bestimmungen & Bedingungen lesen und zustimmen");
	define("DOWNLOAD_TERMS_MSG", "Bestimmungen & Bedingungen");
	define("DOWNLOAD_TERMS_USER_DESC", "Ich habe die Bestimmungen & Bedingungen gelesen und stimme zu");
	define("DOWNLOAD_TERMS_USER_ERROR", "Um den Produkt runterzuladen bitte Bestimmungen & Bedingungen lesen und zustimmen");

	define("DOWNLOAD_TITLE_MSG", "Download Titel");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Datein");
	define("DOWNLOAD_INTERVAL_MSG", "Download Zeitraum");
	define("DOWNLOAD_LIMIT_MSG", "Download Begrenzung");
	define("DOWNLOAD_LIMIT_DESC", "Anzahl die eine Datei runtergeladen werden kann");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximales Download");
	define("PREVIEW_TYPE_MSG", "Vorschau Typ");
	define("PREVIEW_TITLE_MSG", "Vorschau Titel");
	define("PREVIEW_PATH_MSG", "Pfad zur Vorschau Datei");
	define("PREVIEW_IMAGE_MSG", "Vorschau Bild");
	define("MORE_FILES_MSG", "Mehr Dateien");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Nur mit Optionen benutzen");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Vorschau als Download");
	define("PREVIEW_USE_PLAYER_MSG", "Nutze Player zur Vorschau");
	define("PROD_PREVIEWS_MSG", "Vorschau");

?>