<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  reviews.php                                              ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/navigator.php");
	include_once("./includes/record.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/reviews_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./messages/" . $language_code . "/reviews_messages.php");


	$merchant_id = get_param("user");
	
	if (!$merchant_id) {
		header ("Location: " . get_custom_friendly_url("index.php"));
		exit;
	}

	$cms_page_code = "merchant_reviews_page";
	$script_name   = "merchant_reviews.php";
	$current_page  = get_custom_friendly_url("merchant_reviews.php");
	$tax_rates     = get_tax_rates();
	$auto_meta_title = "Отзывы о продавце";
	$is_reviews = true;
		
	include_once("./includes/page_layout.php");

?>
