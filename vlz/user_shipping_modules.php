<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 Blazze modules                                   ***
  ***      File:  user_shipping_modules.php                               ***
  ***      http://hlieviy.com                                              ***
  ***                                                                      ***
  ****************************************************************************
*/


    include_once("./includes/common.php");
    include_once("./includes/record.php");
    include_once("./includes/navigator.php");
    include_once("./includes/sorter.php");
    include_once("./messages/" . $language_code . "/download_messages.php");

    check_user_session();

    $cms_page_code = "user_shipping_modules";
    $script_name   = "user_shipping_modules.php";
    $current_page  = get_custom_friendly_url("user_shipping_modules.php");
    $auto_meta_title = "Методы доставки"." : ".LIST_MSG;

    include_once("./includes/page_layout.php");

?>