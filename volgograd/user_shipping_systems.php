<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 Blazze modules                                   ***
  ***      File:  user_shippings_systems.php                               ***
  ***      http://hlieviy.com                                              ***
  ***                                                                      ***
  ****************************************************************************
*/


    include_once("./includes/common.php");
    include_once("./includes/record.php");
    include_once("./includes/navigator.php");
    include_once("./includes/sorter.php");
    include_once("./messages/" . $language_code . "/download_messages.php");

    check_user_session();

    $cms_page_code = "user_shipping_systems";
    $script_name   = "user_shipping_systems.php";
    $current_page  = get_custom_friendly_url("user_shipping_systems.php");
    $auto_meta_title = "Системы доставки"." : ".LIST_MSG;

    include_once("./includes/page_layout.php");

?>