<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Manualer");
	define("NO_MANUALS_MSG", "Inga manualer hittades");
	define("NO_MANUAL_ARTICLES_MSG", "Inga artiklar");
	define("MANUALS_PREV_ARTICLE", "Fцregеende");
	define("MANUALS_NEXT_ARTICLE", "Nдsta");
	define("MANUAL_CONTENT_MSG", "Index");
	define("MANUALS_SEARCH_IN_MSG", "Sцk i");
	define("MANUALS_SEARCH_FOR_MSG", "Sцk");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Alla manualer");
	define("MANUALS_SEARCH_RESULTS_INFO", "Hittade {results_number} artikel/artiklar med {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Sцkresultat");
	define("MANUALS_NOT_FOUND_ANYTHING", "Kunde inte hitta nеgonting med '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Inget innehеll");
	define("MANUALS_SEARCH_TITLE", "Manualsцkning");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Manualsцkningsresultat");

?>