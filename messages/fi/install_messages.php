<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "Viart Shop asennus");

	define("INSTALL_STEP_1_TITLE", "Asennus: Vaihe 1");
	define("INSTALL_STEP_1_DESC", "Kiitos kun asennat ViArt ostoskortin. Ole hyvд ja tдydennд alla olevat tiedot. Huomio ettд tietokannan tulisi jo olla olemassa. Jos asennat kдyttдen ODBC/Microsoft Access, luo ensin DNS");
	define("INSTALL_STEP_2_TITLE", "Asennus: Vaihe 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Asennus: Vaihe 3");
	define("INSTALL_STEP_3_DESC", "Valitse sivun teema, tдtд asetusta voit muuttaa myцhemmin");
	define("INSTALL_FINAL_TITLE", "Asennus: Valmis");
	define("SELECT_DATE_TITLE", "Valitse pдivдyksen nдyttц");

	define("DB_SETTINGS_MSG", "Tietokanta-asetukset");
	define("DB_PROGRESS_MSG", "Tietokannan luominen");
	define("SELECT_PHP_LIB_MSG", "Valitse PHP kirjasto");
	define("SELECT_DB_TYPE_MSG", "Valitse tietokannan tyyppi");
	define("ADMIN_SETTINGS_MSG", "Hallinnalliset asetukset");
	define("DATE_SETTINGS_MSG", "Aika/Pдivдasetukset");
	define("NO_DATE_FORMATS_MSG", "Ei saatavilla");
	define("INSTALL_FINISHED_MSG", "Tдssд vaiheessa perusasennus on valmis. Ole hyvд ja kдytд hallinta-asetuksia muuttaaksesi muita toimintoja");
	define("ACCESS_ADMIN_MSG", "Pддstдksesi hallintaosioon, paina tдstд");
	define("ADMIN_URL_MSG", "Hallinnan osoite");
	define("MANUAL_URL_MSG", "Manuaaliosoite (URL)");
	define("THANKS_MSG", "Kiitos kun valitsit <b>ViArt SHOP:in</b>.");

	define("DB_TYPE_FIELD", "Tietokannan tyyppi");
	define("DB_TYPE_DESC", "Valitse <b>tietokannan tyyppi</b> jota kдytдt. Jos kдytдt SQL Serveriд tai Microsoft Accessia, valitse ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP Kirjasto");
	define("DB_HOST_FIELD", "Isдntдnimi");
	define("DB_HOST_DESC", "Anna <b>nimi</b> tai <b>serverin IP osoite</b> jossa ViArt tietokanta on. Jos serveri on omalla koneellasti, luultavasti \"<b>localhost</b>\" ja jдtд portti tyhjдksi. Tarvittaessa kysy palveluntarjoajaltasi lisдtietoja tietokannoista.");
	define("DB_PORT_FIELD", "Portti");
	define("DB_NAME_FIELD", "Tietokannan nimi / DSN");
	define("DB_NAME_DESC", "Jos kдytдt tietokantaa kuten MySQL tai PostgreSQL ole hyvд ja anna <b>tietokannan nimi</b> mihin haluat ViArtin luovan taulukot. Tдmдn tietokannan pitдд olla jo olemassa. Jos olet vain asentamassa testitarkoituksessa, yleensд on olemassa \"<b>test</b>\" tietokanta jota voit kдyttдд. Jos ei, ole hyvд ja luo esim tietokanta 'viart'. ");
	define("DB_USER_FIELD", "Kдyttдjдtunnus");
	define("DB_PASS_FIELD", "Salasana");
	define("DB_USER_PASS_DESC", "<b>Kдyttдjдtunnus</b> ja <b>Salasana</b> -Ole hyvд ja anna kдyttдjдtunnus ja salasana tietokannalle.Jos kдytдt paikallista konetta, kдyttдjдtunnus on tod.nдk \"<b>root</b>\" ja salasana tyhjд. Tдmд on ok testauksessa, mutta ei varsinaisessa asennuksessa!");
	define("DB_PERSISTENT_FIELD", "Jatkuva yhteys");
	define("DB_PERSISTENT_DESC", "MYSQL/Postgre jatkuva yhteys. Jos et tiedд mitд se tarkoittaa, on parempi olla koskematta tдhдn kohtaan");
	define("DB_CREATE_DB_FIELD", "Create DB");
	define("DB_CREATE_DB_DESC", "to create database if possible, tick this box. Works only for MySQL and Postgre");
	define("DB_POPULATE_FIELD", "Luo tietokanta");
	define("DB_POPULATE_DESC", "Luo tietokanta ja rakenne");
	define("DB_TEST_DATA_FIELD", "Testidata");
	define("DB_TEST_DATA_DESC", "Tekee testidataa taulukkoon");
	define("ADMIN_EMAIL_FIELD", "Hallinnan sдhkцposti");
	define("ADMIN_LOGIN_FIELD", "Hallinan sisддnkirjaus");
	define("ADMIN_PASS_FIELD", "Hallinan salasana");
	define("ADMIN_CONF_FIELD", "Vahvista salasana");
	define("DATETIME_SHOWN_FIELD", "Pдivдmддrдn nдyttц (sivustolla)");
	define("DATE_SHOWN_FIELD", "Pдivдyksen nдyttц (sivustolla)");
	define("DATETIME_EDIT_FIELD", "Pдivдyksen nдyttц (muokatessa)");
	define("DATE_EDIT_FIELD", "Pдivдmддrдn nдyttц (muokatessa)");
	define("DATE_FORMAT_COLUMN", "Pдivдysmuoto");
	define("CURRENT_DATE_COLUMN", "Nykyinen pдivд");

	define("DB_LIBRARY_ERROR", "PHP toiminnot {db_library} eivдt ole mддriteltyinд. Tarkista tietokannan mддritykset tiedostosta - php.ini.");
	define("DB_CONNECT_ERROR", "En saa yhteyttд tietokantaan, tarkista asetukset");
	define("INSTALL_FINISHED_ERROR", "Asennus on jo tehty!");
	define("WRITE_FILE_ERROR", "Tiedostoon<b>'includes/var_definition.php'</b>. Ei voi kirjoittaa. Vaihda asetukset ennen jatkamista");
	define("WRITE_DIR_ERROR", "Kansioon <b>'includes/'</b>. Ei voi kirjoittaa. Vaihda asetuksia ennen jatkamista");
	define("DUMP_FILE_ERROR", "Dumppitiedostoa '{file_name}' ei lцytynyt");
	define("DB_TABLE_ERROR", "Taulukkoa '{table_name}' ei lцytynyt. Tee tarvittavat muutokset tietokantaan.");
	define("TEST_DATA_ERROR", "Tarkia<b>{POPULATE_DB_FIELD}</b> ennen testitaulukkojen tekemistд");
	define("DB_HOST_ERROR", "Isдntдnimeд jonka annoit ei lцydy");
	define("DB_PORT_ERROR", "Yhteyttд tдhдn porttiin (MYSQL) ei voitu luoda");
	define("DB_USER_PASS_ERROR", "Salasana tai kдyttдjдtunnus ovat vддrin");
	define("DB_NAME_ERROR", "Sisддnkirjaus onnistui, mutta tietokantaa '{db_name}' ei lцytynyt");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt SHOP pдivitys");
	define("UPGRADE_NOTE", "HUOM! Tee varmuuskopiot ennen jatkamista");
	define("UPGRADE_AVAILABLE_MSG", "Tietokannan pдivitys saatavilla");
	define("UPGRADE_BUTTON", "Pдivitд tietokanta versioon {version_number} nyt");
	define("CURRENT_VERSION_MSG", "Nyt asennettu versio");
	define("LATEST_VERSION_MSG", "Versio saatavilla");
	define("UPGRADE_RESULTS_MSG", "Pдivityksen tulos");
	define("SQL_SUCCESS_MSG", "SQL kyselyt onnistuivat");
	define("SQL_FAILED_MSG", "SQL kyselyt epдonnistuivat");
	define("SQL_TOTAL_MSG", "SQL kyselyitд suoritettu");
	define("VERSION_UPGRADED_MSG", "Tietokantasi on pдivitetty");
	define("ALREADY_LATEST_MSG", "Sinulla on jo viimeisin versio");
	define("DOWNLOAD_NEW_MSG", "Uusin versio havaittu");
	define("DOWNLOAD_NOW_MSG", "Lataa versio {version_number} nyt");
	define("DOWNLOAD_FOUND_MSG", "Huomasimme, ettд uusi versio {version_number} on saatavilla. Klikkaa linkkiд aloittaaksesi latauksen.ladattuasi ja pдivitettyдsi, muista kдynnistдд pдivitystoiminto uudelleen");
	define("NO_XML_CONNECTION", "Varoitus! Yhteyttд osoitteeseen 'http://www.viart.com/' ei voida luoda!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>