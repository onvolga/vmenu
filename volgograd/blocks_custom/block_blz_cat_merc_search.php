<?php 
    include_once("./includes/sorter.php");
    include_once("./includes/navigator.php");
    include_once("./includes/items_properties.php");
    include_once("./includes/products_functions.php");
    include_once("./includes/table_view_functions.php");
    include_once("./includes/shopping_cart.php");
    include_once("./includes/filter_functions.php");
    include_once("./includes/previews_functions.php");
    include_once("./messages/" . $language_code . "/cart_messages.php");
    include_once("./messages/" . $language_code . "/reviews_messages.php");
    include_once("./messages/" . $language_code . "/download_messages.php");

    $search_category_id = get_param("search_category_id");
    $search_string = trim(get_param("search_string"));
    if($search_string){

        $t->set_file("block_body", "block_blz_cat_merc_search.html");
        $words = explode(" ", $search_string);
        $search_words = array();
        foreach ($words as $w) {
            if(mb_strlen($w) > 3){
                $search_words[] = $w;
            }
        }
        if(count($search_words)){
            //category search
            $search = prapareSQLLike($search_words);
            $stmt = "SELECT category_name, friendly_url FROM va_categories WHERE " . $search . " GROUP BY category_name";
            $db->query($stmt);
            $search_categories = array();
            while($db->next_record()){
                $search_categories[] = array("name" => $db->f("category_name"), "url" => $db->f("friendly_url"));
            }
            
            //merchant search
            $search = prapareSQLLike($search_words, "company_name");
            $stmt = "SELECT company_name, friendly_url FROM va_users WHERE (" . $search . ") GROUP BY company_name";
            $db->query($stmt);
            $search_shops = array();
            while($db->next_record()){
                $search_shops[] = array("name" => $db->f("company_name"), "url" => $db->f("friendly_url"));
            }

            //category parse
            if(count($search_categories)){
                foreach ($search_categories as $v) {
                    $t->set_var("url", $v["url"]);
                    $t->set_var("name", $v["name"]);
                    $t->parse('found_category', true);
                }
                 $t->parse('found_categories', false);
                
            }

            //merchant parse
            if(count($search_shops)){
                foreach ($search_shops as $v) {
                    $t->set_var("url", $v["url"]);
                    $t->set_var("name", $v["name"]);
                    $t->parse('found_shop', true);
                }
                 $t->parse('found_shops', false);
                
            }

            $block_parsed = true;
        }

    }

    /**
     * @param $needle words
     * @param $searchField mysql field name
     * @return String mysql query part
     */
    function prapareSQLLike(array $needle, $searchField = "category_name"){
        $res = "";
        for($i = 0; $i < count($needle); $i++) {

            if($i > 0){
                $res .= ' OR ' . $searchField . " LIKE '%" . $needle[$i] . "%'";
            }
            elseif($i === 0){
                 $res .= ' ' . $searchField . " LIKE '%" . $needle[$i] . "%'";
            }
            
        }

        return $res;

    }