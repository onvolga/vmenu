<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_fm_upload_files.php                                ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/admin_fm_functions.php");
	include_once($root_folder_path . "includes/zip_class.php");
	include_once($root_folder_path . "messages/" . $language_code . "/download_messages.php");

	include_once("./admin_common.php");
	include_once("./admin_fm_config.php");

	check_admin_security("filemanager");

	$dir_root = check_dir_path(get_param("dir_root"));
	$error = check_dir_and_file($dir_root, "", "chmod");
	if (strlen($error)){
		set_session("fm_error",$error);
		header("Location: " . $host."admin_fm.php");
		exit;
	}
	
	$t = new VA_Template($settings["admin_templates_dir"]);
	
	$op = terminator(get_param("op"));

	if(strlen($op) == 0) {
		$op = 1;
	}
	
	if($op == 1) {
		$t->set_file("main","admin_fm_upload_files.html");
	}
	else {
		$t->set_file("main","admin_fm_upload_archive.html");		
	}
	$errors = "";
	$service_message = "";
	$tmp_dir_arch = $tmp_dir_arch;
	$t->set_var("dir_root", $dir_root);
	if(is_array($_FILES)) {
		foreach($_FILES as $key_file => $up_file) {

			$error = "";
			$tmp_name = $up_file["tmp_name"];
			$filename = terminator($up_file["name"]);
			$ext = get_ext_file($filename);
			$filesize = $up_file["size"];
			$upload_error = isset($up_file["error"]) ? $up_file["error"] : "";

			// first - check different errors
			if ($upload_error == 1) {
				$error = FILESIZE_DIRECTIVE_ERROR_MSG . "<br>\n";
			} elseif ($upload_error == 2) {
				$error = FILESIZE_PARAMETER_ERROR_MSG . "<br>\n";
			} elseif ($upload_error == 3) {
				$error = PARTIAL_UPLOAD_ERROR_MSG . "<br>\n";
			} elseif ($upload_error == 4) {
				$error = UPLOAD_SELECT_ERROR . "<br>\n";
			} elseif ($upload_error == 6) {
				$error = TEMPORARY_FOLDER_ERROR_MSG . ".<br>\n";
			} elseif ($upload_error == 7) {
				$error = FILE_WRITE_ERROR_MSG . "<br>\n";
			}

			if (!$error && 
				(!in_array($ext, $conf_array_files_ext) && $op == 1) && 
				(!in_array($ext, $array_archive_file) && $op == 2)
			) {
				$error = fm_errors(115, $filename);
			}
			if(!$error && !is_uploaded_file($tmp_name)) {
				$error = fm_errors(114, $filename);
			}

			if (!$error && !@move_uploaded_file($tmp_name, $dir_root."/".$filename))
			{
				if (!is_dir($dir_root)) {
					$error = FOLDER_DOESNT_EXIST_MSG . $dir_root;
				} elseif (!is_writable($dir_root)) {
					$error = str_replace("{folder_name}", $dir_root, FOLDER_PERMISSION_MESSAGE) . "<br>\n";
				} else {
					$error = UPLOAD_CREATE_ERROR ." <b>" . dir_root . $filename . "</b><br>\n";
				}
			}

			if ($error) {
				if ($upload_error != 4) {
					$errors .= $error;	
				}
			} else {
				if ($op == 1) {
					$service_message .= FILE_SAVED_MSG . $filename ;
				} elseif($op == 2) {
					$array_files = array();
					$flname = $dir_root . "/" .  $filename;
					$archive = new XPRAPTORZIP($flname);
					if ($archive->extract(XPrptrZIP_OPT_PATH, $dir_root . "/" . $tmp_dir_arch) != 0) {
						$error_files = check_error_files($dir_root . "/" . $tmp_dir_arch);
						rm($dir_root . "/" . $tmp_dir_arch);
						if(strlen($error_files) > 0) {
							$error_files = substr($error_files, 0, -2);
							$error = INCORRECT_EXTENSIONS_MSG  . $error_files . "<br>" . ARCHIVE_MSG . $filename . ".";
							$errors .= $error;
						}
						else {
							$archive->extract(XPrptrZIP_OPT_PATH, $dir_root);
							$service_message .= FILE_SAVED_MSG . $filename ;
						}
					} else {
						$error = $archive->errorInfo(true);
					}
					rm($flname);
				}
			}
		}
	}
	if (strlen($error)) {
		$t->set_var("error", $errors);
		$t->parse("errors", true);
	} else {
		$t->set_var("errors", "");
	}
	if(strlen($service_message) > 0) {
		$t->set_var("service_message", $service_message);
		$t->parse("service_messages", true);
	} else {
		$t->set_var("service_messages", "");
	}
	$t->set_var("admin_select_href", "admin_select.php");
	$t->set_var("admin_upload_href", "admin_fm_upload_files.php");
	$t->set_var("admin_filemanager_href", "admin_fm.php");

	include_once("./admin_header.php");
	include_once("./admin_footer.php");
	$t->pparse("main");

?>
