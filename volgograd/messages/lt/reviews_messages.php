<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// apюvalgш/vertinimo юinutлs
	define("OPTIONAL_MSG", "Nebыtinas");
	define("BAD_MSG", "Blogas");
	define("POOR_MSG", "Silpnas");
	define("AVERAGE_MSG", "Vidutinis");
	define("GOOD_MSG", "Geras");
	define("EXCELLENT_MSG", "Puikus");
	define("REVIEWS_MSG", "Apюvalgos");
	define("NO_REVIEWS_MSG", "Apюvalgш nerasta");
	define("WRITE_REVIEW_MSG", "Raрykite apюvalgа");
	define("RATE_PRODUCT_MSG", "Бvertinkite prekж");
	define("RATE_ARTICLE_MSG", "Бvertinkite рб straipsnб");
	define("NOT_RATED_PRODUCT_MSG", "Prekл dar neбvertinta");
	define("NOT_RATED_ARTICLE_MSG", "Straipsnis dar neбvertintas");
	define("AVERAGE_RATING_MSG", "Vidutinis kliento бvertinimas");
	define("BASED_ON_REVIEWS_MSG", "Pagrбsta {total_votes} apюvalgomis");
	define("POSITIVE_REVIEW_MSG", "Teigiamos klientш apюvalgos");
	define("NEGATIVE_REVIEW_MSG", "Neigiamos klientш apюvalgos");
	define("SEE_ALL_REVIEWS_MSG", "Юiыrлkite visas klientш apюvalgas..");
	define("ALL_REVIEWS_MSG", "Visos apюvlagos");
	define("ONLY_POSITIVE_MSG", "Tik teigiamos");
	define("ONLY_NEGATIVE_MSG", "Tik neigiamos");
	define("POSITIVE_REVIEWS_MSG", "Teigiamos apюvalgos");
	define("NEGATIVE_REVIEWS_MSG", "Neigiamos apюvalgos");
	define("SUBMIT_REVIEW_MSG", "Aиiы Jums<br>Jыsш pastabos bus perюiыrлtos. Jei bus patvirtintos jos pasirodys mыsш svetainлje.<br>Mes pasiliekam teisж atmesti bet kokius siuntimus kurie netinka mыsш nuostatoms.");
	define("ALREADY_REVIEW_MSG", "Jыs jau бdлjote savo apюvalgа.");
	define("RECOMMEND_PRODUCT_MSG", "Ar Jыs patarsite<br> рiа prekж kitiems?");
	define("RECOMMEND_ARTICLE_MSG", "Ar Jыs patarsite<br> рб straipsnб kitiems?");
	define("RATE_IT_MSG", "Бvertinkite jб");
	define("NAME_ALIAS_MSG", "Vardas arba slapyvardis");
	define("SHOW_MSG", "Rodyti");
	define("FOUND_MSG", "Rasta");
	define("RATING_MSG", "Vidutinis бvertinimas");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>