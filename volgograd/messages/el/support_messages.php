<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "КЭнфсп впЮиейбт");
	define("SUPPORT_REQUEST_INF_TITLE", "РлзспцпсЯет есщфЮмбфпт");
	define("SUPPORT_REPLY_FORM_TITLE", "БрЬнфзуз");

	define("MY_SUPPORT_ISSUES_MSG", "Фб бйфЮмбфб мпх гйб ВпЮиейб");
	define("MY_SUPPORT_ISSUES_DESC", "ЕЬн ёчефе прпйпдЮрпфе рсьвлзмб ме рспъьн рпх ёчефе бгпсЬуей пй хреэихнпй гйб фзн ВпЮиейб еЯнбй Эфпймпй нб фп лэупхн . ЕрйкпйнщнЮуфе мбжЯ мбт кЬнпнфбт клйк рйп кЬфщ кбй гсЬшфе фп рсьвлзмб убт");
	define("NEW_SUPPORT_REQUEST_MSG", "НЭп бЯфзмб впЮиейбт");
	define("SUPPORT_REQUEST_ADDED_MSG", "ехчбсйуфпэме , фп рспущрйкь гйб фзн ВпЮиейб иб рспурбиЮуей нб убт впзиЮуей рплэ уэнфпмб");
	define("SUPPORT_SELECT_DEP_MSG", "ерйлЭофе фмЮмб");
	define("SUPPORT_SELECT_PROD_MSG", "ерйлЭофе рспъьн");
	define("SUPPORT_SELECT_STATUS_MSG", "ерйлЭофе КбфЬуфбуз");
	define("SUPPORT_NOT_VIEWED_MSG", "Ден фп еЯдбн бкьмз");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Фп Эчпхн дей ерйукЭрфет");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Фп ёчей дей п дйбчейсйуфЮт");
	define("SUPPORT_STATUS_NEW_MSG", "НЭп");
	define("NO_SUPPORT_REQUEST_MSG", "КбнЭнб жЮфзмб ден всЭизке");

	define("SUPPORT_SUMMARY_COLUMN", "РесЯлзшз");
	define("SUPPORT_TYPE_COLUMN", "Фэрпт");
	define("SUPPORT_UPDATED_COLUMN", "ФелехфбЯб бнбнЭщуз");

	define("SUPPORT_USER_NAME_FIELD", "Фп јнпмб убт");
	define("SUPPORT_USER_EMAIL_FIELD", "Фп e-mail убт");
	define("SUPPORT_IDENTIFIER_FIELD", "Рбсбуфбфйкь (фймпльгйп #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "РесйвЬллпн (OS, Database, Web Server, etc.)");
	define("SUPPORT_DEPARTMENT_FIELD", "фмЮмб");
	define("SUPPORT_PRODUCT_FIELD", "рспъьн");
	define("SUPPORT_TYPE_FIELD", "Фэрпт");
	define("SUPPORT_CURRENT_STATUS_FIELD", "ХрЬсчпхуб КбфЬуфбуз");
	define("SUPPORT_SUMMARY_FIELD", "РесЯлзшз");
	define("SUPPORT_DESCRIPTION_FIELD", "РесйгсбцЮ");
	define("SUPPORT_MESSAGE_FIELD", "МЮнхмб");
	define("SUPPORT_ADDED_FIELD", "РспуфЭизке");
	define("SUPPORT_ADDED_BY_FIELD", "РспуфЭизке брь");
	define("SUPPORT_UPDATED_FIELD", "ФелехфбЯб бнбнЭщуз");

	define("SUPPORT_REQUEST_BUTTON", "Submit Request");
	define("SUPPORT_REPLY_BUTTON", "БрЬнфзуз");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "ЛеЯрей фп <b> ID впЮиейбт</b>");
	define("SUPPORT_MISS_CODE_ERROR", "ЛеЯрей з рбсЬмефспт <b>ерйвевбЯщузт</b>");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Фп ID впЮиейбт</b> ёчей лбнибумЭнет рбсбмЭфспхт");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>З ерйвевбЯщуз </b> ёчей ЛЬипт рбсбмЭфспхт");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>