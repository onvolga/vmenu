<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  profile_functions.php                                    ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


function get_phone_codes()
{
	global $db, $table_prefix;
	$phone_codes = array(array("", ""));
	$sql  = " SELECT country_code, phone_code FROM " . $table_prefix . "countries ";
	$sql .= " WHERE show_for_user=1 AND phone_code IS NOT NULL AND phone_code <>'' ";
	$sql .= " ORDER BY country_order, country_name ";
	$db->query($sql);	
	while ($db->next_record()) {
		$country_code = $db->f("country_code");
		$phone_code = $db->f("phone_code");	
		$phone_codes[] = array($phone_code, $country_code.": (".$phone_code.") ");
	}
	return $phone_codes;
}

function phone_code_checks($phone_codes)
{
	global $settings, $r, $phone_parameters;
	$phone_code_select = get_setting_value($settings, "phone_code_select", 1);
	if ($phone_code_select) {
		foreach ($phone_parameters as $id => $field_name) {
			$show_option = $r->get_property_value($field_name, SHOW);
			if ($show_option) {
				$phone_number = $r->get_value($field_name);
				if (preg_match("/^\s*\(([^\)]+)\)\s*(.*)$/", $phone_number, $matches)) {
					$phone_code = $matches[1];
					$phone_number = $matches[2];
					$code_desc = get_array_value($phone_code, $phone_codes);
					if (strlen($code_desc)) {
						$r->set_value($field_name."_code", $phone_code);
						$r->set_value($field_name, $phone_number);
					}
				}
			}
		}
	}
}

function disable_phone_codes()
{
	global $phone_parameters, $r;
	foreach ($phone_parameters as $id => $field_name) {
		$code_field = $field_name."_code";
		if ($r->parameter_exists($code_field)) {
			$r->change_property($code_field, USE_IN_INSERT, false);
			$r->change_property($code_field, USE_IN_UPDATE, false);
			$r->change_property($code_field, USE_IN_SELECT, false);
		}
	}
}

function join_phone_fields()
{
	global $phone_parameters, $r;
	foreach ($phone_parameters as $id => $field_name) {
		$code_field = $field_name."_code";
		if ($r->parameter_exists($code_field) && !$r->is_empty($code_field)) {
			$phone_code = $r->get_value($code_field);
			$phone_number = $r->get_value($field_name);
			$r->set_value($field_name, "(".$phone_code.") ".$phone_number);
		}
	}
}

function prepare_states(&$r)
{
	global $t, $db, $table_prefix;
	// get country data from record
	$personal_country_id = $r->parameter_exists("country_id") ? $r->get_value("country_id") : "";
	$delivery_country_id = $r->parameter_exists("delivery_country_id") ? $r->get_value("delivery_country_id") : "";
	$bill_country_id = $r->parameter_exists("bill_country_id") ? $r->get_value("bill_country_id") : "";
	$states = array(); 
	$personal_states = array(array("", SELECT_STATE_MSG)); 
	$delivery_states = array(array("", SELECT_STATE_MSG)); 
	$bill_states = array(array("", SELECT_STATE_MSG)); 
	$sql = "SELECT country_id, state_id, state_name FROM " . $table_prefix . "states WHERE show_for_user=1 ORDER BY country_id, state_order, state_name ";
	$db->query($sql);
	while ($db->next_record()) {
		$country_id = $db->f("country_id");
		$state_id = $db->f("state_id");
		$state_name = get_translation($db->f("state_name"));
		if ($personal_country_id == $country_id) {
			$personal_states[] = array($state_id, $state_name);
		}
		if ($delivery_country_id == $country_id) {
			$delivery_states[] = array($state_id, $state_name);
		}
		if ($bill_country_id == $country_id) {
			$bill_states[] = array($state_id, $state_name);
		}
		if (!isset($states[$country_id])) { $states[$country_id] = array(); }
		$states[$country_id][$state_id] = $state_name;
	}
	$states_json = va_json_encode($states);

	// update record controls and set special tags
	$r->change_property("state_id", VALUES_LIST, $personal_states);
	if ($r->parameter_exists("state_id") && sizeof($personal_states) <= 1) {
		$r->change_property("state_id", VALIDATION, false);
	}
	$r->change_property("delivery_state_id", VALUES_LIST, $delivery_states);
	if ($r->parameter_exists("delivery_state_id") && sizeof($delivery_states) <= 1) {
		$r->change_property("delivery_state_id", VALIDATION, false);
	}
	$r->change_property("bill_state_id", VALUES_LIST, $bill_states);
	if ($r->parameter_exists("bill_state_id") && sizeof($bill_states) <= 1) {
		$r->change_property("bill_state_id", VALIDATION, false);
	}
	$t->set_var("states_json", $states_json);

	if ($personal_country_id && sizeof($personal_states) > 1) {
		$t->set_var("state_id_control_style", "display: inline;");
		$t->set_var("state_id_required_style", "display: inline;");
		$t->set_var("state_id_comments_style", "display: none;");
	} else {
		$t->set_var("state_id_control_style", "display: none;");
		if ($personal_country_id) {
			$t->set_var("state_id_comments", NO_STATES_FOR_COUNTRY_MSG);
		} else {
			$t->set_var("state_id_comments", SELECT_COUNTRY_FIRST_MSG);
		}
		$t->set_var("state_id_comments_style", "display: inline;");
		$t->set_var("state_id_required_style", "display: none;");
	}
	if ($delivery_country_id && sizeof($delivery_states) > 1) {
		$t->set_var("delivery_state_id_control_style", "display: inline;");
		$t->set_var("delivery_state_id_required_style", "display: inline;");
		$t->set_var("delivery_state_id_comments_style", "display: none;");
	} else {
		$t->set_var("delivery_state_id_control_style", "display: none;");
		if ($delivery_country_id) {
			$t->set_var("delivery_state_id_comments", NO_STATES_FOR_COUNTRY_MSG);
		} else {
			$t->set_var("delivery_state_id_comments", SELECT_COUNTRY_FIRST_MSG);
		}
		$t->set_var("delivery_state_id_comments_style", "display: inline;");
		$t->set_var("delivery_state_id_required_style", "display: none;");
	}
	if ($bill_country_id && sizeof($bill_states) > 1) {
		$t->set_var("bill_state_id_control_style", "display: inline;");
		$t->set_var("bill_state_id_required_style", "display: inline;");
		$t->set_var("bill_state_id_comments_style", "display: none;");
	} else {
		$t->set_var("bill_state_id_control_style", "display: none;");
		if ($bill_country_id) {
			$t->set_var("cill_state_id_comments", NO_STATES_FOR_COUNTRY_MSG);
		} else {
			$t->set_var("bill_state_id_comments", SELECT_COUNTRY_FIRST_MSG);
		}
		$t->set_var("bill_state_id_comments_style", "display: inline;");
		$t->set_var("bill_state_id_required_style", "display: none;");
	}


		

	return $states;
}

function get_js_states()
{
	global $db, $table_prefix;

	$states_js = "";
	$sql = " SELECT country_id, state_id, state_name FROM ".$table_prefix."states ORDER BY country_id, state_order, state_name";
	$db->query($sql);
	while ($db->next_record()) {
		$country_id = $db->f("country_id");
		$state_id = $db->f("state_id");
		$state_name = get_translation($db->f("state_name"));
		if ($states_js) { $states_js .= "#"; }
		$states_js .= $country_id."|".$state_id."|".prepare_js_value($state_name);
	}
	return $states_js;
}

?>