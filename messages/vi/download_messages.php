<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Sai tham sЇ tдi xuЇng");
	define("DOWNLOAD_MISS_PARAM", "Sai tham sЇ tдi xuЇng");
	define("DOWNLOAD_INACTIVE", "ChСc nеng tдi xuЇng chЯa kнch hoХt.");
	define("DOWNLOAD_EXPIRED", "Khoдng th¶i gian tдi xuЇng bё hЄt hХn.");
	define("DOWNLOAD_LIMITED", "BХn рг tдi quб sЇ lҐn quy рёnh.");
	define("DOWNLOAD_PATH_ERROR", "РЯ¶ng dзn khфng th¬ tмm th¤y.");
	define("DOWNLOAD_RELEASE_ERROR", "Phiкn bдn phбt hаnh khфng th¬ tмm th¤y.");
	define("DOWNLOAD_USER_ERROR", "Chп cу thаnh viкn реng kэ mѕi cу th¬ tдi xuЇng file nаy.");
	define("ACTIVATION_OPTIONS_MSG", "Kнch hoХt nhжng tщy chчn");
	define("ACTIVATION_MAX_NUMBER_MSG", "SЇ tЇi рa cьa nhжng lҐn kнch hoХt");
	define("DOWNLOAD_OPTIONS_MSG", "Tдi xuЇng/ Tщy chчn phҐn m«m");
	define("DOWNLOADABLE_MSG", "Tдi xuЇng (phҐn m«m)");
	define("DOWNLOADABLE_DESC", "р¬ tдi xuЇng sдn ph¦m bХn cу th¬ mф tд rх 'Khoдng th¶i gian tдi xuЇng', 'РЯ¶ng dзn р¬ tдi xuЇng' vа 'Kнch hoХt nhжng tщy chчn'");
	define("DOWNLOAD_PERIOD_MSG", "Khoдng th¶i gian tдi xuЇng");
	define("DOWNLOAD_PATH_MSG", "РЯ¶ng dзn р¬ tдi t®p");
	define("DOWNLOAD_PATH_DESC", "bХn cу th¬ thкm nhi«u рЯ¶ng dзn р¬ phвn chia");
	define("UPLOAD_SELECT_MSG", "Chчn t®p р¬ tдi lкn vа nh¤n nъt {button_name}.");
	define("UPLOADED_FILE_MSG", "T®p <b>{filename}</b> vЧa рЯюc tдi lкn.");
	define("UPLOAD_SELECT_ERROR", "Xin vui lтng chчn t®p рҐu tiкn.");
	define("UPLOAD_IMAGE_ERROR", "Chп t®p dХng hмnh дnh mѕi рЯюc cho phйp.");
	define("UPLOAD_FORMAT_ERROR", "LoХi t®p nаy khфng рЯюc phйp.");
	define("UPLOAD_SIZE_ERROR", "Khфng cho phйp t®p cу dung lЯюng lѕn hЅn {fileszie}");
	define("UPLOAD_DIMENSION_ERROR", "Khфng cho phйp t®p hмnh дnh lѕn hЅn {dimension}.");
	define("UPLOAD_CREATE_ERROR", "H® thЇng khфng th¬ tХo t®p.");
	define("UPLOAD_ACCESS_ERROR", "BХn khфng cу quy«n tдi lкn t®p dж li®u.");
	define("DELETE_FILE_CONFIRM_MSG", "BХn cу muЇn chЎc xуa t®p dж li®u nаy?");
	define("NO_FILES_MSG", "Khфng tмm th¤y t®p dж li®u");
	define("SERIAL_GENERATE_MSG", "TХo sЇ S/N");
	define("SERIAL_DONT_GENERATE_MSG", "Khфng tХo");
	define("SERIAL_RANDOM_GENERATE_MSG", "Khфng tХo sЇ S/N ngзu nhiкn cho sдn ph¦m nаy");
	define("SERIAL_FROM_PREDEFINED_MSG", "L¤y sЇ S/N tЧ danh sбch tХo sЗn.");
	define("SERIAL_PREDEFINED_MSG", "Danh sбch S/N tХo sЗn");
	define("SERIAL_NUMBER_COLUMN", "S/N");
	define("SERIAL_USED_COLUMN", "SШ dшng");
	define("SERIAL_DELETE_COLUMN", "Xуa");
	define("SERIAL_MORE_MSG", "Thкm sЇ S/N");
	define("SERIAL_PERIOD_MSG", "Khoдng th¶i gian hi®u lсc S/N");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Hi¬n thё рi«u khoдn vа рi«u ki®n");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Р¬ tдi xuЇng sдn ph¦m nаy khбch hаng xin рчc vа р°ng э vѕi cбc рi«u khoдn vа рi«u ki®n cьa chъng tфi");
	define("DOWNLOAD_TERMS_MSG", "Cбc рi«u khoдn vа рi«u ki®n");
	define("DOWNLOAD_TERMS_USER_DESC", "Tфi рг рчc vа р°ng э vѕi cбc рi«u khoдn vа рi«u ki®n.");
	define("DOWNLOAD_TERMS_USER_ERROR", "Р¬ tдi xuЇng sдn ph¦m nаy khбch hаng xin рчc vа р°ng э vѕi cбc рi«u khoдn vа рi«u ki®n cьa chъng tфi");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>