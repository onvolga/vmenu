<?php
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/sorter.php");
	include_once($root_folder_path . "includes/navigator.php");
	include_once("./admin_common.php");

	check_admin_security("products_reviews");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","admin_blz_images_list.html");

	include_once("./admin_header.php");
	include_once("./admin_footer.php");
    

    $db->query("SELECT * FROM " . $table_prefix . "blz_items_icons ORDER BY icon_order ASC");
	if ($db->next_record())
	{

		$t->set_var("no_records", "");
		do {

            $t->set_var('icon_path', $db->f('icon_path'));
            $t->set_var('icon_order', $db->f('icon_order'));
            $t->set_var('icon_name', $db->f('icon_name'));


            $t->set_var('icon_id', $db->f('icon_id'));
            $t->parse("records");
		} while ($db->next_record());
	}
	else
	{
		$t->set_var("records", "");
		$t->parse("no_records", false);
	}

	$t->pparse("main"); 
?>