<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "nvt");
	define("BAD_MSG", "1 ster");
	define("POOR_MSG", "2 sterren");
	define("AVERAGE_MSG", "3 sterren");
	define("GOOD_MSG", "4 sterren");
	define("EXCELLENT_MSG", "5 sterren");
	define("REVIEWS_MSG", "Reviews");
	define("NO_REVIEWS_MSG", "Geen reviews gevonden");
	define("WRITE_REVIEW_MSG", "Geef uw mening");
	define("RATE_PRODUCT_MSG", "Beoordeel dit product");
	define("RATE_ARTICLE_MSG", "<font size=\\\"3\\\"><b>Reageer!</b></font>");
	define("NOT_RATED_PRODUCT_MSG", "Product nog niet beoordeeld.");
	define("NOT_RATED_ARTICLE_MSG", "Er zijn nog geen reacties op dit artikel.");
	define("AVERAGE_RATING_MSG", "Gemiddelde beoordeling");
	define("BASED_ON_REVIEWS_MSG", "gebaseerd op {total_votes} beoordelingen");
	define("POSITIVE_REVIEW_MSG", "Positieve beoordeling");
	define("NEGATIVE_REVIEW_MSG", "Negatieve beoordeling");
	define("SEE_ALL_REVIEWS_MSG", "Bekijk alle beoordelingen...");
	define("ALL_REVIEWS_MSG", "Alle beoordelingen");
	define("ONLY_POSITIVE_MSG", "Alleen positieve");
	define("ONLY_NEGATIVE_MSG", "Alleen negatieve");
	define("POSITIVE_REVIEWS_MSG", "Positieve beoordelingen");
	define("NEGATIVE_REVIEWS_MSG", "Negatieve beoordelingen");
	define("SUBMIT_REVIEW_MSG", "Bedankt.<br>Uw opmerkingen zullen worden beoordeeld en, indien goedgekeurd, op onze website worden geplaatst.<br>We behouden ons het recht voor om reviews te weigeren.");
	define("ALREADY_REVIEW_MSG", "U heeft al een review geplaatst.");
	define("RECOMMEND_PRODUCT_MSG", "Zou u dit product<br> aan anderen aanbevelen?");
	define("RECOMMEND_ARTICLE_MSG", "Zou u dit artikel <br> aan anderen aanbevelen?");
	define("RATE_IT_MSG", "Beoordeling");
	define("NAME_ALIAS_MSG", "Uw naam");
	define("SHOW_MSG", "Toon");
	define("FOUND_MSG", "Gevonden");
	define("RATING_MSG", "Gemiddelde beoordeling");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>