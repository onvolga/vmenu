<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Volitelnй");
	define("BAD_MSG", "Zlэ");
	define("POOR_MSG", "Slabэ");
	define("AVERAGE_MSG", "Prщmмrnэ");
	define("GOOD_MSG", "Dobrэ");
	define("EXCELLENT_MSG", "Vэbornэ");
	define("REVIEWS_MSG", "Hodnocenн");
	define("NO_REVIEWS_MSG", "Ћбdnй hodnocenн nenalezeno");
	define("WRITE_REVIEW_MSG", "Napsбt recenzi");
	define("RATE_PRODUCT_MSG", "Hodnotit tenhle produkt");
	define("RATE_ARTICLE_MSG", "Hodnotit tenhle иlбnek");
	define("NOT_RATED_PRODUCT_MSG", "Produkt jeљte nenн hodnocenэ");
	define("NOT_RATED_ARTICLE_MSG", "Иlбnek jeљte nenн hodnocenэ.");
	define("AVERAGE_RATING_MSG", "Prщmмrnй hodnocenн zбkaznнkщ");
	define("BASED_ON_REVIEWS_MSG", "zaloћenй na {total_votes} recenzнch");
	define("POSITIVE_REVIEW_MSG", "Pozitivnн hodnocenн zбkaznнkщ ");
	define("NEGATIVE_REVIEW_MSG", "Negativnн hodnocenн zбkaznнkщ ");
	define("SEE_ALL_REVIEWS_MSG", "Zobrazit vљechny hodnocenн zбkaznнkщ");
	define("ALL_REVIEWS_MSG", "Vљechny hodnocenн");
	define("ONLY_POSITIVE_MSG", "Jenom pozitivnн");
	define("ONLY_NEGATIVE_MSG", "Jenom negativnн");
	define("POSITIVE_REVIEWS_MSG", "Pozitivnн hodnocenн");
	define("NEGATIVE_REVIEWS_MSG", "Negativnн hodnocenн");
	define("SUBMIT_REVIEW_MSG", "Dмkujeme<br>Vaљe pшipomнnky budou zrevidovбny. Pokud pшejdou schvбlenнm, zobrazн se na naљн strбnce.<br>Vyhrazujeme si prбvo nezveшejnit pшнspмvky nesplтujнcн naљe pravidla.");
	define("ALREADY_REVIEW_MSG", "Uћ jste vloћili recenzi.");
	define("RECOMMEND_PRODUCT_MSG", "Doporuиili by jste <br> tento produkt jinэm?");
	define("RECOMMEND_ARTICLE_MSG", "Doporuиili by jste <br> tento иlбnek jinэm?");
	define("RATE_IT_MSG", "Hodnotit tenhle produkt");
	define("NAME_ALIAS_MSG", "Jmйno nebo alias");
	define("SHOW_MSG", "Ukбzat");
	define("FOUND_MSG", "Nalezen");
	define("RATING_MSG", "Prщmмrnй hodnocenн");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>