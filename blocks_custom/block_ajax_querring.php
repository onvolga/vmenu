<?php

	error_reporting(E_ALL);
	ini_set("display_errors", 1);

	include_once('./includes/support_class.php');
	$default_title = "";
	
	$sesUsrType = get_session("session_user_type_id");
	$sesUsr = get_session("session_user_id");

	$support = new SupportUser($sesUsr, $sesUsrType);
	$grant_access = $support->check_acces_security("access_products");


	if($grant_access === false || !$sesUsrType || !$sesUsr){
		echo '{"error":"access denied", "error_code":"1"}';
		exit;
	}

	$category_id = get_param("category_id");

	// check level number
	$start_level = 1; // use 1 by default
	$sql  = " SELECT category_path ";
	$sql .= " FROM " . $table_prefix . "categories ";
	$sql .= " WHERE category_id=" . $db->tosql($category_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$category_path = trim($db->f("category_path"), ",");
		$ids = explode(",", $category_path);
		$start_level = sizeof($ids);
	}
	$categories_ids = VA_Categories::find_all_ids("c.parent_category_id=" . $db->tosql($category_id, INTEGER), VIEW_CATEGORIES_PERM);
	$allowed_categories_ids = VA_Categories::find_all_ids("c.parent_category_id=" . $db->tosql($category_id, INTEGER), VIEW_CATEGORIES_ITEMS_PERM);
	/*$allowed_post_ids = VA_Categories::find_all_ids("c.allowed_post_subcategories=1", ADD_ITEMS_PERM);*/

	if (!$categories_ids) {
		echo '{"error":"no categories", "error_code":"2"}';
		exit;
	};

	$categories = array();
	$sql  = " SELECT c.category_id, c.category_name, c.friendly_url, ";
	$sql .= " c.parent_category_id ";		

	$sql .= ", sc.subs_number ";
		
	$sql .= " FROM " . $table_prefix . "categories c ";

	$sql .= " LEFT JOIN (";
	$sql .= " SELECT parent_category_id, COUNT(*) AS subs_number ";
	$sql .= " FROM " . $table_prefix . "categories ";
	$sql .= " WHERE parent_category_id IN (" . $db->tosql($categories_ids, INTEGERS_LIST) . ") ";
	$sql .= " GROUP BY parent_category_id ";
	$sql .= " ) sc ON sc.parent_category_id=c.category_id ";
	
	//$sql .= " LEFT JOIN va_categories pc on pc.category_id = c.parent_category_id ";
//var_dump($allowed_post_ids);
	$sql .= " WHERE c.category_id IN (" . $db->tosql($categories_ids, INTEGERS_LIST) . ") ";
	$sql .= " ORDER BY c.category_order, c.category_name ";
	$db->query($sql);
	$order_int = 0;
	while ($db->next_record()) {
		$order_int++;
		$cur_category_id = $db->f("category_id");
		$category_name = get_translation($db->f("category_name"));

		$friendly_url = $db->f("friendly_url");

		$subs_number = intval($db->f("subs_number"));
		$allowed_post_subcategories = 0;
/*if(in_array($cur_category_id, $allowed_post_ids)){
$allowed_post_subcategories = 1;
}*/
		$parent_category_id = $db->f("parent_category_id");
		$categories[$order_int]["parent_id"] = $parent_category_id;
		$categories[$order_int]["category_id"] = $cur_category_id;
		$categories[$order_int]["category_name"] = $category_name;
		$categories[$order_int]["friendly_url"] = $friendly_url;
		$categories[$order_int]["subs_number"] = $subs_number;
		$categories[$order_int]["allowed_post"] = $allowed_post_subcategories;
	}

	echo json_encode($categories);
	exit;
	
