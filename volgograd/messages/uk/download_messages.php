<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// повідомлення для скачування
	define("DOWNLOAD_WRONG_PARAM", "Невірно вказано параметр(и) для скачування");
	define("DOWNLOAD_MISS_PARAM", "Не вказано параметр(и) для скачування");
	define("DOWNLOAD_INACTIVE", "Закачка неактивна");
	define("DOWNLOAD_EXPIRED", "Період для скачування закінчився");
	define("DOWNLOAD_LIMITED", "Ви перевищили кількість скачувань");
	define("DOWNLOAD_PATH_ERROR", "Неможливо встановити шлях до продукту");
	define("DOWNLOAD_RELEASE_ERROR", "Неможливо знайти вказану версію продукту");
	define("DOWNLOAD_USER_ERROR", "Тільки зареєстрованні користувачі можуть скачувати цей файл");
	define("ACTIVATION_OPTIONS_MSG", "Опції активації");
	define("ACTIVATION_MAX_NUMBER_MSG", "Максимальная кількість активацій");
	define("DOWNLOAD_OPTIONS_MSG", "Опції програмних продуктів");
	define("DOWNLOADABLE_MSG", "Програмні продукти");
	define("DOWNLOADABLE_DESC", "для програмних продуктів можна вибрати 'Період завантаження', 'Шлях до програмного продукту' та 'Опції активації'");
	define("DOWNLOAD_PERIOD_MSG", "Період для скачування");
	define("DOWNLOAD_PATH_MSG", "Шлях до програмного продукту");
	define("DOWNLOAD_PATH_DESC", "Ви можете додати кілька шляхів, розділивши їх крапкою з комою");
	define("UPLOAD_SELECT_MSG", "Виберіть файл для закачування і натисніть кнопку {button_name}.");
	define("UPLOADED_FILE_MSG", "Файл <b>{filename}</b> був закачаний.");
	define("UPLOAD_SELECT_ERROR", "Будь ласка виберіть спочатку файл.");
	define("UPLOAD_IMAGE_ERROR", "Тільки малюнки дозволяються.");
	define("UPLOAD_FORMAT_ERROR", "Неприпустимий");
	define("UPLOAD_SIZE_ERROR", "Файли більші за {filesize} не дозволяються.");
	define("UPLOAD_DIMENSION_ERROR", "Малюнки більші за {dimension} не дозволяються.");
	define("UPLOAD_CREATE_ERROR", "Система не може створити файл.");
	define("UPLOAD_ACCESS_ERROR", "Ви не маєте прав на завантаження файлів");
	define("DELETE_FILE_CONFIRM_MSG", "Видалити цей файл?");
	define("NO_FILES_MSG", "Файли не знайдено");
	define("SERIAL_GENERATE_MSG", "Генерувати серійний номер");
	define("SERIAL_DONT_GENERATE_MSG", "не генерувати");
	define("SERIAL_RANDOM_GENERATE_MSG", "генерувати серійний номер для програмного продукту");
	define("SERIAL_FROM_PREDEFINED_MSG", "взяти серійник з підготовленого переліку");
	define("SERIAL_PREDEFINED_MSG", "Підготовлені серійні номери");
	define("SERIAL_NUMBER_COLUMN", "Серійний номер");
	define("SERIAL_USED_COLUMN", "Використаний");
	define("SERIAL_DELETE_COLUMN", "Видалити");
	define("SERIAL_MORE_MSG", "Додати серійних номерів?");
	define("SERIAL_PERIOD_MSG", "Період серійного номеру");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Показати Умови і Угоди");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Для скачування продукту користувач повинен прочитати та погодитись з Умовами і Угодами");
	define("DOWNLOAD_TERMS_MSG", "Умови і Угоди");
	define("DOWNLOAD_TERMS_USER_DESC", "Я прочитав Умови і Угоди та згоден з ними");
	define("DOWNLOAD_TERMS_USER_ERROR", "Для скачування продукту Вам необхідно прочитати та погодитись з Умовами і Угодами");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>