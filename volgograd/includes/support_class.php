<?php

/**
 * support items
 */

	class SupportUser
	{

		/**
		 * @var int
		 */
		protected $sessionUserId;

		/**
		 * @var int
		 */
		protected $sessionUserType;

		/**
		 * @var array permissions cache
		 */
		protected $accessData = array();

		public function __construct($sesUserId = null, $sesUserType = null){
			$this->sessionUserId = intval($sesUserId);
			$this->sessionUserType = intval($sesUserType);
		}

		/**
		 * @return bool is permission granted
		 */
		public function check_acces_security($setting_name = ""){

			global $db, $settings, $table_prefix;
			
			$is_accessible = false;

			if ($setting_name) {

				if(array_key_exists($setting_name, $this->accessData)){
					$is_accessible = $this->accessData[$setting_name];
				}
				else{			
					$sql  = " SELECT setting_value ";
					$sql .= " FROM " . $table_prefix . "user_types_settings ";
					$sql .= " WHERE type_id=" . $db->tosql($this->sessionUserType, INTEGER);
					$sql .= " AND setting_name=" . $db->tosql($setting_name, TEXT);
					$allow_access = get_db_value($sql);
					
					if ($allow_access) {
						$this->accessData[$setting_name] = true;
						$is_accessible = true;
					}
					else{
						$this->accessData[$setting_name] = false;
					}

				}
			}

			return $is_accessible;
		}
	}