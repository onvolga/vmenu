<?php
$is_ajax = get_param("is_ajax");
$id = intval(get_param("id"));
$html_template = get_setting_value($block, "html_template", "block_random_review.html");
mb_internal_encoding("UTF-8");       
$t->set_file("block_body", $html_template);
if(!$max = get_session('max_id')){
    $max = get_db_value("SELECT MAX(review_id) AS max_id FROM va_blz_reviews");
    $min = get_db_value("SELECT MIN(review_id) AS max_id FROM va_blz_reviews");
    set_session('max_id', $max);
    set_session('min_id', $min);
}
$stmt = "   SELECT * FROM va_blz_reviews
            ORDER BY RAND()
            LIMIT 1
        ";

if($is_ajax && $id){
    if($id == $max){
        $id = get_session('min_id');
        $stmt = "SELECT * FROM va_blz_reviews WHERE review_id = " . $id;
    }
    else{
        $stmt = "   SELECT * FROM va_blz_reviews
                    WHERE review_id > " . $id . " 
                    ORDER BY review_id LIMIT 1
                ";
    }
}

$db->query($stmt);
	if ($db->next_record()) { 
        $ans = array();
        $ans['author'] = $db->f('review_author');
        $ans['comment'] = $db->f('review');
        $ans['id'] = $db->f('review_id');
        if(mb_strlen($ans['comment']) > 105){
            $ans['comment'] =  mb_substr($ans['comment'], 0, 102) . '...';
        }
        if($is_ajax == 1){
            echo json_encode($ans);
            exit;
        }
        else{
            $t->set_var('comment', $ans['comment']);
            $t->set_var('author', $ans['author']);
            $t->set_var('id', $ans['id']);
        }
    }
$block_parsed = true;