<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  cart_add.php                                             ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	$type = "";
	include_once("./includes/common.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	
	// set headers for block
	header("Pragma: no-cache");
	header("Expires: 0");
	header("Cache-Control: no-cache, must-revalidate");
	header("Content-Type: text/html; charset=" . CHARSET);

	if (strlen($sc_errors)) {
		echo "errors=".$sc_errors;
	} else if (strlen($sc_message)) {
		echo "success=".$sc_message;
	} else {
		echo "errors=".ERRORS_MSG;
	}

?>
