var userAgent = navigator.userAgent.toLowerCase();
var isIE = ((userAgent.indexOf("msie") != -1) && (userAgent.indexOf("opera") == -1) && (userAgent.indexOf("webtv") == -1));

var tid = new Array();
var lastMenu = new Array();
var hiddenObjects = new Array();

function show(menuName, menuType) 
{
	var actMenu = new Array();
	var subName = "";
	var subMenus = menuName.split("_");
	var addWidth = false; var addHeight = true;
	for (var m = 0; m < subMenus.length; m++) {
		if (m == 0) {
			subName = subMenus[m];
		} else {
			subName += "_" + subMenus[m];
			addWidth = true; var addHeight = false;
		}
		var parentMenuName = "m_" + subName;
		var subMenuName = "sm_" + subName;
		if (menuType == "2" || menuType == "secondary") {
			parentMenuName = "secondary_" + subName;
			subMenuName = "secondary_ddm_" + subName;
		}
		var parentMenu = document.getElementById(parentMenuName);
		var subMenu = document.getElementById(subMenuName);

		if (subMenu) {
			actMenu[subMenuName] = 1;
			//subMenu.style.top = findPosY(parentMenu, addHeight) + "px";
			//subMenu.style.left = findPosX(parentMenu, addWidth) + "px";
			subMenu.style.display='block';
			hideSelectBoxes(subMenuName);
			if (tid[subName]) {
				clearTimeout(tid[subName]);
				tid[subName] = "";
			}
		}
	}

	for (menuName in lastMenu) {
		if (!actMenu[menuName]) {
			var menuObj = document.getElementById(menuName);
			menuObj.style.display = "none";
			showSelectBoxes(menuName);
		}
	}
	lastMenu = actMenu;

}

function hide(menuName, menuType)
{
	var subMenus = menuName.split("_");
	for (var m = 0; m < subMenus.length; m++) {
		if (m == 0) {
			subName = subMenus[m];
		} else {
			subName += "_" + subMenus[m];
		}
		var blockName = "sm_" + subName;
		if (menuType == "2" || menuType == "secondary") {
			blockName = "secondary_ddm_" + subName;
		}
		var blockMenu = document.getElementById(blockName);
		if (blockMenu) {
			tid[subName] = setTimeout("hideMenu('" + subName + "', '" + menuType + "')", 1500);
		}
	}
}

function hideMenu(menuName, menuType)
{
	var subMenuName = "sm_" + menuName;
	if (menuType == "2" || menuType == "secondary") {
		subMenuName = "secondary_ddm_" + menuName;
	}
	var subMenu = document.getElementById(subMenuName);
	if (subMenu) {
		subMenu.style.display='none';
		showSelectBoxes(subMenuName);
	}

}

function findPosX(obj, addWidth)
{
	var curleft = 0;
	if (addWidth) {
		curleft += obj.offsetWidth;
	}
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function findPosY(obj, addHeight)
{
	var curtop = 0;
	if (addHeight) {
		curtop += obj.offsetHeight;
	}
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	} else if (obj.y) {
		curtop += obj.y;
	}
	return curtop;
}

function getMousePos(e) 
{
	var posX = 0;
	var posY = 0;
	if (!e) var e = window.event;
	if (e.pageX || e.pageY) {
		posX = e.pageX;
		posY = e.pageY;
	}	else if (e.clientX || e.clientY) 	{
		posX = e.clientX + document.body.scrollLeft
			+ document.documentElement.scrollLeft;
		posY = e.clientY + document.body.scrollTop
			+ document.documentElement.scrollTop;
	}
	var mousePos = new Array(posX, posY);
	return mousePos;
}

function getPageSize()
{
  var w = 0, h = 0;
  if (window.innerWidth) { //Non-IE
    w = window.innerWidth;
    h = window.innerHeight;
  } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
    //IE 6+ in 'standards compliant mode'
    w = document.documentElement.clientWidth;
    h = document.documentElement.clientHeight;
  } else if ( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    w = document.body.clientWidth;
    h = document.body.clientHeight;
  }

	var pageSize= new Array(w, h);    
	return pageSize;
}

function getPageSizeWithScroll()
{
	var xWithScroll = 0; var yWithScroll = 0; 
	if (window.innerHeight && window.scrollMaxY) { // Firefox         
		yWithScroll = window.innerHeight + window.scrollMaxY;         
		xWithScroll = window.innerWidth + window.scrollMaxX;     
	} else if (document.body.scrollHeight > document.body.offsetHeight) { // all but Explorer Mac         
		yWithScroll = document.body.scrollHeight;         
		xWithScroll = document.body.scrollWidth;     
	} else { // works in Explorer 6 Strict, Mozilla (not FF) and Safari         
		yWithScroll = document.body.offsetHeight;         
		xWithScroll = document.body.offsetWidth;       
	}     
	var arrayPageSizeWithScroll = new Array(xWithScroll,yWithScroll);    
	return arrayPageSizeWithScroll; 
} 

function getScroll()
{
	var w = window.pageXOffset ||
		document.body.scrollLeft ||
		document.documentElement.scrollLeft;
	var h = window.pageYOffset ||
		document.body.scrollTop ||
		document.documentElement.scrollTop;
	var arrayScroll = new Array(w, h);    
	return arrayScroll;
}

function showSelectBoxes(objId) {
	// delete hidden objects for this menu
	delete hiddenObjects[objId];
	var obj = document.getElementById(objId);
	var menuObjects = obj.overlapObjects;
	if (menuObjects && menuObjects.length > 0) {
		for (var i = 0; i < menuObjects.length; i++) {
			var objControl = menuObjects[i];
			// check if object control not present in hidden objects for new popup menus
			var objExists = false;
			for (id in hiddenObjects) {
				var checkObjects = hiddenObjects[id];
				for (var ob = 0; ob < checkObjects.length; ob++) {
					if (objControl == checkObjects[ob]) {
						objExists = true;
						break;
					}
				}
			}
			if (!objExists) {
				objControl.style.visibility = "visible";
			}
		}
	}
	obj.overlapObjects = null;
}

function hideSelectBoxes(objId, objExclude) {

	var obj = document.getElementById(objId);

	var x = findPosX(obj, false);
	var y = findPosY(obj, false);
	var w = obj.offsetWidth;
	var h = obj.offsetHeight;

	// create a new Array for overlapping Objects we will hide
	var overlapObjects = new Array();

	var objectsNames = new Array("object", "iframe");
	if (isIE) {
		// hide select controls for IE 
		objectsNames[2] = "select";
	}

	for (n = 0; n < objectsNames.length; n++) {
		var objectName = objectsNames[n];
		var objects = document.getElementsByTagName(objectName);

		for (i = 0; i < objects.length; i++) {
			var objectControl = objects[i];
	
			var objName = objectControl.name;
			var isExclude = false;
			if (objExclude && objExclude.length) {
				for (var ex = 0; ex < objExclude.length; ex++) {
					if (objName == objExclude[ex]) {
						isExclude = true;
					}
				}
			}
			// check if object was hidden before
			var objExists = false;
			if (objectControl.style.visibility == "hidden") {
				for (id in hiddenObjects) {
					var checkObjects = hiddenObjects[id];
					for (var ob = 0; ob < checkObjects.length; ob++) {
						if (objectControl == checkObjects[ob]) {
							objExists = true;
							break;
						}
					}
				}
			}
			if (isExclude == true || (objectControl.style.visibility == "hidden" && !objExists)) {
				// don't hide object hidden by user
				continue;
			}
	
			var ox = findPosX(objectControl, false);
			var oy = findPosY(objectControl, false);
			var ow = objectControl.offsetWidth;
			var oh = objectControl.offsetHeight;
	
			if (ox > (x + w) || (ox + ow) < x) {
				continue;
			}
			if (oy > (y + h) || (oy + oh) < y) {
				continue;
			}

			overlapObjects[overlapObjects.length] = objectControl;
			objectControl.style.visibility = "hidden";
		}
	}

	// save overlapObjects array in global variables
	obj.overlapObjects = overlapObjects;
	hiddenObjects[objId] = overlapObjects;
}

// function to show more filter options
function popupBlock(linkName, blockName, imageName)
{                              	
	var linkObj = document.getElementById(linkName);
	var blockObj = document.getElementById(blockName);
	var imageObj = document.getElementById(imageName);

	if (blockObj.style.display == "none" || blockObj.style.display == "") {
		//blockObj.style.left = findPosX(linkObj, 0) + "px";
		//blockObj.style.top = findPosY(linkObj, 1) + "px";
		blockObj.style.display = "block";
		hideSelectBoxes(blockName, "");
		if (imageObj) {
			imageObj.src = "images/icons/minus_small.gif";
		}
	} else {
		blockObj.style.display = "none";
		showSelectBoxes(blockName);
		if (imageObj) {
			imageObj.src = "images/icons/plus_small.gif";
		}
	}
}

function openPopup(pageUrl, width, height)
{
	var scrollbars = "yes";
	var popupWin = window.open (pageUrl, 'popupWin', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=' + scrollbars + ',resizable=yes,width=' + width + ',height=' + height);
	popupWin.focus();
	return false;
}