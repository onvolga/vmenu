<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  messages.php                                             ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	define("CHARSET", "windows-1256");
	// date messages
	define("YEAR_MSG", "Уде");
	define("YEARS_QTY_MSG", "{quantity} years");
	define("MONTH_MSG", "ФеС");
	define("MONTHS_QTY_MSG", "{quantity} months");
	define("DAY_MSG", "нжг");
	define("DAYS_MSG", "ГнЗг");
	define("DAYS_QTY_MSG", "{quantity} days");
	define("HOUR_MSG", "УЗЪе");
	define("HOURS_QTY_MSG", "{quantity} hours");
	define("MINUTE_MSG", "ПЮнЮе");
	define("MINUTES_QTY_MSG", "{quantity} minutes");
	define("SECOND_MSG", "ЛЗдне");
	define("SECONDS_QTY_MSG", "{quantity} seconds");
	define("WEEK_MSG", "Week");
	define("WEEKS_QTY_MSG", "{quantity} weeks");
	define("TODAY_MSG", "Today");
	define("YESTERDAY_MSG", "Yesterday");
	define("LAST_7DAYS_MSG", "Last 7 Days");
	define("THIS_MONTH_MSG", "This Month");
	define("LAST_MONTH_MSG", "Last Month");
	define("THIS_QUARTER_MSG", "This Quarter");
	define("THIS_YEAR_MSG", "This Year");

	// months
	define("JANUARY", "ндЗнС");
	define("FEBRUARY", "ЭИСЗнС");
	define("MARCH", "гЗСУ");
	define("APRIL", "ГИСнб");
	define("MAY", "гЗнж");
	define("JUNE", "нжднж");
	define("JULY", "нжбнж");
	define("AUGUST", "ГЫУШУ");
	define("SEPTEMBER", "УнИКгИС");
	define("OCTOBER", "ГЯКжИС");
	define("NOVEMBER", "джЭгИС");
	define("DECEMBER", "ПнУгИС");

	define("JANUARY_SHORT", "ндЗнС");
	define("FEBRUARY_SHORT", "ЭИСЗнС");
	define("MARCH_SHORT", "гЗСУ");
	define("APRIL_SHORT", "ГИСнб");
	define("MAY_SHORT", "гЗнж");
	define("JUNE_SHORT", "нжИнж");
	define("JULY_SHORT", "нжбнж");
	define("AUGUST_SHORT", "ГЫУШУ");
	define("SEPTEMBER_SHORT", "УнИКгИС");
	define("OCTOBER_SHORT", "ГЯКжИС");
	define("NOVEMBER_SHORT", "джЭгИС");
	define("DECEMBER_SHORT", "ПнУгИС");

	// weekdays
	define("SUNDAY", "ЗбГНП");
	define("MONDAY", "ЗбЕЛднд");
	define("TUESDAY", "ЗбЛбЛЗБ");
	define("WEDNESDAY", "ЗбГСИЪЗБ");
	define("THURSDAY", "ЗбОгнУ");
	define("FRIDAY", "ЗбМгЪе");
	define("SATURDAY", "ЗбУИК");

	define("SUNDAY_SHORT", "ЗбГНП");
	define("MONDAY_SHORT", "ЗбЕЛднд");
	define("TUESDAY_SHORT", "ЗбЛбЛЗБ");
	define("WEDNESDAY_SHORT", "ЗбГСИЪЗБ");
	define("THURSDAY_SHORT", "ЗбОгнУ");
	define("FRIDAY_SHORT", "ЗбМгЪе");
	define("SATURDAY_SHORT", "ЗбУИК");

	// validation messages
	define("REQUIRED_MESSAGE", "<b>{field_name}</b> гШбжИ");
	define("UNIQUE_MESSAGE", " гжМжПе гУИЮЗр Эн УМбЗКдЗ <b>{field_name}</b> ЗбЮнге Эн ЗбНЮб");
	define("VALIDATION_MESSAGE", "{field_name} ЭФб ЗбКГЯнП ббНЮб");
	define("MATCHED_MESSAGE", "<b>{field_one}</b> ж <b>{field_two}</b> бЗ нКШЗИЮЗд");
	define("INSERT_ALLOWED_ERROR", "Sorry, but insert operation is not allowed for you");
	define("UPDATE_ALLOWED_ERROR", "Sorry, but update operation is not allowed for you");
	define("DELETE_ALLOWED_ERROR", "Sorry, but delete operation is not allowed for you");
	define("ALPHANUMERIC_ALLOWED_ERROR", "Only alpha-numeric characters, hyphen and underscore are allowed for field <b>{field_name}</b>");

	define("INCORRECT_DATE_MESSAGE", "<b>{field_name}</b> нНКжн Ъбм Юнге КЗСнОне ОЗШЖе, ЗбСМЗБ ЗУКОПЗг ЗбКЮжнг");
	define("INCORRECT_MASK_MESSAGE", "<b>{field_name}</b> бЗ КФЗИе ЗбХнЫе ббдЩЗг, ЗбСМЗБ ЗУКОПЗг ЗбХнЫе ЗбКЗбне '<b>{field_mask}</b>'");
	define("INCORRECT_EMAIL_MESSAGE", "{field_name} ИСнП ЗбЯКСждн ОЗШЖ Эн ");
	define("INCORRECT_VALUE_MESSAGE", "<b>{field_name}</b> Юнге ОЗШЖе Эн");

	define("MIN_VALUE_MESSAGE", "{min_value} нМИ ГбЗ КЯжд ЗЮб гд <b>{field_name}</b> ЗбЮнге Эн ЗбНЮб");
	define("MAX_VALUE_MESSAGE", "{max_value} нМИ ГбЗ КЯжд ЗЯЛС гд <b>{field_name}</b> ЗбЮнге Эн ЗбНЮб");
	define("MIN_LENGTH_MESSAGE", "{min_length} нМИ ГбЗ нЯжд ЗЮб гд <b>{field_name}</b> ЗбШжб Эн ЗбНЮб");
	define("MAX_LENGTH_MESSAGE", "{max_length} нМИ ГбЗ КЯжд ЗЯЛС гд <b>{field_name}</b> ЗбЮнге Эн ЗбНЮб");

	define("FILE_PERMISSION_MESSAGE", "ЗбСМЗБ ЗЪШЗЖе ЗбКХСнН ЮИб ЗбЕУКгСЗС .<b>'{file_name}'</b> бЗ нжМП КХСнН ЯКЗИе ббгбЭ");
	define("FOLDER_PERMISSION_MESSAGE", "ЗбСМЗБ КЫннС КХСнН ЗбгМббП ЮИб ЗбЗУКгСЗС .<b>'{folder_name}'</b> бЗ нжМП КХСнН ЯКЗИе ббгМбП");
	define("INVALID_EMAIL_MSG", ".ИСнПЯ ЗбЕбЯКСждн ЫнС ХНнН");
	define("DATABASE_ERROR_MSG", "ОШГ Эн ЮЗЪПЙ ЗбИнЗдЗК");
	define("BLACK_IP_MSG", "This action is not permitted from your host.");
	define("BANNED_CONTENT_MSG", "Sorry, the provided content contains illegal statement.");
	define("ERRORS_MSG", "ОШГ");
	define("REGISTERED_ACCESS_MSG", "Only registered users could access this option.");
	define("SELECT_FROM_LIST_MSG", "ЗОКС бЗЖНЙ гцд");

	// titles 
	define("TOP_RATED_TITLE", "ГЯЛС КХжнК");
	define("TOP_VIEWED_TITLE", "ГЯЛС гФЗеПе");
	define("RECENTLY_VIEWED_TITLE", "КгК гФЗеПКе НПнЛЗр");
	define("HOT_TITLE", "УЗОд");
	define("LATEST_TITLE", "ЗбГНПЛ");
	define("CONTENT_TITLE", "ЗбгНКжм");
	define("RELATED_TITLE", "гКЪбЮ");
	define("SEARCH_TITLE", "ЕИНЛ");
	define("ADVANCED_SEARCH_TITLE", "ИНЛ гКЮПг");
	define("LOGIN_TITLE", "ПОжб гУМбнд");
	define("CATEGORIES_TITLE", "ЗбГЮУЗг");
	define("MANUFACTURERS_TITLE", "ЗбХЗдЪ");
	define("SPECIAL_OFFER_TITLE", "ЪСжЦ ОЗХе");
	define("NEWS_TITLE", "ГОИЗС");
	define("EVENTS_TITLE", "ГНПЗЛ");
	define("PROFILE_TITLE", "ЗбгбЭ");
	define("USER_HOME_TITLE", "СЖнУне ЗбЪЦж");
	define("DOWNLOAD_TITLE", "ЗбКНгнб");
	define("FAQ_TITLE", "ГУЖбе гКЯССе");
	define("POLL_TITLE", "КХжнК");
	define("HOME_PAGE_TITLE", "ЗбСЖнУне");
	define("CURRENCY_TITLE", "ЗбЪгбе");
	define("SUBSCRIBE_TITLE", "ЗбКУМнб");
	define("UNSUBSCRIBE_TITLE", "ЕбЫЗБ ЗбКУМнб");
	define("UPLOAD_TITLE", "СЭЪ");
	define("ADS_TITLE", "ЗбЕЪбЗдЗК");
	define("ADS_COMPARE_TITLE", "гЮЗСдЙ ЗбЕЪбЗдЗК");
	define("ADS_SELLERS_TITLE", "ЗбИЗЖЪжд");
	define("AD_REQUEST_TITLE", "ЮПг ЪСЦЗр, ЗУГб ЗбИЗЖЪ");
	define("LANGUAGE_TITLE", "ЗббЫЗК");
	define("MERCHANTS_TITLE", "Merchants");
	define("PREVIEW_TITLE", "Preview");
	define("ARTICLES_TITLE", "Articles");
	define("SITE_MAP_TITLE", "Site Map");
	define("LAYOUTS_TITLE", "Layouts");

	// menu items
	define("MENU_ABOUT", "гд дНд");
	define("MENU_ACCOUNT", "НУЗИЯ");
	define("MENU_BASKET", "УбКЯ");
	define("MENU_CONTACT", "ЗКХб ИдЗ");
	define("MENU_DOCUMENTATION", "ЗбжЛЗЖЮ");
	define("MENU_DOWNLOADS", "ЗбКНгнбЗК");
	define("MENU_EVENTS", "ЗбГНПЗЛ");
	define("MENU_FAQ", "ГУЖбе гКЯССе");
	define("MENU_FORUM", "ЗбгдКПм");
	define("MENU_HELP", "ЗбгУЗЪПе");
	define("MENU_HOME", "ЗбСЖнУне");
	define("MENU_HOW", "ЯнЭ ККУжЮ");
	define("MENU_MEMBERS", "ЗбГЪЦЗБ");
	define("MENU_MYPROFILE", "гбЭн");
	define("MENU_NEWS", "ЗОИЗС");
	define("MENU_PRIVACY", "ЗбОХжХне");
	define("MENU_PRODUCTS", "ЗбгдКМЗК");
	define("MENU_REGISTRATION", "ЗбКУМнб");
	define("MENU_SHIPPING", "ЗбФНд");
	define("MENU_SIGNIN", "КУМнб ЗбПОжб");
	define("MENU_SIGNOUT", "КУМнб ЗбОСжМ");
	define("MENU_SUPPORT", "ЗбПЪг ЗбЭдн");
	define("MENU_USERHOME", "СЖнУне ЗбЪЦж");
	define("MENU_ADS", "ЗбЕЪбЗдЗК ЗбКМЗСне");
	define("MENU_ADMIN", "ЗбЕПЗСе");
	define("MENU_KNOWLEDGE", "Knowledge Base");

	// main terms
	define("NO_MSG", "бЗ");
	define("YES_MSG", "дЪг");
	define("NOT_AVAILABLE_MSG", "ЫнС гКжЭС");
	define("MORE_MSG", "...ЗбгТнП");
	define("READ_MORE_MSG", "...ЗЮСГ ЗбгТнП");
	define("CLICK_HERE_MSG", "ЗЦЫШ едЗ");
	define("ENTER_YOUR_MSG", "ГПОб");
	define("CHOOSE_A_MSG", "ЕОКС");
	define("PLEASE_CHOOSE_MSG", "ЗбСМЗБ ЕОКнЗС");
	define("SELECT_MSG", "ЕОКС");
	define("DATE_FORMAT_MSG", "<b>{date_format}</b> ЗУКОПг ЗбШСнЮе ЗбКЗбне");
	define("NEXT_PAGE_MSG", "ЗбКЗбн");
	define("PREV_PAGE_MSG", "ЗбУЗИЮ");
	define("FIRST_PAGE_MSG", "ЗбГжб");
	define("LAST_PAGE_MSG", "ЗбГОнС");
	define("OF_PAGE_MSG", "гд");
	define("TOP_CATEGORY_MSG", "ЗбГЭЦб");
	define("SEARCH_IN_CURRENT_MSG", "ЗбЮУг ЗбНЗбн");
	define("SEARCH_IN_ALL_MSG", "Яб ЗбГЮУЗг");
	define("FOUND_IN_MSG", "жМтцПу Эн");
	define("TOTAL_VIEWS_MSG", "ЗбгФЗеПЗК");
	define("VOTES_MSG", "ЗбГХжЗК");
	define("TOTAL_VOTES_MSG", "Total Votes");
	define("TOTAL_POINTS_MSG", "Total Points");
	define("VIEW_RESULTS_MSG", "ЪСЦ ЗбдКЗЖМ");
	define("PREVIOUS_POLLS_MSG", "ЗбКХжнКЗК ЗбУЗИЮе");
	define("TOTAL_MSG", "ЗбЯб");
	define("CLOSED_MSG", "гЫбЮ");
	define("CLOSE_WINDOW_MSG", "ГЫбЮ ЗбдЗЭРе");
	define("ASTERISK_MSG", "ЗбгЪбг (*) - гШбжИ");
	define("PROVIDE_INFO_MSG", "{button_name}' ЗбСМЗБ ЗЪШЗБ ЗбгЪбжгЗК Эн ЗбНЮжб Збгбжде ИЗбГНгС Лг ЗЦЫШ Ъбм");
	define("FOUND_ARTICLES_MSG", "<b>{search_string}</b>'  гФЗИее бЯбгЙ ЗбИНЛ <b>{found_records}</b> Кг ЗнМЗП");
	define("NO_ARTICLE_MSG", "бЗ нжМП гжЦжЪ нНгб ЗбСЮг ЗбгПОб");
	define("NO_ARTICLES_MSG", "бг нКг ЗнМЗП гжЗЦнЪ");
	define("NOTES_MSG", "гбНжЩЗК");
	define("KEYWORDS_MSG", "ЯбгЗК ЗбИНЛ");
	define("LINK_URL_MSG", "ЗбСЗИШ");
	define("DOWNLOAD_URL_MSG", "КНгнб");
	define("SUBSCRIBE_FORM_MSG", ". '{button_name}' ббЕФКСЗЯ Эн ЮЗЖгКдЗ ЗбИСнПне ЗбСМЗБ ЗПОЗб ИСнПЯ ЗбЕбЯКСждн Лг ЗбЦЫШ Ъбм");
	define("UNSUBSCRIBE_FORM_MSG", ". '{button_name}' ЗбСМЗБ ЗПОЗб ИСнПЯ ЗбЕбЯКСждн Лг ЗЦЫШ");
	define("SUBSCRIBE_LINK_MSG", "ЗбЕФКСЗЯ");
	define("UNSUBSCRIBE_LINK_MSG", "ЕбЫЗБ ЗбЕФКСЗЯ");
	define("SUBSCRIBED_MSG", "гИСжЯ! Кг ЗбЗФКСЗЯ Эн ЮЗЖгКдЗ ЗбИСнПне ИдМЗН");
	define("ALREADY_SUBSCRIBED_MSG", "бЮП ЮгК гУИЮЗр ИЗббЕФКСЗЯ Эн ЮЗЖгКдЗ ЗбИСнПне. ФЯСЗр бЯ");
	define("UNSUBSCRIBED_MSG", "Кг ЕбЫЗБ ЗФКСЗЯЯ Эн ЮЗЖгКдЗ ЗбИСнПне ИдМЗН. ФЯСЗр бЯ");
	define("UNSUBSCRIBED_ERROR_MSG", ".дГУЭ, жбЯд бг дМП Эн УМбЗКдЗ ИСнПЗ гФЗИеЗ бИСнПЯ, ЮП КЯжд ЗбЫнК ЗФКСЗЯЯ УЗИЮЗр");
	define("FORGOT_PASSWORD_MSG", "дУнК СЮгЯ ЗбУСнї");
	define("FORGOT_PASSWORD_DESC", ":ЗбСМЗБ ЕПОЗб ИСнПЯ ЗбЕбЯКСждн ЗббРн ЗУКОПгКе Эн ЗбКУМнб");
	define("FORGOT_EMAIL_ERROR_MSG", ".дГУЭ, жбЯд бг дМП Эн УМбЗКдЗ ИСнПЗ гФЗИеЗ бИСнПЯ");
	define("FORGOT_EMAIL_SENT_MSG", ".гЪбжгЗК КУМнб ПОжбЯ Кг ЗСУЗбеЗ Збм ИСнПЯ");
	define("RESET_PASSWORD_REQUIRE_MSG", "Some required parameters were missed.");
	define("RESET_PASSWORD_PARAMS_MSG", "The parameters you supplied does not match any in the database.");
	define("RESET_PASSWORD_EXPIRY_MSG", "The reset code you supplied has expired. Please request a new code to reset your password.");
	define("RESET_PASSWORD_SAVED_MSG", "Your new password was successfully saved.");
	define("PRINTER_FRIENDLY_MSG", "ЗбШИЗЪе");
	define("PRINT_PAGE_MSG", "ЗХИЪ еРе ЗбХЭНе");
	define("ATTACHMENTS_MSG", "Attachments");
	define("VIEW_DETAILS_MSG", "View Details");
	define("HTML_MSG", "HTML");
	define("PLAIN_TEXT_MSG", "Plain Text");
	define("META_DATA_MSG", "Meta Data");
	define("META_TITLE_MSG", "Page Title");
	define("META_KEYWORDS_MSG", "Meta Keywords");
	define("META_DESCRIPTION_MSG", "Meta Description");
	define("FRIENDLY_URL_MSG", "Friendly URL");
	define("IMAGES_MSG", "ЗбХжС");
	define("IMAGE_MSG", "ЗбХжСе");
	define("IMAGE_TINY_MSG", "Tiny Image");
	define("IMAGE_TINY_ALT_MSG", "Tiny Image Alt");
	define("IMAGE_SMALL_MSG", "ХжСе ХЫнС");
	define("IMAGE_SMALL_DESC", "shown on list page");
	define("IMAGE_SMALL_ALT_MSG", "Small Image Alt");
	define("IMAGE_LARGE_MSG", "ХжСе ЯИнСе");
	define("IMAGE_LARGE_DESC", "shown on details page");
	define("IMAGE_LARGE_ALT_MSG", "Large Image Alt");
	define("IMAGE_SUPER_MSG", "Super-Sized Image");
	define("IMAGE_SUPER_DESC", "image popup in the new window");
	define("IMAGE_POSITION_MSG", "Image Position");
	define("UPLOAD_IMAGE_MSG", "ЗСЭЪ ЗбХжСе");
	define("UPLOAD_FILE_MSG", "Upload File");
	define("SELECT_IMAGE_MSG", "Select Image");
	define("SELECT_FILE_MSG", "Select File");
	define("SHOW_BELOW_PRODUCT_IMAGE_MSG", "show image below large product image");
	define("SHOW_IN_SEPARATE_SECTION_MSG", "show image in separate images section");
	define("IS_APPROVED_MSG", "Approved");
	define("NOT_APPROVED_MSG", "Not Approved");
	define("IS_ACTIVE_MSG", "Is Active");
	define("CATEGORY_MSG", "ЗбКХднЭ");
	define("SELECT_CATEGORY_MSG", "ЗОКС КХднЭЗр");
	define("DESCRIPTION_MSG", "ЗбФСН");
	define("SHORT_DESCRIPTION_MSG", "Short Description");
	define("FULL_DESCRIPTION_MSG", "Full Description");
	define("HIGHLIGHTS_MSG", "Highlights");
	define("SPECIAL_OFFER_MSG", "ЪСжЦ ОЗХе");
	define("ARTICLE_MSG", "Article");
	define("OTHER_MSG", "Other");
	define("WIDTH_MSG", "Width");
	define("HEIGHT_MSG", "Height");
	define("LENGTH_MSG", "Length");
	define("WEIGHT_MSG", "ЗбжТд");
	define("QUANTITY_MSG", "ЗбЯгне");
	define("CALENDAR_MSG", "Calendar");
	define("FROM_DATE_MSG", "From Date");
	define("TO_DATE_MSG", "To Date");
	define("TIME_PERIOD_MSG", "Time Period");
	define("GROUP_BY_MSG", "Group By");
	define("BIRTHDAY_MSG", "Birthday");
	define("BIRTH_DATE_MSG", "Birth Date");
	define("BIRTH_YEAR_MSG", "Birth Year");
	define("BIRTH_MONTH_MSG", "Birth Month");
	define("BIRTH_DAY_MSG", "Birth Day");
	define("STEP_NUMBER_MSG", "Step {current_step} of {total_steps}");
	define("WHERE_STATUS_IS_MSG", "Where status is");
	define("ID_MSG", "ID");
	define("QTY_MSG", "Qty");
	define("TYPE_MSG", "ЗбджЪ");
	define("NAME_MSG", "Name");
	define("TITLE_MSG", "ЗбЕУг");
	define("DEFAULT_MSG", "Default");
	define("OPTIONS_MSG", "Options");
	define("EDIT_MSG", "Edit");
	define("CONFIRM_DELETE_MSG", "Would you like to delete this {record_name}?");
	define("DESC_MSG", "Desc");
	define("ASC_MSG", "Asc");
	define("ACTIVE_MSG", "Active");
	define("INACTIVE_MSG", "Inactive");
	define("EXPIRED_MSG", "гдКен");
	define("EMOTICONS_MSG", "Emoticons");
	define("EMOTION_ICONS_MSG", "Emotion Icons (smiles)");
	define("VIEW_MORE_EMOTICONS_MSG", "View more Emoticons");
	define("SITE_NAME_MSG", "Site Name");
	define("SITE_URL_MSG", "СЗИШ ЗбгжЮЪ");
	define("SORT_ORDER_MSG", "Sort Order");
	define("NEW_MSG", "New");
	define("USED_MSG", "Used");
	define("REFURBISHED_MSG", "Refurbished");
	define("ADD_NEW_MSG", "Add New");
	define("SETTINGS_MSG", "Settings");
	define("VIEW_MSG", "ФЗеП");
	define("STATUS_MSG", "ЗбНЗбе");
	define("NONE_MSG", "None");
	define("PRICE_MSG", "ЗбУЪС");
	define("TEXT_MSG", "Text");
	define("WARNING_MSG", "Warning");
	define("HIDDEN_MSG", "Hidden");
	define("CODE_MSG", "Code");
	define("LANGUAGE_MSG", "Language");
	define("DEFAULT_VIEW_TYPE_MSG", "Default View Type");
	define("CLICK_TO_OPEN_SECTION_MSG", "Click to open section");
	define("CURRENCY_WRONG_VALUE_MSG", "Currency code has wrong value.");
	define("TRANSACTION_AMOUNT_DOESNT_MATCH_MSG", "<b>Transaction Amount</b> and <b>Order Amount</b> doesn't match.");
	define("STATUS_CANT_BE_UPDATED_MSG", "The status for order #{order_id} can't be updated. ");
	define("CANT_FIND_STATUS_MSG", "Can't find the status with ID:{status_id}");
	define("NOTIFICATION_SENT_MSG", "Notification sent");
	define("AUTO_SUBMITTED_PAYMENT_MSG", "Auto-submitted payment");
	define("FONT_METRIC_FILE_ERROR", "Could not include font metric file");
	define("PER_LINE_MSG", "per line");
	define("PER_LETTER_MSG", "per letter");
	define("PER_NON_SPACE_LETTER_MSG", "per non-space letter");
	define("LETTERS_ALLOWED_MSG", "letters allowed");
	define("LETTERS_ALLOWED_PER_LINEMSG", "letters allowed per line");
	define("RENAME_MSG", "Rename");
	define("IMAGE_FORMAT_ERROR_MSG", "Image format is not supported by the GD library");
	define("GD_LIBRARY_ERROR_MSG", "GD library not loaded");
	define("INVALID_CODE_MSG", "Invalid code: ");
	define("INVALID_CODE_TYPE_MSG", "Invalid code type");
	define("INVALID_FILE_EXTENSION_MSG", "Invalid file extension:");
	define("FOLDER_WRITE_PERMISSION_MSG", "The folder doesn't exists or you do not have the permissions");
	define("UNDEFINED_RECORD_PARAMETER_MSG", "Undefined record parameter: <b>{parameter_name}</b>");
	define("MAX_RECORDS_LIMITATION_MSG", "You are not allowed to add more than <b>{max_records}</b> {records_name} for your version");
	define("ACCESS_DENIED_MSG", "You are not allowed to access this section.");
	define("DELETE_RECORDS_BEFORE_PROCEED_MSG", "Please delete some {records_name} before proceed.");
	define("FOLDER_DOESNT_EXIST_MSG", "The folder doesn't exist:");
	define("FILE_DOESNT_EXIST_MSG", "The file doesn't exist:");
	define("PARSE_ERROR_IN_BLOCK_MSG", "Parse error in block:");
	define("BLOCK_DOENT_EXIST_MSG", "Block doesn't exist:");
	define("NUMBER_OF_ELEMENTS_MSG", "Number of elements");
	define("MISSING_COMPONENT_MSG", "Missing component/parameter.");
	define("RELEASES_TITLE", "ЗбЕШбЗЮ");
	define("DETAILED_MSG", "Detailed");
	define("LIST_MSG", "List");
	define("READONLY_MSG", "Readonly");
	define("CREDIT_MSG", "Credit");
	define("ONLINE_MSG", "Online");
	define("OFFLINE_MSG", "Offline");
	define("SMALL_CART_MSG", "Small Cart");
	define("NEVER_MSG", "Never");
	define("SEARCH_EXACT_WORD_OR_PHRASE", "exact wording or phrase");
	define("SEARCH_ONE_OR_MORE", "one or more of these words");
	define("SEARCH_ALL", "all these words");
	define("RELATED_ARTICLES_MSG", "Related Articles");
	define("RELATED_FORUMS_MSG", "Related Forums");
	define("APPEARANCE_MSG", "Appearance");
	define("REMOVE_FILTER_MSG", "remove filter");

	define("RECORD_UPDATED_MSG", "The record has been successfully updated.");
	define("RECORD_ADDED_MSG", "New record has been successfully added.");
	define("RECORD_DELETED_MSG", "The record has been successfully deleted.");
	define("FAST_PRODUCT_ADDING_MSG", "Fast Product Adding");

	define("CURRENT_SUBSCRIPTION_MSG", "Current Subscription");
	define("SUBSCRIPTION_EXPIRATION_MSG", "Subscription expiration date");
	define("UPGRADE_DOWNGRADE_MSG", "Upgrade/Downgrade");
	define("SUBSCRIPTION_MONEY_BACK_MSG", "Subscription Money Back");
	define("MONEY_TO_CREDITS_BALANCE_MSG", "money will be added to your credits balance");
	define("USED_VOUCHERS_MSG", "Used Vouchers");
	define("VOUCHERS_TOTAL_MSG", "Vouchers Total");
	define("SUBSCRIPTIONS_GROUPS_MSG", "Subscriptions Groups");
	define("SUBSCRIPTIONS_GROUP_MSG", "Subscriptions Group");
	define("SUBSCRIPTIONS_MSG", "Subscriptions");
	define("SUBSCRIPTION_START_DATE_MSG", "Subscription Start Date");
	define("SUBSCRIPTION_EXPIRY_DATE_MSG", "Subscription Expiration Date");
	define("RECALCULATE_COMMISSIONS_AND_POINTS_MSG", "Automatically recalculate commissions and points for this item using price value");
	define("SUBSCRIPTION_PAGE_MSG", "Subscription page");
	define("SUBSCRIPTION_WITHOUT_REGISTRATION_MSG", "User can add subscriptions to his cart without registration");
	define("SUBSCRIPTION_REQUIRE_REGISTRATION_MSG", "User must have an account before accessing subscription page");

	define("MATCH_EXISTED_PRODUCTS_MSG", "Match Existing Products");
	define("MATCH_BY_ITEM_CODE_MSG", "by product code");
	define("MATCH_BY_MANUFACTURER_CODE_MSG", "by manufacturer code");
	define("DISPLAY_COMPONENTS_AND_OPTIONS_LIST_MSG", "Display options and components list");
	define("AS_LIST_MSG", "as list");
	define("AS_TABLE_MSG", "as table");

	define("ACCOUNT_SUBSCRIPTION_MSG", "Account subscription");
	define("ACCOUNT_SUBSCRIPTION_DESC", "To activate his account user needs to pay the subscription fee");
	define("SUBSCRIPTION_CANCELLATION_MSG", "Subscription cancellation");
	define("CONFIRM_CANCEL_SUBSCRIPTION_MSG", "Are you sure you want cancel this subscription?");
	define("CONFIRM_RETURN_SUBSCRIPTION_MSG", "Are you sure you want cancel this subscription and return {credits_amount} to balance?");
	define("CANCEL_SUBSCRIPTION_MSG", "Cancel Subscription");
	define("DONT_RETURN_MONEY_MSG", "Don't return the money");
	define("RETURN_MONEY_TO_CREDITS_BALANCE_MSG", "Return money for unused period to credits balance");
	define("UPGRADE_DOWNGRADE_TYPE_MSG", "Can user upgrade/downgrade his account type");

	define("PREDEFINED_TYPES_MSG", "Predefined Types");
	define("SHIPPING_TAX_PERCENT_MSG", "Shipping Tax Percent");
	define("PACKAGES_NUMBER_MSG", "Number of Packages");
	define("PER_PACKAGE_MSG", "per package");
	define("CURRENCY_SHOW_DESC", "user can choose this currency in which prices will be shown");
	define("CURRENCY_DEFAULT_SHOW_DESC", "all prices shown by default in this currency");
	define("RECOMMENDED_MSG", "Recomended");
	define("UPDATE_STATUS_MSG", "Update status");

	define("FIRST_CONTROLS_ARE_FREE_MSG", "First {free_price_amount} controls are free");
	define("FIRST_LETTERS_ARE_FREE_MSG", "First {free_price_amount} letters are free");
	define("FIRST_NONSPACE_LETTERS_ARE_FREE_MSG", "First  {free_price_amount} non-space letters are free");

	// email & SMS messages
	define("EMAIL_NOTIFICATION_MSG", "Email Notification");
	define("EMAIL_NOTIFICATION_ADMIN_MSG", "Administrator Email Notification");
	define("EMAIL_NOTIFICATION_USER_MSG", "User Email Notification");
	define("EMAIL_SEND_ADMIN_MSF", "Send notification to administrator ");
	define("EMAIL_SEND_USER_MSG", "Send notification to user");
	define("EMAIL_USER_IF_STATUS_MSG", "Send notification email to user when the status is applied");
	define("EMAIL_TO_MSG", "To");
	define("EMAIL_TO_USER_DESC", "Customer email used if is empty");
	define("EMAIL_FROM_MSG", "From");
	define("EMAIL_CC_MSG", "Cc");
	define("EMAIL_BCC_MSG", "Bcc");
	define("EMAIL_REPLY_TO_MSG", "Reply To");
	define("EMAIL_RETURN_PATH_MSG", "Return Path");
	define("EMAIL_SUBJECT_MSG", "Subject");
	define("EMAIL_MESSAGE_TYPE_MSG", "Message Type");
	define("EMAIL_MESSAGE_MSG", "Message");
	define("SMS_NOTIFICATION_MSG", "SMS Notification");
	define("SMS_NOTIFICATION_ADMIN_MSG", "Administrator SMS Notification");
	define("SMS_NOTIFICATION_USER_MSG", "User SMS Notification ");
	define("SMS_SEND_ADMIN_MSF", "Send SMS Notification to Administrator");
	define("SMS_SEND_USER_MSG", "Send SMS Notification to User ");
	define("SMS_USER_IF_STATUS_MSG", "Send SMS notification to user when the status is applied");
	define("SMS_RECIPIENT_MSG", "SMS Recipient");
	define("SMS_RECIPIENT_ADMIN_DESC", "administrator's cell phone number");
	define("SMS_RECIPIENT_USER_DESC", "if empty 'Cell phone' value is used");
	define("SMS_ORIGINATOR_MSG", "SMS Originator");
	define("SMS_MESSAGE_MSG", "SMS Message");

	// account messages
	define("LOGIN_AS_MSG", "Кг КУМнб ПОжбЯ ЯЬ");
	define("LOGIN_INFO_MSG", "гЪбжгЗК ЗбПОжб");
	define("ACCESS_HOME_MSG", "ббПОжб Збм ХЭНКЯ ЗбСЖнУне");
	define("REMEMBER_LOGIN_MSG", "КРЯС ЗУгн ж ЯбгЙ ЗбгСжС");
	define("ENTER_LOGIN_MSG", "ЗПОб ЗУгЯ ж ЯбгЙ ЗбгСжС ббЕУКгСЗС");
	define("LOGIN_PASSWORD_ERROR", "ЗЗбЗУг Зж ЯбгЙ ЗбгСжС ЫнС ХНнНнд");
	define("ACCOUNT_APPROVE_ERROR", "дГУЭ, бг нКг КЭЪнб ЪЦжнКЯ НКм ЗбВд");
	define("ACCOUNT_EXPIRED_MSG", "Your account has expired.");
	define("NEW_PROFILE_ERROR", ".бЗ КжМП бЯ ЗбХбЗНнЗК бЭКН ЪЦжне");
	define("EDIT_PROFILE_ERROR", ".бЗ КгбЯ ЗбХбЗНнЗК бКНСнС еРЗ ЗбгбЭ");
	define("CHANGE_DETAILS_MSG", "КЪПнб ОХЗЖХЯ");
	define("CHANGE_DETAILS_DESC", "ЗЦЫШ Ъбм ЗбСЗИШ Эн ЗбГЪбм ЗРЗ ЯдК КСнП КЫннС гЪбжгЗК ЗбПОжб ЗббКн ЗПОбКеЗ ЪдП ЗдФЗБ ЗбЪЦжне");
	define("CHANGE_PASSWORD_MSG", "КЫннС ЯбгЙ ЗбгСжС");
	define("CHANGE_PASSWORD_DESC", "ЗбСЗИШ Эн ЗбГУЭб УнГОРЯ бХЭНЙ КЫннС ЯбгЙ ЗбгСжС");
	define("SIGN_UP_MSG", "ЗбКУМнб ЗбВд");
	define("MY_ACCOUNT_MSG", "ЪЦжнКн");
	define("NEW_USER_MSG", "ЪЦж МПнП");
	define("EXISTS_USER_MSG", "ЪЦж гУМб гУИЮЗр");
	define("EDIT_PROFILE_MSG", "КНСнС ЗбгбЭ");
	define("PERSONAL_DETAILS_MSG", "ЗбгЪбжгЗК ЗбОЗХе");
	define("DELIVERY_DETAILS_MSG", "гЪбжгЗК ЗбКжХнб");
	define("SAME_DETAILS_MSG", " ЗРЗ бг КЯд гКФЗИеКнд, ЗбСМЗБ ЗЯгЗб ЗбНЮжб Эн ЗбЗУЭб <br> ЗРЗ ЯЗдК гЪбжгЗК ЗбФНд гЛб ЗбгЪбжгЗК ЗбОЗХе ЗФС Ъбм ЗбгСИЪ");
	define("DELIVERY_MSG", "ЗбФНд");
	define("SUBSCRIBE_CHECKBOX_MSG", ".ЗбСМЗБ ЗбКГФнС Ъбм ЗбгСИЪ ЗРЗ ЯдК КСнП ЗУКЮИЗб ЪСжЦдЗ ж ЗбКОЭнЦЗК");
	define("ADDITIONAL_DETAILS_MSG", "Additional Details");
	define("GUEST_MSG", "Guest");

	// ads messages
	define("MY_ADS_MSG", "ЕЪбЗдЗКн");
	define("MY_ADS_DESC", "ЗРЗ ЯЗдК бПнЯ НЗМнЗК КСнП ИнЪеЗ ЦЪеЗ едЗ, Зде Уеб ж УСнЪ");
	define("AD_GENERAL_MSG", "гЪбжгЗК ЗбЕЪбЗд ЗбЪЗге");
	define("ALL_ADS_MSG", "All Ads");
	define("AD_SELLER_MSG", "ЗбИЗЖЪ");
	define("AD_START_MSG", "КЗСнО ЗбИПБ");
	define("AD_RUNS_MSG", "ЪПП ЗнЗг ЗбЪСЦ");
	define("AD_QTY_MSG", "ЗбЯгне");
	define("AD_AVAILABILITY_MSG", "ЗбКужЭхС");
	define("AD_COMPARED_MSG", "ЗУгН ИгЮЗСдЙ ЗбЕЪбЗдЗК");
	define("AD_UPLOAD_MSG", "ЗСЭЪ ХжСе");
	define("AD_DESCRIPTION_MSG", "ЗбФСН");
	define("AD_SHORT_DESC_MSG", "ФСН ЮХнС");
	define("AD_FULL_DESC_MSG", "ФСН ЯЗгб");
	define("AD_LOCATION_MSG", "ЗбЪджЗд");
	define("AD_LOCATION_INFO_MSG", "гЪбжгЗК ЗЦЗЭне");
	define("AD_PROPERTIES_MSG", "ОХЗЖХ ЗбЕЪбЗд");
	define("AD_SPECIFICATION_MSG", "гжЗХЭЗК ЗбЕЪбЗд");
	define("AD_MORE_IMAGES_MSG", "ЗбгТнП гд ЗбХжС");
	define("AD_IMAGE_DESC_MSG", "ФСН ЗбХжСе");
	define("AD_DELETE_CONFIRM_MSG", "еб КСнП НРЭ еРЗ ЗбЕЪбЗдї");
	define("AD_NOT_APPROVED_MSG", "бг нЮИб");
	define("AD_RUNNING_MSG", "нЪгб");
	define("AD_CLOSED_MSG", "гЫбЮ");
	define("AD_NOT_STARTED_MSG", "бг нИПГ");
	define("AD_NEW_ERROR", ".бЗ КгбЯ ЗбХбЗНне бЕЦЗЭЙ ЗЪбЗд");
	define("AD_EDIT_ERROR", ".бЗ КгбЯ ЗбХбЗНне бКНСнС ЗбЕЪбЗд");
	define("AD_DELETE_ERROR", ".бЗ КгбЯ ЗбХбЗНне бНРЭ ЗбЕЪбЗд");
	define("NO_ADS_MSG", "бЗ КжМП ЗЪбЗдЗК Эн еРЗ ЗбКХднЭ");
	define("NO_AD_MSG", "бЗ нжМП ЗЪбЗд ИеРЗ ЗбСЮг Эн еРЗ ЗбКХднЭ");
	define("FOUND_ADS_MSG", "<b>{search_string}</b>' ЗЪбЗдЗК КжЗЭЮ ЗбЯбге <b>{found_records}</b> Кг ЗнМЗП");
	define("AD_OFFER_MESSAGE_MSG", "ЗбЪСЦ");
	define("AD_OFFER_LOGIN_ERROR", "нМИ КУМнб ЗбПОжб ЮИб ЗбЗУКгСЗС");
	define("AD_REQUEST_BUTTON", "ЗСУб ЗбЗУКЪбЗг");
	define("AD_SENT_MSG", ".Кг ЗСУЗб ЪСЦЯ ИдМЗН");
	define("ADS_SETTINGS_MSG", "Ads Settings");
	define("ADS_DAYS_MSG", "Days to run Ad");
	define("ADS_HOT_DAYS_MSG", "Days to run Hot Ad");
	define("AD_HOT_OFFER_MSG", "Hot Offer");
	define("AD_HOT_ACTIVATE_MSG", "Add this Ad to Hot Offers section");
	define("AD_HOT_START_MSG", "Hot Offer Start Date");
	define("AD_HOT_DESCRIPTION_MSG", "Hot Description");
	define("ADS_SPECIAL_DAYS_MSG", "Days to run Special Ad");
	define("AD_SPECIAL_OFFER_MSG", "Special Offer");
	define("AD_SPECIAL_ACTIVATE_MSG", "Add this Ad to Special Offers section");
	define("AD_SPECIAL_START_MSG", "Special Offer Start Date");
	define("AD_SPECIAL_DESCRIPTION_MSG", "Special Offer Description");
	define("AD_SHOW_ON_SITE_MSG", "Show on site");
	define("AD_CREDITS_BALANCE_ERROR", "Not enough credits on your balance, you need {more_credits} more to post this Ad.");
	define("AD_CREDITS_MSG", "Ad Credits");
	define("ADS_SPECIAL_OFFERS_SETTINGS_MSG", "Ads Special Offers Settings");

	define("EDIT_DAY_MSG", "Edit Day");
	define("DAYS_PRICE_MSG", "Days Price");
	define("ADS_PUBLISH_PRICE_MSG", "Price to post Ad");
	define("DAYS_NUMBER_MSG", "Number of Days");
	define("DAYS_TITLE_MSG", "Days Title");
	define("USER_ADS_LIMIT_MSG", "Number of ads can be added by user");
	define("USER_ADS_LIMIT_DESC", "leave this field blank if you do not want to limit number of ads user can post");
	define("USER_ADS_LIMIT_ERROR", "Sorry, but you are not allowed to add more than {ads_limit} ads.");

	define("ADS_SHOW_TERMS_MSG", "Show Terms & Conditions");
	define("ADS_SHOW_TERMS_DESC", "To submit an ad user has to read and agree to our terms and conditions");
	define("ADS_TERMS_MSG", "Terms & Conditions");
	define("ADS_TERMS_USER_DESC", "I have read and agree to your terms and conditions");
	define("ADS_TERMS_USER_ERROR", "To submit an ad you have to read and agree to our terms and conditions");
	define("ADS_ACTIVATION_MSG", "Ads Activation");
	define("ACTIVATE_ADS_MSG", "Activate Ads");
	define("ACTIVATE_ADS_NOTE", "automatically activate all user ads if his status changed to 'Approved'");
	define("DEACTIVATE_ADS_MSG", "Deactivate Ads");
	define("DEACTIVATE_ADS_NOTE", "automatically deactivate all user ads if his status changed to 'Not Approved'");

	define("MIN_ALLOWED_ADS_PRICE_MSG", "Minimum Allowed Price");
	define("MIN_ALLOWED_ADS_PRICE_NOTE", "leave this field blank if you do not want to limit the lower price for ads");
	define("MAX_ALLOWED_ADS_PRICE_MSG", "Maximum Allowed Price");
	define("MAX_ALLOWED_ADS_PRICE_NOTE", "leave this field blank if you do not want to limit the higher price for ads");

	// search message
	define("SEARCH_FOR_MSG", "ЗИНЛ Ъд");
	define("SEARCH_IN_MSG", "ЗИНЛ Эн");
	define("SEARCH_TITLE_MSG", "ЗбЕУг");
	define("SEARCH_CODE_MSG", "Code");
	define("SEARCH_SHORT_DESC_MSG", "ФСН ЮХнС");
	define("SEARCH_FULL_DESC_MSG", "ФСН Шжнб");
	define("SEARCH_CATEGORY_MSG", "ЗбИНЛ Эн ЗбКХднЭ");
	define("SEARCH_MANUFACTURER_MSG", "ЗбХЗдЪ");
	define("SEARCH_SELLER_MSG", "ЗбИЗЖЪ");
	define("SEARCH_PRICE_MSG", "НПжП ЗбУЪС");
	define("SEARCH_WEIGHT_MSG", "НПжП ЗбжТд");
	define("SEARCH_RESULTS_MSG", "Search Results");
	define("FULL_SITE_SEARCH_MSG", "Full Site Search");

	// compare messages
	define("COMPARE_MSG", "гЮЗСде");
	define("COMPARE_REMOVE_MSG", "ЕТЗбе");
	define("COMPARE_REMOVE_HELP_MSG", "ЗЦЫШ едЗ бЕТЗбЙ ЗбгдКМ гд ЮЗЖгЙ ЗбгЮЗСде");
	define("COMPARE_MIN_ALLOWED_MSG", "нМИ Зд КОКЗС Ъбм ЗбГЮб гдКМнд ЗЛднд");
	define("COMPARE_MAX_ALLOWED_MSG", "нМИ Зд бЗ КОКЗС ЗОЛС гд ОгУЙ гдКМЗК");
	define("COMPARE_PARAM_ERROR_MSG", "гЮЗСдЙ ЗбЛжЗИК КНгб Юнге ОЗШЖе");

	// Tell a friend messages
	define("TELL_FRIEND_TITLE", "ЗОИС ХПнЮЯ");
	define("TELL_FRIEND_SUBJECT_MSG", "ХПнЮЯ ЗСУб ЗбнЯ еРЗ ЗбСЗИШ");
	define("TELL_FRIEND_DEFAULT_MSG", "{item_url} Эн еРЗ ЗбгжЮЪ {item_title} ЗЪКЮП ЗдЯ УКЯжд геКгЗр ИеРЗ ЗбгдКМ - {friend_name} ЗбУбЗг ЪбнЯг");
	define("TELL_YOUR_NAME_FIELD", "ЗУгЯ");
	define("TELL_YOUR_EMAIL_FIELD", "ИСнПЯ ЗбЕбЯКСждн");
	define("TELL_FRIENDS_NAME_FIELD", "ЗУг ХПнЮЯ");
	define("TELL_FRIENDS_EMAIL_FIELD", "ИСнПе ЗбЕбЯКСждн");
	define("TELL_COMMENT_FIELD", "гбЗНЩЗК");
	define("TELL_FRIEND_PRIVACY_NOTE_MSG", ".ИнЗд ОХжХне: бд дЮжг ИЗУКОПЗг ИСнПЯ Зж ИСнП ХПнЮЯ бЗн ЫСЦ Зж Эн Зн НЗбе");
	define("TELL_SENT_MSG", "!Кг ЗСУЗб ЗбИСнП ИдМЗН<br>ФЯСЗр бЯ");
	define("TELL_FRIEND_MESSAGE_MSG", "Thought you might be interested in seeing the {item_title} at {item_url}\\n\\n{user_name} left you a note:\\n{user_comment}");
	define("TELL_FRIEND_PARAM_MSG", "Introduce a friend' URL");
	define("TELL_FRIEND_PARAM_DESC", "adds a friend's URL parameter to a 'Tell a Friend' link if it exists for a user");
	define("FRIEND_COOKIE_EXPIRES_MSG", "Friend Cookie Expires");

	define("CONTACT_US_TITLE", "Contact Us");
	define("CONTACT_USER_NAME_FIELD", "ЗУгЯ");
	define("CONTACT_USER_EMAIL_FIELD", "ИСнПЯ ЗбЕбЯКСждн");
	define("CONTACT_SUMMARY_FIELD", "гбОХ ЮХнС");
	define("CONTACT_DESCRIPTION_FIELD", "ЗбФСН");
	define("CONTACT_REQUEST_SENT_MSG", "Your request was successfully sent.");

	// buttons
	define("GO_BUTTON", "ЗРеИ");
	define("CONTINUE_BUTTON", "ЗУКгС");
	define("BACK_BUTTON", "ЗбСМжЪ");
	define("NEXT_BUTTON", "ЗббЗНЮ");
	define("PREV_BUTTON", "ЗбУЗИЮ");
	define("SIGN_IN_BUTTON", "КУМнб ЗбПОжб");
	define("LOGIN_BUTTON", "ЗбПОжб");
	define("LOGOUT_BUTTON", "ЗбОСжМ");
	define("SEARCH_BUTTON", "ЗбИНЛ");
	define("RATE_IT_BUTTON", "Юунцге");
	define("ADD_BUTTON", "ЗЦЭ");
	define("UPDATE_BUTTON", "НПцЛ");
	define("APPLY_BUTTON", "КШИнЮ");
	define("REGISTER_BUTTON", "УМб");
	define("VOTE_BUTTON", "ХжК");
	define("CANCEL_BUTTON", "ЕбЫЗБ");
	define("CLEAR_BUTTON", "гУН");
	define("RESET_BUTTON", "ЗЪЗПе");
	define("DELETE_BUTTON", "НРЭ");
	define("DELETE_ALL_BUTTON", "Delete All");
	define("SUBSCRIBE_BUTTON", "ЗбЗдКУЗИ");
	define("UNSUBSCRIBE_BUTTON", "ЕбЫЗБ ЗбЗдКУЗИ");
	define("SUBMIT_BUTTON", "ЗСУб");
	define("UPLOAD_BUTTON", "ЗСЭЪ");
	define("SEND_BUTTON", "ЗСУб");
	define("PREVIEW_BUTTON", "Preview");
	define("FILTER_BUTTON", "Filter");
	define("DOWNLOAD_BUTTON", "Download");
	define("REMOVE_BUTTON", "Remove");
	define("EDIT_BUTTON", "Edit");
	define("CHANGE_BUTTON", "Change");
	define("SAVE_BUTTON", "Save");

	// controls
	define("CHECKBOXLIST_MSG", "Checkboxes List");
	define("LABEL_MSG", "Label");
	define("LISTBOX_MSG", "ListBox");
	define("RADIOBUTTON_MSG", "Radio Buttons");
	define("TEXTAREA_MSG", "TextArea");
	define("TEXTBOX_MSG", "TextBox");
	define("TEXTBOXLIST_MSG", "Text Boxes List");
	define("IMAGEUPLOAD_MSG", "Image Upload");
	define("CREDIT_CARD_MSG", "Credit Card");
	define("GROUP_MSG", "Group");

	// fields
	define("LOGIN_FIELD", "ЗУг ЗбгУКОПг");
	define("PASSWORD_FIELD", "ЯбгЙ ЗбгСжС");
	define("CONFIRM_PASS_FIELD", "КГЯнП ЯбгЙ ЗбгСжС");
	define("NEW_PASS_FIELD", "ЯбгЙ ЗбгСжС ЗбМПнПе");
	define("CURRENT_PASS_FIELD", "ЯбгЙ ЗбгСжС ЗбНЗбне");
	define("FIRST_NAME_FIELD", "ЗбЕУг ЗбГжб");
	define("LAST_NAME_FIELD", "ЗбЕУг ЗбГОнС");
	define("NICKNAME_FIELD", "ЗбЕУг");
	define("PERSONAL_IMAGE_FIELD", "ЗбХжСе ЗбФОХне");
	define("COMPANY_SELECT_FIELD", "ЗбФСЯе");
	define("SELECT_COMPANY_MSG", "ЗОКС ФСЯе");
	define("COMPANY_NAME_FIELD", "ЗУг ЗбФСЯе");
	define("EMAIL_FIELD", "ЗбИСнП");
	define("STREET_FIRST_FIELD", "ЗбЪджЗд 1");
	define("STREET_SECOND_FIELD", "ЗбЪджЗд 2");
	define("CITY_FIELD", "ЗбгПнде");
	define("PROVINCE_FIELD", "ЗбгЮЗШЪе");
	define("SELECT_STATE_MSG", "Select State");
	define("STATE_FIELD", "ЗбжбЗне");
	define("ZIP_FIELD", "ЗбСгТ/ЗбХдПжЮ ЗбИСнПн");
	define("SELECT_COUNTRY_MSG", "ЗОКС Пжбе");
	define("COUNTRY_FIELD", "ЗбПжбе");
	define("PHONE_FIELD", "ЗбеЗКЭ");
	define("DAYTIME_PHONE_FIELD", "ЗбеЗКЭ ЗбдеЗСн");
	define("EVENING_PHONE_FIELD", "ЗбеЗКЭ ЗбгУЗЖн");
	define("CELL_PHONE_FIELD", "ЗбеЗКЭ ЗбОбнжн");
	define("FAX_FIELD", "ЗбЭЗЯУ");
	define("VALIDATION_CODE_FIELD", "Validation Code");
	define("AFFILIATE_CODE_FIELD", "Affiliate Code");
	define("AFFILIATE_CODE_HELP_MSG", "Please use the following URL {affiliate_url} to create a link affiliated with our site");
	define("PAYPAL_ACCOUNT_FIELD", "PayPal Account");
	define("TAX_ID_FIELD", "Tax Number");
	define("MSN_ACCOUNT_FIELD", "MSN Account");
	define("ICQ_NUMBER_FIELD", "ICQ Number");
	define("USER_SITE_URL_FIELD", "User's Site URL");
	define("HIDDEN_STATUS_FIELD", "Hidden Status");
	define("HIDE_MY_ONLINE_STATUS_MSG", "Do not show my online status");
	define("SUMMARY_MSG", "ЗбгбОХ");

	// no records messages
	define("NO_RECORDS_MSG", "бг нКг ЗнМЗП Зн УМбЗК");
	define("NO_EVENTS_MSG", "бг нКг ЗнМЗП ЗнЙ ЗНПЗЛ");
	define("NO_QUESTIONS_MSG", "бг нКг ЗнМЗП ЗнЙ ЗУЖбе");
	define("NO_NEWS_MSG", "бг нКг ЗнМЗП ЗнЙ ЗОИЗС");
	define("NO_POLLS_MSG", "бг нКг ЗнМЗП ЗнЙ КХжнКЗК");

	// SMS messages
	define("SMS_TITLE", "SMS");
	define("SMS_TEST_TITLE", "SMS Test");
	define("SMS_TEST_DESC", "Please enter your cell phone number and press button 'SEND_BUTTON' to receive test message");
	define("INVALID_CELL_PHONE", "Incorrect cell phone number");

	define("ARTICLE_RELATED_PRODUCTS_TITLE", "Article Related Products");
	define("CATEGORY_RELATED_PRODUCTS_TITLE", "Category Related Products");
	define("SELECT_TYPE_MSG", "ЗОКС ЗбджЪ");
	define("OFFER_PRICE_MSG", "Offer Price");
	define("OFFER_MESSAGE_MSG", "Offer Message");

	define("MY_WISHLIST_MSG", "My Wishlist");
	define("SELECT_WISHLIST_TYPE_MSG", "Please select a type to save product in your wishlist");
	define("MY_REMINDERS_MSG", "My Reminders");
	define("EDIT_REMINDER_MSG", "Edit Reminder");


	define("SELECT_SUBFOLDER_MSG", "Select Subfolder");
	define("CURRENT_DIR_MSG", "Current directory");
	define("NO_AVAILIABLE_CATEGORIES_MSG", "No categories available");
	define("SHOW_FOR_NON_REGISTERED_USERS_MSG", "Show for non registered users");
	define("SHOW_FOR_REGISTERED_USERS_MSG", "Show for all registered users");
	define("VIEW_ITEM_IN_THE_LIST_MSG", "View item in products list");
	define("ACCESS_DETAILS_MSG", "Access Details");
	define("ACCESS_ITEMS_DETAILS_MSG", "Access item details / buy item");
	define("OTHER_SUBSCRIPTIONS_MSG", "Other Subscriptions");
	define("USE_CATEGORY_ALL_SITES_MSG", "Use this category for all sites (untick this checkbox to select sites manually)");
	define("USE_ITEM_ALL_SITES_MSG", "Use this item for all sites (untick this checkbox to select sites manually) ");
	define("SAVE_SUBSCRIPTIONS_SETTINGS_BY_CATEGORY_MSG", "Save subscriptions settings from categories");
	define("SAVE_SITES_SETTINGS_BY_CATEGORY_MSG", "Save sites settings from categories");
	define("ACCESS_LEVELS_MSG", "Access Levels");
	define("SITES_MSG", "Sites");
	define("NON_REGISTERED_USERS_MSG", "Non registered users");
	define("REGISTERED_CUSTOMERS_MSG", "Registered Users");
	define("PREVIEW_IN_SEPARATE_SECTION_MSG", "Show in separate section");
	define("PREVIEW_BELOW_DETAILS_IMAGE_MSG", "Show below image on details page");
	define("PREVIEW_BELOW_LIST_IMAGE_MSG", "Show below image on listing page");
	define("PREVIEW_POSITION_MSG", "Position");
	define("ADMIN_NOTES_MSG", "Administrator Notes");
	define("USER_NOTES_MSG", "User Notes");
	define("CMS_PERMISSIONS_MSG", "CMS Permissions");
	define("ARTICLES_PERMISSIONS_MSG", "Articles Permissions");
	define("FOOTER_LINK_MSG", "Footer Link");
	define("FOOTER_LINKS_MSG", "Footer Links");
	define("WISHLIST_MSG", "Wishlist");
	define("TYPE_IS_NOT_AVAILIABLE_MSG", "Type is not availiable");
	define("TYPE_IS_NOT_SELECTED_MSG", "Type is not selected");
	define("SHOW_ALL_MSG", "Show all");

	define("MEMBER_SINCE_MSG", "Member Since");
	define("SITEMAP_TITLE_INDEX", "title");
	define("SITEMAP_URL_INDEX", "url");
	define("SITEMAP_SUBS_INDEX", "subs");

	define("HOT_RELEASES_MSG", "Hot Releases");
	define("PAY_FOR_AD_MSG", "Pay for Ad");
	define("CHECK_SITE_MSG", "check the site");

	define("CONTACT_US_MSG", "Contact Us");
	define("REGISTERED_USERS_ALLOWED_MESSAGES_MSG", "Only registered users can send their messages.");
	define("NOT_ALLOWED_SEND_MESSAGES_MSG", "Sorry, but you are not allowed to send messages.");
	define("MESSAGE_SENT_MSG", "Your message has been sent sucessfully");
	define("MESSAGE_INTERVAL_ERROR", "Please wait for {interval_time} to send your message.");
	define("ONE_LINE_SUMMARY_MSG", "гбОХ ЮХнС");
	define("DETAILED_COMMENT_MSG", "КЪбнЮ гЭХб");

?>