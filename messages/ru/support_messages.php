<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.10RE                                               ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viartsoft.ru                                         ***
  ***                                                                      ***
  ****************************************************************************
*/

	// сообщения поддержки
	define("SUPPORT_TITLE", "Центр запросов");
	define("SUPPORT_REQUEST_INF_TITLE", "Информация по запросу");
	define("SUPPORT_REPLY_FORM_TITLE", "Ответ");
	define("MY_SUPPORT_ISSUES_MSG", "Мои запросы в поддержку");
	define("MY_SUPPORT_ISSUES_DESC", "Если у Вас возникли вопросы по товарам или затруднения с оформлением заказа, наша служба поддержки готова помочь.");
	define("NEW_SUPPORT_REQUEST_MSG", "Новый запрос");
	define("SUPPORT_REQUEST_ADDED_MSG", "Спасибо,<br> наша служба поддержки постарается как можно скорее помочь Вам с вашим запросом.");
	define("SUPPORT_SELECT_DEP_MSG", "Выберите отдел");
	define("SUPPORT_SELECT_PROD_MSG", "Выберите");
	define("SUPPORT_SELECT_STATUS_MSG", "Выберите");
	define("SUPPORT_NOT_VIEWED_MSG", "Не просмотрено");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Просмотрено пользователем");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Просмотрено администратором");
	define("SUPPORT_STATUS_NEW_MSG", "Новый");
	define("NO_SUPPORT_REQUEST_MSG", "Нет запросов");

	define("SUPPORT_SUMMARY_COLUMN", "Заголовок");
	define("SUPPORT_TYPE_COLUMN", "Тип запроса");
	define("SUPPORT_UPDATED_COLUMN", "Изменено");

	define("SUPPORT_USER_NAME_FIELD", "Представьтесь, пожалуйста");
	define("SUPPORT_USER_EMAIL_FIELD", "Ваш электронный адрес");
	define("SUPPORT_IDENTIFIER_FIELD", "Номер счета / заказа (необязательно)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Название товара (необязательно)");
	define("SUPPORT_DEPARTMENT_FIELD", "Департамент");
	define("SUPPORT_PRODUCT_FIELD", "Укажите объект запроса");
	define("SUPPORT_TYPE_FIELD", "Выберите тип запроса");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Текущий статус");
	define("SUPPORT_SUMMARY_FIELD", "Краткий заголовок");
	define("SUPPORT_DESCRIPTION_FIELD", "Текст Вашего запроса");
	define("SUPPORT_MESSAGE_FIELD", "Сообщение");
	define("SUPPORT_ADDED_FIELD", "Отправлено");
	define("SUPPORT_ADDED_BY_FIELD", "Отправил");
	define("SUPPORT_UPDATED_FIELD", "Изменено");

	define("SUPPORT_REQUEST_BUTTON", "Отправить");
	define("SUPPORT_REPLY_BUTTON", "Ответить");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Пропущен параметр <b>Support ID</b>.");
	define("SUPPORT_MISS_CODE_ERROR", "Пропущен параметр <b>Проверочный код</b>.");
	define("SUPPORT_WRONG_ID_ERROR", "Параметр <b>Support ID</b> имеет неправильное значение.");
	define("SUPPORT_WRONG_CODE_ERROR", "Параметр <b>Проверочный код</b> имеет неправильное значение.");

	define("MAIL_DATA_MSG", "Данные письма");
	define("HEADERS_MSG", "Заголовки");
	define("ORIGINAL_TEXT_MSG", "Исходный текст");
	define("ORIGINAL_HTML_MSG", "Исходный HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Извините, но у Вас недостаточно полномочий для этого.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Извините, но у Вас недостаточно полномочий для этого.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Извините, но у Вас недостаточно полномочий для этого.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Извините, но у Вас недостаточно полномочий для этого.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Извините, но у Вас недостаточно полномочий для этого.<br>");
	define("NO_TICKETS_FOUND_MSG", "Нет запросов.");
	define("HIDDEN_TICKETS_MSG", "Скрытые запросы");
	define("ALL_TICKETS_MSG", "Все запросы");
	define("ACTIVE_TICKETS_MSG", "Активные запросы");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "Вы не назначены в этот департамент.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "Вы не назначены ни в какой департамент.");
	define("REPLY_TO_NAME_MSG", "Ответ  {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Категория базы знаний");
	define("KNOWLEDGE_TITLE_MSG", "Название раздела");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Статус раздела базы знаний");
	define("SELECT_RESPONSIBLE_MSG", "Выбрать ответственного");
	//ticket types
	define("TICKET_BUG_ISSUE_MSG", "Ошибка");
	define("TICKET_QUESTION_MSG", "Вопрос");
	define("TICKET_WISH_REQUEST_MSG", "Пожелание");
	define("TICKET_OTHER_MSG", "Другое");
	define("TICKET_QUOTE_MSG", "Цитата");
	//ticket statuses
	define("TICKET_NEW_MSG", "Новый");
	define("TICKET_ANSWERED_MSG", "Ответил");
	define("TICKET_REQUEST_INFO_MSG", "Запрос дополнительной информации");
	define("TICKET_INVESTIGATING_MSG", "Расследование");
	define("TICKET_CLOSED_MSG", "Закрыто");
	define("TICKET_ASSIGNED_MANAGER_MSG", "Назначена менеджеру");
	define("TICKET_CUSOMER_REPLY_MSG", "Ответ клиенту");
	define("TICKET_ADDED_KB_MSG", "Дополнить в базу знаний");
	//define("PRIORITY_HIGH_MSG", "Высокий");
    //define("PRIORITY_NORMAL_MSG", "Обычный");
    //define("PRIORITY_LOW_MSG", "Низкий");
?>
