// compare javacript
function compareItems(formName)
{
	var itemsForm = document.forms[formName];
	// check available items indexes 
	var indexes = new Array();
	if (itemsForm.items_indexes && itemsForm.items_indexes.value != "") {
		indexes = itemsForm.items_indexes.value.split(",");
	}

	var checkedNumber = 0;
	var checkedItems = "";
	for (var id in indexes) {
		var idx = indexes[id];
		if(itemsForm.elements["compare"+idx] && itemsForm.elements["compare"+idx].checked) {
			checkedNumber++;
			if(checkedNumber > 1) { checkedItems += ","; }
			checkedItems += itemsForm.elements["compare"+idx].value;
		}
	}
	if (checkedNumber < 2) {
		alert(compareMinAllowed);
	} else if (checkedNumber > 5) {
		alert(compareMaxAllowed);
	} else {
		document.compare_form.items.value = checkedItems;
		document.compare_form.submit();
	}

	return false;
}

function compareRecentItems(formName)
{
	var checkedNumber = 0;
	var checkedItems = "";
	var recentForm = document.forms[formName];
	var compareItems = recentForm.compare;
	for (var i = 0; i < compareItems.length; i++) {
		if(recentForm.compare[i].checked) {
				checkedNumber++;
				if(checkedNumber > 1) { checkedItems += ","; }
				checkedItems += recentForm.compare[i].value;
		}
	}
	if (checkedNumber < 2) {
		alert(compareMinAllowed);
	} else if (checkedNumber > 5) {
		alert(compareMaxAllowed);
	} else {
		recentForm.items.value = checkedItems;
		recentForm.submit();
	}

	return false;
}