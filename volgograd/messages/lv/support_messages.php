<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Atbalsta centrs");
	define("SUPPORT_REQUEST_INF_TITLE", "Atbalsta informвcija");
	define("SUPPORT_REPLY_FORM_TITLE", "Atbilde");

	define("MY_SUPPORT_ISSUES_MSG", "Mani atbalsta pieprasоjumi");
	define("MY_SUPPORT_ISSUES_DESC", "Ja Jums ir raduрвs problзmas ar kвdu no mыsu produktiem/pakalpojumiem, lыdzu, jautвjiet mыsu Klientu atbalsta komandai. Spiediet uz zemвka redzamвs saites, lai nosыtоtu savu pieprasоjumu mыsu Klientu atbalsta speciвlistiem.");
	define("NEW_SUPPORT_REQUEST_MSG", "Jauns pieprasоjums");
	define("SUPPORT_REQUEST_ADDED_MSG", "Paldies!<br>Klienta atbalsta speciвlisti ar Jums sazinвsies tuvвkajв laikв, lai sniegtu Jums nepiecieрamo atbalstu.");
	define("SUPPORT_SELECT_DEP_MSG", "Izvзlзties departamentu");
	define("SUPPORT_SELECT_PROD_MSG", "Izvзlзties produktu");
	define("SUPPORT_SELECT_STATUS_MSG", "Izvзlзties status");
	define("SUPPORT_NOT_VIEWED_MSG", "Nav skatоts");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Skatоjuрies klienti");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Skatоjuрies administratori");
	define("SUPPORT_STATUS_NEW_MSG", "Jauns");
	define("NO_SUPPORT_REQUEST_MSG", "Netika atrasts nekas atbilstoрs");

	define("SUPPORT_SUMMARY_COLUMN", "Kopsavilkums");
	define("SUPPORT_TYPE_COLUMN", "Veids");
	define("SUPPORT_UPDATED_COLUMN", "Pзdзjo reizi atjaunots");

	define("SUPPORT_USER_NAME_FIELD", "Tavs vвrds, uzvвrds");
	define("SUPPORT_USER_EMAIL_FIELD", "Tava e-pasta adrese");
	define("SUPPORT_IDENTIFIER_FIELD", "Atpazоstamоba (Rзнina Nr)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Vide (operзtвjsistзma, datu bвze, Web serveris, u.c.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Departaments");
	define("SUPPORT_PRODUCT_FIELD", "Produkts");
	define("SUPPORT_TYPE_FIELD", "Veids");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Patreizзjais stвvoklis");
	define("SUPPORT_SUMMARY_FIELD", "Kopsavilkums vienв rindв");
	define("SUPPORT_DESCRIPTION_FIELD", "Apraksts");
	define("SUPPORT_MESSAGE_FIELD", "Ziтa");
	define("SUPPORT_ADDED_FIELD", "Pievienots");
	define("SUPPORT_ADDED_BY_FIELD", "Pievienojis");
	define("SUPPORT_UPDATED_FIELD", "Pзdзjo reizi atjaunots");

	define("SUPPORT_REQUEST_BUTTON", "Apstiprinвt pieprasоjumu");
	define("SUPPORT_REPLY_BUTTON", "Atbildзt");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Trыkst <b>Atbalsta ID</b> vзrtоba");
	define("SUPPORT_MISS_CODE_ERROR", "Trыkst <b>Apstripinвjuma</b> vзrtоba");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Atbalsta ID</b> ievadоtв vзrtоba nav pareiza");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Apstiprinвjuma</b> ievadоtв vзrtоba nav pareiza");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>