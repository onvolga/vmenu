<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// palaikymo юinutлs
	define("SUPPORT_TITLE", "Palaikymo bыstinл");
	define("SUPPORT_REQUEST_INF_TITLE", "Uюklausti юiniш");
	define("SUPPORT_REPLY_FORM_TITLE", "Atsakyti");

	define("MY_SUPPORT_ISSUES_MSG", "Mano palaikymo uюklausimai");
	define("MY_SUPPORT_ISSUES_DESC", "Jei Jыs susiduriate su bлdomis dлl prekiш kurias pirkote, mыsш palaikymo grupл pasiruoрus padлti. Jauskitлs laisvi susisiekti su mumis, spausdami nuorodа kad pasiшsti Palaikymo Uюklausimа.");
	define("NEW_SUPPORT_REQUEST_MSG", "Naujas Uюklausimas");
	define("SUPPORT_REQUEST_ADDED_MSG", "Aиiы Jums<br>Mыsш Palaikymo grupл stengsis jums padлti kaip galima greiиiau dлl jыsш Uюklausimo.");
	define("SUPPORT_SELECT_DEP_MSG", "Rinkitлs skyriш");
	define("SUPPORT_SELECT_PROD_MSG", "Rinkitлs prekж");
	define("SUPPORT_SELECT_STATUS_MSG", "Rinkitлs stovб");
	define("SUPPORT_NOT_VIEWED_MSG", "Neperюiыrлta");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Юiыrлta kliento");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Юiыrлta valdytojo");
	define("SUPPORT_STATUS_NEW_MSG", "Nauja");
	define("NO_SUPPORT_REQUEST_MSG", "Klausimш nerasta");

	define("SUPPORT_SUMMARY_COLUMN", "Suvestinл");
	define("SUPPORT_TYPE_COLUMN", "Tipas");
	define("SUPPORT_UPDATED_COLUMN", "Paskutinis atnaujinimas");

	define("SUPPORT_USER_NAME_FIELD", "Jыsш vardas");
	define("SUPPORT_USER_EMAIL_FIELD", "Jыsш e-paрto adresas");
	define("SUPPORT_IDENTIFIER_FIELD", "Identifikatorius (sаskaitos nr.)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Aplinka (OS, Duombazл, Tinklo serveris, ir pan.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Skyrius");
	define("SUPPORT_PRODUCT_FIELD", "Prekл");
	define("SUPPORT_TYPE_FIELD", "Tipas");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Esamas stovis");
	define("SUPPORT_SUMMARY_FIELD", "Vienos-eilutлs suvestinл");
	define("SUPPORT_DESCRIPTION_FIELD", "Apraрymas");
	define("SUPPORT_MESSAGE_FIELD", "Юinutл");
	define("SUPPORT_ADDED_FIELD", "Pridлta");
	define("SUPPORT_ADDED_BY_FIELD", "Pridлta kieno");
	define("SUPPORT_UPDATED_FIELD", "Paskutinis atnaujinimas");

	define("SUPPORT_REQUEST_BUTTON", "Teikti uюklausimа");
	define("SUPPORT_REPLY_BUTTON", "Atsakyti");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Trыkstamas <b>Palaikymo ID</b> parametras");
	define("SUPPORT_MISS_CODE_ERROR", "Trыkstamas <b>Patikrinimo</b> parametras");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Palaikymo ID</b> parametras turi blogа reikрmж");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Patikrinimo</b> parametras turi blogа reikрmж");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>