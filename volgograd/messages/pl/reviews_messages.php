<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// informacje dot. opinii/ocen
	define("OPTIONAL_MSG", "Alternatywny");
	define("BAD_MSG", "Zіy");
	define("POOR_MSG", "Sі±by");
	define("AVERAGE_MSG", "Przeciкtny");
	define("GOOD_MSG", "Dobry");
	define("EXCELLENT_MSG", "Doskonaіy");
	define("REVIEWS_MSG", "Recenzje");
	define("NO_REVIEWS_MSG", "Nie znaleziono recenzji");
	define("WRITE_REVIEW_MSG", "Napisz recenzjк");
	define("RATE_PRODUCT_MSG", "Oceс ten produkt");
	define("RATE_ARTICLE_MSG", "Oceс ten artykuі");
	define("NOT_RATED_PRODUCT_MSG", "Produkt nie dostaі jeszcze oceny");
	define("NOT_RATED_ARTICLE_MSG", "Artykuі nie dostaі jeszcze oceny");
	define("AVERAGE_RATING_MSG", "Przeciкtna ocena klientуw");
	define("BASED_ON_REVIEWS_MSG", "bazuj±cy na {total_votes} recenzjach");
	define("POSITIVE_REVIEW_MSG", "Pozytywna ocena klienta");
	define("NEGATIVE_REVIEW_MSG", "Negatywna ocena klienta");
	define("SEE_ALL_REVIEWS_MSG", "Zobacz wszystkie recenzje klientуw");
	define("ALL_REVIEWS_MSG", "Wszystkie recenzje");
	define("ONLY_POSITIVE_MSG", "Tylko pozytywne");
	define("ONLY_NEGATIVE_MSG", "Tylko negatywne");
	define("POSITIVE_REVIEWS_MSG", "Pozytywne recenzje");
	define("NEGATIVE_REVIEWS_MSG", "Negatywne recenzje");
	define("SUBMIT_REVIEW_MSG", "Dziкkujemy<br>Twoje komentarze zostaіy dodane. Je¶li je zaakceptujemy pojawi± siк na stronie.<br> Zastrzegamy sobie prawo do odrzucenia jakichkolwiek wpisуw niezgodnych z naszymi celami.");
	define("ALREADY_REVIEW_MSG", "Juї umie¶ciіe¶ recenzjк");
	define("RECOMMEND_PRODUCT_MSG", "Czy chcesz zarekomendowaж<br>ten produkt swoim znajomym?");
	define("RECOMMEND_ARTICLE_MSG", "Czy chcesz zarekomendowaж<br>ten artykuі swoim znajomym?");
	define("RATE_IT_MSG", "Oceс to");
	define("NAME_ALIAS_MSG", "Nazwa lub alias");
	define("SHOW_MSG", "Pokaї");
	define("FOUND_MSG", "Znaleziono");
	define("RATING_MSG", "Statystyczne oceny");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>