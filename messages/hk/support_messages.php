<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "&#25903;&#25588;&#20013;&#24515;");
	define("SUPPORT_REQUEST_INF_TITLE", "&#35201;&#27714;&#36039;&#26009;");
	define("SUPPORT_REPLY_FORM_TITLE", "&#22238;&#35206;");

	define("MY_SUPPORT_ISSUES_MSG", "&#25105;&#30340;&#25903;&#25588;&#35201;&#35531;");
	define("MY_SUPPORT_ISSUES_DESC", "&#22914;&#23565;&#20219;&#20309;&#26377;&#38364;&#25152;&#36092;&#30340;&#29986;&#21697;&#26377;&#21839;&#38988;,&#25105;&#20497;&#30340;&#25903;&#25588;&#20013;&#24515;&#38568;&#26178;&#21487;&#25552;&#20379;&#24171;&#21161;. &#27489;&#36814;&#25353;&#27492;&#36899;&#32080;&#36865;&#20986;&#20320;&#30340;&#21332;&#21161;&#35201;&#27714;");
	define("NEW_SUPPORT_REQUEST_MSG", "&#26032;&#35531;&#27714;");
	define("SUPPORT_REQUEST_ADDED_MSG", "&#22810;&#35613;<br>&#25903;&#25588;&#20013;&#24515;&#26371;&#30433;&#24555;&#25552;&#20379;&#21332;&#21161;");
	define("SUPPORT_SELECT_DEP_MSG", "&#36984;&#25799;&#37096;&#38272;");
	define("SUPPORT_SELECT_PROD_MSG", "&#36984;&#25799;&#29986;&#21697;");
	define("SUPPORT_SELECT_STATUS_MSG", "&#36984;&#25799;&#29376;&#24907;");
	define("SUPPORT_NOT_VIEWED_MSG", "&#26410;&#38321;&#35712;");
	define("SUPPORT_VIEWED_BY_USER_MSG", "&#29992;&#25142;&#24050;&#38321;&#35712;");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "&#31649;&#29702;&#21729;&#24050;&#38321;&#35712;");
	define("SUPPORT_STATUS_NEW_MSG", "&#26032;");
	define("NO_SUPPORT_REQUEST_MSG", "&#27794;&#26377;&#35352;&#37636;");

	define("SUPPORT_SUMMARY_COLUMN", "&#32317;&#35261;");
	define("SUPPORT_TYPE_COLUMN", "&#39006;&#22411;");
	define("SUPPORT_UPDATED_COLUMN", "&#26368;&#24460;&#26356;&#26032;");

	define("SUPPORT_USER_NAME_FIELD", "&#20320;&#30340;&#21517;&#31281;");
	define("SUPPORT_USER_EMAIL_FIELD", "&#20320;&#30340;&#38651;&#37109;");
	define("SUPPORT_IDENTIFIER_FIELD", "еЏѓиЂѓ (еёіе–® #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "з”ўе“ЃзЁ®йЎћ");
	define("SUPPORT_DEPARTMENT_FIELD", "&#37096;&#38272;");
	define("SUPPORT_PRODUCT_FIELD", "&#29986;&#21697;");
	define("SUPPORT_TYPE_FIELD", "&#39006;&#22411;");
	define("SUPPORT_CURRENT_STATUS_FIELD", "&#29694;&#22312;&#29376;&#24907;");
	define("SUPPORT_SUMMARY_FIELD", "&#19968;&#35261;");
	define("SUPPORT_DESCRIPTION_FIELD", "&#35498;&#26126;");
	define("SUPPORT_MESSAGE_FIELD", "&#35338;&#24687;");
	define("SUPPORT_ADDED_FIELD", "&#22686;&#21152;");
	define("SUPPORT_ADDED_BY_FIELD", "&#26032;&#22686;&#32773;");
	define("SUPPORT_UPDATED_FIELD", "&#26368;&#24460;&#26356;&#26032;");

	define("SUPPORT_REQUEST_BUTTON", "&#36865;&#20986;&#35531;&#27714;");
	define("SUPPORT_REPLY_BUTTON", "&#22238;&#35206;");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Missing <b>Support ID</b> parameter");
	define("SUPPORT_MISS_CODE_ERROR", "Missing <b>Verification</b> parameter");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Support ID</b> parameter has wrong value");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verification</b> parameter has wrong value");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>