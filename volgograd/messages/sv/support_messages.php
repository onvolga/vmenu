<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Supportcenter");
	define("SUPPORT_REQUEST_INF_TITLE", "Frеga efter information");
	define("SUPPORT_REPLY_FORM_TITLE", "Svara");

	define("MY_SUPPORT_ISSUES_MSG", "Mina Support-frеgor");
	define("MY_SUPPORT_ISSUES_DESC", "Om du rеkar ut fцr nеgra problem med en produkt som du har kцpt, kan vеr support ta hand om dig. Vдnligen kontakta oss genom att klicka pе lдnken ovan fцr att skicka en fцrfrеgan till support. ");
	define("NEW_SUPPORT_REQUEST_MSG", "Ny fцrfrеgan");
	define("SUPPORT_REQUEST_ADDED_MSG", "Tack sе mycket!<br>Vеr support kommer fцrsцka hjдlpa dig sе snart som mцjligt.");
	define("SUPPORT_SELECT_DEP_MSG", "Vдlj omrеde");
	define("SUPPORT_SELECT_PROD_MSG", "Vдlj produkt");
	define("SUPPORT_SELECT_STATUS_MSG", "Vдlj status");
	define("SUPPORT_NOT_VIEWED_MSG", "Inte visad");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Visad av kund");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Visad av adminstrator");
	define("SUPPORT_STATUS_NEW_MSG", "Ny");
	define("NO_SUPPORT_REQUEST_MSG", "Inga supportfrеgor hittades");

	define("SUPPORT_SUMMARY_COLUMN", "Summering");
	define("SUPPORT_TYPE_COLUMN", "Typ");
	define("SUPPORT_UPDATED_COLUMN", "Senast uppdaterad");

	define("SUPPORT_USER_NAME_FIELD", "Ditt namn");
	define("SUPPORT_USER_EMAIL_FIELD", "Din epostadress");
	define("SUPPORT_IDENTIFIER_FIELD", "Fakturanummer");
	define("SUPPORT_ENVIRONMENT_FIELD", "Datamiljц (OS, Databas, Webserver, etc)");
	define("SUPPORT_DEPARTMENT_FIELD", "Avdelning");
	define("SUPPORT_PRODUCT_FIELD", "Produkt");
	define("SUPPORT_TYPE_FIELD", "Typ");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Nuvarande status");
	define("SUPPORT_SUMMARY_FIELD", "Enradssummering");
	define("SUPPORT_DESCRIPTION_FIELD", "Beskrivning");
	define("SUPPORT_MESSAGE_FIELD", "Meddelande");
	define("SUPPORT_ADDED_FIELD", "Inlagd");
	define("SUPPORT_ADDED_BY_FIELD", "Inlagd av");
	define("SUPPORT_UPDATED_FIELD", "Senast uppdaterad");

	define("SUPPORT_REQUEST_BUTTON", "Skicka fцrfrеgan");
	define("SUPPORT_REPLY_BUTTON", "Svara");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Saknar <b>Support-ID</b>-parameter");
	define("SUPPORT_MISS_CODE_ERROR", "Saknar <b>Verifikations</b>-paramenter");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Support-ID</b>-parametern har fel vдrde");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verifikations</b>-parameter har fel vдrde");

	define("MAIL_DATA_MSG", "Maildata");
	define("HEADERS_MSG", "Rubriker");
	define("ORIGINAL_TEXT_MSG", "Originaltext");
	define("ORIGINAL_HTML_MSG", "Original-HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Tyvдrr, men du har inte tillеtelse att stдnga etiketter.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Tyvдrr, men du har inte tillеtelse att svara pе etiketter.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Tyvдrr, men du har inte tillеtelse att skapa nya etiketter.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Tyvдrr, men du har inte tillеtelse att ta bort etiketter.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Tyvдrr, men du har inte tillеtelse att uppdatera etiketter.<br>");
	define("NO_TICKETS_FOUND_MSG", "Inga etiketter hittades.");
	define("HIDDEN_TICKETS_MSG", "Gцmda etiketter");
	define("ALL_TICKETS_MSG", "Alla etiketter");
	define("ACTIVE_TICKETS_MSG", "Aktivera etiketter");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "Du har inte tillgеng till denna avdelningen.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "Du har inte tillgеng till nеgon avdelning.");
	define("REPLY_TO_NAME_MSG", "Svara till {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Kunskapskategori");
	define("KNOWLEDGE_TITLE_MSG", "Kunskapstitel");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Kunskapsartikel status");
	define("SELECT_RESPONSIBLE_MSG", "Vдlj ansvarsomrеde");

?>