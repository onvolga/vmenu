<?php

	$root_folder_path = (isset($is_admin_path) && $is_admin_path) ? "../" : "./";
	include_once($root_folder_path . "includes/pdflib.php");
	include_once($root_folder_path . "includes/pdf.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/parameters.php");
	include_once($root_folder_path . "includes/shopping_cart.php");

	@ini_set("max_execution_time", 200);
	function pdf_packing_slip($orders_ids)
	{
		global $db, $table_prefix, $settings, $currency, $parameters, $site_id;
		global $is_admin_path, $root_folder_path, $date_show_format;

		// output buffer
		$pdf_buffer = "";

		// additional connection
		$dbi = new VA_SQL();
		$dbi->DBType      = $db->DBType      ;
		$dbi->DBDatabase  = $db->DBDatabase  ;
		$dbi->DBUser      = $db->DBUser      ;
		$dbi->DBPassword  = $db->DBPassword  ;
		$dbi->DBHost      = $db->DBHost      ;
		$dbi->DBPort      = $db->DBPort      ;
		$dbi->DBPersistent= $db->DBPersistent;

		// get initial invoice settings
		$packing = array();
		$sql  = " SELECT setting_name, setting_value FROM " . $table_prefix . "global_settings";
		$sql .= " WHERE setting_type='printable'";
		if (isset($site_id)) {
			$sql .= "AND (site_id=1 OR site_id=" . $db->tosql($site_id, INTEGER, true, false) . ")";
			$sql .= "ORDER BY site_id ASC";
		} else {
			$sql .= "AND site_id=1";
		}
		$db->query($sql);
		while ($db->next_record()) {
			$packing[$db->f("setting_name")] = $db->f("setting_value");
		}

		// global variables
		$site_url	= get_setting_value($settings, "site_url", "");
		$secure_url	= get_setting_value($settings, "secure_url", "");
		$tmp_dir = get_setting_value($settings, "tmp_dir", "");
		$tmp_images = array();
		$packing_sets = array();
		$order_sets = array();

		// General PDF settings
		$pdf_library = isset($packing["pdf_lib"]) ? $packing["pdf_lib"] : 1;
		if ($pdf_library == 2) {
			$pdf = new VA_PDFLib();
		} else {
			$pdf = new VA_PDF();
		}
		$pdf->set_creator("admin_packing_pdf.php");
		$pdf->set_author("ViArt Ltd");
		$pdf->set_title(PACKING_SLIP_NO_MSG . $orders_ids);
		$pdf->set_font_encoding(CHARSET);
		$page_number = 0;

		// general order fields settings
		$r = new VA_Record($table_prefix . "orders");
		$r->add_where("order_id", INTEGER);
		$r->add_textbox("site_id", INTEGER);
		for ($i = 0; $i < sizeof($parameters); $i++) {
			$r->add_textbox($parameters[$i], TEXT);
			$r->add_textbox("delivery_" . $parameters[$i], TEXT);
		}
		$r->add_textbox("invoice_number", TEXT);
		$r->add_textbox("invoice_copy_number", TEXT);
		$r->add_textbox("order_status", INTEGER);
		$r->add_hidden("order_status_type", TEXT);
		$r->add_textbox("user_id", INTEGER);
		$r->add_textbox("payment_id", INTEGER);
		$r->add_textbox("order_placed_date", DATETIME);
		$r->add_textbox("currency_code", TEXT);
		$r->add_textbox("currency_rate", NUMBER);
		$r->add_textbox("shipping_tracking_id", TEXT);
		$r->add_textbox("remote_address", TEXT);
		$r->add_textbox("cc_name", TEXT);
		$r->add_textbox("cc_first_name", TEXT);
		$r->add_textbox("cc_last_name", TEXT);
		$r->add_textbox("cc_number", TEXT);
		$r->add_textbox("cc_start_date", DATETIME);
		$r->change_property("cc_start_date", VALUE_MASK, array("MM", " / ", "YYYY"));
		$r->add_textbox("cc_expiry_date", DATETIME);
		$r->change_property("cc_expiry_date", VALUE_MASK, array("MM", " / ", "YYYY"));
		$r->add_textbox("cc_type", INTEGER);
		$r->add_textbox("cc_issue_number", INTEGER);
		$r->add_textbox("cc_security_code", TEXT);
		$r->add_textbox("pay_without_cc", TEXT);
		$r->add_textbox("tax_name", TEXT);
		$r->add_textbox("tax_percent", NUMBER);
		$r->add_textbox("tax_total", NUMBER);
		$r->add_textbox("tax_prices_type", INTEGER);
		$r->add_textbox("tax_round", INTEGER);
		$r->change_property("tax_round", USE_IN_SELECT, false);
		$r->add_textbox("total_discount", NUMBER);
		$r->add_textbox("total_discount_tax", NUMBER);
		$r->add_textbox("shipping_type_desc", TEXT);
		$r->add_textbox("shipping_cost", NUMBER);
		$r->add_textbox("shipping_taxable", NUMBER);
		$r->add_textbox("credit_amount", NUMBER);
		$r->add_textbox("processing_fee", NUMBER);
		$r->add_textbox("order_total", NUMBER);

		$ids = explode(",", $orders_ids);
		for ($id = 0; $id < sizeof($ids); $id++)
		{
			$order_id = $ids[$id];
			$r->set_value("order_id", $order_id);
			$r->get_db_values();
			$order_site_id = $r->get_value("site_id");
			$order_status = $r->get_value("order_status");
			$tmp_images = array(); // array where we save all temporary images

			if (isset($packing_sets[$order_site_id])) {
				$packing = $packing_sets[$order_site_id];
				$order_info = $order_sets[$order_site_id];
			} else {
				// get packing settings for current order
				$packing = array();
				$sql  = " SELECT setting_name, setting_value FROM " . $table_prefix . "global_settings";
				$sql .= " WHERE setting_type='printable'";
				if ($order_site_id) {
					$sql .= "AND (site_id=1 OR site_id=" . $db->tosql($order_site_id, INTEGER, true, false) . ")";
					$sql .= "ORDER BY site_id ASC";
				} else {
					$sql .= "AND site_id=1";
				}
				$db->query($sql);
				while ($db->next_record()) {
					$packing[$db->f("setting_name")] = $db->f("setting_value");
				}
	  
				// get order fields settings for current order
				$order_info = array();
				$sql  = " SELECT setting_name, setting_value FROM " . $table_prefix . "global_settings";
				$sql .= " WHERE setting_type='order_info'";
				if (isset($order_site_id)) {
					$sql .= "AND (site_id=1 OR site_id=" . $db->tosql($order_site_id, INTEGER, true, false) . ")";
					$sql .= "ORDER BY site_id ASC";
				} else {
					$sql .= "AND site_id=1";
				}
				$db->query($sql);
				while ($db->next_record()) {
					$order_info[$db->f("setting_name")] = $db->f("setting_value");
				}
	  
				$packing_sets[$order_site_id] = $packing;
				$order_sets[$order_site_id] = $order_info;
			}

			// check parameters list to hide
			$personal_number = 0; $delivery_number = 0;
			for ($i = 0; $i < sizeof($parameters); $i++)
			{
				$personal_param = "show_" . $parameters[$i];
				$delivery_param = "show_delivery_" . $parameters[$i];
				if (isset($order_info[$personal_param]) && $order_info[$personal_param] == 1) {
					$personal_number++;
					$r->parameters[$parameters[$i]][SHOW] = true;
				} else {
					$r->parameters[$parameters[$i]][SHOW] = false;
				}
				if (isset($order_info[$delivery_param]) && $order_info[$delivery_param] == 1) {
					$delivery_number++;
					$r->parameters["delivery_" . $parameters[$i]][SHOW] = true;
				} else {
					$r->parameters["delivery_" . $parameters[$i]][SHOW] = false;
				}
			}


			// codes settings 
			$packing_image = get_setting_value($packing, "packing_image", 0);
			$show_item_code = get_setting_value($packing, "item_code_packing", 0);
			$show_manufacturer_code = get_setting_value($packing, "manufacturer_code_packing", 0);
			$sc_properties_packing = get_setting_value($packing, "sc_properties_packing", 0);
			$shipping_method_packing = get_setting_value($packing, "shipping_method_packing", 0);
	  
			// image settings
			if ($packing_image) {
				$site_url = get_setting_value($settings, "site_url", "");
				$restrict_products_images = get_setting_value($settings, "restrict_products_images", "");		
				product_image_fields($packing_image, $image_type_name, $image_field, $image_alt_field, $watermark, $product_no_image);			
				$item_image_tmp_dir  = get_setting_value($settings, "tmp_dir", $root_folder_path);
				$item_image_position = 0;
				if ($packing_image == 1) {
					$max_image_width  = get_setting_value($settings, "tiny_image_max_width", 100);
					$item_image_position = 1;
				} elseif ($packing_image == 2) {
					$max_image_width  = get_setting_value($settings, "small_image_max_width", 200);
					$item_image_position = 1;
				} elseif ($packing_image == 3) {
					$max_image_width  = get_setting_value($settings, "big_image_max_width", 300);
					$item_image_position = 2;
				}			
			}

			$columns = array(
				"item_image" => array("name" => IMAGE_MSG, "active" => $packing_image, "align" => "center"), 
				"quantity" => array("name" => QTY_MSG, "active" => true, "align" => "center"), 
				"item_name" => array("name" => PROD_TITLE_COLUMN, "active" => true, "align" => "left"), 
				"item_code" => array("name" => PROD_CODE_MSG, "active" => $show_item_code, "align" => "center"), 
				"manufacturer_code" => array("name" => MANUFACTURER_CODE_MSG, "active" => $show_manufacturer_code, "align" => "center"), 
			);
			foreach ($columns as $column_name => $column_values) {
				$columns[$column_name]["width"] = 0;
				$columns[$column_name]["start"] = 0;
			}
	  
			// get order currency
			$order_currency_code = $r->get_value("currency_code");
			$order_currency_rate = $r->get_value("currency_rate");
			$currency = get_currency($order_currency_code);
	  
			$order_placed_date = $r->get_value("order_placed_date");
			$order_date = va_date($date_show_format, $order_placed_date);
			//$t->set_var("order_date", $order_date);
    
			$r->set_value("company_id", get_translation(get_db_value("SELECT company_name FROM " . $table_prefix . "companies WHERE company_id=" . $db->tosql($r->get_value("company_id"), INTEGER))));
			$r->set_value("state_id", get_translation(get_db_value("SELECT state_name FROM " . $table_prefix . "states WHERE state_id=" . $db->tosql($r->get_value("state_id"), INTEGER))));
			$r->set_value("country_id", get_translation(get_db_value("SELECT country_name FROM " . $table_prefix . "countries WHERE country_id=" . $db->tosql($r->get_value("country_id"), INTEGER))));
			$r->set_value("delivery_company_id", get_translation(get_db_value("SELECT company_name FROM " . $table_prefix . "companies WHERE company_id=" . $db->tosql($r->get_value("delivery_company_id"), INTEGER))));
			$r->set_value("delivery_state_id", get_translation(get_db_value("SELECT state_name FROM " . $table_prefix . "states WHERE state_id=" . $db->tosql($r->get_value("delivery_state_id"), INTEGER))));
			$r->set_value("delivery_country_id", get_translation(get_db_value("SELECT country_name FROM " . $table_prefix . "countries WHERE country_id=" . $db->tosql($r->get_value("delivery_country_id"), INTEGER))));
			$r->set_value("cc_type", get_translation(get_db_value("SELECT credit_card_name FROM " . $table_prefix . "credit_cards WHERE credit_card_id=" . $db->tosql($r->get_value("cc_type"), INTEGER))));


			// parse properties
			$orders_properties = array(); $cart_properties = array(); $personal_properties = array();
			$delivery_properties = array(); $payment_properties = array();
			$properties_total = 0; $properties_taxable = 0;
			$sql  = " SELECT op.property_id, op.property_type, op.property_name, op.property_value, ";
			$sql .= " op.property_value, op.property_price, op.property_points_amount, op.tax_free, ocp.control_type ";
			$sql .= " FROM (" . $table_prefix . "orders_properties op ";
			$sql .= " INNER JOIN " . $table_prefix . "order_custom_properties ocp ON op.property_id=ocp.property_id)";
			$sql .= " WHERE op.order_id=" . $db->tosql($order_id, INTEGER);
			$sql .= " ORDER BY op.property_order, op.property_id ";
			$db->query($sql);
			while ($db->next_record()) {
				$property_id = $db->f("property_id");
				$property_type = $db->f("property_type");
				$property_name = get_translation($db->f("property_name"));
				$property_value = get_translation($db->f("property_value"));
				$property_price = $db->f("property_price");
				$property_points_amount = $db->f("property_points_amount");
				$property_tax_free = $db->f("tax_free");
				$control_type = $db->f("control_type");
				$properties_total += $property_price;
				if ($property_tax_free != 1) {
					$properties_taxable += $property_price;
				}
    
				// check value description
				if (($control_type == "CHECKBOXLIST" ||  $control_type == "RADIOBUTTON" || $control_type == "LISTBOX") && is_numeric($property_value)) {
					$sql  = " SELECT property_value FROM " . $table_prefix . "order_custom_values ";
					$sql .= " WHERE property_value_id=" . $dbi->tosql($property_value, INTEGER);
					$dbi->query($sql);
					if ($dbi->next_record()) {
						$property_value = get_translation($dbi->f("property_value"));
					}
				}
				if (isset($orders_properties[$property_id])) {
					$orders_properties[$property_id]["value"] .= "; " . $property_value;
					$orders_properties[$property_id]["price"] += $property_price;
					$orders_properties[$property_id]["points_amount"] += $property_points_amount;
				} else {
					$orders_properties[$property_id] = array(
						"type" => $property_type, "name" => $property_name, "value" => $property_value, 
						"price" => $property_price, "points_amount" => $property_points_amount, "tax_free" => $property_tax_free,
					);
				}
	  
				// save data by arrays
				if ($property_type == 1) {
				  $cart_properties[$property_id] = $orders_properties[$property_id];
				} elseif ($property_type == 2) {
					$personal_properties[$property_id] = $orders_properties[$property_id];
				} elseif ($property_type == 3) {
					$delivery_properties[$property_id] = $orders_properties[$property_id];
				} elseif ($property_type == 4) {
					$payment_properties[$property_id] = $orders_properties[$property_id];
				}
			}

			$packing_slips_ids = array();
			$sql  = " SELECT oe.order_items FROM (" . $table_prefix . "orders_events oe ";
			$sql .= " LEFT JOIN " . $table_prefix . "order_statuses os ON os.status_id=oe.status_id) ";
			$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
			$sql .= " AND os.is_dispatch=1 ";
			$db->query($sql);
			if ($db->next_record()) {
			 	do {
					$packing_slips_ids[] = $db->f("order_items");
				} while ($db->next_record());
			} else {
				$packing_slips_ids[] = "";
			}
	  
			// get order items data
			$items_ids = array(); $packing_slips = array();
			for ($ps = 0; $ps < sizeof($packing_slips_ids); $ps++) {
				$order_items = $packing_slips_ids[$ps];
				$sql  = " SELECT * FROM " . $table_prefix . "orders_items ";
				$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
				if (strlen($order_items)) {
					$sql .= " AND order_item_id IN (" . $db->tosql($order_items, TEXT, false) . ") ";
				}
				$db->query($sql);
				while ($db->next_record()) {
					$order_item_id = $db->f("order_item_id");
					$item_id = $db->f("item_id");
	  
					$items_ids[] = $item_id;
					$packing_slips[$ps][$order_item_id] = array(
						"item_id" => $db->f("item_id"),
						"order_item_id" => $db->f("order_item_id"),
						"quantity" => $db->f("quantity"),
						"item_name" => strip_tags(get_translation($db->f("item_name"))),
						"item_code" => $db->f("item_code"),
						"manufacturer_code" => $db->f("manufacturer_code"),
					);
				}
			}

			// check product images
			$images = array(); $max_image_width = 0;
			if ($packing_image && $image_field) {
	  
				$sql  = " SELECT item_id, " . $image_field; 
				$sql .= " FROM " . $table_prefix . "items";
				$sql .= " WHERE item_id IN (" . $db->tosql($items_ids, INTEGERS_LIST) . ")";
				$db->query($sql);			
				while ($db->next_record()) {
					$image_height = 0;
					$item_id = $db->f("item_id");
					$item_image = $db->f($image_field);
					if (!strlen($item_image)) {
						$item_image = $product_no_image;
						$image_exists = false;
					} else {
						$image_exists = true;
					}
	  
					$item_image_tmp_created = false;
					if ($item_image) {
						$pos = strrpos($item_image, '.');
						if (!$pos) {
							$item_image_type = "jpg";
						}
						$item_image_type = substr($item_image, $pos+1);

						$item_image_tmp_name = $item_image_tmp_dir . "tmp_pdf_".$item_id ."_". md5(uniqid(rand(), true)) .".". $item_image_type;
						$item_image = str_replace($settings['site_url'], '', $item_image);
						if (preg_match("/^http\:\/\//", $item_image)) {
							$item_image  = "";
						} else {						
							if ($site_url && $image_exists && ($watermark || $restrict_products_images)) {
								if ($item_image_tmp_dir) {
									if (!file_exists($item_image_tmp_name)) {
										$item_image = $site_url . "image_show.php?item_id=".$item_id."&type=".$image_type_name."&vc=".md5($item_image);
										$out = fopen($item_image_tmp_name, 'wb');
										$item_image_tmp_created = true;
										if (function_exists("curl_init") && $out) {	
											$ch = curl_init();
											curl_setopt($ch, CURLOPT_FILE, $out);
											curl_setopt($ch, CURLOPT_HEADER, 0);
											curl_setopt($ch, CURLOPT_URL, $item_image);
											curl_exec($ch);
											if (curl_errno($ch)) {
												$item_image = "";
											} else {
												$item_image = $item_image_tmp_name;
												$tmp_images[] = $item_image_tmp_name;
											}
											curl_close($ch);
											fclose($out);
										} else {
											$item_image = "";
										}
									} else {
										$item_image  = $item_image_tmp_name;
									}
								} else {
									$item_image = "";
								}
							} else {
								if ($is_admin_path) {
									$item_image  = $root_folder_path . $item_image;
								}
							}
						}
					}
					// check image size
					$image_width = ""; $image_height = "";
					if ($item_image) {
						$image_size = @getimagesize($item_image);
						if (is_array($image_size)) {
							$image_width = $image_size[0];
							$image_height = $image_size[1];
							if ($image_width > $max_image_width) { $max_image_width = $image_width; }
						}
					}
					// save image
					$images[$item_id] = array(
						"file" => $item_image,
						"type" => $item_image_type,
						"width" => $image_width,
						"height" => $image_height,
					);
				}
			}


			// check what columns to show and where
			$column_end = 40;
			// left space for image
			if ($max_image_width <= 0) { $max_image_width = 50; }
			if ($packing_image && $item_image_position == 1 && $max_image_width > 0) {
				$columns["item_image"]["start"] = $column_end;
				$columns["item_image"]["width"] = $max_image_width;
				$column_end += $max_image_width;
			}
	  
			// quantity field
			$columns["quantity"]["start"] = $column_end;
			$columns["quantity"]["width"] = 30;
			$column_end += $columns["quantity"]["width"];
	  
			// check how many columns left and calculate column width
			$columns_left = 1;
			if ($show_item_code) { $columns_left++; }
			if ($show_manufacturer_code) { $columns_left++; }
			$width_left = 515 - ($column_end - 40);
			$average_width = intval($width_left / $columns_left);
	  
			// title field
			$columns["item_name"]["start"] = $column_end;
			$columns["item_name"]["width"] = $average_width;
			$column_end += $average_width;
	  
			if ($show_item_code) { 
				$columns["item_code"]["start"] = $column_end;
				$columns["item_code"]["width"] = $average_width;
				$column_end += $average_width;
			}
			if ($show_manufacturer_code) { 
				$columns["manufacturer_code"]["start"] = $column_end;
				$columns["manufacturer_code"]["width"] = $average_width;
				$column_end += $average_width;
			}


			for ($ps = 0; $ps < sizeof($packing_slips); $ps++) {
				packing_slip_new_page($pdf, $height_position, $page_number, $packing);
				packing_slip_header($pdf, $height_position, $packing, $r);
				packing_slip_user_info($pdf, $height_position, $r, $personal_number, $delivery_number, $personal_properties, $delivery_properties, $currency);
				packing_slip_table_header($pdf, $height_position, $r, $columns);

				$order_items = $packing_slips[$ps];
				foreach ($order_items as $order_item_id => $order_item) {
					if ($height_position < 140) {
						$pdf->end_page();
						packing_slip_new_page($pdf, $height_position, $page_number, $packing);
						packing_slip_table_header($pdf, $height_position, $r, $columns);
					}
    
    
					$item_id = $order_item["item_id"];
					$order_item_id = $order_item["order_item_id"];
					$quantity = $order_item["quantity"];
					$item_name = strip_tags(get_translation($order_item["item_name"]));
					$item_code = $order_item["item_code"];
					$manufacturer_code = $order_item["manufacturer_code"];
    
					$image_height = 0;
					if ($packing_image && isset($images[$item_id])) { 
						$item_image = $images[$item_id]["file"];
						$image_height = $images[$item_id]["height"];
						$item_image_type = $images[$item_id]["type"];
						$pdf->place_image($item_image, $columns["item_image"]["start"], $height_position - $image_height, $item_image_type);	
					}
	  
					// set font for quantity and title
					$pdf->setfont("helvetica", "", 9);
					// set quantity and product title
					$qty_height = $pdf->show_xy($quantity, $columns["quantity"]["start"] + 4, $height_position - 2, $columns["quantity"]["width"] - 6, 0, $columns["quantity"]["align"]);
					$item_height = $pdf->show_xy($item_name, $columns["item_name"]["start"] + 4, $height_position - 2, $columns["item_name"]["width"] - 6, 0);
					// set smaller font for product additional information
					$pdf->setfont("helvetica", "", 8);
	  
					// show item properties 
					$properties_height = 0;
					$sql  = " SELECT property_name, property_value FROM " . $table_prefix . "orders_items_properties ";
					$sql .= " WHERE order_item_id=" . $db->tosql($order_item_id, INTEGER);
					$dbi->query($sql);
					while ($dbi->next_record()) {
						$item_height += 2;
						$property_name = strip_tags(get_translation($dbi->f("property_name")));
						$property_value = strip_tags(get_translation($dbi->f("property_value")));
						$property_line = $property_name . ": " . $property_value;
						$property_height = $pdf->show_xy($property_line, $columns["item_name"]["start"] + 8, $height_position - $item_height - 2, $columns["item_name"]["width"] - 10, 0);
						$item_height += $property_height;
					}
					//end
	  
					// add additional indent after each product
					$item_height += 6;
					$row_height = $item_height;
    
					$item_code_image_height = 0; $manufacturer_code_image_height = 0;
					$item_code_height = 0; $manufacturer_code_height = 0; 
					if (strlen($item_code)) {
						if ($show_item_code == 1) {
							$item_code_height = $pdf->show_xy($item_code, $columns["item_code"]["start"], $height_position - 2, $columns["item_code"]["width"], 0, "center");
							$item_code_height += 6;
						} elseif ($show_item_code == 2) {
							$image_type = "png";
							if ($tmp_dir) {
								$tmp_dir = get_setting_value($settings, "tmp_dir", "");
								$item_code_image = $tmp_dir . "tmp_" . md5(uniqid(rand(), true)) . "." . $image_type;
								save_barcode ($item_code_image, $item_code, $image_type, "code128");
								$tmp_images[] = $item_code_image;
							} else {
								$item_code_image = $settings["site_url"] . "barcode_image.php?text=" . $item_code;
							}
			        $image_size = @GetImageSize($item_code_image);
							if (is_array($image_size)) {
								$image_width = $image_size[0];
								$item_code_image_height = $image_size[1];
							} else {
								$image_width = 100;
								$item_code_image_height = 100;
							}
							$item_code_image_height += 10; // additional pixels for better positioning by center
							if ($average_width > $image_width) {
								$image_shift = round(($average_width - $image_width) / 2);
							}	else {
								$image_shift = 0;
							}
						  $pdf->place_image($item_code_image, $columns["item_code"]["start"] + $image_shift, $height_position - $item_code_image_height + 5, $image_type);
						}
					}
					if (strlen($manufacturer_code)) {
						if ($show_manufacturer_code == 1) {
							$manufacturer_code_height  = $pdf->show_xy($manufacturer_code, $columns["manufacturer_code"]["start"], $height_position - 2, $columns["manufacturer_code"]["width"], 0, "center");
							$manufacturer_code_height += 6;
						} elseif ($show_manufacturer_code == 2) {
							$image_type = "png";
							if ($tmp_dir) {
								$tmp_dir = get_setting_value($settings, "tmp_dir", "");
								$manufacturer_code_image = $tmp_dir . "tmp_" . md5(uniqid(rand(), true)) . "." . $image_type;
								save_barcode ($manufacturer_code_image, $manufacturer_code, $image_type, "code128");
								$tmp_images[] = $manufacturer_code_image;
							} else {
								$manufacturer_code_image = $settings["site_url"] . "barcode_image.php?text=" . $manufacturer_code;
							}
			        $image_size = @GetImageSize($manufacturer_code_image);
							if (is_array($image_size)) {
								$image_width = $image_size[0];
								$manufacturer_code_image_height = $image_size[1];
							} else {
								$image_width = 100;
								$manufacturer_code_image_height = 100;
							}
							$manufacturer_code_image_height += 10; // additional pixels for better positioning
							if ($average_width > $image_width) {
								$image_shift = round(($average_width - $image_width) / 2);
							}	else {
								$image_shift = 0;
							}
						  $pdf->place_image($manufacturer_code_image, $columns["manufacturer_code"]["start"]+ $image_shift, $height_position - $manufacturer_code_image_height + 5, $image_type);
						}
					}
	  
	  
					if ($image_height > $row_height)  {
						$row_height = $image_height;
					}
					if ($item_code_image_height > $row_height) {
						$row_height = $item_code_image_height;
					}
					if ($manufacturer_code_image_height > $row_height) {
						$row_height = $manufacturer_code_image_height;
					}
					if ($item_code_height > $row_height) {
						$row_height = $item_code_height;
					}
					if ($manufacturer_code_height > $row_height) {
						$row_height = $manufacturer_code_height;
					}
	  
					$height_position -= $row_height;
	  
					// show table row  
					$pdf->setlinewidth(1.0);
					$pdf->rect (40, $height_position, 515, $row_height);
					foreach ($columns as $column_name => $values) {
						if ($values["active"]) {
							$pdf->line( $values["start"], $height_position, $values["start"], $height_position + $row_height);
						}
					}
					// end table row  
				}
	  
				// show shopping cart properties
				if ($sc_properties_packing) {
					$height_position -= 2;
					foreach ($cart_properties as $property_id => $property_values) {
						$property_price = $property_values["price"];
						$property_tax_id = 0;
						$property_tax = get_tax_amount("", 0, $property_price, 1, $property_tax_id, $property_values["tax_free"], $property_tax_percent);
						$property_height = $pdf->show_xy($property_values["name"] . ": " . $property_values["value"], 50, $height_position - 2, 500, 0, "left");
						$height_position -= ($property_height + 2);
					}
				}
	  
				if ($shipping_method_packing) {
					$shipping_type_desc = get_translation($r->get_value("shipping_type_desc"));
					if (strlen($shipping_type_desc)) {
						$height_position -= 2;
						$shipping_height = $pdf->show_xy(PROD_SHIPPING_MSG . ": " . $shipping_type_desc, 50, $height_position - 2, 500, 0, "left");
						$height_position -= ($shipping_height + 2);
					}
				}
	  
				packing_slip_footer($pdf, $height_position, $packing, $r);
				$pdf->end_page();
			}

			// clearing temporary images
			for ($t = 0; $t < sizeof($tmp_images); $t++) {
				unlink($tmp_images[$t]);
			}

		}

		$pdf_buffer = $pdf->get_buffer();
		return $pdf_buffer;
	}


	function packing_slip_new_page(&$pdf, &$height_position, &$page_number, $packing)
	{
		$page_number++;
		$pdf_library = isset($packing["pdf_page_type"]) ? $packing["pdf_page_type"] : "A4";
		if($pdf_library == "LETTER"){
			$pdf->begin_page(612, 792);
		$height_position = 750;
		}else{
			$pdf->begin_page(595, 842);
			$height_position = 800;
		}
	
		$pdf->setfont ("helvetica", "", 8);
		if ($page_number > 1) {
			//$pdf->show_xy("- " . $page_number . " -", 40, 20, 555, 0, "center");
		}
	}
	
	function packing_slip_header(&$pdf, &$height_position, $packing, $r)
	{
		global $db, $table_prefix, $date_show_format;
	
		$order_id = $r->get_value("order_id");
		$invoice_number = $r->get_value("invoice_number");
		if (!$invoice_number) { $invoice_number = $order_id; }
		$order_placed_date = $r->get_value("order_placed_date");
		$order_date = va_date($date_show_format, $order_placed_date);

		$image_height = 0;
		$start_position = $height_position;
		if (isset($packing["packing_logo"]) && strlen($packing["packing_logo"])) {
			$image_path = $packing["packing_logo"];
			if (!file_exists($image_path)) {
				if (preg_match("/^\.\.\//", $image_path)) {
					if (@file_exists(preg_replace("/^\.\.\//", "", $image_path))) {
						$image_path = preg_replace("/^\.\.\//", "", $image_path);
					}
				} else if (@file_exists("../".$image_path)) {
					$image_path = "../" . $image_path;
				}
			}
			$image_size = @GetImageSize($image_path);
			$image_width = $image_size[0];
			$image_height = $image_size[1];
			if ($image_width > 0 && $image_height > 0) {
				if (preg_match("/((\.jpeg)|(\.jpg))$/i", $image_path)) {
					$image_type = "jpeg";
				} elseif (preg_match("/(\.gif)$/i", $image_path)) {
					$image_type = "gif";
				} elseif (preg_match("/((\.tif)|(\.tiff))$/i", $image_path)) {
					$image_type = "tiff";
				} elseif (preg_match("/(\.png)$/i", $image_path)) {
					$image_type = "png";
				}
			  $pdf->place_image($image_path, 555 - $image_width, $height_position - $image_height, $image_type);
			}
		}
	
		if (isset($packing["packing_header"])) {
			$packing_header = strip_tags($packing["packing_header"]);
			if (strlen($packing_header)) {
				$pdf->setfont("helvetica", "", 10);
				$header_lines = explode("\n", $packing_header);
				for ($i = 0; $i < sizeof($header_lines); $i++) {
					$header_line = $header_lines[$i];
					$line_height = $pdf->show_xy($header_line, 40, $height_position, 200, 0, "left");
					$height_position -= ($line_height + 2);
				}
			}
		}
	
		$height_position -= 12;
		$pdf->setfont("helvetica", "B", 10);
		$pdf->show_xy(INVOICE_DATE_MSG . ":", 40, $height_position, 80, 0, "left");
		$pdf->setfont("helvetica", "", 10);
		$pdf->show_xy($order_date, 120, $height_position, 200, 0, "left");
	
		$height_position -= 12;
		$pdf->setfont("helvetica", "B", 10);
		$pdf->show_xy(INVOICE_NUMBER_MSG . ":", 40, $height_position, 80, 0, "left");
		$pdf->setfont("helvetica", "", 10);
		$pdf->show_xy($invoice_number, 120, $height_position, 200, 0, "left");
	
		if ($height_position > ($start_position - $image_height)) {
			$height_position = $start_position - $image_height;
		}
	}
	
	
	function packing_slip_table_header(&$pdf, &$height_position, $r, $columns)
	{
		$pdf->setlinewidth(1.0);
	
		$pdf->setfont("helvetica", "B", 8);
		$height_position -= 12;
	
		$max_height = 12;
		foreach ($columns as $column_name => $values) {
			if ($values["active"]) {
				$column_height = $pdf->show_xy($values["name"], $values["start"] + 1, $height_position - 2, $values["width"] - 2, 0, "center");
				if ($column_height > $max_height) {
					$max_height = $column_height;
				}
			}
		}
		$max_height += 6;
		$pdf->rect ( 40, $height_position - $max_height, 515, $max_height);
		foreach ($columns as $column_name => $values) {
			if ($values["active"]) {
				$pdf->line( $values["start"], $height_position - $max_height, $values["start"], $height_position);
			}
		}
		$height_position -= $max_height;
	}


	function packing_slip_user_info(&$pdf, &$height_position, $r, $personal_number, $delivery_number, $personal_properties, $delivery_properties, $currency)
	{
		$pdf->setfont ("helvetica", "BU", 10);
		$height_position -= 24;
		$pdf->show_xy(INVOICE_TO_MSG.":", 40, $height_position, 250, 0, "left");
		if ($delivery_number > 0) {
			$pdf->show_xy(DELIVERY_TO_MSG.":", 300, $height_position, 250, 0, "left");
		}
	
		$personal_height = $height_position;
		$pdf->setfont("helvetica", "", 10);
	
		$name = "";
		if ($r->parameters["name"][SHOW]) {
			$name = $r->get_value("name");
		}
		if ($r->parameters["first_name"][SHOW] && $r->get_value("first_name")) {
			if ($name) { $name .= " "; }
			$name .= $r->get_value("first_name");
		}
		if ($r->parameters["last_name"][SHOW] && $r->get_value("last_name")) {
			if ($name) { $name .= " "; }
			$name .= $r->get_value("last_name");
		}
	
		if (strlen($name)) {
			$personal_height -= 12;
			$pdf->show_xy($name, 40, $personal_height, 250, 0, "left");
		}
	
		if ($r->parameters["company_id"][SHOW] && $r->get_value("company_id")) {
			$personal_height -= 12;
			$pdf->show_xy($r->get_value("company_id"), 40, $personal_height, 250, 0, "left");
		}
		if ($r->parameters["company_name"][SHOW] && $r->get_value("company_name")) {
			$personal_height -= 12;
			$pdf->show_xy($r->get_value("company_name"), 40, $personal_height, 250, 0, "left");
		}
	
		if ($r->parameters["address1"][SHOW] && $r->get_value("address1")) {
			$personal_height -= 12;
			$pdf->show_xy($r->get_value("address1"), 40, $personal_height, 250, 0, "left");
		}
		if ($r->parameters["address2"][SHOW] && $r->get_value("address2")) {
			$personal_height -= 12;
			$pdf->show_xy($r->get_value("address2"), 40, $personal_height, 250, 0, "left");
		}
		$city = ""; $address_line = "";
		if ($r->parameters["city"][SHOW]) {
			$city = $r->get_value("city");
		}
		if ($r->parameters["province"][SHOW]) {
			$address_line = $r->get_value("province");
		}
		if ($r->parameters["state_id"][SHOW] && $r->get_value("state_id")) {
			if ($address_line) { $address_line .= " "; }
			$address_line .= $r->get_value("state_id");
		}
		if ($r->parameters["zip"][SHOW] && $r->get_value("zip")) {
			if ($address_line) { $address_line .= " "; }
			$address_line .= $r->get_value("zip");
		}
		if (strlen($city) && strlen($address_line)) {
			$address_line = $city . ", " . $address_line;
		} elseif (strlen($city)) {
			$address_line = $city;
		}
	
		if (strlen($address_line)) {
			$personal_height -= 12;
			$pdf->show_xy($address_line, 40, $personal_height, 250, 0, "left");
		}
	
		if ($r->parameters["country_id"][SHOW] && $r->get_value("country_id")) {
			$personal_height -= 12;
			$pdf->show_xy($r->get_value("country_id"), 40, $personal_height, 250, 0, "left");
		}
	
		if ($r->parameters["phone"][SHOW] && !$r->is_empty("phone")) {
			$personal_height -= 12;
			$pdf->show_xy(PHONE_FIELD.": ".$r->get_value("phone"), 40, $personal_height, 250, 0, "left");
		}
		if ($r->parameters["daytime_phone"][SHOW] && !$r->is_empty("daytime_phone")) {
			$personal_height -= 12;
			$pdf->show_xy(DAYTIME_PHONE_FIELD.": ".$r->get_value("daytime_phone"), 40, $personal_height, 250, 0, "left");
		}
		if ($r->parameters["evening_phone"][SHOW] && !$r->is_empty("evening_phone")) {
			$personal_height -= 12;
			$pdf->show_xy(EVENING_PHONE_FIELD.": ".$r->get_value("evening_phone"), 40, $personal_height, 250, 0, "left");
		}
		if ($r->parameters["cell_phone"][SHOW] && !$r->is_empty("cell_phone")) {
			$personal_height -= 12;
			$pdf->show_xy(CELL_PHONE_FIELD.": ".$r->get_value("cell_phone"), 40, $personal_height, 250, 0, "left");
		}
		if ($r->parameters["fax"][SHOW] && !$r->is_empty("fax")) {
			$personal_height -= 12;
			$pdf->show_xy(FAX_FIELD.": ".$r->get_value("fax"), 40, $personal_height, 250, 0, "left");
		}
	
		if ($r->parameters["email"][SHOW] && strlen($r->get_value("email"))) {
			$personal_height -= 12;
			$pdf->show_xy(EMAIL_FIELD.": " . $r->get_value("email"), 40, $personal_height, 250, 0, "left");
		}
	
		foreach ($personal_properties as $property_id => $property_values) {
			$property_price = $property_values["price"];
			$property_tax_id = 0;
			$property_tax = get_tax_amount("", 0, $property_price, 1, $property_tax_id, $property_values["tax_free"], $property_tax_percent);
			$property_height = $pdf->show_xy($property_values["name"] . ": " . $property_values["value"], 40, $personal_height - 12, 250, 0, "left");
			$personal_height -= $property_height;
		}
	
		$delivery_height = $height_position;
	
		$delivery_name = "";
		if ($r->parameters["delivery_name"][SHOW]) {
			$delivery_name = $r->get_value("delivery_name");
		}
		if ($r->parameters["delivery_first_name"][SHOW] && $r->get_value("delivery_first_name")) {
			if ($delivery_name) { $delivery_name .= " "; }
			$delivery_name .= $r->get_value("delivery_first_name");
		}
		if ($r->parameters["delivery_last_name"][SHOW] && $r->get_value("delivery_last_name")) {
			if ($delivery_name) { $delivery_name .= " "; }
			$delivery_name .= $r->get_value("delivery_last_name");
		}
	
		if (strlen($delivery_name)) {
			$delivery_height -= 12;
			$pdf->show_xy($delivery_name, 300, $delivery_height, 250, 0, "left");
		}
	
		if ($r->parameters["delivery_company_id"][SHOW] && $r->get_value("delivery_company_id")) {
			$delivery_height -= 12;
			$pdf->show_xy($r->get_value("delivery_company_id"), 300, $delivery_height, 250, 0, "left");
		}
		if ($r->parameters["delivery_company_name"][SHOW] && $r->get_value("delivery_company_name")) {
			$delivery_height -= 12;
			$pdf->show_xy($r->get_value("delivery_company_name"), 300, $delivery_height, 250, 0, "left");
		}
	
		if ($r->parameters["delivery_address1"][SHOW] && $r->get_value("delivery_address1")) {
			$delivery_height -= 12;
			$pdf->show_xy($r->get_value("delivery_address1"), 300, $delivery_height, 250, 0, "left");
		}
		if ($r->parameters["delivery_address2"][SHOW] && $r->get_value("delivery_address2")) {
			$delivery_height -= 12;
			$pdf->show_xy($r->get_value("delivery_address2"), 300, $delivery_height, 250, 0, "left");
		}
		$delivery_city = ""; $delivery_address = "";
		if ($r->parameters["delivery_city"][SHOW]) {
			$delivery_city = $r->get_value("delivery_city");
		}
		if ($r->parameters["delivery_province"][SHOW]) {
			$delivery_address = $r->get_value("delivery_province");
		}
		if ($r->parameters["delivery_state_id"][SHOW] && $r->get_value("delivery_state_id")) {
			if ($delivery_address) { $delivery_address .= " "; }
			$delivery_address .= $r->get_value("delivery_state_id");
		}
		if ($r->parameters["delivery_zip"][SHOW] && $r->get_value("delivery_zip")) {
			if ($delivery_address) { $delivery_address .= " "; }
			$delivery_address .= $r->get_value("delivery_zip");
		}
		if (strlen($delivery_city) && strlen($delivery_address)) {
			$delivery_address = $delivery_city . ", " . $delivery_address;
		} elseif (strlen($delivery_city)) {
			$delivery_address = $delivery_city;
		}
	
		if (strlen($delivery_address)) {
			$delivery_height -= 12;
			$pdf->show_xy($delivery_address, 300, $delivery_height, 250, 0, "left");
		}
	
		if ($r->parameters["delivery_country_id"][SHOW] && $r->get_value("delivery_country_id")) {
			$delivery_height -= 12;
			$pdf->show_xy($r->get_value("delivery_country_id"), 300, $delivery_height, 250, 0, "left");
		}
	
		if ($r->parameters["delivery_phone"][SHOW] && !$r->is_empty("delivery_phone")) {
			$delivery_height -= 12;
			$pdf->show_xy(PHONE_FIELD.": ".$r->get_value("delivery_phone"), 300, $delivery_height, 250, 0, "left");
		}
		if ($r->parameters["delivery_daytime_phone"][SHOW] && !$r->is_empty("delivery_daytime_phone")) {
			$delivery_height -= 12;
			$pdf->show_xy(DAYTIME_PHONE_FIELD.": ".$r->get_value("delivery_daytime_phone"), 300, $delivery_height, 250, 0, "left");
		}
		if ($r->parameters["delivery_evening_phone"][SHOW] && !$r->is_empty("delivery_evening_phone")) {
			$delivery_height -= 12;
			$pdf->show_xy(EVENING_PHONE_FIELD.": ".$r->get_value("delivery_evening_phone"), 300, $delivery_height, 250, 0, "left");
		}
		if ($r->parameters["delivery_cell_phone"][SHOW] && !$r->is_empty("delivery_cell_phone")) {
			$delivery_height -= 12;
			$pdf->show_xy(CELL_PHONE_FIELD.": ".$r->get_value("delivery_cell_phone"), 300, $delivery_height, 250, 0, "left");
		}
		if ($r->parameters["delivery_fax"][SHOW] && !$r->is_empty("delivery_fax")) {
			$delivery_height -= 12;
			$pdf->show_xy(FAX_FIELD.": ".$r->get_value("delivery_fax"), 300, $delivery_height, 250, 0, "left");
		}
	
		if ($r->parameters["delivery_email"][SHOW] && strlen($r->get_value("delivery_email"))) {
			$delivery_height -= 12;
			$pdf->show_xy(EMAIL_FIELD.": " . $r->get_value("delivery_email"), 300, $delivery_height, 250, 0, "left");
		}
	
		foreach ($delivery_properties as $property_id => $property_values) {
			$property_price = $property_values["price"];
			$property_tax_id = 0;
			$property_tax = get_tax_amount("", 0, $property_price, 1, $property_tax_id, $property_values["tax_free"], $property_tax_percent);
			$property_height = $pdf->show_xy($property_values["name"].": " . $property_values["value"], 300, $delivery_height - 12, 250, 0, "left");
			$delivery_height -= $property_height;
		}
	
		if ($personal_height > $delivery_height) {
			$height_position = $delivery_height;
		} else {
			$height_position = $personal_height;
		}
		$height_position -= 12;
	
	}

	function packing_slip_footer(&$pdf, &$height_position, $packing, $r)
	{
		global $db, $table_prefix;
	
		$order_id = $r->get_value("order_id");
		$pdf->setfont("helvetica", "", 8);

		if (isset($packing["sw_orders_coupons_ps"])) {
			$sql  = " SELECT oc.coupon_title, oc.coupon_code ";
			$sql .= " FROM ".$table_prefix."orders o LEFT JOIN ".$table_prefix."orders_coupons oc ON o.order_id = oc.order_id ";
			$sql .= " WHERE o.order_id = ".$db->tosql($order_id,INTEGER);
			$db->query($sql);
			while ($db->next_record()) {
				$coupon_title = strip_tags(get_translation($db->f("coupon_title")));
				$coupon_code = strip_tags($db->f("coupon_code"));
				if(strlen($coupon_title)){
					$coupon_height = $pdf->show_xy($coupon_title." (".COUPON_MSG.": ".$coupon_code.")", 43, $height_position - 15, 300, 0, "left");
					$height_position -= $coupon_height + 2;
				}
			}
		}

		if (isset($packing["packing_footer"])) {
			$packing_footer = strip_tags($packing["packing_footer"]);
			if (strlen($packing_footer)) {
				$footer_lines = explode("\n", $packing_footer);
				$height_position = 40 + sizeof($footer_lines) * 10;
				for ($i = 0; $i < sizeof($footer_lines); $i++) {
					$height_position -= 10;
					$footer_line = $footer_lines[$i];
					$pdf->show_xy($footer_line, 40, $height_position, 555, 0, "center");
				}
			}
		}
	}
	

?>