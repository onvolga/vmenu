<?php

	// function to recalculate order total value after shipping change
	function recalculate_order($order_id)
	{
		global $db, $table_prefix, $settings;

		// get global settings
		$global_tax_prices_type = get_setting_value($settings, "tax_prices_type", 0);
		$global_tax_round = get_setting_value($settings, "tax_round", 1);
		$tax_prices = get_setting_value($settings, "tax_prices", 0);
		$points_decimals = get_setting_value($settings, "points_decimals", 0);

		// get order tax rates
		$tax_available = false; $tax_percent_sum = 0; $taxes_total = 0; 
		$order_tax_rates = order_tax_rates($order_id);
		if (sizeof($order_tax_rates) > 0) {
			$tax_available = true;
		}

		// get information about order
		$sql  = " SELECT o.user_type_id, o.site_id, o.coupons_ids, o.vouchers_ids, o.total_discount, o.total_discount_tax, o.shipping_type_desc, ";
		$sql .= " o.shipping_cost, o.shipping_taxable, o.tax_name, o.tax_percent, o.vouchers_amount, ";
		$sql .= " o.processing_fee, o.shipping_type_id, o.country_id, o.state_id, o.delivery_state_id, ";
		$sql .= " o.tax_prices_type, o.weight_total, o.goods_total, o.goods_tax, o.goods_incl_tax, ";
		$sql .= " o.currency_code, o.currency_rate, ";
		$sql .= " o.shipping_points_amount, o.total_points_amount, o.credit_amount, o.total_reward_credits, o.total_reward_points ";
		$sql .= " FROM " . $table_prefix . "orders o ";
		$sql .= " WHERE o.order_id=" . $db->tosql($order_id, INTEGER);
		$db->query($sql);
		$db->next_record();

		$order_user_type_id = $db->f("user_type_id");
		$order_site_id = $db->f("site_id");
		$order_status_type = $db->f("status_type");
		$tax_prices_type = $db->f("tax_prices_type");
		if (!strlen($tax_prices_type)) {
			$tax_prices_type = $global_tax_prices_type;
		}
		$tax_round = $db->f("tax_round_type");
		if (!strlen($tax_round)) {
			$tax_round = $global_tax_round;
		}

		$order_coupons_ids = $db->f("coupons_ids");
		$vouchers_ids = $db->f("vouchers_ids");
		$vouchers_amount = $db->f("vouchers_amount");

		//$goods_total = $db->f("goods_total");
		//$goods_tax = $db->f("goods_tax");
		//$goods_incl_tax = $db->f("goods_incl_tax");
		//if ($tax_prices_type == 1) {
		//	$goods_excl_tax = $goods_total - $goods_tax;
		//	$goods_incl_tax = $goods_total;
		//} else {
		//	$goods_excl_tax = $goods_total;
		//	$goods_incl_tax = $goods_total + $goods_tax;
		//}
		$total_discount = $db->f("total_discount");
		$total_discount_tax = $db->f("total_discount_tax");
		$processing_fee = $db->f("processing_fee");

		// calculate tax for old shipping
		$old_shipping_cost = $db->f("shipping_cost");
		$old_shipping_taxable = $db->f("shipping_taxable");

		// NEW SHIPPING STRUCTURE
		$shipments_cost_excl_tax = 0; $shipments_tax = 0; $shipments_cost_incl_tax = 0;
		$sql  = " SELECT * FROM " . $table_prefix . "orders_shipments ";
		$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
		$db->query($sql);
		if ($db->next_record()) {
			do {
				$order_shipping_id = $db->f("order_shipping_id");
				$shipping_cost = $db->f("shipping_cost");
				$points_cost = $db->f("points_cost");
				$shipping_tax_free = $db->f("tax_free");
				$orders_shipments[$order_shipping_id] = $db->Record;
				// calculate tax and total values
				$shipping_tax_id = 0;
				$shipping_tax_values = get_tax_amount($order_tax_rates, "shipping", $shipping_cost, 1, $shipping_tax_id, $shipping_tax_free, $shipping_tax_percent, "", 2, $tax_prices_type, $tax_round);
				$shipping_tax_total = add_tax_values($order_tax_rates, $shipping_tax_values, "shipping", $tax_round);
				if ($tax_prices_type == 1) {
					$shipping_cost_excl_tax = $shipping_cost - $shipping_tax_total;
					$shipping_cost_incl_tax = $shipping_cost;
				} else {
					$shipping_cost_excl_tax = $shipping_cost;
					$shipping_cost_incl_tax = $shipping_cost + $shipping_tax_total;
				}
				$orders_shipments[$order_shipping_id]["shipping_cost_excl_tax"] = $shipping_cost_excl_tax;
				$orders_shipments[$order_shipping_id]["shipping_tax"] = $shipping_tax_total;
				$orders_shipments[$order_shipping_id]["shipping_cost_incl_tax"] = $shipping_cost_incl_tax;
				// calculate sum of shipments
				$shipments_cost_excl_tax += $shipping_cost_excl_tax; 
				$shipments_tax += $shipping_tax_total; 
				$shipments_cost_incl_tax += $shipping_cost_incl_tax;
			} while ($db->next_record());
		} else {
			// old way shipping
			$shipping_tax_id = 0;
			$shipping_tax_free = ($old_shipping_taxable) ? 0 : 1;
			$shipping_tax_values = get_tax_amount($order_tax_rates, "shipping", $old_shipping_cost, 1, $shipping_tax_id, $shipping_tax_free, $shipping_tax_percent, "", 2, $tax_prices_type, $tax_round);
			$shipping_tax_total = add_tax_values($order_tax_rates, $shipping_tax_values, "shipping", $tax_round);
	  
			if ($tax_prices_type == 1) {
				$shipping_cost_excl_tax = $shipping_cost - $shipping_tax_total;
				$shipping_cost_incl_tax = $shipping_cost;
			} else {
				$shipping_cost_excl_tax = $shipping_cost;
				$shipping_cost_incl_tax = $shipping_cost + $shipping_tax_total;
			}

			// calculate sum of shipments
			$shipments_cost_excl_tax += $shipping_cost_excl_tax; 
			$shipments_tax += $shipping_tax_total; 
			$shipments_cost_incl_tax += $shipping_cost_incl_tax;
		} 

		// check order properties
		$properties_total = 0; $properties_tax = 0; $properties_taxable = 0; $properties_incl_tax = 0; $properties_excl_tax = 0;
		$sql  = " SELECT op.property_id, op.property_type, op.property_name, op.property_value, ";
		$sql .= "  op.property_price, op.property_points_amount, op.tax_free ";
		$sql .= " FROM " . $table_prefix . "orders_properties op ";
		$sql .= " WHERE op.order_id=" . $db->tosql($order_id, INTEGER);
		$db->query($sql);
		while ($db->next_record()) {
			$property_id   = $db->f("property_id");
			$property_type = $db->f("property_type");
			$property_price = $db->f("property_price");
			$property_points_amount = $db->f("property_points_amount");
			$property_tax_free = $db->f("tax_free");
			$control_type = $db->f("control_type");
	  
			$properties_total += $property_price;
			if ($property_tax_free != 1) {
				$properties_taxable += $property_price;
			}
			$property_tax_id = 0;
			$property_tax_values = get_tax_amount($order_tax_rates, "properties", $property_price, 1, $property_tax_id, $property_tax_free, $property_tax_percent, "", 2, $tax_prices_type, $tax_round);
			$property_tax = add_tax_values($order_tax_rates, $property_tax_values, "properties", $tax_round);

			if ($tax_prices_type == 1) {
				$property_price_excl_tax = $property_price - $property_tax;
				$property_price_incl_tax = $property_price;
			} else {
				$property_price_excl_tax = $property_price;
				$property_price_incl_tax = $property_price + $property_tax;
			}
			// calculate sum of properties
			$properties_tax += $property_tax;
			$properties_incl_tax += $property_price_incl_tax;
			$properties_excl_tax += $property_price_excl_tax;
		}


		// get info about order items
		$goods_total = 0; $goods_tax = 0; $goods_excl_tax = 0; $goods_incl_tax = 0;
		$total_quantity = 0; $total_items = 0;
		$sql  = " SELECT oi.order_item_id,oi.top_order_item_id,oi.item_id,oi.item_user_id,oi.item_type_id,";
		$sql .= " oi.item_status,oi.item_code,oi.manufacturer_code, oi.component_name, oi.item_name, ";
		$sql .= " oi.is_recurring, oi.recurring_last_payment, oi.recurring_next_payment, oi.downloadable, ";
		$sql .= " oi.price,oi.tax_id,oi.tax_free,oi.tax_percent,oi.discount_amount,oi.real_price, oi.weight, ";
		$sql .= " oi.buying_price,oi.points_price,oi.reward_points,oi.reward_credits,oi.quantity,oi.coupons_ids ";
		$sql .= " FROM " . $table_prefix . "orders_items oi ";
		$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
		$db->query($sql);
		while ($db->next_record()) {
			$item_type_id = $db->f("item_type_id");

			$price = $db->f("price");
			$quantity = $db->f("quantity");
			$item_tax_id = $db->f("tax_id");
			$item_tax_free = $db->f("tax_free");
			$item_total = $price * $quantity;

			$buying_price = $db->f("buying_price");
			$points_price = $db->f("points_price");
			$reward_points = $db->f("reward_points");
			$reward_credits = $db->f("reward_credits");

			// new
			$item_tax = get_tax_amount($order_tax_rates, $item_type_id, $price, 1, $item_tax_id, $item_tax_free, $item_tax_percent, "", 1, $tax_prices_type, $tax_round);
			$item_tax_values = get_tax_amount($order_tax_rates, $item_type_id, $price, 1, $item_tax_id, $item_tax_free, $item_tax_percent, "", 2, $tax_prices_type, $tax_round);
			$item_tax_total_values = get_tax_amount($order_tax_rates, $item_type_id, $item_total, $quantity, $item_tax_id, $item_tax_free, $item_tax_percent, "", 2, $tax_prices_type, $tax_round);
			$item_tax_total = add_tax_values($order_tax_rates, $item_tax_total_values, "products", $tax_round);

			if ($tax_prices_type == 1) {
				$price_excl_tax = $price - $item_tax;
				$price_incl_tax = $price;
				$price_excl_tax_total = $item_total - $item_tax_total;
				$price_incl_tax_total = $item_total;
			} else {
				$price_excl_tax = $price;
				$price_incl_tax = $price + $item_tax;
				$price_excl_tax_total = $item_total;
				$price_incl_tax_total = $item_total + $item_tax_total;
			}

			$goods_total += $item_total;
			$goods_tax += $item_tax_total;
			$goods_excl_tax += $price_excl_tax_total;
			$goods_incl_tax += $price_incl_tax_total;
			$total_quantity += $quantity;

			// calculate points and credits
			//$total_points_price += ($points_price  * $quantity);
			//$total_reward_points += ($reward_points * $quantity);
			//$total_reward_credits += ($reward_credits * $quantity);
		}//*/

		$tax_total = $goods_tax + $properties_tax + $shipments_tax;
		$order_total = $goods_incl_tax + $properties_incl_tax + $shipments_cost_incl_tax + $processing_fee;

		// update goods and order total data
		$sql  = " UPDATE " . $table_prefix . "orders SET ";
		$sql .= " goods_total=" . $db->tosql($goods_total, NUMBER) . ", ";
		$sql .= " goods_tax=" . $db->tosql($goods_tax, NUMBER) . ", ";
		$sql .= " goods_incl_tax=" . $db->tosql($goods_incl_tax, NUMBER) . ", ";
		$sql .= " tax_total=" . $db->tosql($tax_total, NUMBER) . ", ";
		$sql .= " order_total=" . $db->tosql($order_total, NUMBER) . " ";
		$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
		$db->query($sql);
	}


	function update_order_shipping($order_id, $order_shipping_id, $shipping_type_id, $shipping_type_desc, $shipping_cost)
	{
		global $db, $table_prefix;

		// check current shipping
		$current_shipping = "";
		if ($order_shipping_id) {
			$sql  = " SELECT shipping_desc FROM " . $table_prefix . "orders_shipments ";
			$sql .= " WHERE order_shipping_id=" . $db->tosql($order_shipping_id, INTEGER);
			$current_shipping = get_db_value($sql);
		} else {
			$sql  = " SELECT shipping_type_desc FROM " . $table_prefix . "orders ";
			$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
			$current_shipping = get_db_value($sql);
		}

		// get additional information about shipping
		$shipping_type_code = ""; $shipping_taxable = 1; $shipping_tax_free = 0; $shipping_points_amount = 0; 
		if (strlen($shipping_type_id)) {
			$sql  = " SELECT shipping_type_code, shipping_type_desc, is_taxable ";
			$sql .= " FROM ". $table_prefix . "shipping_types ";
			$sql .= " WHERE shipping_type_id=" . $db->tosql($shipping_type_id, INTEGER);
			$db->query($sql);
			if ($db->next_record()) {
				$shipping_type_code = $db->f("shipping_type_code");
				$shipping_type_desc = $db->f("shipping_type_desc");
				$shipping_taxable = $db->f("is_taxable");
				$shipping_tax_free = ($shipping_taxable) ? 0 : 1;
			} else {
				$shipping_type_desc = PROD_SHIPPING_MSG;
			}
		}

		// update shipping information
		if (strlen($shipping_type_desc)) {
			if ($order_shipping_id) {
				$sql  = " UPDATE " . $table_prefix . "orders_shipments SET ";
				$sql .= " shipping_id=" . $db->tosql($shipping_type_id, INTEGER, true, false) . ", ";
				$sql .= " shipping_code=" . $db->tosql($shipping_type_code, TEXT) . ", ";
				$sql .= " shipping_desc=" . $db->tosql($shipping_type_desc, TEXT) . ", ";
				$sql .= " shipping_cost=" . $db->tosql($shipping_cost, NUMBER) . ", ";
				$sql .= " points_cost=" . $db->tosql($shipping_points_amount, NUMBER) . ", ";
				$sql .= " tax_free=" . $db->tosql($shipping_tax_free, INTEGER);
				//TODO: apply new fields
				//$sql .= " expecting_date=" . $db->tosql($expecting_date, DATETIME);
				//$sql .= " goods_weight=" . $db->tosql($goods_weight, INTEGER);
				$sql .= " WHERE order_shipping_id=" . $db->tosql($order_shipping_id, INTEGER);
				$db->query($sql);
			} else {
				$sql  = " UPDATE " . $table_prefix . "orders SET ";
				$sql .= " shipping_type_id=" . $db->tosql($shipping_type_id, INTEGER) . ", ";
				$sql .= " shipping_type_code=" . $db->tosql($shipping_type_code, TEXT) . ", ";
				$sql .= " shipping_type_desc=" . $db->tosql($shipping_type_desc, TEXT) . ", ";
				$sql .= " shipping_cost=" . $db->tosql($shipping_cost, NUMBER) . ", ";
				$sql .= " shipping_points_amount=" . $db->tosql($shipping_points_amount, NUMBER) . ", ";
				$sql .= " shipping_taxable=" . $db->tosql($shipping_taxable, INTEGER);
				$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
				$db->query($sql);
			}

			// save event with updated shipping
			$r = new VA_Record($table_prefix . "orders_events");
			$r->add_textbox("order_id", INTEGER);
			$r->add_textbox("status_id", INTEGER);
			$r->add_textbox("admin_id", INTEGER);
			$r->add_textbox("event_date", DATETIME);
			$r->add_textbox("event_type", TEXT);
			$r->add_textbox("event_name", TEXT);
			$r->add_textbox("event_description", TEXT);
			$r->set_value("order_id", $order_id);
			$r->set_value("status_id", 0);
			$r->set_value("admin_id", get_session("session_admin_id"));
			$r->set_value("event_date", va_time());
			$r->set_value("event_type", "update_order_shipping");
			if ($current_shipping) {
				$r->set_value("event_name", $current_shipping . " &ndash;&gt; " . $shipping_type_desc);
			} else {
				$r->set_value("event_name", $shipping_type_desc);
			}
			$r->insert_record();

			recalculate_order($order_id);
		}
	}

?>