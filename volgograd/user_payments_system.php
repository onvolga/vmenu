<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 Blazze modules                                   ***
  ***      File:  user_payments_system.php                                 ***
  ***      http://hlieviy.com                                              ***
  ***                                                                      ***
  ****************************************************************************
*/


    include_once("./includes/common.php");
    include_once("./includes/record.php");
    include_once("./includes/navigator.php");
    include_once("./includes/sorter.php");
    include_once("./messages/" . $language_code . "/download_messages.php");

    check_user_session();

    $cms_page_code = "user_payments_system";
    $script_name   = "user_payments_system.php";
    $current_page  = get_custom_friendly_url("user_payments_system.php");
    $auto_meta_title = "Платежные системы" . " : " . LIST_MSG;

    include_once("./includes/page_layout.php");

?>