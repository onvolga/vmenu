<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "гСЯТ ЗбПЪг");
	define("SUPPORT_REQUEST_INF_TITLE", "ШбИ гЪбжгЗК");
	define("SUPPORT_REPLY_FORM_TITLE", "СП");

	define("MY_SUPPORT_ISSUES_MSG", "ШбИЗКн ббПЪг");
	define("MY_SUPPORT_ISSUES_DESC", ".ЗРЗ жЗМеКЯ ЗнЙ гФЯбе Эн Зн гд гдКМЗКдЗ, бЗ ККСПП Эн ЗбЗКХЗб ИгПнС ЗбПЪг ЗбЭдн ЪИС ЗбЦЫШ Ъбм ЗбСЗИШ Эн ЗбГЪбм Лг ЗСУЗб ШбИ ПЪг");
	define("NEW_SUPPORT_REQUEST_MSG", "ШбИ МПнП");
	define("SUPPORT_REQUEST_ADDED_MSG", "ФЯСЗр бЯ, ЭСнЮ ЗбПЪг ЗбЭдн УнеКг ИгФЯбКЯ ж УнЮжг ИЗбСП ЪбнЯ Эн ЗЮСИ ЭСХе");
	define("SUPPORT_SELECT_DEP_MSG", "ЗОКС ЗбПЗЖСе");
	define("SUPPORT_SELECT_PROD_MSG", "ЗОКС ЗбгдКМ");
	define("SUPPORT_SELECT_STATUS_MSG", "ЗОКС ЗбНЗбе");
	define("SUPPORT_NOT_VIEWED_MSG", "бг ККг гФЗеПКе");
	define("SUPPORT_VIEWED_BY_USER_MSG", "КгК гФЗеПКе гд ЮИб ЗбТИжд");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "КгК гФЗеПКе гд ЗбгПнС ЗбЪЗг");
	define("SUPPORT_STATUS_NEW_MSG", "МПнП");
	define("NO_SUPPORT_REQUEST_MSG", "бЗ КжМП ЗнЙ ШбИЗК.");

	define("SUPPORT_SUMMARY_COLUMN", "ЗбгбОХ");
	define("SUPPORT_TYPE_COLUMN", "ЗбджЪ");
	define("SUPPORT_UPDATED_COLUMN", "ВОС КНПнЛ");

	define("SUPPORT_USER_NAME_FIELD", "ЗУгЯ");
	define("SUPPORT_USER_EMAIL_FIELD", "ИСнПЯ ЗбЕбЯКСждн");
	define("SUPPORT_IDENTIFIER_FIELD", "(ЗбгЪСЭ (СЮг ЗбЭЗКжСе");
	define("SUPPORT_ENVIRONMENT_FIELD", "ЗбИнЖе");
	define("SUPPORT_DEPARTMENT_FIELD", "ЗбПЗЖСе");
	define("SUPPORT_PRODUCT_FIELD", "ЗбгдКМ");
	define("SUPPORT_TYPE_FIELD", "ЗбджЪ");
	define("SUPPORT_CURRENT_STATUS_FIELD", "ЗбНЗбе ЗбНЗбне");
	define("SUPPORT_SUMMARY_FIELD", "гбОХ ЮХнС");
	define("SUPPORT_DESCRIPTION_FIELD", "ЗбФСН");
	define("SUPPORT_MESSAGE_FIELD", "ЗбСУЗбе");
	define("SUPPORT_ADDED_FIELD", "гЦЗЭ");
	define("SUPPORT_ADDED_BY_FIELD", "гЦЗЭ ИжЗУШЙ");
	define("SUPPORT_UPDATED_FIELD", "ВОС КНПнЛ");

	define("SUPPORT_REQUEST_BUTTON", "ЗСУб ЗбШбИ");
	define("SUPPORT_REPLY_BUTTON", "ЗбСП");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "СЮг ШбИ ЗбПЪг гЭЮжП");
	define("SUPPORT_MISS_CODE_ERROR", "СЮг ЗбКГЯнП гЭЮжП");
	define("SUPPORT_WRONG_ID_ERROR", "СЮг ШбИ ЗбПЪг нНгб Юнге ОЗШЖе");
	define("SUPPORT_WRONG_CODE_ERROR", "СЮг ЗбКГЯнП нНгб Юнге ОЗШЖе");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>