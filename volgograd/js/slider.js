var speed = 2;
var defaultSpeed = 1;
var slideShowSpeed = 10000;
var sliderSpeed = new Array(); //Specify slider scroll speed (larger is faster 1-10)
var sliderOptions = new Array();

function initBlocks()
{
	var blockCode = ""; var sliderType = "";
	var foundBlocks = document.getElementsByName("block");
	for (var b = 0; b < foundBlocks.length; b++) {
		var formObj = foundBlocks[b];
		var pbId = formObj.pb_id.value;
		if (formObj.block_code) {
			blockCode = formObj.block_code.value;
		}
		if (formObj.slider_type) {
			sliderType = formObj.slider_type.value;
		}
	
		if (sliderType >= 1 && sliderType <= 4) {
			initSlider(pbId, sliderType);
		} else if (sliderType == 5) {
			initSlider(pbId, sliderType);
		}
	}
}

function initSlider(pbId, sliderType)
{
	if (sliderType >= 1 && sliderType <= 4) {
		// create copy of block content
		var dataObj = document.getElementById("data_" + pbId);
		var parentObj = dataObj.parentNode;
  
		parentObj.style.width = parentObj.offsetWidth+"px";
		parentObj.style.height = parentObj.offsetHeight+"px";
  
		dataObj.style.position = "absolute";
		dataObj.style.left = "0px";
		dataObj.style.top = "0px";
  
		// create duplicate content to show slider 
		var secondObj = dataObj.cloneNode(true);
		secondObj.setAttribute("id", "second_data_" + pbId)	
		secondObj.style.position = "absolute";
		if (sliderType == 1) {
			secondObj.style.top = dataObj.offsetHeight + "px";
			secondObj.style.height = dataObj.offsetHeight + "px";		
		} else if (sliderType == 2) {
			secondObj.style.left = dataObj.offsetWidth + "px";
			secondObj.style.width = dataObj.offsetWidth + "px";		
		} else if (sliderType == 3) {
			secondObj.style.top = -dataObj.offsetHeight + "px";
			secondObj.style.height = dataObj.offsetHeight + "px";		
		} else if (sliderType == 4) {
			secondObj.style.left = -dataObj.offsetWidth + "px";
			secondObj.style.width = dataObj.offsetWidth + "px";		
		}
		parentObj.insertBefore(secondObj, dataObj.nextSibling);
  
		if (parentObj.attachEvent)	{
			parentObj.attachEvent('onmouseover', function (){ sliderSpeed[pbId] = 0;});
			parentObj.attachEvent('onmouseout', function (){ sliderSpeed[pbId] = defaultSpeed;});
		} else {
			parentObj.onmouseover = function (){ sliderSpeed[pbId] = 0; }; 
			parentObj.onmouseout = function (){ sliderSpeed[pbId] = defaultSpeed; };
		}
  
		sliderSpeed[pbId] = defaultSpeed;
		setTimeout("moveSlider("+pbId+","+sliderType+")", 500);
	} else if (sliderType == 5) {
		sliderOptions[pbId] = new Array();
		sliderOptions[pbId]["curItemId"] = 1;
		sliderOptions[pbId]["sliderSpeed"] = slideShowSpeed;
		// check maxId
		var itemObj = document.getElementById("data_" + pbId + "_1");
		var maxItemId = 1;
		do {
			sliderOptions[pbId]["maxItemId"] = maxItemId;
			maxItemId++;
			itemObj = document.getElementById("data_" + pbId + "_" + maxItemId);
		} while (itemObj);
		// add navigation buttons
		addSliderNavigation(pbId);
		// activate slide show 
		sliderOptions[pbId]["function"] = setTimeout("slideShow("+pbId+")", sliderOptions[pbId]["sliderSpeed"]);
	}
}

function addSliderNavigation(pbId)
{
	var activeObj = document.getElementById("data_" + pbId + "_1");
	var parentObj = activeObj.parentNode;

	var maxLeft = parentObj.offsetWidth - 22;
	var maxTop = parentObj.offsetHeight - 22;

	// next link
	var aObj = document.createElement('a');
	aObj.id = "next_" + pbId;
	aObj.href = "#";
	aObj.onclick = function () { nextSlide(pbId); return false; };
	parentObj.appendChild(aObj);

	var imgObj = document.createElement('img');
	imgObj.src = "images/icons/slider_next.png";
	imgObj.border = "0";
	imgObj.style.position = "absolute";
	imgObj.style.left = maxLeft + "px";
	imgObj.style.top = maxTop + "px";
	aObj.appendChild(imgObj);

	// play link
	maxLeft = maxLeft - 25;
	aObj = document.createElement('a');
	aObj.id = "play_" + pbId;
	aObj.href = "#";
	aObj.onclick = function () { playSlideShow(pbId); return false; };
	aObj.style.display = "none";
	parentObj.appendChild(aObj);

	imgObj = document.createElement('img');
	imgObj.src = "images/icons/slider_play.png";
	imgObj.border = "0";
	imgObj.style.position = "absolute";
	imgObj.style.left = maxLeft + "px";
	imgObj.style.top = maxTop + "px";
	aObj.appendChild(imgObj);

	// pause link
	aObj = document.createElement('a');
	aObj.id = "pause_" + pbId;
	aObj.href = "#";
	aObj.onclick = function () { pauseSlideShow(pbId); return false; };
	parentObj.appendChild(aObj);

	imgObj = document.createElement('img');
	imgObj.src = "images/icons/slider_pause.png";
	imgObj.border = "0";
	imgObj.style.position = "absolute";
	imgObj.style.left = maxLeft + "px";
	imgObj.style.top = maxTop + "px";
	aObj.appendChild(imgObj);

	// prev link
	maxLeft = maxLeft - 25;
	aObj = document.createElement('a');
	aObj.id = "prev_" + pbId;
	aObj.href = "#";
	aObj.onclick = function () { prevSlide(pbId); return false; };
	parentObj.appendChild(aObj);

	imgObj = document.createElement('img');
	imgObj.src = "images/icons/slider_prev.png";
	imgObj.border = "0";
	imgObj.style.position = "absolute";
	imgObj.style.left = maxLeft + "px";
	imgObj.style.top = maxTop + "px";
	aObj.appendChild(imgObj);

}

function changeObjects(pbId)
{
	var curObj = sliderOptions[pbId]["curObj"];
	var newObj = sliderOptions[pbId]["newObj"];
	var step = sliderOptions[pbId]["step"];

	if (step == 100) {
		curObj.style.position = "absolute";
		newObj.style.position = "absolute";
		newObj.style.filter = "alpha(opacity=0)";
		newObj.style.display = "block";
	}
	curObj.style.filter = "alpha(opacity="+step+")";
	newObj.style.filter = "alpha(opacity="+(100-step)+")";
	curObj.style.opacity = step/100;
	newObj.style.opacity = (100-step)/100;

	// decrease step number
	step = step - 5;
	sliderOptions[pbId]["step"] = step;
	if (step > 0) {
		setTimeout("changeObjects("+pbId+")", 10);
	} else {
		curObj.style.display = "none";
		newObj.style.display = "block";
		// remove filter
		//curObj.style.filter = "";
		//newObj.style.filter = "";

		sliderOptions[pbId]["curItemId"] = sliderOptions[pbId]["newItemId"];;

		var sliderType = sliderOptions[pbId]["sliderType"];
		if (sliderOptions[pbId]["sliderSpeed"] > 0) {
			sliderOptions[pbId]["function"] = setTimeout("slideShow("+pbId+")", sliderOptions[pbId]["sliderSpeed"]);
		}
	}
}

function slideShow(pbId, increment)
{
	if (increment != 1 && increment != -1) {
		increment = 1;
	}
	var curItemId = sliderOptions[pbId]["curItemId"];
	var curObj = document.getElementById("data_" + pbId + "_" + curItemId);
	var newItemId = curItemId + increment;
	var newObj = document.getElementById("data_" + pbId + "_" + newItemId);
	if (!newObj && curItemId > 1) {
		if (increment == 1) {
			newItemId = 1;
		} else if (increment == -1) {
			newItemId = sliderOptions[pbId]["maxItemId"];
		}
		newObj = document.getElementById("data_" + pbId + "_" + newItemId);
	}
	if (newObj) {
		sliderOptions[pbId]["curObj"] = curObj;
		sliderOptions[pbId]["newObj"] = newObj;
		sliderOptions[pbId]["newItemId"] = newItemId;
		sliderOptions[pbId]["step"] = 100;
		setTimeout("changeObjects("+pbId+")", 20);
	}
}

function nextSlide(pbId)
{
	if (sliderOptions[pbId]["function"]) {
		clearTimeout(sliderOptions[pbId]["function"]);
	}
	slideShow(pbId);
}

function prevSlide(pbId)
{
	if (sliderOptions[pbId]["function"]) {
		clearTimeout(sliderOptions[pbId]["function"]);
	}
	slideShow(pbId, -1);
}


function playSlideShow(pbId)
{
	var playObj = document.getElementById("play_" + pbId);
	var pauseObj = document.getElementById("pause_" + pbId);
	playObj.style.display = "none";
	pauseObj.style.display = "inline";

	if (sliderOptions[pbId]["function"]) {
		clearTimeout(sliderOptions[pbId]["function"]);
	}
	sliderOptions[pbId]["sliderSpeed"] = slideShowSpeed;
	sliderOptions[pbId]["function"] = setTimeout("slideShow("+pbId+")", sliderOptions[pbId]["sliderSpeed"]);
}

function pauseSlideShow(pbId)
{
	var playObj = document.getElementById("play_" + pbId);
	var pauseObj = document.getElementById("pause_" + pbId);
	playObj.style.display = "inline";
	pauseObj.style.display = "none";

	sliderOptions[pbId]["sliderSpeed"] = 0;
	if (sliderOptions[pbId]["function"]) {
		clearTimeout(sliderOptions[pbId]["function"]);
	}
}


function moveSlider(pbId, sliderType)
{
	var dataObj = document.getElementById("data_" + pbId);
	var secondObj = document.getElementById("second_data_" + pbId);

	var sliderWidth = dataObj.offsetWidth;
	var sliderHeight = dataObj.offsetHeight;

	if (sliderType == 1) {
		if (parseInt(dataObj.style.top) > (sliderHeight*(-1)+8)) 	//if slider hasn't reached the end of its height
			dataObj.style.top = parseInt(dataObj.style.top) - sliderSpeed[pbId]+"px" 	// move slider up
		else
			dataObj.style.top = parseInt(secondObj.style.top) + sliderHeight+"px"	 // reset to original position
		
		if (parseInt(secondObj.style.top) > (sliderHeight*(-1)+8))
			secondObj.style.top = parseInt(secondObj.style.top) - sliderSpeed[pbId] +"px"
		else
			secondObj.style.top = parseInt(dataObj.style.top) + sliderHeight+"px"
	} else if (sliderType == 2) {
		if (parseInt(dataObj.style.left) > (sliderWidth*(-1)+8))	 	// if slider hasn't reached the end of its width
			dataObj.style.left = parseInt(dataObj.style.left) - sliderSpeed[pbId] +"px"	 // move slider left
		else
			dataObj.style.left = parseInt(secondObj.style.left) + sliderWidth+"px"	// reset to original position
		
		if (parseInt(secondObj.style.left) > (sliderWidth*(-1)+8))
			secondObj.style.left = parseInt(secondObj.style.left) - sliderSpeed[pbId] +"px"
		else
			secondObj.style.left = parseInt(dataObj.style.left) + sliderWidth+"px"
	} else if (sliderType == 3) {
		if (parseInt(dataObj.style.top) < (sliderHeight + 8)) 	//if slider hasn't reached the end of its height
			dataObj.style.top = parseInt(dataObj.style.top) + sliderSpeed[pbId]+"px" 	// move slider up
		else
			dataObj.style.top = parseInt(secondObj.style.top) - sliderHeight+"px"	 // reset to original position
		
		if (parseInt(secondObj.style.top) < (sliderHeight +8))
			secondObj.style.top = parseInt(secondObj.style.top) + sliderSpeed[pbId] +"px"
		else
			secondObj.style.top = parseInt(dataObj.style.top) - sliderHeight+"px"
	} else if (sliderType == 4) {
		if (parseInt(dataObj.style.left) < (sliderWidth +8))	 	// if slider hasn't reached the end of its width
			dataObj.style.left = parseInt(dataObj.style.left) + sliderSpeed[pbId] +"px"	 // move slider left
		else
			dataObj.style.left = parseInt(secondObj.style.left) - sliderWidth+"px"	// reset to original position
		
		if (parseInt(secondObj.style.left) < (sliderWidth + 8))
			secondObj.style.left = parseInt(secondObj.style.left) + sliderSpeed[pbId] +"px"
		else
			secondObj.style.left = parseInt(dataObj.style.left) - sliderWidth+"px"
	}

	setTimeout("moveSlider("+pbId+","+sliderType+")", 30);
}

//window.onload = initBlocks;
BLZ_Event(window, 'load', initBlocks);