var startDelay = 0; //Specify initial delay before slider starts to scroll on page (1000=1 seconds)
var sliderSpeed = 1; //Specify slider scroll speed (larger is faster 1-10)
var pauseit = 1; //Pause slider onMousever (0=no, 1=yes)
var speed = 20;
var copySpeed = sliderSpeed;
var pauseSpeed = (pauseit==0)? copySpeed: 0;

function horizontalSlider(){
	sliderSecond = document.getElementById("sliderContent2");
	if (parseInt(slider.style.left) > (sliderWidth*(-1)+8))	 	//if slider hasn't reached the end of its width
		slider.style.left = parseInt(slider.style.left) - copySpeed+"px"	 //move slider left
	else
		slider.style.left = parseInt(sliderSecond.style.left) + sliderWidth+"px"	//else, reset to original position
	
	if (parseInt(sliderSecond.style.left) > (sliderWidth*(-1)+8))
		sliderSecond.style.left = parseInt(sliderSecond.style.left) - copySpeed+"px"
	else
		sliderSecond.style.left = parseInt(slider.style.left) + sliderWidth+"px"
}

function pauseSlider()	{
	if(sliderContainer.attachEvent)	{
	   sliderContainer.attachEvent('onmouseover', function (){copySpeed = pauseSpeed;});
	   sliderContainer.attachEvent('onmouseout', function (){copySpeed = sliderSpeed;});
	} 
	else	{
	   sliderContainer.onmouseover = function (){copySpeed = pauseSpeed;}; 
	   sliderContainer.onmouseout = function (){copySpeed = sliderSpeed;};
	}
}

function initSlider()	{
	slider = document.getElementById("sliderContent");	//Set ID for main DIV
	slider.style.left = 0;
	slider.className = "sliderClassHorizontal";		//Specify CLASS for main DIV tag
	sliderContainer = document.getElementById("sliderBox");	
	
	// Calculation main TABLE width
	sliderTABLE = document.getElementById('offerTable');
	sliderTR = sliderTABLE.getElementsByTagName('tr');
	sliderTDLength = sliderTR[0].getElementsByTagName("td").length;
	sliderTDWidth = 200;
	sliderTABLE.style.width = sliderTDLength * parseInt(sliderTDWidth) + "px"
	sliderWidth = parseInt(sliderTABLE.style.width);				//Specify main TABLE width
	// *************************	
	
	sliderSecond = document.createElement("div")			//Create new element DIV
	sliderSecond.setAttribute("id", "sliderContent2")	//Set ID for new DIV
	sliderSecond.className = "sliderClassHorizontal";	//Specify the same CLASS for new DIV tag
	sliderSecond.innerHTML = slider.innerHTML;			//Copy content of main DIV
	sliderContainer.insertBefore(sliderSecond, slider.nextSibling)	//Insert new UL after main DIV
	sliderSecond.style.left = sliderTABLE.style.width;		//Set new DIV left space	
	sliderSecond.style.width = sliderTABLE.style.width;		//Set new DIV width
	
	sliderBoxWidth = sliderContainer.offsetWidth;
	pauseSlider();
	setTimeout('sliderTime=setInterval("horizontalSlider()",speed)', startDelay);
}

window.onload = initSlider;