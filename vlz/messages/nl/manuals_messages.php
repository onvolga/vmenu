<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Handleidingen");
	define("NO_MANUALS_MSG", "Geen handleidingen gevonden");
	define("NO_MANUAL_ARTICLES_MSG", "Geen artikelen");
	define("MANUALS_PREV_ARTICLE", "Vorige");
	define("MANUALS_NEXT_ARTICLE", "Volgende");
	define("MANUAL_CONTENT_MSG", "Index");
	define("MANUALS_SEARCH_IN_MSG", "Zoeken in");
	define("MANUALS_SEARCH_FOR_MSG", "Zoeken");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Alle handleidingen");
	define("MANUALS_SEARCH_RESULTS_INFO", "{results_number} artikelen gevonden voor zoekterm(en) {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Zoekresultaten");
	define("MANUALS_NOT_FOUND_ANYTHING", "Niets gevonden voor zoekterm(en) '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Geen inhoud");
	define("MANUALS_SEARCH_TITLE", "Handleidingen zoeken");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Handleidingen zoeken resultaten");

?>