<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_product_option.php                                  ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/editgrid.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./includes/shopping_cart.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");

	check_user_security("access_products");

	$cms_page_code = "user_product_option";
	$script_name   = "user_product_option.php";
	$current_page  = get_custom_friendly_url("user_product_option.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = OPTIONS_AND_COMPONENTS_MSG.": ".EDIT_OPTION_MSG;

	include_once("./includes/page_layout.php");

?>
