<?php

    include_once("./includes/editgrid.php");
    include_once("./messages/".$language_code."/admin_messages.php");

    $default_title = "Редактирование модуля доставки";

    check_user_security("merchant_orders");

    $user_id = intval(get_session("session_user_id"));

    $stmt = "   select uts.setting_value 
                from va_user_types_settings uts
                inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
                where setting_name = 'user_shipping_general_selection'";

    $selectionSsAvailable = get_db_value($stmt);

    $stmt = "   select uts.setting_value 
                from va_user_types_settings uts
                inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
                where setting_name = 'user_shipping_individual'";

    $userSsAvailable = get_db_value($stmt);
    if(!$selectionSsAvailable || !$userSsAvailable){
        header("Location: " . get_custom_friendly_url("user_home.php"));
        exit;
    }


    $site_url = get_setting_value($settings, "site_url", "");   
    $secure_url = get_setting_value($settings, "secure_url", "");

    $html_template = get_setting_value($block, "html_template", "block_user_shipping_system.html"); 
  $t->set_file("block_body", $html_template);
 

/////////////////////////////////////////////////

    $t->set_var("CONFIRM_DELETE_JS", str_replace("{record_name}", SHIPPING_MODULE_MSG, CONFIRM_DELETE_MSG));

    // set up html form parameters
    $r = new VA_Record($table_prefix . "shipping_modules");
    $r->add_where("shipping_module_id", INTEGER);
    $r->change_property("shipping_module_id", USE_IN_INSERT, true);
    $r->add_checkbox("is_active", INTEGER);
//cst   
    $r->add_hidden("available_for_user", INTEGER);
    $r->add_hidden("is_default", INTEGER); 
    $r->change_property("is_default", DEFAULT_VALUE, 1);
    $r->add_hidden("is_call_center", INTEGER); 
    $r->change_property("is_call_center", DEFAULT_VALUE, 1);
    
    
    $r->add_hidden("user_added_id", INTEGER);

    


    $r->add_textbox("shipping_module_name", TEXT, SHIPPING_MODULE_NAME_MSG);
    $r->change_property("shipping_module_name", REQUIRED, true);
    $r->add_textbox("module_notes", TEXT, MODULE_NOTES_MSG);
    $r->add_textbox("cost_add_percent", FLOAT, COST_ADD_PERCENT_MSG);
    $r->add_checkbox("is_external", TEXT);
    $r->add_textbox("php_external_lib", TEXT, PHP_EXTERNAL_LIBRARY_MSG);
    $r->add_textbox("external_url", TEXT, EXTERNAL_URL_MSG);
    $r->add_textbox("tracking_url", TEXT, TRACKING_URL_MSG);
    $is_external = get_param("is_external");
    if ($is_external) {
        $r->change_property("php_external_lib", REQUIRED, true);
    }
    $r->get_form_values();

    $r->change_property("available_for_user", USE_IN_INSERT, true);
    $r->set_value("available_for_user", 1);
    $r->change_property("user_added_id", USE_IN_INSERT, true);
    $r->set_value("user_added_id", $user_id);
    $r->change_property("is_default", USE_IN_INSERT, true);
    $r->set_value("is_default", 1);
    $r->change_property("is_call_center", USE_IN_INSERT, true);
    $r->set_value("is_call_center", 1);

    $rp = new VA_Record($table_prefix . "shipping_modules_parameters", "parameters");
    $rp->add_where("parameter_id", INTEGER);
    $rp->change_property("parameter_id", USE_IN_ORDER, ORDER_ASC);
    $rp->add_hidden("shipping_module_id", INTEGER);
    $rp->change_property("shipping_module_id", USE_IN_INSERT, true);
    $rp->add_textbox("parameter_name", TEXT, PARAMETER_NAME_MSG);
    $rp->change_property("parameter_name", REQUIRED, true);
    $rp->add_textbox("parameter_source", TEXT, PARAMETER_SOURCE_MSG);
    $rp->add_checkbox("not_passed", INTEGER, NOT_PASSED_MSG);

    $shipping_module_id = get_param("shipping_module_id");

    $more_parameters = get_param("more_parameters");
    $number_parameters = get_param("number_parameters");

    $eg = new VA_EditGrid($rp, "parameters");
    $eg->get_form_values($number_parameters);

    $operation = get_param("operation");

    $return_page = "user_shipping_systems.php";

    if (strlen($operation) && !$more_parameters)
    {
        if ($operation == "cancel")
        {
            header("Location: " . $return_page);
            exit;
        }
        elseif ($operation == "delete" && $shipping_module_id)
        {
            $shipping_type_ids = "";
            $sql = "SELECT shipping_type_id FROM " . $table_prefix . "shipping_types WHERE shipping_module_id = " . $db->tosql($shipping_module_id, INTEGER);
            $db->query($sql);
            while ($db->next_record()) {
                if (strlen($shipping_type_ids)) { $shipping_type_ids .= ","; }
                $shipping_type_ids .= $db->f("shipping_type_id");
            }
            $db->query("DELETE FROM " . $table_prefix . "shipping_types_countries WHERE shipping_type_id IN (" . $db->tosql($shipping_type_ids, TEXT, false) . ")");
            $db->query("DELETE FROM " . $table_prefix . "shipping_types WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER));
            $db->query("DELETE FROM " . $table_prefix . "shipping_modules_parameters WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER));
            $db->query("DELETE FROM " . $table_prefix . "shipping_modules WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER));
            header("Location: " . $return_page);
            exit;
        }

        $is_valid = $r->validate();
        $is_valid = ($eg->validate() && $is_valid);

        if ($is_valid)
        {
            if (strlen($shipping_module_id))
            {
                $r->update_record();
                $eg->set_values("shipping_module_id", $shipping_module_id);
                $eg->update_all($number_parameters);
            }
            else
            {
                $db->query("SELECT MAX(shipping_module_id) FROM " . $table_prefix . "shipping_modules");
                $db->next_record();
                $shipping_module_id = $db->f(0) + 1;
                $r->set_value("shipping_module_id", $shipping_module_id);
                $r->insert_record();
                $eg->set_values("shipping_module_id", $shipping_module_id);
                $eg->insert_all($number_parameters);
            }

            header("Location: " . $return_page);
            exit;
        }
    }
    elseif (strlen($shipping_module_id) && !$more_parameters)
    {
        $r->get_db_values();
        $eg->set_value("shipping_module_id", $shipping_module_id);
        $eg->change_property("parameter_id", USE_IN_SELECT, true);
        $eg->change_property("parameter_id", USE_IN_WHERE, false);
        $eg->change_property("shipping_module_id", USE_IN_WHERE, true);
        $eg->change_property("shipping_module_id", USE_IN_SELECT, true);
        $number_parameters = $eg->get_db_values();
        if ($number_parameters == 0) {
            $number_parameters = 5;
        }
    }
    elseif ($more_parameters)
    {
        $number_parameters += 5;
    }
    else
    {
        $number_parameters = 5;
    }

    $t->set_var("number_parameters", $number_parameters);
    $eg->set_parameters_all($number_parameters);
    $r->set_parameters();

    if (strlen($shipping_module_id)) {
        $t->set_var("save_button", UPDATE_BUTTON);
        $t->parse("delete", false);
    } else {
        $t->set_var("save_button", ADD_NEW_MSG);
        $t->set_var("delete", "");
    }



/////////////////////////////////////////////////



    $block_parsed = true;

?>
