<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "ЗОКнЗСн");
	define("BAD_MSG", "УнБ");
	define("POOR_MSG", "ЦЪнЭ");
	define("AVERAGE_MSG", "гКжУШ");
	define("GOOD_MSG", "МнП");
	define("EXCELLENT_MSG", "ггКЗТ");
	define("REVIEWS_MSG", "ЗбКЪбнЮЗК");
	define("NO_REVIEWS_MSG", "бЗ КжМП ЗнЙ КЪбнЮЗК");
	define("WRITE_REVIEW_MSG", "ЗЦЭ КЪбнЮЗр");
	define("RATE_PRODUCT_MSG", "Юунцг еРЗ ЗбгдКМ");
	define("RATE_ARTICLE_MSG", "Юунцг еРЗ ЗбгжЦжЪ");
	define("NOT_RATED_PRODUCT_MSG", ".бг нКг КЮннг еРЗ ЗбгдКМ ИЪП");
	define("NOT_RATED_ARTICLE_MSG", ".бг нКг КЮннг еРе ЗбгжЦжЪ ИЪП");
	define("AVERAGE_RATING_MSG", "гКжУШ КЮннгЗК ЗбТИЗЖд");
	define("BASED_ON_REVIEWS_MSG", "{total_votes} :ИЗбЕЪКгЗП Ъбм ЪПП ЗбКЪбнЮЗК");
	define("POSITIVE_REVIEW_MSG", "КЪбнЮЗК ЗбТИЗЖд ЗбЕнМЗИне");
	define("NEGATIVE_REVIEW_MSG", "КЪбнЮЗК ЗбТИЗЖд ЗбУбИне");
	define("SEE_ALL_REVIEWS_MSG", "ЗШбЪ Ъбм МгнЪ ЗбКЪбнЮЗК");
	define("ALL_REVIEWS_MSG", "Яб ЗбКЪбнЮЗК");
	define("ONLY_POSITIVE_MSG", "ЗбЕнМЗИне ЭЮШ");
	define("ONLY_NEGATIVE_MSG", "ЗбУбИне ЭЮШ");
	define("POSITIVE_REVIEWS_MSG", "ЗбКЪбнЮЗК ЗбЕнМЗИне");
	define("NEGATIVE_REVIEWS_MSG", "ЗбКЪбнЮЗК ЗбУбИне");
	define("SUBMIT_REVIEW_MSG", ".УККг гСЗМЪЙ КЪбнЮЯ, ж УнКг дФСе ЗРЗ КгК ЗбгжЗЭЮе Ъбне. бЗНЩ ЗддЗ дгбЯ ЗбНЮ Эн СЭЦ ЗнЙ КЪбнЮ бЗ нКжЗЭЮ гЪ гКШбИЗКдЗ <br>ФЯСЗр бЯ");
	define("ALREADY_REVIEW_MSG", "бЮП ЮгК гУИЮЗр ИжЦЪ КЪбнЮЯ");
	define("RECOMMEND_PRODUCT_MSG", "еб КдХН ИеРЗ ЗбгдКМ ббВОСндї");
	define("RECOMMEND_ARTICLE_MSG", "еб КдХН еРЗ ЗбгжЦжЪ ббВОСндї");
	define("RATE_IT_MSG", "Юнге");
	define("NAME_ALIAS_MSG", "ЗбЕУг Зж ЗбЕУг ЗбгУКЪЗС");
	define("SHOW_MSG", "ЗЩеС");
	define("FOUND_MSG", "жМцП");
	define("RATING_MSG", "гКжУШ ЗбКЮннгЗК");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>