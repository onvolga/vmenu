<?php                           

	include_once("./includes/record.php");
	include_once("./includes/reviews_functions.php");

	$default_title = "Отзывы о <b>{reviewed_item_name}</b>";

	$html_template = get_setting_value($block, "html_template", "block_users_reviews.html"); 
  $t->set_file("block_body", $html_template);
	
	// set urls
	$reviews_url = new VA_URL("merchant_reviews.php");
	$reviews_url->add_parameter("user", REQUEST, "user");

	// get products reviews settings

	$user_id = get_session("session_user_id");
	$allowed_view = 1;
	$allowed_post = 1;
	$reviews_per_page = 15;


	if (!strlen(get_session("session_user_id"))) { 
		$use_validation = true;
	} else {
		$use_validation = false;
	}

	$merchant_id = get_param("user");

	$t->set_var('merchant_id', $merchant_id);

	$merchant_type_id = get_db_value(" SELECT user_type_id from " . $table_prefix . "users WHERE user_id = " . $db->tosql($merchant_id, INTEGER));
	
	$is_seller = get_db_value(" SELECT is_seller from " . $table_prefix . "user_types WHERE type_id = " . $db->tosql($merchant_type_id, INTEGER));

	$reviews_href = "merchant_reviews.php";

	$rating = 
		array( 
			array("", ""), array(1, BAD_MSG), array(2, POOR_MSG), 
			array(3, AVERAGE_MSG), array(4, GOOD_MSG), array(5, EXCELLENT_MSG),
			);

	$rr = new VA_Record($table_prefix . "user_reviews");
	// global data
	$rr->operations[INSERT_ALLOWED] = (($allowed_post == 1) || ($allowed_post == 2 && get_session("session_user_id")));
	$rr->operations[UPDATE_ALLOWED] = false;
	$rr->operations[DELETE_ALLOWED] = false;
	$rr->operations[SELECT_ALLOWED] = false;
	$rr->redirect = false;
	$rr->success_messages[INSERT_SUCCESS] = SUBMIT_REVIEW_MSG;

	// internal fields
	$rr->add_where("ur_id", INTEGER);
	$rr->add_textbox("merchant_id", INTEGER);
	$rr->change_property("merchant_id", DEFAULT_VALUE, $merchant_id);

	//$rr->change_property("merchant_id", USE_SQL_NULL, false);

	$rr->add_textbox("date_added", DATETIME);
	$rr->add_textbox("remote_address", TEXT);
	$rr->add_textbox("user_added_id", TEXT);

	$rr->add_textbox("approved", INTEGER);
	$rr->change_property("approved", DEFAULT_VALUE, 1);
	// predefined fields
	$rr->add_select("rating", INTEGER, $rating, RATE_IT_MSG);
	$rr->add_textbox("user_name", TEXT, NAME_ALIAS_MSG);
	$rr->change_property("user_name", REQUIRED, true);
	$rr->set_control_event("user_name", AFTER_VALIDATE, "check_content");
	$rr->add_textbox("user_email", TEXT, EMAIL_FIELD);
	$rr->change_property("user_email", REQUIRED, true);
	$rr->change_property("user_email", REGEXP_MASK, EMAIL_REGEXP);
	$rr->set_control_event("user_email", AFTER_VALIDATE, "check_content");
	$rr->add_textbox("comments", TEXT, DETAILED_COMMENT_MSG);
	$rr->set_control_event("comments", AFTER_VALIDATE, "check_content");

	if ($user_id) {	
		$user_info = get_session("session_user_info");
		$user_nickname = get_setting_value($user_info, "nickname", "");
		$user_email = get_setting_value($user_info, "email", "");
		if (strlen($user_nickname)) {
			$rr->change_property("user_name", SHOW, false);
		}
		if (strlen($user_email)) {
			$rr->change_property("user_email", SHOW, false);
		}
	}

	$rr->add_textbox("validation_number_user", TEXT, VALIDATION_CODE_FIELD);
	$rr->change_property("validation_number_user", USE_IN_INSERT, false);
	$rr->change_property("validation_number_user", USE_IN_UPDATE, false);
	$rr->change_property("validation_number_user", USE_IN_SELECT, false);
	if (!$user_id) {
		$rr->change_property("validation_number_user", REQUIRED, true);
		$rr->change_property("validation_number_user", SHOW, true);
		$rr->change_property("validation_number_user", AFTER_VALIDATE, "check_validation_number");
	} else {
		$rr->change_property("validation_number_user", SHOW, false);
	}

	// set events
	$rr->set_event(ON_DOUBLE_SAVE, "review_double_save");
	$rr->set_event(BEFORE_INSERT, "before_insert_review");
	$rr->set_event(AFTER_INSERT, "after_insert_review");
	$rr->set_event(BEFORE_VALIDATE, "additional_review_checks");
	$rr->set_event(BEFORE_SHOW, "review_form_check");


	// check if merchant exists
	$is_item = false;
	if($is_seller){
		$sql  = " SELECT * FROM " . $table_prefix . "users ";
		$sql .= " WHERE user_id=" . $db->tosql($merchant_id, INTEGER);
		$db->query($sql);
		if($db->next_record())
		{
			$product_info = $db->Record;
			$item_name = get_translation($db->f("company_name"));
			$t->set_var("reviewed_item_name", $item_name);
			if (!strlen($meta_title)) {
				$meta_title = REVIEWS_MSG.": ".$company_name;
			}
			$is_item = true;
		}
	}
	else
	{
		$item_name = ERRORS_MSG;
		$rr->errors = "Продавец не существует";
		$t->set_var("reviewed_item_name", ERRORS_MSG);
	}

	$t->set_var("rnd",           va_timestamp());
	$t->set_var("reviews_href",  $reviews_href . '?user=' . $merchant_id);
	$t->set_var("merchant_id",  htmlspecialchars($merchant_id));

	$remote_address = get_ip();

	$rr->process();

	$sql = " SELECT COUNT(*) FROM " . $table_prefix . "user_reviews WHERE approved=1 AND rating <> 0 AND merchant_id=" . $db->tosql($merchant_id, INTEGER);
	$total_rating_votes = get_db_value($sql);

	$average_rating_float = 0;
	$total_rating_sum = 0;
	if($total_rating_votes)
	{
		$sql = " SELECT SUM(rating) FROM " . $table_prefix . "user_reviews WHERE approved=1 AND rating <> 0 AND merchant_id=" . $db->tosql($merchant_id, INTEGER);
		$total_rating_sum = get_db_value($sql);
		$average_rating_float = round($total_rating_sum / $total_rating_votes, 2);
	}

	$t->set_var("user", htmlspecialchars($user));

	if($is_item && ($allowed_view == 1 || ($allowed_view == 2 && strlen($user_id))))
	{
		$n = new VA_Navigator($settings["templates_dir"], "navigator.html", $reviews_href);
		

		$total_votes = $sql = " SELECT COUNT(*) FROM " . $table_prefix . "user_reviews WHERE approved=1 AND merchant_id=" . $db->tosql($merchant_id, INTEGER);


		
		
		if($total_votes)
		{
			// parse summary statistic

			$t->set_var("total_votes", $total_votes);

			$average_rating = round($average_rating_float, 0);
			$average_rating_image = $average_rating ? "rating-" . $average_rating : "not-rated";
			$t->set_var("average_rating_image", $average_rating_image);
			$t->set_var("average_rating_alt", $average_rating_float);

			$t->parse("summary_statistic", false);

			$sql    = " SELECT COUNT(*) FROM " . $table_prefix . "user_reviews ";
			$where  = " WHERE comments IS NOT NULL ";
			$where .= " AND approved=1 AND merchant_id=" . $db->tosql($merchant_id, INTEGER);
		
			$total_records = get_db_value($sql . $where);
			$t->set_var("total_records", $total_records);
			

			$record_number = 0;
			$records_per_page = $reviews_per_page ? $reviews_per_page : 10;
			$pages_number = 5;

			$page_number = $n->set_navigator("navigator", "page", SIMPLE, $pages_number, $records_per_page, $total_records, false);
  
			$sql = " SELECT * FROM " . $table_prefix . "user_reviews ";
			$order_by = " ORDER BY date_added DESC";  
			$db->RecordsPerPage = $records_per_page;
			$db->PageNumber = $page_number;
			$db->query($sql . $where . $order_by);
			if($db->next_record())
			{
				$latest_comments = $db->f("comments");
				do 
				{
					$record_number++;
					if($record_number > 1) {
						$t->parse("delimiter", false);
					} else {
						$t->set_var("delimiter", "");
					}
					$review_user_id = $db->f("user_added_id");
					$review_user_name = htmlspecialchars($db->f("user_name"));
					if (!$review_user_id) {
						$review_user_name .= " (" . GUEST_MSG . ")";
					}
					$review_user_class = $review_user_id ? "forumUser" : "forumGuest";

					if ($db->f("recommended") == 1) {
						$recommended_image = "commend";
					} else if ($db->f("recommended") == -1) {
						$recommended_image = "discommend";
					} else {
						$recommended_image = "neutral";
					}
					$t->set_var("recommended_image", $recommended_image);
					$rating = round($db->f("rating"), 0);
					$rating_image = $rating ? "rating-" . $rating : "not-rated";
					$t->set_var("rating_image", $rating_image);
					$t->set_var("review_user_class", $review_user_class);
					$t->set_var("review_user_name", $review_user_name);
					$date_added = $db->f("date_added", DATETIME);
					$date_added_string = va_date($datetime_show_format, $date_added);
					$t->set_var("review_date_added", $date_added_string);
					$t->set_var("review_comments", nl2br(htmlspecialchars($db->f("comments"))));
      
					$t->parse("reviews_list", true);
				} while ($db->next_record());
				$t->parse("reviews", false);
			}
			else
				$t->parse("no_reviews", false);
		}
		else
		{
			$t->set_var("total_records", 0);
			$t->parse("no_reviews", false);
		}
	
		$t->parse("reviews_block", false);
	}

	$block_parsed = true;

function check_validation_number()
{
	global $db, $rr;
	if($rr->get_property_value("validation_number_user", IS_VALID)) {
		$validated_number = check_image_validation($rr->get_value("validation_number_user"));
		if (!$validated_number) {
			$error_message = str_replace("{field_name}", VALIDATION_CODE_FIELD, VALIDATION_MESSAGE);
			$rr->change_property("validation_number_user", IS_VALID, false);
			$rr->change_property("validation_number_user", ERROR_DESC, $error_message);
		} else {
			// saved validated number for following submits	and delete this value in case of success
			set_session("session_validation_number", $validated_number);
		}
	}
}

function check_content($parameter)
{
	global $rr;
	$control_name = $parameter[CONTROL_NAME];
	if ($parameter[IS_VALID] && check_banned_content($parameter[CONTROL_VALUE])) {
		$rr->parameters[$control_name][IS_VALID] = false;
		$rr->parameters[$control_name][ERROR_DESC] = "<b>".$parameter[CONTROL_DESC]."</b>: ".BANNED_CONTENT_MSG;
	}
}

function additional_review_checks()
{
	global $rr;
	if (check_black_ip()) {
		$rr->errors = BLACK_IP_MSG."<br>";	
	} else if (!check_add_product_review($rr->get_value("user"))) {
		$rr->errors = ALREADY_REVIEW_MSG."<br>";
	}
}

function review_form_check()
{
	global $rr;

	if (check_black_ip()) {
		$rr->record_show = false;	
		$rr->errors = BLACK_IP_MSG;	
	} else if (!check_add_merchant($rr->get_value("merchant_id"))) {
		$rr->record_show = false;	
		if (!$rr->success_message) {
			//$rr->success_message = ALREADY_REVIEW_MSG;
			$rr->success_message = "Не зарегистрированные пользователи могут добавлять только один комментарий в сутки.";

		}
	}
}

function before_insert_review()
{
	global $rr, $db, $table_prefix;

	$approved = 1;
	$rr->set_value("date_added", va_time());
	$rr->set_value("remote_address", get_ip());
	$rr->set_value("approved", $approved);
	$rr->set_value("user_added_id", get_session("session_user_id"));
	$user_id = get_session("session_user_id");
	if ($user_id) {	
		$user_info = get_session("session_user_info");
		$user_nickname = get_setting_value($user_info, "nickname", "");
		$user_email = get_setting_value($user_info, "email", "");
		if (strlen($user_nickname)) {
			$rr->set_value("user_name", $user_nickname);
		}
		if (strlen($user_email)) {
			$rr->set_value("user_email", $user_email);
		}
	}
}
function after_insert_review()
{
	global $rr, $db, $table_prefix, $t, $settings, $product_info, $datetime_show_format,$merchant_id;

	// record was added clear validation variable
	set_session("session_validation_number", "");

	// if review was approved update it rating
	if ($rr->get_value("approved") == 1) {
		update_product_rating($rr->get_value("merchant_id"));
	}

	// get last review id
	if ($db->DBType == "mysql") {
		$sql = " SELECT LAST_INSERT_ID() ";
		$new_review_id = get_db_value($sql);
		$rr->set_value("ur_id", $new_review_id);
	} else if ($db->DBType == "access") {
		$sql = " SELECT @@IDENTITY ";
		$new_review_id = get_db_value($sql);
		$rr->set_value("review_id", $new_review_id);
	}

	// clear values and set default
	$rr->empty_values();
	$rr->set_default_values();
	$rr->change_property("merchant_id", DEFAULT_VALUE, $merchant_id);
}

function review_double_save()
{
	global $rr;
	$rr->operation = "double";
	$rr->success_message = SUBMIT_REVIEW_MSG;
	$rr->empty_values();
	$rr->set_default_values();
}

?>