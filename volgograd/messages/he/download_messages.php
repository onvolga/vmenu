<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "ערכי טעינה שגויים");
	define("DOWNLOAD_MISS_PARAM", "ערכי טעינה חסרים");
	define("DOWNLOAD_INACTIVE", "טעינה לא פעילה");
	define("DOWNLOAD_EXPIRED", "פג תוקף תקופת הטעינה שלך");
	define("DOWNLOAD_LIMITED", "עברת אתמיספר הטעינות המירבי");
	define("DOWNLOAD_PATH_ERROR", "לא נימצא מסלול המוצר");
	define("DOWNLOAD_RELEASE_ERROR", "לא נמצאה ההוצאה");
	define("DOWNLOAD_USER_ERROR", "טעינהת קובץ זה רק למשתמשים רשומים");
	define("ACTIVATION_OPTIONS_MSG", "אפשרויות הפעלה");
	define("ACTIVATION_MAX_NUMBER_MSG", "מיספר הפעלות מירבי");
	define("DOWNLOAD_OPTIONS_MSG", "אפשרויות למוצרי טעינה / תוכנות");
	define("DOWNLOADABLE_MSG", "מוצרי טעינה (תוכנות)");
	define("DOWNLOADABLE_DESC", "למוצרי טעינה ניתן לציין 'תקופת טעינה', 'מסלול לקובץ טעינה', ו-'אפשרויות הפעלה'");
	define("DOWNLOAD_PERIOD_MSG", "תקופת טעינה");
	define("DOWNLOAD_PATH_MSG", "מסלול לקובץ טעינה");
	define("DOWNLOAD_PATH_DESC", "אתה יכול להוסיףמיספר מסלולים מופרדים ע\"י נקודה-פסיק");
	define("UPLOAD_SELECT_MSG", "בחר קובץ להעלאה ולחץ על כפתור {button_name} ");
	define("UPLOADED_FILE_MSG", "קובץ <b>{filename}</b> הועלה.");
	define("UPLOAD_SELECT_ERROR", "נא לבחור קובץ תחילה");
	define("UPLOAD_IMAGE_ERROR", "מותרים רק קבצי תמונה");
	define("UPLOAD_FORMAT_ERROR", "סוג קובץ זה אסור");
	define("UPLOAD_SIZE_ERROR", "קבצים גדולים מ- {filesize} אסורים");
	define("UPLOAD_DIMENSION_ERROR", "תמונות גדולות מ- {dimension} אסורים");
	define("UPLOAD_CREATE_ERROR", "המערכת אינה יכולה ליצור את הקובץ");
	define("UPLOAD_ACCESS_ERROR", "אין לך אישור להעלאת קבצים");
	define("DELETE_FILE_CONFIRM_MSG", "האם אתה בטוח שברצונך למחוק קובץ זה?");
	define("NO_FILES_MSG", "לא נמצאו קבצים");
	define("SERIAL_GENERATE_MSG", "צורמיספר סידורי");
	define("SERIAL_DONT_GENERATE_MSG", "אל תיצור");
	define("SERIAL_RANDOM_GENERATE_MSG", "צורמיספר אקראי עבור מוצר תוכנה");
	define("SERIAL_FROM_PREDEFINED_MSG", "קבלמיספר סידורי מרשימה מוגדרת מראש");
	define("SERIAL_PREDEFINED_MSG", "מיספר סידורי מוגדר מראש");
	define("SERIAL_NUMBER_COLUMN", "מיספר סידורי");
	define("SERIAL_USED_COLUMN", "בשימוש");
	define("SERIAL_DELETE_COLUMN", "מחק");
	define("SERIAL_MORE_MSG", "להוסיף עודמיספרים סדוריים?");
	define("SERIAL_PERIOD_MSG", "תקופתמיספר סידורי");
	define("DOWNLOAD_SHOW_TERMS_MSG", "הראה תנאים ");
	define("DOWNLOAD_SHOW_TERMS_DESC", "כדי להוריד קובץ זה על המשתמש לקרוא ולהסכים לתנאים שלנו.");
	define("DOWNLOAD_TERMS_MSG", "תנאים");
	define("DOWNLOAD_TERMS_USER_DESC", "קראתי ואני מסכים לתנאים");
	define("DOWNLOAD_TERMS_USER_ERROR", "כדי להוריד קובץ זה עלך לקרוא ולהסכים לתנאים שלנו.");

	define("DOWNLOAD_TITLE_MSG", "כותרת טעינה");
	define("DOWNLOADABLE_FILES_MSG", "קבצים ניתנים להורדה");
	define("DOWNLOAD_INTERVAL_MSG", "מירווח הורדה");
	define("DOWNLOAD_LIMIT_MSG", "גבולות הורדה");
	define("DOWNLOAD_LIMIT_DESC", "מיספר פעמים שקובץ מורשה להורדה");
	define("MAXIMUM_DOWNLOADS_MSG", "מיספר הורדות מירבי");
	define("PREVIEW_TYPE_MSG", "סוג צפיה מראש");
	define("PREVIEW_TITLE_MSG", "כותרת צפיה מראש");
	define("PREVIEW_PATH_MSG", "מסלול לצפיה מראש בקובץ");
	define("PREVIEW_IMAGE_MSG", "תמונה לצפיה מראש");
	define("MORE_FILES_MSG", "עוד קבצים");
	define("UPLOAD_MSG", "העלאה");
	define("USE_WITH_OPTIONS_MSG", "לשימוש עם אפשרויות בלבד");
	define("PREVIEW_AS_DOWNLOAD_MSG", "צפיה מראש תוך כדי הורדה");
	define("PREVIEW_USE_PLAYER_MSG", "השתמש בנגן לצפיה מראש");
	define("PROD_PREVIEWS_MSG", "צפיות מראש");

?>