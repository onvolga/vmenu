<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  forgot_password.php                                      ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/navigator.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");


	$cms_page_code = "forgot_password";
	$script_name   = "forgot_password.php";
	$current_page  = get_custom_friendly_url("forgot_password.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = FORGOT_PASSWORD_MSG;

	include_once("./includes/page_layout.php");

?>
