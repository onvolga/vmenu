<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Opcional");
	define("BAD_MSG", "Mau");
	define("POOR_MSG", "Medнocre");
	define("AVERAGE_MSG", "Mйdio");
	define("GOOD_MSG", "Bom");
	define("EXCELLENT_MSG", "Excelente");
	define("REVIEWS_MSG", "Revisхes");
	define("NO_REVIEWS_MSG", "Nгo foram encontradas revisхes");
	define("WRITE_REVIEW_MSG", "Escreva uma revisгo");
	define("RATE_PRODUCT_MSG", "Classifique este produto");
	define("RATE_ARTICLE_MSG", "Classifique este artigo");
	define("NOT_RATED_PRODUCT_MSG", "O produto ainda nгo estб classificado.");
	define("NOT_RATED_ARTICLE_MSG", "O artigo ainda nгo estб classificado.");
	define("AVERAGE_RATING_MSG", "Classificaзгo Mйdia do Cliente");
	define("BASED_ON_REVIEWS_MSG", "baseado em {total_votes} revisхes");
	define("POSITIVE_REVIEW_MSG", "Revisгo Positiva do Cliente");
	define("NEGATIVE_REVIEW_MSG", "Revisгo Negativa do Cliente");
	define("SEE_ALL_REVIEWS_MSG", "Ver todas as revisхes do cliente...");
	define("ALL_REVIEWS_MSG", "Todas as Revisхes");
	define("ONLY_POSITIVE_MSG", "Apenas as Positivas");
	define("ONLY_NEGATIVE_MSG", "Apenas as Negativas");
	define("POSITIVE_REVIEWS_MSG", "Revisхes Positivas");
	define("NEGATIVE_REVIEWS_MSG", "Revisхes Negativas");
	define("SUBMIT_REVIEW_MSG", "Obrigado<br>Os seus comentбrios serгo revistos. Se forem aprovados, serгo mostrados no nosso site.<br>Reservamo-nos no direito de declinar qualquer submissхes que nгo se enquadrem no вmbito deste site.");
	define("ALREADY_REVIEW_MSG", "Jб colocou uma revisгo.");
	define("RECOMMEND_PRODUCT_MSG", "Recomendaria<br> este produto a outros?");
	define("RECOMMEND_ARTICLE_MSG", "Recomendaria<br> este artigo a outros?");
	define("RATE_IT_MSG", "Classifique-o");
	define("NAME_ALIAS_MSG", "Nome ou pseudуnimo");
	define("SHOW_MSG", "Mostrar");
	define("FOUND_MSG", "Encontrado");
	define("RATING_MSG", "Classificaзгo Mйdia");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>