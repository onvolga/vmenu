User-agent: *
Disallow: /admin/
Disallow: /blocks/
Disallow: /dimon/
Disallow: /cache/
Disallow: /db/
Disallow: /cgi-bin/
Disallow: /includes/
Disallow: /install/
Disallow: /styles/
Disallow: /language/
Disallow: /logs/
Disallow: /templates/
Disallow: /tmp/
Disallow: /order_info.php
Disallow: /order_confirmation.php
Disallow: /order_final.php
Disallow: /article_print.php
Disallow: /product_print.php
Disallow: /tell_friend.php
Disallow: /search.php
Disallow: /site_search.php
Disallow: /products_search.php
Disallow: /basket.php
Disallow: /support_messages.php
Disallow: /site_search.php
Disallow: /products_search.php
Disallow: /forgot_password.php
Disallow: /poll_vote.php
Disallow: /polls.php
Disallow: /*&type=
Disallow: /?type=
Disallow: /*&sort_dir=
Disallow: /*?filter=
Disallow: /*?sort_ord=
Disallow: /*?s_form=
Disallow: /*block_login_
Disallow: /*?rnd=
Disallow: /*cart=ADD*
Disallow: /user_login.php
Disallow: /user_orders.php
Disallow: /user_order.php
Disallow: /error-404
Disallow: /user_home.php
Disallow: /user_profile.php
Disallow: /user_support.php
Disallow: /user_addresses.php
Disallow: /user_address.php
Disallow: /user_products.php
Disallow: /user_product.php
Disallow: /user_product_images.php
Disallow: /user_product_options.php
Disallow: /user_ads.php
Disallow: /user_ad.php
Disallow: /user_carts.php
Disallow: /user_cart.php
Disallow: /user_merchant_orders.php
Disallow: /user_merchant_order.php
Disallow: /merchant_invoice_html
Disallow: /merchant_invoice_html.php
Disallow: /user_merchant_sales.php
Disallow: /user_merchant_items.php
Disallow: /user_shipping_systems.php
Disallow: /user_shipping_system.php
Disallow: /user_payments_system.php
Disallow: /user_wishlist.php
Disallow: /user_reminders.php
Disallow: /user_reminder.php
Disallow: /user_change_password.php
Disallow: /demo/
Disallow: /primerka/
Sitemap: http://dostavka34.ru/sitemap1.xml
Sitemap: http://dostavka34.ru/volgograd/sitemap1.xml
Sitemap: http://dostavka34.ru/vlz/sitemap1.xml
Host: dostavka34.ru





