<?php
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once("./admin_common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/friendly_functions.php");

	check_admin_security("products_reviews");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main", "admin_blz_icon.html");


	$t->set_var("CONFIRM_DELETE_JS", str_replace("{record_name}", 'иконкy', CONFIRM_DELETE_MSG));


	$r = new VA_Record($table_prefix . "blz_items_icons");
	$r->return_page = "admin_blz_images_list.php";

	$r->add_where("icon_id", INTEGER);
    $r->add_textbox("icon_order", INTEGER, 'Порядок показа');
    $r->change_property("icon_order", REQUIRED, true);
    $r->add_textbox("icon_name", TEXT, 'Название');
    $r->change_property("icon_name", REQUIRED, true);
	$r->add_textbox("icon_path", TEXT, 'Путь к изображению');  
	$r->change_property("icon_path", REQUIRED, true);

    
	$r->process();

	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->pparse("main");

?>
