<?php

    include_once("./messages/".$language_code."/admin_messages.php");

    $default_title = "Способы доставки";

    check_user_security("merchant_orders");

    $user_id = intval(get_session("session_user_id"));

    $stmt = "   select uts.setting_value 
                from va_user_types_settings uts
                inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
                where setting_name = 'user_shipping_general_selection'";

    $selectionSsAvailable = get_db_value($stmt);

    $stmt = "   select uts.setting_value 
                from va_user_types_settings uts
                inner join va_users u on u.user_type_id=uts.type_id and u.user_id=" . $user_id . "
                where setting_name = 'user_shipping_individual'";

    $userSsAvailable = get_db_value($stmt);
    if(!$selectionSsAvailable || !$userSsAvailable){
        header("Location: " . get_custom_friendly_url("user_home.php"));
        exit;
    }

    $shipping_module_id = get_param("shipping_module_id");

    $sql = " SELECT shipping_module_name FROM " . $table_prefix . "shipping_modules WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER) . " AND user_added_id=".$user_id;
    $db->query($sql);
    if ($db->next_record()) {
        $shipping_module_name = get_translation($db->f("shipping_module_name"));
    } else {
        header ("Location: user_home.php");
        exit;
    }

    $site_url = get_setting_value($settings, "site_url", "");   
    $secure_url = get_setting_value($settings, "secure_url", "");

    $html_template = get_setting_value($block, "html_template", "block_user_shipping_modules.html"); 
  $t->set_file("block_body", $html_template);


    $t->set_var("admin_href", "user_home.php");

    $t->set_var("admin_shipping_type_href",    "user_shipping_module.php");
    $t->set_var("shipping_module_id",          $shipping_module_id);
    $t->set_var("shipping_module_name",        $shipping_module_name);

    $s = new VA_Sorter($settings["templates_dir"], "sorter_img.html", "user_shipping_modules.php");
    $s->set_sorter(ID_MSG, "sorter_shipping_type_id", "1", "shipping_type_id");
    $s->set_sorter(SHIPPING_TYPE_MSG, "sorter_shipping_type_desc", "2", "shipping_type_desc");
    $s->set_sorter(ACTIVE_MSG, "sorter_is_active", "3", "is_active");
    $n = new VA_Navigator($settings["templates_dir"], "navigator.html", "user_shipping_modules.php");

    // set up variables for navigator
    $db->query("SELECT COUNT(*) FROM " . $table_prefix . "shipping_types WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER));
    $db->next_record();
    $total_records = $db->f(0);
    $records_per_page = get_param("q") > 0 ? get_param("q") : 25;
    $pages_number = 5;
    $page_number = $n->set_navigator("navigator", "page", SIMPLE, $pages_number, $records_per_page, $total_records, false);

    $db->RecordsPerPage = $records_per_page;
    $db->PageNumber = $page_number;
    $db->query("SELECT * FROM " . $table_prefix . "shipping_types WHERE shipping_module_id=" . $db->tosql($shipping_module_id, INTEGER) . $s->order_by);
    if ($db->next_record())
    {

        $t->parse("sorters", false);
        $t->set_var("no_records", "");
        do {
            $is_active = ($db->f("is_active") == 1) ? "<b>".YES_MSG."</b>" : NO_MSG;
            
            $t->set_var("shipping_type_id", $db->f("shipping_type_id"));
            $t->set_var("shipping_type_desc", get_translation($db->f("shipping_type_desc")));
            $t->set_var("is_active", $is_active);
            
            $t->parse("records", true);
        } while ($db->next_record());
    }
    else
    {
        $t->set_var("sorters", "");
        $t->set_var("records", "");
        $t->set_var("navigator", "");
        $t->parse("no_records", false);
    }

  $block_parsed = true;

?>