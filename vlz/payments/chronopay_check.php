<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  chronopay_check.php                                      ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


/*
 * Chronopay (www.chronopay.com) transaction handler by http://www.viarts.ru/
 */

	$success_message = "";
	$sql  = " SELECT success_message, error_message FROM " . $table_prefix . "orders ";
	$sql .= " WHERE order_id=" . $db->tosql($order_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$success_message = $db->f("success_message");
		$error_message = $db->f("error_message");
	}

	if (!strlen($success_message) && !strlen($error_message)) {
		$pending_message = "There is no answer from payment gateway or this order was declined by payment gateway. This order will be reviewed manually.";
	}

?>
