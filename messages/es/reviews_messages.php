<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// comentarios/mensajes de evaluaciуn
	define("OPTIONAL_MSG", "Opcional");
	define("BAD_MSG", "Malo");
	define("POOR_MSG", "Mediocre");
	define("AVERAGE_MSG", "Normal");
	define("GOOD_MSG", "Bueno");
	define("EXCELLENT_MSG", "Excelente");
	define("REVIEWS_MSG", "Crнticas");
	define("NO_REVIEWS_MSG", "No hay comentarios en la base de datos");
	define("WRITE_REVIEW_MSG", "Escribir un comentario");
	define("RATE_PRODUCT_MSG", "Valorar el producto");
	define("RATE_ARTICLE_MSG", "Valorar el artнculo");
	define("NOT_RATED_PRODUCT_MSG", "El producto no ha sido valorado");
	define("NOT_RATED_ARTICLE_MSG", "El artнculo no ha sido valorado");
	define("AVERAGE_RATING_MSG", "Valoraciуn media de clientes");
	define("BASED_ON_REVIEWS_MSG", "basado en {total_votes} comentarios");
	define("POSITIVE_REVIEW_MSG", "Valoraciуn positiva del cliente");
	define("NEGATIVE_REVIEW_MSG", "Valoraciуn negativa del cliente");
	define("SEE_ALL_REVIEWS_MSG", "Ver todos los comentarios de los clientes");
	define("ALL_REVIEWS_MSG", "Todos los comentarios");
	define("ONLY_POSITIVE_MSG", "Solamente positivos");
	define("ONLY_NEGATIVE_MSG", "Solamente negativos");
	define("POSITIVE_REVIEWS_MSG", "Comentarios positivos");
	define("NEGATIVE_REVIEWS_MSG", "Comentarios negativos");
	define("SUBMIT_REVIEW_MSG", "Gracias <br> Sus comentarios se revisarбn y si se aprueban, aparecerбn en nuestro sitio. <br>Nos reservamos el derecho de declinar cualquier comentario que no concuerde con los principios de nuestra empresa.");
	define("ALREADY_REVIEW_MSG", "Ya ha puesto su comentario");
	define("RECOMMEND_PRODUCT_MSG", "їRecomendarнa usted<br>este producto a otras personas?");
	define("RECOMMEND_ARTICLE_MSG", "їRecomendarнa usted<br>este artнculo a otras personas?");
	define("RATE_IT_MSG", "Valore este producto");
	define("NAME_ALIAS_MSG", "Nombre o apodo");
	define("SHOW_MSG", "Mostrar");
	define("FOUND_MSG", "Encontrado");
	define("RATING_MSG", "Valoraciуn media");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>