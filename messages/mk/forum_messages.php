<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Форм");
	define("TOPIC_INFO_TITLE", "Тема");
	define("TOPIC_MESSAGE_TITLE", "Поруки");

	define("MY_FORUM_TOPICS_MSG", "Мои теми на форумот");
	define("ALL_FORUM_TOPICS_MSG", "Сите теми на форумот");
	define("MY_FORUM_TOPICS_DESC", "Дали сте помислиле дека проблемот кој го имате вие некој друг го има исто така.Дали би сакале своите искуства да ги поделите со други.Станете корисник на овој форум, станете член на оваа заедница");
	define("NEW_TOPIC_MSG", "Нова тема");
	define("NO_TOPICS_MSG", "Не е најдена ниедна тема");
	define("FOUND_TOPICS_MSG", "Најдовме  <b>{found_records}</b> теми кои одговараат на терминот/те   '<b>{search_string}</b>' кој го барате");
	define("NO_FORUMS_MSG", "Не е пронајден форум");

	define("FORUM_NAME_COLUMN", "Форум");
	define("FORUM_TOPICS_COLUMN", "Теми");
	define("FORUM_REPLIES_COLUMN", "Одговори");
	define("FORUM_LAST_POST_COLUMN", "Последни дополнувања");
	define("FORUM_MODERATORS_MSG", "Водител");

	define("TOPIC_NAME_COLUMN", "Тема");
	define("TOPIC_AUTHOR_COLUMN", "Автор");
	define("TOPIC_VIEWS_COLUMN", "Видени");
	define("TOPIC_REPLIES_COLUMN", "Одговори");
	define("TOPIC_UPDATED_COLUMN", "Последни дополнувања");
	define("TOPIC_ADDED_MSG", "Благодариме<br>Вашата тема е додадена");

	define("TOPIC_ADDED_BY_FIELD", "Додал");
	define("TOPIC_ADDED_DATE_FIELD", "Додадена");
	define("TOPIC_UPDATED_FIELD", "Последни дополнувања");
	define("TOPIC_NICKNAME_FIELD", "nickname/Прекор");
	define("TOPIC_EMAIL_FIELD", "Ваша е-маил адреса");
	define("TOPIC_NAME_FIELD", "Тема");
	define("TOPIC_MESSAGE_FIELD", "Порака");
	define("TOPIC_NOTIFY_FIELD", "Испрати ги сите одговори на мој е-маил");

	define("ADD_TOPIC_BUTTON", "Додај тема");
	define("TOPIC_MESSAGE_BUTTON", "Додај порака");

	define("TOPIC_MISS_ID_ERROR", "Недостасува <b>Нит ID</b>параметар.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Нит ID</b> параметарот со неточна вредност.");
	define("FORUM_SEARCH_MESSAGE", "Најдовме {search_count}  пораки кои го содржат бараниот поим '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Претходен поглед");
	define("TOPIC_SAVE_BUTTON", "Спреми");

	define("LAST_POST_ON_SHORT_MSG", "На:");
	define("LAST_POST_IN_SHORT_MSG", "Во:");
	define("LAST_POST_BY_SHORT_MSG", "Од:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Последниизмени");

?>