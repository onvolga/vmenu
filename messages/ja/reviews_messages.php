<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "”C€У");
	define("BAD_MSG", "‚ж‚ч‚И‚ў");
	define("POOR_MSG", "‚ ‚Ь‚и‚ж‚ч‚И‚ў");
	define("AVERAGE_MSG", "•Ѓ’К");
	define("GOOD_MSG", "‚ж‚ў");
	define("EXCELLENT_MSG", "‘fђ°‚з‚µ‚ў");
	define("REVIEWS_MSG", "ѓЊѓrѓ…Ѓ[");
	define("NO_REVIEWS_MSG", "ѓЊѓrѓ…Ѓ[‚Н‚ ‚и‚Ь‚№‚сЃB");
	define("WRITE_REVIEW_MSG", "ѓЊѓrѓ…Ѓ[‚рЏ‘‚ч");
	define("RATE_PRODUCT_MSG", "‚щ‚Мђ»•i‚р•]‰ї‚·‚й");
	define("RATE_ARTICLE_MSG", "‚щ‚М‹LЋ–‚р•]‰ї‚·‚й");
	define("NOT_RATED_PRODUCT_MSG", "‚щ‚Мђ»•i‚Н‚Ь‚ѕ•]‰ї‚і‚к‚Д‚ў‚Ь‚№‚сЃB");
	define("NOT_RATED_ARTICLE_MSG", "‹LЋ–‚Н‚Ь‚ѕ•]‰ї‚і‚к‚Д‚ў‚Ь‚№‚сЃB");
	define("AVERAGE_RATING_MSG", "•Ѕ‹П•]‰ї");
	define("BASED_ON_REVIEWS_MSG", "ѓЊѓrѓ…Ѓ[ {total_votes} ‚ЙЉо‚Г‚ч");
	define("POSITIVE_REVIEW_MSG", "ѓ|ѓWѓeѓBѓu‚Иѓ—ѓXѓ^ѓ}Ѓ[ѓЊѓrѓ…Ѓ[");
	define("NEGATIVE_REVIEW_MSG", "ѓlѓKѓeѓBѓu‚Иѓ—ѓXѓ^ѓ}Ѓ[ѓЊѓrѓ…Ѓ[");
	define("SEE_ALL_REVIEWS_MSG", "‚·‚Ч‚Д‚Мѓ—ѓXѓ^ѓ}Ѓ[ѓЊѓrѓ…Ѓ[‚рЊ©‚й");
	define("ALL_REVIEWS_MSG", "‚·‚Ч‚Д‚МѓЊѓrѓ…Ѓ[");
	define("ONLY_POSITIVE_MSG", "ѓ|ѓWѓeѓBѓuѓЊѓrѓ…Ѓ[‚М‚Э");
	define("ONLY_NEGATIVE_MSG", "ѓlѓKѓeѓBѓuѓЊѓrѓ…Ѓ[‚М‚Э");
	define("POSITIVE_REVIEWS_MSG", "ѓ|ѓWѓeѓBѓu‚ИѓЊѓrѓ…Ѓ[");
	define("NEGATIVE_REVIEWS_MSG", "ѓlѓKѓeѓBѓu‚ИѓЊѓrѓ…Ѓ[");
	define("SUBMIT_REVIEW_MSG", "‚ ‚и‚Є‚Ж‚¤‚І‚ґ‚ў‚Ь‚·ЃB <br>‚Ё‹q—l‚Мѓ›ѓЃѓ“ѓћ‚НђRЌё‚і‚кЃAЏі”F‚і‚к‚й‚ЖЃAѓTѓCѓћ‚ЙЊfЌЪ‚і‚к‚Ь‚·ЃB <br> •ѕЋР‚МѓKѓCѓhѓ‰ѓCѓ“‚Й“Y‚н‚И‚ў“а—e‚ЄЉЬ‚Ь‚к‚Д‚ў‚йЏкЌ‡ЃAЊfЌЪ‚р‰“—¶‚і‚№‚Д‚ў‚Ѕ‚ѕ‚ч‚щ‚Ж‚Є‚ ‚и‚Ь‚·ЃB");
	define("ALREADY_REVIEW_MSG", "ѓЊѓrѓ…Ѓ[‚НЊfЌЪЌП‚Э‚Е‚·ЃB");
	define("RECOMMEND_PRODUCT_MSG", "‚щ‚Мђ»•i‚р‘ј‚Мѓ—ѓXѓ^ѓ}Ѓ[‚Йђ„‘E‚µ‚Ь‚·‚хЃH");
	define("RECOMMEND_ARTICLE_MSG", "‚щ‚М‹LЋ–‚р‘ј‚Мѓ—ѓXѓ^ѓ}Ѓ[‚Йђ„‘E‚µ‚Ь‚·‚хЃH");
	define("RATE_IT_MSG", "•]‰ї");
	define("NAME_ALIAS_MSG", "–ј‘O‚Ь‚Ѕ‚Нѓjѓbѓ™ѓlЃ[ѓЂ");
	define("SHOW_MSG", "•\Ћ¦");
	define("FOUND_MSG", "Њ©‚В‚х‚и‚Ь‚µ‚Ѕ");
	define("RATING_MSG", "•]‰ї•Ѕ‹П");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>