<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Manuais");
	define("NO_MANUALS_MSG", "Nenhum manual foi encontrado");
	define("NO_MANUAL_ARTICLES_MSG", "Nгo existem artigos");
	define("MANUALS_PREV_ARTICLE", "Anterior");
	define("MANUALS_NEXT_ARTICLE", "Seguinte");
	define("MANUAL_CONTENT_MSG", "Нndice");
	define("MANUALS_SEARCH_IN_MSG", "Pesquisar em");
	define("MANUALS_SEARCH_FOR_MSG", "Pesquisar");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Todos os Manuais");
	define("MANUALS_SEARCH_RESULTS_INFO", "Encontrado(s) {results_number} artigo(s) para {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Resultados da Pesquisa");
	define("MANUALS_NOT_FOUND_ANYTHING", "Nada encontrado para '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Sem conteъdo");
	define("MANUALS_SEARCH_TITLE", "Pesquisa dos Manuais");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Resultados da Pesquisa dos Manuais");

?>