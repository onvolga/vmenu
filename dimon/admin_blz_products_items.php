<?php
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/sorter.php");
	include_once($root_folder_path . "includes/navigator.php");
	include_once("./admin_common.php");

	check_admin_security("products_reviews");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","admin_blz_products_items.html");

    $t->set_var('site_url', $site_url);
    
    $operation = get_param('operation');
	$item_id = get_param('item_id');
    $category_id = get_param('category_id');
    if($operation == 'cancel'){
        header('Location: ' . './admin_items_list.php?category_id='.$category_id);
        exit;
    }
    if($operation == 'save'){

        $checkboxes = intval(get_param('checkboxes'));
        $selected_chb = array();
        //clear existed
        $db->query("DELETE FROM  " . $table_prefix . "blz_items_icons_assigned WHERE item_id = " . $item_id);
        for($i = 0; $i < $checkboxes; $i++){
            $chb = intval(get_param('chb_' . $i));
            if($chb > 0){
                $selected_chb[] = $chb;
            }
        }
        foreach ($selected_chb as $id){
            $stmt = "INSERT INTO " . $table_prefix . "blz_items_icons_assigned (item_id, icon_id) VALUES (" . $item_id . ", " . $id .")";
            $db->query($stmt);
        }

        header('Location: ' . './admin_items_list.php?category_id='.$category_id);
        exit;
    }
    
    $db->query("
        SELECT ii.*, iia.icon_id AS checked FROM " . $table_prefix . "blz_items_icons ii
        LEFT JOIN " . $table_prefix . "blz_items_icons_assigned iia ON ii.icon_id = iia.icon_id AND iia.item_id = " . $item_id . "
        ORDER BY ii.icon_order ASC"
    );
	if ($db->next_record())
	{
        $boxes = 0;
		$t->set_var("no_records", "");
		do {
            $t->set_var('cb_name', $boxes++);
            $t->set_var('icon_path', $db->f('icon_path'));
            $t->set_var('icon_order', $db->f('icon_order'));
            $t->set_var('icon_name', $db->f('icon_name'));
            $checked = $db->f('checked');
            if($checked){
                $t->set_var('checked', 'checked="checked"');
            }
            else{
                $t->set_var('checked', '');
            }

            $t->set_var('icon_id', $db->f('icon_id'));
            $t->parse("records");
		} while ($db->next_record());
	}
	else
	{
		$t->set_var("records", "");
		$t->parse("no_records", false);
	}
    $t->set_var("checkboxes", $boxes);
    $t->set_var("item_id", $item_id);
    $t->set_var("category_id", $category_id);
	include_once("./admin_header.php");
	include_once("./admin_footer.php");
    
	$t->pparse("main"); 
?>