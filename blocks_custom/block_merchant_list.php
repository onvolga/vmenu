<?php

    include_once("./includes/shopping_cart.php");
    include_once("./includes/order_items.php");
    include_once("./messages/" . $language_code . "/cart_messages.php");

    $default_title = "Список продавцов";

    $html_template = get_setting_value($block, "html_template", "block_merchant_list.html"); 
    $t->set_file("block_body", $html_template);

    

    $stmt = "SELECT rtot.m_sum/rtot.m_avg AS merchant_rating, rtot.m_avg AS messages, u.* 
        FROM va_users u inner join va_user_types ut on u.user_type_id = ut.type_id AND ut.is_seller = 1 
        LEFT JOIN (SELECT COUNT(*) AS m_avg, SUM(rating) AS m_sum, merchant_id  FROM va_user_reviews WHERE approved=1 AND rating <> 0 GROUP BY merchant_id) rtot ON rtot.merchant_id = u.user_id
        ORDER BY merchant_rating DESC
    ";

    $db->query($stmt);

    while ($db->next_record()) {

        $t->set_var("company_name", $db->f("company_name"));
        $t->set_var("messages", ($db->f("messages"))?$db->f("messages"):0);
        
        $short_description = $db->f("short_description");       
        $t->set_var("short_description", $short_description);       
        if(strlen($short_description)){
            $t->parse("seller_short_description_block", false);
        }
        else{
            $t->set_var("seller_short_description_block", false);
        }

        $user_site = $db->f("user_site");       
        $t->set_var("user_site", $user_site);       
        if(strlen($user_site)){
            $t->parse("seller_site_block", false);
        }
        else{
            $t->set_var("seller_site_block", false);
        }

        $t->set_var("user_id", $db->f("user_id"));

        $t->set_var("seller_logo", $db->f("personal_image"));
        $merchant_rating = round($db->f("merchant_rating"), 2);
        $average_rating = round($merchant_rating, 0);
        $average_rating_image = $average_rating ? "rating-" . $average_rating : "not-rated";
        $t->set_var("average_rating_image", $average_rating_image);
        $t->set_var("average_rating_alt", $average_rating_float);
        //num rating
        $t->set_var("merchant_rating", $merchant_rating);
        $t->parse("seller_data_block", true);
    }

    $block_parsed = true;
?>