<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_product_registrations.php                           ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/editgrid.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	
	check_user_security("access_product_registration");

	$cms_page_code = "user_product_registrations";
	$script_name   = "user_product_registrations.php";
	$current_page  = get_custom_friendly_url("user_product_registrations.php");
	$auto_meta_title = MY_PRODUCT_REGISTRATIONS_MSG;
	
	include_once("./includes/page_layout.php");

?>
