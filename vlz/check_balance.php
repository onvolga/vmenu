<?php

	/**
	 *
	 * Скрипт проверки баланса
	 *
	 */
	
	include_once("./includes/common.php");
	
	// Получаем всех пользователей в совокупности с их кредитам
	$sql_us = "SELECT * FROM " . $table_prefix . "users AS u ";
	$sql_us .= " FULL OUTER JOIN " . $table_prefix . "users_credits AS uc ON u.user_id = uc.user_id";
	$db->query($sql_us);
	
	// Подсчитываем текущий баланс всех юзеров
	// $sql = "SELECT * FROM " . $table_prefix . "users_credits";
	// $db->query($sql);
	$users_balance_array = array();
	while($db->next_record()) {
		$users_balance_array[$db->f('user_id')] += $db->f('credit_amount') * $db->f('credit_action');
	}
	
	var_dump($users_balance_array);
	
	// 
	foreach($users_balance_array as $user_id_key => $user_credit_item) {
		
		// Получаем предметы, принадлежащие продавцу, по которым еще не было проведено списание
		$general_sql = "SELECT * FROM " . $table_prefix . "orders_items AS oi ";
		$general_sql .= " JOIN " . $table_prefix . "orders AS o ON o.order_id = oi.order_id ";
		$general_sql .= " LEFT OUTER JOIN " . $table_prefix . "writes_off AS woff ON woff.order_item_id = oi.order_item_id ";
		$general_sql .= "WHERE oi.item_user_id = " . $user_id_key . " AND woff.order_item_id is NULL ";
		
		// Комиссионные за предыдущий месяц
		if(date('n') == 1) { // В январе смотрим на декабрь предыдущего года
			$additional_sql_prev = " AND YEAR(o.order_placed_date) = YEAR(NOW()) - 1 AND MONTH(o.order_placed_date) = 12";
		} else { // В остальное время смотрим на предыдущий месяц текущего года
			$additional_sql_prev = " AND YEAR(o.order_placed_date) = YEAR(NOW()) AND MONTH(o.order_placed_date) = MONTH(NOW()) - 1";
		}
		
		$db->query($general_sql . $additional_sql_prev);
		$summ_commisions_last_month = 0;
		while($db->next_record()) { 
			$summ_commisions_last_month += $db->f('merchant_commission') * $db->f('quantity');
		}

		$additional_sql_current = " AND YEAR(o.order_placed_date) = YEAR(NOW()) AND MONTH(o.order_placed_date) = MONTH(NOW())";
		$db->query($general_sql . $additional_sql_current);
		$summ_commisions_current_month = 0;
		while($db->next_record()) { 
			$summ_commisions_current_month += $db->f('merchant_commission') * $db->f('quantity');
		}
		
		// Получаем разницу между текущем балансом и блокированными комиссионными прошлого месяца и текущего
		$diff_balance = $user_credit_item - $summ_commisions_last_month - $summ_commisions_current_month;
		
		echo "Разница блокированного и баланса: " . $diff_balance . "<br/>";
		
		if($diff_balance < 50) {
			echo "Шлем письмо<br/>";
		}
		
		if($diff_balance > 0) {
			echo "Работаем с юзером ".$user_id_key."<br/>";
		} else {
			// Что-то выключаем			
			echo "Блокируем юзера ".$user_id_key."<br/>";
		}
		echo "------------------<br/>";
		
	}