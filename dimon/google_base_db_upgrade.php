<?php
/*
 *
 * ViArt Google Base attributes and types patch
 *
 */

	include_once ("./admin_config.php");
	include_once ($root_folder_path . "includes/common.php");
	include_once ($root_folder_path . "includes/record.php");
	include_once ($root_folder_path . "includes/shopping_cart.php");
	include_once ($root_folder_path . "messages/".$language_code."/cart_messages.php");
	include_once("./admin_common.php");

	
	check_admin_security("import_export");
	check_admin_security("products_export_google_base");

	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Apparel & Accessories' WHERE type_name = 'Apparel'";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Media > Books' WHERE type_name = 'Books'";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Media > Music' WHERE type_name = 'Music'";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Media > DVDs & Movies' WHERE type_name = 'Movies'";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Software > Video Game Software' WHERE type_name = 'Video and PC Games'";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Apparel & Accessories > Shoes' WHERE type_name = 'Shoes'";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_types SET type_name = 'Apparel & Accessories > Jewelry' WHERE type_name = 'Jewelry'";
	$db->query($sql);
	$sql = "INSERT INTO " . $table_prefix . "google_base_types (type_name) VALUES ('Apparel & Accessories > Clothing')";
	$db->query($sql);
	$sql = "UPDATE " . $table_prefix . "google_base_attributes SET attribute_name = 'age_group' WHERE attribute_name = 'age_range'";
	$db->query($sql);
	$sql = "INSERT INTO " . $table_prefix . "google_base_attributes (attribute_name,attribute_type,value_type) VALUES ('gender','g','string'),('pattern','g','string'),('item_group_id','g','string')";
	$db->query($sql);
	echo 'DB patched successfully';
	
?>