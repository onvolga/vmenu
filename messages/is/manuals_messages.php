<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "HandbГ¦kur");
	define("NO_MANUALS_MSG", "Engar handbГ¦kur fundust");
	define("NO_MANUAL_ARTICLES_MSG", "Engar greinar");
	define("MANUALS_PREV_ARTICLE", "Fyrri");
	define("MANUALS_NEXT_ARTICLE", "NГ¦sta");
	define("MANUAL_CONTENT_MSG", "Efnisyfirlit");
	define("MANUALS_SEARCH_IN_MSG", "Leita Г­");
	define("MANUALS_SEARCH_FOR_MSG", "Leita");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Allar handbГ¦kur");
	define("MANUALS_SEARCH_RESULTS_INFO", "Fann {results_number} grein(ar) undir {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "LeitarniГ°urstГ¶Г°ur");
	define("MANUALS_NOT_FOUND_ANYTHING", "Ekkert fannst undir '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Ekkert innihald");
	define("MANUALS_SEARCH_TITLE", "Leita Г­ handbГіkum");
	define("MANUALS_SEARCH_RESULTS_TITLE", "NiГ°urstГ¶Г°ur leitar Г­ handbГіkum");

?>