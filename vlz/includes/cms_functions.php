<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  cms_functions.php                                        ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

function parse_cms_block_properties($properties_string, $property_type = "", $group_properties = false)
{
	$block_properties = array();
	if ($properties_string != "") {
		$properties_strings = explode("#property#", $properties_string);
		for ($p = 0; $p < sizeof($properties_strings); $p++) {
			$property_string = $properties_strings[$p];
			if (!$property_string) { continue; }
			$block_property = get_cms_params($property_string);
			if ($property_type && $block_property["type"] != $property_type) { continue; }
			if ($group_properties) {
				$property_id = $block_property["id"];
				$value = $block_property["value"];
				$block_properties[$property_id][] = $value;
			} else {
				$block_properties[] = $block_property;
			}
		}
	}	
	return $block_properties;
}

function get_cms_params($params_string)
{
	$params = array();
	$params_pairs = explode("&", $params_string);

	for ($p = 0; $p < sizeof($params_pairs); $p++) {
		$param_pair = $params_pairs[$p];
		$equal_pos = strpos($param_pair, "=");
		if($equal_pos === false) {
			$params[$param_pair] = "";
		} else {
			$param_name = substr($param_pair, 0, $equal_pos);
			$param_value = substr($param_pair, $equal_pos + 1);
			$params[$param_name] = decode_js_value($param_value);
		}
	}
	return $params;
}

function decode_js_value($js_value)
{
	$find = array("%25", "%2B", "%26", "%22", "%27", "%0A", "%0D", "%3D", "%7C", "%23");
	$replace = array("%", "+", "&", "\"", "'", "\n", "\r", "=", "|", "#");
	$js_value = str_replace($find, $replace, $js_value);
	return $js_value;
}

function check_category_layout($cms_page_code, $category_path, $category_id)
{
	global $db, $site_id, $table_prefix;
	$cms_ps_id = "";
	if (strlen($category_path)) {
		$categories_ids = trim($category_path, ",");
		$ids = explode(",", $categories_ids);
		$where = "";
		for ($c = 0; $c < sizeof($ids); $c++) {
			$id = $ids[$c];
			if ($where) { $where .= " OR "; }
			$where .= "key_code=" . $db->tosql($id, TEXT);		
		}
		$layout_types = array();
		$cms_ps_ids = array();
		if ($where) {
			$sql  = " SELECT cps.ps_id, cps.key_code, cps.key_rule ";
			$sql .= " FROM (" . $table_prefix . "cms_pages_settings cps ";
			$sql .= " INNER JOIN " . $table_prefix . "cms_pages cp ON cp.page_id=cps.page_id) ";
			$sql .= " WHERE cp.page_code=" . $db->tosql($cms_page_code, TEXT);
			if (isset($site_id) && $site_id != 1) {
				$sql .= " AND (site_id=1 OR site_id=" . $db->tosql($site_id, INTEGER) . ") ";
			} else {
				$sql .= " AND site_id=1 ";
			}
			$sql .= " AND (" . $where . ") ";
			$sql .= " AND key_type='category' ";
			$sql .= " ORDER BY site_id ASC ";
			$db->query($sql);
			while ($db->next_record()) {
				$row_ps_id = $db->f("ps_id");
				$row_key_code = $db->f("key_code");
				$row_key_rule = $db->f("key_rule");
				$layout_types[$row_key_code] = $row_key_rule;
				$cms_ps_ids[$row_key_code] = $row_ps_id;
			}
		}

		for ($c = (sizeof($ids) - 1); $c >= 0; $c--) {
			$id = $ids[$c];
			if (isset($layout_types[$id])) {
				$key_rule = $layout_types[$id];
				if ($key_rule == "all" || $key_rule == "") {
					$cms_ps_id = $cms_ps_ids[$id];
					break;
				} else if ($key_rule == "category" && $id == $category_id) {
					$cms_ps_id = $cms_ps_ids[$id];
					break;
				}
			}
		}
	}
	return $cms_ps_id;
}

?>