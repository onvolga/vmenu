<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// повідомлення мануалів
	define("MANUALS_TITLE", "Документації");
	define("NO_MANUALS_MSG", "Жодної документації не знайдено");
	define("NO_MANUAL_ARTICLES_MSG", "Жодної статті не знайдено");
	define("MANUALS_PREV_ARTICLE", "Попередня");
	define("MANUALS_NEXT_ARTICLE", "Наступна");
	define("MANUAL_CONTENT_MSG", "Зміст");
	define("MANUALS_SEARCH_IN_MSG", "Шукати в");
	define("MANUALS_SEARCH_FOR_MSG", "Шукати");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Усі документації");
	define("MANUALS_SEARCH_RESULTS_INFO", "Знайдено {results_number} статті(я) для {search_result}");
	define("MANUALS_SEARCH_RESULT_MSG", "Результати пошуку");
	define("MANUALS_NOT_FOUND_ANYTHING", "Нічого не знайдено для '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Зміст відсутній");
	define("MANUALS_SEARCH_TITLE", "Пошук документації ");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Результати пошуку документації");

?>