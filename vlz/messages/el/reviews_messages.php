<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "РспбйсефйкЬ");
	define("BAD_MSG", "Кбкь");
	define("POOR_MSG", "Цфщчь");
	define("AVERAGE_MSG", "МЭупт ьспт");
	define("GOOD_MSG", "Кбль");
	define("EXCELLENT_MSG", "ХрЭспчп");
	define("REVIEWS_MSG", "КсйфйкЭт");
	define("NO_REVIEWS_MSG", "Ден всЭизкбн КсйфйкЭт");
	define("WRITE_REVIEW_MSG", "гсЬше мйб ксйфйкЮ");
	define("RATE_PRODUCT_MSG", "Вбимпльгзуе бхфь фп рспъьн");
	define("RATE_ARTICLE_MSG", "Вбимпльгзуе бхфь фп Ьсисп");
	define("NOT_RATED_PRODUCT_MSG", "Фп рспъьн бхфь ден ёчей вбимплпгзиеЯ бкьмз");
	define("NOT_RATED_ARTICLE_MSG", "Фп Ьсисп ден ёчей вбимплпгзиеЯ бкьмз");
	define("AVERAGE_RATING_MSG", "МЭупт ьспт вбимплпгЯбт");
	define("BASED_ON_REVIEWS_MSG", "ВбуйумЭнпт уе {total_votes} КсйфйкЭт");
	define("POSITIVE_REVIEW_MSG", "ИефйкЭт КсйфйкЭт ерйукерфюн");
	define("NEGATIVE_REVIEW_MSG", "БснзфйкЭт КсйфйкЭт ерйукерфюн");
	define("SEE_ALL_REVIEWS_MSG", "Дет ьлет фйт КсйфйкЭт");
	define("ALL_REVIEWS_MSG", "ьлет пй КсйфйкЭт ");
	define("ONLY_POSITIVE_MSG", "Мьнп пй ИефйкЭт");
	define("ONLY_NEGATIVE_MSG", "Мьнп пй БснзфйкЭт");
	define("POSITIVE_REVIEWS_MSG", "ИефйкЭт КсйфйкЭт ерйукерфюн");
	define("NEGATIVE_REVIEWS_MSG", "БснзфйкЭт КсйфйкЭт ерйукерфюн");
	define("SUBMIT_REVIEW_MSG", "Убт ехчбсйуфпэме <br> Иб еоефЬупхме фб учьлйб убт кбй бн ден бнфйвбЯнпхн фзн рплйфйкЮ фзт йуфпуелЯдбт мбт<br>иб фб емцбнЯупхме.");
	define("ALREADY_REVIEW_MSG", "ёчейт Юдз гсЬшей ксйфйкЮ");
	define("RECOMMEND_PRODUCT_MSG", "ИЭлейт нб рспфеЯнейт бхфь фп рспъьн уе Ьллпхт ?");
	define("RECOMMEND_ARTICLE_MSG", "ИЭлейт нб рспфеЯнейт бхфь фп Ьсисп уе Ьллпхт ?");
	define("RATE_IT_MSG", "Вбимпльгзуе фп");
	define("NAME_ALIAS_MSG", "јнпмб Ю рбсбфупэклй");
	define("SHOW_MSG", "Дет");
	define("FOUND_MSG", "всЭизкбн");
	define("RATING_MSG", "МЭупт ьспт вбимплпгЯбт");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>