<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Valinnainen");
	define("BAD_MSG", "Todella huono");
	define("POOR_MSG", "Huono");
	define("AVERAGE_MSG", "Keskinkertainen");
	define("GOOD_MSG", "Hyvд");
	define("EXCELLENT_MSG", "Erinomainen");
	define("REVIEWS_MSG", "Arvostelut");
	define("NO_REVIEWS_MSG", "Arvosteluja ei lцytynyt");
	define("WRITE_REVIEW_MSG", "Kirjoita arvostelu");
	define("RATE_PRODUCT_MSG", "Arvostele tдmд tuote");
	define("RATE_ARTICLE_MSG", "Arvostele tдmд artikkeli");
	define("NOT_RATED_PRODUCT_MSG", "Tдtд tuotetta ei vielд ole arvosteltu");
	define("NOT_RATED_ARTICLE_MSG", "Artikkelia ole ole vielд arvosteltu");
	define("AVERAGE_RATING_MSG", "Keskiarvo");
	define("BASED_ON_REVIEWS_MSG", "based on {total_votes} reviews");
	define("POSITIVE_REVIEW_MSG", "Positiivinen asiakasarvostelu");
	define("NEGATIVE_REVIEW_MSG", "Negatiivinen asiakasarvostelu");
	define("SEE_ALL_REVIEWS_MSG", "Katso kaikki arvostelut");
	define("ALL_REVIEWS_MSG", "Kaikki arvostelut");
	define("ONLY_POSITIVE_MSG", "Vain positiviiset");
	define("ONLY_NEGATIVE_MSG", "Vain negatiiviset");
	define("POSITIVE_REVIEWS_MSG", "Positiviiset arvostelut");
	define("NEGATIVE_REVIEWS_MSG", "Negatiiviset arvostelut");
	define("SUBMIT_REVIEW_MSG", "Kiitos!<br>Kommenttisi tarkistetaan.Jos se hyvдksytддn, se julkaistaan tддllд.<br>Pidдtдmme oikeuden olla julkaisematta kaikkia arvosteluja/kommentteja");
	define("ALREADY_REVIEW_MSG", "Olet jo tehnyt arvostelun");
	define("RECOMMEND_PRODUCT_MSG", "Would you recommend<br> this product to others?");
	define("RECOMMEND_ARTICLE_MSG", "Would you recommend<br> this article to others?");
	define("RATE_IT_MSG", "Arvostele");
	define("NAME_ALIAS_MSG", "Nimi / Lempinimi");
	define("SHOW_MSG", "Nдytд");
	define("FOUND_MSG", "Lцydetty");
	define("RATING_MSG", "Keskiarvo");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>