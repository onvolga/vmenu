<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Opsiyonel");
	define("BAD_MSG", "Berbat");
	define("POOR_MSG", "Zayэf");
	define("AVERAGE_MSG", "Eh iюte..");
	define("GOOD_MSG", "Эyi");
	define("EXCELLENT_MSG", "Mьkemmel");
	define("REVIEWS_MSG", "Yorumlar");
	define("NO_REVIEWS_MSG", "Bir yorum bulunamadэ.");
	define("WRITE_REVIEW_MSG", "Yorum yaz.");
	define("RATE_PRODUCT_MSG", "Bu ьrьne oy ver.");
	define("RATE_ARTICLE_MSG", "Bu makaleye oy ver.");
	define("NOT_RATED_PRODUCT_MSG", "Ьrьne henьz oy verilmedi.");
	define("NOT_RATED_ARTICLE_MSG", "Makaleye henьz oy verilmedi.");
	define("AVERAGE_RATING_MSG", "Ьrьne Verilen Ortalama Oy");
	define("BASED_ON_REVIEWS_MSG", "toplam {total_votes} yoruma dayanэlarak belirlenmiюtir.");
	define("POSITIVE_REVIEW_MSG", "Olumlu Yorumlar");
	define("NEGATIVE_REVIEW_MSG", "Olumsuz Yorumlar");
	define("SEE_ALL_REVIEWS_MSG", "Tьm yorumlarэ gцr..");
	define("ALL_REVIEWS_MSG", "Tьm Yorumlar ");
	define("ONLY_POSITIVE_MSG", "Sadece Olumlular");
	define("ONLY_NEGATIVE_MSG", "Sadece Olumsuzlar");
	define("POSITIVE_REVIEWS_MSG", "Olumlu Yorumlar");
	define("NEGATIVE_REVIEWS_MSG", "Olumsuz Yorumlar");
	define("SUBMIT_REVIEW_MSG", "Tesekkьr ederiz<br>Yorumunuz onaydan geзtikten sonra yayinlanacaktir.<br>Yorumu yayinlamayi reddetme hakkimiz saklidir.");
	define("ALREADY_REVIEW_MSG", "Halihazэrda bir yorum gцnderdiniz.");
	define("RECOMMEND_PRODUCT_MSG", "Bu ьrьnь baюkalarэna <br> tavsiye eder misiniz?");
	define("RECOMMEND_ARTICLE_MSG", "Bu makaleyi baюkalarэna <br> tavsiye eder misiniz?");
	define("RATE_IT_MSG", "Oy Ver");
	define("NAME_ALIAS_MSG", "Adэ");
	define("SHOW_MSG", "Gцster");
	define("FOUND_MSG", "Bulunan");
	define("RATING_MSG", "Ortalama Oy");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>