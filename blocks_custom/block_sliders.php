<?php 

	$default_title = "";

	$pb_id = $block["pb_id"];
	$block_id = $block["block_key"];

	set_script_tag("js/slider.js");
	$slider_type = get_setting_value($vars, "slider_type", "5");//7 carousel
    
    $items_per_slide = get_setting_value($vars, "items_per_slide", "1");//how many images should rotate
    $control_padding = get_setting_value($vars, "control_padding", "0");//js try to place items on screen
	$blz_show_titles = get_setting_value($vars, "blz_show_titles", "0");//parse row with title
	$blz_elem_width = get_setting_value($vars, "blz_elem_width", "0");//set col width;

	$block_view_type = get_setting_value($vars, "block_view_type", "1");
    if($slider_type == 7){
        set_link_tag("styles/carousel.css", "stylesheet", "text/css");
        $block_view_type = 7;
    }
	$slider_width = get_setting_value($vars, "slider_width", "");
	$slider_height = get_setting_value($vars, "slider_height", "");

	if ($block_view_type == 1) {
		$html_template = get_setting_value($block, "html_template", "block_sliders.html");
	}
    else if ($block_view_type == 2) {
		$default_title = "";
		$html_template = get_setting_value($block, "html_template", "block_sliders.html");
	} 
    else if($block_view_type === 7){/*carousel*/
        $default_title = "";
		$html_template = get_setting_value($block, "html_template", "block_slider_carousel.html");
    }else {
		$html_template = get_setting_value($block, "html_template", "block_sliders_content.html");
	}
	$t->set_file("block_body", $html_template);
	$t->set_var("pb_id", $pb_id);
	$t->set_var("slider_type", $slider_type);
	$t->set_var("data", "");
	$t->set_var("cols", "");
	$t->set_var("rows", "");
    if($slider_type == 7){
        $t->set_var("items_per_slide", $items_per_slide);
    }
	// check slider default values
	$sql  = " SELECT slider_height, slider_width, slider_title ";
	$sql .= " FROM ". $table_prefix . "sliders";
	$sql .= " WHERE slider_id = " . $db->tosql($block_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		if (!$slider_width) {
			$slider_width = $db->f("slider_width");
		}
		if (!$slider_height) {
			$slider_height = $db->f("slider_height");
		}
		$default_title = $db->f("slider_title");
	}

	$sql  = " SELECT COUNT(*) ";
	$sql .= " FROM " . $table_prefix . "sliders_items ";				
	$sql .= " WHERE slider_id=" . $db->tosql($block_id, INTEGER);
	$sql .= " AND show_for_user=1 ";
	$slider_records = get_db_value($sql);
    $t->set_var('slides_qty', $slider_records);
	if ($slider_type == 1 || $slider_type == 3) { // vertical
		$t->set_var("column_width", "100%");
		$t->set_var("table_width", "100%");
	} else if ($slider_type == 2 || $slider_type == 4) { // horizontal
		$t->set_var("column_width", intval($slider_width));
		$t->set_var("table_width", intval($slider_width) * $slider_records);
	} else if ($slider_type == 5) { // slideshow
		$t->set_var("column_width", "100%");
		$t->set_var("table_width", "100%");
	}

	$slider_width = get_css_dim($slider_width);
	$slider_height = get_css_dim($slider_height);
	$max_image_height = 0;
	
	$data_block_style = "";
	if (strlen($slider_width)) {
		$data_block_style .= "width: " . $slider_width . "; ";
	}
	if (strlen($slider_height)) {
		$data_block_style .= "height: " . $slider_height. "; ";
	}
	$t->set_var("data_block_style", $data_block_style);

	$row = 0;
	$sql  = " SELECT item_id, item_name, slider_image, slider_link, slider_html, item_order ";
	$sql .= " FROM " . $table_prefix . "sliders_items ";				
	$sql .= " WHERE slider_id=" . $db->tosql($block_id, INTEGER);
	$sql .= " AND show_for_user=1 ";
	$sql .= " ORDER BY item_order ";
	$db->query($sql);
	while ($db->next_record()) {
		$row++;
		$item_name = $db->f("item_name");
		$slider_html = $db->f("slider_html");
		$slider_image = $db->f("slider_image");
		$slider_link = $db->f("slider_link");
		if (!strlen($slider_link)) { 
			$slider_link = "#";
		}

		if ($slider_type != 5) {
			$t->set_var("data_id", "data_".$pb_id);
		} else if ($slider_type == 5) {
			$t->set_var("data_id", "data_".$pb_id."_".$row);
			if ($row == 1) {
				$t->set_var("data_style", "display: block; ");
			} else {
				$t->set_var("data_style", "display: none; ");
			}
		}
        if($slider_type == 7) {
            $t->set_var("slideName", $item_name);
        }
        
		if ($slider_image) {
			if (preg_match("/^http\:\/\//", $slider_image)) {
				$image_size = "";
			} else {
				$image_size = @GetImageSize($slider_image);
			}
			$t->set_var("src", htmlspecialchars($slider_image));
			if(is_array($image_size)) {
                if(intval($image_size[1]) > $max_image_height){
                    $max_image_height = $image_size[1];
                }
				if($blz_elem_width == 0 && $slider_type == 7){
					$t->set_var("width", "width=\"" . $image_size[0] . "\"");
					$t->set_var("eWidth", $image_size[0]);
				}
				elseif($blz_elem_width == 0 || $slider_type != 7){
					$t->set_var("width", "width=\"" . $image_size[0] . "\"");
				}
				
				else{
					$t->set_var("width", "width=\"" . intval($blz_elem_width) . "\"");
					$t->set_var("eWidth", $blz_elem_width);
					
				}
				$t->set_var("height", "height=\"" . $image_size[1] . "\"");
			} else {
				$t->set_var("width", "");
				$t->set_var("height", "");
			}
			$t->set_var("slider_link", htmlspecialchars($slider_link));
			$t->parse("slider_image", false);
		} else {
			$t->set_var("slider_image", "");
		}
		$t->set_var("slider_html", $slider_html);

		if ($slider_type == 1 || $slider_type == 3) { // vertical 
			$t->parse("cols", false);
			$t->parse("rows", true);
		} else if ($slider_type == 2 || $slider_type == 4) {
			$t->parse("cols", true);
		} 
        else if ($slider_type == 7){
			if($blz_show_titles == 1){
				$t->parse("slider_title", false);
			}
			else{
				$t->set_var("slider_title", false);
			}
            $t->parse("li", true);
        }
        else {
			$t->parse("cols", false);
			$t->parse("rows", false);
			$t->parse("data", true);
		}
	}
	if($blz_show_titles == 1){
		$max_image_height += 30;
	}
	
	$cPad = ($control_padding == 1) ? 1 : 0;
	$t->set_var("checkPadding", $cPad);


    $t->set_var("elemHeight", $max_image_height);
	if ($slider_type != 5) {
		if ($slider_type == 2 || $slider_type == 4) {
			$t->parse("rows", true);
		}
		$t->parse("data", false);
	}

	$block_parsed = true;

?>