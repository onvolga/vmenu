<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// mensajes de manuales
	define("MANUALS_TITLE", "Manuales");
	define("NO_MANUALS_MSG", "No se encontraron manuales");
	define("NO_MANUAL_ARTICLES_MSG", "No hay artнculos");
	define("MANUALS_PREV_ARTICLE", "Anterior");
	define("MANUALS_NEXT_ARTICLE", "Siguiente");
	define("MANUAL_CONTENT_MSG", "Index");
	define("MANUALS_SEARCH_IN_MSG", "Buscar en");
	define("MANUALS_SEARCH_FOR_MSG", "Buscar");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Todos los Manuales");
	define("MANUALS_SEARCH_RESULTS_INFO", "Encontrado(s) {results_number} artнculo(s) por {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Resultados de la bъsqueda");
	define("MANUALS_NOT_FOUND_ANYTHING", "No se ha encontrado nada para '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "No hay contenido");
	define("MANUALS_SEARCH_TITLE", "Buscar Manuales");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Resultados de la bъsqueda de Manuales");

?>