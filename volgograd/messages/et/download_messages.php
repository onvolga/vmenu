<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Vale(d) allalaadimise parameeter(-id).");
	define("DOWNLOAD_MISS_PARAM", "Puuduv(ad) allalaadimise parameeter(-id).");
	define("DOWNLOAD_INACTIVE", "Allalaadimine passiivne.");
	define("DOWNLOAD_EXPIRED", "Sinu allalaadimise periood on lС…ppenud.");
	define("DOWNLOAD_LIMITED", "Oled СЊletanud maksimaalse allalaadimiste arvu.");
	define("DOWNLOAD_PATH_ERROR", "Ei leita teed tooteni ");
	define("DOWNLOAD_RELEASE_ERROR", "Uuendust ei leitud.");
	define("DOWNLOAD_USER_ERROR", "Seda faili saavad alla laadida ainult registreerunud kasutajad.");
	define("ACTIVATION_OPTIONS_MSG", "Aktiveerimise valikud");
	define("ACTIVATION_MAX_NUMBER_MSG", "Aktiveerimiste maksimaalne arv");
	define("DOWNLOAD_OPTIONS_MSG", "Allalaetav / tarkvara valikud");
	define("DOWNLOADABLE_MSG", "Allalaetav (tarkvara)");
	define("DOWNLOADABLE_DESC", "Allalaetavatel toodetel saab tРґpsustada ka 'Allalaadimise perioodi', 'Teed allalaetava failini' ja 'Aktiveerimise valikuid'");
	define("DOWNLOAD_PERIOD_MSG", "Allalaadimise periood");
	define("DOWNLOAD_PATH_MSG", "Tee allalaetava failini");
	define("DOWNLOAD_PATH_DESC", "VС…id lisada mitu teed eraldades need semikoolonitega");
	define("UPLOAD_SELECT_MSG", "Vali fail СЊleslaadimiseks ja vajuta {button_name} nuppu.");
	define("UPLOADED_FILE_MSG", "Fail <b>{filename}</b> on СЊles laetud.");
	define("UPLOAD_SELECT_ERROR", "Esmalt palun vali fail.");
	define("UPLOAD_IMAGE_ERROR", "Lubatud on ainult pildifailid.");
	define("UPLOAD_FORMAT_ERROR", "Seda tСЊСЊpi fail ei ole lubatud.");
	define("UPLOAD_SIZE_ERROR", "Failid suuremad kui {filesize} ei ole lubatud.");
	define("UPLOAD_DIMENSION_ERROR", "Pildid suuremad kui  {dimension}  ei ole lubatud.");
	define("UPLOAD_CREATE_ERROR", "SСЊsteem ei saa luua faili.");
	define("UPLOAD_ACCESS_ERROR", "Sul ei ole luba failide СЊleslaadimiseks.");
	define("DELETE_FILE_CONFIRM_MSG", "Kas oled kindel, et tahad seda faili kustutada?");
	define("NO_FILES_MSG", "Ei leitud СЊhtegi faili");
	define("SERIAL_GENERATE_MSG", "Genereeri seerianumber");
	define("SERIAL_DONT_GENERATE_MSG", "Р”ra genereeri");
	define("SERIAL_RANDOM_GENERATE_MSG", "Genereeri juhuslik seeria tarkvara tootele");
	define("SERIAL_FROM_PREDEFINED_MSG", "VС…ta seerianumber eelnevalt mРґРґratud loetelust");
	define("SERIAL_PREDEFINED_MSG", "Eelnevalt mРґРґratud seerianumbrid");
	define("SERIAL_NUMBER_COLUMN", "Seerianumber");
	define("SERIAL_USED_COLUMN", "Kasutatud");
	define("SERIAL_DELETE_COLUMN", "Kustuta");
	define("SERIAL_MORE_MSG", "Lisa veel seerianumbreid?");
	define("SERIAL_PERIOD_MSG", "Seerianumbri periood");
	define("DOWNLOAD_SHOW_TERMS_MSG", "NРґita tingimusi ja nС…udmisi");
	define("DOWNLOAD_SHOW_TERMS_DESC", "Toote allalaadimiseks peab kasutaja lРґbi lugema ning nС…ustuma meie tingimuste ja nС…udmistega");
	define("DOWNLOAD_TERMS_MSG", "Tingimused ja nС…udmised");
	define("DOWNLOAD_TERMS_USER_DESC", "Olen lРґbi lugenud ning nС…ustun Teie tingimuste ja nС…udmistega");
	define("DOWNLOAD_TERMS_USER_ERROR", "Toote allalaadimiseks peate Teie lРґbi lugema ning nС…ustuma meie tingimuste ja nС…udmistega");

	define("DOWNLOAD_TITLE_MSG", "Allalaadimise pealkiri (1305)");
	define("DOWNLOADABLE_FILES_MSG", "Allalaetavad failid");
	define("DOWNLOAD_INTERVAL_MSG", "Allalaadimise intervall");
	define("DOWNLOAD_LIMIT_MSG", "Allalaadimiste limiit");
	define("DOWNLOAD_LIMIT_DESC", "arv kordi, mil faili saab alla laadida");
	define("MAXIMUM_DOWNLOADS_MSG", "Maksimaalne allalaadimiste arv");
	define("PREVIEW_TYPE_MSG", "Eelvaate tСЊСЊp");
	define("PREVIEW_TITLE_MSG", "Eelvaate pealkiri");
	define("PREVIEW_PATH_MSG", "Tee eelvaate failini");
	define("PREVIEW_IMAGE_MSG", "Pildi eelvaade (1314)");
	define("MORE_FILES_MSG", "Veel faile");
	define("UPLOAD_MSG", "Lae СЊles ");
	define("USE_WITH_OPTIONS_MSG", "Kasuta ainult koos valikutega");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Eelvaade alla laadimisel (1318)");
	define("PREVIEW_USE_PLAYER_MSG", "Kasuta mРґngijat eelvaateks");
	define("PROD_PREVIEWS_MSG", "Eelvaated");

?>