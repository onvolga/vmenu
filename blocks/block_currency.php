<?php

	$default_title = CURRENCY_TITLE;
/*                           
  * Файл дополнен
  * MainOlMa
  * Добавление опции simple_currency - 2 вставки - 06.05.2011
  * для упрощённого вывода валют в шапке
*/
/*--------------------Добавление опции simple_currency(1/2)---------------*/
$simple_currency = get_setting_value($vars, "simple_currency", 0);
//если отмечен чекбокс в опциях блока валют, то выбирается другой шаблон
if ($simple_currency!=""){$html_template = get_setting_value($block, "html_template", "vi_header_currency.html");}
else
$html_template = get_setting_value($block, "html_template", "block_currency.html");
/*-------------------/Добавление опции simple_currency(1/2)---------------*/
	
	$currency_selection = get_setting_value($vars, "currency_selection", 1);
/*--------------------Добавление опции simple_currency(2/2)---------------*/
	//Закоментирована оригинальная строка ниже
	//$html_template = get_setting_value($block, "html_template", "block_currency.html");
/*-------------------/Добавление опции simple_currency(2/2)---------------*/ 
  $t->set_file("block_body", $html_template);
	$t->set_var("currencies_images", "");

	$currency = get_currency();
	$currency_code = $currency["code"];

	$query_string = transfer_params("", true);

	$sql = " SELECT currency_code, currency_title, currency_image, currency_image_active FROM " . $table_prefix . "currencies ";
	$sql .= " WHERE show_for_user=1 ";
	$db->query($sql);
	while ($db->next_record()) 
	{
		$row_currency_code = $db->f("currency_code");
		$row_currency_title = $db->f("currency_title");
		$currency_image = $db->f("currency_image");
		$currency_image_active = $db->f("currency_image_active");
		$currency_selected = ($currency_code == $row_currency_code) ? "selected" : "";
		$t->set_var("currency_selected", $currency_selected);
		$t->set_var("currency_code", $row_currency_code);
		$t->set_var("currency_title", $row_currency_title);
		if ($currency_selection != 2 && $currency_image) {
			// If current row currency is a selected by user, make it "highlighted" use active image
			// If it's not empty
			if ($currency_code == $row_currency_code && $currency_image_active != "") {
				$currency_image = $currency_image_active;
			}
			$image_size = preg_match("/^http\:\/\//", $currency_image) ? "" : @getimagesize($currency_image);
			$t->set_var("src", htmlspecialchars($currency_image));
			if (is_array($image_size)) {
				$t->set_var("width", "width=\"" . $image_size[0] . "\"");
				$t->set_var("height", "height=\"" . $image_size[1] . "\"");
			} else {
				$t->set_var("width", "");
				$t->set_var("height", "");
			}

			$currency_query = $query_string;
			if ($currency_query) {
				$currency_query .= "&";
			} else {
				$currency_query .= "?";
			}
 			$currency_query .= "currency_code=" . $row_currency_code; 
			$t->set_var("currency_query", htmlspecialchars($currency_query));

			$t->parse("currencies_images", true);
		} elseif ($currency_selection == 2) {
			$t->parse("currencies", true);
		}
	}

	if ($currency_selection == 2) {
		$t->set_var("currencies_images", "");
		$t->sparse("select_currencies", false);
	}

	$block_parsed = true;


?>