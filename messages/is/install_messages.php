<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "Uppsetning ViArt Verslunar");

	define("INSTALL_STEP_1_TITLE", "Uppsetning: Skref 1");
	define("INSTALL_STEP_1_DESC", "Takk fyrir aГ° velja ViArt Verslun. Til aГ° halda uppsetningu ГЎfram, vinsamlegast fylltu Гєt upplГЅsingarnar hГ©r aГ° neГ°an. AthugaГ°u aГ° gagnabankinn sem ГѕГє velur Г¦tti nГє Гѕegar aГ° vera til. Ef ГѕГє ert aГ° setja upp ГЎ gagnagrunn sem notar ODBC, t.d MS Access Г¦ttirГ°u fyrst aГ° ГєtbГєa DSN ГЎГ°ur en ГѕГє heldur ГЎfram.");
	define("INSTALL_STEP_2_TITLE", "Uppsetning: Skref 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Uppsetning: Skref 3");
	define("INSTALL_STEP_3_DESC", "Vinsamlegast veldu Гєtlit sГ­Г°u. ГћГє munt geta breytt Гєtliti seinna.");
	define("INSTALL_FINAL_TITLE", "Uppsetning: Lokaskref");
	define("SELECT_DATE_TITLE", "Veldu dagsetningarsniГ°");

	define("DB_SETTINGS_MSG", "Stillingar gagnagrunns");
	define("DB_PROGRESS_MSG", "GagnagrunnsupplГЅsingar sГіttar...");
	define("SELECT_PHP_LIB_MSG", "Veja PHP safn?");
	define("SELECT_DB_TYPE_MSG", "Velja gagnagrunnstegund");
	define("ADMIN_SETTINGS_MSG", "StjГіrnunarstillingar");
	define("DATE_SETTINGS_MSG", "DagsetningarsniГ°");
	define("NO_DATE_FORMATS_MSG", "Engin dagsetningarsniГ° tiltГ¦k");
	define("INSTALL_FINISHED_MSG", "NГє er grunnuppsetningu lokiГ°. Vinsamlegast athugaГ°u hvort aГ° allar stillingar Г­ stjГіrnunarhlutanum sГ©u rГ©ttar og breyttu ef ГѕГ¶rf krefur.");
	define("ACCESS_ADMIN_MSG", "Smelltu hГ©r til aГ° fГЎ aГ°gang aГ° stjГіrnunarhlutanum");
	define("ADMIN_URL_MSG", "StjГіrnunarslГіГ°");
	define("MANUAL_URL_MSG", "Manual URL");
	define("THANKS_MSG", "Takk fyrir aГ° velja <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "GerГ° gagnagrunns");
	define("DB_TYPE_DESC", "Vinsamlegast veldu ГѕГЎ <b>gerГ° gagnagrunns</b> sem ГѕГє notar. Ef ГѕГє ert aГ° nota SQL ГѕjГіn eГ°a Microsoft Access, veldu ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP safn");
	define("DB_HOST_FIELD", "Nafn hГЅsistГ¶lvu");
	define("DB_HOST_DESC", "Vinsamlegast slГЎГ°u inn <b>nafn</b> eГ°a <b>IP fang ГѕjГіnsins</b> sem mun hГЅsa ViArt gagnagrunninn. Ef ГѕГє notar heimilistГ¶lvu ГѕГ­na til aГ° hГЅsa gagnagrunninn Г¦tti aГ° duga aГ° slГЎ inn \"<b>localhost</b>\" og skilja port/tengi eftir tГіmt. Ef ГѕГє ert aГ° nota gagnagrunn hjГЎ hГЅsingaraГ°ila skaltu athuga upplГЅsingar frГЎ hГЅsingaraГ°ila varГ°andi stillingar ГѕjГіnsins.");
	define("DB_PORT_FIELD", "Tengi");
	define("DB_NAME_FIELD", "Nafn gagnagrunns / DSN");
	define("DB_NAME_DESC", "Ef ГѕГє ert aГ° nota MySQL eГ°a PostgreSQL eГ°a aГ°ra sambГ¦rilega gagnagrunna, vinsamlegast slГЎГ°u inn <b>nafn gagnagrunnsins</b> Гѕar sem ГѕГє vilt aГ° ViArt ГєtbГєi tГ¶flur gagnagrunnsins. Гћessi gagnagrunnur verГ°ur aГ° vera til Г­ kerfinu nГє Гѕegar. Ef ГѕГє ert aГ°eins aГ° setja upp ViArt Г­ tilraunaskyni ГЎ ГѕГ­na persГіnulegu tГ¶lvu, ГѕГЎ eiga flest kerfi aГ° bjГіГ°a upp ГЎ \"<b>prufu</b>\" gagnagrunn sem ГѕГє getur notaГ°. Ef ekki, ГєtbГєГ°u ГѕГЎ gagnagrunn eins og t.d. ААњviartААќ og notaГ°u hann. Ef ГѕГє ert aГ° nota Microsoft Access eГ°a SQL ГѕjГіn skal nafn gagnagrunnsins vera <b>DSN nafniГ°</b> sem ГѕГє hefur uppsett Г­ Gagnaveitu (ODBC) hluta stjГіrnborГ°sins (Control Panel).");
	define("DB_USER_FIELD", "Notendanafn");
	define("DB_PASS_FIELD", "LykilorГ°");
	define("DB_USER_PASS_DESC", "<b>Notendanafn</b> og <b>lykilorГ°</b> - Vinsamlegast slГЎГ°u inn ГѕaГ° notendanafn og lykilorГ° sem ГѕГє vilt nota til aГ° fГЎ aГ°gang aГ° gagnagrunninum. Ef ГѕГє ert aГ° nota reynsluГєtgГЎfu er notendanafniГ° lГ­klega \"<b>root</b>\" og lykilorГ°areiturinn hafГ°ur tГіmur. SlГ­kt fyrirkomulag er ГЎgГ¦tt fyrir tilraunaГєtgГЎfu, en ГѕaГ° ber aГ° athuga aГ° ГѕaГ° er ekki Г¶ruggt ГЎ framleiГ°sluГѕjГіnum.");
	define("DB_PERSISTENT_FIELD", "StГ¶Г°ug tenging");
	define("DB_PERSISTENT_DESC", "Til aГ° nota MySQL eГ°a Postgre stГ¶Г°uga tengingu, merktu Г­ Гѕennan hakreit. Ef ГѕГє veist ekki hvaГ° Гѕetta ГѕГЅГ°ir, er lГ­klega best aГ° haka ekki Г­ reitinn.");
	define("DB_CREATE_DB_FIELD", "ГљtbГєa gagnagrunn");
	define("DB_CREATE_DB_DESC", "hakaГ°u hГ©r viljir ГѕГє lГЎta stofna gagnagrunn sjГЎlfvirkt. Virkar aГ°eins ГЎ MySQL og Postgres");
	define("DB_POPULATE_FIELD", "Fylla inn Г­ gagnagrunn");
	define("DB_POPULATE_DESC", "Til aГ° ГєtbГєa tГ¶fluskipan gagnagrunns og fylla meГ° gГ¶gnum, merktu Г­ hakreitinn.");
	define("DB_TEST_DATA_FIELD", "TilraunagГ¶gn");
	define("DB_TEST_DATA_DESC", "til aГ° bГ¦ta tilraunagГ¶gnum viГ° gagnagrunninn, merktu Г­ hakreitinn");
	define("ADMIN_EMAIL_FIELD", "Netfang stjГіrnanda");
	define("ADMIN_LOGIN_FIELD", "Notendanafn stjГіrnanda");
	define("ADMIN_PASS_FIELD", "LykilorГ° stjГіrnanda");
	define("ADMIN_CONF_FIELD", "StaГ°festa lykilorГ°");
	define("DATETIME_SHOWN_FIELD", "SniГ° dags. og tГ­ma (sГ©st ГЎ vef)");
	define("DATE_SHOWN_FIELD", "Dags. sniГ° (sГ©st ГЎ vef)");
	define("DATETIME_EDIT_FIELD", "SniГ° dags. og tГ­ma (Гѕegar ritstГЅrt)");
	define("DATE_EDIT_FIELD", "Dags. sniГ° (Гѕegar ritstГЅrt)");
	define("DATE_FORMAT_COLUMN", "SniГ° dagsetningar");
	define("CURRENT_DATE_COLUMN", "Dagsetning");

	define("DB_LIBRARY_ERROR", "PHP skipanir fyrir {db_library} hafa ekki veriГ° skilgreindar. Vinsamlegast athugaГ°u gagnagrunnsstillingarnar Г­ PHP stillingum ГѕГ­num - php.ini.");
	define("DB_CONNECT_ERROR", "Ekki tГіkst aГ° tengjast gagnagrunni. Vinsamlegast athugaГ°u gagnagrunnsbreyturnar.");
	define("INSTALL_FINISHED_ERROR", "Uppsetningarferli var nГє Гѕegar lokiГ°.");
	define("WRITE_FILE_ERROR", "Hef ekki heimild til aГ° skrifa Г­ skrГЎ <b>'includes/var_definition.php'</b>. Vinsamlegast athugaГ°u skrГЎarrГ©ttindin ГЎГ°ur en ГѕГє heldur ГЎfram");
	define("WRITE_DIR_ERROR", "Hef ekki heimild til aГ° skrifa Г­ mГ¶ppu  <b>'includes/'</b>. Vinsamlegast athugaГ°u mГ¶ppurГ©ttindi ГЎГ°ur en ГѕГє heldur ГЎfram");
	define("DUMP_FILE_ERROR", "Dump skrГЎin '{file_name}' fannst ekki.");
	define("DB_TABLE_ERROR", "Tafla '{file_name}' fannst ekki. Vinsamlegast settu viГ°eigandi gГ¶gn inn Г­ gagnagrunninn.");
	define("TEST_DATA_ERROR", "Athuga skal <b>{POPULATE_DB_FIELD}</b> ГЎГ°ur en tГ¶flur eru fylltar meГ° tilraunagГ¶gnum.");
	define("DB_HOST_ERROR", "HГЅsisnafniГ° sem ГѕГє tilgreindir fannst ekki.");
	define("DB_PORT_ERROR", "Ekki tГіkst aГ° tengjast gagnagrunni meГ° tilgreindu tengi");
	define("DB_USER_PASS_ERROR", "NotendanafniГ° eГ°a lykilorГ°iГ° sem ГѕГє slГіst inn er ekki rГ©tt");
	define("DB_NAME_ERROR", "SkrГЎningarstillingarnar voru rГ©ttar, en gagnagrunnurinn '{db_name}' fannst ekki.");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt Verslun uppfГ¦rsla");
	define("UPGRADE_NOTE", "GГ¦ttu Гѕess aГ° ГєtbГєa afrit af gagnagrunni ГЎГ°ur en lengra er haldiГ°.");
	define("UPGRADE_AVAILABLE_MSG", "GagnagrunnsuppfГ¦rsla fГЎanleg");
	define("UPGRADE_BUTTON", "UppfГ¦ra gagnagrunn Г­ {version_number} nГєna");
	define("CURRENT_VERSION_MSG", "SГє ГєtgГЎfa sem uppsett er nГєna");
	define("LATEST_VERSION_MSG", "ГљtgГЎfa fГЎanleg til uppsetningar");
	define("UPGRADE_RESULTS_MSG", "NiГ°urstГ¶Г°ur uppfГ¦rslu");
	define("SQL_SUCCESS_MSG", "SQL leitir/fyrirspurnir tГіkust");
	define("SQL_FAILED_MSG", "SQL leitir/fyrirspurnir mistГіkust");
	define("SQL_TOTAL_MSG", "Samtals SQL leitir/fyrirspurnir framkvГ¦mdar");
	define("VERSION_UPGRADED_MSG", "Gagnagrunnur Гѕinn hefur veriГ° uppfГ¦rГ°ur Г­");
	define("ALREADY_LATEST_MSG", "ГћГє hefur nГє Гѕegar nГЅjustu ГєtgГЎfu");
	define("DOWNLOAD_NEW_MSG", "NГЅja ГєtgГЎfan fannst");
	define("DOWNLOAD_NOW_MSG", "NiГ°urhala ГєtgГЎfa {version_number} nГє");
	define("DOWNLOAD_FOUND_MSG", "ГљtgГЎfa {version_number} er nГє fГЎanleg til niГ°urhals. Smelltu ГЎ slГіГ°ina hГ©r aГ° neГ°an til aГ° hefja niГ°urhal. Eftir aГ° niГ°urhali er lokiГ° og nГЅjum skrГЎm komiГ° fyrir, ekki gleyma aГ° keyra uppfГ¦rlsuferliГ° aftur.");
	define("NO_XML_CONNECTION", "ViГ°vГ¶run! Engin tenging viГ° 'http://www.viart.com/' til staГ°ar!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>