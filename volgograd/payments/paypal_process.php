<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1                                                  ***
  ***      File:  paypal_process.php                                       ***
  ***      Built: Thu Sep  6 17:18:08 2012                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


/*
 * PayPal Standard (www.paypal.com) transaction handler by http://www.viart.com/
 */

	$is_admin_path = true;
	$root_folder_path = "../";

	include_once ($root_folder_path ."includes/common.php");
	include_once ($root_folder_path ."includes/order_items.php");
	include_once ($root_folder_path ."messages/".$language_code."/cart_messages.php");
	include_once ($root_folder_path ."includes/parameters.php");

	$vc = get_session("session_vc");
	$order_id = get_session("session_order_id");

	$order_errors = check_order($order_id, $vc);
	if($order_errors) {
		echo $order_errors;
		exit;
	}
	
	$post_parameters = ""; $payment_parameters = array(); $pass_parameters = array(); $pass_data = array(); $variables = array();
	get_payment_parameters($order_id, $payment_parameters, $pass_parameters, $post_parameters, $pass_data, $variables, "");

	$pass_data = array();
	foreach ($payment_parameters as $parameter_name => $parameter_value) {
		if (isset($pass_parameters[$parameter_name]) && $pass_parameters[$parameter_name] == 1) {
			$pass_data[$parameter_name] = $parameter_value;
			if(isset($pass_data[strtolower($parameter_name)])){
				unset($pass_data[strtolower($parameter_name)]);
				$pass_data[$parameter_name] = $parameter_value;
			}
		}
	}

	$pass_data["upload"] = "1";
	$i_count = 0;
	$pp_tax = 0;
	foreach ($variables['items'] as $items_index => $items_array) {
		$i_count ++;
		$pp_itemamount = $items_array["price_excl_tax"];
		$pp_tax += ($items_array["price_incl_tax"] - $items_array["price_excl_tax"])*$items_array["quantity"];
		$pass_data["item_name_".$i_count] = $items_array["item_name"]." ".$items_array["properties_text"];
		$pass_data["item_number_".$i_count] = $items_array["item_code"];
		$pass_data["quantity_".$i_count] = $items_array["quantity"];
		$pass_data["amount_".$i_count] = $pp_itemamount;
	}
	foreach ($variables["properties"] as $number => $property) {
		$i_count ++;
		$pp_itemamount = $property["property_price_excl_tax"];
		$pp_tax += $property["property_price_incl_tax"] - $property["property_price_excl_tax"];
		$pass_data["item_name_".$i_count] = $property['property_name'];
		$pass_data["item_number_".$i_count] = "";
		$pass_data["quantity_".$i_count] = 1;
		$pass_data["amount_".$i_count] = $pp_itemamount;
	}
	if ($variables["shipping_type_desc"]) {
		$i_count ++;
		$pp_itemamount = $variables["shipping_cost_excl_tax"];
		$pp_tax += $variables["shipping_cost_incl_tax"] - $variables["shipping_cost_excl_tax"];
		$pass_data["item_name_".$i_count] = $variables["shipping_type_desc"];
		$pass_data["item_number_".$i_count] = "";
		$pass_data["quantity_".$i_count] = 1;
		$pass_data["amount_".$i_count] = $pp_itemamount;
//		$pass_data["handling_cart"] = $variables["shipping_cost_incl_tax"];
	}
	if ($variables["processing_fee"] != 0) {
		$i_count ++;
		$pp_itemamount = $variables["processing_fee"];
		$pass_data["item_name_".$i_count] = PROCESSING_FEE_MSG;
		$pass_data["item_number_".$i_count] = "";
		$pass_data["quantity_".$i_count] = 1;
		$pass_data["amount_".$i_count] = $pp_itemamount;
	}
	if ($variables["total_discount"] != 0) {
		$pp_discount_amount = $variables["total_discount_excl_tax"];
		$pass_data["discount_amount_cart"] = $pp_discount_amount;
	}
	if ($variables["tax_cost"]) {
		$pass_data["tax_cart"] = $variables["tax_cost"];
	}



	$t = new VA_Template('.'.$settings["templates_dir"]);
	$t->set_file("main","payment.html");
	$payment_name = 'PayPal Website Payments Standard';
	$goto_payment_message = str_replace("{payment_system}", $payment_name, GOTO_PAYMENT_MSG);
	$goto_payment_message = str_replace("{button_name}", CONTINUE_BUTTON, $goto_payment_message);
	$t->set_var("GOTO_PAYMENT_MSG", $goto_payment_message);
	$t->set_var("payment_url",$payment_parameters['payment_url']);
	$t->set_var("submit_method", "post");
	foreach ($pass_data as $parameter_name => $parameter_value) {
		$t->set_var("parameter_name", $parameter_name);
		$t->set_var("parameter_value", $parameter_value);
		$t->parse("parameters", true);
	}
	$t->sparse("submit_payment", false);
	$t->pparse("main");
		
	exit;
?>