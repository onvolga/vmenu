<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//hasznР±lati utasРЅtР±sok СЊzenetei
	define("MANUALS_TITLE", "HasznР±lati utasРЅtР±sok");
	define("NO_MANUALS_MSG", "HasznР±lati utasРЅtР±s nem talР±lhatСѓ.");
	define("NO_MANUAL_ARTICLES_MSG", "Nincsenek cikkek");
	define("MANUALS_PREV_ARTICLE", "ElС…zС…");
	define("MANUALS_NEXT_ARTICLE", "KС†vetkezС…");
	define("MANUAL_CONTENT_MSG", "Index");
	define("MANUALS_SEARCH_IN_MSG", "KeresР№s itt");
	define("MANUALS_SEARCH_FOR_MSG", "KeresР№s");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Р¦sszes HasznР±lati utasРЅtР±s");
	define("MANUALS_SEARCH_RESULTS_INFO", "{results_number} cikk talР±lhatСѓ {search_string} keresС…-kifejezР№sre.");
	define("MANUALS_SEARCH_RESULT_MSG", "KeresР№s eredmР№nye");
	define("MANUALS_NOT_FOUND_ANYTHING", "Nem talР±lhatСѓ semmi a  '{search_string}' keresС…-kifejezР№sre");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Nincs tartalom");
	define("MANUALS_SEARCH_TITLE", "HasznР±lati utasРЅtР±sok KeresР№se");
	define("MANUALS_SEARCH_RESULTS_TITLE", "HasznР±lati utasРЅtР±sok KeresР№sР№nek eredmР№nye");

?>