<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// повідомлення відгуків
	define("OPTIONAL_MSG", "Не обов'язково");
	define("BAD_MSG", "Погано");
	define("POOR_MSG", "Бiдно");
	define("AVERAGE_MSG", "Середньо");
	define("GOOD_MSG", "Добре");
	define("EXCELLENT_MSG", "Блискуче");
	define("REVIEWS_MSG", "Вiдгуки");
	define("NO_REVIEWS_MSG", "Немає вiдгукiв");
	define("WRITE_REVIEW_MSG", "Напишiть вiдгук");
	define("RATE_PRODUCT_MSG", "Залишiть свою оцiнку продукту");
	define("RATE_ARTICLE_MSG", "Оцініть данну статтю");
	define("NOT_RATED_PRODUCT_MSG", "По цьому продукту ще немає вiдгукiв.");
	define("NOT_RATED_ARTICLE_MSG", "По цій статті ще немає відгуків");
	define("AVERAGE_RATING_MSG", "Середнiй рейтинг користувачiв");
	define("BASED_ON_REVIEWS_MSG", "базується на {total_votes} голосах");
	define("POSITIVE_REVIEW_MSG", "Позитивиний вiдгук");
	define("NEGATIVE_REVIEW_MSG", "Негативний вiдгук");
	define("SEE_ALL_REVIEWS_MSG", "Переглянути всi вiдгуки...");
	define("ALL_REVIEWS_MSG", "Всi вiдгуки");
	define("ONLY_POSITIVE_MSG", "Тiльки позитивнi");
	define("ONLY_NEGATIVE_MSG", "Тiльки негативнi");
	define("POSITIVE_REVIEWS_MSG", "Позитивнi вiдгуки");
	define("NEGATIVE_REVIEWS_MSG", "Негативнi вiдгуки");
	define("SUBMIT_REVIEW_MSG", "Дякуємо<br>Вашi коментарi будуть переглянутi i, у разi погодження, з'являться на нашому сайтi.<br>Ми залишаємо за собою право вiдхилити будь-якi нотатки, якi не вiдповiдають нашим нормативам.");
	define("ALREADY_REVIEW_MSG", "Ви вже розмiстили вiдгук.");
	define("RECOMMEND_PRODUCT_MSG", "Чи рекомендуєте ви<br> цей продукт iншим?");
	define("RECOMMEND_ARTICLE_MSG", "Чи порекомендуєте ви <br> цю статтю іншим?");
	define("RATE_IT_MSG", "Загальна оцiнка");
	define("NAME_ALIAS_MSG", "Iм'я або псевдонiм");
	define("SHOW_MSG", "Показати");
	define("FOUND_MSG", "Знайдено");
	define("RATING_MSG", "Рейтинг");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>