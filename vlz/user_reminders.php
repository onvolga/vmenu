<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_reminders.php                                       ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./messages/" . $language_code . "/download_messages.php");

	check_user_session();

	$cms_page_code = "user_reminders";
	$script_name   = "user_reminders.php";
	$current_page  = get_custom_friendly_url("user_reminders.php");
	$auto_meta_title = MY_REMINDERS_MSG.": ".LIST_MSG;

	include_once("./includes/page_layout.php");

?>
