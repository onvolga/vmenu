<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//tР±mogatР±s СЊzenetek
	define("SUPPORT_TITLE", "TermР№ktР±mogatР±si KС†zpont");
	define("SUPPORT_REQUEST_INF_TITLE", "KР№rР№s InformР±ciСѓ");
	define("SUPPORT_REPLY_FORM_TITLE", "VР±laszolnak");

	define("MY_SUPPORT_ISSUES_MSG", "TР±mogatР±si kР№rР№seim");
	define("MY_SUPPORT_ISSUES_DESC", "Ha tapasztalsz bР±rmilyen problР№mР±t a termР№kkel amit megvР±sР±roltР±l, a TР±mogatР±si Csapatunk kР№szen Р±ll segРЅteni. A linkre kattintР±ssal fel lehet venni a kapcsolatot velСЊnk, Р№s kСЊldhetsz tР±mogatР±si kР№rР№st.");
	define("NEW_SUPPORT_REQUEST_MSG", "РЄj KР№rР№s");
	define("SUPPORT_REQUEST_ADDED_MSG", "KС†szС†njСЊk<br>A tР±mogatР±si csapatunk megprСѓbР±l segРЅtsР№get nyСЉjtani , amilyen gyorsan lehetsР№ges.");
	define("SUPPORT_SELECT_DEP_MSG", "KivР±lasztР±s osztР±ly");
	define("SUPPORT_SELECT_PROD_MSG", "KivР±lasztР±s termР№k");
	define("SUPPORT_SELECT_STATUS_MSG", "KivР±lasztР±s stР±tusz");
	define("SUPPORT_NOT_VIEWED_MSG", "Nem nР№zett");
	define("SUPPORT_VIEWED_BY_USER_MSG", "NР№zett СЊgyfР№l Р±ltal");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "NР№zett СЊgyintР№zС… Р±ltal");
	define("SUPPORT_STATUS_NEW_MSG", "РЄj");
	define("NO_SUPPORT_REQUEST_MSG", "Nincs kР№rdР№s");

	define("SUPPORT_SUMMARY_COLUMN", "Р¦sszefoglalСѓ");
	define("SUPPORT_TYPE_COLUMN", "Tipus");
	define("SUPPORT_UPDATED_COLUMN", "LegutСѓbb frissРЅtett");

	define("SUPPORT_USER_NAME_FIELD", "Neved");
	define("SUPPORT_USER_EMAIL_FIELD", "Email CРЅmed");
	define("SUPPORT_IDENTIFIER_FIELD", "AzonosРЅtСѓ (SzР±mla #)");
	define("SUPPORT_ENVIRONMENT_FIELD", "KС†rnyezet (OS, AdatbР±zis, Web szerver, stb.)");
	define("SUPPORT_DEPARTMENT_FIELD", "OsztР±ly");
	define("SUPPORT_PRODUCT_FIELD", "TermР№k");
	define("SUPPORT_TYPE_FIELD", "Tipus");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Jelenlegi Helyzet");
	define("SUPPORT_SUMMARY_FIELD", "Egysoros С†sszefoglalСѓ");
	define("SUPPORT_DESCRIPTION_FIELD", "LeРЅrР±s");
	define("SUPPORT_MESSAGE_FIELD", "Р¬zenet");
	define("SUPPORT_ADDED_FIELD", "HozzР±adott");
	define("SUPPORT_ADDED_BY_FIELD", "HozzР±adta:");
	define("SUPPORT_UPDATED_FIELD", "LegutСѓbb frissРЅtett");

	define("SUPPORT_REQUEST_BUTTON", "KР№rР№s tovР±bbРЅtР±sa");
	define("SUPPORT_REPLY_BUTTON", "VР±lasz");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "HiР±nyzСѓ <b>Support ID</b> paramР№ter");
	define("SUPPORT_MISS_CODE_ERROR", "HiР±nyzСѓ <b>Verification</b> paramР№ter");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Support ID</b> paramР№ter az Р№rtР№ke rossz.");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verification</b> paramР№ter az Р№rtР№ke rossz.");

	define("MAIL_DATA_MSG", "Adatok kСЊldР№se");
	define("HEADERS_MSG", "FejlР№cek");
	define("ORIGINAL_TEXT_MSG", "Eredeti szС†veg");
	define("ORIGINAL_HTML_MSG", "Eredeti HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "SajnР±ljuk, de szР±modra nem engedР№lyezett a karton lezР±rР±sa.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "SajnР±ljuk, de szР±modra nem engedР№lyezett a vР±lasz kСЊldР№s a kartonokra.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "SajnР±ljuk, de szР±modra nem engedР№lyezett az СЉj karton lР№trehozР±sa.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "SajnР±ljuk, de szР±modra nem engedР№lyezett a kartonok tС†rlР№se.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "SajnР±ljuk, de szР±modra nem engedР№lyezett a kartonok frissРЅtР№se.<br>");
	define("NO_TICKETS_FOUND_MSG", "Nem talР±ltunk kartonokat.");
	define("HIDDEN_TICKETS_MSG", "Rejtett Kartonok");
	define("ALL_TICKETS_MSG", "Р¦sszes Karton");
	define("ACTIVE_TICKETS_MSG", "AktРЅv Kartonok");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "Nem vagy az OsztР±ly tagja.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "Nem vagy egyik OsztР±ly tagja sem.");
	define("REPLY_TO_NAME_MSG", "VР±lasz kСЊldР№s neki: {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "TudР±sbР±zis KaregСѓria");
	define("KNOWLEDGE_TITLE_MSG", "TudР±sbР±zis CРЅm");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "TudР±sbР±zis Cikk stР±tusz");
	define("SELECT_RESPONSIBLE_MSG", "VР±lassz IlletР№kest");

?>