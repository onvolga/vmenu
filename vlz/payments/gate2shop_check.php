<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.8                                                ***
  ***      File:  gate2shop_check.php                                      ***
  ***      Built: Tue Sep 13 18:30:31 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	$par = array("nameOnCard", "cardNumber", "cvv2", "expMonth", "expYear", "first_name", "last_name", "address1", "address2", "city", 
	"country", "email", "state", "zip", "phone1", "phone2", "phone3", "currency", "customField1", "customField2", "customField3", 
	"customField4", "customField5", "merchant_unique_id", "merchant_site_id", "merchant_id", "requestVersion", "PPP_TransactionID", 
	"productId", "userid", "message", "Error", "Status", "ClientUniqueID", "ExErrCode", "ErrCode", "AuthCode", "Reason", "ReasonCode", 
	"Token", "responsechecksum", "totalAmount", "TransactionID", "ppp_status", "invoice_id", "payment_method", "unknownParameters", 
	"merchantLocale", "customData", "return_action");
	
	for ($i=0;$i<count($par);$i++){
		$answer[$par[$i]] = get_param($par[$i]);
	}
	
	$error_message = "";
	$post_parameters = ""; 
	$payment_params = array(); 
	$pass_parameters = array(); 
	$pass_data = array(); 
	$variables = array();
	get_payment_parameters($order_id, $payment_params, $pass_parameters, $post_parameters, $pass_data, $variables, "");
	
	$transaction_id = $answer["TransactionID"];

	$checksum = $answer["TransactionID"].$answer["ErrCode"].$answer["ExErrCode"].$answer["Status"];
	if(!isset($payment_parameters["secret"])){
		$error_message = "Need Secret Code.";
		return;
	}else{
		$secret = $payment_parameters["secret"];
		$checksum = $secret.$checksum;
	}
	
	//echo "<!-- ".$answer["Status"]." -->";
	
	$checksum = md5($checksum);
	
	if (strtolower($answer["return_action"]) == 'cancel') {
		$error_message = "Your transaction has been cancelled.";
		return;
	}
	if (!strlen($answer["responsechecksum"]) || $checksum != $answer["responsechecksum"]){
		$error_message = "Checksum don't consist with response.";
		return;
	}
	if (strval($answer["ErrCode"])=='0' && strval($answer["ExErrCode"])=='-2'){
		$pending_message = "Your order will be reviewed manually.";
		return;
	}
	if (strval($answer["ErrCode"])!='0' || strval($answer["ExErrCode"])!='0'){
		if(strlen($answer["Error"])){
			$error_message = "ErrCode: ".$answer["ErrCode"].", ExErrCode: ".$answer["ExErrCode"].", ".$answer["Error"];
			return;
		}else{
			$error_message = "ErrCode: ".$answer["ErrCode"].", ExErrCode: ".$answer["ExErrCode"].", "."Your transaction has been declined.";
			return;
		}
	}
	if (!strlen($answer["TransactionID"])) {
		$error_message = "Can't obtain Transaction ID parameter.";
		return;
	}

?>