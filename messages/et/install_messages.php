<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "ViArt SHOP installeerimine");

	define("INSTALL_STEP_1_TITLE", "Installeerimine: Samm 1");
	define("INSTALL_STEP_1_DESC", "TРґname, et valisid ViArt SHOP'i. Installeerimise jРґtkamiseks, tРґida palun allolevad vajalikud andmed. Pane tРґhele, et valiksid juba olemasoleva andmebaasi. Kui installeerid andmebaasi, mis kasutab ODBC'd, nРґiteks MC Access, peaksid enne jРґtkamist looma sellele DSN'i.");
	define("INSTALL_STEP_2_TITLE", "Installeerimine: Samm 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Installeerimine: Samm 3");
	define("INSTALL_STEP_3_DESC", "Palun vali veebilehe paigutuse. Sa vС…id pРґrast paigutust muuta.");
	define("INSTALL_FINAL_TITLE", "Installeerimine: LС…pp");
	define("SELECT_DATE_TITLE", "Vali kuupРґeva formaat");

	define("DB_SETTINGS_MSG", "Andmebaasi seaded");
	define("DB_PROGRESS_MSG", "Andmebaasi struktuuri loomise protsess");
	define("SELECT_PHP_LIB_MSG", "Vali PHP Library ");
	define("SELECT_DB_TYPE_MSG", "Vali andmebaasi tСЊСЊp");
	define("ADMIN_SETTINGS_MSG", "Administratiivsed seaded");
	define("DATE_SETTINGS_MSG", "KuupРґeva formaadid");
	define("NO_DATE_FORMATS_MSG", "KuupРґeva formaadid ei ole saadaval");
	define("INSTALL_FINISHED_MSG", "NСЊСЊdseks on lС…ppenud sinu pС…hiinstalleerimine. Palun kontrolli administreerimise sektsiooni seadeid ning tee vajalikud muudatused.");
	define("ACCESS_ADMIN_MSG", "Administreerimise sektsiooni sisenemiseks kliki siia");
	define("ADMIN_URL_MSG", "Administratsiooni URL");
	define("MANUAL_URL_MSG", "Manual URL");
	define("THANKS_MSG", "TРґname, et valisid <b>ViArt SHOP</b>");

	define("DB_TYPE_FIELD", "Andmebaasi tСЊСЊp");
	define("DB_TYPE_DESC", "Palun vali <b>andmebaasi tСЊСЊp</b>, mida sa kasutad. Kui kasutad SQL serverit vС…i Microsoft Access'i, vali palun ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP Library ");
	define("DB_HOST_FIELD", "Host'i nimi");
	define("DB_HOST_DESC", "Palun sisesta <b>nimi</b> vС…i <b>serveri IP aadress</b>, millel hakkab tС†С†tama sinu ViArt andmebaas. Kui sinu andmebaas tС†С†tab sinu lokaalsel personaalarvutil, siis vС…id tС…enРґoliselt selle jРґtta kui \"<b>localhost</b>\" ja port tСЊhjaks. Kui kasutad andmebaasi, mida pakub sulle hosting firma, vaata palun selle dokumentatsioonist serveri seadeid.");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "Andmebaasi nimi / DSN");
	define("DB_NAME_DESC", "Kui kasutad MySQL vС…i PostgreSQL sarnast andmebaasi, siis sisesta palun <b>andmebaasi nimi</b> , kuhu sa tahad, et ViArt loob oma tabelid. See andmebaas peab juba eksisteerima. Kui installeerid ViArt'i oma lokaalsesse personaalarvutisse testimise eesmРґrgil, siis enamikel sСЊsteemidel on \"<b>test</b>\" andmebaas, mida saad kasutada. Kui mitte, loo andmebaas  viart  ja kasuta seda. Kui kasutad Microsoft Access'i vС…i SQL serverit, siis andmebaasi nimi peaks olema <b>DSN'i nimi</b>, mille oled seadnud Data Sources (ODBC)sektsioonis oma arvuti Control Panel'is.");
	define("DB_USER_FIELD", "Kasutajanimi");
	define("DB_PASS_FIELD", "Parool");
	define("DB_USER_PASS_DESC", "<b>Kasutajanimi</b> ja <b>parool</b> - palun sisesta kasutajanimi ja parool, mida tahad kasutada andmebaasi sisenemiseks. Kui kasutad lokaalset test installeerimist, on kasutajanimi tС…enРґoliselt \"<b>root</b>\" ja parool tСЊhi. See sobib testimiseks, kuid pane tРґhele, et see ei ole ohutu tootmise serverites.");
	define("DB_PERSISTENT_FIELD", "PСЊsiСЊhendus");
	define("DB_PERSISTENT_DESC", "Et kasutada MySQL vС…i Postgre pСЊsiСЊhendusi, tee mРґrge sellesse ruutu. Kui sa ei tea, mida see tРґhendab, siis parem oleks jРґtta see ruut tСЊhjaks.");
	define("DB_CREATE_DB_FIELD", "Loo andmebaas");
	define("DB_CREATE_DB_DESC", "Andmebaasi loomiseks mРґrgi vС…imalusel Рґra see ruut. TС†С†tab ainult MySQL ja Postgre puhul.");
	define("DB_POPULATE_FIELD", "Asusta andmebaas");
	define("DB_POPULATE_DESC", "Andmebaasi tabeli struktuuri loomiseks ja selle asustamiseks andmetega tee mРґrkeruutu mРґrge");
	define("DB_TEST_DATA_FIELD", "Testandmed");
	define("DB_TEST_DATA_DESC", "Testandmete lisamiseks oma andmebaasi tee mРґrkeruutu mРґrge");
	define("ADMIN_EMAIL_FIELD", "Administraatori e-post");
	define("ADMIN_LOGIN_FIELD", "Administraatori kasutajanimi");
	define("ADMIN_PASS_FIELD", "Administraatori parool");
	define("ADMIN_CONF_FIELD", "Kinnita parool");
	define("DATETIME_SHOWN_FIELD", "KuupРґeva ja aja formaat (nРґidatud veebilehel)");
	define("DATE_SHOWN_FIELD", "KuupРґeva formaat (nРґidatud veebilehel)");
	define("DATETIME_EDIT_FIELD", "KuupРґeva ja aja formaat (muutmiseks)");
	define("DATE_EDIT_FIELD", "KuupРґea formaat (muutmiseks)");
	define("DATE_FORMAT_COLUMN", "KuupРґeva formaat");
	define("CURRENT_DATE_COLUMN", "TРґnane kuupРґev");

	define("DB_LIBRARY_ERROR", "{db_library} PHP funktsioonid ei ole mРґРґratletud. Palun kontrolli oma andmebaasi seadeid konfiguratsiooni failis   php.ini.");
	define("DB_CONNECT_ERROR", "Ei saa СЊhendada andmebaasiga. Palun kontrolli oma andmebaasi parameetreid.");
	define("INSTALL_FINISHED_ERROR", "Installeerimise protsess juba lС…ppenud.");
	define("WRITE_FILE_ERROR", "Ei ole kirjalikku luba failile <b>'includes/var_definition.php'</b>. Palun muuda faili luba enne jРґtkamist.");
	define("WRITE_DIR_ERROR", "Ei ole kirjalikku luba kaustale <b>'includes/'</b>. Palun muuda kausta luba enne jРґtkamist.");
	define("DUMP_FILE_ERROR", "Dump faili '{file_name}' ei leitud.");
	define("DB_TABLE_ERROR", "Tabelit '{table_name}' ei leitud. Palun sisesta andmebaasi vajalikud andmed.");
	define("TEST_DATA_ERROR", "Kontrolli <b>{POPULATE_DB_FIELD}</b> enne testandmete sisestamist andmebaasi");
	define("DB_HOST_ERROR", "Ei leitud sinu tРґpsustatud host'i nime.");
	define("DB_PORT_ERROR", "Ei saa СЊhendada andmebaasi serveriga kasutades antud porti.");
	define("DB_USER_PASS_ERROR", "Antud kasutajanimi vС…i parool ei ole С…ige.");
	define("DB_NAME_ERROR", "Login seaded olid С…iged, kuid andmebaasi '{db_name}'  ei leitud.");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt SHOP'i uuendamine");
	define("UPGRADE_NOTE", "Teade: Palun kaalu andmebaasi varukoopia tegemise vС…imalust enne jРґtkamist.");
	define("UPGRADE_AVAILABLE_MSG", "Andmebaasi uuendus on saadaval");
	define("UPGRADE_BUTTON", "Uuenda andmebaas {version_number} versiooniks nСЊСЊd");
	define("CURRENT_VERSION_MSG", "Preagu installeeritud versioon");
	define("LATEST_VERSION_MSG", "Versioon installeerimiseks saadaval");
	define("UPGRADE_RESULTS_MSG", "Uuendamise tulemused");
	define("SQL_SUCCESS_MSG", "SQL pРґringud С…nnestusid");
	define("SQL_FAILED_MSG", "SQL pРґringud ebaС…nnestusid");
	define("SQL_TOTAL_MSG", "KС…ik SQL pРґringud lС…petatud");
	define("VERSION_UPGRADED_MSG", "Sinu andmebaas on uuendatud");
	define("ALREADY_LATEST_MSG", "Sul juba on uusim versioon");
	define("DOWNLOAD_NEW_MSG", "Avastatud on uus versioon");
	define("DOWNLOAD_NOW_MSG", "Lae alla  {version_number} versioon nСЊСЊd");
	define("DOWNLOAD_FOUND_MSG", "Oleme avastanud, et uus {version_number} versioon on saadaval allalaadimiseks. Palun kliki allolevale lingile, et alustada allalaadimist. PРґrast allalaadimise lС…ppemist ning failide asendamist Рґra unusta lРґbi teha uuendamist.");
	define("NO_XML_CONNECTION", "Hoiatus! Р¬hendus 'http://www.viart.com/' ei ole saadaval!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>