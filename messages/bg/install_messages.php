<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//съобщения свързани с инсталацията
	define("INSTALL_TITLE", "Инсталация на ViArt магазин");

	define("INSTALL_STEP_1_TITLE", "Инсталация: Стъпка 1");
	define("INSTALL_STEP_1_DESC", "Благодарим ви, че избрахте ViArt магазина. За да продължите с инсталацията, моля попълнете поисканите по-долу детайли. Обърнете внимание, че избраната от вас база данни вече трябва да съществува. Ако инсталирате база данни, която използва ODBC, като MS Access, трябва първо да създадете DSN за нея, преди да продължите.");
	define("INSTALL_STEP_2_TITLE", "Инсталация: Стъпка 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Инсталация: Стъпка 3");
	define("INSTALL_STEP_3_DESC", "Моля изберете изгледа на сайта. Ще можете да промените изгледа и след това.");
	define("INSTALL_FINAL_TITLE", "Инсталация: Окончателна");
	define("SELECT_DATE_TITLE", "Изберете формат на датата.");

	define("DB_SETTINGS_MSG", "Настройки на базата данни.");
	define("DB_PROGRESS_MSG", "Процес на запълване структурата на базата данни.");
	define("SELECT_PHP_LIB_MSG", "Избор на PHP библиотека.");
	define("SELECT_DB_TYPE_MSG", "Избор на тип база данни.");
	define("ADMIN_SETTINGS_MSG", "Административни настройки.");
	define("DATE_SETTINGS_MSG", "Формати на датата.");
	define("NO_DATE_FORMATS_MSG", "Няма налични формати на датата.");
	define("INSTALL_FINISHED_MSG", "В този момент вашата първоначална инсталация е завършена. Моля проверете всички настройки в частта за администрация и направете всички нужни промени.");
	define("ACCESS_ADMIN_MSG", "За да влезете в частта за администрация натиснете тук.");
	define("ADMIN_URL_MSG", "Адрес за администрация");
	define("MANUAL_URL_MSG", "Адрес за наръчник");
	define("THANKS_MSG", "Благодарим ви, че избрахте <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "Тип база данни");
	define("DB_TYPE_DESC", "Моля изберете <b>типа на базата данни</b> която използвате. Ако използвате SQL Server или Microsoft Access, моля изберете ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP библиотека");
	define("DB_HOST_FIELD", "Име на хоста");
	define("DB_HOST_DESC", "Моля въведете <b>име</b> или <b>IP адрес на сървъра</b> на който вашата ViArt база данни ще работи. Ако използвате вашата база данни на вашият компютър, тогава само може да оставите това като \"<b>localhost</b>\" , а порта празен. Ако използвате база данни предоставена ви от хост компанията ви, моля погледнете документите на вашата хост компания, за настройките на сървъра.");
	define("DB_PORT_FIELD", "Порт");
	define("DB_NAME_FIELD", "Име на база данни / DSN");
	define("DB_NAME_DESC", "Ако използвате база данни като MySQL или PostgreSQL тогава моля въведете <b>името на базата данни</b> където бихте искали вашия ViArt да създаде таблиците си. Тази база данни трябва вече да съществува. Ако просто инсталирате ViArt за тестови цели на вашия локален компютър тогава повечето системи имат \"<b>тестова</b>\" база данни, която можете да използвате. Ако не, моля създайте база данни като \"viart\" и използвайте нея. Ако използвате Microsoft Access или SQL Server тогава Име на базата данни трябва да бъде <b>име на DSN</b> която сте настроили в Data Sources (ODBC) частта във вашия Control Panel.");
	define("DB_USER_FIELD", "Потребителско име");
	define("DB_PASS_FIELD", "Парола");
	define("DB_USER_PASS_DESC", "<b>Потребителско име</b> и <b>Парола</b> - моля въведете потребителско име и парола, с които искате да достъпвате базата данни. Ако използвате локална тестова инсталация потребителското име вероятно е \"<b>root</b>\" и паролата вероятно не е попълнена. Това не е проблем в тестов режим но имайте предвид, че не е безопасно в реален режим.");
	define("DB_PERSISTENT_FIELD", "Постоянна връзка");
	define("DB_PERSISTENT_DESC", "За да използвате MySQL или Postgre постоянни връзки, отметнете тази кутийка. Ако не знаете какво означава това, по добре не я отмятайте.");
	define("DB_CREATE_DB_FIELD", "Създаване база данни");
	define("DB_CREATE_DB_DESC", "За създаване на база данни, ако е възможно, отбележете тук. Работи само за MySQL и Postgre.");
	define("DB_POPULATE_FIELD", "Запълване на база данни");
	define("DB_POPULATE_DESC", "За да създадете структурната таблица на базата данни и я запълните с данни и отметнете кутийката.");
	define("DB_TEST_DATA_FIELD", "Тестови данни");
	define("DB_TEST_DATA_DESC", "За да добавите някакви тестови данни в базата данни, отбележете тази кутийка.");
	define("ADMIN_EMAIL_FIELD", "E-mail на администратора");
	define("ADMIN_LOGIN_FIELD", "Потребителско име на администратора");
	define("ADMIN_PASS_FIELD", "Парола на администратора");
	define("ADMIN_CONF_FIELD", "Повторете парола");
	define("DATETIME_SHOWN_FIELD", "Формат на датата и часа (показван на страницата).");
	define("DATE_SHOWN_FIELD", "Формат на датата (показван на страницата).");
	define("DATETIME_EDIT_FIELD", "Формат на датата и часа (за редакция).");
	define("DATE_EDIT_FIELD", "Формат на датата (за редакция).");
	define("DATE_FORMAT_COLUMN", "Формат на датата");
	define("CURRENT_DATE_COLUMN", "Днешна дата");

	define("DB_LIBRARY_ERROR", "PHP функциите за {db_library} не са дефинирани. Моля проверете настройките на базата данни в конфигурационния файл - php.ini.");
	define("DB_CONNECT_ERROR", "Няма връзка с базата данни. Моля проверете параметрите на базата данни.");
	define("INSTALL_FINISHED_ERROR", "Процеса на инсталацията приключи.");
	define("WRITE_FILE_ERROR", "Нямате права за писане във файла <b>'includes/var_definition.php'</b>. Моля променете правата за файла преди да продължите.");
	define("WRITE_DIR_ERROR", "Нямате права за писане в папката <b>'includes/'</b>. Моля променете правата за папката преди да продължите.");
	define("DUMP_FILE_ERROR", "Dump файла '{file_name}' не е намерен.");
	define("DB_TABLE_ERROR", "Таблицата '{table_name}' не беше намерена. Моля запълнете базата данни с необходимите данни.");
	define("TEST_DATA_ERROR", "Отметнете <b>{POPULATE_DB_FIELD}</b> преди запълването на таблиците с данни за теста.");
	define("DB_HOST_ERROR", "Името на хоста, който сте въвели не е намерено.");
	define("DB_PORT_ERROR", "Няма връзка със сървъра за базата данни на посочения порт.");
	define("DB_USER_PASS_ERROR", "Потребителското име или паролата не са правилни.");
	define("DB_NAME_ERROR", "Настройките за включване са правилни, но базата данни '{db_name}' не може да бъде намерена.");

	//съобщения свързани с обновяването
	define("UPGRADE_TITLE", "Осъвременяване на ViArt магазина.");
	define("UPGRADE_NOTE", "Забележка: Моля направете резервно копие на базата данни преди да продължите.");
	define("UPGRADE_AVAILABLE_MSG", "Обновяването на базата данни е възможно.");
	define("UPGRADE_BUTTON", "Надграждане на базата данни до {version_number} сега.");
	define("CURRENT_VERSION_MSG", "Текуща версия");
	define("LATEST_VERSION_MSG", "Версия налична за инсталация.");
	define("UPGRADE_RESULTS_MSG", "Резултати от надграждането.");
	define("SQL_SUCCESS_MSG", "SQL прехвърляне успешно.");
	define("SQL_FAILED_MSG", "SQL прехвърляне неуспешно.");
	define("SQL_TOTAL_MSG", "Общо SQL прехвърляния са извършени.");
	define("VERSION_UPGRADED_MSG", "Вашата база данни бе надградена до");
	define("ALREADY_LATEST_MSG", "Вие вече имате последната версия.");
	define("DOWNLOAD_NEW_MSG", "Открита е нова версия.");
	define("DOWNLOAD_NOW_MSG", "Свалете версията {version_number} сега.");
	define("DOWNLOAD_FOUND_MSG", "Открихме, че нова {version_number} версия е налична за сваляне. Моля натиснете на връзката долу за да започне свалянето. След като изтеглянето приключи и замените файловете не забравяйте да включите отново рутинното обновяване. ");
	define("NO_XML_CONNECTION", "Внимание! Няма връзка с 'http://www.viart.com/' !");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>