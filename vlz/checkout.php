<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  checkout.php                                             ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/navigator.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");

	$cms_page_code = "checkout";
	$script_name   = "checkout.php";
	$current_page  = get_custom_friendly_url("checkout.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = CHECKOUT_LOGIN_TITLE;

	include_once("./includes/page_layout.php");

?>
