<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "ЕгкбфЬуфбуз ViArt SHOP");

	define("INSTALL_STEP_1_TITLE", "ЕгкбфЬуфбуз ВЮмб 1");
	define("INSTALL_STEP_1_DESC", "Убт ехчбсйуфпэме гйб фзн ерйлпгЮ фпх кбфбуфЮмбфпт ViArt SHOP. <br>Рбсбкблпэме ухмрлзсюуфе ьлет фйт лерфпмЭсейет рпх иб убт жзфзипэн.<br>РспуЭофе убн рсюфп ВЮмб нб хрЬсчей Юдз з вЬуз дедпмЭнщн. ЕЬн чсзуймпрпйеЯфе убн вЬуз дедпмЭнщн фзн ODBC, р.ч. MS Access  рсЭрей рсюфб нб дзмйпхсгЮуефе фб  dsn бллйют з ЕгкбфЬуфбуз ден иб рспчщсЮуей.");
	define("INSTALL_STEP_2_TITLE", "ЕгкбфЬуфбуз ВЮмб 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "ЕгкбфЬуфбуз ВЮмб 3");
	define("INSTALL_STEP_3_DESC", "ЕрйлЭофе фзн емцЬнйуз фзт йуфпуелЯдбт убт , бсгьфесб мрпсеЯфе нб бллЬоефе фзн ерйлпгЮ убт.");
	define("INSTALL_FINAL_TITLE", "ФЭлпт егкбфЬуфбузт");
	define("SELECT_DATE_TITLE", "ерйлЭофе рщт иб емцбнЯжефе з ЗмеспмзнЯб");

	define("DB_SETTINGS_MSG", "СхимЯуейт вЬузт дедпмЭнщн");
	define("DB_PROGRESS_MSG", "Populating database structure progress");
	define("SELECT_PHP_LIB_MSG", "ерйлЭофе ВйвлйпиЮкз PHP");
	define("SELECT_DB_TYPE_MSG", "ерйлЭофе фэрп вЬузт дедпмЭнщн");
	define("ADMIN_SETTINGS_MSG", "СхимЯуейт дйбчеЯсйузт");
	define("DATE_SETTINGS_MSG", "СхимЯуейт змеспмзнЯбт");
	define("NO_DATE_FORMATS_MSG", "Ден хрЬсчпхн дйбиЭуймет ерйлпгЭт змеспмзнЯбт");
	define("INSTALL_FINISHED_MSG", "Уе бхфь фп узмеЯп з ЕгкбфЬуфбуз плпклзсюизке , Рбсбкблю вевбйщиеЯфе ьфй ёчефе вЬлей фйт ущуфЭт рбсбмЭфспхт");
	define("ACCESS_ADMIN_MSG", "Гйб рсьувбуз уфп кпнфсьл рЬнел кЬнфе клйк ЕДї");
	define("ADMIN_URL_MSG", "URL ДйбчейсйуфЮ");
	define("MANUAL_URL_MSG", "Manual URL");
	define("THANKS_MSG", "ехчбсйуфпэме рпх ерйлЭобфе фп <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "Фэрпт вЬузт дедпмЭнщн");
	define("DB_TYPE_DESC", "Please select the <b>type of database</b> that you are using. If you using SQL Server or Microsoft Access, please select ODBC.");
	define("DB_PHP_LIB_FIELD", "ВйвлйпиЮкз PHP");
	define("DB_HOST_FIELD", "Hostname");
	define("DB_HOST_DESC", "Please enter the <b>name</b> or <b>IP address of the server</b> on which your ViArt database will run. If you are running your database on your local PC then you can probably just leave this as \"<b>localhost</b>\" and the port blank. If you using a database provided by your hosting company, please see your hosting company's documentation for the server settings.");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "јнпмб вЬузт дедпмЭнщн / DSN");
	define("DB_NAME_DESC", "If you are using a database such as MySQL or PostgreSQL then please enter the <b>name of the database</b> where you would like ViArt to create its tables. This database must exist already. If you are just installing ViArt for testing purposes on your local PC then most systems have a \"<b>test</b>\" database you can use. If not, please create a database such as \"viart\" and use that. If you are using Microsoft Access or SQL Server then the Database Name should be the <b>name of the DSN</b> that you have set up in the Data Sources (ODBC) section of your Control Panel.");
	define("DB_USER_FIELD", "јнпмб чсЮуфз");
	define("DB_PASS_FIELD", "Кщдйкьт");
	define("DB_USER_PASS_DESC", "<b>Username</b> and <b>Password</b> - please enter the username and password you want to use to access the database. If you are using a local test installation the username is probably \"<b>root</b>\" and the password is probably blank. This is fine for testing, but please note that this is not secure on production servers.");
	define("DB_PERSISTENT_FIELD", "Persistent Connection");
	define("DB_PERSISTENT_DESC", "to use MySQL or Postgre persistent connections, tick this box. If you do not know what it means, then leaving it unticked is probably best.");
	define("DB_CREATE_DB_FIELD", "Create DB");
	define("DB_CREATE_DB_DESC", "to create database if possible, tick this box. Works only for MySQL and Postgre");
	define("DB_POPULATE_FIELD", "ГемЯуфе фзн вЬуз дедпмЭнщн");
	define("DB_POPULATE_DESC", "ФуекЬсефе едю гйб дзмйпхсгЮуефе фзн дпмЮ фзт вЬузт кбй нб ухмрлзсюуефе фб дедпмЭнб фзт");
	define("DB_TEST_DATA_FIELD", "Test Data");
	define("DB_TEST_DATA_DESC", "to add some test data to your database tick the checkbox");
	define("ADMIN_EMAIL_FIELD", "E-mail ДйбчейсйуфЮ");
	define("ADMIN_LOGIN_FIELD", "ЕЯупдпт ДйбчейсйуфЮ");
	define("ADMIN_PASS_FIELD", "Кщдйкьт ДйбчейсйуфЮ");
	define("ADMIN_CONF_FIELD", "ОбнбгсЬшфе фпн кщдйкь");
	define("DATETIME_SHOWN_FIELD", "ЕмцЬнйуз змеспмзнЯбт кбй юсбт(Уфзн йуфпуелЯдб)");
	define("DATE_SHOWN_FIELD", "ЕмцЬнйуз змеспмзнЯбт (уфзн йуфпуелЯдб)");
	define("DATETIME_EDIT_FIELD", "ЗмеспмзнЯб кбй юсб (гйб ереоесгбуЯб)");
	define("DATE_EDIT_FIELD", "ЗмеспмзнЯб (гйб ереоесгбуЯб)");
	define("DATE_FORMAT_COLUMN", "ЕмцЬнйуз змеспмзнЯбт");
	define("CURRENT_DATE_COLUMN", "ФсЭчпхуб ЗмеспмзнЯб");

	define("DB_LIBRARY_ERROR", "PHP functions for {db_library} are not defined. Please check your database settings in your configuration file - php.ini.");
	define("DB_CONNECT_ERROR", "Ден мрпсю нб ерйкпйнщнЮущ ме фзн вЬуз дедпмЭнщн , Рбсбкблю елофе фйт рбсбмЭфспхт");
	define("INSTALL_FINISHED_ERROR", "З дйбдйкбуЯб егкбфЬуфбузт ёчей Юдз фелейюуей");
	define("WRITE_FILE_ERROR", "Ден ёчефе Ьдейб нб бллЬоефе бхфь фп бсчеЯп <b>'includes/var_definition.php'</b>.Рбсбкблю бллЬофе фйт йдйьфзфет фпх бсчеЯпх рсйн нб ухнечЯуефе");
	define("WRITE_DIR_ERROR", "Ден ёчефе Ьдейб нб бллЬоефе бхфьн фпн цЬкелп <b>'includes/'</b>.Рбсбкблю бллЬофе фйт йдйьфзфет фпх цбкЭлпх рсйн нб ухнечЯуефе");
	define("DUMP_FILE_ERROR", "Фп бсчеЯп '{file_name}' ден всЭизке !");
	define("DB_TABLE_ERROR", "П рЯнбкбт '{table_name} ден всЭизке ! Рбсбкблю рспуиЭуфе уфзн вЬуз дедпмЭнщн фб брбсбЯфзфб уфпйчеЯб.");
	define("TEST_DATA_ERROR", "Check <b>{POPULATE_DB_FIELD}</b> before populating tables with test data");
	define("DB_HOST_ERROR", "The hostname that you specified could not be found.");
	define("DB_PORT_ERROR", "Can't connect to database server using specified port.");
	define("DB_USER_PASS_ERROR", "The username or password you specified is not correct.");
	define("DB_NAME_ERROR", "Login settings were correct, but the database '{db_name}' could not be found.");

	// upgrade messages
	define("UPGRADE_TITLE", "ViArt SHOP бнбвЬимйуз");
	define("UPGRADE_NOTE", "УзмеЯщуз : Брпизкеэуфе фзн вЬуз дедпмЭнщн рсйн ухнечЯуефе.");
	define("UPGRADE_AVAILABLE_MSG", "ЕЯнбй дйбиЭуймз бнбвЬимйуз фпх рспгсЬммбфпт");
	define("UPGRADE_BUTTON", "БнбвбимЯуфе уе {version_number} Фюсб ");
	define("CURRENT_VERSION_MSG", "ёчейт Юдз егкбфбуфЮуей бхфЮн фзн Экдпуз");
	define("LATEST_VERSION_MSG", "Экдпуз дйбиЭуймз гйб егкбфбуз");
	define("UPGRADE_RESULTS_MSG", "БрпфелЭумбфб бнбвЬимйузт");
	define("SQL_SUCCESS_MSG", "SQL есщфЮмбфб ерйфхчЮ");
	define("SQL_FAILED_MSG", "SQL есщфЮмбфб лбнибумЭнб");
	define("SQL_TOTAL_MSG", "јлб фб SQL есщфЮмбфб Эчпхн рсбгмбфпрпйзиеЯ");
	define("VERSION_UPGRADED_MSG", "З Экдпуз убт ёчей бнбвбимйуфеЯ уе");
	define("ALREADY_LATEST_MSG", "ёчейт Юдз фзн рйп рсьуцбфз Экдпуз");
	define("DOWNLOAD_NEW_MSG", "The new version was detected");
	define("DOWNLOAD_NOW_MSG", "Download version {version_number} now");
	define("DOWNLOAD_FOUND_MSG", "We have detected that the new {version_number} version is available to download. Please click the link below to start downloading. After completing the download and replacing the files don't forget to run Upgrade routine again.");
	define("NO_XML_CONNECTION", "Warning! No connection to 'http://www.viart.com/' available!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>