<?php
	$t->set_file("block_body", "header.html");

	// check if we need include header menu
	if ($t->block_exists("header_menu", "block_body")) {
		$sub_block = "header_menu";
		$php_script = "block_site_navigation.php";
		if (file_exists("./blocks_custom/block_site_navigation.php")) {
			include("./blocks_custom/block_site_navigation.php");
		} else {
			include("./blocks/block_site_navigation.php");
		}
		$t->parse($sub_block, false);
		$sub_block = "";
	}
	// end of header menu check

	$request_uri_path = get_request_path();
	$request_uri_base = basename($request_uri_path);
	// set site logo
	$logo_image = get_translation(get_setting_value($settings, "logo_image", "images/tr.gif"));
	$logo_image_alt = get_translation(get_setting_value($settings, "logo_image_alt", HOME_PAGE_TITLE));
	$logo_width = get_setting_value($settings, "logo_image_width", "");
	$logo_height = get_setting_value($settings, "logo_image_height", "");
	$logo_size = "";
	if ($logo_width || $logo_height) {
		if ($logo_width) { $logo_size = "width=\"".$logo_width."\""; }
		if ($logo_height) { $logo_size .= " height=\"".$logo_height."\""; }
	} elseif ($logo_image && !preg_match("/^http\:\/\//", $logo_image)) {
		//$logo_image = $absolute_url . $logo_image;
		$image_size = @GetImageSize($logo_image);
		if (is_array($image_size)) {
			$logo_size = $image_size[3];
		}
	}
	$logo_image_alt = parseSiteTags($logo_image_alt);

	$t->set_var("logo_alt", htmlspecialchars($logo_image_alt));
	$t->set_var("logo_src", htmlspecialchars($logo_image));
	$t->set_var("logo_size", $logo_size);


	$t->set_var("index_href", get_custom_friendly_url("index.php"));
	$t->set_var("products_href", get_custom_friendly_url("products.php"));
	$t->set_var("basket_href", get_custom_friendly_url("basket.php"));
	$t->set_var("user_profile_href", get_custom_friendly_url("user_profile.php"));
	$t->set_var("admin_href", "admin.php");
	$t->set_var("help_href", get_custom_friendly_url("page.php") . "?page=help");
	$t->set_var("about_href", get_custom_friendly_url("page.php") . "?page=about");

	$block_parsed = true;

?>