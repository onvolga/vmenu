function openWindow(pagename, filetype)
{
	var uploadWin = window.open (pagename + '?filetype=' + filetype, 'uploadWin', 'toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=400,height=300');
	uploadWin.focus();
}

function setFilePath(filepath, filetype)
{

	if(filepath != "" && filetype == "personal_image")
	{
		document.user_profile.personal_image.value = filepath;
		document.user_profile.personal_image.focus();
	}
}

function setFileName(filename, filetype)
{
	if(filename != "" && filetype == "personal")
	{
		document.user_profile.personal_image.value = "images/users/" + filename;
		document.user_profile.personal_image.focus();
	}
}

function checkSame()
{
	var sameChecked = document.user_profile.same_as_personal.checked;
	if(sameChecked) {
		var fieldName = "";
		var fields = new Array("name", "first_name", "last_name", "company_id", "company_name", "email", 
			"address1", "address2", "city", "province", "address1", "state_code", "state_id", "zip", "country_code", "country_id",
			"phone", "daytime_phone", "evening_phone", "cell_phone", "fax",
			"phone_code", "daytime_phone_code", "evening_phone_code", "cell_phone_code", "fax_code");
		var orderForm = document.user_profile;
		for (var i = 0; i < fields.length; i++) {
			fieldName = fields[i];
			if (orderForm.elements[fieldName] && orderForm.elements["delivery_" + fieldName]) {
				orderForm.elements["delivery_" + fieldName].value = orderForm.elements[fieldName].value;
			}
		}
	}
}

function uncheckSame()
{
	if (document.user_profile.same_as_personal) {
		document.user_profile.same_as_personal.checked = false;
	}
}

function changeAffiliateCode()
{
	var siteURL = document.user_profile.site_url.value;
	var affiliateHelp = document.user_profile.affiliate_help.value;
	var affiliateCode = document.user_profile.affiliate_code.value;
	if (affiliateCode == "") {
		affiliateCode = "type_your_code_here";
	}
	var affiliateURL = siteURL + "?af=" + affiliateCode;
	affiliateHelp = affiliateHelp.replace("\{affiliate_url\}", affiliateURL);

	var affiliateHelpConrol = document.getElementById("affiliate_help_info");
	if (affiliateHelpConrol) {
		affiliateHelpConrol.innerHTML = affiliateHelp;
	}
}


function updateStates(pbId, controlType)
{
	var peronsalCountryObj = document.getElementById("country_id_" + pbId);
	var deliveryCountryObj = document.getElementById("delivery_country_id_" + pbId);
	var personalStateObj = document.getElementById("state_id_" + pbId);
	var deliveryStateObj = document.getElementById("delivery_state_id_" + pbId);
	var personalStateComObj = document.getElementById("state_id_comments_" + pbId);
	var deliveryStateComObj = document.getElementById("delivery_state_id_comments_" + pbId);
	var personalStateReqObj = document.getElementById("state_id_required_" + pbId);
	var deliveryStateReqObj = document.getElementById("delivery_state_id_required_" + pbId);

	var countryId = ""; var stateObj = ""; var stateCommentsObj = ""; var stateRequiredObj = "";
	if (controlType == "personal") {
		if (peronsalCountryObj) {
			countryId = peronsalCountryObj.options[peronsalCountryObj.selectedIndex].value;
		}
		if (personalStateObj) { stateObj = personalStateObj; }
		if (personalStateComObj) { stateCommentsObj = personalStateComObj; }
		if (personalStateReqObj) { stateRequiredObj = personalStateReqObj; }
	} else {
		if (deliveryCountryObj) {
			countryId = deliveryCountryObj.options[deliveryCountryObj.selectedIndex].value;
		}
		if (deliveryStateObj) { stateObj = deliveryStateObj; }
		if (deliveryStateComObj) { stateCommentsObj = deliveryStateComObj; }
		if (deliveryStateReqObj) { stateRequiredObj = deliveryStateReqObj; }
	}
	if (stateObj) {
		// remove old list
		var totalOptions = stateObj.options.length;
		for (var i = totalOptions - 1; i >= 1; i--) {
			stateObj.options[i] = null;
		}
		// check and add new states list
		if (countryId == "") {
			stateObj.style.display = "none";
			if (stateRequiredObj) { stateRequiredObj.style.display = "none"; }
			if (stateCommentsObj) {
				stateCommentsObj.innerHTML = selectCountryFirst;
				stateCommentsObj.style.display = "inline";
			}
		} else if (states[countryId]) {
			for(stateId in states[countryId]){
				//var key  = val;
				var stateName = states[countryId][stateId];				
				stateObj.options[stateObj.length] = new Option(stateName, stateId);
			}
			stateObj.style.display = "inline";
			if (stateRequiredObj) { stateRequiredObj.style.display = "inline"; }
			if (stateCommentsObj) {
				stateCommentsObj.innerHTML = "";
				stateCommentsObj.style.display = "none";
			}
		} else {
			stateObj.style.display = "none";
			if (stateRequiredObj) { stateRequiredObj.style.display = "none"; }
			if (stateCommentsObj) {
				stateCommentsObj.innerHTML = noStatesForCountry;
				stateCommentsObj.style.display = "inline";
			}
		}
	}
}