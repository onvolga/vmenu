<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Forkert download parameter (s).");
	define("DOWNLOAD_MISS_PARAM", "Manglende download parameter (s).");
	define("DOWNLOAD_INACTIVE", "Download inaktive.");
	define("DOWNLOAD_EXPIRED", "Din download er udlГёbet.");
	define("DOWNLOAD_LIMITED", "You have exceed the maximum number of downloads. Du har overskedet det maksimale antal downloads.");
	define("DOWNLOAD_PATH_ERROR", "Path to product cannot be found. Sti til produktet ikke kan findes.");
	define("DOWNLOAD_RELEASE_ERROR", "Release blev ikke fundet.");
	define("DOWNLOAD_USER_ERROR", "Kun registrerede brugere kan hente denne fil.");
	define("ACTIVATION_OPTIONS_MSG", "Aktivering Valg");
	define("ACTIVATION_MAX_NUMBER_MSG", "Max antal aktiveringer");
	define("DOWNLOAD_OPTIONS_MSG", "Downloadable / Software Valg");
	define("DOWNLOADABLE_MSG", "Downloadable (Software)");
	define("DOWNLOADABLE_DESC", "for download produkter, kan du ogsГҐ angive 'download periode', 'Sti til download filer' og 'aktiveringes valg'");
	define("DOWNLOAD_PERIOD_MSG", "Download periode");
	define("DOWNLOAD_PATH_MSG", "Sti til download filer");
	define("DOWNLOAD_PATH_DESC", "du kan tilfГёje flere stier delt med semikolon");
	define("UPLOAD_SELECT_MSG", "VГ¦lg fil til at uploade, og tryk pГҐ {button_name} knappen.");
	define("UPLOADED_FILE_MSG", "File <b> {filename} </ b> blev uploadet.");
	define("UPLOAD_SELECT_ERROR", "VГ¦lg en fil fГёrst.");
	define("UPLOAD_IMAGE_ERROR", "Kun billedfiler er tilladt.");
	define("UPLOAD_FORMAT_ERROR", "Denne filtype er ikke tilladt.");
	define("UPLOAD_SIZE_ERROR", "Filer stГёrre end {filesize} er ikke tilladt.");
	define("UPLOAD_DIMENSION_ERROR", "Billeder, der er stГёrre end {dimension} er ikke tilladt.");
	define("UPLOAD_CREATE_ERROR", "Systemet kan ikke oprette filen.");
	define("UPLOAD_ACCESS_ERROR", "Du har ikke tilladelse til at uploade filer.");
	define("DELETE_FILE_CONFIRM_MSG", "Er du sikker pГҐ du vil slette denne fil?");
	define("NO_FILES_MSG", "Ingen filer blev fundet");
	define("SERIAL_GENERATE_MSG", "Generer serienummer");
	define("SERIAL_DONT_GENERATE_MSG", "generer ikke ");
	define("SERIAL_RANDOM_GENERATE_MSG", "generere tilfГ¦ldigt serienummer til software produktet");
	define("SERIAL_FROM_PREDEFINED_MSG", "fГҐ serienummer fra foruddefineret liste");
	define("SERIAL_PREDEFINED_MSG", "Foruddefinerede nummerering");
	define("SERIAL_NUMBER_COLUMN", "Serienummer");
	define("SERIAL_USED_COLUMN", "Brugt");
	define("SERIAL_DELETE_COLUMN", "Delete Slet");
	define("SERIAL_MORE_MSG", "TilfГёj flere serienumre?");
	define("SERIAL_PERIOD_MSG", "Serienummer periode");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Vis vilkГҐr & betingelser");
	define("DOWNLOAD_SHOW_TERMS_DESC", "For at hente denne vare skal brugeren lГ¦se og acceptere vores betingelser og vilkГҐr");
	define("DOWNLOAD_TERMS_MSG", "VilkГҐr & betingelser");
	define("DOWNLOAD_TERMS_USER_DESC", "Jeg har lГ¦st og accepterer vilkГҐr og betingelser");
	define("DOWNLOAD_TERMS_USER_ERROR", "For at downloade det produkt skal du lГ¦se og acceptere vores betingelser og vilkГҐr");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>