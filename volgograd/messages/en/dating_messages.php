<?php
	// dating messages
	define("DATING_TITLE", "Dating");
	define("CAN_USER_ADD_DATING_MSG", "Can user add new dating profile");
	define("CAN_USER_EDIT_DATING_MSG", "Can user edit his dating profile");
	define("CAN_USER_DELETE_DATING_MSG", "Can user delete his dating profile");
	define("AUTO_APPROVE_DATING_MSG", "Automatically approve submitted dating profile");
	define("MY_DATING_MSG", "My Dating");
	define("MY_DATING_DESC", "If you like to find your love, just submit your dating profile here.");
	define("ADD_MY_DATING_PROFILE_MSG", "Add My Dating Profile");
	define("NO_DATING_PROFILES_MSG", "No dating profiles were found");
	define("DATING_SEX_FIELD", "I am a");
	define("LOOKING_SEX_FIELD", "Looking for a");
	define("PERSONAL_INFO_FIELD", "A few words about you");
	define("LOOKING_INFO_FIELD", "Who you are looking for");
	define("ABOUT_ME_MSG", "About me");
	define("I_AM_LOOKING_MSG", "I'm looking for a");
	define("DATING_GENERAL_INFO_MSG", "General Information");
	define("MALE_MSG", "Male");
	define("FEMALE_MSG", "Female");
	define("USER_DATING_LIMIT_ERROR", "Sorry, but you are not allowed to add more than {profiles_limit} dating profiles.");
	define("ETHNICITY_MSG", "Ethnicity");
	define("DATING_NEW_ERROR", "You don't have permissions to create a new profile.");
	define("DATING_EDIT_ERROR", "You don't have permissions to edit this profile.");
	define("DATING_DELETE_ERROR", "You don't have permissions to delete this profile.");

	define("PHOTO_RESTRICTIONS_MSG", "Photo Restrictions");
	define("PHOTO_TINY_MSG", "Tiny Photo");
	define("PHOTO_SMALL_MSG", "Small Photo");
	define("PHOTO_LARGE_MSG", "Large Photo");
	define("PHOTO_SUPER_MSG", "Super-sized Photo");

	define("GENERATE_TINY_PHOTO_MSG", "generate tiny photo");
	define("GENERATE_SMALL_PHOTO_MSG", "generate small photo");
	define("GENERATE_LARGE_PHOTO_MSG", "generate large photo");
	define("GENERATE_SUPER_PHOTO_MSG", "generate super-sized photo");

?>