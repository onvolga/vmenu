<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  map_store.php                                             ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	@set_time_limit(900);
	include_once("./includes/common.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./messages/" . $language_code . "/forum_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/navigator.php");
	
	$cms_page_code = "map_store";
	$script_name   = "map_store.php";
	$current_page  = get_custom_friendly_url("map_store.php");
	$auto_meta_title = SITE_MAP_TITLE;

	include_once("./includes/page_layout.php");

?>
