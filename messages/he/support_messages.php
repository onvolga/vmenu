<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "מרכז תמיכה");
	define("SUPPORT_REQUEST_INF_TITLE", "בקש מידע");
	define("SUPPORT_REPLY_FORM_TITLE", "תשובה");

	define("MY_SUPPORT_ISSUES_MSG", "בקשות התמיכה שלי");
	define("MY_SUPPORT_ISSUES_DESC", "אם יש לך בעיה כל שהיא עם המוצרים שרכשת, צוות התמיכה שלנו ערוכים לעזרה. לתמיכה, הקש על הקישור שמעל או שלח בקשת תמיכה.");
	define("NEW_SUPPORT_REQUEST_MSG", "בקשה חדשה");
	define("SUPPORT_REQUEST_ADDED_MSG", "תודה<br>צוות התמיכה שלנו ינסה לעזור לך בהקדם האפשרי.");
	define("SUPPORT_SELECT_DEP_MSG", "בחר מחלקה");
	define("SUPPORT_SELECT_PROD_MSG", "בחר מוצר");
	define("SUPPORT_SELECT_STATUS_MSG", "בחר סטטוס");
	define("SUPPORT_NOT_VIEWED_MSG", "לא נצפה");
	define("SUPPORT_VIEWED_BY_USER_MSG", "נצפה ע\"י המשתמש");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "נצפה ע\"י המנהל");
	define("SUPPORT_STATUS_NEW_MSG", "חדש");
	define("NO_SUPPORT_REQUEST_MSG", "לא נמצאו נושאים");

	define("SUPPORT_SUMMARY_COLUMN", "סיכום");
	define("SUPPORT_TYPE_COLUMN", "סוג");
	define("SUPPORT_UPDATED_COLUMN", "עודכן לאחרונה");

	define("SUPPORT_USER_NAME_FIELD", "שם");
	define("SUPPORT_USER_EMAIL_FIELD", "כתוכת אי-מייל");
	define("SUPPORT_IDENTIFIER_FIELD", "זיהוי (מס' חשבונית)");
	define("SUPPORT_ENVIRONMENT_FIELD", "סביבה (מערכת הפעלה, בסיס נתונים, שרת, וכד')");
	define("SUPPORT_DEPARTMENT_FIELD", "מחלקה");
	define("SUPPORT_PRODUCT_FIELD", "מוצר");
	define("SUPPORT_TYPE_FIELD", "סוג");
	define("SUPPORT_CURRENT_STATUS_FIELD", "סטטוס נוכחי");
	define("SUPPORT_SUMMARY_FIELD", "סיכום בשורה אחת");
	define("SUPPORT_DESCRIPTION_FIELD", "תאור");
	define("SUPPORT_MESSAGE_FIELD", "הודעה");
	define("SUPPORT_ADDED_FIELD", "נוסף");
	define("SUPPORT_ADDED_BY_FIELD", "הוסף על ידי");
	define("SUPPORT_UPDATED_FIELD", "עודכן לאחרונה");

	define("SUPPORT_REQUEST_BUTTON", "הגש בקשה");
	define("SUPPORT_REPLY_BUTTON", "תשובה");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "חסר ערך <b>Support ID</b> ");
	define("SUPPORT_MISS_CODE_ERROR", "חסר ערך <b>Verification</b> ");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Support ID</b> ערך שגוי");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Verification</b> ערך שגוי");

	define("MAIL_DATA_MSG", "מידע דואר");
	define("HEADERS_MSG", "כותרות");
	define("ORIGINAL_TEXT_MSG", "טקסט מקורי");
	define("ORIGINAL_HTML_MSG", "HTML מקורי");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "מצטערים, אינך מורשה לסגור כרטיסים <br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "מצטערים, אינך מורשה לענות לכרטיסים <br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "מצטערים, אינך מורשה ליצור כרטיסים חדשים <br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "מצטערים, אינך מורשה להסיר כרטיסים <br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "מצטערים, אינך מורשה לעדכן כרטיסים <br>");
	define("NO_TICKETS_FOUND_MSG", "לא נמצאו כרטיסים");
	define("HIDDEN_TICKETS_MSG", "כרטיסים חבויים");
	define("ALL_TICKETS_MSG", "כל הכרטיסים");
	define("ACTIVE_TICKETS_MSG", "כרטיסים פעילים");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "אינך מוקצה למחלקה זו");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "אינך מוקצה לאף מחלקה");
	define("REPLY_TO_NAME_MSG", "תשובה ל- {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "קטגורית מידע");
	define("KNOWLEDGE_TITLE_MSG", "כותרת מידע");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "סטטוס של פריט מידע");
	define("SELECT_RESPONSIBLE_MSG", "בחר אחראי");

?>