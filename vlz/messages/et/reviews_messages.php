<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Valikuline");
	define("BAD_MSG", "Halb");
	define("POOR_MSG", "NС…rk");
	define("AVERAGE_MSG", "Keskmine");
	define("GOOD_MSG", "Hea");
	define("EXCELLENT_MSG", "SuurepРґrane");
	define("REVIEWS_MSG", "Arvustused");
	define("NO_REVIEWS_MSG", "Arvustusi ei leitud");
	define("WRITE_REVIEW_MSG", "Kirjuta arvustus");
	define("RATE_PRODUCT_MSG", "Hinda seda toodet");
	define("RATE_ARTICLE_MSG", "Hinda seda artiklit");
	define("NOT_RATED_PRODUCT_MSG", "Toodet ei ole veel hinnatud.");
	define("NOT_RATED_ARTICLE_MSG", "Artiklit ei ole veel hinnatud.");
	define("AVERAGE_RATING_MSG", "Kliendi keskmine hinne");
	define("BASED_ON_REVIEWS_MSG", "PС…hineb {total_votes} arvustusel");
	define("POSITIVE_REVIEW_MSG", "Positiivne kliendi arvustus");
	define("NEGATIVE_REVIEW_MSG", "Negatiivne kliendi arvustus");
	define("SEE_ALL_REVIEWS_MSG", "Vaata kС…iki klientide arvustusi...");
	define("ALL_REVIEWS_MSG", "KС…ik arvustused");
	define("ONLY_POSITIVE_MSG", "Ainult positiivsed");
	define("ONLY_NEGATIVE_MSG", "Ainult negatiivsed");
	define("POSITIVE_REVIEWS_MSG", "Positiivsed arvustused");
	define("NEGATIVE_REVIEWS_MSG", "Negatiivsed arvustused");
	define("SUBMIT_REVIEW_MSG", "AitРґh<br>Sinu kommentaarid vaadatakse СЊle. Heaks kiitmise korral ilmuvad need meie veebilehel. <br>Me jРґtame endale С…iguse lСЊkata tagasi postitused, mis ei sobi meie juhtnС†С†ridega.");
	define("ALREADY_REVIEW_MSG", "Sa oled juba esitanud oma arvustuse.");
	define("RECOMMEND_PRODUCT_MSG", "Kas soovitaksid <br> seda toodet teistele?");
	define("RECOMMEND_ARTICLE_MSG", "Kas soovitaksid <br> seda artiklit teistele?");
	define("RATE_IT_MSG", "Hinda!");
	define("NAME_ALIAS_MSG", "Nimi vС…i alias");
	define("SHOW_MSG", "NРґita");
	define("FOUND_MSG", "Leitud");
	define("RATING_MSG", "Keskmine hinne");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Ainult registreerunud kasutajad saavad lisada oma arvustusi.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Vabandame, kuid Teil ei ole lubatud lisada arvustusi.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Lubatud nРґha arvustusi");
	define("ALLOWED_POST_REVIEWS_MSG", "Lubatud lisada arvustusi");
	define("REVIEWS_RESTRICTIONS_MSG", "Arvustuste piirangud");
	define("REVIEWS_PER_USER_MSG", "Arvustuste arv kasutaja kohta");
	define("REVIEWS_PER_USER_DESC", "jРґta see vРґli tСЊhjaks, kui ei taha piirata kasutajate poolt lisatavate arvustuste arvu");
	define("REVIEWS_PERIOD_MSG", "Arvustuste periood");
	define("REVIEWS_PERIOD_DESC", "periood, mil arvustuste piirangud kehtivad");

	define("AUTO_APPROVE_REVIEWS_MSG", "Automaatselt kiida arvustused heaks");
	define("BY_RATING_MSG", "Hindamise jРґrgi");
	define("SELECT_REVIEWS_FIRST_MSG", "Palun vali esmalt arvustused.");
	define("SELECT_REVIEWS_STATUS_MSG", "Palun vali staatus.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "Sa oled muutmas valitud arvustuste staatust staatuseks '{status_name}'. JРґtkata?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Kas oled kindel, et tahad eemaldada valitud arvustused ({total_reviews})?");

?>