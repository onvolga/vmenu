<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Tщy chчn");
	define("BAD_MSG", "Kйm");
	define("POOR_MSG", "DЯѕi trung bмnh");
	define("AVERAGE_MSG", "Trung bмnh");
	define("GOOD_MSG", "TЇt");
	define("EXCELLENT_MSG", "R¤t tЇt");
	define("REVIEWS_MSG", "Cбc bмnh lu§n");
	define("NO_REVIEWS_MSG", "Khфng tмm th¤y bмnh lu§n");
	define("WRITE_REVIEW_MSG", "ViЄt bмnh lu§n");
	define("RATE_PRODUCT_MSG", "Рбnh giб sдn ph¦m nаy");
	define("RATE_ARTICLE_MSG", "Рбnh giб mшc nаy");
	define("NOT_RATED_PRODUCT_MSG", "Sдn ph¦m nаy chЯa рЯюc рбnh giб.");
	define("NOT_RATED_ARTICLE_MSG", "Mшc nаy chЯa рЯюc рбnh giб.");
	define("AVERAGE_RATING_MSG", "MСc рбnh giб cьa khбch hаng");
	define("BASED_ON_REVIEWS_MSG", "dсa trкn {total_votes} рбnh giб");
	define("POSITIVE_REVIEW_MSG", "Рa nh gia  kha ch ha ng tф t");
	define("NEGATIVE_REVIEW_MSG", "Рa nh gia  kha ch ha ng khфng tф t");
	define("SEE_ALL_REVIEWS_MSG", "Xem t¤t cд cбc рбnh giб cьa khбch hаng");
	define("ALL_REVIEWS_MSG", "T¤t cд cбc рбnh giб.");
	define("ONLY_POSITIVE_MSG", "Chi  tф t");
	define("ONLY_NEGATIVE_MSG", "Chi  xв u");
	define("POSITIVE_REVIEWS_MSG", "Рa nh gia  tф t");
	define("NEGATIVE_REVIEWS_MSG", "Рa nh gia  khфng tф t");
	define("SUBMIT_REVIEW_MSG", "Cбm Ѕn bХn<br>Bмnh lu§n cьa bХn sЁ рЯюc рбnh giб. NЄu рЯюc xбc nh§n, nу sЁ xu¤t hi®n trкn trang web cьa chъng tфi.");
	define("ALREADY_REVIEW_MSG", "Ba n рa  viк t mф t ba i bi nh luв n");
	define("RECOMMEND_PRODUCT_MSG", "Ba n co  muф n khuyкn du ng sa n phв m na y рк n ngЯЅ i kha c?");
	define("RECOMMEND_ARTICLE_MSG", "Ba n co  muф n khuyкn du ng mu c na y рк n vЅ i ngЯЅ i kha c?");
	define("RATE_IT_MSG", "Рa nh gia ");
	define("NAME_ALIAS_MSG", "Tкn hoе c nick");
	define("SHOW_MSG", "Hiк n thi ");
	define("FOUND_MSG", "Ti m");
	define("RATING_MSG", "Рa nh gia  kha ch ha ng");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>