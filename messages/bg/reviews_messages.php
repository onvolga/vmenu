<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//съобщения свързани рецензии и класации
	define("OPTIONAL_MSG", "По избор");
	define("BAD_MSG", "Лошо");
	define("POOR_MSG", "Слабо");
	define("AVERAGE_MSG", "Средно");
	define("GOOD_MSG", "Добро");
	define("EXCELLENT_MSG", "Отлично");
	define("REVIEWS_MSG", "Рецензии");
	define("NO_REVIEWS_MSG", "Не са намерени рецензии.");
	define("WRITE_REVIEW_MSG", "Напиши рецензия.");
	define("RATE_PRODUCT_MSG", "Оцени този продукт.");
	define("RATE_ARTICLE_MSG", "Оцени този артикул.");
	define("NOT_RATED_PRODUCT_MSG", "Продуктът все още не е оценен.");
	define("NOT_RATED_ARTICLE_MSG", "Артикулът все още не е оценен.");
	define("AVERAGE_RATING_MSG", "Осреднен клиентски рейтинг.");
	define("BASED_ON_REVIEWS_MSG", "базиран на {total_votes} рецензии.");
	define("POSITIVE_REVIEW_MSG", "Положителна клиентска рецензия.");
	define("NEGATIVE_REVIEW_MSG", "Отрицателна клиентска рецензия.");
	define("SEE_ALL_REVIEWS_MSG", "Виж всички клиентски рецензии.");
	define("ALL_REVIEWS_MSG", "Всички рецензии.");
	define("ONLY_POSITIVE_MSG", "Само положителните.");
	define("ONLY_NEGATIVE_MSG", "Само отрицателните.");
	define("POSITIVE_REVIEWS_MSG", "Положителни рецензии.");
	define("NEGATIVE_REVIEWS_MSG", "Отрицателни рецензии.");
	define("SUBMIT_REVIEW_MSG", "Благодарим<br>Вашите коментари ще бъдат разгледани. Ако бъдат одобрени ще се поставят на сайта ни.<br>Ние запазваме правото си да отхвърлим всяко предложение, което не отговаря на нашите предписания.");
	define("ALREADY_REVIEW_MSG", "Вече оставихте рецензия.");
	define("RECOMMEND_PRODUCT_MSG", "Бихте ли препоръчали<br> този продукт на други?");
	define("RECOMMEND_ARTICLE_MSG", "Бихте ли препоръчали<br> този артикул на други?");
	define("RATE_IT_MSG", "Оцени го.");
	define("NAME_ALIAS_MSG", "Име или псевдоним.");
	define("SHOW_MSG", "Покажи");
	define("FOUND_MSG", "Намерени");
	define("RATING_MSG", "Среден рейтинг.");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Само регистрирани потребители могат да добавят свои рецензии.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Съжаляваме, но вие не може да добавяте рецензии.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Допускани да виждат рецензии");
	define("ALLOWED_POST_REVIEWS_MSG", "Допускани да качват рецензии");
	define("REVIEWS_RESTRICTIONS_MSG", "Ограничения на рецензиите");
	define("REVIEWS_PER_USER_MSG", "Брой рецензии на потребител");
	define("REVIEWS_PER_USER_DESC", "Оставете това поле празно ако не искате да ограничавате броят рецензии, които потребителя може да качва.");
	define("REVIEWS_PERIOD_MSG", "Период на рецензиите");
	define("REVIEWS_PERIOD_DESC", "Период, в който ограниченията за рецензиите са в сила.");

	define("AUTO_APPROVE_REVIEWS_MSG", "Одобрявай рецензиите автоматично.");
	define("BY_RATING_MSG", "По рейтинг");
	define("SELECT_REVIEWS_FIRST_MSG", "Моля, първо изберете рецензии.");
	define("SELECT_REVIEWS_STATUS_MSG", "Моля, изберете статус.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "Ще промените статусът на избраните рецензии на '{status_name}' .Продължи?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Сигурни ли сте, че искате да премахнете избраните рецензии ({total_reviews})?");

?>