<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "Aihe");
	define("TOPIC_MESSAGE_TITLE", "viesti");

	define("MY_FORUM_TOPICS_MSG", "Omat aiheeni");
	define("ALL_FORUM_TOPICS_MSG", "Kaikki aiheet");
	define("MY_FORUM_TOPICS_DESC", "Haluatko jakaa mielipiteitд, kertoa ongelmistasi ja auttaa? Tee se tддllд ja liity foorumiin!");
	define("NEW_TOPIC_MSG", "Uusi aihe");
	define("NO_TOPICS_MSG", "Ei aiheita");
	define("FOUND_TOPICS_MSG", "Lцysin <b>{found_records}</b> aihetta jotka vastaavat hakuasi '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Ei foorumeita");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "Aiheet");
	define("FORUM_REPLIES_COLUMN", "vastaukset");
	define("FORUM_LAST_POST_COLUMN", "Viim. pдivitetty");
	define("FORUM_MODERATORS_MSG", "Yllдpitдjдt");

	define("TOPIC_NAME_COLUMN", "Aihe");
	define("TOPIC_AUTHOR_COLUMN", "Tekijд");
	define("TOPIC_VIEWS_COLUMN", "Katsottu");
	define("TOPIC_REPLIES_COLUMN", "Vastauksia");
	define("TOPIC_UPDATED_COLUMN", "Viim.pдiv");
	define("TOPIC_ADDED_MSG", "Kiitos<br>Aiheesi lisдttiin");

	define("TOPIC_ADDED_BY_FIELD", "Lisдtty");
	define("TOPIC_ADDED_DATE_FIELD", "Lisдtty");
	define("TOPIC_UPDATED_FIELD", "Viim pдiv.");
	define("TOPIC_NICKNAME_FIELD", "Lempinimi");
	define("TOPIC_EMAIL_FIELD", "Email osoitteesi");
	define("TOPIC_NAME_FIELD", "Aihe");
	define("TOPIC_MESSAGE_FIELD", "Viesti");
	define("TOPIC_NOTIFY_FIELD", "Lдhetд vastaukset sдhkцpostiini");

	define("ADD_TOPIC_BUTTON", "Lisдд aihe");
	define("TOPIC_MESSAGE_BUTTON", "Lisдд viesti");

	define("TOPIC_MISS_ID_ERROR", "Puuttuva <b>Aihe ID</b> parametri.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Taihe ID</b> parametreillд on vддrд arvo");
	define("FORUM_SEARCH_MESSAGE", "Lцysimme {search_count} viestiд jotka vastaavat hakua '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Esikatselu");
	define("TOPIC_SAVE_BUTTON", "Tallenna");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>