<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.10RE                                               ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viartsoft.ru                                         ***
  ***                                                                      ***
  ****************************************************************************
*/

	// отзывы
	define("OPTIONAL_MSG", "Не обязательно");
	define("BAD_MSG", "Очень плохо");
	define("POOR_MSG", "Плохо");
	define("AVERAGE_MSG", "Средне");
	define("GOOD_MSG", "Хорошо");
	define("EXCELLENT_MSG", "Отлично");
	define("REVIEWS_MSG", "Отзывы");
	define("NO_REVIEWS_MSG", "Отзывы не найдены");
	define("WRITE_REVIEW_MSG", "Напишите отзыв или совет");
	define("RATE_PRODUCT_MSG", "Оцените");
	define("RATE_ARTICLE_MSG", "Оцените");
	define("NOT_RATED_PRODUCT_MSG", "Пока нет отзывов и оценок");
	define("NOT_RATED_ARTICLE_MSG", "Пока нет отзывов и оценок");
	define("AVERAGE_RATING_MSG", "Средний рейтинг");
	define("BASED_ON_REVIEWS_MSG", "Отзывов: ");
	define("POSITIVE_REVIEW_MSG", "Положительный отзыв");
	define("NEGATIVE_REVIEW_MSG", "Негативный отзыв");
	define("SEE_ALL_REVIEWS_MSG", "См. все отзывы пользователей");
	define("ALL_REVIEWS_MSG", "Все отзывы");
	define("ONLY_POSITIVE_MSG", "Только положительные");
	define("ONLY_NEGATIVE_MSG", "Только отрицательные");
	define("POSITIVE_REVIEWS_MSG", "Положительные отзывы");
	define("NEGATIVE_REVIEWS_MSG", "Отрицательные отзывы");
	define("SUBMIT_REVIEW_MSG", "Спасибо! <br> Возможно, Ваши отзыв, оценка или сообщение появятся не сразу, а после проверки Модератором, для защиты от спама. <br> Мы сохраняем за собой право отклонять любые сообщения, которые содержат ненормативную лексику, спам, не относятся к теме и т.п.");
	define("ALREADY_REVIEW_MSG", "Вы уже поместили отзыв.");
	define("RECOMMEND_PRODUCT_MSG", "Вы удовлетворены <br> качеством товаров/услуг?");
	define("RECOMMEND_ARTICLE_MSG", "Вы считаете эту информацию полезной?");
	define("RATE_IT_MSG", "Ваша оценка");
	define("NAME_ALIAS_MSG", "Имя");
	define("SHOW_MSG", "Показать");
	define("FOUND_MSG", "Найдено");
	define("RATING_MSG", "Рейтинг");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Только зарегистрированные пользователи могут оставлять отзывы и комментерии.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Извините, но у вас нет доступа к отзывам.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Разрешить просмотр отзывов");
	define("ALLOWED_POST_REVIEWS_MSG", "Разрешить оставлять отзывы");
	define("REVIEWS_RESTRICTIONS_MSG", "Ограничения отзывов");
    define("REVIEWS_PER_USER_MSG", "Кол-во отзывов от пользователя");
    define("REVIEWS_PER_USER_DESC", "оставьте это поле пустым, если не хотите ограничивать кол-во отзывов от одного пользователя");
	define("REVIEWS_PERIOD_MSG", "Период ограничения для отзывов");
	define("REVIEWS_PERIOD_DESC", "период, в течении которого пользователь не сможет оставить повторный отзыв");

	define("AUTO_APPROVE_REVIEWS_MSG", "Публиковать отзывы без проверки автоматически");
	define("BY_RATING_MSG", "Рейтинг");
	define("SELECT_REVIEWS_FIRST_MSG", "Пожалуйста, выберите отзывы.");
	define("SELECT_REVIEWS_STATUS_MSG", "Пожалуйста, выберите статус.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "Вы собираетесь изменить статус отзыва(ов) для '{status_name}'. Продолжить?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Вы уверены, что хотите удалить ({total_reviews}) отзывов?");

?>
