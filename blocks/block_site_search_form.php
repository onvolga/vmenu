<?php

	$default_title = "{FULL_SITE_SEARCH_MSG}";
/*                           
  * Файл дополнен
  * MainOlMa
  * Добавление опции simple_search - 1 вставка - 06.05.2011
  * для упрощённого вывода поиска в шапке
*/

	if (isset($vars["tag_name"])) {

/*--------------------Добавление опции simple_search--------------------*/
$simple_search = get_setting_value($vars, "simple_search", 0);
//если отмечен чекбокс в опциях блока поиска, то выбирается другой шаблон
if ($simple_search!=""){$html_template = get_setting_value($block, "html_template", "vi_header_search.html");}
else
/*-------------------/Добавление опции simple_search--------------------*/

		$html_template = get_setting_value($block, "html_template", "block_site_search_form.html"); 
	  $t->set_file("block_body", $html_template);
	}

	$t->set_var("search_href", get_custom_friendly_url("site_search.php"));

	$q = trim(get_param("q"));
	$sq = trim(get_param("sq"));

	// set up search form parameters
	$t->set_var("q", htmlspecialchars($q));
	$t->set_var("sq", htmlspecialchars($sq));

	if (isset($vars["tag_name"])) {
		$block_parsed = true;
	}
?>