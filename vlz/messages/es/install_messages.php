<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// mensajes de la instalaciуn
	define("INSTALL_TITLE", "Instalaciуn de ViArt SHOP");

	define("INSTALL_STEP_1_TITLE", "Instalaciуn: Paso 1");
	define("INSTALL_STEP_1_DESC", "Gracias por elegir ViArt. Para completar esta instalaciуn por favor rellene los detalles necesarios. Tenga en cuenta que la base de datos en la cual usted hace la instalaciуn debe haber sido creada con antelaciуn. Si estб instalando en una base de datos que utiliza ODBC, por ejemplo MS Access, debe crear un DSN antes de continuar.");
	define("INSTALL_STEP_2_TITLE", "Instalaciуn: Paso 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Instalaciуn: Paso 3");
	define("INSTALL_STEP_3_DESC", "Por favor Seleccione el aspecto de su sitio. Podrб cambiar dicho aspecto posteriormente.");
	define("INSTALL_FINAL_TITLE", "Instalaciуn: Final");
	define("SELECT_DATE_TITLE", "Seleccione el formato de Fecha");

	define("DB_SETTINGS_MSG", "Parбmetros de la base de datos");
	define("DB_PROGRESS_MSG", "Progreso del llenado de la estructura de la Base de Datos");
	define("SELECT_PHP_LIB_MSG", "Seleccionar la Biblioteca de PHP");
	define("SELECT_DB_TYPE_MSG", "Seleccionar el tipo de base de datos");
	define("ADMIN_SETTINGS_MSG", "Parбmetros de administraciуn ");
	define("DATE_SETTINGS_MSG", "Formatos de la Fecha");
	define("NO_DATE_FORMATS_MSG", "Los formatos de la fecha no estбn disponibles");
	define("INSTALL_FINISHED_MSG", "En este punto ha terminado la instalaciуn bбsica. Por favor asegъrese de que los parбmetros sean correctos y haga los cambios necesarios en la secciуn de Administraciуn.");
	define("ACCESS_ADMIN_MSG", "Para entrar en la secciуn de Administraciуn pinche aquн.");
	define("ADMIN_URL_MSG", "Direcciуn de la pбgina de Administraciуn");
	define("MANUAL_URL_MSG", "Manual URL");
	define("THANKS_MSG", "Gracias por elegir <b>ViArt SHOP</b>. ");

	define("DB_TYPE_FIELD", "Tipo de base de datos");
	define("DB_TYPE_DESC", "Por favor seleccione el <b>tipo de base de datos</b> que estб utilizando. Si utiliza SQL Server o Microsoft Access, Por favor seleccione ODBC.");
	define("DB_PHP_LIB_FIELD", "Libreria de PHP");
	define("DB_HOST_FIELD", "Servidor");
	define("DB_HOST_DESC", "Por favor, introduzca la <b>Nombre</b> o <b>la direcciуn IP del servidor</b> en donde estan las bases de datos en las que ViArt se ejecutarб. Si estб ejecutando su base de datos en su PC, entonces probablemente puede dejar tal y como \\\"<b>localhost</b>\\\" Y el puerto en blanco. Si utiliza una base de datos proporcionada por su empresa de alojamiento, Por favor, consulte a su empresa de alojamiento de la documentaciуn para la configuraciуn del servidor.");
	define("DB_PORT_FIELD", "Puerto");
	define("DB_NAME_FIELD", "Nombre de la base de datos / DSN");
	define("DB_NAME_DESC", "Si estб utilizando una base de datos como MySQL o PostgreSQL a continuaciуn, por favor, introduzca el <b>nombre de la base de datos</b> En la que ViArt va a crear sus tablas. Esta base de datos debe estar creada ya. Si tan sуlo instala ViArt para propуsitos de prueba en el equipo de PC local PC entonces la mayorнa de los sistemas tienen una Base de datos que puede utilizar \\\"<b>test</b>\\\" . Si no, por favor, crear una base de datos llamada \\\"viart\\\" . Si estб utilizando Microsoft Access o SQL Server, la base de datos debe ser el nombre <b>Nombre de la DSN</b> en que usted ha creado las fuentes de datos (ODBC) en la secciуn de su panel de control");
	define("DB_USER_FIELD", "Nombre de usuario");
	define("DB_PASS_FIELD", "Contraseсa");
	define("DB_USER_PASS_DESC", "<b>Nombre de usuario</b> y <b>Contraseсa</b> - Por favor, introduzca el nombre de usuario y la contraseсa que utiliza para acceder a la base de datos. Si estб usando una instalaciуn local de ensayo es, probablemente, el nombre de usuario \\<b>root</b>\\\" Y la contraseсa es probablemente en blanco. Esto es lo mбs recomendable para las pruebas, pero tenga en cuenta que esto no es seguro en los servidores de producciуn.");
	define("DB_PERSISTENT_FIELD", "Conexiуn constante");
	define("DB_PERSISTENT_DESC", "para usar conexiones persistentes en MySQL o Postgre, marque esta casilla. Si no sabe lo que significa, es probablemente mejor dejarla sin marcar.");
	define("DB_CREATE_DB_FIELD", "Crear DB");
	define("DB_CREATE_DB_DESC", "es posible crear la base de datos, Marque esta casilla. Sуlo funciona para MySQL y Postgre");
	define("DB_POPULATE_FIELD", "Poblar la base de datos");
	define("DB_POPULATE_DESC", "Para crear la estructura de las tablas y llenarlas con datos marque la casilla.");
	define("DB_TEST_DATA_FIELD", "Datos de prueba");
	define("DB_TEST_DATA_DESC", "Para aсadir algunos datos de prueba a su base de datos, marque la casilla de verificaciуn");
	define("ADMIN_EMAIL_FIELD", "Correo electrуnico del Administrador");
	define("ADMIN_LOGIN_FIELD", "Nombre del Administrador");
	define("ADMIN_PASS_FIELD", "Contraseсa del administrador");
	define("ADMIN_CONF_FIELD", "Confirmar contraseсa");
	define("DATETIME_SHOWN_FIELD", "Formato de hora (mostrado en el sitio)");
	define("DATE_SHOWN_FIELD", "Formato de fecha (mostrado en el sitio)");
	define("DATETIME_EDIT_FIELD", "Formato de hora (para ediciуn)");
	define("DATE_EDIT_FIELD", "El formato de la fecha (para ediciуn)");
	define("DATE_FORMAT_COLUMN", "Formato de la fecha");
	define("CURRENT_DATE_COLUMN", "Fecha de hoy");

	define("DB_LIBRARY_ERROR", "Las funciones de PHP para {db_library} no estбn definidos. Por favor, compruebe su configuraciуn de la base de datos de su archivo de configuraciуn - php.ini.");
	define("DB_CONNECT_ERROR", "No es posible conectarse con la base de datos. Por favor verifique los parбmetros de la base de datos.");
	define("INSTALL_FINISHED_ERROR", "El proceso de la instalaciуn ha terminado.");
	define("WRITE_FILE_ERROR", "No hay permiso de escritura para el archivo <b>'includes/var_definition.php'</b>. Por favor cambie el permiso de escritura del archivo antes de continuar.");
	define("WRITE_DIR_ERROR", "No hay permiso de escritura para entrar en la carpeta  <b>'includes/'</b>. Por favor cambie el permiso de la carpeta antes de continuar.");
	define("DUMP_FILE_ERROR", "El archivo de volcado '{file_name}' no se encuentra.");
	define("DB_TABLE_ERROR", "La tabla '{table_name}' no se encuentra. Por favor llene la base de datos con los datos necesarios.");
	define("TEST_DATA_ERROR", "Compruebe <b>{POPULATE_DB_FIELD}</b> Antes de rellenar las tablas con los datos de los ensayos");
	define("DB_HOST_ERROR", "El nombre del host que ha especificado no pudo ser encontrado.");
	define("DB_PORT_ERROR", "No se puede conectar al servidor de base de datos usando el puerto especificado.");
	define("DB_USER_PASS_ERROR", "El nombre de usuario o contraseсa especificada no es correcta.");
	define("DB_NAME_ERROR", "Las configuraciones de conexiуn son correctas, pero la base de datos '{db_name}' No se pudo encontrar.");

	// mensajes de actualizar
	define("UPGRADE_TITLE", "Actualizaciуn de ViArt SHOP");
	define("UPGRADE_NOTE", "Aviso: Por favor tenga en cuenta que es aconsejable hacer una copia de seguridad de la base de datos antes de proceder.");
	define("UPGRADE_AVAILABLE_MSG", "Actualizaciуn disponible");
	define("UPGRADE_BUTTON", "Actualizar a {version_number} ahora.");
	define("CURRENT_VERSION_MSG", "La versiуn instalada actualmente.");
	define("LATEST_VERSION_MSG", "La versiуn disponible para la instalaciуn.");
	define("UPGRADE_RESULTS_MSG", "Resultados de la actualizaciуn");
	define("SQL_SUCCESS_MSG", "Las instrucciones SQL han tenido йxito");
	define("SQL_FAILED_MSG", "Las instrucciones SQL han fallado");
	define("SQL_TOTAL_MSG", "Total de instrucciones SQL ejecutadas");
	define("VERSION_UPGRADED_MSG", "Su versiуn ha sido actualizada a");
	define("ALREADY_LATEST_MSG", "Usted ya posee la ъltima versiуn");
	define("DOWNLOAD_NEW_MSG", "Se detectу una nueva versiуn");
	define("DOWNLOAD_NOW_MSG", "Descargar la versiуn {version_number} ahora");
	define("DOWNLOAD_FOUND_MSG", "Hemos detectado que la nueva {version_number} Versiуn se encuentra disponible para descargar. Por favor, haga clic en el enlace para iniciar la descarga. Despuйs de completar la descarga y la sustituciуn de los archivos no se olvide de ejecutar de nuevo la rutina de actualizaciуn.");
	define("NO_XML_CONNECTION", "Advertencia! No hay conexiуn disponible a 'http://www.viart.com/' !");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>