<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// Instruksjonsmanual meldinger
	define("MANUALS_TITLE", "Instruksjonsmanualer");
	define("NO_MANUALS_MSG", "Ingen instruksjonsmanualer funnet");
	define("NO_MANUAL_ARTICLES_MSG", "Ingen artikler");
	define("MANUALS_PREV_ARTICLE", "Forrige");
	define("MANUALS_NEXT_ARTICLE", "Neste");
	define("MANUAL_CONTENT_MSG", "Indeks");
	define("MANUALS_SEARCH_IN_MSG", "Sшk i ");
	define("MANUALS_SEARCH_FOR_MSG", "Sшk ");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Alle instruksonsmanualer");
	define("MANUALS_SEARCH_RESULTS_INFO", "Har funnet {results_number} artikkel/artikler for {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Sшkeresultater");
	define("MANUALS_NOT_FOUND_ANYTHING", "Fant ingen treff for '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Ingen innhold");
	define("MANUALS_SEARCH_TITLE", "Sшk i instruksjonsmanualer");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Sшkeresultater fra instruksjonsmanualer");

?>