<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  install_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// installation messages
	define("INSTALL_TITLE", "ViArts webbutik - Installation");

	define("INSTALL_STEP_1_TITLE", "Installation: Steg 1");
	define("INSTALL_STEP_1_DESC", "Tack fцr att du har valt att installera ViArts webbutik. Fцr att komma igеng med installationen behцver du fylla i nedanstеende obligatoriska fдlt. Vдnligen se till att databasen du valt redan finns. Om du installerar till en databas som anvдnder ODBC eller MS Access, sе behцver du fцrst skapa en DSN-koppling till den innan du fortsдtter.");
	define("INSTALL_STEP_2_TITLE", "Installation: Steg 2");
	define("INSTALL_STEP_2_DESC", "");
	define("INSTALL_STEP_3_TITLE", "Installation: Steg 3");
	define("INSTALL_STEP_3_DESC", "Vдnligen vдlj en webbsides-layout. Du kan дndra layouten senare.");
	define("INSTALL_FINAL_TITLE", "Installation: Sista Steget");
	define("SELECT_DATE_TITLE", "Vдlj Datumformat");

	define("DB_SETTINGS_MSG", "Databasinstдllningar");
	define("DB_PROGRESS_MSG", "Skapar databasstrukturen");
	define("SELECT_PHP_LIB_MSG", "Vдlj PHP-bibliotek");
	define("SELECT_DB_TYPE_MSG", "Vдlj databastyp");
	define("ADMIN_SETTINGS_MSG", "Administrativa instдllningar");
	define("DATE_SETTINGS_MSG", "Datumformat");
	define("NO_DATE_FORMATS_MSG", "Inget datumformat tillgдngligt");
	define("INSTALL_FINISHED_MSG", "Grundinstallationen дr genomfцrd. Var vдnlig se till att alla instдllningar i adminstrationssektionen дr som de ska.");
	define("ACCESS_ADMIN_MSG", "Fцr att komma еt administrationen, klicka hдr");
	define("ADMIN_URL_MSG", "Administrations-URL");
	define("MANUAL_URL_MSG", "Manual-URL");
	define("THANKS_MSG", "Tack fцr att du valt <b>ViArt SHOP</b>.");

	define("DB_TYPE_FIELD", "Databastyp");
	define("DB_TYPE_DESC", "Var vдnlig vдlj vilken <b>typ av databas</b> som du anvдnder. Om du anvдnder SQL Server eller Microsoft Access, var vдnlig vдlj ODBC.");
	define("DB_PHP_LIB_FIELD", "PHP-bibliotek");
	define("DB_HOST_FIELD", "Domдn");
	define("DB_HOST_DESC", "Var vдnlig ange <b>namn</b> eller <b>IP-adress fцr servern</b> dдr din ViArt databas kommer kцras. Om du kцr din databas pе din lokala PC sе kan du troligtvis enbart lеta det stе \"<b>localhost</b>\" och lдmna fдltet fцr porten tomt. Om du anvдnder en databas hos din webhost, var vдnlig kontrollera uppgifterna fцr serverinstдllningarna med dem.");
	define("DB_PORT_FIELD", "Port");
	define("DB_NAME_FIELD", "Databasnamn / DSN");
	define("DB_NAME_DESC", "Om du anvдnder en databas som t ex MySQL eller PostgreSQL var vдnlig ange <b>namnet pе databasen</b> dдr du vill att ViArt ska skapa tabeller. Det mеste vara en befintlig databas. Om du bara installerar ViArt i prцvosyfte pе din lokala PC sе har de flesta system en \"<b>test</b>\"-databas som du kan anvдnda. Om det inte finns nеgon sеdan sе bцr du skapa en databas som du t ex dцper till \"viart\" och sedan anvдnda den. Om du anvдnder Microsoft Access eller SQL Server sе bцr databasnamnet vara <b>DSN-namnet</b> som du har angett i Data Sources (ODBC) delen av din kontrollpanel.");
	define("DB_USER_FIELD", "Anvдndarnamn");
	define("DB_PASS_FIELD", "Lцsenord");
	define("DB_USER_PASS_DESC", "<b>Anvдndarnamn</b> och <b>lцsenord</b> - var vдnlig ange anvдndarnamn och lцsenord som du vill anvдnda fцr еtkomst av databasen. Om du anvдnde ren lokal testinstallation sе дr anvдndarnamnet troligtvis \"<b>root</b>\" och lцsenordsfдltet lдmnas tomt. Det дr helt okej nдr man testar, men var vдnlig notera att det inte дr sдkert pе produktionsservrar.");
	define("DB_PERSISTENT_FIELD", "Persistent Connection");
	define("DB_PERSISTENT_DESC", "Fцr att anvдnda MySQL eller Postgre persistent connections, bocka i denna ruta. Om du inte vet vad det innebдr sе дr det sдkerligen bдst att lдmna rutan oklickad.");
	define("DB_CREATE_DB_FIELD", "Skapa databas");
	define("DB_CREATE_DB_DESC", "Fцr att om mцjligt skapa en databas, bocka i rutan. Fungerar bara fцr MySQL och Postgre");
	define("DB_POPULATE_FIELD", "Fyll databasen");
	define("DB_POPULATE_DESC", "Fцr att skapa databasens tabellstruktur och fylla den med data bocka i denna rutan.");
	define("DB_TEST_DATA_FIELD", "Testdata");
	define("DB_TEST_DATA_DESC", "Fцr att lдgga till lite testdata i din databas bocka i rutan.");
	define("ADMIN_EMAIL_FIELD", "Administrationsepost");
	define("ADMIN_LOGIN_FIELD", "Administration anvдndarnamn");
	define("ADMIN_PASS_FIELD", "Administration lцsenord");
	define("ADMIN_CONF_FIELD", "Bekrдfta lцsenord");
	define("DATETIME_SHOWN_FIELD", "Tidsformat (visad pе webbplatsen)");
	define("DATE_SHOWN_FIELD", "Datumformat (visad pе webbplatsen)");
	define("DATETIME_EDIT_FIELD", "Tidsformat (fцr дndring)");
	define("DATE_EDIT_FIELD", "Datumformat (fцr дndring)");
	define("DATE_FORMAT_COLUMN", "Datumformat");
	define("CURRENT_DATE_COLUMN", "Dagens datum");

	define("DB_LIBRARY_ERROR", "PHP-funktioner fцr {db_library} дr inte angivna. Var vдnlig kontrollera dina databasinstдllningar i din konfigurationsfil - php.ini.");
	define("DB_CONNECT_ERROR", "Kan inte koppla mot databasen. Vдnligen kolla dina databas-parametrar.");
	define("INSTALL_FINISHED_ERROR", "Installationsprocessen дr fдrdig.");
	define("WRITE_FILE_ERROR", "Har inte skrivrдttigheter till filen <b>'includes/var_definition.php'</b>. Vдnligen gцr дndringar fцr skrivrдttigheter pе filen innan du fortsдtter.");
	define("WRITE_DIR_ERROR", "Har inte skrivrдttigheter till katalogen <b>'includes/'</b>. Vдnligen дndra katalogens rдttigheter innan du fortsдtter.");
	define("DUMP_FILE_ERROR", "Dump-filen '{file_name}' hittades inte.");
	define("DB_TABLE_ERROR", "Tabellen '{table_name}' hittades inte. Vдnligen fyll databasen med nцdvдndig information.");
	define("TEST_DATA_ERROR", "Klicka i <b>{POPULATE_DB_FIELD}</b> innan du fyller tabeller med testdata");
	define("DB_HOST_ERROR", "Hostnamnet som du angav kan inte hittas.");
	define("DB_PORT_ERROR", "Kan inte ansluta till databasservern via den angivna porten.");
	define("DB_USER_PASS_ERROR", "Anvдndarnamnet eller lцsenordet som du har angett дr inte rдtt.");
	define("DB_NAME_ERROR", "Inloggningsuppgifterna var rдtt, men databasen '{db_name}' kunde inte hittas.");

	// upgrade messages
	define("UPGRADE_TITLE", "ViaArt Shop - Uppgradering");
	define("UPGRADE_NOTE", "OBS: Vдnligen tдnk pе att gцra en backup av databasen innan du fortsдtter med uppgraderingen.");
	define("UPGRADE_AVAILABLE_MSG", "Uppgradering tillgдnglig");
	define("UPGRADE_BUTTON", "Uppgradera till {version_number} nu");
	define("CURRENT_VERSION_MSG", "Din nuvarande version");
	define("LATEST_VERSION_MSG", "Version tillgдnglig att installera");
	define("UPGRADE_RESULTS_MSG", "Uppgraderingsresultat");
	define("SQL_SUCCESS_MSG", "SQL-queries - lyckade");
	define("SQL_FAILED_MSG", "SQL-queries - misslyckade");
	define("SQL_TOTAL_MSG", "Totalt antal SQL-queries kцrda");
	define("VERSION_UPGRADED_MSG", "Din version har uppgraderas till");
	define("ALREADY_LATEST_MSG", "Du har redan den senaste versionen");
	define("DOWNLOAD_NEW_MSG", "Den nya versionen hittades");
	define("DOWNLOAD_NOW_MSG", "Ladda ner version nr {version_number} nu");
	define("DOWNLOAD_FOUND_MSG", "Vi har upptдckt att den nya {version_number} versionen дr tillgдglig fцr nedladdning. Var vдnlig klicka pе lдnken nedanfцr fцr att starta nedladdningen. Efter att nedladdningen дr klar och filerna дr utbytta, glцm inte att kцra uppgraderingsrutinen igen.");
	define("NO_XML_CONNECTION", "Varning! Ingen uppkoppling till 'http://www.viart.com/' дr tillgдnglig!");

	define("END_USER_LICENSE_AGREEMENT_MSG", "End User License Agreement");
	define("AGREE_LICENSE_AGREEMENT_MSG", "I have read and agree to the License Agreement");
	define("READ_LICENSE_AGREEMENT_MSG", "Click here to read license agreement");
	define("LICENSE_AGREEMENT_ERROR", "Please read and agree to the License Agreement before proceeding.");

?>