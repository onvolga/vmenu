<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_footer.php                                         ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	$t->set_file("admin_footer", "admin_footer.html");

	$t->set_var("footer_html", get_setting_value($settings, "html_below_footer", ""));

	if ($va_version_code & 4) {
		$t->global_parse("support_link", false, false, true);
	}
	
	$t->parse("admin_footer");

?>
