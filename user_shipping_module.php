<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 Blazze modules                                   ***
  ***      File:  user_shipping_module.php                                 ***
  ***      http://hlieviy.com                                              ***
  ***                                                                      ***
  ****************************************************************************
*/


    include_once("./includes/common.php");
    include_once("./includes/record.php");
    include_once("./includes/navigator.php");
    include_once("./includes/sorter.php");
    include_once("./messages/" . $language_code . "/download_messages.php");

    check_user_session();

    $cms_page_code = "user_shipping_module";
    $script_name   = "user_shipping_module.php";
    $current_page  = get_custom_friendly_url("user_shipping_module.php");
    $auto_meta_title = "Редактирование метода доставки";

    include_once("./includes/page_layout.php");

?>