<?php

	/**
	 *
	 * Скрипт запуска списания оплаты
	 *
	 */
	
	include_once("./includes/common.php");

	// Достаем продукты заказа, не относящиеся к текущему месяцу и не списанные (наличие в таблице списанных поздразумевает списание)
	$sql = " SELECT oi.order_item_id AS s_order_item_id, oi.user_id AS s_seller_id, oi.merchant_commission AS s_merchant_commission, oi.quantity AS s_quantity";
	$sql .= " FROM " . $table_prefix . "orders_items AS oi ";
	$sql .= " JOIN " . $table_prefix . "orders AS o ON o.order_id = oi.order_id ";
	$sql .= " LEFT OUTER JOIN " . $table_prefix . "writes_off AS woff ON woff.order_item_id = oi.order_item_id ";
	$sql .= " WHERE woff.order_item_id is NULL AND IF (YEAR(o.order_placed_date) = YEAR(NOW()), MONTH(o.order_placed_date) <> MONTH(NOW()), 1)";
	
	$db->query($sql);

	// И формируем массив для списывания каждого из них	
	$for_write_off_array = array();
	
	while($db->next_record()) {
		
		$for_write_off_array[] = array(
			"order_item_id" => $db->f('s_order_item_id'), // order_item_id - id продукта заказа
			"seller_id" => $db->f('s_seller_id'), // seller_id - id продавца
			"date_write_off" => date("Y-m-d H:i:s"), // date_write_off - текущая дата, время
			"amount" => $db->f('s_merchant_commission') * $db->f('s_quantity') // amount - сумма для списания
		);
		
	}
	
	// После формирования запихиваем это в таблицу списаний
	$first_iter = true;
	$insert_sql = "INSERT INTO " . $table_prefix . "writes_off (order_item_id, seller_id, date_write_off, amount) VALUES ";	
	foreach($for_write_off_array as $item_array) {
		if(!$first_iter) { $insert_sql .= ","; } else { $first_iter = false; }
		$insert_sql .= " ('".$item_array["order_item_id"]."', '".$item_array["seller_id"]."', '".$item_array["date_write_off"]."', '".$item_array["amount"]."') ";
	}
	
	$db->query($insert_sql);
	
	// Формируем кредитные списания для каждого продавца, суммирую его комиссии
	$credits_writes_off = array();
	foreach($for_write_off_array as $item_array) {
		$credits_writes_off[$item_array["seller_id"]] += $item_array["amount"];
	}
	
	// Записываем кредитные списания
	foreach($credits_writes_off as $seller_key => $credit_amount) {
		$wroff_sql = "INSERT INTO ". $table_prefix . "users_credits (user_id, order_id, order_item_id, credit_amount, credit_action, credit_type, date_added) ";
		$wroff_sql .= " VALUES ('" . $seller_key . "', '0', '0', '" . $credit_amount . "', '-1', '3', '" . date("Y-m-d H:i:s") . "')";
		$db->query($wroff_sql);
	}