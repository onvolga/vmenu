<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//tР±mogatР±s СЊzenetek
	define("FORUM_TITLE", "FСѓrum");
	define("TOPIC_INFO_TITLE", "TР№ma InformР±ciСѓ");
	define("TOPIC_MESSAGE_TITLE", "Р¬zenet");

	define("MY_FORUM_TOPICS_MSG", "FСѓrum tР№mР±im");
	define("ALL_FORUM_TOPICS_MSG", "Р¦sszes FСѓrum TР№ma");
	define("MY_FORUM_TOPICS_DESC", "Mindig csodР±lkoztР±l ha  egy problР№mР±ra megoldР±st keresve rР±jС†ttР№l , hogy mР±snР±l is elС…fordult ez a problР№ma , Р№s tapasztalata segРЅtett neked? SzeretnР№d megosztani tudР±sod az СЉj felhasznР±lСѓkkal? MiР№rt ne lennР№l egy fСѓrum felhasznР±lСѓ becsatlakozva a kС†zС†ssР№gbe?");
	define("NEW_TOPIC_MSG", "РЄj TР№ma");
	define("NO_TOPICS_MSG", "TР№ma nem talР±lhatСѓ.");
	define("FOUND_TOPICS_MSG", " <b>{found_records}</b> tР№mР±t talР±ltunk, ami megfelel a '<b>{search_string}</b>' keresР№snek.");
	define("NO_FORUMS_MSG", "FСѓrum nem talР±lhatСѓ");

	define("FORUM_NAME_COLUMN", "FСѓrum");
	define("FORUM_TOPICS_COLUMN", "TР№mР±k");
	define("FORUM_REPLIES_COLUMN", "VР±laszok");
	define("FORUM_LAST_POST_COLUMN", "UtoljР±ra frissРЅtve");
	define("FORUM_MODERATORS_MSG", "ModerР±torok");

	define("TOPIC_NAME_COLUMN", "TР№ma");
	define("TOPIC_AUTHOR_COLUMN", "SzerzС…");
	define("TOPIC_VIEWS_COLUMN", "Megtekintve");
	define("TOPIC_REPLIES_COLUMN", "VР±laszok");
	define("TOPIC_UPDATED_COLUMN", "UtoljР±ra frissРЅtve");
	define("TOPIC_ADDED_MSG", "KС†szС†njСЊk  <br> a tР№mР±d elkР№szСЊlt");

	define("TOPIC_ADDED_BY_FIELD", "HozzР±adta");
	define("TOPIC_ADDED_DATE_FIELD", "HozzР±adva");
	define("TOPIC_UPDATED_FIELD", "UtoljР±ra frissРЅtve");
	define("TOPIC_NICKNAME_FIELD", "BecenР№v");
	define("TOPIC_EMAIL_FIELD", "Email cРЅmed");
	define("TOPIC_NAME_FIELD", "TР№ma");
	define("TOPIC_MESSAGE_FIELD", "Р¬zenet");
	define("TOPIC_NOTIFY_FIELD", "KСЊld el mindegyik vР±laszt az email cРЅmemre");

	define("ADD_TOPIC_BUTTON", "TР№ma hozzР±adР±sa");
	define("TOPIC_MESSAGE_BUTTON", "Р¬zenet hozzР±adР±sa");

	define("TOPIC_MISS_ID_ERROR", "HiР±nyzСѓ <b>Thread ID</b> paramР№ter.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> paramР№ter Р№rtР№ke nem megfelelС….");
	define("FORUM_SEARCH_MESSAGE", " {search_count} СЊzenetet van ami megfelel a  '{search_string}' keresР№si feltР№telnek.");
	define("TOPIC_PREVIEW_BUTTON", "ElС…zetes");
	define("TOPIC_SAVE_BUTTON", "MentР№s");

	define("LAST_POST_ON_SHORT_MSG", "IdС…:");
	define("LAST_POST_IN_SHORT_MSG", "TР№ma:");
	define("LAST_POST_BY_SHORT_MSG", "SzerzС…:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "UtolsСѓ hozzР±szСѓlР±s:");

?>