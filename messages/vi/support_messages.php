<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Trung tвm hф  trЅ ");
	define("SUPPORT_REQUEST_INF_TITLE", "Thфng tin yкu cв u");
	define("SUPPORT_REPLY_FORM_TITLE", "Pha n hф i");

	define("MY_SUPPORT_ISSUES_MSG", "Yкu cв u hф  trЅ  cu a tфi");
	define("MY_SUPPORT_ISSUES_DESC", "Nк u ba n gе p va i vв n рк  vЅ i nhЯ ng sa n phв m ma  ba n рa  mua, bф  phв n hф  trЅ  cu a chu ng tфi sе n sa ng giu p рЅ .");
	define("NEW_SUPPORT_REQUEST_MSG", "Yкu cв u mЅ i");
	define("SUPPORT_REQUEST_ADDED_MSG", "Ca m Ѕn ba n<br>Bф  phв n hф  trЅ  cu a chu ng tфi se  cф  gе ng phu c vu  yкu cв u cu a ba n nhanh nhв t co  thк .");
	define("SUPPORT_SELECT_DEP_MSG", "LЯ a cho n Bф  phв n.");
	define("SUPPORT_SELECT_PROD_MSG", "LЯ a cho n sa n phв m");
	define("SUPPORT_SELECT_STATUS_MSG", "LЯ a cho n ti nh tra ng");
	define("SUPPORT_NOT_VIEWED_MSG", "Khфng xem");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Xem bЅ i ngЯЅ i du ng");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Xem bЅ i ngЯЅ i qua n tri ");
	define("SUPPORT_STATUS_NEW_MSG", "MЅ i");
	define("NO_SUPPORT_REQUEST_MSG", "Khфng ti m thв y");

	define("SUPPORT_SUMMARY_COLUMN", "To m tе t");
	define("SUPPORT_TYPE_COLUMN", "Loa i");
	define("SUPPORT_UPDATED_COLUMN", "Lв n cв p nhв t cuф i");

	define("SUPPORT_USER_NAME_FIELD", "Tкn");
	define("SUPPORT_USER_EMAIL_FIELD", "Рi a chi  email cu a ba n");
	define("SUPPORT_IDENTIFIER_FIELD", "Ma  sф  ho a рЅn");
	define("SUPPORT_ENVIRONMENT_FIELD", "Ca c mф ta  (Hк  рiк u ha nh, database, ma y chu  web, etc.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Bф  phв n");
	define("SUPPORT_PRODUCT_FIELD", "Sa n phв m");
	define("SUPPORT_TYPE_FIELD", "Loa i");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Ti nh tra ng hiк n ta i");
	define("SUPPORT_SUMMARY_FIELD", "To m tе t ngе n go n");
	define("SUPPORT_DESCRIPTION_FIELD", "Mф ta ");
	define("SUPPORT_MESSAGE_FIELD", "Nф i dung");
	define("SUPPORT_ADDED_FIELD", "Thкm");
	define("SUPPORT_ADDED_BY_FIELD", "Thкm bЅ i");
	define("SUPPORT_UPDATED_FIELD", "Lв n cв p nhв t cuф i");

	define("SUPPORT_REQUEST_BUTTON", "РЯa yкu cв u");
	define("SUPPORT_REPLY_BUTTON", "Hф i рa p");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Sai tham sф <b>sф  ID hф  trЅ </b>");
	define("SUPPORT_MISS_CODE_ERROR", "Sai tham sф  <b>Xa c thЯ c</b>");
	define("SUPPORT_WRONG_ID_ERROR", "<b>ma  sф  ID hф  trЅ </b> bi  sai gia  tri ");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Xa c thЯ c</b> tham sф  bi  sai");

	define("MAIL_DATA_MSG", "DЯ  liк u mail");
	define("HEADERS_MSG", "Tiкu рк ");
	define("ORIGINAL_TEXT_MSG", "Nф i dung (text)");
	define("ORIGINAL_HTML_MSG", "Nф i dung (HTML)");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>