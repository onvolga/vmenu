<?php 
    $friendly_urls = get_setting_value($settings, "friendly_urls", 0);
	$friendly_extension = get_setting_value($settings, "friendly_extension", "");
    
    $html_template = get_setting_value($block, "html_template", "block_blz_popular.html");
    mb_internal_encoding("UTF-8");       
    $t->set_file("block_body", $html_template);
    
    $dbi = new VA_SQL();
	$dbi->DBType       = $db->DBType;
	$dbi->DBDatabase   = $db->DBDatabase;
	$dbi->DBUser       = $db->DBUser;
	$dbi->DBPassword   = $db->DBPassword;
	$dbi->DBHost       = $db->DBHost;
	$dbi->DBPort       = $db->DBPort;
	$dbi->DBPersistent = $db->DBPersistent;
    
    $stmt = "SELECT * FROM va_blz_sliders ORDER BY slider_order";

    $db->query($stmt);
    
    $tPos = 0;
    while($db->next_record()){
        $tab_name = $db->f('user_tab_name');
        $item_ids = $db->f('ids_list');
        
        if(!$tab_name || !$item_ids){
            continue;
        }
        $t->set_var("tab_name", $tab_name);
        $t->set_var("tab_pos", ++$tPos);
        $t->parse("slider_header", true);
        $stmt = "SELECT i.item_name, i.item_id, i.friendly_url, i.big_image, i.small_image, i.price, i.sales_price, i.special_offer, i.short_description FROM " . $table_prefix . "items i WHERE item_id IN (" . $item_ids . ") ORDER BY FIELD(item_id, " . $item_ids . ")";
        $dbi->query($stmt);
        $t->set_var("parent_item", '');
        $cnt = 0;
        while($dbi->next_record()){
                
            $cnt++;
            $item_id = $dbi->f('item_id');
            $item_name = $dbi->f('item_name');
 

            
            $offer_text = '';
            $price = $dbi->f('price');
            $sales_price = $dbi->f('sales_price');
            $friendly_url = $dbi->f("friendly_url");
            if($sales_price > 0){
                $price = $sales_price;
            }
            $small_image = $dbi->f("small_image");
            $big_image = $dbi->f("big_image");
            if(!$small_image){
                $small_image = $big_image;
            }

            if(strlen($small_image) && @getimagesize($small_image)) {
                //could be changed later
            }
            else {
                $small_image = "images/no_photo.gif";
            }
            if ($friendly_urls && $friendly_url) {
                $product_url = $friendly_url . $friendly_extension;
            }
            else {
                $product_url = get_custom_friendly_url("product_details.php") . "?item_id=" . $item_id;
            }
            /*prepare preview*/
            if($big_image){
                $image_offer_js = " onmousemove=\"moveSpecialOffer(event);\" onmouseover=\"popupSpecialOffer('blzItem{$item_id}', 'block');\" onmouseout=\"popupSpecialOffer('blzItem{$item_id}', 'none');\" ";
                $special_offer = $dbi->f('special_offer');
                $short_description = $dbi->f('short_description');
                if($special_offer){
                    $offer_text = $special_offer;
                }
                else{
                    $offer_text = $short_description;
                }
            }
            else{
                $image_offer_js = '';
            }
			
			if($price == "0") {
				$price = "Уточняется";
			} else {
				$price .= " р.";
			}
			
            $t->set_var("o_price", $price);
            $t->set_var("p_item_id", $item_id);          
            $t->set_var("image_offer_js", $image_offer_js);
            $t->set_var("big_image", $big_image);
            $t->set_var("offer_text", $offer_text);
            $t->set_var("special_href", $product_url);
            $t->set_var("special_image", $small_image); 
            $t->set_var("product_name", $item_name);
            
            $t->parse("blz_item_preview", true);
            $t->parse("parent_item", true);
        
            /*last item*/
        
            $t->set_var("calc_items", $cnt);
          
        }           
            
        $t->parse("slider_elem", true);
    }

    $block_parsed = true;