<?php

	$default_title = "{MERCHANT_MSG}";

	if(!isset($merchant_id)) {
		$merchant_id = get_param("user");
	}
	if(!strlen($merchant_id)) {
		if ($cms_page_code == "product_details") {
			$item_id = get_param("item_id");
			$sql  = " SELECT user_id FROM " . $table_prefix . "items ";
			$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);
			$merchant_id = get_db_value($sql);
		}
	}


	$desc_image = get_setting_value($vars, "merchant_info_image", 0);
	$desc_type = get_setting_value($vars, "merchant_info_desc_value", 0);
	$is_nickname = get_setting_value($vars, "merchant_info_nick", 0);
	$is_country_flag = get_setting_value($vars, "merchant_info_country_flag", 0);
	$is_online_status = get_setting_value($vars, "merchant_info_online", 0);
	$is_member_since = get_setting_value($vars, "merchant_info_member", 0);
	$is_prod_link = get_setting_value($vars, "merchant_info_prod_link", 0);

	// check online time
	$ctime = va_time();
	$online_time = get_setting_value($settings, "online_time", 5);
	$online_ts = mktime ($ctime[HOUR], $ctime[MINUTE] - $online_time, $ctime[SECOND], $ctime[MONTH], $ctime[DAY], $ctime[YEAR]);

//custom merchant data
$show_rating = get_setting_value($vars, "show_rating", 0);
$show_site = get_setting_value($vars, "show_site", 0);
$show_city = get_setting_value($vars, "show_city", 0);
$showOrderMinimal = get_setting_value($vars, "merchant_order_minimal", 0);
$show_contact_phone = get_setting_value($vars, "show_contact_phone", 0);
$show_icq_number = get_setting_value($vars, "show_icq_number", 0);
$show_ss = get_setting_value($vars, "show_ss", 0);
$show_ps = get_setting_value($vars, "show_ps", 0);

$show_shipping_condition = get_setting_value($vars, "show_shipping_condition", 0);
$show_work_with = get_setting_value($vars, "show_work_with", 0);
$show_warranty = get_setting_value($vars, "show_warranty", 0);
$show_remark = get_setting_value($vars, "show_remark", 0);
$show_legal = get_setting_value($vars, "show_legal", 0);
	//if (!isset($merchant_info) || !is_array($merchant_info) || !sizeof($merchant_info)) {
		$merchant_info = array();
		$sql  = " SELECT * FROM ". $table_prefix . "users "; 
		$sql .= " WHERE user_id=" . $db->tosql($merchant_id, INTEGER);
		$db->query($sql);
		if ($db->next_record()) {
			//var_dump($db->Record);
			$merchant_info = $db->Record;
			$merchant_info["registration_date"] = $db->f("registration_date", DATETIME);
			// check last visit date
			$last_visit_ts = 0;
			$last_visit_date = $db->f("last_visit_date", DATETIME);
			if (is_array($last_visit_date)) {
				$last_visit_ts = va_timestamp($last_visit_date);
			}
			$merchant_info["last_visit_ts"] = $last_visit_ts;
		}
	//}

	if (sizeof($merchant_info)) {


	  $t->set_file("block_body", "block_merchant_info.html");
		// check name
		$merchant_name = $merchant_info["nickname"];
		if (!strlen($merchant_name)) {
			$merchant_name = get_translation($merchant_info["company_name"]);
		}
		if (!strlen($merchant_name)) {
			$merchant_name = $merchant_info["name"];
		}
		if (!strlen($merchant_name)) {
			$merchant_name = trim($merchant_info["first_name"]." ".$merchant_info["last_name"]);
		}
		if (!strlen($merchant_name)) {
			$merchant_name = $merchant_info["login"];
		}
	
		// parse personal image
		$personal_image = $merchant_info["personal_image"];
		if ($desc_image && $personal_image) {
			if (preg_match("/^http\:\/\//", $personal_image)) {
				$image_size = "";
			} else {
				$image_size = @GetImageSize($personal_image);
			}
			if (is_array($image_size)) {
				$t->set_var("image_size", $image_size[3]);
			} else {
				$t->set_var("image_size", "");
			}
			$t->set_var("alt", htmlspecialchars($merchant_name));
			$t->set_var("personal_image_src", htmlspecialchars($personal_image));

			$t->sparse("personal_image_block", false);
		} else {
			$t->set_var("personal_image_block", "");
		}

		$merchant_description = "";
		if ($desc_type == 1) {
			$merchant_description = $merchant_info["short_description"];
		} else if ($desc_type == 2) {
			$merchant_description = $merchant_info["full_description"];
		}
		if ($merchant_description) {
			$t->set_var("merchant_description", $merchant_description);
			$t->sparse("merchant_description_block", false);
		} else {
			$t->set_var("merchant_description_block", "");
		}

		$t->set_var("country_block", "");
		$t->set_var("nickname_country_block", "");
		if ($is_country_flag) {
			$country_code = $merchant_info["country_code"];
			$country_image_src = "images/flags/".strtolower($country_code).".gif";
			if (file_exists($country_image_src)) {
				$image_size = @GetImageSize($country_image_src);
				if (is_array($image_size)) {
					$t->set_var("image_size", $image_size[3]);
				} else {
					$t->set_var("image_size", "");
				}
				$t->set_var("alt", htmlspecialchars($country_code));
				$t->set_var("country_image_src", $country_image_src);
				if ($is_nickname) {
					$t->sparse("nickname_country_block", false);
				} else {
					$t->sparse("country_block", false);
				}
			} 
		}
		$t->set_var("nickname", $merchant_name);
		if ($is_nickname) {
			$t->set_var("nickname", $merchant_name);
			$t->sparse("nickname_block", false);
		} else {
			$t->set_var("nickname_block", "");
		}
        
//city
        $merchant_city = $merchant_info['city'];
        if($merchant_city && $show_city == 1){
                $t->set_var("merchant_city", $merchant_city);
                $t->sparse("merchant_city_block", false);
        } else {
                $t->set_var("merchant_city_block", "");
        }
//user_site
        $merchant_site = $merchant_info['user_site'];
        if($merchant_site && $show_site == 1){
                $t->set_var("merchant_site", $merchant_site);
                $t->sparse("merchant_site_block", false);
        } else {
                $t->set_var("merchant_site_block", "");
        }
//minimal_order        
        $minimal_order = $merchant_info['minimal_order'];
        if($minimal_order && $showOrderMinimal == 1){
                $t->set_var("minimal_order", $minimal_order);
                $t->sparse("minimal_order_block", false);
        } else {
                $t->set_var("minimal_order_block", "");
        }
//minimal order free shipping        
        $user_shipping_minimal = $merchant_info['user_shipping_minimal'];
        if($user_shipping_minimal && $showOrderMinimal == 1){
                $t->set_var("user_shipping_minimal", $user_shipping_minimal);
                $t->sparse("minimal_order_shipping_block", false);
        } else {
                $t->set_var("minimal_order_shipping_block", "");
        }
        
//phone
        $daytime_phone = $merchant_info['daytime_phone'];
        if($daytime_phone && $show_contact_phone == 1){
                $t->set_var("daytime_phone", $daytime_phone);
                $t->sparse("daytime_phone_block", false);
        } else {
                $t->set_var("daytime_phone_block", "");
        }
//icq number      
        $icq_number = $merchant_info['icq_number'];
        if($icq_number && $show_icq_number == 1){
                $t->set_var("icq_number", $icq_number);
                $t->sparse("icq_number_block", false);
        } else {
                $t->set_var("icq_number_block", "");
        }
//shippings
        $ss_record = $merchant_info['ss_record'];
		// echo "<pre>"; print_r($merchant_info); echo "</pre>";
        if($ss_record && $show_ss == 1){
        		$stmt = "select shipping_module_name from va_shipping_modules where shipping_module_id in (" . $ss_record . ")";				
        		$db->query($stmt);
        		while($db->next_record()){
        			$t->set_var("shipping_name", $db->f("shipping_module_name"));
        			$t->parse("ss_module_name", true);
        		}
                
                $t->sparse("ss_record_block", false);
        } else {
                $t->set_var("ss_record_block", "");
        }
//shipping condition

		$free_shipping_condition = $merchant_info['free_shipping_condition'];
        if($free_shipping_condition && $show_shipping_condition == 1){
                $t->set_var("free_shipping_condition", $free_shipping_condition);
                $t->sparse("free_shipping_condition_block", false);
        } else {
                $t->set_var("free_shipping_condition_block", "");
        }

//payments systems
        $ps_record = $merchant_info['ps_record'];
        if($ps_record && $show_ps == 1){
        		$stmt = "select payment_name, user_payment_name from va_payment_systems where payment_id in (" . $ps_record . ")";
        		$db->query($stmt);
        		while($db->next_record()){
        			$ups_name = $db->f("user_payment_name");
					$ps_name = $db->f("payment_name");
        			$t->set_var("payment_name", strlen($ups_name) ? $ups_name : $ps_name);
        			$t->parse("ps_module_name", true);
        		}
                
                $t->sparse("ps_record_block", false);
        } else {
                $t->set_var("ps_record_block", "");
        }
//work with        
        $work_with_private = $merchant_info['work_with_private'];
        $work_with_companies = $merchant_info['work_with_companies'];
        if(($work_with_private || $work_with_companies) && $show_work_with == 1){
                if($work_with_private == 1){
                	$t->sparse("work_with_private_block", false);
                }
                if($work_with_companies == 1){
                	$t->sparse("work_with_companies_block", false);
                }
                $t->sparse("work_with_block", false);
        } else {
                $t->set_var("work_with_block", "");
        }

//show warranty       
        $warranty_and_returning = $merchant_info['warranty_and_returning'];
        if($warranty_and_returning && $show_warranty == 1){
                 $t->set_var("warranty_and_returning", $warranty_and_returning);
                $t->sparse("warranty_and_returning_block", false);
        } else {
                $t->set_var("warranty_and_returning_block", "");
        }
//remark
		$remark = $merchant_info['remark'];
        if($remark && $show_remark == 1){
                 $t->set_var("remark", $remark);
                $t->sparse("remark_block", false);
        } else {
                $t->set_var("remark_block", "");
        }

//show legal       
        $ogrn = $merchant_info['ogrn'];
        $legal_name = $merchant_info['legal_name'];
        $legal_address = $merchant_info['legal_address'];
        $tax_id = $merchant_info['tax_id'];
        
        if(($ogrn || $legal_name || $legal_address || $tax_id) && $show_legal == 1){
        		$html_data = "";
        		if($legal_name){
        			$html_data .= "Юридическое название: <b>" . $legal_name . "</b><br /><br />";
        		}
        		if($ogrn){
        			$html_data .= "ОГРН/ОГРНИП: <b>" . $ogrn . "</b><br /><br />";
        		}
        		if($legal_address){
        			$html_data .= "Юридический адрес: <b>" . $legal_address . "</b><br /><br />";
        		}
        		if($tax_id){
        			$html_data .= "ИНН: <b>" . $tax_id . "</b><br />";
        		}
                $t->set_var("legal_combained", $html_data);
                $t->sparse("legal_block", false);
        } else {
                $t->set_var("legal_block", "");
        }
//merchant rating
        if($show_rating){
	        $sql = " SELECT COUNT(*) FROM " . $table_prefix . "user_reviews WHERE approved=1 AND rating <> 0 AND merchant_id=" . $db->tosql($merchant_id, INTEGER);
			$total_rating_votes = get_db_value($sql);

			$average_rating_float = 0;
			$total_rating_sum = 0;
			if($total_rating_votes)
			{
				$sql = " SELECT SUM(rating) FROM " . $table_prefix . "user_reviews WHERE approved=1 AND rating <> 0 AND merchant_id=" . $db->tosql($merchant_id, INTEGER);
				$total_rating_sum = get_db_value($sql);
				$average_rating_float = round($total_rating_sum / $total_rating_votes, 2);
			}
			$t->set_var('merchant_id', $merchant_id);
		    $average_rating = round($average_rating_float, 0);
			$average_rating_image = $average_rating ? "rating-" . $average_rating : "not-rated";
			$t->set_var("average_rating_image", $average_rating_image);
			$t->set_var("average_rating_alt", $average_rating_float);
			$t->parse('merchant_rating_block', false);
			if($average_rating){
				$t->parse('merchant_links_view', false);
				$t->parse('merchant_links_add', false);
			}
			else{
				$t->parse('merchant_links_add', false);
			}
		} else {
			$t->set_var('merchant_links_view', false);
			$t->set_var('merchant_links_add', false);
			$t->set_var('merchant_rating_block', false);
		}
        
		if ($is_member_since) {
			$registration_date = $merchant_info["registration_date"];
			$t->set_var("member_since", va_date($date_show_format, $registration_date));
			$t->sparse("member_since_block", false);
		} else {
			$t->set_var("member_since_block", "");
		}

		if ($is_online_status) {
			$last_visit_ts = $merchant_info["last_visit_ts"];
			if ($last_visit_ts >= $online_ts) {
				$t->set_var("online_status", "<font color=\"green\">".ONLINE_MSG."</font>");
			} else {
				$t->set_var("online_status", "<font color=\"silver\">".OFFLINE_MSG."</font>");
			}
			$t->sparse("online_status_block", false);
		} else {
			$t->set_var("online_status_block", "");
		}
		
		if ($is_prod_link) {
			$friendly_urls      = get_setting_value($settings, "friendly_urls", 0);
			$friendly_extension = get_setting_value($settings, "friendly_extension", "");
			$friendly_url = $merchant_info["friendly_url"];
			if ($friendly_urls && strlen($friendly_url)) {
				$merchant_products_url = $friendly_url.$friendly_extension;
			} else {
				$merchant_products_url = "user_list.php?user=".$merchant_id;
			}
			$t->set_var("merchant_products_url", $merchant_products_url);
			$t->sparse("prod_link_block", false);
		} else {
			$t->set_var("prod_link_block", "");
		}

		$block_parsed = true;
	}

?>