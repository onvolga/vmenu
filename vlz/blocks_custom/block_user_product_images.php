<?php

	$default_title = "Изображения продукта";

	check_user_security("access_products");

	$item_id = get_param("item_id");
	$sql  = " SELECT item_name FROM " . $table_prefix . "items ";
	$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);
	$sql .= " AND user_id=" . $db->tosql(get_session("session_user_id"), INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$item_name = get_translation($db->f("item_name"));
	} else {
		$item_id = "";
	}

/*check access*/
$sesUsrType = get_session("session_user_type_id");
$sesUsr = get_session("session_user_id");

$allow_images = get_db_value("select setting_value from va_user_types_settings uts inner join va_users u on u.user_type_id = uts.type_id where u.user_type_id = " . $sesUsrType . " AND user_id=" . $sesUsr . " AND setting_name = 'access_product_images'");

	// get product settings
	$setting_type = "user_product_" . get_session("session_user_type_id");
	$product_settings = array();
	$sql  = " SELECT setting_name,setting_value FROM " . $table_prefix . "global_settings ";
	$sql .= " WHERE setting_type=" . $db->tosql($setting_type, TEXT);
	if (isset($site_id)) {
		$sql .= " AND (site_id=1 OR site_id=" . $db->tosql($site_id, INTEGER, true, false) . ")";
		$sql .= " ORDER BY site_id ASC ";
	} else {
		$sql .= " AND site_id=1 ";
	}
	$db->query($sql);
	while($db->next_record()) {
		$product_settings[$db->f("setting_name")] = $db->f("setting_value");
	}

	if (!$item_id || !$allow_images) {
		header("Location: " . get_custom_friendly_url("user_products.php"));
		exit;
	}

	$html_template = get_setting_value($block, "html_template", "block_user_product_images.html"); 
  $t->set_file("block_body", $html_template);
	$t->set_var("user_home_href",  	get_custom_friendly_url("user_home.php"));
	$t->set_var("user_products_href",  get_custom_friendly_url("user_products.php"));
	$t->set_var("user_product_href",   get_custom_friendly_url("user_product.php"));
	$t->set_var("item_image_href",   get_custom_friendly_url("user_product_image.php"));
	

	$t->set_var("item_id", $item_id);
	$t->set_var("item_name", htmlspecialchars($item_name));

	$s = new VA_Sorter($settings["admin_templates_dir"], "sorter_img.html", get_custom_friendly_url("user_product_images.php"));
	$s->set_sorter(ID_MSG, "sorter_image_id", "1", "image_id");
	$s->set_sorter(IMAGE_TITLE_MSG, "sorter_image_title", "2", "image_title");

	$n = new VA_Navigator($settings["templates_dir"], "navigator.html", get_custom_friendly_url("user_product_images.php"));

	// set up variables for navigator
	$sql  = " SELECT COUNT(*) FROM " . $table_prefix . "items_images ";
	$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);

	$db->query($sql);
	$db->next_record();
	$total_records = $db->f(0);
	$records_per_page = 20;
	$pages_number = 10;

	$page_number = $n->set_navigator("navigator", "page", SIMPLE, $pages_number, $records_per_page, $total_records, false);
	$db->RecordsPerPage = $records_per_page;
	$db->PageNumber = $page_number;
	
	$sql  = " SELECT * FROM " . $table_prefix . "items_images ";
	$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);
	$sql .= $s->order_by;
	$db->query($sql);
	if($db->next_record())
	{

		$t->parse("sorters", false);
		$t->set_var("no_records", "");
		do {
			$t->set_var("image_id", $db->f("image_id"));
			$t->set_var("image_title", $db->f("image_title"));


			$t->parse("records", true);
		} while($db->next_record());
	} else {
		$t->set_var("records", "");
		$t->set_var("navigator", "");
		$t->parse("no_records", false);
	}

	if ($allow_images) {
		$t->parse("add_item", false);
	}

	$block_parsed = true;

?>
