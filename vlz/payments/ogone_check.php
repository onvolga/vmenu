<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.8                                                ***
  ***      File:  ogone_check.php                                          ***
  ***      Built: Tue Sep 13 18:30:31 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

/*
 * oGone (http://ogone.com) transaction handler by www.viart.com
 */
	$root_folder_path = "./";
	include_once ($root_folder_path ."payments/ogone_functions.php");

	if ($order_id == get_param("orderID")) {
		checkOrder();
	} else {
		$error_message .= "oGone: No order found";
	}
	
?>