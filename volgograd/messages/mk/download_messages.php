<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Грешни параметри на симнување/downlodiranje");
	define("DOWNLOAD_MISS_PARAM", "Недостасуваат параметри на симнување/downlodiranje");
	define("DOWNLOAD_INACTIVE", "Download неактивен");
	define("DOWNLOAD_EXPIRED", "Вашиот период за download e поминат ");
	define("DOWNLOAD_LIMITED", "Реализиравте максимален број на downlodiranja");
	define("DOWNLOAD_PATH_ERROR", "Патеката кон производот не може да се најде");
	define("DOWNLOAD_RELEASE_ERROR", "Изданието не е најдено");
	define("DOWNLOAD_USER_ERROR", "Оваа датотека е дозволена само за регистрирани корисници");
	define("ACTIVATION_OPTIONS_MSG", "Активирање на опција");
	define("ACTIVATION_MAX_NUMBER_MSG", "Максимален број на активирања");
	define("DOWNLOAD_OPTIONS_MSG", "Опции за превземање/downlodiranje ");
	define("DOWNLOADABLE_MSG", "Downloadable (Software)");
	define("DOWNLOADABLE_DESC", "За производите кои се  симнуваат може да наведете  'Download Period', 'Path to Downloadable File' и  'Activations Options'");
	define("DOWNLOAD_PERIOD_MSG", "Период за превземање");
	define("DOWNLOAD_PATH_MSG", "Патека кон датотека за превземање");
	define("DOWNLOAD_PATH_DESC", "може да поставите повеќе патишта разделени со запирка");
	define("UPLOAD_SELECT_MSG", "Селектирајте дадотека за пренос/Upload и протиснете  {button_name}");
	define("UPLOADED_FILE_MSG", "Датотеката  <b>{filename}</b> е префрлена.");
	define("UPLOAD_SELECT_ERROR", "Ве молиме прво селектирајте датотека");
	define("UPLOAD_IMAGE_ERROR", "Дозволен е пренос само на слики");
	define("UPLOAD_FORMAT_ERROR", "Овој тип на датотека не е дозволен");
	define("UPLOAD_SIZE_ERROR", "Датотеки поголеми од {filesize} не се дозволени.");
	define("UPLOAD_DIMENSION_ERROR", "Слики поголеми од {dimension} не се дозволени");
	define("UPLOAD_CREATE_ERROR", "Системот неможе да креира датотека");
	define("UPLOAD_ACCESS_ERROR", "Вие немате дозвола за пренос/upload датотека");
	define("DELETE_FILE_CONFIRM_MSG", "Навистина би ја избришале оваа датотека?");
	define("NO_FILES_MSG", "Ниедна датотека не е најдена");
	define("SERIAL_GENERATE_MSG", "Генерирање сериски број");
	define("SERIAL_DONT_GENERATE_MSG", "не генерира");
	define("SERIAL_RANDOM_GENERATE_MSG", "генерирање случаен сериски број за software производ");
	define("SERIAL_FROM_PREDEFINED_MSG", "земете сериски број од преддефинирани броеви");
	define("SERIAL_PREDEFINED_MSG", "Претходно дефинирани сериски броеви");
	define("SERIAL_NUMBER_COLUMN", "Сериски број");
	define("SERIAL_USED_COLUMN", "Употребен");
	define("SERIAL_DELETE_COLUMN", "Избриши");
	define("SERIAL_MORE_MSG", "Дадади повеќе сериски броеви?");
	define("SERIAL_PERIOD_MSG", "Период на важење сериски број");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Прикажи термини и услови");
	define("DOWNLOAD_SHOW_TERMS_DESC", "За да превземете дадотека корисникот мора да се сложи со термините и условите");
	define("DOWNLOAD_TERMS_MSG", "Термини и услови");
	define("DOWNLOAD_TERMS_USER_DESC", "Ги прочитав и се сложувам со термините и условите");
	define("DOWNLOAD_TERMS_USER_ERROR", "За да превземете дадотека вие мора да се сложите со термините и условите");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>