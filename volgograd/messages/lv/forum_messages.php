<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Forums");
	define("TOPIC_INFO_TITLE", "Tзmas apraksts");
	define("TOPIC_MESSAGE_TITLE", "Ziтa");

	define("MY_FORUM_TOPICS_MSG", "Manas foruma tзmas");
	define("ALL_FORUM_TOPICS_MSG", "Visas foruma tзmas");
	define("MY_FORUM_TOPICS_DESC", "Vai esat kвdreiz domвjis, ka kвds ir saskвries ar Jыsu esoрo problзmu. Vai vзlaties dalоties ar savu pieredzi ar jaunajiem lietotвjiem? Ja jв, tad kвpзc gan nekпыt par reмistrзtu foruma lietotвju un pievienoties pвrзjiem?");
	define("NEW_TOPIC_MSG", "Jauna tзma");
	define("NO_TOPICS_MSG", "Nav atrasta neviena tзma");
	define("FOUND_TOPICS_MSG", "Esam atraduрi<b>{found_records}</b> atbilstoрus foruma tзmas terminus '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "Forums");
	define("FORUM_TOPICS_COLUMN", "Tзma");
	define("FORUM_REPLIES_COLUMN", "Atbildes");
	define("FORUM_LAST_POST_COLUMN", "Pзdзjo reizi");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "Tзma");
	define("TOPIC_AUTHOR_COLUMN", "Autors");
	define("TOPIC_VIEWS_COLUMN", "Skatоts");
	define("TOPIC_REPLIES_COLUMN", "Atbildes");
	define("TOPIC_UPDATED_COLUMN", "Pзdзjo reizi");
	define("TOPIC_ADDED_MSG", "Paldies!<br>Jыsu tзma ir pievienota.");

	define("TOPIC_ADDED_BY_FIELD", "Pievienoja");
	define("TOPIC_ADDED_DATE_FIELD", "Pievienots");
	define("TOPIC_UPDATED_FIELD", "Pзdзjo reizi atjaunots");
	define("TOPIC_NICKNAME_FIELD", "Jыsu iesauka");
	define("TOPIC_EMAIL_FIELD", "Jыsu e-pasta adrese");
	define("TOPIC_NAME_FIELD", "Tзma");
	define("TOPIC_MESSAGE_FIELD", "Ziтa");
	define("TOPIC_NOTIFY_FIELD", "Sыtоt visas atbildes uz manu e-pastu");

	define("ADD_TOPIC_BUTTON", "Pievienot tзmu");
	define("TOPIC_MESSAGE_BUTTON", "Pievienot ziтu");

	define("TOPIC_MISS_ID_ERROR", "Trыkst <b>Nosacоjuma ID</b> parametrs.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Nosacоjuma</b> parametrs nav ievadоts pareizi.");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>