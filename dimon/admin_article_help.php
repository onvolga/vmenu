<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_article_help.php                                   ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once("./admin_common.php");

	check_admin_security("ads");

	$type = get_param("type");
	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main", "admin_article_help.html");
	$t->show_tags = true;
	if ($type == "tell_friend") {
		$t->parse("tell_friend_block", false);
	} else {
		$t->set_var("tell_friend_block", "");
	}

	$t->pparse("main");

?>
