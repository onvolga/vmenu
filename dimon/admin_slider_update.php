<?php

    /**
     *
     *slider upgrade script
     *
     */
     
	include_once("./admin_config.php");

	include_once($root_folder_path . "includes/common.php");

	include_once("./admin_common.php");

	include_once($root_folder_path . "includes/record.php");



	check_admin_security("db_management");
    

//$items_per_slide = settings(items_per_slide)
//control_padding
    $sql = " SELECT block_id FROM ".$table_prefix."cms_blocks WHERE block_code='sliders'"; 
    $block_id = get_db_value($sql);

    $sql = " SELECT property_id FROM ".$table_prefix."cms_blocks_properties WHERE variable_name='slider_type' AND block_id=" . $block_id; 
	$property_id = get_db_value($sql);

	$sql = " SELECT MAX(value_id) FROM " . $table_prefix . "cms_blocks_values "; 
	$value_id = get_db_value($sql);
    $value_id += 1;
    $sql = " SELECT MAX(value_order) FROM ".$table_prefix."cms_blocks_values WHERE property_id=" . $property_id;  
    $value_order = get_db_value($sql);
    $value_order += 1;
    
    $sql = "INSERT INTO ".$table_prefix."cms_blocks_values (value_id,property_id,value_order,value_name,variable_name,variable_value,hide_value,is_default_value) VALUES ($value_id, $property_id, $value_order, '{CAROUSEL_SLIDER_MSG}', NULL, '7', 0, 0)";
    $db->query($sql);
    
    $sql = "INSERT INTO " .$table_prefix."cms_blocks_properties (block_id,property_order,property_name,control_type,parent_property_id,parent_value_id,variable_name,default_value,required,property_style,control_style,start_html,middle_html,before_control_html,after_control_html,end_html,control_code,onchange_code,onclick_code) VALUES ($block_id, 7, 'ITEMS_PER_SLIDE_MSG', 'TEXTBOX', NULL, NULL, 'items_per_slide', '4', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)";
    $db->query($sql);
    
    $sql = "INSERT INTO ".$table_prefix."cms_blocks_properties (block_id,property_order,property_name,control_type,parent_property_id,parent_value_id,variable_name,default_value,required,end_html) VALUES ($block_id, 6, 'CONTROL_PADDING_MSG' , 'CHECKBOX' , NULL , NULL , 'control_padding' , NULL , 0 , NULL)";
	$db->query($sql);
	
	echo 'slider data has been updated<br /><br />';
	echo "<a href=\"" . $site_url . get_admin_dir() . "\">return to cms</a>";
?>