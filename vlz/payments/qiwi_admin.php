<?php

	$qiwi_login	   = "логин";
	$qiwi_password = "пароль";
	
	// Коды завершения
	$qiwi_end_codes = array(
		"0"   => "Счет выставлен успешно.",
		"13"  => "Сервер занят, повторите запрос позже.",
		"150" => "Ошибка авторизации (неверный логин/пароль).",
		"210" => "Счет не найден.",
		"215" => "Счет с таким номером заказа (txn_id) уже существует.",
		"241" => "Сумма слишком мала.",
		"242" => "Превышена максимальная сумма платежа – 15000 руб.",
		"278" => "Превышение максимального интервала получения списка счетов.",
		"298" => "Агента не существует в системе.",
		"300" => "Неизвестная ошибка.",
		"330" => "Ошибка шифрования.",
		"370" => "Превышено максимальное количество одновременно выполняемых запросов."
	);
	
	// Статус счета
	$qiwi_order_status = array(
		"50"  => "Выставлен",
		"52"  => "Проводится",
		"60"  => "Оплачен",
		"150" => "Отменен (ошибка на терминале)",
		"151" => "Отменен (ошибка авторизации, недостаточно средств на балансе, отклонен абонентом при оплате с лицевого счета оператора сотовой связи и т.п.).",
		"160" => "Отменен",
		"161" => "Отменен (истекло время)"
	)
	
?>