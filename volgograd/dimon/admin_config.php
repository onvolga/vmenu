<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                 ***
  ***      File:  admin_config.php                                         ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	$is_admin_path = true; // use admin path to the root of the web folder
	$root_folder_path = "../";
	$tracking_ignore = true; // if it set to true ignoring statistics for such pages

	define("WHERE_DB_FIELD",   1);
	define("USUAL_DB_FIELD",   2);
	define("FOREIGN_DB_FIELD", 3);
	define("HIDE_DB_FIELD",    4);
	define("RELATED_DB_FIELD", 5);
	define("CUSTOM_FIELD",     6);

	$site_id  = 1;
	/**
	 * Compare two versions in string format. Returns 1 (if first is bigger), 2 (if second), or 0 if equal
	 *
	 * @param string $version1
	 * @param string $version2
	 * @return integer
	 */	
	 
	 
	function update_rating($table_name, $column_name, $review_id)
	{
		global $db;
		global $table_prefix;

		$sql = "SELECT " . $column_name . " FROM " . $table_name . " WHERE review_id=" . $db->tosql($review_id, INTEGER);		
		$column_id = get_db_value($sql);

		$sql = " SELECT COUNT(*) FROM " . $table_name . " WHERE approved=1 AND rating <> 0 AND " . $column_name . "=" . $db->tosql($column_id, INTEGER);
		$total_rating_votes = get_db_value($sql);

		$sql = " SELECT SUM(rating) FROM " . $table_name . " WHERE approved=1 AND rating <> 0 AND " . $column_name . "=" . $db->tosql($column_id, INTEGER);
		$total_rating_sum = get_db_value($sql);
		if(!strlen($total_rating_sum)) $total_rating_sum = 0;

		$average_rating = $total_rating_votes ? $total_rating_sum / $total_rating_votes : 0;

		if ($column_name == "item_id") {
			$sql  = " UPDATE " . $table_prefix . "items ";
			$sql .= " SET votes=" . $total_rating_votes . ", points=" . $total_rating_sum . ", ";
			$sql .= " rating=" . $average_rating;
			$sql .= " WHERE item_id=" . $db->tosql($column_id, INTEGER);
			$db->query($sql);
		} else {
			$sql  = " UPDATE " . $table_prefix . "articles ";
			$sql .= " SET total_votes=" . $total_rating_votes . ", total_points=" . $total_rating_sum . ", ";
			$sql .= " rating=" . $average_rating;
			$sql .= " WHERE article_id=" . $db->tosql($column_id, INTEGER);
			$db->query($sql);
		}
	}
	 
	 
	function delete_tickets($support_id)
	{
		global $db, $table_prefix;
		
		// delete attachments if available
		$sql = "SELECT file_path FROM " . $table_prefix . "support_attachments WHERE support_id=" . $db->tosql($support_id, INTEGER);
		$db->query($sql);
		while ($db->next_record()) {
			$file_path = $db->f("file_path");
			@unlink($file_path);
		}
		
		$db->query("DELETE FROM " . $table_prefix . "support_attachments WHERE support_id=" . $db->tosql($support_id, INTEGER));
		$db->query("DELETE FROM " . $table_prefix . "support_messages WHERE support_id=" . $db->tosql($support_id, INTEGER));
		$db->query("DELETE FROM " . $table_prefix . "support WHERE support_id=" . $db->tosql($support_id, INTEGER));
	}

	/**
	 * Return array with permissions for the Privilege Group of currently logged administrator
	 *
	 * @param void
	 * @return array
	 */	
	function get_permissions() 
	{
		global $db, $table_prefix;

		$permissions = array();
		$privilege_id = get_session("session_admin_privilege_id");
		$sql  = " SELECT block_name, permission FROM " . $table_prefix . "admin_privileges_settings ";
		$sql .= " WHERE privilege_id=" . $db->tosql($privilege_id, INTEGER, true, false);
		$db->query($sql);
		while($db->next_record()) {
			$block_name = $db->f("block_name");
			$permissions[$block_name] = $db->f("permission");
		}
		
		return $permissions;
	}

	/**
	 * Delete users with ids separated by comma
	 *
	 * @param string $user_ids
	 * @return void
	 */	
	function delete_users($users_ids) 
	{
		global $db, $table_prefix;
		$db->query("DELETE FROM " . $table_prefix . "users WHERE user_id IN (" . $db->tosql($users_ids, TEXT, false) . ")");
	}

	/**
	 * Return folder name of administrative scripts
	 *
	 * @param void
	 * @return string
	 */	
	function get_admin_dir()
	{
		$admin_folder = "";
		$request_uri = get_request_uri();
		$request_uri = preg_replace("/\/+/", "/", $request_uri);
		if (strpos($request_uri,"?")){
			$request_uri = substr($request_uri,0,strpos($request_uri,"?"));
		}
		$slash_position = strrpos($request_uri, "/");
		
		if ($slash_position !== false) {
			$request_path = substr($request_uri, 0, $slash_position);
			$slash_position = strrpos($request_path, "/");
			if ($slash_position !== false) {
				$admin_folder = substr($request_path, $slash_position + 1);
			}
		}
		if (strlen($admin_folder)) {
			$admin_folder .= "/";
		} else {
			$admin_folder = "admin/";
		}
		
		return $admin_folder;
	}


function parse_admin_tabs($tabs, $current_tab, $tabs_in_row = 10)
{
	global $t;
	$tab_row = 0; $tab_number = 0; $active_tab = false;

	foreach ($tabs as $tab_name => $tab_info) {
		$tab_title = $tab_info["title"];
		$tab_show = isset($tab_info["show"]) ? $tab_info["show"] : true;
		if ($tab_show) {
			$tab_number++;
			$t->set_var("tab_id", "tab_" . $tab_name);
			$t->set_var("tab_name", $tab_name);
			$t->set_var("tab_title", $tab_title);
			if ($tab_name == $current_tab) {
				$active_tab = true;
				$t->set_var("tab_class", "adminTabActive");
				$t->set_var($tab_name . "_style", "display: block;");
			} else {
				$t->set_var("tab_class", "adminTab");
				$t->set_var($tab_name . "_style", "display: none;");
			}
			$t->parse("tabs", true);
			if ($tab_number % $tabs_in_row == 0) {
				$tab_row++;
				$t->set_var("row_id", "tab_row_" . $tab_row);
				if ($active_tab) {
					$t->rparse("tabs_rows", true);
				} else {
					$t->parse("tabs_rows", true);
				}
				$t->set_var("tabs", "");
			}
		} else {
			// hide all related blocks in case if tab hidden
			$t->set_var($tab_name . "_style", "display: none;");
		}
	}
	if ($tab_number % $tabs_in_row != 0) {
		$tab_row++;
		$t->set_var("row_id", "tab_row_" . $tab_row);
		if ($active_tab) {
			$t->rparse("tabs_rows", true);
		} else {
			$t->parse("tabs_rows", true);
		}
	}
	$t->set_var("current_tab", $current_tab);
	$t->set_var("tab", $current_tab);
}

	/**
	 *convert incoming string to parsed wysiwyg editors for admin pages
	 *@var string $editors_list string containing editors separated with commas
	 *@var int $shown_type based on Administrator HTML Editor CMS setting
	 *@return void
	 */	
	function add_html_editors($editors_list, $shown_type)
	{
		global $t;
		static $CKEditor;
		$editors_array = array();
		if($shown_type == 2 && !isset($CKEditor)) {
			if(@is_file('../ckeditor/ckeditor.js')) {
				$t->set_var("CKEditor_tag", '<script src="../ckeditor/ckeditor.js" type="text/javascript"></script>');
				$CKEditor = 2;
			}
			else {
				$t->set_var("CKEditor_tag", "");
				$CKEditor = 1;
				$shown_type = 0;
			}
		}
		$editors_array = explode(',', $editors_list);
		if($shown_type == 1) {
			foreach($editors_array as $editor){
				$t->set_var($editor . "_ext_editor", "");
				$t->parse($editor . "_int_editor", false);
			}		
		}
		elseif($shown_type == 2 && $CKEditor === 2) {
			foreach($editors_array as $editor){
				$t->parse($editor . "_ext_editor", false);
				$t->set_var($editor . "_int_editor", "");
			}
		}
		else {
			foreach($editors_array as $editor){
				$t->set_var($editor . "_int_editor", "");
				$t->set_var($editor . "_ext_editor", "");
				if($CKEditor === 1) {
					$t->set_var('editor_error', '<div style="width:557px;text-align:left;font-weight:700;font-size:8pt;padding:4px;" class="errorbg">' . EXTERNAL_CKEDITOR_TIP . '</div>');
				}
			}
		}
	}

	function get_admin_settings($admin_id, $settings = array())
	{
		global $db, $table_prefix;
		// build SQL query
		if (!is_array($settings) && strlen($settings)) {
			$settings = array($settings);
		}
		$sql  = " SELECT * FROM ".$table_prefix."admins_settings ";
		$sql .= " WHERE admin_id=" . $db->tosql($admin_id, INTEGER);
		if (is_array($settings) && sizeof($settings) > 0) {
			$sql .= " AND (";
			for ($s = 0; $s < sizeof($settings); $s++) {
				if ($s > 0) { $sql .= " OR "; }
				$sql .= " setting_name=" . $db->tosql($settings[$s], TEXT); 
			}
			$sql .= " )";
		}
		// get admin settings
		$admin_settings = array();
		$db->query($sql);
		while ($db->next_record()) {
			$setting_name = $db->f("setting_name");
			$setting_value = $db->f("setting_value");
			$admin_settings[$setting_name] = $setting_value;
		}
		return $admin_settings;
	}

	function update_admin_settings($admin_id, $settings)
	{
		global $db, $table_prefix;
		if (is_array($settings) && sizeof($settings) > 0) {
			foreach ($settings as $setting_name => $setting_value) {
				// delete value before add it again
				$sql  = " DELETE FROM ".$table_prefix."admins_settings ";
				$sql .= " WHERE admin_id=" . $db->tosql($admin_id, INTEGER);
				$sql .= " AND setting_name=" . $db->tosql($setting_name, TEXT); 
				$db->query($sql);
				// add a new value
				if (strlen($setting_value)) {
					$sql  = " INSERT INTO ".$table_prefix."admins_settings ";
					$sql .= " (admin_id, setting_name, setting_value) VALUES (";
					$sql .= $db->tosql($admin_id, INTEGER) . ",";
					$sql .= $db->tosql($setting_name, TEXT) . ",";
					$sql .= $db->tosql($setting_value, TEXT) . ") ";
					$db->query($sql);
				}
			}
		}
	}


	// get and set records per page parameter
	function set_recs_param($page_name, $pass_parameters = "", $remove_parameters = "")
	{
		global $t;

		// prepare page url		
		if (!is_array($remove_parameters)) {
			$remove_parameters = array("page");
		}
		if (!is_array($pass_parameters)) {
			$pass_parameters = $_GET;
		}
		$page_url = $page_name; $param_number = 0;
		foreach($pass_parameters as $name => $value) {
			if (!in_array($name, $remove_parameters)) {
				$param_number++;
				if ($param_number > 1) {
					$page_url .= "&".$name."=".urlencode($value);
				} else {
					$page_url .= "?".$name."=".urlencode($value);
				}
			}
		}
		$page_url .= ($param_number) ? "&" : "?";

		$admin_id = get_session("session_admin_id");
		$admin_settings = get_admin_settings($admin_id, "records_per_page");
		$records_per_page = get_setting_value($admin_settings, "records_per_page", 25);
		$recs = get_param("recs");
		$recs_values = array(10, 25, 50, 100, 200);
		if ($recs && in_array($recs, $recs_values)) { 
			$records_per_page = $recs; 
			update_admin_settings($admin_id, array("records_per_page" => $recs));
		}
		if (!in_array($records_per_page, $recs_values)) {
			$records_per_page = 25;
		}
		for ($r = 0; $r < sizeof($recs_values); $r++) {
			$recs_value = $recs_values[$r];
			$recs_url = $page_url . "recs=".urlencode($recs_value);
			$t->set_var("recs_value", $recs_value);
			$t->set_var("recs_value_title", $recs_value);
			$t->set_var("recs_url", $recs_url);

			if ($recs_value == $records_per_page) {
				$t->set_var("recs_style", "recsShow");
			} else {
				$t->set_var("recs_style", "recsLink");
			}
			if ($r < sizeof($recs_values) - 1) {
				$t->set_var("recs_delimiter", ",");
			} else {
				$t->set_var("recs_delimiter", "");
			}
			$t->sparse("recs_values", true);
		}
		return $records_per_page;
	}


?>