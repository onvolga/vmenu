<?php

	class Date_DeltaRussian 
	{
		var $intervals = array(
			"seconds"  => array("секунда", "секунды", "секунд"),
			"minutes"  => array("минута", "минуты", "минут"),
			"hours"    => array("час", "часа", "часов"),
			"mday"     => array("день", "дня", "дней"),
			"mon"      => array("месяц", "месяца", "месяцев"),
			"year"     => array("год", "года", "лет")
		);
		var $from = "seconds";

		// Creates new object.
		// If $from is specified, "granularity" while spelling is $from.
		function Date_DeltaRussian($from = "seconds") 
		{
			$this->from = $from;
		}

		// returns the associative array with date deltas.
		function getDelta($first, $last)
		{
			if ($last < $first) return false;

			// Solve H:M:S part.
			$hms = ($last - $first) % (3600 * 24);
			$delta['seconds'] = $hms % 60;
			$delta['minutes'] = floor($hms/60) % 60;
			$delta['hours']   = floor($hms/3600) % 60;

			// Now work only with date, delta time = 0.
			$last -= $hms;
			$f = getdate($first);
			$l = getdate($last); // the same daytime as $first!

			$dYear = $dMon = $dDay = 0;

			// Delta day. Is negative, month overlapping.
			$dDay += $l['mday'] - $f['mday'];
			if ($dDay < 0) {
				$monlen = Date_DeltaRussian::monthLength(date("Y", $first), date("m", $first));
				$dDay += $monlen;
				$dMon--;
			}
			$delta['mday'] = $dDay;

			// Delta month. If negative, year overlapping.
			$dMon += $l['mon'] - $f['mon'];
			if ($dMon < 0) {
				$dMon += 12;
				$dYear --;
			}
			$delta['mon'] = $dMon;

			// Delta year.
			$dYear += $l['year'] - $f['year'];
			$delta['year'] = $dYear;
			
			return $delta;
		}

		// Makes the spellable phrase.
		function spellDelta($first, $last)
		{
			// Solve data delta.
			$delta = $this->getDelta($first, $last);
			if (!$delta) return false;

			// Make spellable phrase.
			$parts = array();
			foreach (array_reverse($delta) as $k=>$n) {
				if (!$n) continue;
				$parts[] = Date_DeltaRussian::declension($n, $this->intervals[$k]);
				if ($this->from && $k == $this->from) break;
			}
			return join(" ", $parts);
		}

		// Returns the length (in days) of the specified month.
		function monthLength($year, $mon)
		{
			$l = 28;
			while (checkdate($mon, $l+1, $year)) $l++;
			return $l;
		}

		// Функция предназначена для вывода численных результатов с учетом 
		// склонения слов, например: "1 ответ", "2 ответа", "13 ответов" и т.д. 
		// $int — целое число. 
		// $expressions — массив, например: array("ответ", "ответа", "ответов") 
		function declension($int, $expressions) 
		{ 
		   settype($int, "integer"); 
		   $count = $int % 100; 
		   if ($count >= 5 && $count <= 20) { 
			  $result = $int." ".$expressions['2']; 
		   } else { 
			  $count = $count % 10; 
			  if ($count == 1) { 
				 $result = $int." ".$expressions['0']; 
			  } elseif ($count >= 2 && $count <= 4) { 
				 $result = $int." ".$expressions['1']; 
			  } else { 
				 $result = $int." ".$expressions['2']; 
			  } 
		   } 
		   return $result; 
		} 
	}
	
	$default_title = "{MSG_RANDOM_ITEMS}";
	
	$html_template = get_setting_value($block, "html_template", "vi_block_random_items_uge.html");
	$t->set_file("block_body", $html_template);
	
	$friendly_urls      = get_setting_value($settings, "friendly_urls",      0);
	$friendly_extension = get_setting_value($settings, "friendly_extension", "");
	$new_product_enable = get_setting_value($settings, "new_product_enable", 0);
	$new_product_order  = get_setting_value($settings, "new_product_order",  0);	

	// Количество товаров
	$items_number = get_setting_value($vars, "items_number", 3);
	
	// Количество колонок
	$items_columns_number = get_setting_value($vars, "items_columns_number", 3);
	
	// Высота блока
	$item_block_height = get_setting_value($vars, "item_block_height", 150);
	
	// Категории
	$items_category_id = get_param("category_id");
	if (get_setting_value($vars, "items_category_id", "") != "")
	{
		$items_category_id = get_setting_value($vars, "items_category_id", "");
		if ($items_category_id == "all")
			$items_category_id = "";
	}

	// Производители
	$items_manufacturer_id = get_param("manf");
	if (get_setting_value($vars, "items_manufacturer_id", "") != "")
	{
		$items_manufacturer_id = get_setting_value($vars, "items_manufacturer_id", "");
		if ($items_manufacturer_id == "all")
			$items_manufacturer_id = "";
	}

	// Товары
	$items_ids = get_setting_value($vars, "items_ids", "");
	
	// Длина краткого описания
	$short_desc_length = get_setting_value($vars, "short_desc_length", 0);
	
	// Тип изображения
	$type_img = get_setting_value($vars, "type_img", "small_image");
	
	// Тип изображения, если нет специального
	$type_img_2 = get_setting_value($vars, "type_img_2", "small_image");
	
	// Показывать оставшееся время
	$show_remaining_time = get_setting_value($vars, "show_remaining_time", 0);
	
	// Показывать остаток
	$show_stock_level = get_setting_value($vars, "show_stock_level", 0);
	
	// Показывать цену
	$show_prices_block = get_setting_value($vars, "show_prices_block", 0);
	
	// Показывать кнопку "Купить"
	if ($show_prices_block) {
		$show_add_button = get_setting_value($vars, "show_add_button", 0);
	} else {
		$show_add_button = 0;
	}
	
	$curr_date = date("Y.m.d");
	
	// Формируем запрос на выборку товара
	$sql  = "SELECT * ".
			"FROM (SELECT item_id, item_name, ".$type_img.", ".$type_img_2." AS type_img_2, full_description, short_description, friendly_url, ".
			"price, sales_price, is_sales, discount_percent, ".
			"stock_level, use_stock_level, disable_out_of_stock, hide_out_of_stock, ".
			"min_quantity, max_quantity, quantity_increment, ".
			"show_hit_symbol, ".
			"DATE_FORMAT(show_begin_date, '%Y.%m.%d') AS sbd, DATE_FORMAT(show_end_date, '%Y.%m.%d') AS sed";
	if ($new_product_enable) {
		switch ($new_product_order) {
			case 0:
				$sql .= ", issue_date AS new_product_date";
				break;
			case 1:
				$sql .= ", date_added AS new_product_date";
				break;
			case 2:
				$sql .= ", date_modified AS new_product_date";
				break;
		}		
	}
	$sql .=	" FROM ".$table_prefix."items ".
			"WHERE is_showing=1 AND ((".$type_img." IS NOT NULL) OR (".$type_img." IS NULL AND ".$type_img_2." IS NOT NULL)) ".
			"AND ((hide_out_of_stock=1 AND stock_level > 0) OR hide_out_of_stock=0 OR hide_out_of_stock IS NULL) ";
	if ($items_manufacturer_id != "") {
		$sql .= "AND manufacturer_id IN (".$items_manufacturer_id.") ";
	}
	if ($items_ids != "") {
		$sql .= "AND item_id IN (".$items_ids.") ";	
	}
	$sql .= "ORDER BY RAND()) AS i";
	if ($items_category_id != "") {
		$sql .= ", ".$table_prefix."items_categories AS ic WHERE i.item_id=ic.item_id AND ic.category_id IN (".$items_category_id.") AND";
	} else {
		$sql .= " WHERE";
	}
	$sql .= " ((sbd IS NULL AND sed IS NULL) ".
			"OR (sbd IS NULL AND ('".$curr_date."' <= sed)) ".
			"OR (('".$curr_date."' >= sbd) AND sed IS NULL) ".
			"OR (('".$curr_date."' >= sbd) AND ('".$curr_date."' <= sed))) ".
			"LIMIT 0, ".$items_number.";";

	$db->query($sql);

	// Номер текущего элемента
	$form_id = 1;
	if ($db->next_record())
	{
		$friendly_urls      = get_setting_value($settings, "friendly_urls", 0);
		$friendly_extension = get_setting_value($settings, "friendly_extension", "");
		if ($friendly_urls && isset($page_friendly_url) && $page_friendly_url) {
			$products_page = $page_friendly_url.$friendly_extension;
		} else {
			$products_page = get_custom_friendly_url($script_name);
		}
		$pass_parameters = array(
			"item_id" => get_param("item_id"), "category_id" => get_param("category_id"), "manf" => get_param("manf"),
			"search_string" => get_param("search_string"), "sq" => get_param("sq"),
			"search_category_id" => get_param("search_category_id"), "pq" => get_param("pq"), "fq" => get_param("fq"),
			"s_tit" => get_param("s_tit"), "s_des" => get_param("s_des"),
			"user" => get_param("user"),
			"lprice" => get_param("lprice"), "hprice" => get_param("hprice"),
			"lweight" => get_param("lweight"), "hweight" => get_param("hweight"),
			"sort_ord" => get_param("sort_ord"), "sort_dir" => get_param("sort_dir"),
			"filter" => get_param("filter"),
			"page" => get_param("page"),
			"pn_pr" => get_param("pn_pr")
		);
		$query_string = get_query_string($pass_parameters, "", "", false);
		$rp = $products_page.$query_string;
		
		$products_form_url = $products_page;
		$t->set_var("products_form_url", $products_form_url);
		
		$quantity_control = get_setting_value($settings, "quantity_control_random", "");
		$zero_quantity    = 0;
				
		// Макс. кол-во элементов
		$i_max = $items_columns_number * floor(($items_number - 1) / $items_columns_number);

		$t->set_var("hit_block",            "");
		$t->set_var("product_new_image",    "");
		$t->set_var("sales",                "");
		$t->set_var("price_block",          "");
		$t->set_var("save",                 "");
		$t->set_var("prices_block",         "");
		$t->set_var("description_block",    "");
		$t->set_var("remaining_time_block", "");
		$t->set_var("stock_level_block",    "");
		$t->set_var("add_button",           "");
		$t->set_var("item",                 "");
		$t->set_var("random_items_block_uge",   "");	
		
		$t->set_var("pb_id",            $pb_id);
		$t->set_var("rp",               $rp);
		$t->set_var("multi_add",        0);
		$t->set_var("start_index",      1);
		$t->set_var("redirect_to_cart", get_setting_value($settings, "redirect_to_cart", ""));
		
		$items_indexes = "";
		for ($n = 1; $n <= $items_number; $n ++) {
			if ($items_indexes != "") { $items_indexes .= ","; }
			$items_indexes .= $n;
		}
		$t->set_var("items_indexes", $items_indexes);
		
		srand((double) microtime() * 1000000);
		$random_value = rand();
		$t->set_var("rnd", $random_value);

		$t->set_var("item_block_height", $item_block_height);

		// Ширина одной колонки
		$column_width = floor(99 / $items_columns_number);
		$t->set_var("item_column_width", $column_width);
		
		do
		{
			$item_id              = $db->f("item_id");
			$item_name            = get_translation($db->f("item_name"));
			$stock_level          = $db->f("stock_level");
			$use_stock_level      = $db->f("use_stock_level");
			$disable_out_of_stock = $db->f("disable_out_of_stock");
			$hide_out_of_stock    = $db->f("hide_out_of_stock");
			$use_stock_level      = $db->f("use_stock_level");
			$price                = $db->f("price");
			$sales_price          = $db->f("sales_price");
			$min_quantity         = $db->f("min_quantity");
			$max_quantity         = $db->f("max_quantity");
			$quantity_increment   = $db->f("quantity_increment");
			$show_begin_date      = $db->f("show_begin_date");
			$show_end_date        = $db->f("show_end_date");
			$show_hit_symbol      = $db->f("show_hit_symbol");
			
			$quantity_limit = ($use_stock_level && ($disable_out_of_stock || $hide_out_of_stock));
			if (!$stock_level) { $stock_level = 0; }
			
			$t->set_var("form_id",     $form_id);
			$t->set_var("item_id",     $item_id);
			$t->set_var("tax_percent", 0);
			
			$product_params = prepare_product_params();
			$product_params["item_name"]      = strip_tags($item_name);
			$product_params["form_id"]        = $item_id;
			$product_params["use_sl"]         = $use_stock_level;
			$product_params["sl"]             = $stock_level;
			$product_params["in_sm"]          = "";
			$product_params["out_sm"]         = "";
			$product_params["quantity_price"] = "";
			$product_params["comp_price"]     = "0";
			$product_params["comp_tax"]       = "0";
			$product_params["properties_ids"] = "";
			$product_params["pe"]             = "0";
			
			// Устанавливаем нужный стиль блока, чтобы рамки были правильные
			if (($form_id % $items_columns_number) == 0)
			{
				if ($form_id > $i_max) {
					$t->set_var("item_class", "item_bottom_right");
				} else {
					$t->set_var("item_class", "item_right");
				}
			} else {
				if ($form_id > $i_max) {
					$t->set_var("item_class", "item_bottom");
				} else {
					$t->set_var("item_class", "item");
				}
			}
			
			
			// Изображение товара
			$type_img_1 = $db->f($type_img);
			if ($type_img_1 != "") {
				$t->set_var("src", $type_img_1);
			} else {
				$t->set_var("src", $db->f("type_img_2"));
			}
						
			
			// Хит продаж
			if ($db->f("show_hit_symbol")) {
				$t->sparse("hit_block", false);
			} else {
				$t->set_var("hit_block", "");
			}
			
			
			// Наименование товара
			$t->set_var("item_name", $item_name);
			$t->set_var("alt",       $item_name);
			
			
			// Новинка
			if ($new_product_enable) {
				$new_product_date = $db->f("new_product_date");
				$is_new_product   = is_new_product($new_product_date);
			} else {
				$is_new_product = false;
			}
			if ($is_new_product) {
				$t->sparse("product_new_image", false);			
			} else {
				$t->set_var("product_new_image", "");
			}
			

			// Получение ссылки на товар
			$friendly_url = $db->f("friendly_url");
			if ($friendly_urls && $friendly_url) {
				$details_url = $friendly_url.$friendly_extension;
			} else {
				$details_url = "product_details.php?item_id=".urlencode($db->f("item_id"));
			}
			$t->set_var("product_details_url", $details_url);

			
			// Цены
			if ($show_prices_block) {
				if (($sales_price != $price) and $db->f("is_sales")) {
					$discount_percent = round($db->f("discount_percent"), 0);
					if (!$discount_percent and ($price > 0)) {
						$discount_percent = round(($price - $sales_price) * 100 / $price, 0);
					}
					
					$t->set_var("price_block_class",   "priceBlockOld");
					$t->set_var("price_control",       currency_format($price));
					$t->set_var("sales_price_control", currency_format($sales_price));
					$t->set_var("discount_percent",    $discount_percent);
					$t->set_var("you_save",            currency_format($price - $sales_price));

					$t->sparse("price_block", false);
					$t->sparse("sales",       false);
					$t->sparse("save",        false);
				} else {
					$t->set_var("price_block_class", "priceBlock");
					$t->set_var("price",             currency_format($price));
					$t->set_var("price_control",     currency_format($price));
					$t->set_var("sales",             "");
					$t->set_var("save",              "");
					
					$t->sparse("price_block", false);
				}
				$t->sparse("prices_block", false);
			}
			if (($sales_price != $price) and $db->f("is_sales")) {
				$product_params["base_price"] = $sales_price;
			} else {
				$product_params["base_price"] = $price;
			}
			
			
			// Описание товара
			$t->set_var("description_block", "");
			if ($short_desc_length > 0)
			{
				$item_short_desc = get_translation($db->f("short_description"));
				if ($item_short_desc == "") {
					$item_short_desc = get_translation($db->f("full_description"));
				}
				if ($item_short_desc != "") {
					// Обрезаем краткое описание
					if (strtoupper(CHARSET) == "UTF-8")
					{
						// Для сайта в utf-8
						if (strlen(iconv("utf-8", "windows-1251", $item_short_desc)) > $short_desc_length)
							$item_short_desc = iconv("windows-1251", "utf-8", substr(iconv("utf-8", "windows-1251", $item_short_desc), 0, $short_desc_length)."...");
					}
					else
					{
						// Для сайта в windows-1251
						if (strlen($item_short_desc) > $short_desc_length)
							$item_short_desc = substr($item_short_desc, 0, $short_desc_length)."...";
					}
					$item_short_desc = str_replace("<br><br>", "<br>", $item_short_desc);
					$item_short_desc = str_replace("<br>", " ", $item_short_desc);
					$t->set_var("item_description", $item_short_desc);
					$t->sparse("description_block", false);
				}
			}

			
			// Оставшееся время
			$t->set_var("remaining_time_block", "");
			$sed = $db->f("sed");
			if ($show_remaining_time && ($sed != "")) {
				$sed = str_replace(".", "/", $sed);
				$end_time  = strtotime($sed) + 24*60*60;
				$curr_time = strtotime('now');
				$delta = $end_time - $curr_time;
				if ($delta > 0) {
					$one_week = 7*24*60*60;
					if ($delta > $one_week) {
						$dM = new Date_DeltaRussian("hours");
					} else {
						$dM = new Date_DeltaRussian("minutes");
					}
					$t->set_var("remaining_time", $dM->spellDelta($curr_time, $end_time));
					$t->sparse("remaining_time_block", false);	
				}
			}
			
			
			// Остаток товара
			if ($show_stock_level && (!$quantity_limit || ($stock_level > 0))) {
				$t->set_var("stock_level", $stock_level);
				$t->sparse("stock_level_block", false);	
			} else {
				$t->set_var("stock_level_block", "");
			}

			
			// Кнопка "Купить"
			if ($show_add_button) {
				if ($quantity_limit && ($stock_level == 0)) {
					$t->set_var("add_button", "&nbsp;");
				} else {
					set_quantity_control($quantity_limit, $stock_level, $quantity_control, "products_".$pb_id, $form_id, $zero_quantity, $min_quantity, $max_quantity, $quantity_increment);	
					$t->set_var("buy_href", "javascript:document.products_".$pb_id.".submit();");
					$t->sparse("add_button", false);
				}
			}

			set_product_params($product_params);			
			$t->parse("item", true);
			
			$form_id ++;
		} while ($db->next_record());
		$block_parsed = true;
	} else {
		$block_parsed = false;
	}
	
	$t->sparse("random_items_block", false);
	
?>