<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Fуrum");
	define("TOPIC_INFO_TITLE", "Informaзгo do Tуpico");
	define("TOPIC_MESSAGE_TITLE", "Mensagem");

	define("MY_FORUM_TOPICS_MSG", "Os Meus Tуpicos do Fуrum");
	define("ALL_FORUM_TOPICS_MSG", "Todos os Tуpicos do Fуrum");
	define("MY_FORUM_TOPICS_DESC", "Jб alguma vez pensou que a dificuldade que estб a ter, pode ter sido experienciada por outra(s) pessoa(s)? Gostaria de partilhar a sua experiкncia com novos utilizadores? Por que nгo tornar-se um utilizador do Fуrum e juntar-se а comunidade?");
	define("NEW_TOPIC_MSG", "Novo Tуpico");
	define("NO_TOPICS_MSG", "Nгo foram encontrados tуpicos");
	define("FOUND_TOPICS_MSG", "Encontrбmos <b>{found_records}</b> tуpicos com o(s) termo(s) '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "Nгo foram encontrados fуruns");

	define("FORUM_NAME_COLUMN", "Fуrum");
	define("FORUM_TOPICS_COLUMN", "Tуpicos");
	define("FORUM_REPLIES_COLUMN", "Respostas");
	define("FORUM_LAST_POST_COLUMN", "Ъltima actualizaзгo");
	define("FORUM_MODERATORS_MSG", "Moderadores");

	define("TOPIC_NAME_COLUMN", "Tуpico");
	define("TOPIC_AUTHOR_COLUMN", "Autor");
	define("TOPIC_VIEWS_COLUMN", "Vistas");
	define("TOPIC_REPLIES_COLUMN", "Respostas");
	define("TOPIC_UPDATED_COLUMN", "Ъltima actualizaзгo");
	define("TOPIC_ADDED_MSG", "Obrigado,<br>O seu tуpico foi adicionado");

	define("TOPIC_ADDED_BY_FIELD", "Adicionado por");
	define("TOPIC_ADDED_DATE_FIELD", "Adicionado");
	define("TOPIC_UPDATED_FIELD", "Ultima atualizaзгo");
	define("TOPIC_NICKNAME_FIELD", "Alcunha");
	define("TOPIC_EMAIL_FIELD", "O Seu Endereзo de E-mail");
	define("TOPIC_NAME_FIELD", "Tуpico");
	define("TOPIC_MESSAGE_FIELD", "Mensagem");
	define("TOPIC_NOTIFY_FIELD", "Enviar todas as respostas para o meu e-mail");

	define("ADD_TOPIC_BUTTON", "Adicionar Tуpico");
	define("TOPIC_MESSAGE_BUTTON", "Adicionar Mensagem");

	define("TOPIC_MISS_ID_ERROR", "Parвmetro <b>Thread ID</b> em falta.");
	define("TOPIC_WRONG_ID_ERROR", "O parвmetro <b>Thread ID</b> tem um valor errado.");
	define("FORUM_SEARCH_MESSAGE", "Encontrбmos {search_count} mensagens contendo o(s) termo(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Visualizar");
	define("TOPIC_SAVE_BUTTON", "Guardar");

	define("LAST_POST_ON_SHORT_MSG", "A:");
	define("LAST_POST_IN_SHORT_MSG", "Em:");
	define("LAST_POST_BY_SHORT_MSG", "Por:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Ъltima alteraзгo:");

?>