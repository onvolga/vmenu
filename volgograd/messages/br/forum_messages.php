<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "FСѓrum");
	define("TOPIC_INFO_TITLE", "InformaР·Ріo sobre tСѓpicos");
	define("TOPIC_MESSAGE_TITLE", "Mensagem");

	define("MY_FORUM_TOPICS_MSG", "Meus tСѓpicos");
	define("ALL_FORUM_TOPICS_MSG", "Todos os tСѓpicos");
	define("MY_FORUM_TOPICS_DESC", "Alguma vez se perguntou se o problema que vocРє estР± tendo, alguma outra pessoa jР± passou pelo mesmo? VocРє gostaria de compartilhar sua experiencia com os novos usuР±rios? Por que nРіo se tornar um usuР±rio do fСѓrum e se juntar Р° comunidade? ");
	define("NEW_TOPIC_MSG", "Novo tСѓpico");
	define("NO_TOPICS_MSG", "NРіo foram encontrados tСѓpicos");
	define("FOUND_TOPICS_MSG", "Foram encontrados <b>{found_records}</b> tСѓpicos correspondendo ao(s) termo(s) '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "NРіo foram encontrados fСѓruns");

	define("FORUM_NAME_COLUMN", "FСѓrum");
	define("FORUM_TOPICS_COLUMN", "TСѓpicos");
	define("FORUM_REPLIES_COLUMN", "Respostas");
	define("FORUM_LAST_POST_COLUMN", "Ultima AtualizaР·Ріo");
	define("FORUM_MODERATORS_MSG", "Moderador");

	define("TOPIC_NAME_COLUMN", "TСѓpico");
	define("TOPIC_AUTHOR_COLUMN", "Autor");
	define("TOPIC_VIEWS_COLUMN", "VisualizaР·С…es");
	define("TOPIC_REPLIES_COLUMN", "Respostas");
	define("TOPIC_UPDATED_COLUMN", "Ultima AtualizaР·Ріo");
	define("TOPIC_ADDED_MSG", "Obrigado<br> Seu tСѓpico foi adicionado");

	define("TOPIC_ADDED_BY_FIELD", "Adicionado por");
	define("TOPIC_ADDED_DATE_FIELD", "Adicionado em");
	define("TOPIC_UPDATED_FIELD", "Ultima AtualizaР·Ріo");
	define("TOPIC_NICKNAME_FIELD", "Apelido");
	define("TOPIC_EMAIL_FIELD", "Email");
	define("TOPIC_NAME_FIELD", "TСѓpico");
	define("TOPIC_MESSAGE_FIELD", "Mensagem");
	define("TOPIC_NOTIFY_FIELD", "Enviar todas as respostas para meu email");

	define("ADD_TOPIC_BUTTON", "Adicionar TСѓpico");
	define("TOPIC_MESSAGE_BUTTON", "Adicionar Mensagem");

	define("TOPIC_MISS_ID_ERROR", "ParРІmetro <b>Thread ID</b> faltando.");
	define("TOPIC_WRONG_ID_ERROR", "ParРІmetro <b>Thread ID</b> com valor incorreto");
	define("FORUM_SEARCH_MESSAGE", "Foram encontrados {search_count} mensagens correspondendo ao(s) termo(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Previsualizar");
	define("TOPIC_SAVE_BUTTON", "Salvar");

	define("LAST_POST_ON_SHORT_MSG", "Em:");
	define("LAST_POST_IN_SHORT_MSG", "De:");
	define("LAST_POST_BY_SHORT_MSG", "Por:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "РЄltima alteraР·Ріo:");

?>