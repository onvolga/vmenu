<?php                           

	$default_title = "";

	$html_template = get_setting_value($block, "html_template", "block_checkout_breadcrumb.html"); 
  $t->set_file("block_body", $html_template);

	if ($current_page == "order_info.php") {
		$t->set_var("step1_class", "active");
		$t->set_var("step2_class", "nonactive");
		$t->set_var("step3_class", "nonactive");
	} else if ($current_page == "credit_card_info.php") {
		$t->set_var("step1_class", "nonactive");
		$t->set_var("step2_class", "active");
		$t->set_var("step3_class", "nonactive");
	} else if ($current_page == "order_confirmation.php") {
		$t->set_var("step1_class", "nonactive");
		$t->set_var("step2_class", "nonactive");
		$t->set_var("step3_class", "active");
	}

	$block_parsed = true;

?>