<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum messages
	define("FORUM_TITLE", "Fуrum");
	define("TOPIC_INFO_TITLE", "Informace o tematu");
	define("TOPIC_MESSAGE_TITLE", "Zprбva");

	define("MY_FORUM_TOPICS_MSG", "Moje temata");
	define("ALL_FORUM_TOPICS_MSG", "Vљechny temata");
	define("MY_FORUM_TOPICS_DESC", "Stalo se Vбm nмkdy, ћe problйm, kterэ mбte, jiћ nмkdo pшed Vбma шeљil? Chtмli by jste zdнlet Vaљe zkuљenosti s novэmi uћivateli? Staтte se иlenem fуra a pшidejte se ke komunitм.");
	define("NEW_TOPIC_MSG", "Novй tйma");
	define("NO_TOPICS_MSG", "Ћбdnй tйmata nebyli nalezeny");
	define("FOUND_TOPICS_MSG", "Naљel jsem <b>{found_records}</b> temata obsahujнcн text '<b>{search_string}</b>'");
	define("NO_FORUMS_MSG", "No forums found");

	define("FORUM_NAME_COLUMN", "Fуrum");
	define("FORUM_TOPICS_COLUMN", "Tйma");
	define("FORUM_REPLIES_COLUMN", "Odpovмdн");
	define("FORUM_LAST_POST_COLUMN", "Naposled aktualizovбno");
	define("FORUM_MODERATORS_MSG", "Moderators");

	define("TOPIC_NAME_COLUMN", "Tйma");
	define("TOPIC_AUTHOR_COLUMN", "Autor");
	define("TOPIC_VIEWS_COLUMN", "Zobrazenн");
	define("TOPIC_REPLIES_COLUMN", "Odpovмdн");
	define("TOPIC_UPDATED_COLUMN", "Naposled aktualizovбno");
	define("TOPIC_ADDED_MSG", "Dмkujeme<br>Vaљe tйma bylo pшidбno");

	define("TOPIC_ADDED_BY_FIELD", "Pшidal");
	define("TOPIC_ADDED_DATE_FIELD", "Pшidбno");
	define("TOPIC_UPDATED_FIELD", "Naposled aktualizovбno");
	define("TOPIC_NICKNAME_FIELD", "Pшezdнvka");
	define("TOPIC_EMAIL_FIELD", "Vaљe emailovб adresa");
	define("TOPIC_NAME_FIELD", "Tйma");
	define("TOPIC_MESSAGE_FIELD", "Zprбva");
	define("TOPIC_NOTIFY_FIELD", "Odeslat vљechny odpovмde na mщj email");

	define("ADD_TOPIC_BUTTON", "Pшidat tйma");
	define("TOPIC_MESSAGE_BUTTON", "Pшidat zprбvu");

	define("TOPIC_MISS_ID_ERROR", "Schбzн <b>Thread ID</b> parameter.");
	define("TOPIC_WRONG_ID_ERROR", "<b>Thread ID</b> mб chybnou hodnotu.");
	define("FORUM_SEARCH_MESSAGE", "We've found {search_count} messages matching the term(s) '{search_string}'");
	define("TOPIC_PREVIEW_BUTTON", "Preview");
	define("TOPIC_SAVE_BUTTON", "Save");

	define("LAST_POST_ON_SHORT_MSG", "On:");
	define("LAST_POST_IN_SHORT_MSG", "In:");
	define("LAST_POST_BY_SHORT_MSG", "By:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "Last modified:");

?>