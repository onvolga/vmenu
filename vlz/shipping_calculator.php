<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.1 RE                                               ***
  ***      File:  shipping_calculator.php                                  ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/

	

	$type = "details";
	include_once("./includes/common.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");
	include_once("./messages/" . $language_code . "/admin_messages.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/shipping_functions.php");

	/**
	 * custom user shippings
	 */
	$user_items = get_param("items");
	$user_items_session = get_session('current_merchant');
	if($user_items){
		set_session('current_merchant', $user_items);
	}
	elseif(!$user_items && $user_items_session){
		$user_items = $user_items_session;
	}
	if($user_items == 'all'){
		$user_items = false;
	}

	$tax_rates = get_tax_rates();
	// check id of temporary item in the shopping cart
 	if (!isset($new_cart_id)) {
		$new_cart_id = get_param("new_cart_id");
	}

	$t = new VA_Template($settings["templates_dir"]);
	$t->set_file("main", "shipping_calculator.html");
	$t->set_var("shipping_calculator_href", "shipping_calculator.php");
	$t->set_var("new_cart_id", htmlspecialchars($new_cart_id));

	$css_file = "";
	if (isset($settings["style_name"]) && $settings["style_name"]) {
		$css_file = "styles/" . $settings["style_name"];
		if (isset($settings["scheme_name"]) && $settings["scheme_name"]) {
			$css_file .= "_" . $settings["scheme_name"];
		}
		$css_file .= ".css";
	}
	$t->set_var("css_file", $css_file);

	$country_id = get_param("country_id");
	$state_id = get_param("state_id");
	$postal_code_param = get_param("postal_code");
	$operation = get_param("operation");
	$user_type_id = get_session("session_user_type_id");

	$delivery_items = array();
	if (strlen($new_cart_id)) {
		// for newly added item check temporary shipping cart array
		$shopping_cart = get_session("shipping_cart");
		$user_items = $shopping_cart[0]["USER_ADDED_ID"];
	} else {
		$shopping_cart = get_session("shopping_cart");
	}
	if (is_array($shopping_cart)) {

		$items_quantity = 0; $items_weight = 0; $total_weight = 0;
		foreach ($shopping_cart as $cart_id => $item) {
			if(($user_items && $item['USER_ADDED_ID'] !=$user_items) || (!$user_items && $item['USER_ADDED_ID'])){continue;}
			$item_id = $item["ITEM_ID"];
			$quantity = $item["QUANTITY"];
			$price = $item["PRICE"];
			$full_price = $item["PRICE"];
			$properties = $item["PROPERTIES"];
			$total_weight = 0;

			// clear 
			$t->set_var("components_block", "");
			$t->set_var("properties_values", "");

			$item_name = get_translation($item["ITEM_NAME"]);
			$sql  = " SELECT i.item_id, i.item_name, i.weight, i.packages_number, ";
			$sql .= " i.width, i.height, i.length, i.is_shipping_free, i.shipping_cost, ";
			$sql .= " i.is_separate_shipping, i.shipping_modules_default, i.shipping_modules_ids ";
			$sql .= " FROM " . $table_prefix . "items i ";
			$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);
			$db->query($sql);
			if ($db->next_record()) {
				$weight = $db->f("weight");
				$packages_number = $db->f("packages_number");
				$width = $db->f("width");
				$height = $db->f("height");
				$length = $db->f("length");
				$is_shipping_free = $db->f("is_shipping_free");
				$shipping_cost = $db->f("shipping_cost");
				$is_separate_shipping = $db->f("is_separate_shipping");
				$shipping_modules_default = $db->f("shipping_modules_default");
				$shipping_modules_ids = $db->f("shipping_modules_ids");
			}

			// check product properties and their additional weight
			$properties_values = ""; $options_weight = 0; $options_price = 0;
			if (is_array($properties)) {
				foreach ($properties as $property_id => $property_values) {
					$sql  = " SELECT property_type_id, property_name, control_type ";
					$sql .= " FROM " . $table_prefix . "items_properties ";
					$sql .= " WHERE property_id=" . $db->tosql($property_id, INTEGER);
					$db->query($sql);
					if ($db->next_record()) {
						$property_type_id = $db->f("property_type_id");
						// show only product options and subcomponents separately
						if ($property_type_id == 1) {
							$property_name = get_translation($db->f("property_name"));
							$control_type = $db->f("control_type");

							$additional_weight = 0;
							if (strtoupper($control_type) == "LISTBOX" || strtoupper($control_type) == "RADIOBUTTON"
								|| strtoupper($control_type) == "CHECKBOXLIST" || strtoupper($control_type) == "TEXTBOXLIST") {
								$values_list = ""; 
								for($pv = 0; $pv < sizeof($property_values); $pv++) {
									$sql  = " SELECT property_value, additional_weight, additional_price ";
									$sql .= " FROM " . $table_prefix . "items_properties_values ipv ";
									$sql .= " WHERE property_id=" . $db->tosql($property_id, INTEGER);
									$sql .= " AND item_property_id=" . $db->tosql($property_values[$pv], INTEGER);
									$db->query($sql);
									if ($db->next_record()) {
										$additional_weight = $db->f("additional_weight");
										$additional_price = $db->f("additional_price");
										if (strtoupper($control_type) == "TEXTBOXLIST") {
											$values_list .= "<br>";
											$values_list .= get_translation($db->f("property_value")) . ": ";
											$values_list .= $item["PROPERTIES_INFO"][$property_id]["TEXT"][$property_values[$pv]];
										} else {
											if ($values_list) { $values_list .= ", "; }
											$values_list .= get_translation($db->f("property_value"));
										}
									}
								}

								if (strtoupper($control_type) == "TEXTBOXLIST") {
									$properties_values .= "<br>" . $property_name . ": ";
								} else {
									$properties_values .= "<br>" . $property_name . ": " . $values_list;
								}
								if ($additional_weight > 0) {
									$properties_values .= " (<b>" . weight_format($additional_weight) . "</b>)";
								}
								if (strtoupper($control_type) == "TEXTBOXLIST") {
									$properties_values .= $values_list;
								}
							} elseif ($property_values[0]) {
								$property_value = get_translation($property_values[0]);
								if (preg_match("/^http\:\/\//", $property_value)) {
									$property_value = "<a href=\"".$property_value."\" target=\"_blank\">" . basename($property_value) . "</a>";
								}
								$properties_values .= "<br>" . $property_name . ": " . $property_value;

								if ($additional_weight > 0) {
									$properties_values .= " (<b>" . weight_format($additional_weight) . "</b>)";
								}
							}
							$options_weight += $additional_weight;
							$options_price += $additional_price;
						}
					}
				}
			}

			$t->set_var("item_name", htmlspecialchars($item_name));
			$t->set_var("properties_values", $properties_values);
			$t->set_var("quantity", $quantity);	
			if ($weight > 0) {
				$t->set_var("item_weight", "(<b>".weight_format($weight)."</b>)");
			} else {
				$t->set_var("item_weight", "");
			}
			$weight += $options_weight;
			$full_price += $options_price;
			$item_total_weight = $quantity * $weight;
			$items_quantity += $quantity; 
			$total_weight += $item_total_weight;

			// add parent item to delivery list
			$delivery_items[] = array(
				"item_id" => $item_id, 
				"quantity" => $quantity, 
				"packages_number" => $packages_number, 
				"price" => $price, 
				"full_price" => $full_price, 
				"weight" => $weight, 
				"full_weight" => $weight, 
				"width" => $width, 
				"height" => $height, 
				"length" => $length, 
				"is_shipping_free" => $is_shipping_free, 
				"shipping_cost" => $shipping_cost, 
				"is_separate_shipping" => $is_separate_shipping, 
				"shipping_modules_default" => $shipping_modules_default, 
				"shipping_modules_ids" => $shipping_modules_ids, 
			);

			// parse components
			$components = $item["COMPONENTS"];
			if (is_array($components) && sizeof($components) > 0) {
				$t->set_var("components", "");
				foreach ($components as $property_id => $component_values) {
					foreach ($component_values as $item_property_id => $component) {
						$property_type_id = $component["type_id"];
						$sub_item_id = $component["sub_item_id"];
						$sub_quantity = $component["quantity"];
						if ($property_type_id == 2) {
							$sql  = " SELECT pr.property_name AS component_name, pr.quantity, pr.quantity_action, ";
							$sql .= " i.item_id, i.price, i.packages_number, i.weight, i.width, i.height, i.length, ";
							$sql .= " i.is_shipping_free, i.shipping_cost, ";
							$sql .= " i.is_separate_shipping, i.shipping_modules_default, i.shipping_modules_ids ";
							$sql .= " FROM (" . $table_prefix . "items_properties pr ";
							$sql .= " INNER JOIN " . $table_prefix . "items i ON pr.sub_item_id=i.item_id)";
							$sql .= " WHERE pr.property_id=" . $db->tosql($property_id, INTEGER);
						} else {
							$sql  = " SELECT ipv.property_value AS component_name, i.weight, ipv.quantity, pr.quantity_action, ";
							$sql .= " i.item_id, i.price, i.packages_number, i.weight, i.width, i.height, i.length, ";
							$sql .= " i.is_shipping_free, i.shipping_cost, ";
							$sql .= " i.is_separate_shipping, i.shipping_modules_default, i.shipping_modules_ids ";
							$sql .= " FROM ((" . $table_prefix . "items_properties_values ipv ";
							$sql .= " INNER JOIN " . $table_prefix . "items_properties pr ON ipv.property_id=pr.property_id) ";
							$sql .= " INNER JOIN " . $table_prefix . "items i ON ipv.sub_item_id=i.item_id)";
							$sql .= " WHERE ipv.item_property_id=" . $db->tosql($item_property_id, INTEGER);
						}
						$db->query($sql);
						if ($db->next_record()) {
							$component_item_id = $db->f("item_id");
							$component_name = $db->f("component_name");
							$component_quantity = $db->f("quantity");
							$component_qty_action = $db->f("quantity_action");
							if ($component_quantity < 1) { $component_quantity = 1; }
							if ($component_qty_action != 2) { $component_quantity = $component_quantity * $quantity; }
			
							$price = $db->f("price");
							$weight = $db->f("weight");
							$component_total_weight = $component_quantity * $weight;
							$total_weight += $component_total_weight;
							$packages_number = $db->f("packages_number");
							$width = $db->f("width");
							$height = $db->f("height");
							$length = $db->f("length");
							$is_shipping_free = $db->f("is_shipping_free");
							$shipping_cost = $db->f("shipping_cost");
							$is_separate_shipping = $db->f("is_separate_shipping");
							$shipping_modules_default = $db->f("shipping_modules_default");
							$shipping_modules_ids = $db->f("shipping_modules_ids");

							// add component item to delivery list
							$delivery_items[] = array(
								"item_id" => $component_item_id, 
								"quantity" => $component_quantity, 
								"packages_number" => $packages_number, 
								"price" => $price, 
								"full_price" => $price, 
								"weight" => $weight, 
								"full_weight" => $weight, 
								"width" => $width, 
								"height" => $height, 
								"length" => $length, 
								"is_shipping_free" => $is_shipping_free, 
								"shipping_cost" => $shipping_cost, 
								"is_separate_shipping" => $is_separate_shipping, 
								"shipping_modules_default" => $shipping_modules_default, 
								"shipping_modules_ids" => $shipping_modules_ids, 
							);

							$selection_name = "";
							if (isset($item["PROPERTIES_INFO"][$property_id])) {
								$selection_name = $item["PROPERTIES_INFO"][$property_id]["NAME"] . ": ";
							} 
							$t->set_var("selection_name", $selection_name);
							$t->set_var("component_quantity", $component_quantity);
							$t->set_var("component_name", htmlspecialchars($component_name));
							if ($weight > 0) {
								$t->set_var("component_weight", "(<b>".weight_format($weight)."</b>)");
							} else {
								$t->set_var("component_weight", "");
							}

							$t->sparse("components", true);
						}
					}
				}
				$t->sparse("components_block", false);
			} else {
				$t->set_var("components_block", "");
			}

			// set total weight of item bundle
			$items_weight += $total_weight;
			if ($total_weight > 0) {
				$t->set_var("total_weight", weight_format($total_weight));
			} else {
				$t->set_var("total_weight", NOT_AVAILABLE_MSG);
			}

			$t->parse("items", true);
		}

		if ($items_weight > 0) {
			$t->set_var("items_weight", weight_format($items_weight));
		} else {
			$t->set_var("items_weight", NOT_AVAILABLE_MSG);
		}
		$t->set_var("items_quantity", $items_quantity);
	}

	// check delivery methods and their costs
	if ($operation == "go") {
		$shipping_groups = get_shipping_types($country_id, $state_id, $postal_code_param, $site_id, $user_type_id, $delivery_items);
	
		//prepare taxes
		$tax_rates = get_tax_rates(true, $country_id, $state_id, $postal_code);		
	    $tax_persentage = 0;
	    $tax_fixed_amount = 0;

		$user_info = get_session("session_user_info");
		$user_tax_free = false;
		if(is_array($user_info) && $user_info["tax_free"] == true){
			$user_tax_free = true;
		}
		if (sizeof($tax_rates) > 0 && $user_tax_free === false) {
			foreach ($tax_rates as $tax_id => $tax_info) {
				if (floatval($tax_info["types"]["shipping"]["fixed_amount"]) > 0 || floatval($tax_info["types"]["shipping"]["tax_percent"]) > 0){
					if(floatval($tax_info["types"]["shipping"]["tax_percent"]) > 0){
						$tax_persentage += floatval($tax_info["types"]["shipping"]["tax_percent"]);
					}
					else{
						$tax_fixed_amount += floatval($tax_info["types"]["shipping"]["fixed_amount"]);
					}
				}
				elseif(floatval($tax_info["fixed_amount"]) > 0 || floatval($tax_info["tax_percent"]) > 0){
					if(floatval($tax_info["tax_percent"]) > 0){
						$tax_persentage += floatval($tax_info["tax_percent"]);
					}
					else{		
						$tax_fixed_amount += floatval($tax_info["fixed_amount"]);
					}
				}
			}
		}
//var_dump($shipping_groups);		
		if (sizeof($shipping_groups)) {
			$methods_shown = 0;
			foreach ($shipping_groups as $group_id => $group) {
				$shipping_types = $group["types"];
				$t->set_var("shipping_methods", "");
				foreach ($shipping_types as $id => $shipping_info) {
					$t->set_var("day_block", false);
					list ($shipping_id, $shipping_code, $shipping_name, $shipping_cost, $tare_weight, $shipping_taxable, $shipping_time) = $shipping_info;
		  
					$t->set_var("shipping_name", htmlspecialchars($shipping_name));
					if ($shipping_cost > 0 || $tax_fixed_amount > 0) {
						$shipping_cost = $shipping_cost * (1 + ($tax_persentage / 100)) + $tax_fixed_amount;
						$t->set_var("shipping_cost", currency_format($shipping_cost));
					} else {
						$t->set_var("shipping_cost", FREE_SHIPPING_MSG);
					}
					$t->set_var("id_num", $methods_shown);
					$methods_shown++;
					if($shipping_info["additional_data"]["phones"]){
						$t->set_var("phones", $shipping_info["additional_data"]["phones"]);
						$t->parse("phones_block", false);
					} else {
						$t->set_var("phones_block", false);
					}
					if($shipping_info["additional_data"]["info"]){
						$t->set_var("info", $shipping_info["additional_data"]["info"]);
						$t->parse("info_block", false);
					} else {
						$t->set_var("info_block", false);
					}
					
					$show_days = false;
					foreach ($shipping_info["additional_data"]["days"] as $day_name => $day_info) {

						if($day_info["available"] == 1){
							$dname = constant(strtoupper($day_name));

							if($day_info["info"]){
								$dname .= " " . $day_info["info"];
							}

							$t->set_var("day_name", $dname);
							
							$show_days = true;
							$t->parse("day_block", true);
						}
					}

					if($show_days == true){
						$t->parse("days_block", false);
					} else {
						$t->set_var("days_block", false);
					}

					if($shipping_info["additional_data"]["blz_shipping_type"] == 1){
						$t->set_var("deliv_ship_block", false);
						$t->set_var("type", " Самовывоз");
						if(strlen($shipping_info["additional_data"]["address"])){
							$t->set_var("address", $shipping_info["additional_data"]["address"]);
							$t->parse("address_block", false);
						} else {
							$t->set_var("address_block", false);
						}
						if(isset($shipping_info["additional_data"]["self_delivery"])){
							$t->set_var("self_delivery", ($shipping_info["additional_data"]["self_delivery"] == 1) ? "Магазин продавца" : "Пункт выдачи заказа");
							$t->parse("self_delivery_block", false);
						} else {
							$t->set_var("self_delivery_block", false);
						}
						$t->parse("button_det", false);
						
						$t->parse("self_ship_block", false);			
						$t->parse("additional_info_block", false);
					}
					elseif($shipping_info["additional_data"]["blz_shipping_type"] == 2){
						$t->set_var("self_ship_block", false);
						$t->set_var("type", " Доставка");

						if(isset($shipping_info["additional_data"]["evening"])){
							$t->set_var("evening", ($shipping_info["additional_data"]["evening"] == 1) ? "Да" : "Нет");
							$t->parse("evening_block", false);
						} else {
							$t->set_var("evening_block", false);
						}
						if(isset($shipping_info["additional_data"]["weekend"])){
							$t->set_var("weekend", ($shipping_info["additional_data"]["weekend"] == 1) ? "Да" : "Нет");
							$t->parse("weekend_block", false);
						} else {
							$t->set_var("weekend_block", false);
						}
						if(isset($shipping_info["additional_data"]["at_time"])){
							$t->set_var("at_time", ($shipping_info["additional_data"]["at_time"] == 1) ? "Да" : "Нет");
							$t->parse("at_time_block", false);
						} else {
							$t->set_var("at_time_block", false);
						}
						if(strlen($shipping_info["additional_data"]["to_floor"])){
							$t->set_var("to_floor", ($shipping_info["additional_data"]["to_floor"] == 1) ? "Платный" : "Бесплатный");
							$t->parse("to_floor_block", false);
						} else {
							$t->set_var("to_floor_block", false);
						}
						$t->parse("button_det", false);

						$t->parse("deliv_ship_block", false);
						$t->parse("additional_info_block", false);
					}
					else{
						$t->set_var("button_det", "");
						$t->set_var("additional_info_block", "");
					}
					$t->parse("shipping_methods", true);
				}

				$user_names = $group["user_names"];
				if (!is_array($user_names) || !sizeof($user_names)) {
					$user_names = array(SHIPPING_METHOD_MSG);
				}

				$t->set_var("group_name", implode(" / ", $user_names));

				$t->parse("shipping", true);
				if($methods_shown === 0){
					$t->parse("no_shipping", false);
				}
			}
		} else {
			$t->parse("no_shipping", false);
		}
	}


	// set delivery fields
	$countries = get_db_values("SELECT country_id,country_name FROM " . $table_prefix . "countries WHERE show_for_user=1 ORDER BY country_order, country_name ", array(array("", SELECT_COUNTRY_MSG)));
	set_options($countries, $country_id, "country_id");

	$states = get_db_values("SELECT state_id,state_name FROM " . $table_prefix . "states WHERE show_for_user=1 ORDER BY country_id, state_order, state_name ", array(array("", "")));
	set_options($states, $state_id, "state_id");

	$t->set_var("postal_code", htmlspecialchars($postal_code_param));

	/**
	 *dostavka custom state selector 
	 */	
	$stmt = "select s.state_id, s.state_name, c.country_id from va_states s inner join va_countries c on c.country_id=s.country_id order by c.country_order, s.state_order, s.state_name";
	$db->query($stmt);
	$js_prepared_states = array();
	while($db->next_record()){
		$js_prepared_states["cntr_" . $db->f("country_id")][] = array("id" => $db->f("state_id"), "name" => $db->f("state_name"));
	}
	$t->set_var("prepared_data_js", json_encode($js_prepared_states));

	$page_code = get_param("page");
	$user_id = get_session("session_user_id");		
	$user_info = get_session("session_user_info");
	$user_type_id = get_setting_value($user_info, "user_type_id", "");

	
	$t->pparse("main");

function weight_format($weight)
{
	global $settings;
	$weight_measure = get_setting_value($settings, "weight_measure", "");
	if ($weight > 0) {
		if (strpos ($weight, ".") !== false) {
			while (substr($weight, strlen($weight) - 1) == "0") {
				$weight = substr($weight, 0, strlen($weight) - 1);
			}
		}
		if (substr($weight, strlen($weight) - 1) == ".") {
			$weight = substr($weight, 0, strlen($weight) - 1);
		}
		$weight .= " " . $weight_measure;
	} else {
		$weight = "0";
	}
	return $weight;
}


?>