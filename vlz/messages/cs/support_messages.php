<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  support_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// support messages
	define("SUPPORT_TITLE", "Centrum podpory");
	define("SUPPORT_REQUEST_INF_TITLE", "Ћбdost o informaci");
	define("SUPPORT_REPLY_FORM_TITLE", "Odpovмdмt");

	define("MY_SUPPORT_ISSUES_MSG", "Mй ћбdosti o podporu");
	define("MY_SUPPORT_ISSUES_DESC", "Pokud mбte jakйkoliv problйmy s produktama, kterй jste u nбs zakoupili, nбљ tнm podpory zбkaznнkщm je pшipraven Vбm pomoci. Pro zadбnн poћadavky staин kliknout na hornн link");
	define("NEW_SUPPORT_REQUEST_MSG", "Novб poћadavka");
	define("SUPPORT_REQUEST_ADDED_MSG", "Dмkujeme<br>Nбљ tнm podpory se pokusн pomoci s Vaљн poћadavkou tak rychle, jak to jen bude moћnй.");
	define("SUPPORT_SELECT_DEP_MSG", "Zvolit oddмlenн");
	define("SUPPORT_SELECT_PROD_MSG", "Zvolit produkt");
	define("SUPPORT_SELECT_STATUS_MSG", "Vybrat stav");
	define("SUPPORT_NOT_VIEWED_MSG", "Neиtenй");
	define("SUPPORT_VIEWED_BY_USER_MSG", "Иtenй zбkaznнkem");
	define("SUPPORT_VIEWED_BY_ADMIN_MSG", "Иtenй administrбtorem");
	define("SUPPORT_STATUS_NEW_MSG", "Novб");
	define("NO_SUPPORT_REQUEST_MSG", "Ћбdnй dotazy nenalezeny");

	define("SUPPORT_SUMMARY_COLUMN", "Sumбш");
	define("SUPPORT_TYPE_COLUMN", "Typ");
	define("SUPPORT_UPDATED_COLUMN", "Naposled aktualizovбno");

	define("SUPPORT_USER_NAME_FIELD", "Vaљe jmйno");
	define("SUPPORT_USER_EMAIL_FIELD", "Vaљe emailovб adresa");
	define("SUPPORT_IDENTIFIER_FIELD", "Identifikбtor (инslo faktury)");
	define("SUPPORT_ENVIRONMENT_FIELD", "Prostшedн (OS, Databбze, Web Server, atd.)");
	define("SUPPORT_DEPARTMENT_FIELD", "Oddelenн");
	define("SUPPORT_PRODUCT_FIELD", "Produkt");
	define("SUPPORT_TYPE_FIELD", "Typ");
	define("SUPPORT_CURRENT_STATUS_FIELD", "Momentбlnн status");
	define("SUPPORT_SUMMARY_FIELD", "Jednoшбdkovэ sumбш");
	define("SUPPORT_DESCRIPTION_FIELD", "Popis");
	define("SUPPORT_MESSAGE_FIELD", "Zprбva");
	define("SUPPORT_ADDED_FIELD", "Pшidanэ");
	define("SUPPORT_ADDED_BY_FIELD", "Pшidal");
	define("SUPPORT_UPDATED_FIELD", "Naposled aktualizovбno");

	define("SUPPORT_REQUEST_BUTTON", "Odeslat poћadavku");
	define("SUPPORT_REPLY_BUTTON", "Odpovмdмt");
	                                    
	define("SUPPORT_MISS_ID_ERROR", "Schбzн <b>Podpora ID</b> parametr");
	define("SUPPORT_MISS_CODE_ERROR", "Schбzн <b>kontrolnэ</b> parametr");
	define("SUPPORT_WRONG_ID_ERROR", "<b>Podpora ID</b> parametr mб nesprбvnou hodnotu");
	define("SUPPORT_WRONG_CODE_ERROR", "<b>Kontrolnэ</b> parametr mб nesprбvnou hodnotu");

	define("MAIL_DATA_MSG", "Mail Data");
	define("HEADERS_MSG", "Headers");
	define("ORIGINAL_TEXT_MSG", "Original Text");
	define("ORIGINAL_HTML_MSG", "Original HTML");
	define("CLOSE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to close tickets.<br>");
	define("REPLY_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to reply tickets.<br>");
	define("CREATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to create new tickets.<br>");
	define("REMOVE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to remove tickets.<br>");
	define("UPDATE_TICKET_NOT_ALLOWED_MSG", "Sorry, but you are not allowed to update tickets.<br>");
	define("NO_TICKETS_FOUND_MSG", "No tickets were found.");
	define("HIDDEN_TICKETS_MSG", "Hidden Tickets");
	define("ALL_TICKETS_MSG", "All Tickets");
	define("ACTIVE_TICKETS_MSG", "Active Tickets");
	define("NOT_ASSIGNED_THIS_DEP_MSG", "You are not assigned to this department.");
	define("NOT_ASSIGNED_ANY_DEP_MSG", "You are not assigned to any department.");
	define("REPLY_TO_NAME_MSG", "Reply to {name}");
	define("KNOWLEDGE_CATEGORY_MSG", "Knowledge Category");
	define("KNOWLEDGE_TITLE_MSG", "Knowledge Title");
	define("KNOWLEDGE_ARTICLE_STATUS_MSG", "Knowledge Article Status");
	define("SELECT_RESPONSIBLE_MSG", "Select Responsible");

?>