<?php

	$default_title = "Изображениe продукта";
	check_user_security("access_products");

	/*check access*/
	$sesUsrType = get_session("session_user_type_id");
	$sesUsr = get_session("session_user_id");

	$allow_images = get_db_value("select setting_value from va_user_types_settings uts inner join va_users u on u.user_type_id = uts.type_id where u.user_type_id = " . $sesUsrType . " AND user_id=" . $sesUsr . " AND setting_name = 'access_product_images'");

	if (!$allow_images) {
		header("Location: " . get_custom_friendly_url("user_products.php"));
		exit;
	}

	$html_template = get_setting_value($block, "html_template", "block_user_product_image.html"); 
  $t->set_file("block_body", $html_template);
	$t->set_var("user_home_href",  	get_custom_friendly_url("user_home.php"));
	$t->set_var("user_products_href",  get_custom_friendly_url("user_products.php"));
	$t->set_var("user_product_href",   get_custom_friendly_url("user_product.php"));
	$t->set_var("item_image_href",   get_custom_friendly_url("user_product_image.php"));
	

	$item_id = get_param("item_id");

	$sql  = " SELECT item_name FROM " . $table_prefix . "items ";
	$sql .= " WHERE item_id=" . $db->tosql($item_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$item_name = get_translation($db->f("item_name"));
	} else {
		die(str_replace("{item_id}", $item_id, PRODUCT_ID_NO_LONGER_EXISTS_MSG));
	}


	$t->set_var("admin_upload_href", "user_upload.php");
	$t->set_var("admin_select_href", "user_select.php");
	$t->set_var("CONFIRM_DELETE_JS", str_replace("{record_name}", IMAGE_MSG, CONFIRM_DELETE_MSG));

	$t->set_var("item_id", $item_id);
	$t->set_var("item_name", $item_name);

	$full_image_url = get_setting_value($settings, "full_image_url", 1);
	$site_url = get_setting_value($settings, "site_url", "");
	if ($full_image_url){
		$t->set_var("image_site_url", $site_url);					
	} else {
		$t->set_var("image_site_url", "");					
	}

	$image_positions = array(
		array(1, "Показывать на отдельной вкладке"),
		array(2, "Показывать под основным изображением"),
	);

	$r = new VA_Record($table_prefix . "items_images");
	$r->return_page = "user_product_images.php";

	$r->add_where("image_id", INTEGER);
	$r->add_textbox("item_id", INTEGER);
	$r->change_property("item_id", DEFAULT_VALUE, $item_id);
	$r->change_property("item_id", TRANSFER, true);
	$r->add_textbox("image_title", TEXT, IMAGE_TITLE_MSG);
	$r->change_property("image_title", REQUIRED, true);
	$r->add_radio("image_position", INTEGER, $image_positions, IMAGE_POSITION_MSG);
	$r->add_textbox("image_small", TEXT, IMAGE_SMALL_MSG);
	$r->change_property("image_small", REQUIRED, true);
	$r->add_textbox("image_large", TEXT, IMAGE_LARGE_MSG);
	$r->change_property("image_large", USE_SQL_NULL, false);
	$r->add_textbox("image_super", TEXT);
	$r->add_textbox("image_description", TEXT);

	if($r->is_empty("image_position")){
		$r->set_value("image_position", 2);
	}
	
	$r->process();

	$block_parsed = true;
?>