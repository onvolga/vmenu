<?php

	$default_title = "Twitter Feed";

	$tf_recs = get_setting_value($vars, "recs", "5"); 
	$tf_nickname = get_setting_value($vars, "nickname", "");
	$tf_username = get_setting_value($vars, "username", $tf_nickname);

	$html_template = get_setting_value($block, "html_template", "block_twitter_feed.html"); 
  $t->set_file("block_body", $html_template);

	if (strlen($tf_username)) {
  	$t->set_var("tf_recs", $tf_recs);
	  $t->set_var("tf_username", htmlspecialchars($tf_username));
	  $t->parse("twitter_feeds");
	} else {
		$error_message = str_replace("{field_name}", USERNAME_FIELD, REQUIRED_MESSAGE);
	  $t->set_var("errors_list", $error_message);
	  $t->parse("twitter_errors");
	}

	$block_parsed = true;

?>