<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  block.php                                                ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");

	// set headers for block
	header("Pragma: no-cache");
	header("Expires: 0");
	header("Cache-Control: no-cache, must-revalidate");
	header("Content-Type: text/html; charset=" . CHARSET);

	$pb_id = get_param("pb_id");
	$cms_page_code = get_param("cms_page_code");

	// get block data
	$page_blocks = array();
	$sql  = " SELECT cpb.pb_id, cb.php_script, cpb.frame_id, cpb.block_key, ";
	$sql .= " cpb.html_template, cpb.block_style, cpb.css_class, ";
	$sql .= " cb.block_title, cpb.block_title AS page_block_title ";
	$sql .= " FROM (" . $table_prefix . "cms_pages_blocks cpb ";
	$sql .= " INNER JOIN " . $table_prefix . "cms_blocks cb ON cpb.block_id=cb.block_id) ";
	$sql .= " WHERE cpb.pb_id=" . $db->tosql($pb_id, INTEGER);
	$db->query($sql);
	if ($db->next_record()) {
		$block = $db->Record;
		$php_script = $db->f("php_script");
		$html_template = $db->f("html_template");
		$block_style = $db->f("block_style");
		$block_class = $db->f("css_class");
		$block_key = $db->f("block_key");
		$block_title = $db->f("block_title");
		$page_block_title = $db->f("page_block_title");
	} else {
		echo "Block wasn't found";
		return;
	}

	// get block variables
	$vars = array();
	$sql  = " SELECT cbs.pb_id, cbs.variable_name, cbs.variable_value ";
	$sql .= " FROM " . $table_prefix . "cms_blocks_settings cbs ";
	$sql .= " WHERE cbs.pb_id=" . $db->tosql($pb_id, INTEGER);
	$db->query($sql);
	while ($db->next_record()) {
		$variable_name = $db->f("variable_name");
		$variable_value = $db->f("variable_value");
		if (isset($vars[$variable_name])) {
			if (is_array($vars[$variable_name])) {
				$vars[$variable_name][] = $variable_value;
			} else {
				$vars[$variable_name] = array($vars[$variable_name]);
				$vars[$variable_name][] = $variable_value;
			}
		} else {
			$vars[$variable_name] = $variable_value;
		}
	}

	// added two additional vars to array
	$vars["block_key"] = $block_key;
	$vars["tag_name"] = "block";

	$block_parsed = false;
	$t = new VA_Template($settings["templates_dir"]);
	$t->set_var("pb_id", $pb_id);
	$t->set_var("block_style", $block_style);

	if (file_exists("./blocks_custom/".$php_script)) {
		include("./blocks_custom/".$php_script);
	} else {
		include("./blocks/".$php_script);
	}
	if ($block_parsed) {
		if (strlen($page_block_title)) { $block_title = $page_block_title; }
		if (!strlen($block_title) && isset($default_title)) { $block_title = $default_title; }
		if (!strlen($block_title)) { $block_class = "hidden-title ". $block_class; }
  
		$t->set_var("block_class", $block_class);
		$t->set_block("block_title", $block_title);
		$t->parse("block_title", false);
		$t->pparse("block_body");
	}


?>