<?php
	include_once("./admin_config.php");
	include_once($root_folder_path . "includes/common.php");
	include_once("./admin_common.php");
	include_once($root_folder_path . "includes/record.php");
	include_once($root_folder_path . "includes/friendly_functions.php");

	check_admin_security("products_reviews");

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main", "admin_blz_review.html");


	$t->set_var("CONFIRM_DELETE_JS", str_replace("{record_name}", 'комментарий', CONFIRM_DELETE_MSG));


	$r = new VA_Record($table_prefix . "blz_reviews");
	$r->return_page = "admin_blz_reviews.php";

	$r->add_where("review_id", INTEGER);
    $r->add_checkbox("shown", INTEGER);
	$r->add_textbox("review_author", TEXT, AUTHOR_NAME_MSG);
    
	$r->change_property("review_author", REQUIRED, true);

	$r->add_textbox("review", TEXT, 'Текст комментария');
    
	$r->change_property("review", REQUIRED, true);
    
	$r->process();

	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	$t->pparse("main");

?>
