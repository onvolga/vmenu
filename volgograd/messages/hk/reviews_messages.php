<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "&#36984;&#25799;&#24615;");
	define("BAD_MSG", "&#21155;");
	define("POOR_MSG", "&#24046;");
	define("AVERAGE_MSG", "&#20013;&#31561;");
	define("GOOD_MSG", "&#33391;");
	define("EXCELLENT_MSG", "&#20778;");
	define("REVIEWS_MSG", "&#30332;&#34920;");
	define("NO_REVIEWS_MSG", "&#27794;&#26377;&#30332;&#34920;");
	define("WRITE_REVIEW_MSG", "&#23531;&#30332;&#34920;");
	define("RATE_PRODUCT_MSG", "&#28858;&#27492;&#36008;&#21697;&#35413;&#20998;");
	define("RATE_ARTICLE_MSG", "&#28858;&#27492;&#25991;&#31456;&#35413;&#20998;");
	define("NOT_RATED_PRODUCT_MSG", "&#36008;&#21697;&#26410;&#26377;&#35413;&#20998;");
	define("NOT_RATED_ARTICLE_MSG", "&#25991;&#31456;&#26410;&#26377;&#35413;&#20998;");
	define("AVERAGE_RATING_MSG", "&#23458;&#25142;&#24179;&#22343;&#35413;&#20998;");
	define("BASED_ON_REVIEWS_MSG", "&#26681;&#25818; {total_votes} &#30332;&#34920;");
	define("POSITIVE_REVIEW_MSG", "&#27491;&#38754;&#23458;&#25142;&#30332;&#34920;");
	define("NEGATIVE_REVIEW_MSG", "&#36000;&#38754;&#23458;&#25142;&#30332;&#34920;");
	define("SEE_ALL_REVIEWS_MSG", "&#26597;&#30475;&#25152;&#26377;&#23458;&#25142;&#30332;&#34920;");
	define("ALL_REVIEWS_MSG", "&#25152;&#26377;&#30332;&#34920;");
	define("ONLY_POSITIVE_MSG", "&#21482;&#39023;&#31034;&#27491;&#38754;");
	define("ONLY_NEGATIVE_MSG", "&#21482;&#39023;&#31034;&#36000;&#38754;");
	define("POSITIVE_REVIEWS_MSG", "&#27491;&#38754;&#30332;&#34920;");
	define("NEGATIVE_REVIEWS_MSG", "&#36000;&#38754;&#30332;&#34920;");
	define("SUBMIT_REVIEW_MSG", "&#22810;&#35613;<br>&#20320;&#30340;&#30332;&#34920;&#23559;&#26371;&#20316;&#26597;&#38321;.&#27298;&#26597;&#24460;&#23559;&#26371;&#30332;&#20296;&#26412;&#32178;.<br>&#25105;&#20497;&#20445;&#30041;&#23113;&#25298;&#19981;&#31526;&#21512;&#35215;&#21063;&#30340;&#30332;&#34920;");
	define("ALREADY_REVIEW_MSG", "&#24050;&#19978;&#36617;&#20320;&#30340;&#30332;&#34920;");
	define("RECOMMEND_PRODUCT_MSG", "&#20320;&#26371;&#21542;&#25512;&#20171;<br> &#27492;&#36008;&#21697;&#32102;&#20854;&#20182;&#20154;?");
	define("RECOMMEND_ARTICLE_MSG", "&#20320;&#26371;&#21542;&#25512;&#20171;<br> &#27492;&#25991;&#31456;&#32102;&#20854;&#20182;&#20154;?");
	define("RATE_IT_MSG", "&#35413;&#20998;");
	define("ONE_LINE_SUMMARY_MSG", "&#19968;&#35261;");
	define("DETAILED_COMMENT_MSG", "&#35443;&#32048;&#35413;&#20729;");
	define("NAME_ALIAS_MSG", "&#22995;&#21517;&#25110;&#21029;&#21517;");
	define("SHOW_MSG", "&#39023;&#31034;");
	define("FOUND_MSG", "&#25214;&#21040;");
	define("RATING_MSG", "&#24179;&#22343;&#35413;&#20998;");

?>