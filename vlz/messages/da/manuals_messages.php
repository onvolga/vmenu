<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// manuals messages
	define("MANUALS_TITLE", "Manualer ");
	define("NO_MANUALS_MSG", "Ingen manualer fundet");
	define("NO_MANUAL_ARTICLES_MSG", "ingen artikler");
	define("MANUALS_PREV_ARTICLE", "Forrige");
	define("MANUALS_NEXT_ARTICLE", "NГ¦ste");
	define("MANUAL_CONTENT_MSG", "Indeks");
	define("MANUALS_SEARCH_IN_MSG", "SГёg i");
	define("MANUALS_SEARCH_FOR_MSG", "SГёg ");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Alle manualer");
	define("MANUALS_SEARCH_RESULTS_INFO", "Har fundet {results_number} artikler om {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Se");
	define("MANUALS_NOT_FOUND_ANYTHING", "Fandt ikke noget om '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Intet indhold");
	define("MANUALS_SEARCH_TITLE", "Manualer sГёgning");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Manualer  sГёgeresultater");

?>