<?php

	$default_title = "{SUBSCRIBE_TITLE}";

	$html_template = get_setting_value($block, "html_template", "block_subscribe.html"); 
  $t->set_file("block_body", $html_template);

	$error_desc   = "";
	$message_desc = "";
	$unsubscribe  = get_param("unsubscribe");

	$query_string = transfer_params("", true);
	$t->set_var("query_string", htmlspecialchars($query_string));

	$subscribed_email = get_param("subscribed_email");

	$subscribe_desc = str_replace("{button_name}", SUBSCRIBE_BUTTON, SUBSCRIBE_FORM_MSG);
	$t->set_var("SUBSCRIBE_FORM_MSG", $subscribe_desc);

	if (strlen($subscribed_email)) {
		if(preg_match(EMAIL_REGEXP, $subscribed_email)) {
			$sql  = " SELECT COUNT(*) FROM " . $table_prefix . "newsletters_users ";
			$sql .= " WHERE email=" . $db->tosql($subscribed_email, TEXT);
			$db->query($sql);
			$db->next_record();
			$email_count = $db->f(0);
			if($email_count > 0) {
				$message_desc = ALREADY_SUBSCRIBED_MSG;
			} else {
				$sql  = " INSERT INTO " . $table_prefix . "newsletters_users (email, date_added) ";
				$sql .= " VALUES (";
				$sql .= $db->tosql($subscribed_email, TEXT) . ", ";
				$sql .= $db->tosql(va_time(), DATETIME) . ") ";
				$db->query($sql);
				$message_desc = SUBSCRIBED_MSG;
			}
		} else {
			$error_desc = INVALID_EMAIL_MSG;
		}
	}

	if ($message_desc) {
		$t->set_var("message_desc", $message_desc);
		$t->parse("subscribe_message", false);
	}
	if ($error_desc) {
		$t->set_var("subscribed_email", htmlspecialchars($subscribed_email));
		$t->set_var("error_desc", $error_desc);
		$t->parse("subscribe_error", false);
	} else {
		$t->set_var("subscribed_email", "");
	}

	$block_parsed = true;

?>