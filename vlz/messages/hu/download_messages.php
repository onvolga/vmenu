<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//letС†ltР№s СЊzenetek
	define("DOWNLOAD_WRONG_PARAM", "Rossz letС†ltР№s paramР№ter (ek).");
	define("DOWNLOAD_MISS_PARAM", "EltС‹nt letС†ltР№s paramР№ter (ek).");
	define("DOWNLOAD_INACTIVE", "LetС†ltР№s nem aktРЅv.");
	define("DOWNLOAD_EXPIRED", "A letС†ltР№sed idС…szaka lejР±rt.");
	define("DOWNLOAD_LIMITED", "ElР№rted a maximР±lis szР±mСЉ letС†ltР№st.");
	define("DOWNLOAD_PATH_ERROR", "At elР№rР№si СЉtja a termР№knek nem talР±lhatСѓ.");
	define("DOWNLOAD_RELEASE_ERROR", "PublikР±lР±st nem talР±lhatСѓ.");
	define("DOWNLOAD_USER_ERROR", "Csak regisztrР±lt felhasznР±lСѓk tС†lthetik le ezt a fР±jlt.");
	define("ACTIVATION_OPTIONS_MSG", "AktivР±lР±si opciСѓk");
	define("ACTIVATION_MAX_NUMBER_MSG", "Az aktivР±lР±sok szР±mР±nak maximuma");
	define("DOWNLOAD_OPTIONS_MSG", "LetС†lthetС… /Szoftver opciСѓk");
	define("DOWNLOADABLE_MSG", "LetС†lthetС… (Szoftver)");
	define("DOWNLOADABLE_DESC", "LetС†lthetС… termР№knek  meg tudod hatР±rozni a 'letС†ltР№si idС…szakot', szintР№n meg lehet hatР±rozni 'LetС†ltР№s befejezС…dР№se', 'ElР№rР№se a letС†lthetС… fР±jlnak' Р№s az 'AktivР±lР±s opciСѓk' t.");
	define("DOWNLOAD_PERIOD_MSG", "LetС†ltР№si idС…szak");
	define("DOWNLOAD_PATH_MSG", "LetС†lthetС… fР±jl elР№rР№si СЉtja");
	define("DOWNLOAD_PATH_DESC", "TС†bb elР№rР№si utat is meg lehet adni, pontosvesszС…vel elvР±lasztva.");
	define("UPLOAD_SELECT_MSG", "VР±laszd ki a feltС†ltendС… fР±jlt Р№s nyomd meg a {button_name} gombot.");
	define("UPLOADED_FILE_MSG", "<b>{filename}</b> fР±jl feltС†ltС…dС†tt.");
	define("UPLOAD_SELECT_ERROR", "KР№rek kivР±lasztani egy fР±jlt elС…szС†r.");
	define("UPLOAD_IMAGE_ERROR", "Csak kР№p fР±jl tС†lthetС… fel.");
	define("UPLOAD_FORMAT_ERROR", "Ez tРЅpusСЉ fР±jl nem megengedett.");
	define("UPLOAD_SIZE_ERROR", "Nagyobb fР±jlok mint {filesize} nem megengedett.");
	define("UPLOAD_DIMENSION_ERROR", "Nagyobb kР№p mint {dimension} nem megengedett.");
	define("UPLOAD_CREATE_ERROR", "Rendszer nem tudta lР№trehozni a fР±jlt.");
	define("UPLOAD_ACCESS_ERROR", "Nem rendelkezel engedР№llyel fР±jl feltС†ltР№sР№re.");
	define("DELETE_FILE_CONFIRM_MSG", "Biztos tС†rli ezt a fР±jlt?");
	define("NO_FILES_MSG", "FР±jl nem talР±lhatСѓ");
	define("SERIAL_GENERATE_MSG", "SorozatszР±m generР±lР±s");
	define("SERIAL_DONT_GENERATE_MSG", "Ne generР±ljon");
	define("SERIAL_RANDOM_GENERATE_MSG", "GenerР±ljon vР№letlenszerС‹ sorozatszР±mot a szoftvernek");
	define("SERIAL_FROM_PREDEFINED_MSG", "Vegye a sorozatszР±mot az elС…re meghatР±rozott szР±mokat tartalmazСѓ listР±bСѓl");
	define("SERIAL_PREDEFINED_MSG", "ElС…re meghatР±rozott sorozatszР±mok");
	define("SERIAL_NUMBER_COLUMN", "SorozatszР±m");
	define("SERIAL_USED_COLUMN", "HasznР±lt");
	define("SERIAL_DELETE_COLUMN", "TС†rlР№s");
	define("SERIAL_MORE_MSG", "TС†bb sorozatszР±m hozzР±adР±sa?");
	define("SERIAL_PERIOD_MSG", "SzР№riaszР±m periСѓdus");
	define("DOWNLOAD_SHOW_TERMS_MSG", "SzabР±lyok Р№s feltР№telek megjelenРЅtР№se");
	define("DOWNLOAD_SHOW_TERMS_DESC", "A termР№k letС†ltР№sР№hez a felhasznР±lСѓnak el kel olvasnia Р№s el kell fogadnia a szabР±lyoka Р№s feltР№teleket.");
	define("DOWNLOAD_TERMS_MSG", "SzabР±lyok Р№s feltР№telek");
	define("DOWNLOAD_TERMS_USER_DESC", "Elolvastam Р№s elfogadtam a szabР±lyokat Р№s feltР№teleket!");
	define("DOWNLOAD_TERMS_USER_ERROR", "A termР№k letС†ltР№sР№hez el kell olvasnod Р№s el kell fogadnod a felhasznР±lР±si feltР№teleket Р№s szabР±lyokat.");

	define("DOWNLOAD_TITLE_MSG", "LetС†ltР№s cРЅme");
	define("DOWNLOADABLE_FILES_MSG", "LetС†lthetС… FР±jlok");
	define("DOWNLOAD_INTERVAL_MSG", "LetС†ltР№si idС…szak");
	define("DOWNLOAD_LIMIT_MSG", "LetС†ltР№si limit");
	define("DOWNLOAD_LIMIT_DESC", "a fР±jl ennyiszer lesz letС†lthetС…");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum LetС†ltР№sek szР±ma");
	define("PREVIEW_TYPE_MSG", "ElС…nР№zet tРЅpusa");
	define("PREVIEW_TITLE_MSG", "ElС…nР№zet cРЅme");
	define("PREVIEW_PATH_MSG", "ElС…nР№zeti fР±sjl URL");
	define("PREVIEW_IMAGE_MSG", "ElС…nР№zeti kР№p");
	define("MORE_FILES_MSG", "TС†bb FР±jl");
	define("UPLOAD_MSG", "FeltС†ltР№s");
	define("USE_WITH_OPTIONS_MSG", "Csak opciСѓkkal hasznР±lva");
	define("PREVIEW_AS_DOWNLOAD_MSG", "ElС…nР№zet letС†ltР№skР№nt");
	define("PREVIEW_USE_PLAYER_MSG", "ElС…nР№zet lejР±tszСѓval");
	define("PROD_PREVIEWS_MSG", "BemutatСѓk Р№s LetС†ltР№sek");
	//tР±mogatР±s СЊzenetek
?>