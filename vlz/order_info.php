<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  order_info.php                                           ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./includes/ads_functions.php");
	include_once("./includes/order_items.php");
	include_once("./includes/order_links.php");
	include_once("./includes/order_items_properties.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/profile_functions.php");
	include_once("./includes/parameters.php");
	include_once("./includes/navigator.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");

	$cms_page_code = "order_info";
	$script_name   = "order_info.php";
	$current_page  = get_custom_friendly_url("order_info.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = CHECKOUT_INFO_TITLE;

	include_once("./includes/page_layout.php");

?>
