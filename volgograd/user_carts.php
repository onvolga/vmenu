<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_carts.php                                           ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");

	check_user_security("my_carts");

	$cms_page_code = "user_carts";
	$script_name   = "user_carts.php";
	$current_page  = get_custom_friendly_url("user_carts.php");
	$auto_meta_title = MY_SAVED_CARTS_MSG;

	$tax_rates = get_tax_rates();

	include_once("./includes/page_layout.php");

?>
