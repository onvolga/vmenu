<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  download_messages.php                                    ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// download messages
	define("DOWNLOAD_WRONG_PARAM", "Nesprбvny download parameter.");
	define("DOWNLOAD_MISS_PARAM", "Chэbajъci download parameter.");
	define("DOWNLOAD_INACTIVE", "Stiahnutie neaktнvne.");
	define("DOWNLOAD_EXPIRED", "Vбљ иas na stiahnutie vyprљal.");
	define("DOWNLOAD_LIMITED", "Prekroиili ste maximбlny poиet moћnэch stiahnutн.");
	define("DOWNLOAD_PATH_ERROR", "Nemфћem nбjsќ cestu k produktu.");
	define("DOWNLOAD_RELEASE_ERROR", "Vydanie nenбjdenй");
	define("DOWNLOAD_USER_ERROR", "Iba registrovanн uћнvatelia mфћu stiahnuќ tento sъbor");
	define("ACTIVATION_OPTIONS_MSG", "Activation Options");
	define("ACTIVATION_MAX_NUMBER_MSG", "Max Number of Activations");
	define("DOWNLOAD_OPTIONS_MSG", "Downloadable / Software Options");
	define("DOWNLOADABLE_MSG", "Downloadable (Software)");
	define("DOWNLOADABLE_DESC", "for downloadable product you can also specify 'Download Period', 'Path to Downloadable File' and 'Activations Options'");
	define("DOWNLOAD_PERIOD_MSG", "Download Period");
	define("DOWNLOAD_PATH_MSG", "Path to Downloadable File");
	define("DOWNLOAD_PATH_DESC", "you could add multiple paths devided by semicolon");
	define("UPLOAD_SELECT_MSG", "Vyberte sъbor pre nahranie a stlaиte tlaинtko {button_name} .");
	define("UPLOADED_FILE_MSG", "Sъbor <b>{filename}</b> bol nahratэ.");
	define("UPLOAD_SELECT_ERROR", "Prosнm vyberte najprv nejakэ sъbor.");
	define("UPLOAD_IMAGE_ERROR", "Sъ povolenй iba sъbory typu obrбzok.");
	define("UPLOAD_FORMAT_ERROR", "This type of file is not allowed.");
	define("UPLOAD_SIZE_ERROR", "Sъbory vдиљљie ako {filesize} nie sъ dovolenй.");
	define("UPLOAD_DIMENSION_ERROR", "Sъbory rozmerov vдиљнch ako {dimension} nie sъ dovolenй.");
	define("UPLOAD_CREATE_ERROR", "Systйm nemфћe vytvoriќ sъbor.");
	define("UPLOAD_ACCESS_ERROR", "You don't have permissions to upload files.");
	define("DELETE_FILE_CONFIRM_MSG", "Are you sure you want to delete this file?");
	define("NO_FILES_MSG", "No files were found");
	define("SERIAL_GENERATE_MSG", "Generate Serial Number");
	define("SERIAL_DONT_GENERATE_MSG", "don't generate");
	define("SERIAL_RANDOM_GENERATE_MSG", "generate random serial for software product");
	define("SERIAL_FROM_PREDEFINED_MSG", "get serial number from predefined list");
	define("SERIAL_PREDEFINED_MSG", "Predefined Serial Numbers");
	define("SERIAL_NUMBER_COLUMN", "Serial Number");
	define("SERIAL_USED_COLUMN", "Used");
	define("SERIAL_DELETE_COLUMN", "Delete");
	define("SERIAL_MORE_MSG", "Add more serial numbers?");
	define("SERIAL_PERIOD_MSG", "Serial Number Period");
	define("DOWNLOAD_SHOW_TERMS_MSG", "Show Terms & Conditions");
	define("DOWNLOAD_SHOW_TERMS_DESC", "To download the product user has to read and agree to our terms and conditions");
	define("DOWNLOAD_TERMS_MSG", "Terms & Conditions");
	define("DOWNLOAD_TERMS_USER_DESC", "I have read and agree to your terms and conditions");
	define("DOWNLOAD_TERMS_USER_ERROR", "To download the product you have to read and agree to our terms and conditions");

	define("DOWNLOAD_TITLE_MSG", "Download Title");
	define("DOWNLOADABLE_FILES_MSG", "Downloadable Files");
	define("DOWNLOAD_INTERVAL_MSG", "Download Interval");
	define("DOWNLOAD_LIMIT_MSG", "Downloads Limit");
	define("DOWNLOAD_LIMIT_DESC", "number of times file can be downloaded");
	define("MAXIMUM_DOWNLOADS_MSG", "Maximum Downloads");
	define("PREVIEW_TYPE_MSG", "Preview Type");
	define("PREVIEW_TITLE_MSG", "Preview Title");
	define("PREVIEW_PATH_MSG", "Path to Preview File");
	define("PREVIEW_IMAGE_MSG", "Preview Image");
	define("MORE_FILES_MSG", "More Files");
	define("UPLOAD_MSG", "Upload");
	define("USE_WITH_OPTIONS_MSG", "Use with options only");
	define("PREVIEW_AS_DOWNLOAD_MSG", "Preview as download");
	define("PREVIEW_USE_PLAYER_MSG", "Use player to preview");
	define("PROD_PREVIEWS_MSG", "Previews");

?>