/*                           
  * ���� �������
  * MainOlMa
  * ������� ������������ ����� ����� ��� ������� - 1 ������� - 27.10.2012
*/
function initProgress(parentObjId)
{
	var parentObj = document.getElementById(parentObjId);
	var xPos = findPosX(parentObj);
	var yPos = findPosY(parentObj);
	var blockWidth = parentObj.offsetWidth;
	var blockHeight = parentObj.offsetHeight;

	var shadowObj = document.createElement("div");
	shadowObj.id = "pb_shadow_" + parentObjId;
	shadowObj.style.zIndex = "999";
	shadowObj.style.position = "absolute";
	shadowObj.style.backgroundColor = "#d0d0d0";
	shadowObj.style.textAlign = "center";
	shadowObj.style.verticalAlign = "middle";
	shadowObj.style.left = xPos + "px";
	shadowObj.style.top  = yPos + "px";
	shadowObj.style.width = blockWidth + "px";
	shadowObj.style.height = blockHeight + "px";
	shadowObj.style.opacity    = "0.6";
	shadowObj.style.mozOpacity = "0.6";
	shadowObj.style.filter     = "alpha(opacity=60)";
	shadowObj.onclick    = function() { disableProgress(parentObjId); };

	document.body.insertBefore(shadowObj, document.body.firstChild);

	for (p = 1; p <= 5; p++) {
		var prDiv = document.createElement("div");
		prDiv.id = "pb_progress_" + parentObjId + "_" + p;
		prDiv.style.zIndex = "1000";
		prDiv.style.position = "absolute";
		prDiv.style.backgroundColor = "gray";
		prDiv.style.left = xPos + Math.round(blockWidth/2) - 30 + p*10 + "px";
		prDiv.style.top  = yPos + Math.round(blockHeight/2) + "px";
		prDiv.style.width = "1px";
		prDiv.style.height = "1px";
		prDiv.style.border = "1px solid white";
		document.body.insertBefore(prDiv, document.body.firstChild);
	}
	showProgress(parentObjId, 0);
}

function showProgress(parentObjId, progress)
{
	if (progress == 15) { 
		progress = 0; 
		for (p = 1; p <= 5; p++) {
			var prObj = document.getElementById("pb_progress_" + parentObjId + "_" + p);
			if (prObj) {
				prObj.style.top = (parseInt(prObj.style.top) + 2) + "px";
				prObj.style.left = (parseInt(prObj.style.left) + 2) + "px";
				prObj.style.width = "1px";
				prObj.style.height = "1px";
			} else {
				disableProgress(parentObjId);
				return;
			}
		}
	}
  var blockNumber = Math.floor(progress / 3) + 1;
  var addSize = progress % 3;
  var posCorrection = (addSize > 0) ? 1 : 0;
	var prObj = document.getElementById("pb_progress_" + parentObjId + "_" + blockNumber);
	if (prObj) {
		prObj.style.top = (parseInt(prObj.style.top) - posCorrection) + "px";
		prObj.style.left = (parseInt(prObj.style.left) - posCorrection) + "px";
		prObj.style.width = (1 + addSize*2) + "px";
		prObj.style.height = (1 + addSize*2) + "px";
	} else {
		disableProgress(parentObjId);
		return;
	}

	progress = progress + 1;
	setTimeout("showProgress('"+parentObjId+"',"+progress+")", 200);
}

function disableProgress(parentObjId)
{
	var shadowObj = document.getElementById("pb_shadow_" + parentObjId);
	if (shadowObj) {
		document.body.removeChild(shadowObj);
	}
	for (p = 1; p <= 5; p++) {
		var prObj = document.getElementById("pb_progress_" + parentObjId + "_" + p);
		if (prObj) {
			document.body.removeChild(prObj);
		}
	}
}

function reloadBlock(pbId)
{
	var url = "block.php?pb_id=" + encodeURIComponent(pbId);
	callAjax(url, replaceBlock, pbId);
}

function replaceBlock(blockContent, pbId)
{
	var oldBlockObj = document.getElementById("pb_" + pbId);
	var parentObj = oldBlockObj.parentNode;
	var divObj = document.createElement('div'); 
	divObj.innerHTML = blockContent; 
	var newBlockObj = divObj.firstChild;
	parentObj.replaceChild(newBlockObj, oldBlockObj);
	disableProgress("pb_"+pbId);
}

function showPopupBlock(msgType, msgText, leftPos, topPos)
{
	// delete popup block if it was initialized before
	hidePopupBlock();

	var pageSize = getPageSize()
	var popupObj = document.createElement("div");
	popupObj.id = "popupBlock";
	if (msgType == "success") {
		popupObj.className = "messagebg";
	} else {
		popupObj.className = "errorbg";
	}
	popupObj.style.zIndex = "999";
	popupObj.style.position = "absolute";
	popupObj.style.width = "300px";
	popupObj.style.border = "1px solid #CED7DF";
	popupObj.style.background= "#FFFFFF";
	popupObj.style.padding = "5px";
	if (leftPos) {
		popupObj.style.left = parseInt(leftPos) + "px";
	} else {
		popupObj.style.left = pageSize[0]/2 - 150 + "px";
	}
	if (topPos) {
		popupObj.style.top  = parseInt(topPos) + "px";
	} else {
		popupObj.style.top  = pageSize[1]/2 - 20 + "px";
	}
	popupObj.style.cursor = "pointer";
	popupObj.onclick = function() { hidePopupBlock(); };

	popupObj.innerHTML = msgText;
	document.body.insertBefore(popupObj, document.body.firstChild);	
	//������� ������������ ����� ����� ��� �������
	setTimeout(function(){$("#popupBlock").fadeOut('3000');}, 3000);
}

function hidePopupBlock()
{
	var popupObj = document.getElementById("popupBlock");
	if (popupObj) {
		document.body.removeChild(popupObj);
	}
}