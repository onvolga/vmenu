<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  forum_messages.php                                       ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// forum iletileri
	define("FORUM_TITLE", "Forum");
	define("TOPIC_INFO_TITLE", "Baюlэk Bilgisi");
	define("TOPIC_MESSAGE_TITLE", "ileti");

	define("MY_FORUM_TOPICS_MSG", "Forum Koularэm");
	define("ALL_FORUM_TOPICS_MSG", "Tьm Forum Konularэ");
	define("MY_FORUM_TOPICS_DESC", "Sizin yaюadэрэnэz problemi daha цnce baюa birinin de yaюadэрэnэ hiз dьюьndьnьz mь? Deneyimlerinizi yeni kullanэcэlarla paylaюmak ister misiniz? Forum kullanэcэsэ olmaya ve topluluрa katэlmaya ne dersiniz?");
	define("NEW_TOPIC_MSG", "Yeni Konu");
	define("NO_TOPICS_MSG", "Hiз konu bulunamadэ.");
	define("FOUND_TOPICS_MSG", "<b>{search_string}</b>' ifadesine uygun <b>{found_records}</b> sonuз bulundu. ");
	define("NO_FORUMS_MSG", "Hiзbir Forum Bulunamadэ");

	define("FORUM_NAME_COLUMN", "Forum");
	define("FORUM_TOPICS_COLUMN", "Konular");
	define("FORUM_REPLIES_COLUMN", "Cevaplar");
	define("FORUM_LAST_POST_COLUMN", "Son gьncellenme");
	define("FORUM_MODERATORS_MSG", "Moderatцrler");

	define("TOPIC_NAME_COLUMN", "Konu");
	define("TOPIC_AUTHOR_COLUMN", "Yazar");
	define("TOPIC_VIEWS_COLUMN", "Gцrьntьlenme sayэsэ");
	define("TOPIC_REPLIES_COLUMN", "Cevaplar");
	define("TOPIC_UPDATED_COLUMN", "Son gьncellenme");
	define("TOPIC_ADDED_MSG", "Teюekkьr ederiz<br>Gцnderdiрiniz konu eklendi.");

	define("TOPIC_ADDED_BY_FIELD", "Ekleyen:");
	define("TOPIC_ADDED_DATE_FIELD", "Eklendi");
	define("TOPIC_UPDATED_FIELD", "Son Gьncelleme");
	define("TOPIC_NICKNAME_FIELD", "Ьye Adэ");
	define("TOPIC_EMAIL_FIELD", "Eposta Adresiniz");
	define("TOPIC_NAME_FIELD", "Konu");
	define("TOPIC_MESSAGE_FIELD", "Mesaj");
	define("TOPIC_NOTIFY_FIELD", "Cevaplarэ mail adresime gцnder.");

	define("ADD_TOPIC_BUTTON", "Konu ekle");
	define("TOPIC_MESSAGE_BUTTON", "Mesaj Ekle");

	define("TOPIC_MISS_ID_ERROR", "Eksik <b>iliюki/baр ID</b> parametrsi.");
	define("TOPIC_WRONG_ID_ERROR", " <b>iliюki/baр ID</b> parametresi yanlэю bir deрer iзermektedir.");
	define("FORUM_SEARCH_MESSAGE", "{search_string}' ifadesine uygun {search_count} iletileri bulundu.");
	define("TOPIC_PREVIEW_BUTTON", "Цnizleme");
	define("TOPIC_SAVE_BUTTON", "Kaydet");

	define("LAST_POST_ON_SHORT_MSG", "ьzerine:");
	define("LAST_POST_IN_SHORT_MSG", "iзinde:");
	define("LAST_POST_BY_SHORT_MSG", "yoluyla:");
	define("FORUM_MESSAGE_LAST_MODIFIED_MSG", "son deрiюtirilme:");

?>