<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE                                                ***
  ***      File:  user_merchant_sales.php                                  ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./includes/common.php");
	include_once("./includes/record.php");
	include_once("./includes/navigator.php");
	include_once("./includes/sorter.php");
	include_once("./includes/products_functions.php");
	include_once("./includes/shopping_cart.php");
	include_once("./messages/" . $language_code . "/cart_messages.php");

	check_user_security("merchant_sales");

	$cms_page_code = "user_merchant_sales";
	$script_name   = "user_merchant_sales.php";
	$current_page  = get_custom_friendly_url("user_merchant_sales.php");
	$tax_rates = get_tax_rates();
	$auto_meta_title = MERCHANT_SALES_MSG;

	include_once("./includes/page_layout.php");

?>
