<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  manuals_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// юinyno юinutлs
	define("MANUALS_TITLE", "Юinynai");
	define("NO_MANUALS_MSG", "Юinynш nerasta");
	define("NO_MANUAL_ARTICLES_MSG", "Nлra straipsniш");
	define("MANUALS_PREV_ARTICLE", "Ankstesnis");
	define("MANUALS_NEXT_ARTICLE", "Kitas");
	define("MANUAL_CONTENT_MSG", "Rodyklл");
	define("MANUALS_SEARCH_IN_MSG", "Ieрkoti kur");
	define("MANUALS_SEARCH_FOR_MSG", "Ieрkoti");
	define("MANUALS_SEARCH_IN_FIRST_VARIANT", "Visi юinynai");
	define("MANUALS_SEARCH_RESULTS_INFO", "Rasta {results_number} straipsnis(iш) paieрkai {search_string}");
	define("MANUALS_SEARCH_RESULT_MSG", "Radiniai");
	define("MANUALS_NOT_FOUND_ANYTHING", "Nerasta nieko ieрkant '{search_string}'");
	define("MANUAL_ARTICLE_NO_CONTENT_MSG", "Nлra turinio");
	define("MANUALS_SEARCH_TITLE", "Юinynш paieрka");
	define("MANUALS_SEARCH_RESULTS_TITLE", "Юinynш radiniai");

?>