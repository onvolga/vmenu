<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  messages.php                                             ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	define("CHARSET", "utf-8");
	// date messages
	define("YEAR_MSG", "ГЃr");
	define("YEARS_QTY_MSG", "ГЎr");
	define("MONTH_MSG", "MГЎnuГ°ur");
	define("MONTHS_QTY_MSG", "mГЎnuГ°ir");
	define("DAY_MSG", "Dagur");
	define("DAYS_MSG", "Dagar");
	define("DAYS_QTY_MSG", "dagar");
	define("HOUR_MSG", "Klukkustund");
	define("HOURS_QTY_MSG", "klukkustudir");
	define("MINUTE_MSG", "MГ­nГєta");
	define("MINUTES_QTY_MSG", "mГ­nГєtur");
	define("SECOND_MSG", "SekГєnda");
	define("SECONDS_QTY_MSG", "sekГєndur");
	define("WEEK_MSG", "Vika");
	define("WEEKS_QTY_MSG", "vikur");
	define("TODAY_MSG", "ГЌ dag");
	define("YESTERDAY_MSG", "ГЌ gГ¦r");
	define("LAST_7DAYS_MSG", "SГ­Г°ustu 7 daga");
	define("THIS_MONTH_MSG", "Гћennan mГЎnuГ°");
	define("LAST_MONTH_MSG", "ГЌ sГ­Г°asta mГЎnuГ°i");
	define("THIS_QUARTER_MSG", "Гћennan fjГіrГ°ung");
	define("THIS_YEAR_MSG", "Гћetta ГЎr");

	// months
	define("JANUARY", "JanГєar");
	define("FEBRUARY", "FebrГєar");
	define("MARCH", "Mars");
	define("APRIL", "AprГ­l");
	define("MAY", "MaГ­");
	define("JUNE", "JГєnГ­");
	define("JULY", "JГєlГ­");
	define("AUGUST", "ГЃgГєst");
	define("SEPTEMBER", "September");
	define("OCTOBER", "OktГіber");
	define("NOVEMBER", "NГіvember ");
	define("DECEMBER", "Desember");

	define("JANUARY_SHORT", "Jan");
	define("FEBRUARY_SHORT", "Feb");
	define("MARCH_SHORT", "Mar");
	define("APRIL_SHORT", "Apr");
	define("MAY_SHORT", "MaГ­");
	define("JUNE_SHORT", "JГєn");
	define("JULY_SHORT", "JГєl");
	define("AUGUST_SHORT", "ГЃgГє");
	define("SEPTEMBER_SHORT", "Sep");
	define("OCTOBER_SHORT", "Okt");
	define("NOVEMBER_SHORT", "NГіv");
	define("DECEMBER_SHORT", "Des");

	// weekdays
	define("SUNDAY", "Sunnudagur");
	define("MONDAY", "MГЎnudagur");
	define("TUESDAY", "ГћriГ°judagur");
	define("WEDNESDAY", "MiГ°vikudagur");
	define("THURSDAY", "Fimmtudagur");
	define("FRIDAY", "FГ¶studagur");
	define("SATURDAY", "Laugardagur");

	define("SUNDAY_SHORT", "Sun");
	define("MONDAY_SHORT", "MГЎn");
	define("TUESDAY_SHORT", "Гћri");
	define("WEDNESDAY_SHORT", "MiГ°");
	define("THURSDAY_SHORT", "Fim");
	define("FRIDAY_SHORT", "FГ¶s");
	define("SATURDAY_SHORT", "Lau");

	// validation messages
	define("REQUIRED_MESSAGE", "<b>{field_name}</b> er krafist");
	define("UNIQUE_MESSAGE", "GildiГ° Г­ reitnum <b>{field_name}</b> er Гѕegar Г­ gagnagrunninum");
	define("VALIDATION_MESSAGE", "AuГ°kenning tГіkst ekki fyrir reitinn <b>{field_name}</b>");
	define("MATCHED_MESSAGE", "<b>{field_one}</b> og <b>{field_two}</b> stemmdu ekki");
	define("INSERT_ALLOWED_ERROR", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° setja inn");
	define("UPDATE_ALLOWED_ERROR", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° uppfГ¦ra");
	define("DELETE_ALLOWED_ERROR", "ГћvГ­ miГ°ur, ГѕГє hefur ekki heimild til aГ° eyГ°a");
	define("ALPHANUMERIC_ALLOWED_ERROR", "AГ°eins tГ¶lur, bГіkstafir, bandstik og niГ°urstrik eru leyfГ° Г­ reitnum <b>{field_name}</b>");

	define("INCORRECT_DATE_MESSAGE", "<b>{field_name}</b> hefur ekki rГ©tt dagsetningargildi. NotiГ° almanaks dagsetningu.");
	define("INCORRECT_MASK_MESSAGE", "<b>{field_name}</b> stemmir ekki viГ° mГ¶skva. NotiГ° eftirfarandi '<b>{field_mask}</b>'");
	define("INCORRECT_EMAIL_MESSAGE", "Г“gilt netfang Г­ reitnum {field_name}");
	define("INCORRECT_VALUE_MESSAGE", "Ekki rГ©tt gildi Г­ reitnum <b>{field_name}</b>");

	define("MIN_VALUE_MESSAGE", "GildiГ° Г­ reitnum <b>{field_name}</b> mГЎ ekki vera minna en {min_value}");
	define("MAX_VALUE_MESSAGE", "GildiГ° Г­ reitnum <b>{field_name}</b> mГЎ ekki vera stГ¦rra en {max_value}");
	define("MIN_LENGTH_MESSAGE", "GildiГ° Г­ reitnum <b>{field_name}</b> mГЎ ekki vera minna en {min_length} stafir");
	define("MAX_LENGTH_MESSAGE", "GildiГ° Г­ reitnum <b>{field_name}</b> mГЎ ekki vera stГ¦rra en {max_length} stafir");

	define("FILE_PERMISSION_MESSAGE", "Hef ekki skrifrГ©ttindi ГЎ skrГЎ <b>'{file_name}'</b>. Vinsamlega breytiГ° rГ©ttindum fyrir skrГЎ ГЎГ°ur en haldiГ° er ГЎfram. ");
	define("FOLDER_PERMISSION_MESSAGE", "Hef ekki skrifrГ©ttindi ГЎ mГ¶ppu <b>'{folder_name}'</b>. Vinsamlega breytiГ° rГ©ttindum fyrir mГ¶ppu ГЎГ°ur en haldiГ° er ГЎfram.");
	define("INVALID_EMAIL_MSG", "NetfangiГ° Гѕitt er ekki gilt.");
	define("DATABASE_ERROR_MSG", "Villa Г­ gagnagrunni hefur komiГ° upp.");
	define("BLACK_IP_MSG", "Гћessi aГ°gerГ° er ekki leyfГ° af Гѕinni vГ©l.");
	define("BANNED_CONTENT_MSG", "ГћvГ­ miГ°ur, uppgefiГ° innihald inniheldur Гіleyfilega yfirlГЅsingu.");
	define("ERRORS_MSG", "Villur");
	define("REGISTERED_ACCESS_MSG", "Гћessi valkostur er aГ°eins fyrir skrГЎГ°a notendur. ");
	define("SELECT_FROM_LIST_MSG", "VeljiГ° af lista");

	// titles 
	define("TOP_RATED_TITLE", "HГ¦stu einkunnir");
	define("TOP_VIEWED_TITLE", "Mest skoГ°aГ°");
	define("RECENTLY_VIEWED_TITLE", "NГЅlega skoГ°aГ°");
	define("HOT_TITLE", "Heitt");
	define("LATEST_TITLE", "SГ­Г°ast");
	define("CONTENT_TITLE", "Innihald");
	define("RELATED_TITLE", "Tengt");
	define("SEARCH_TITLE", "Leita");
	define("ADVANCED_SEARCH_TITLE", "SГ©rhГ¦fГ° leit");
	define("LOGIN_TITLE", "innskrГЎning");
	define("CATEGORIES_TITLE", "Flokkar");
	define("MANUFACTURERS_TITLE", "FramleiГ°endur");
	define("SPECIAL_OFFER_TITLE", "SГ©rtilboГ°");
	define("NEWS_TITLE", "FrГ©ttir");
	define("EVENTS_TITLE", "AtburГ°ir");
	define("PROFILE_TITLE", "PrГіfГ­ll");
	define("USER_HOME_TITLE", "UpphafssГ­Г°a");
	define("DOWNLOAD_TITLE", "NiГ°urhal");
	define("FAQ_TITLE", "Algengar spurningar");
	define("POLL_TITLE", "KГ¶nnun");
	define("HOME_PAGE_TITLE", "UpphafssГ­Г°a");
	define("CURRENCY_TITLE", "GjaldmiГ°ill");
	define("SUBSCRIBE_TITLE", "ГЌ ГЎskrift");
	define("UNSUBSCRIBE_TITLE", "Гљr ГЎskrift");
	define("UPLOAD_TITLE", "Upphal");
	define("ADS_TITLE", "AuglГЅsingar");
	define("ADS_COMPARE_TITLE", "AuglГЅsinga samanburГ°ur");
	define("ADS_SELLERS_TITLE", "Seljendur");
	define("AD_REQUEST_TITLE", "Gera tilboГ° / Senda seljanda fyrirspurn");
	define("LANGUAGE_TITLE", "TungumГЎl");
	define("MERCHANTS_TITLE", "Kaupmenn");
	define("PREVIEW_TITLE", "ForskoГ°un");
	define("ARTICLES_TITLE", "Greinar");
	define("SITE_MAP_TITLE", "Vefkort");
	define("LAYOUTS_TITLE", "Гљtlit");

	// menu items
	define("MENU_ABOUT", "um okkur");
	define("MENU_ACCOUNT", "aГ°gangur Гѕinn");
	define("MENU_BASKET", "karfan ГѕГ­n");
	define("MENU_CONTACT", "hafa samband");
	define("MENU_DOCUMENTATION", "gГ¶gn");
	define("MENU_DOWNLOADS", "niГ°urhal");
	define("MENU_EVENTS", "atburГ°ir");
	define("MENU_FAQ", "algengar spurningar");
	define("MENU_FORUM", "spjallborГ°");
	define("MENU_HELP", "hjГЎlp");
	define("MENU_HOME", "heim");
	define("MENU_HOW", "hvernig ГЎ aГ° versla");
	define("MENU_MEMBERS", "meГ°limir");
	define("MENU_MYPROFILE", "minn prГіfГ­ll");
	define("MENU_NEWS", "frГ©ttir");
	define("MENU_PRIVACY", "friГ°helgi");
	define("MENU_PRODUCTS", "vГ¶rur");
	define("MENU_REGISTRATION", "skrГЎning");
	define("MENU_SHIPPING", "vГ¶rusending");
	define("MENU_SIGNIN", "innskrГЎning");
	define("MENU_SIGNOUT", "ГєtskrГЎning");
	define("MENU_SUPPORT", "ГѕjГіnusta");
	define("MENU_USERHOME", "mitt svГ¦Г°i");
	define("MENU_ADS", "smГЎauglГЅsingar");
	define("MENU_ADMIN", "kerfisstjГіrn");
	define("MENU_KNOWLEDGE", "Гћekkingargrunnur");

	// main terms
	define("NO_MSG", "Nei");
	define("YES_MSG", "JГЎ");
	define("NOT_AVAILABLE_MSG", "ГЃ ekki viГ°");
	define("MORE_MSG", "meira...");
	define("READ_MORE_MSG", "lesa meira...");
	define("CLICK_HERE_MSG", "smelltu hГ©r");
	define("ENTER_YOUR_MSG", "SlГЎГ°u inn");
	define("CHOOSE_A_MSG", "Veldu");
	define("PLEASE_CHOOSE_MSG", "Vinsamlegast veldu");
	define("SELECT_MSG", "Velja");
	define("DATE_FORMAT_MSG", "notiГ° eftirfarandi sniГ° <b>{date_format}</b>");
	define("NEXT_PAGE_MSG", "NГ¦sta");
	define("PREV_PAGE_MSG", "Fyrri");
	define("FIRST_PAGE_MSG", "Fyrsta");
	define("LAST_PAGE_MSG", "SГ­Г°asta");
	define("OF_PAGE_MSG", "af");
	define("TOP_CATEGORY_MSG", "Efst");
	define("SEARCH_IN_CURRENT_MSG", "NГєverandi flokkur");
	define("SEARCH_IN_ALL_MSG", "Allir flokkar");
	define("FOUND_IN_MSG", "FundiГ° Г­ ");
	define("TOTAL_VIEWS_MSG", "SkoГ°aГ° alls");
	define("VOTES_MSG", "AtkvГ¦Г°i");
	define("TOTAL_VOTES_MSG", "HeildarfjГ¶ldi atkvГ¦Г°a");
	define("TOTAL_POINTS_MSG", "HeildarfjГ¶ldi punkta");
	define("VIEW_RESULTS_MSG", "SkoГ°a niГ°urstГ¶Г°ur");
	define("PREVIOUS_POLLS_MSG", "Fyrri kannanir");
	define("TOTAL_MSG", "Samtals");
	define("CLOSED_MSG", "LokaГ°");
	define("CLOSE_WINDOW_MSG", "Loka glugga");
	define("ASTERISK_MSG", "stjГ¶rnumerkt (*) - verГ°ur aГ° fylla inn");
	define("PROVIDE_INFO_MSG", "Vinsamlega fylliГ° inn upplГЅsingar Гѕar sem merkt er meГ° rauГ°u, smelliГ° svo ГЎ '{button_name}'");
	define("FOUND_ARTICLES_MSG", "Fundist hafa <b>{found_records}</b> greinar sem innihalda '<b>{search_string}</b>'");
	define("NO_ARTICLE_MSG", "Engin grein er til meГ° Гѕessu auГ°kenni");
	define("NO_ARTICLES_MSG", "Engar greinar fundust");
	define("NOTES_MSG", "MiГ°ar");
	define("KEYWORDS_MSG", "LykilorГ°");
	define("LINK_URL_MSG", "Tengill");
	define("DOWNLOAD_URL_MSG", "NiГ°urhal");
	define("SUBSCRIBE_FORM_MSG", "Til aГ° fГЎ frГ©ttabrГ©fiГ° okkar skrifaГ°u tГ¶lvupГіstfang Гѕitt Г­ textagluggann fyrir neГ°an og smelltu ГЎ '{button_name}' .");
	define("UNSUBSCRIBE_FORM_MSG", "Vinsamlega skrifaГ°u tГ¶lvupГіstfang Гѕitt Г­ textagluggann fyrir neГ°an og smelltu ГЎ '{button_name}' .");
	define("SUBSCRIBE_LINK_MSG", "ГЌ ГЎskrift");
	define("UNSUBSCRIBE_LINK_MSG", "Гљr ГЎskrift");
	define("SUBSCRIBED_MSG", "Til hamingju! ГћГє ert nГє ГЎskrifandi aГ° frГ©ttabrГ©finu. ");
	define("ALREADY_SUBSCRIBED_MSG", "ГћГє ert Гѕegar ГЎ pГіstlista okkar. Takk fyrir. ");
	define("UNSUBSCRIBED_MSG", "ГћГє ert ekki lengur ГЎskrifandi aГ° frГ©ttabrГ©finu. Takk fyrir. ");
	define("UNSUBSCRIBED_ERROR_MSG", "ГћvГ­ miГ°ur er tГ¶lvupГіstfang Гѕitt ekki ГЎ skrГЎ hjГЎ okkur. ГћГє gГ¦tir hafa sagt upp ГЎskriftinni nГє Гѕegar. ");
	define("FORGOT_PASSWORD_MSG", "Gleymt lykilorГ°?");
	define("FORGOT_PASSWORD_DESC", "Vinsamlega skrfaГ°u tГ¶lvupГіstfangiГ° sem ГѕГє notaГ°ir viГ° skrГЎningu:");
	define("FORGOT_EMAIL_ERROR_MSG", "ГћvГ­ miГ°ur tengjast engar upplГЅsingar ГЎ skrГЎ viГ° tГ¶lvupГіstfangiГ° sem ГѕГє gafst upp.");
	define("FORGOT_EMAIL_SENT_MSG", "InnskrГЎningarleiГ°beiningar hafa veriГ° sendar ГЎ tГ¶lvupГіstfang Гѕitt. ");
	define("RESET_PASSWORD_REQUIRE_MSG", "Sumar af Гѕeim breytum sem krafist er eru ekki fyrir hendi. ");
	define("RESET_PASSWORD_PARAMS_MSG", "Gefin breyta stemmir ekki viГ° neitt Г­ gagnagrunninum. ");
	define("RESET_PASSWORD_EXPIRY_MSG", "EndurstillingarkГіГ°inn er Гєtrunninn. Vinsamlega sГ¦ktu um nГЅjan kГіГ°a til aГ° endurstilla lykilorГ° Гѕitt. ");
	define("RESET_PASSWORD_SAVED_MSG", "NГЅtt lykilorГ° hefur veriГ° vistaГ°. ");
	define("PRINTER_FRIENDLY_MSG", "PrentvГ¦nt");
	define("PRINT_PAGE_MSG", "Prenta Гѕessa sГ­Г°u");
	define("ATTACHMENTS_MSG", "ViГ°hengi");
	define("VIEW_DETAILS_MSG", "SkoГ°a nГЎnar");
	define("HTML_MSG", "HTML");
	define("PLAIN_TEXT_MSG", "Venjulegur texti");
	define("META_DATA_MSG", "Meta gГ¶gn");
	define("META_TITLE_MSG", "Titill sГ­Г°u");
	define("META_KEYWORDS_MSG", "Meta lykilorГ°");
	define("META_DESCRIPTION_MSG", "Meta lГЅsing");
	define("FRIENDLY_URL_MSG", "NotandavГ¦nt URL");
	define("IMAGES_MSG", "Myndir");
	define("IMAGE_MSG", "Mynd ");
	define("IMAGE_TINY_MSG", "PГ­nulГ­til mynd");
	define("IMAGE_TINY_ALT_MSG", "PГ­nulГ­til mynd Alt");
	define("IMAGE_SMALL_MSG", "LГ­til mynd");
	define("IMAGE_SMALL_DESC", "SГЅna ГЎ lista sГ­Г°u");
	define("IMAGE_SMALL_ALT_MSG", "LГ­til mynd Alt");
	define("IMAGE_LARGE_MSG", "StГіr mynd");
	define("IMAGE_LARGE_DESC", "sГЅna ГЎ nГЎnar sГ­Г°u");
	define("IMAGE_LARGE_ALT_MSG", "StГіr mynd Alt");
	define("IMAGE_SUPER_MSG", "RisastГіr mynd");
	define("IMAGE_SUPER_DESC", "mynd opnast Г­ nГЅjum glugga");
	define("IMAGE_POSITION_MSG", "StaГ°setning myndar");
	define("UPLOAD_IMAGE_MSG", "Senda inn mynd");
	define("UPLOAD_FILE_MSG", "Senda inn skrГЎ");
	define("SELECT_IMAGE_MSG", "Velja mynd");
	define("SELECT_FILE_MSG", "Velja skrГЎ");
	define("SHOW_BELOW_PRODUCT_IMAGE_MSG", "sГЅna mynd fyrir neГ°an stГіra vГ¶rumynd");
	define("SHOW_IN_SEPARATE_SECTION_MSG", "sГЅna mynd Г­ aГ°skildum myndahluta");
	define("IS_APPROVED_MSG", "SamГѕykkt");
	define("NOT_APPROVED_MSG", "Г“samГѕykkt");
	define("IS_ACTIVE_MSG", "Er virkt");
	define("CATEGORY_MSG", "Flokkur");
	define("SELECT_CATEGORY_MSG", "Veldu Flokk");
	define("DESCRIPTION_MSG", "LГЅsing");
	define("SHORT_DESCRIPTION_MSG", "Stutt lГЅsing");
	define("FULL_DESCRIPTION_MSG", "ГЌtarleg lГЅsing");
	define("HIGHLIGHTS_MSG", "AГ°alatriГ°i");
	define("SPECIAL_OFFER_MSG", "SГ©rtilboГ°");
	define("ARTICLE_MSG", "Grein");
	define("OTHER_MSG", "AnnaГ°");
	define("WIDTH_MSG", "Breidd");
	define("HEIGHT_MSG", "HГ¦Г°");
	define("LENGTH_MSG", "Lengd");
	define("WEIGHT_MSG", "Гћyngd");
	define("QUANTITY_MSG", "Magn");
	define("CALENDAR_MSG", "Dagatal");
	define("FROM_DATE_MSG", "FrГЎ dags.");
	define("TO_DATE_MSG", "Til dags.");
	define("TIME_PERIOD_MSG", "TГ­mabil");
	define("GROUP_BY_MSG", "Flokka eftir");
	define("BIRTHDAY_MSG", "AfmГ¦li");
	define("BIRTH_DATE_MSG", "FГ¦Г°ingardagsetning");
	define("BIRTH_YEAR_MSG", "FГ¦Г°ingarГЎr");
	define("BIRTH_MONTH_MSG", "FГ¦Г°ingarmГЎnuГ°ur");
	define("BIRTH_DAY_MSG", "FГ¦Г°ingardagur");
	define("STEP_NUMBER_MSG", "Skref {current_step} af {total_steps}");
	define("WHERE_STATUS_IS_MSG", "Гћar sem staГ°an er");
	define("ID_MSG", "AuГ°kenni");
	define("QTY_MSG", "Magn");
	define("TYPE_MSG", "Tegund");
	define("NAME_MSG", "Nafn");
	define("TITLE_MSG", "Titill");
	define("DEFAULT_MSG", "SjГЎlfgefiГ°");
	define("OPTIONS_MSG", "Valkostir");
	define("EDIT_MSG", "Breyta");
	define("CONFIRM_DELETE_MSG", "Viltu eyГ°a Гѕessu {record_name}?");
	define("DESC_MSG", "Minnkandi");
	define("ASC_MSG", "Vaxandi");
	define("ACTIVE_MSG", "Virkt");
	define("INACTIVE_MSG", "Г“virkt");
	define("EXPIRED_MSG", "ГљtrunniГ°");
	define("EMOTICONS_MSG", "Broskallar");
	define("EMOTION_ICONS_MSG", "Tilfinninga tГЎknmyndir");
	define("VIEW_MORE_EMOTICONS_MSG", "SkoГ°a fleiri broskalla");
	define("SITE_NAME_MSG", "Nafn sГ­Г°u");
	define("SITE_URL_MSG", "URL sГ­Г°u");
	define("SORT_ORDER_MSG", "Flokka pГ¶ntun");
	define("NEW_MSG", "NГЅtt");
	define("USED_MSG", "NotaГ°");
	define("REFURBISHED_MSG", "Uppgert");
	define("ADD_NEW_MSG", "BГ¦ta nГЅju viГ°");
	define("SETTINGS_MSG", "Stillingar");
	define("VIEW_MSG", "SkoГ°a");
	define("STATUS_MSG", "StaГ°a");
	define("NONE_MSG", "Ekkert");
	define("PRICE_MSG", "VerГ°");
	define("TEXT_MSG", "Texti");
	define("WARNING_MSG", "ViГ°vГ¶run");
	define("HIDDEN_MSG", "FaliГ°");
	define("CODE_MSG", "KГіГ°i");
	define("LANGUAGE_MSG", "TungumГЎl");
	define("DEFAULT_VIEW_TYPE_MSG", "SjГЎlfgefin sГЅnisgerГ°");
	define("CLICK_TO_OPEN_SECTION_MSG", "SmelliГ° til aГ° opna hluta");
	define("CURRENCY_WRONG_VALUE_MSG", "GjaldmiГ°ilskГіГ°i hefur ekki rГ©tt gildi.");
	define("TRANSACTION_AMOUNT_DOESNT_MATCH_MSG", "<b>UpphГ¦Г° til greiГ°slu</b> og <b>UpphГ¦Г° pГ¶ntunar</b> stemmir ekki.");
	define("STATUS_CANT_BE_UPDATED_MSG", "StaГ°a pГ¶ntunar #{order_id} er ekki hГ¦gt aГ° uppfГ¦ra.");
	define("CANT_FIND_STATUS_MSG", "StaГ°a finnst ekki fyrir auГ°kenni:{status_id}");
	define("NOTIFICATION_SENT_MSG", "Tilkynning send");
	define("AUTO_SUBMITTED_PAYMENT_MSG", "SjГЎlfvirk sending greiГ°slu");
	define("FONT_METRIC_FILE_ERROR", "Gat ekki innifaliГ° leturgerГ°arskrГЎ");
	define("PER_LINE_MSG", "hver lГ­na");
	define("PER_LETTER_MSG", "hvert brГ©f");
	define("PER_NON_SPACE_LETTER_MSG", "hvert brГ©f, ГЎn bila");
	define("LETTERS_ALLOWED_MSG", "leyfГ° brГ©f");
	define("LETTERS_ALLOWED_PER_LINEMSG", "leyfГ° brГ©f Г­ hverri lГ­nu");
	define("RENAME_MSG", "EndurskГ­ra");
	define("IMAGE_FORMAT_ERROR_MSG", "GD library Гѕekkir ekki Гѕetta myndasniГ°");
	define("GD_LIBRARY_ERROR_MSG", "GD library er ekki hlaГ°iГ°");
	define("INVALID_CODE_MSG", "Г“gildur kГіГ°i: ");
	define("INVALID_CODE_TYPE_MSG", "Г“gild gerГ° kГіГ°a: ");
	define("INVALID_FILE_EXTENSION_MSG", "Г“gild skrГЎarnafnsending:");
	define("FOLDER_WRITE_PERMISSION_MSG", "Mappan er ekki til eГ°a Гѕig skortir tilskilin leyfi. ");
	define("UNDEFINED_RECORD_PARAMETER_MSG", "Г“skilgreind fГ¦rslubreyta: <b>{parameter_name}</b>");
	define("MAX_RECORDS_LIMITATION_MSG", "ГћГє getur ekki bГ¦tt viГ° fleiri en <b>{max_records}</b> {records_name} fyrir ГѕГ­na ГєtgГЎfu");
	define("ACCESS_DENIED_MSG", "ГћГє hefur ekki aГ°gang aГ° Гѕessum hluta. ");
	define("DELETE_RECORDS_BEFORE_PROCEED_MSG", "Vinsamlega eyГ°iГ° einhverjum {records_name} ГЎГ°ur en haldiГ° er ГЎfram.");
	define("FOLDER_DOESNT_EXIST_MSG", "Mappan er ekki til: ");
	define("FILE_DOESNT_EXIST_MSG", "SkrГЎin er ekki til: ");
	define("PARSE_ERROR_IN_BLOCK_MSG", "ГћГЎttunarvilla Г­ blokk: ");
	define("BLOCK_DOENT_EXIST_MSG", "Blokk er ekki til: ");
	define("NUMBER_OF_ELEMENTS_MSG", "FjГ¶ldi atriГ°a");
	define("MISSING_COMPONENT_MSG", "Vantar ГѕГЎtt/breytu");
	define("RELEASES_TITLE", "ГљtgefiГ° efni");
	define("DETAILED_MSG", "NГЎkvГ¦mt");
	define("LIST_MSG", "Listi");
	define("READONLY_MSG", "Ekki skrifanlegt");
	define("CREDIT_MSG", "ГЌ reikning");
	define("ONLINE_MSG", "Tengdur");
	define("OFFLINE_MSG", "Г“tengdur");
	define("SMALL_CART_MSG", "LГ­til kerra");
	define("NEVER_MSG", "Aldrei");
	define("SEARCH_EXACT_WORD_OR_PHRASE", "NГЎkvГ¦mt orГ°alag eГ°a setning");
	define("SEARCH_ONE_OR_MORE", "Eitt eГ°a fleira af Гѕessum orГ°um");
	define("SEARCH_ALL", "Г–ll Гѕessi orГ°");
	define("RELATED_ARTICLES_MSG", "Related Articles");
	define("RELATED_FORUMS_MSG", "Related Forums");
	define("APPEARANCE_MSG", "Appearance");
	define("REMOVE_FILTER_MSG", "remove filter");
	// email & SMS messages
	define("RECORD_UPDATED_MSG", "The record has been successfully updated.");
	define("RECORD_ADDED_MSG", "New record has been successfully added.");
	define("RECORD_DELETED_MSG", "The record has been successfully deleted.");
	define("FAST_PRODUCT_ADDING_MSG", "Fast Product Adding");

	define("CURRENT_SUBSCRIPTION_MSG", "Current Subscription");
	define("SUBSCRIPTION_EXPIRATION_MSG", "Subscription expiration date");
	define("UPGRADE_DOWNGRADE_MSG", "Upgrade/Downgrade");
	define("SUBSCRIPTION_MONEY_BACK_MSG", "Subscription Money Back");
	define("MONEY_TO_CREDITS_BALANCE_MSG", "money will be added to your credits balance");
	define("USED_VOUCHERS_MSG", "Used Vouchers");
	define("VOUCHERS_TOTAL_MSG", "Vouchers Total");
	define("SUBSCRIPTIONS_GROUPS_MSG", "Subscriptions Groups");
	define("SUBSCRIPTIONS_GROUP_MSG", "Subscriptions Group");
	define("SUBSCRIPTIONS_MSG", "Subscriptions");
	define("SUBSCRIPTION_START_DATE_MSG", "Subscription Start Date");
	define("SUBSCRIPTION_EXPIRY_DATE_MSG", "Subscription Expiration Date");
	define("RECALCULATE_COMMISSIONS_AND_POINTS_MSG", "Automatically recalculate commissions and points for this item using price value");
	define("SUBSCRIPTION_PAGE_MSG", "Subscription page");
	define("SUBSCRIPTION_WITHOUT_REGISTRATION_MSG", "User can add subscriptions to his cart without registration");
	define("SUBSCRIPTION_REQUIRE_REGISTRATION_MSG", "User must have an account before accessing subscription page");

	define("MATCH_EXISTED_PRODUCTS_MSG", "Match Existing Products");
	define("MATCH_BY_ITEM_CODE_MSG", "by product code");
	define("MATCH_BY_MANUFACTURER_CODE_MSG", "by manufacturer code");
	define("DISPLAY_COMPONENTS_AND_OPTIONS_LIST_MSG", "Display options and components list");
	define("AS_LIST_MSG", "as list");
	define("AS_TABLE_MSG", "as table");
	// account messages
	define("ACCOUNT_SUBSCRIPTION_MSG", "Account subscription");
	define("ACCOUNT_SUBSCRIPTION_DESC", "To activate his account user needs to pay the subscription fee");
	define("SUBSCRIPTION_CANCELLATION_MSG", "Subscription cancellation");
	define("CONFIRM_CANCEL_SUBSCRIPTION_MSG", "Are you sure you want cancel this subscription?");
	define("CONFIRM_RETURN_SUBSCRIPTION_MSG", "Are you sure you want cancel this subscription and return {credits_amount} to balance?");
	define("CANCEL_SUBSCRIPTION_MSG", "Cancel Subscription");
	define("DONT_RETURN_MONEY_MSG", "Don't return the money");
	define("RETURN_MONEY_TO_CREDITS_BALANCE_MSG", "Return money for unused period to credits balance");
	define("UPGRADE_DOWNGRADE_TYPE_MSG", "Can user upgrade/downgrade his account type");

	define("PREDEFINED_TYPES_MSG", "Predefined Types");
	define("SHIPPING_TAX_PERCENT_MSG", "Shipping Tax Percent");
	define("PACKAGES_NUMBER_MSG", "Number of Packages");
	define("PER_PACKAGE_MSG", "per package");
	define("CURRENCY_SHOW_DESC", "user can choose this currency in which prices will be shown");
	define("CURRENCY_DEFAULT_SHOW_DESC", "all prices shown by default in this currency");
	define("RECOMMENDED_MSG", "Recomended");
	define("UPDATE_STATUS_MSG", "Update status");

	define("FIRST_CONTROLS_ARE_FREE_MSG", "Fyrstu {free_price_amount} stjГіrntГ¦kin eru Гіkeypis");
	define("FIRST_LETTERS_ARE_FREE_MSG", "Fyrstu {free_price_amount} brГ©fin eru Гіkeypis");
	define("FIRST_NONSPACE_LETTERS_ARE_FREE_MSG", "Fyrstu  {free_price_amount} brГ©f ГЎn bila eru Гіkeypis");

	// email & SMS messages
	define("EMAIL_NOTIFICATION_MSG", "Tilkynning meГ° tГ¶lvupГіsti");
	define("EMAIL_NOTIFICATION_ADMIN_MSG", "KerfisstjГіrnartilkynning meГ° tГ¶lvupГіsti");
	define("EMAIL_NOTIFICATION_USER_MSG", "Nota tilkynningu meГ° tГ¶lvupГіsti");
	define("EMAIL_SEND_ADMIN_MSF", "Senda tilkynningu til kerfisstjГіra");
	define("EMAIL_SEND_USER_MSG", "Senda tilkynningu til notanda");
	define("EMAIL_USER_IF_STATUS_MSG", "Senda tilkynningu til notanda Гѕegar stГ¶Г°u er bГ¦tt viГ°");
	define("EMAIL_TO_MSG", "Til ");
	define("EMAIL_TO_USER_DESC", "TГ¶lvupГіstfang notanda notaГ° ef ekkert er sett inn");
	define("EMAIL_FROM_MSG", "FrГЎ");
	define("EMAIL_CC_MSG", "Cc");
	define("EMAIL_BCC_MSG", "Bcc");
	define("EMAIL_REPLY_TO_MSG", "Senda svar til");
	define("EMAIL_RETURN_PATH_MSG", "Svar mun berast til");
	define("EMAIL_SUBJECT_MSG", "Efni");
	define("EMAIL_MESSAGE_TYPE_MSG", "Tegund skeytis");
	define("EMAIL_MESSAGE_MSG", "Skeyti");
	define("SMS_NOTIFICATION_MSG", "SMS tilkynning");
	define("SMS_NOTIFICATION_ADMIN_MSG", "SMS kerfisstjГіratilkynning");
	define("SMS_NOTIFICATION_USER_MSG", "SMS notandatilkynning");
	define("SMS_SEND_ADMIN_MSF", "Senda SMS tilkynningu ГЎ kerfisstjГіra");
	define("SMS_SEND_USER_MSG", "Senda SMS tilkynningu ГЎ notanda");
	define("SMS_USER_IF_STATUS_MSG", "Senda SMS tilkynningu ГЎ notanda Гѕegar stГ¶Г°u er bГ¦tti viГ°");
	define("SMS_RECIPIENT_MSG", "SMS viГ°takandi");
	define("SMS_RECIPIENT_ADMIN_DESC", "farsГ­manГєmer kerfisstjГіra");
	define("SMS_RECIPIENT_USER_DESC", "FarsГ­mareitur notaГ°ur ef ekkert er sett inn");
	define("SMS_ORIGINATOR_MSG", "SMS uppruni");
	define("SMS_MESSAGE_MSG", "SMS skilaboГ°");

	// account messages
	define("LOGIN_AS_MSG", "ГћГє ert skrГЎГ°ur inn sem");
	define("LOGIN_INFO_MSG", "InnskrГЎningar upplГЅsingar");
	define("ACCESS_HOME_MSG", "Til aГ° opna aГ°gang Гѕinn");
	define("REMEMBER_LOGIN_MSG", "Muna notandanafn og lykilorГ°");
	define("ENTER_LOGIN_MSG", "SlГЎГ°u inn notandanafn Гѕitt og lykilorГ° til aГ° halda ГЎfram");
	define("LOGIN_PASSWORD_ERROR", "LykilorГ° eГ°a notandanafn er ekki rГ©tt");
	define("ACCOUNT_APPROVE_ERROR", "ГћvГ­ miГ°ur, aГ°gangur Гѕinn hefur ekki enn veriГ° samГѕykktur. ");
	define("ACCOUNT_EXPIRED_MSG", "AГ°gangur Гѕinn er Гєtrunninn.");
	define("NEW_PROFILE_ERROR", "ГћГє hefur ekki heimild til aГ° opna aГ°gang.");
	define("EDIT_PROFILE_ERROR", "ГћГє hefur ekki heimild til aГ° breyta Гѕessu sniГ°i.");
	define("CHANGE_DETAILS_MSG", "Breyta upplГЅsingum um Гѕig");
	define("CHANGE_DETAILS_DESC", "SmelliГ° ГЎ hlekkinn hГ©r aГ° ofan til aГ° breyta aГ°gangsupplГЅsingum ГѕГ­num.");
	define("CHANGE_PASSWORD_MSG", "Breyta lykilorГ°i");
	define("CHANGE_PASSWORD_DESC", "MeГ° ГѕvГ­ aГ° smella ГЎ hlekkinn hГ©r fyrir neГ°an geturГ°u breytt lykilorГ°i ГѕГ­nu. ");
	define("SIGN_UP_MSG", "SkrГЎ mig nГєna");
	define("MY_ACCOUNT_MSG", "AГ°gangur minn");
	define("NEW_USER_MSG", "NГЅr notandi");
	define("EXISTS_USER_MSG", "NГєverandi notendur");
	define("EDIT_PROFILE_MSG", "Breyta sniГ°i");
	define("PERSONAL_DETAILS_MSG", "PersГіnuupplГЅsingar");
	define("DELIVERY_DETAILS_MSG", "UpplГЅsingar um heimsendingu");
	define("SAME_DETAILS_MSG", "Ef upplГЅsingar um heimsendingu eru ГѕГ¦r sГ¶mu og aГ° ofan, hakiГ° hГ©r <br> ef ekki, vinsamlega fylliГ° Гєt upplГЅsingarnar aГ° neГ°an");
	define("DELIVERY_MSG", "Heimsending");
	define("SUBSCRIBE_CHECKBOX_MSG", "HakaГ°u viГ° hГ©r ef ГѕГє vilt fГЎ frГ©ttabrГ©f okkar. ");
	define("ADDITIONAL_DETAILS_MSG", "NГЎnari upplГЅsingar");
	define("GUEST_MSG", "Gestur");
	// compare messages
	// ads messages
	define("MY_ADS_MSG", "MГ­nar auglГЅsingar");
	define("MY_ADS_DESC", "Ef ГѕaГ° er eitthvaГ° sem ГѕГє Гѕarft aГ° selja geturГ°u auglГЅst hГ©r. ГћaГ° er fljГіtlegt og auГ°velt aГ° setja inn auglГЅsingu.");
	define("AD_GENERAL_MSG", "GrunnupplГЅsingar um auglГЅsingu");
	define("ALL_ADS_MSG", "Allar auglГЅsingar");
	define("AD_SELLER_MSG", "Seljandi");
	define("AD_START_MSG", "Upphafsdagsetning");
	define("AD_RUNS_MSG", "Dagar Г­ birtingu");
	define("AD_QTY_MSG", "Magn");
	define("AD_AVAILABILITY_MSG", "LagerstaГ°a");
	define("AD_COMPARED_MSG", "Leyfa samanburГ° auglГЅsinga");
	define("AD_UPLOAD_MSG", "Senda inn mynd");
	define("AD_DESCRIPTION_MSG", "LГЅsing");
	define("AD_SHORT_DESC_MSG", "Stutt lГЅsing");
	define("AD_FULL_DESC_MSG", "ГЌtarleg lГЅsing");
	define("AD_LOCATION_MSG", "StaГ°setning");
	define("AD_LOCATION_INFO_MSG", "NГЎnari upplГЅsingar");
	define("AD_PROPERTIES_MSG", "Eiginleikar auglГЅsingar");
	define("AD_SPECIFICATION_MSG", "SГ©rkenni auglГЅsingar");
	define("AD_MORE_IMAGES_MSG", "Fleiri myndir");
	define("AD_IMAGE_DESC_MSG", "LГЅsing myndar");
	define("AD_DELETE_CONFIRM_MSG", "Viltu eyГ°a Гѕessari auglГЅsingu?");
	define("AD_NOT_APPROVED_MSG", "Ekki samГѕykkt");
	define("AD_RUNNING_MSG", "Keyrir");
	define("AD_CLOSED_MSG", "LokaГ°");
	define("AD_NOT_STARTED_MSG", "Ekki byrjaГ°");
	define("AD_NEW_ERROR", "ГћГє hefur ekki heimild til aГ° gera nГЅja auglГЅsingu.");
	define("AD_EDIT_ERROR", "ГћГє hefur ekki heimild til aГ° breyta Гѕessari auglГЅsingu. ");
	define("AD_DELETE_ERROR", "ГћГє hefur ekki heimild ti laГ° eyГ°a Гѕessari auglГЅsingu. ");
	define("NO_ADS_MSG", "Engar auglГЅsingar fundust.");
	define("NO_AD_MSG", "Engin auglГЅsing meГ° Гѕessu auГ°kenni er til Г­ Гѕessum flokki. ");
	define("FOUND_ADS_MSG", "Fundist hafa <b>{found_records}</b> auglГЅsingar sem innihalda (s) '<b>{search_string}</b>'");
	define("AD_OFFER_MESSAGE_MSG", "TilboГ°");
	define("AD_OFFER_LOGIN_ERROR", "ГћГє Гѕarfta aГ° vera skrГЎГ°ur inn til aГ° halda ГЎfram. ");
	define("AD_REQUEST_BUTTON", "Senda fyrirspurn");
	define("AD_SENT_MSG", "TilboГ° Гѕitt hefur veriГ° sent.");
	define("ADS_SETTINGS_MSG", "Ads Settings");
	define("ADS_DAYS_MSG", "Days to run Ad");
	define("ADS_HOT_DAYS_MSG", "Days to run Hot Ad");
	define("AD_HOT_OFFER_MSG", "Hot Offer");
	define("AD_HOT_ACTIVATE_MSG", "Add this Ad to Hot Offers section");
	define("AD_HOT_START_MSG", "Hot Offer Start Date");
	define("AD_HOT_DESCRIPTION_MSG", "Hot Description");
	define("ADS_SPECIAL_DAYS_MSG", "Days to run Special Ad");
	define("AD_SPECIAL_OFFER_MSG", "Special Offer");
	define("AD_SPECIAL_ACTIVATE_MSG", "Add this Ad to Special Offers section");
	define("AD_SPECIAL_START_MSG", "Special Offer Start Date");
	define("AD_SPECIAL_DESCRIPTION_MSG", "Special Offer Description");
	define("AD_SHOW_ON_SITE_MSG", "Show on site");
	define("AD_CREDITS_BALANCE_ERROR", "Not enough credits on your balance, you need {more_credits} more to post this Ad.");
	define("AD_CREDITS_MSG", "Ad Credits");
	define("ADS_SPECIAL_OFFERS_SETTINGS_MSG", "Ads Special Offers Settings");

	define("EDIT_DAY_MSG", "Edit Day");
	define("DAYS_PRICE_MSG", "Days Price");
	define("ADS_PUBLISH_PRICE_MSG", "Price to post Ad");
	define("DAYS_NUMBER_MSG", "Number of Days");
	define("DAYS_TITLE_MSG", "Days Title");
	define("USER_ADS_LIMIT_MSG", "Number of ads can be added by user");
	define("USER_ADS_LIMIT_DESC", "leave this field blank if you do not want to limit number of ads user can post");
	define("USER_ADS_LIMIT_ERROR", "Sorry, but you are not allowed to add more than {ads_limit} ads.");
	// controls
	define("ADS_SHOW_TERMS_MSG", "Show Terms & Conditions");
	define("ADS_SHOW_TERMS_DESC", "To submit an ad user has to read and agree to our terms and conditions");
	define("ADS_TERMS_MSG", "Terms & Conditions");
	define("ADS_TERMS_USER_DESC", "I have read and agree to your terms and conditions");
	define("ADS_TERMS_USER_ERROR", "To submit an ad you have to read and agree to our terms and conditions");
	define("ADS_ACTIVATION_MSG", "Ads Activation");
	define("ACTIVATE_ADS_MSG", "Activate Ads");
	define("ACTIVATE_ADS_NOTE", "automatically activate all user ads if his status changed to 'Approved'");
	define("DEACTIVATE_ADS_MSG", "Deactivate Ads");
	define("DEACTIVATE_ADS_NOTE", "automatically deactivate all user ads if his status changed to 'Not Approved'");

	define("MIN_ALLOWED_ADS_PRICE_MSG", "Minimum Allowed Price");
	define("MIN_ALLOWED_ADS_PRICE_NOTE", "leave this field blank if you do not want to limit the lower price for ads");
	define("MAX_ALLOWED_ADS_PRICE_MSG", "Maximum Allowed Price");
	define("MAX_ALLOWED_ADS_PRICE_NOTE", "leave this field blank if you do not want to limit the higher price for ads");

	// search message
	define("SEARCH_FOR_MSG", "Leita aГ°");
	define("SEARCH_IN_MSG", "Leita Г­");
	define("SEARCH_TITLE_MSG", "Titill");
	define("SEARCH_CODE_MSG", "KГіГ°i");
	define("SEARCH_SHORT_DESC_MSG", "Stutt lГЅsing");
	define("SEARCH_FULL_DESC_MSG", "ГЌtarleg lГЅsing");
	define("SEARCH_CATEGORY_MSG", "Leita Г­ flokki");
	define("SEARCH_MANUFACTURER_MSG", "FramleiГ°andi");
	define("SEARCH_SELLER_MSG", "Seljandi");
	define("SEARCH_PRICE_MSG", "VerГ°bil");
	define("SEARCH_WEIGHT_MSG", "ГћyngdartakmГ¶rk");
	define("SEARCH_RESULTS_MSG", "LeitarniГ°urstГ¶Г°ur");
	define("FULL_SITE_SEARCH_MSG", "Leita ГЎ allri sГ­Г°unni");

	// compare messages
	define("COMPARE_MSG", "Bera saman");
	define("COMPARE_REMOVE_MSG", "FjarlГ¦gja");
	define("COMPARE_REMOVE_HELP_MSG", "Smelltu hГ©r til aГ° fjarlГ¦gja Гѕessa vГ¶ru af samanburГ°artГ¶flunni");
	define("COMPARE_MIN_ALLOWED_MSG", "ГћГє verГ°ur aГ° velja minnst 2 vГ¶rur");
	define("COMPARE_MAX_ALLOWED_MSG", "ГћГє getur ekki valiГ° fleiri en 5 vГ¶rur");
	define("COMPARE_PARAM_ERROR_MSG", "SamanburГ°arbreytan hefur ekki rГ©tt gildi");

	// Tell a friend messages
	define("TELL_FRIEND_TITLE", "Segja vini");
	define("TELL_FRIEND_SUBJECT_MSG", "Vinur Гѕinn hefur sent ГѕГ©r Гѕennan hlekk.");
	define("TELL_FRIEND_DEFAULT_MSG", "HГ¦ {friend_name} - MГ©r datt Г­ hug aГ° ГѕГє hefГ°ir ГЎhuga ГЎ aГ° skoГ°a {item_title} ГЎ Гѕessari sГ­Г°u {item_url}");
	define("TELL_YOUR_NAME_FIELD", "NafniГ° Гѕitt");
	define("TELL_YOUR_EMAIL_FIELD", "Netfang Гѕitt");
	define("TELL_FRIENDS_NAME_FIELD", "Nafn vinar");
	define("TELL_FRIENDS_EMAIL_FIELD", "Netfang vinar");
	define("TELL_COMMENT_FIELD", "Athugasemdir");
	define("TELL_FRIEND_PRIVACY_NOTE_MSG", "ATHUGIГђ FRIГђHELGI: ViГ° munum ekki vista eГ°a nota tГ¶lvupГіstfang Гѕitt eГ°a vinar ГѕГ­ns Г­ neinum Г¶Г°rum tilgangi. ");
	define("TELL_SENT_MSG", "SkilaboГ°in hafa veriГ° send!<br>Takk fyrir!");
	define("TELL_FRIEND_MESSAGE_MSG", "Datt Г­ hug aГ° ГѕГє hefГ°ir ГЎhuga ГЎ aГ° skoГ°a {item_title} ГЎ {item_url}\\n\\n{user_name} sendi ГѕГ©r skilaboГ°:\\n{user_comment}");
	define("TELL_FRIEND_PARAM_MSG", "Kynna URL vinar");
	define("TELL_FRIEND_PARAM_DESC", "bГ¦ta URL breytu vinar Г­ 'Segja vini' hlekk ef hann er til fyrir notanda");
	define("FRIEND_COOKIE_EXPIRES_MSG", "Vina kaka rennur Гєt");

	define("CONTACT_US_TITLE", "Hafa samband");
	define("CONTACT_USER_NAME_FIELD", "Nafn");
	define("CONTACT_USER_EMAIL_FIELD", "TГ¶lvupГіstfang");
	define("CONTACT_SUMMARY_FIELD", "Einnar lГ­nu samantekt");
	define("CONTACT_DESCRIPTION_FIELD", "LГЅsing");
	define("CONTACT_REQUEST_SENT_MSG", "BeiГ°ni ГѕГ­n hefur veriГ° send");

	// buttons
	define("GO_BUTTON", "Byrja");
	define("CONTINUE_BUTTON", "ГЃfram");
	define("BACK_BUTTON", "Til baka");
	define("NEXT_BUTTON", "NГ¦sta");
	define("PREV_BUTTON", "Fyrri");
	define("SIGN_IN_BUTTON", "SkrГЎ inn");
	define("LOGIN_BUTTON", "InnskrГЎning");
	define("LOGOUT_BUTTON", "ГљtskrГЎning");
	define("SEARCH_BUTTON", "Leita");
	define("RATE_IT_BUTTON", "Gefa einkunn!");
	define("ADD_BUTTON", "BГ¦ta viГ°");
	define("UPDATE_BUTTON", "UppfГ¦ra");
	define("APPLY_BUTTON", "Breyta");
	define("REGISTER_BUTTON", "SkrГЎ  ");
	define("VOTE_BUTTON", "KjГіsa");
	define("CANCEL_BUTTON", "HГ¦tta viГ°");
	define("CLEAR_BUTTON", "Hreinsa");
	define("RESET_BUTTON", "Endursetja");
	define("DELETE_BUTTON", "EyГ°a");
	define("DELETE_ALL_BUTTON", "EyГ°a Г¶llu");
	define("SUBSCRIBE_BUTTON", "ГЌ ГЎskrift");
	define("UNSUBSCRIBE_BUTTON", "Гљr ГЎskrift");
	define("SUBMIT_BUTTON", "Senda inn");
	define("UPLOAD_BUTTON", "HlaГ°a upp");
	define("SEND_BUTTON", "Senda   ");
	define("PREVIEW_BUTTON", "SkoГ°a");
	define("FILTER_BUTTON", "SГ­a");
	define("DOWNLOAD_BUTTON", "SГ¦kja");
	define("REMOVE_BUTTON", "FjarlГ¦gja");
	define("EDIT_BUTTON", "Breyta");
	define("CHANGE_BUTTON", "Change");
	define("SAVE_BUTTON", "Save");

	// controls
	define("CHECKBOXLIST_MSG", "Valglugga listi");
	define("LABEL_MSG", "Merki");
	define("LISTBOX_MSG", "Listagluggi");
	define("RADIOBUTTON_MSG", "Гљtvarpshnappar");
	define("TEXTAREA_MSG", "TextasvГ¦Г°i");
	define("TEXTBOX_MSG", "Textagluggi");
	define("TEXTBOXLIST_MSG", "Textaglugga listi");
	define("IMAGEUPLOAD_MSG", "Senda inn mynd");
	define("CREDIT_CARD_MSG", "GreiГ°slukort");
	define("GROUP_MSG", "HГіpur");

	// fields
	define("LOGIN_FIELD", "Notandanafn");
	define("PASSWORD_FIELD", "LykilorГ°");
	define("CONFIRM_PASS_FIELD", "StaГ°festa lykilorГ°");
	define("NEW_PASS_FIELD", "NГЅtt lykilorГ°");
	define("CURRENT_PASS_FIELD", "NГєverandi lykilorГ°");
	define("FIRST_NAME_FIELD", "Fornafn");
	define("LAST_NAME_FIELD", "Eftirnafn");
	define("NICKNAME_FIELD", "GГ¦lunafn");
	define("PERSONAL_IMAGE_FIELD", "PersГіnuleg mynd");
	define("COMPANY_SELECT_FIELD", "FyrirtГ¦ki");
	define("SELECT_COMPANY_MSG", "Veldu fyrirtГ¦ki");
	define("COMPANY_NAME_FIELD", "Nafn fyrirtГ¦kis");
	define("EMAIL_FIELD", "TГ¶lvupГіstfang");
	define("STREET_FIRST_FIELD", "Heimilisfang 1");
	define("STREET_SECOND_FIELD", "Heimilisfang 2");
	define("CITY_FIELD", "Borg");
	define("PROVINCE_FIELD", "HГ©raГ°");
	define("SELECT_STATE_MSG", "Velja fylki");
	define("STATE_FIELD", "Fylki");
	define("ZIP_FIELD", "PГіstnГєmer");
	define("SELECT_COUNTRY_MSG", "Velja land");
	define("COUNTRY_FIELD", "Land");
	define("PHONE_FIELD", "SГ­mi");
	define("DAYTIME_PHONE_FIELD", "SГ­mi ГЎ daginn");
	define("EVENING_PHONE_FIELD", "SГ­mi ГЎ kvГ¶ldin");
	define("CELL_PHONE_FIELD", "FarsГ­mi");
	define("FAX_FIELD", "FaxnГєmer");
	define("VALIDATION_CODE_FIELD", "AuГ°kenniskГіГ°i");
	define("AFFILIATE_CODE_FIELD", "KГіГ°i tengiliГ°ar");
	define("AFFILIATE_CODE_HELP_MSG", "Vinsamlega notiГ° eftirfarandi URL {affiliate_url} til aГ° bГєa til hlekk sem tengist sГ­Г°unni Гѕinni");
	define("PAYPAL_ACCOUNT_FIELD", "PayPal aГ°gangur");
	define("TAX_ID_FIELD", "SkattanГєmer");
	define("MSN_ACCOUNT_FIELD", "MSN aГ°gangur");
	define("ICQ_NUMBER_FIELD", "ICQ nГєmer");
	define("USER_SITE_URL_FIELD", "URL ГЎ vef notanda");
	define("HIDDEN_STATUS_FIELD", "StaГ°a falin");
	define("HIDE_MY_ONLINE_STATUS_MSG", "Ekki sГЅna stГ¶Г°u minnar tengingar");
	define("SUMMARY_MSG", "Samantekt");

	// no records messages
	define("NO_RECORDS_MSG", "Engar skrГЎr fundust");
	define("NO_EVENTS_MSG", "Engir atburГ°ir fundust");
	define("NO_QUESTIONS_MSG", "Engar spurningar fundust");
	define("NO_NEWS_MSG", "Engar nГЅjar greinar fundust");
	define("NO_POLLS_MSG", "Engar kannanir fundust");

	// SMS messages
	define("SMS_TITLE", "SMS");
	define("SMS_TEST_TITLE", "SMS prufa");
	define("SMS_TEST_DESC", "Vinsamlega skrifiГ° farsГ­manГєmeriГ° og smelliГ° ГЎ 'SEND_BUTTON' til aГ° fГЎ prufuskilaboГ°");
	define("INVALID_CELL_PHONE", "Ekki rГ©tt farsГ­manГєmer");

	define("ARTICLE_RELATED_PRODUCTS_TITLE", "VГ¶rur sem tengjast grein");
	define("CATEGORY_RELATED_PRODUCTS_TITLE", "VГ¶rur sem tengjast flokk");
	define("SELECT_TYPE_MSG", "Velja gerГ°");
	define("OFFER_PRICE_MSG", "TilboГ°sverГ°");
	define("OFFER_MESSAGE_MSG", "TilboГ°sskilaboГ°");

	define("MY_WISHLIST_MSG", "Minn Гіskalisti");
	define("SELECT_WISHLIST_TYPE_MSG", "Please select a type to save product in your wishlist");
	define("MY_REMINDERS_MSG", "MГ­nar ГЎminningar");
	define("EDIT_REMINDER_MSG", "Breyta ГЎminningu");


	define("SELECT_SUBFOLDER_MSG", "Select Subfolder");
	define("CURRENT_DIR_MSG", "Current directory");
	define("NO_AVAILIABLE_CATEGORIES_MSG", "No categories available");
	define("SHOW_FOR_NON_REGISTERED_USERS_MSG", "Show for non registered users");
	define("SHOW_FOR_REGISTERED_USERS_MSG", "Show for registered users");
	define("VIEW_ITEM_IN_THE_LIST_MSG", "View item in products list");
	define("ACCESS_DETAILS_MSG", "Access Details");
	define("ACCESS_ITEMS_DETAILS_MSG", "Access item details / buy item");
	define("OTHER_SUBSCRIPTIONS_MSG", "Other Subscriptions");
	define("USE_CATEGORY_ALL_SITES_MSG", "Use this category for all sites (untick this checkbox to select sites manually)");
	define("USE_ITEM_ALL_SITES_MSG", "Use this item for all sites (untick this checkbox to select sites manually) ");
	define("SAVE_SUBSCRIPTIONS_SETTINGS_BY_CATEGORY_MSG", "Save subscriptions settings from categories");
	define("SAVE_SITES_SETTINGS_BY_CATEGORY_MSG", "Save sites settings from categories");
	define("ACCESS_LEVELS_MSG", "Access Levels");
	define("SITES_MSG", "Sites");
	define("NON_REGISTERED_USERS_MSG", "Non registered users");
	define("REGISTERED_CUSTOMERS_MSG", "Registered Users");
	define("PREVIEW_IN_SEPARATE_SECTION_MSG", "Show in separate section");
	define("PREVIEW_BELOW_DETAILS_IMAGE_MSG", "Show below image on details page");
	define("PREVIEW_BELOW_LIST_IMAGE_MSG", "Show below image on listing page");
	define("PREVIEW_POSITION_MSG", "Position");
	define("ADMIN_NOTES_MSG", "Administrator Notes");
	define("USER_NOTES_MSG", "User Notes");
	define("CMS_PERMISSIONS_MSG", "CMS Permissions");
	define("ARTICLES_PERMISSIONS_MSG", "Articles Permissions");
	define("FOOTER_LINK_MSG", "Footer Link");
	define("FOOTER_LINKS_MSG", "Footer Links");
	define("WISHLIST_MSG", "Wishlist");
	define("TYPE_IS_NOT_AVAILIABLE_MSG", "Type is not availiable");
	define("TYPE_IS_NOT_SELECTED_MSG", "Type is not selected");
	define("SHOW_ALL_MSG", "Show all");

	define("MEMBER_SINCE_MSG", "Member Since");
	define("SITEMAP_TITLE_INDEX", "title");
	define("SITEMAP_URL_INDEX", "url");
	define("SITEMAP_SUBS_INDEX", "subs");

	define("HOT_RELEASES_MSG", "Hot Releases");
	define("PAY_FOR_AD_MSG", "Pay for Ad");
	define("CHECK_SITE_MSG", "check the site");

	define("CONTACT_US_MSG", "Contact Us");
	define("REGISTERED_USERS_ALLOWED_MESSAGES_MSG", "Only registered users can send their messages.");
	define("NOT_ALLOWED_SEND_MESSAGES_MSG", "Sorry, but you are not allowed to send messages.");
	define("MESSAGE_SENT_MSG", "The message has been sent");
	define("MESSAGE_INTERVAL_ERROR", "Please wait for {interval_time} to send your message.");
	define("ONE_LINE_SUMMARY_MSG", "ГљtdrГЎttur af vef");
	define("DETAILED_COMMENT_MSG", "NГЎkvГ¦mar athugasemdir");

?>