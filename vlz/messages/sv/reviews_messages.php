<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	// reviews/rating messages
	define("OPTIONAL_MSG", "Valfritt");
	define("BAD_MSG", "Dеlig");
	define("POOR_MSG", "Sеdдr");
	define("AVERAGE_MSG", "Medel");
	define("GOOD_MSG", "Bra");
	define("EXCELLENT_MSG", "Utmдrkt");
	define("REVIEWS_MSG", "Recensioner");
	define("NO_REVIEWS_MSG", "Inga recensioner funna");
	define("WRITE_REVIEW_MSG", "Skriv en recension");
	define("RATE_PRODUCT_MSG", "Betygsдtt denna produkt");
	define("RATE_ARTICLE_MSG", "Betygsдtt denna artikel");
	define("NOT_RATED_PRODUCT_MSG", "Produkten дr inte betygsatt дn.");
	define("NOT_RATED_ARTICLE_MSG", "Artikeln дr inte betygsatt дn.");
	define("AVERAGE_RATING_MSG", "Medelbetyg hos kunderna.");
	define("BASED_ON_REVIEWS_MSG", "baserat pе {total_votes} recensioner");
	define("POSITIVE_REVIEW_MSG", "Positivt kundbetyg");
	define("NEGATIVE_REVIEW_MSG", "Negativt kundbetyg");
	define("SEE_ALL_REVIEWS_MSG", "Se alla kunders recensioner");
	define("ALL_REVIEWS_MSG", "Alla recensioner");
	define("ONLY_POSITIVE_MSG", "Endast positiva");
	define("ONLY_NEGATIVE_MSG", "Endast negativa");
	define("POSITIVE_REVIEWS_MSG", "Positiva recensioner");
	define("NEGATIVE_REVIEWS_MSG", "Negativa recensioner");
	define("SUBMIT_REVIEW_MSG", "Tack sе mycket!<br>Din kommentar kommer granskas. Om den godkдnns kommer den snart synas pе webbplatsen. <br>Vi reserverar rдtten att neka insдndare som inte mцter vеra riktlinjer.");
	define("ALREADY_REVIEW_MSG", "Du har redan skrivit en recension.");
	define("RECOMMEND_PRODUCT_MSG", "Kan du rekommendera<br> denna produkt till andra?");
	define("RECOMMEND_ARTICLE_MSG", "Kan du rekommendera<br> denna artikel till andra?");
	define("RATE_IT_MSG", "Betygsдtt den");
	define("NAME_ALIAS_MSG", "Namn eller alias");
	define("SHOW_MSG", "Visa");
	define("FOUND_MSG", "Hittad");
	define("RATING_MSG", "Medelbetyg");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Only registered users can add their reviews.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "Sorry, but you are not allowed to add reviews.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Allowed to See Reviews");
	define("ALLOWED_POST_REVIEWS_MSG", "Allowed to Post Reviews");
	define("REVIEWS_RESTRICTIONS_MSG", "Reviews Restrictions");
	define("REVIEWS_PER_USER_MSG", "Number of Reviews per User");
	define("REVIEWS_PER_USER_DESC", "leave this field blank if you do not want to limit number of reviews user can post");
	define("REVIEWS_PERIOD_MSG", "Reviews Period");
	define("REVIEWS_PERIOD_DESC", "period when reviews restrictions are in force");

	define("AUTO_APPROVE_REVIEWS_MSG", "Auto Approve Reviews");
	define("BY_RATING_MSG", "By Rating");
	define("SELECT_REVIEWS_FIRST_MSG", "Please select reviews first.");
	define("SELECT_REVIEWS_STATUS_MSG", "Please select status.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "You are about to change the status of selected reviews to '{status_name}'. Continue?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Are you sure you want remove selected reviews ({total_reviews})?");

?>