<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      Viart Shop 4.1 RE RE                                                ***
  ***      File:  admin_reviews.php                                        ***
  ***      Built: Sat Sep  1 19:08:10 2012                                 ***
  ***      http://www.viarts.ru                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	include_once("./admin_config.php");
	include_once($root_folder_path."includes/common.php");
	include_once($root_folder_path."includes/sorter.php");
	include_once($root_folder_path."includes/navigator.php");
	include_once($root_folder_path."includes/record.php");
	include_once($root_folder_path."includes/reviews_functions.php");
	include_once($root_folder_path."messages/".$language_code."/reviews_messages.php");

	include_once("./admin_common.php");

	check_admin_security("products_reviews");

	// begin delete selected reviews
	$operation = get_param("operation");

	// update and remove operations
	$reviews_ids = get_param("reviews_ids");
	//$items_ids = get_param("items_ids");
	$status_id = get_param("status_id");
	if (strlen($operation)) {
		if ($reviews_ids) {
			if ($operation == "remove_reviews") {
				$sql  = " DELETE FROM " . $table_prefix . "user_reviews ";
				$sql .= " WHERE ur_id IN (" . $db->tosql($reviews_ids, INTEGERS_LIST) . ")";
				$db->query($sql);
			} else if ($operation == "update_status" && strlen($status_id)) {

				$sql  = " UPDATE " . $table_prefix . "user_reviews ";
				$sql .= " SET approved=" . $db->tosql($status_id, INTEGER);
				$sql .= " WHERE ur_id IN (" . $db->tosql($reviews_ids, INTEGERS_LIST) . ")";
				$db->query($sql);
			}
		}
		/*if ($items_ids) {
			update_product_rating($items_ids);
		}*/
	}
	// end operations

	$t = new VA_Template($settings["admin_templates_dir"]);
	$t->set_file("main","admin_users_reviews.html");

	$t->set_var("admin_href", "admin.php");
	$t->set_var("admin_reviews_href", "admin_users_reviews.php");
	$t->set_var("admin_items_list_href", "admin_items_list.php");

	// prepare dates for stats
	$current_date = va_time();
	$cyear = $current_date[YEAR]; $cmonth = $current_date[MONTH]; $cday = $current_date[DAY];
	$today_ts = mktime (0, 0, 0, $cmonth, $cday, $cyear);
	$tomorrow_ts = mktime (0, 0, 0, $cmonth, $cday + 1, $cyear);
	$yesterday_ts = mktime (0, 0, 0, $cmonth, $cday - 1, $cyear);
	$week_ts = mktime (0, 0, 0, $cmonth, $cday - 6, $cyear);
	$month_ts = mktime (0, 0, 0, $cmonth, 1, $cyear);
	$last_month_ts = mktime (0, 0, 0, $cmonth - 1, 1, $cyear);
	$last_month_days = date("t", $last_month_ts);
	$last_month_end = mktime (0, 0, 0, $cmonth - 1, $last_month_days, $cyear);

	$t->set_var("date_edit_format", join("", $date_edit_format));

	// show reviews statistics
	$reviews_types = array(
		array("1", IS_APPROVED_MSG),
		array("0", NOT_APPROVED_MSG),
	);

	$reviews_total_online = 0; 

	// search form
	$approved_options = array(array("", ALL_MSG), array("1", IS_APPROVED_MSG), array("0", NOT_APPROVED_MSG));
	$rating_options = 
		array( 
			array("", ""), array(1, BAD_MSG), array(2, POOR_MSG), 
			array(3, AVERAGE_MSG), array(4, GOOD_MSG), array(5, EXCELLENT_MSG),
			);

	$r = new VA_Record($table_prefix . "user_reviews");
	$r->add_textbox("s_ne", TEXT);
	$r->change_property("s_ne", TRIM, true);


	$r->add_select("s_rt", INTEGER, $rating_options);

	$r->add_select("s_ap", TEXT, $approved_options);
	$r->get_form_parameters();
	$r->validate();
	$approved_options = array(array("", ""), array("1", IS_APPROVED_MSG), array("0", NOT_APPROVED_MSG));
	$r->add_select("status_id", TEXT, $approved_options);
	$r->set_form_parameters();
	// end search form

	// build where condition
	$where = "";
	if (!$r->errors)
	{
		if (!$r->is_empty("s_ne")) {
			if (strlen($where)) { $where .= " AND "; }
			$s_ne_sql = $db->tosql($r->get_value("s_ne"), TEXT, false);
			$where .= " (r.user_email LIKE '%" . $s_ne_sql . "%'";
			$where .= " OR r.user_name LIKE '%" . $s_ne_sql . "%')";
		}


		if (!$r->is_empty("s_rt")) {
			if (strlen($where)) { $where .= " AND "; }
			$where .= " r.rating=" . $db->tosql($r->get_value("s_rt"), INTEGER);
		}


		if (!$r->is_empty("s_ap")) {
			if (strlen($where)) { $where .= " AND "; }
			$where .= " r.approved=" . $db->tosql($r->get_value("s_ap"), INTEGER);
		}

	}
	$where_sql = ""; $where_and_sql = "";
	if (strlen($where)) {
		$where_sql = " WHERE " . $where;
		$where_and_sql = " AND " . $where;
	}

	$s = new VA_Sorter($settings["admin_templates_dir"], "sorter_img.html", "admin_users_reviews.php");
	$s->set_parameters(false, true, true, false);
	$s->set_default_sorting(5, "desc");
	$s->set_sorter(ID_MSG, "sorter_review_id", "1", "ur_id");
	$s->set_sorter(USER_NAME_MSG, "sorter_user_name", "2", "user_name");
	$s->set_sorter(RATING_MSG, "sorter_rating", "4", "rating");
	$s->set_sorter(REVIEW_ADDED_MSG, "sorter_date_added", "5", "date_added");
	$s->set_sorter(APPROVED_QST, "sorter_approved", "6", "approved");

	$n = new VA_Navigator($settings["admin_templates_dir"], "navigator.html", "admin_users_reviews.php");

	include_once("./admin_header.php");
	include_once("./admin_footer.php");

	
	// set up variables for navigator
	$db->query("SELECT COUNT(*) FROM " . $table_prefix . "user_reviews r ". $where_sql);
	$db->next_record();
	$total_records = $db->f(0);
	$records_per_page = 15;
	$pages_number = 5;
	$page_number = $n->set_navigator("navigator", "page", SIMPLE, $pages_number, $records_per_page, $total_records, false);
	
	$db->RecordsPerPage = $records_per_page;
	$db->PageNumber = $page_number;
	$db->query("SELECT * FROM " . $table_prefix . "user_reviews r " . $where_sql . $s->order_by);
	$review_index = 0;
	if($db->next_record())
	{
		$t->parse("sorters", false);
		$t->set_var("no_records", "");

		$admin_review_url = new VA_URL("admin_user_review.php", false);
		$admin_review_url->add_parameter("s_ne", REQUEST, "s_ne");
		$admin_review_url->add_parameter("s_rt", REQUEST, "s_rt");
		$admin_review_url->add_parameter("s_ap", REQUEST, "s_ap");
		$admin_review_url->add_parameter("page", REQUEST, "page");
		$admin_review_url->add_parameter("sort_ord", REQUEST, "sort_ord");
		$admin_review_url->add_parameter("sort_dir", REQUEST, "sort_dir");
		
		do
		{
			$review_index++;
			$t->set_var("review_index", $review_index);
			$review_id = $db->f("ur_id");

			$admin_review_url->add_parameter("ur_id", CONSTANT, $review_id);
			$t->set_var("admin_review_url", $admin_review_url->get_url());

			//$row_style = "rowWarn"; // to be used for IP addresses from black list
			$row_style = ($review_index % 2 == 0) ? "row1" : "row2";
			$t->set_var("row_style", $row_style);

			$t->set_var("review_id", $review_id);
			$t->set_var("user_name", htmlspecialchars($db->f("user_name")));
			$t->set_var("summary", htmlspecialchars($db->f("comments")));
			$t->set_var("rating", get_array_value($db->f("rating"), $rating_options));
			$date_added = $db->f("date_added", DATETIME);
			$t->set_var("date_added", va_date($date_show_format, $date_added));
			$approved = $db->f("approved");
			if ($approved) {
				$approved = "<font color=\"blue\"><b>" . YES_MSG . "</b></font>";
			} else  {
				$approved = "<font color=\"silver\">" . NO_MSG . "</font>";
			} 
			$t->set_var("approved", $approved);
			$t->parse("records", true);
		} while($db->next_record());
		$t->set_var("reviews_number", $review_index);
	}
	else
	{
		$t->set_var("records", "");
		$t->set_var("navigator", "");
		$t->parse("no_records", false);
	}

	$t->set_var("s_rt_search", $r->get_value("s_rt"));
	$t->set_var("s_rc_search", $r->get_value("s_rc"));
	$t->set_var("s_ap_search", $r->get_value("s_ap"));

	$t->set_var("admin_href", "admin.php");
	$t->pparse("main");
?>
