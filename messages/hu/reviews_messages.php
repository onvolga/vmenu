<?php
/*
  ****************************************************************************
  ***                                                                      ***
  ***      ViArt Shop 4.0.7                                                ***
  ***      File:  reviews_messages.php                                     ***
  ***      Built: Sun Jun 12 18:19:39 2011                                 ***
  ***      http://www.viart.com                                            ***
  ***                                                                      ***
  ****************************************************************************
*/


	//Р±ttekintР№sek/osztР±lyozР±s СЊzenetek
	define("OPTIONAL_MSG", "VР±laszthatСѓ");
	define("BAD_MSG", "Rossz");
	define("POOR_MSG", "Gyenge");
	define("AVERAGE_MSG", "Р‘tlagos Р¬gyfР№l OsztР±lyozР±s");
	define("GOOD_MSG", "JСѓ");
	define("EXCELLENT_MSG", "KitС‹nС…");
	define("REVIEWS_MSG", "VР№lemР№nyek");
	define("NO_REVIEWS_MSG", "Nincs vР№lemР№ny");
	define("WRITE_REVIEW_MSG", "Рќrj egy vР№lemР№nyt");
	define("RATE_PRODUCT_MSG", "Р™rtР№keld ezt a termР№ket!");
	define("RATE_ARTICLE_MSG", "Р™rtР№keld ezt a cikket!");
	define("NOT_RATED_PRODUCT_MSG", "A termР№k mР№g nincs osztР±lyozva");
	define("NOT_RATED_ARTICLE_MSG", "A cikk mР№g nincs osztР±lyozva");
	define("AVERAGE_RATING_MSG", "A vevС…i osztР±lyozР±s Р±tlaga");
	define("BASED_ON_REVIEWS_MSG", "{total_votes}  vР№lemР№nyen alapul");
	define("POSITIVE_REVIEW_MSG", "PozitРЅv vР±sР±rlСѓi vР№lemР№ny");
	define("NEGATIVE_REVIEW_MSG", "NegatРЅv vР±sР±rlСѓi vР№lemР№ny");
	define("SEE_ALL_REVIEWS_MSG", "NР№zd meg az С†sszes vР±sР±rlСѓi vР№lemР№nyt.");
	define("ALL_REVIEWS_MSG", "Mindegyik vР№lemР№ny");
	define("ONLY_POSITIVE_MSG", "Csak PozitРЅv");
	define("ONLY_NEGATIVE_MSG", "Csak NegatРЅv");
	define("POSITIVE_REVIEWS_MSG", "PozitРЅv vР№lemР№nyek");
	define("NEGATIVE_REVIEWS_MSG", "NegatРЅv vР№lemР№nyek");
	define("SUBMIT_REVIEW_MSG", "KС†szС†njСЊk<br>A vР№lemР№nyedet Р±tnР№zzСЊk ,Р№s ha elfogadР±sra kerСЊl  , meg fog jelenni az oldalunkon.<br>Fent tartjuk a jogot az elutasРЅtР±shoz ,ha a bekСЊldС†tt anyag nem felel meg a weboldalunk szellemisР№gР№nek.");
	define("ALREADY_REVIEW_MSG", "MР±r helyeztР№l egy Р№rtР№kelР№st.");
	define("RECOMMEND_PRODUCT_MSG", "JavasoknР±d ezt a termР№ket mР±soknak?");
	define("RECOMMEND_ARTICLE_MSG", "JavasolnР±l ezt a cikket mР±soknak?");
	define("RATE_IT_MSG", "Р™rtР№keld!");
	define("NAME_ALIAS_MSG", "NР№v vagy nick");
	define("SHOW_MSG", "Megtekint");
	define("FOUND_MSG", "TalР±l");
	define("RATING_MSG", "Р™rtР№kelР№s Р±tlaga");
	define("REGISTERED_USERS_ADD_REVIEWS_MSG", "Csak regisztrР±lt felhasznР±lСѓk kСЊldhetnek hozzР±szСѓlР±st.");
	define("NOT_ALLOWED_ADD_REVIEW_MSG", "SajnР±ljuk, de nem kСЊldhetsz be hozzР±szСѓlР±st.");

	define("ALLOWED_VIEW_REVIEWS_MSG", "Megtekintheti a HozzР±szСѓlР±sokat");
	define("ALLOWED_POST_REVIEWS_MSG", "BekСЊldhet HozzР±szСѓlР±sokat");
	define("REVIEWS_RESTRICTIONS_MSG", "HozzР±szСѓlР±s KorlР±tozР±sok");
	define("REVIEWS_PER_USER_MSG", "HozzР±szСѓlР±sok szР±ma FelhasznР±lСѓnkР№nt");
	define("REVIEWS_PER_USER_DESC", "hagyd СЊresen, ha nem akarod korlР±tozni a felhasznР±lСѓnkР№nti hozzР±szСѓlР±sok szР±mР±t");
	define("REVIEWS_PERIOD_MSG", "HozzР±szСѓlР±s PeriСѓdus");
	define("REVIEWS_PERIOD_DESC", "ennyi ideig ervР№nyesek a hozzР±szСѓlР±s korlР±tozР±sok");

	define("AUTO_APPROVE_REVIEWS_MSG", "Automatikus HozzР±szСѓlР±s elfogadР±s");
	define("BY_RATING_MSG", "OsztР±lyzat szerint");
	define("SELECT_REVIEWS_FIRST_MSG", "KР№rlek, elС…bb vР±lassz ki hozzР±szСѓlР±sokat.");
	define("SELECT_REVIEWS_STATUS_MSG", "KР№rlek, vР±lassz stР±tuszt.");
	define("REVIEWS_STATUS_CONFIRM_MSG", "A kivР±lasztott hozzР±szСѓlР±sok stР±tuszР±t erre akarod vР±ltoztatni: '{status_name}'. Folytatod?");
	define("REVIEWS_DELETE_CONFIRM_MSG", "Biztosan tС†rС†lni akarod a kivР±lasztott hozzР±szСѓlР±sokat ({total_reviews})?");

?>